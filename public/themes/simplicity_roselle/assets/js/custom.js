/*
 * Custom code goes here.
 * A template should always ship with an empty custom.js
 */

window.onscroll = function() {myFunction()};
var navbar = document.getElementById("roselle-navbar");
var stickyPos = $('#roselle-navbar').height();
function myFunction() {
    if (window.pageYOffset >= stickyPos) {
        navbar.classList.add("navbar-fixed-top");
        $('.logo-wrapper').addClass('shrink');
    } else {
        navbar.classList.remove("navbar-fixed-top");
        $('.logo-wrapper').removeClass('shrink');

    }
}
