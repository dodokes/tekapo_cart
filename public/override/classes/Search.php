<?php
if (!defined('_PS_VERSION_'))
    exit;

class Search extends SearchCore
{

    public static function findCms(
        $id_lang,
        $expr,
        $ajax = false,
        Context $context = null
    ) {

        if (!$context) {
            $context = Context::getContext();
        }
        $db = Db::getInstance(_PS_USE_SQL_SLAVE_);

        $page_number = 1;
        $page_size = 100;

        $intersect_array = array();
        $score_array = array();
        $words = explode(' ', Search::sanitize($expr, $id_lang, false, $context->language->iso_code));

        foreach ($words as $key => $word) {
            if (!empty($word) && strlen($word) >= (int)Configuration::get('PS_SEARCH_MINWORDLEN')) {
                $sql_param_search = self::getSearchParamFromWord($word);

                $intersect_array[] = 'SELECT DISTINCT si.id_cms
					FROM '._DB_PREFIX_.'search_word_cms sw
					LEFT JOIN '._DB_PREFIX_.'search_index_cms si ON sw.id_word = si.id_word
					WHERE sw.id_lang = '.(int)$id_lang.'
						AND sw.id_shop = '.$context->shop->id.'
						AND sw.word LIKE
					\''.$sql_param_search.'\'';

                $score_array[] = 'sw.word LIKE \''.$sql_param_search.'\'';
            } else {
                unset($words[$key]);
            }
        }

        if (!count($words)) {
            return ($ajax ? array() : array('total' => 0, 'result' => array()));
        }

        $score = '';
        if (is_array($score_array) && !empty($score_array)) {
            $score = ',(
				SELECT SUM(weight)
				FROM '._DB_PREFIX_.'search_word_cms sw
				LEFT JOIN '._DB_PREFIX_.'search_index_cms si ON sw.id_word = si.id_word
				WHERE sw.id_lang = '.(int)$id_lang.'
					AND sw.id_shop = '.$context->shop->id.'
					AND si.id_cms = c.id_cms
					AND ('.implode(' OR ', $score_array).')
			) position';
        }

        $results = $db->executeS('
		SELECT DISTINCT c.`id_cms`
		FROM `'._DB_PREFIX_.'cms` c
		'.Shop::addSqlAssociation('cms', 'c', false).'
		WHERE c.`active` = 1
		AND c.`indexation` = 1
		AND cms_shop.indexed = 1
		', true, false);

        $eligible_cms_list = array();
        foreach ($results as $row) {
            $eligible_cms_list[] = $row['id_cms'];
        }

        $eligible_cms_list2 = array();
        foreach ($intersect_array as $query) {
            foreach ($db->executeS($query, true, false) as $row) {
                $eligible_cms_list2[] = $row['id_cms'];
            }
        }
        $eligible_cms_list = array_unique(array_intersect($eligible_cms_list, array_unique($eligible_cms_list2)));
        if (!count($eligible_cms_list)) {
            return ($ajax ? array() : array('total' => 0, 'result' => array()));
        }

        $cms_pool = '';
        foreach ($eligible_cms_list as $id_cms) {
            if ($id_cms) {
                $cms_pool .= (int)$id_cms.',';
            }
        }
        if (empty($cms_pool)) {
            return ($ajax ? array() : array('total' => 0, 'result' => array()));
        }
        $cms_pool = ((strpos($cms_pool, ',') === false) ? (' = '.(int)$cms_pool.' ') : (' IN ('.rtrim($cms_pool, ',').') '));

        if ($ajax) {
            $sql = 'SELECT DISTINCT c.id_cms, cl.meta_title cms_meta_title, cl.content cms_content, cl.link_rewrite '.$score.'
					FROM '._DB_PREFIX_.'cms c
					INNER JOIN `'._DB_PREFIX_.'cms_lang` cl ON (
						c.`id_cms` = cl.`id_cms`
						AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').'
					)
					'.Shop::addSqlAssociation('cms', 'c').'
					WHERE p.`id_cms` '.$cms_pool.'
					ORDER BY position DESC LIMIT 10';
            return $db->executeS($sql, true, false);
        }

        $sql = 'SELECT c.*, cms_shop.*, cl.`meta_title`, cl.`meta_description`, cl.`link_rewrite` '.$score.' 
				FROM '._DB_PREFIX_.'cms c
				'.Shop::addSqlAssociation('cms', 'c').'
				INNER JOIN `'._DB_PREFIX_.'cms_lang` cl ON (
					c.`id_cms` = cl.`id_cms`
					AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').'
				)
				WHERE c.`id_cms` '.$cms_pool.'
				GROUP BY cms_shop.id_cms
				ORDER BY position DESC 
				LIMIT '.(int)(($page_number - 1) * $page_size).','.(int)$page_size;
        $result = $db->executeS($sql, true, false);

        $sql = 'SELECT COUNT(*)
				FROM '._DB_PREFIX_.'cms c
				'.Shop::addSqlAssociation('cms', 'c').'
				INNER JOIN `'._DB_PREFIX_.'cms_lang` cl ON (
					c.`id_cms` = cl.`id_cms`
					AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').'
				)
				WHERE c.`id_cms` '.$cms_pool;
        $total = $db->getValue($sql, false);

        if (!$result) {
            $result_properties = false;
        } else {
            $result_properties = $result;
        }

        return array('total' => $total,'result' => $result_properties);
    }

    protected static function getCmsListToIndex($total_languages, $id_cms = false, $limit = 50, $weight_array = array())
    {
        $ids = null;

        if (!$id_cms) {

            $sql = 'SELECT c.id_cms FROM '._DB_PREFIX_.'cms c
				'.Shop::addSqlAssociation('cms', 'c', true, null, true).'
				WHERE cms_shop.`indexed` = 0
				AND c.`indexation` = 1
				AND c.`active` = 1
				ORDER BY cms_shop.`id_cms` ASC
				LIMIT '.(int)$limit;

            $res = Db::getInstance()->executeS($sql, false);
            while ($row = Db::getInstance()->nextRow($res)) {
                $ids[] = $row['id_cms'];
            }
        }

        // Now get every attribute in every language
        $sql = 'SELECT c.id_cms, cl.id_lang, cl.id_shop, l.iso_code';

        if (is_array($weight_array)) {
            foreach ($weight_array as $key => $weight) {
                if ((int)$weight) {
                    switch ($key) {
                        case 'cms_meta_title':
                            $sql .= ', cl.meta_title cms_meta_title';
                            break;
                        case 'cms_content':
                            $sql .= ', cl.content cms_content';
                            break;
                    }
                }
            }
        }

        $sql .= ' FROM '._DB_PREFIX_.'cms c
			LEFT JOIN '._DB_PREFIX_.'cms_lang cl
				ON c.id_cms = cl.id_cms
			'.Shop::addSqlAssociation('cms', 'c', true, null, true).'
			LEFT JOIN '._DB_PREFIX_.'lang l
				ON l.id_lang = cl.id_lang
			WHERE cms_shop.indexed = 0
			AND c.indexation = 1
			'.($id_cms ? 'AND c.id_cms = '.(int)$id_cms : '').'
			'.($ids ? 'AND c.id_cms IN ('.implode(',', array_map('intval', $ids)).')' : '').'
			AND c.`active` = 1
			AND cl.`id_shop` = cms_shop.`id_shop`';

        return Db::getInstance()->executeS($sql, true, false);
    }

    public static function indexationCms($full = false, $id_cms = false)
    {

        $db = Db::getInstance();

        if ($id_cms) {
            $full = false;
        }

        if ($full && Context::getContext()->shop->getContext() == Shop::CONTEXT_SHOP) {
            $db->execute('DELETE si, sw FROM `'._DB_PREFIX_.'search_index_cms` si
				INNER JOIN `'._DB_PREFIX_.'cms` c ON (c.id_cms = si.id_cms)
				'.Shop::addSqlAssociation('cms', 'c').'
				INNER JOIN `'._DB_PREFIX_.'search_word_cms` sw ON (sw.id_word = si.id_word AND cms_shop.id_shop = sw.id_shop)
				WHERE c.`indexation` = 1
				AND c.`active` = 1');
            $db->execute('UPDATE `'._DB_PREFIX_.'cms` c
				'.Shop::addSqlAssociation('cms', 'c').'
				SET cms_shop.`indexed` = 0
				WHERE c.`indexation` = 1
				AND c.`active` = 1
				');
        } elseif ($full) {
            $db->execute('TRUNCATE '._DB_PREFIX_.'search_index_cms');
            $db->execute('TRUNCATE '._DB_PREFIX_.'search_word_cms');

            $db->execute('UPDATE `'._DB_PREFIX_.'cms` c
				'.Shop::addSqlAssociation('cms', 'c').'
				SET cms_shop.`indexed` = 0
				WHERE c.`indexation` = 1
				AND c.`active` = 1
				');
        } else {
            $db->execute('DELETE si FROM `'._DB_PREFIX_.'search_index_cms` si
				INNER JOIN `'._DB_PREFIX_.'cms` c ON (c.id_cms = si.id_cms)
				'.Shop::addSqlAssociation('cms', 'c').'
				WHERE c.`indexation` = 1
				AND c.`active` = 1 
				AND '.($id_cms ? 'c.`id_cms` = '.(int)$id_cms : 'cms_shop.`indexed` = 0'));

            $db->execute('UPDATE `'._DB_PREFIX_.'cms` c
				'.Shop::addSqlAssociation('cms', 'c').'
				SET cms_shop.`indexed` = 0
				WHERE c.`indexation` = 1
				AND c.`active` = 1
				AND '.($id_cms ? 'c.`id_cms` = '.(int)$id_cms : 'c.`indexed` = 0'));
        }

        // Every fields are weighted according to the configuration in the backend
        $weight_array = array(
            'cms_meta_title' => Configuration::get('PS_SEARCH_WEIGHT_CMS_META_TITLE'),
            'cms_content' => Configuration::get('PS_SEARCH_WEIGHT_CMS_CONTENT')
        );

        // Those are kind of global variables required to save the processed data in the database every X occurrences, in order to avoid overloading MySQL
        $count_words = 0;
        $query_array3 = array();

        // Retrieve the number of languages
        $total_languages = count(Language::getIDs(false));

        // Products are processed 50 by 50 in order to avoid overloading MySQL
        while (($cms_list = Search::getCmsListToIndex($total_languages, $id_cms, 50, $weight_array)) && (count($cms_list) > 0)) {
            $cms_list_array = array();
            // Now each non-indexed product is processed one by one, langage by langage

            foreach ($cms_list as $cms) {

                // Data must be cleaned of html, bad characters, spaces and anything, then if the resulting words are long enough, they're added to the array
                $cms_array = array();
                foreach ($cms as $key => $value) {
                    Search::fillCmsArray($cms_array, $weight_array, $key, $value, $cms['id_lang'], $cms['iso_code']);
                }

                // If we find words that need to be indexed, they're added to the word table in the database
                if (is_array($cms_array) && !empty($cms_array)) {
                    $query_array = $query_array2 = array();
                    foreach ($cms_array as $word => $weight) {
                        if ($weight) {
                            $query_array[$word] = '('.(int)$cms['id_lang'].', '.(int)$cms['id_shop'].', \''.pSQL($word).'\')';
                            $query_array2[] = '\''.pSQL($word).'\'';
                        }
                    }

                    if (is_array($query_array) && !empty($query_array)) {
                        // The words are inserted...
                        $db->execute('
						INSERT IGNORE INTO '._DB_PREFIX_.'search_word_cms (id_lang, id_shop, word)
						VALUES '.implode(',', $query_array), false);
                    }
                    $word_ids_by_word = array();
                    if (is_array($query_array2) && !empty($query_array2)) {
                        // ...then their IDs are retrieved

                        $added_words = $db->executeS('
						SELECT sw.id_word, sw.word
						FROM '._DB_PREFIX_.'search_word_cms sw
						WHERE sw.word IN ('.implode(',', $query_array2).')
						AND sw.id_lang = '.(int)$cms['id_lang'].'
						AND sw.id_shop = '.(int)$cms['id_shop'], true, false);
                        foreach ($added_words as $word_id) {
                            $word_ids_by_word['_'.$word_id['word']] = (int)$word_id['id_word'];
                        }
                    }
                }

                foreach ($cms_array as $word => $weight) {
                    if (!$weight) {
                        continue;
                    }
                    if (!isset($word_ids_by_word['_'.$word])) {
                        continue;
                    }
                    $id_word = $word_ids_by_word['_'.$word];
                    if (!$id_word) {
                        continue;
                    }
                    $query_array3[] = '('.(int)$cms['id_cms'].','.
                        (int)$id_word.','.(int)$weight.')';

                    // Force save every 200 words in order to avoid overloading MySQL
                    if (++$count_words % 200 == 0) {
                        Search::saveIndexCms($query_array3);
                    }
                }

                $cms_list_array[] = (int)$cms['id_cms'];

            }

            $cms_list_array = array_unique($cms_list_array);

            $db->execute('UPDATE `'._DB_PREFIX_.'cms` c
				'.Shop::addSqlAssociation('cms', 'c').'
				SET cms_shop.`indexed` = 1
				WHERE c.`indexation` = 1
				AND c.`active` = 1
				AND c.`id_cms` IN (' . implode(',', array_values($cms_list_array)) . ')');

            // One last save is done at the end in order to save what's left
            Search::saveIndexCms($query_array3);

        }



        return true;
    }

    /**
     * @param $cms_array
     * @param $weight_array
     * @param $key
     * @param $value
     * @param $id_lang
     * @param $iso_code
     */
    protected static function fillCmsArray(&$cms_array, $weight_array, $key, $value, $id_lang, $iso_code)
    {
        if (strncmp($key, 'id_', 3) && isset($weight_array[$key])) {
            $words = explode(' ', Search::sanitize($value, (int)$id_lang, true, $iso_code));
            foreach ($words as $word) {
                if (!empty($word)) {
                    $word = Tools::substr($word, 0, PS_SEARCH_MAX_WORD_LENGTH);

                    if (!isset($cms_array[$word])) {
                        $cms_array[$word] = 0;
                    }
                    $cms_array[$word] += $weight_array[$key];
                }
            }
        }
    }

    /*
    protected static function setCmsAsIndexed(&$cms_list)
    {

        if (is_array($cms_list) && !empty($cms_list)) {
            ObjectModel::updateMultishopTable('Cms', array('indexed' => 1), 'a.id_cms IN ('.implode(',', array_map('intval', $cms_list)).')');
        }
    }
    */

    /** $queryArray3 is automatically emptied in order to be reused immediatly */
    protected static function saveIndexCms(&$queryArray3)
    {
        if (is_array($queryArray3) && !empty($queryArray3)) {
            $query = 'INSERT INTO '._DB_PREFIX_.'search_index_cms (id_cms, id_word, weight)
				VALUES '.implode(',', $queryArray3).'
				ON DUPLICATE KEY UPDATE weight = weight + VALUES(weight)';

            Db::getInstance()->execute($query, false);
        }
        $queryArray3 = array();
    }

}