<?php
/**
 * 2007-2018 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */
use PrestaShop\PrestaShop\Core\Product\Search\ProductSearchQuery;
use PrestaShop\PrestaShop\Core\Product\Search\SortOrder;
use PrestaShop\PrestaShop\Adapter\Search\SearchProductSearchProvider;

class SearchControllerCore extends ProductListingFrontController
{
    public $php_self = 'search';
    public $instant_search;
    public $ajax_search;

    private $search_string;
    private $search_tag;

    /**
     * Assign template vars related to page content.
     *
     * @see FrontController::initContent()
     */
    public function init()
    {
        parent::init();

        $this->search_string = Tools::getValue('s');
        if (!$this->search_string) {
            $this->search_string = Tools::getValue('search_query');
        }

        $this->search_tag = Tools::getValue('tag');

        $this->context->smarty->assign(
            array(
                'search_string' => $this->search_string,
                'search_tag'    => $this->search_tag,
            )
        );
    }

    /**
     * Performs the search
     */
    public function initContent()
    {
        parent::initContent();

        // suzy: 2018-10-30 加入自訂頁面搜尋
        if (Configuration::get('SIMPLICITY_SEARCH_CMS')) {
            if ($this->ajax) {
                ob_end_clean();
                header('Content-Type: application/json');
                $this->ajaxDie();
            } else {
                $queryString = Tools::replaceAccentedChars(urldecode($this->search_string));
                $result = Search::findCms(
                    $this->context->language->id,
                    $queryString,
                    false, // ajax, what's the link?
                    null
                );

                $listing = $result['result'];
                if (count($listing) > 0) {
                    foreach ($listing as &$cms) {
                        $cms['link'] = $this->context->link->getCMSLink($cms['id_cms'], $cms['link_rewrite']);
                    }
                }
                $this->context->smarty->assign([
                    'label' => $this->getListingLabel(),
                    'listing' => $listing,
                ]);
                $this->setTemplate('catalog/listing/search-cms-list.tpl');
            }
        } else {
            $this->doProductSearch('catalog/listing/search', array('entity' => 'search'));
        }

    }

    protected function getProductSearchQuery()
    {
        $query = new ProductSearchQuery();
        $query
            ->setSortOrder(new SortOrder('product', 'position', 'desc'))
            ->setSearchString($this->search_string)
            ->setSearchTag($this->search_tag);

        return $query;
    }

    protected function getDefaultProductSearchProvider()
    {
        return new SearchProductSearchProvider(
            $this->getTranslator()
        );
    }

    public function getListingLabel()
    {
        return $this->getTranslator()->trans('Search results', array(), 'Shop.Theme.Catalog');
    }
}
