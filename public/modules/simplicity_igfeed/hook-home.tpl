{if isset($feed_data) && $feed_data|count > 0}
    <section class="simplicity-igfeed">
        <div class="row">
            {foreach $feed_data as $media}
                <div class="col-xs-4 media">
                    <a href="{$media.link}" target="_blank" rel="nofollow">
                        <div class="wrapper">
                            <img src="{$media.image}" alt="{$media.caption|escape:'html':'UTF-8'}" />
                            <div class="overlay">
                                <div class="social-count">
                                    <span class="social1"><i class="material-icons">favorite</i> {$media.likes}</span>
                                    <span class="social2"><i class="material-icons">chat_bubble</i> {$media.comments}</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            {/foreach}
        </div>
    </section>

{/if}