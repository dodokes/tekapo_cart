<?php
  if (!defined('_PS_VERSION_'))
    exit;

  function upgrade_module_1_5_0($object)
  {
    //your code here, for example changes to the DB...

    Db::getInstance()->execute('
		    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'smilepay_csv_table` (
			 `id_smilepay_csv`  int(10) unsigned NOT NULL auto_increment,
			 `id_order` int(10) unsigned NOT NULL,
             `smse_id` TEXT NOT NULL ,
             `barcode1` VARCHAR(50) NOT NULL ,
             `barcode2` VARCHAR(50) NOT NULL ,
             `barcode3` VARCHAR(50) NOT NULL ,
             `dateline` VARCHAR(255) NOT NULL ,
             `amount` decimal(20,6) NOT NULL ,
			 `date_upd` datetime NOT NULL,
			 PRIMARY KEY(`id_smilepay_csv`)
		) ENGINE='._MYSQL_ENGINE_.' default CHARSET=utf8');

        $smilepay_csv_obj= new Smilepay_csv();
        if(!$smilepay_csv_obj->registerHook('displayAdminOrderTabOrder') || !$smilepay_csv_obj->registerHook('displayAdminOrderContentOrder')
            ||!$smilepay_csv_obj->registerHook('displayOrderDetail'))
            return false;
            
    return true; //if there were no errors
  }
?>