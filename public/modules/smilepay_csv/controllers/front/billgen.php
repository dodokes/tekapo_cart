<?php

include_once(dirname(__FILE__) . "\\..\\..\\smilepay_csv.php");

class Smilepay_csvBillgenModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {
        $this->display_column_left = false;
        //$this->display_column_right = FALSE;
        parent::initContent();

        $cart = $this->context->cart;
        $smilepaycsv_obj = new Smilepay_csv();

        if (!isset($_POST['id_order']) || empty($_POST['id_order'])) {
            $this->context->smarty->assign(array(
                'Status' => '-1',
            ));
            $this->setTemplate('module:smilepay_csv/views/templates/hook/payment_return.tpl');
            exit;
        }
        $order_id = $_POST['id_order'];
        $rq = Db::getInstance()->executeS('select * from `' . _DB_PREFIX_ . 'smilepay_csv_table` where id_order=' . $order_id);

        if (isset($rq) && !empty($rq)) {
            $order = new Order($order_id);
            $shopurl = new ShopUrl($order->id_shop);
            $this->context->smarty->assign(array(
                'Status' => '1',
                'barcode1' => $rq[0]['barcode1'],
                'barcode2' => $rq[0]['barcode2'],
                'barcode3' => $rq[0]['barcode3'],
                'dead_date' => $rq[0]['dateline'],
                'SmilePayNO' => $rq[0]['smse_id'],
                'total' => intval(round($rq[0]['amount'])),
                'data_id' => $order_id,
                'this_path' => $smilepaycsv_obj->getPathUri(),
                'shop_url' => $shopurl->getURL(),
                'order_ref_id' => $order->reference,
                'shop_phone' => $smilepaycsv_obj->Storephone,
            ));

            $this->setTemplate('module:smilepay_csv/views/templates/hook/payment_return.tpl');

        } else {
            $this->context->smarty->assign(array(
                'Status' => '-1',
            ));
            $this->setTemplate('module:smilepay_csv/views/templates/hook/payment_return.tpl');
        }

    }
}

?>

