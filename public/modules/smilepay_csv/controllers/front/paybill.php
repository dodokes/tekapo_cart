<?php

class Smilepay_csvPaybillModuleFrontController extends ModuleFrontController
{
    public $display_column_left = false;
    public $display_column_right = false;

    public function postProcess()
    {

        $cart = $this->context->cart;

        if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || !$this->module->active) {
            Tools::redirect('index.php?controller=order&step=1');
        }

        // Check that this payment option is still available in case the customer changed his address just before the end of the checkout process
        $authorized = false;
        foreach (Module::getPaymentModules() as $module) {
            if ($module['name'] == 'smilepay_csv') {
                $authorized = true;
                break;
            }
        }

        if (!$authorized) {
            die($this->module->l('This payment method is not available.', 'validation'));
        }


        //parent::initContent();

        $customer = new Customer($cart->id_customer);
        if (!Validate::isLoadedObject($customer)) {
            Tools::redirect('index.php?controller=order&step=1');
        }

        // 接收
        $od_sob = $_REQUEST['Od_sob'];
        $data_id = $_REQUEST['Data_id'];
        $amount = trim($_REQUEST['Amount']);

        //CSV CODE
        $Payment1 = $_REQUEST['Payment1'];
        $Payment2 = $_REQUEST['Payment2'];
        $Payment3 = $_REQUEST['Payment3'];
        $Deadline_date = $_REQUEST['Deadline_date'];
        $Deadline_time = $_REQUEST['Deadline_time'];
        //$account_roturl=Tools::getHttpHost(true).__PS_BASE_URI__.'index.php?fc=module&module=smilepay_csv&controller=paybill&id_lang='.$_GET['id_lang'].'&id_cart='.(int)$id_cart.'&id_module='.(int)$id_module.'&id_order='.$id_order.'&key='.$key;

        $order_hist = new Order($data_id);
        if (!Validate::isLoadedObject($order_hist)) {
            die($this->module->l('This payment order_hist is not available.', 'validation'));
        }


        //var_dump($this);
        $this->context->smarty->assign(array(
            'status' => 'ok',
            'od_sob' => $od_sob,
            'data_id' => $data_id,
            'barcode1' => $Payment1,
            'barcode2' => $Payment2,
            'barcode3' => $Payment3,
            'dead_date' => $Deadline_date . '-' . $Deadline_time,
            'total' => $amount,
            'shop_url' => Tools::getHttpHost(true) . __PS_BASE_URI__,
            'this_path' => $this->module->getPathUri()
        ));

        $this->setTemplate('payment_paybill.tpl');

    }

}
