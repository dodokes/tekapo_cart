{capture name=path}{l s='Smilepay csv' mod='smilepay_csv'}{/capture}



{if $status =='ok'}
<h2>{l s='SmilePay csv payment detail' mod='smilepay_csv'}</h2>

          <p>
<table class="std">
   </tr>
  <tr>
    <td colspan="2"><div align="center"><strong><font size="5"><img src="{$this_path}smilepay_csv.jpg" alt="{l s='SmilePay csv' mod='smilepay_csv'}" width="86" height="49" />{l s='shop bill model' mod='smilepay_csv'}</font></strong></div></td>
    </tr>
  	  <tr>
    <td>只要持單到全國的任何一家 『7-11』『全家』『萊爾富』『OK』便利商店繳費即可!</td>
	<td><strong>(第一聯: 客戶收執聯)</strong></td>
	</tr>
  <tr>
    <td colspan="2"><table width="100%%"  class="std">
      <tr>
        <td>消費項目</td>
        <td>流水編號</td>
        <td>繳費金額</td>
        <td>代收店舖收訖章</td>
        </tr>
      <tr>
        <td>{$od_sob}</td>
        <td rowspan="3">{$barcode2}</td>
        <td rowspan="3">{$total}</td>
        <td rowspan="4">&nbsp;</td>
        </tr>
      <tr>
        <td>店舖訂單編號</td>
        </tr>
	 <tr>
        <td>{$data_id}</td>
        </tr>
		      <tr>
             <td colspan="3">{l s='shop url： %s.' sprintf=$shop_url mod='smilepay_csv'}  {l s='services tel：000-0000-00.' mod='smilepay_csv'}</td>             
        </tr>
    </table></td>
    </tr>
  <tr>
    <td height="24" colspan="2"><hr></td>
    </tr>
    <tr>
    <td></td>
	<td><strong>(第二聯: 店舖收執聯)</strong></td>
	</tr>
  <tr>
    <td colspan="2"><table width="100%%" class="std">
      <tr>
        <td>消費項目</td>
        <td>流水編號</td>
        <td>繳費金額</td>
        <td>代收店舖收訖章</td>
      </tr>
      <tr>
        <td>{$od_sob}</td>
        <td rowspan="3">{$barcode2}</td>
        <td rowspan="3">{$total}</td>
        <td rowspan="4">&nbsp;</td>
        </tr>
      <tr>
        <td>店舖訂單編號</td>
      </tr>
      <tr>
        <td>{$data_id}</td>
      </tr>
      <tr>
             <td colspan="3">{l s='shop url： %s.' sprintf=$shop_url mod='smilepay_csv'}  {l s='services tel：000-0000-00.' mod='smilepay_csv'}</td>   
             
      </tr>
    </table></td>
    </tr>
  <tr>
    <td colspan="2"><hr></td>
    </tr>
    <tr><td colspan="2">
    <table class="std">
    <td rowspan="3"><p>繳費注意事項： </p>
      <p>1. 本繳費單請盡量以雷射印表機列印 </p>
    <p>2.	若店鋪無法讀取條碼，煩請消費者以其他方式另行繳費</p>
    </td>
    <td>第一段條碼：<IMG SRC="{$this_path}barcode.php?&barcode={$barcode1}&width=350&height=50"></td>
  </tr>
    <tr>
      <td>第二段條碼：<IMG SRC="{$this_path}barcode.php?&barcode={$barcode2}&width=350&height=50"></td>
  </tr>
    <tr>
      <td>第三段條碼：<IMG SRC="{$this_path}barcode.php?&barcode={$barcode3}&width=350&height=50"></td>
  </tr>
  <tr>
    <td >	<strong>繳款截止期限：{$dead_date}</strong></td>
	<td><input type="submit" name="printbtn" id="printbtn" onclick="window.print()" value="列印本頁" />
  	</td>	
  </tr>
  </table> 
  </td>
   </tr>
</table>    
</p>
{/if}        