
<h3 class="card-title h3">付款資訊</h3>

{if $Status == '1'}

  <p>只要持單到全國的任何一家 『7-11』『全家』『萊爾富』『OK』便利商店繳費即可！</p>

  <br><br>


  <div id="payment_info">

    <table border="1">

      <tr>
        <td colspan="2" style="border-style: hidden;"><div align="center"><strong><font size="5">{l s='shop bill model' mod='smilepay_csv'}</font></strong></div></td>
      </tr>
      <tr>
        <td style="border-style: hidden;">&nbsp;</td>
        <td style="border-style: hidden;"><strong>(第一聯: 客戶收執聯)</strong></td>
      </tr>
      <tr>
        <td colspan="2"><table width="100%"  class="std" border="1">
            <tr>
              <td>消費項目</td>
              <td>流水編號</td>
              <td>繳費金額</td>
              <td>代收店舖收訖章</td>
            </tr>
            <tr height ="40">
              <td>{$data_id}</td>
              <td rowspan="3" style="vertical-align: middle;">{$barcode2}</td>
              <td rowspan="3">{if $total}NT${/if}{$total}</td>
              <td rowspan="4">&nbsp;</td>
            </tr>
            <tr>
              <td>店舖訂單編號</td>
            </tr>
            <tr>
              <td  height ="40">{$order_ref_id}</td>
            </tr>
            <tr>
              <td colspan="3">{l s='shop url：' mod='smilepay_csv'}{$shop_url}  {if $shop_phone}{l s='services tel' mod='smilepay_csv'}{$shop_phone}{/if}</td>
            </tr>
          </table></td>
      </tr>

      <tr>
        <td height="24" colspan="2" style="border-style: hidden;"><hr style="border-style: dotted"></td>
      </tr>

      <tr>
        <td style="border-style: hidden;"></td>
        <td style="border-style: hidden;"><strong>(第二聯: 店舖收執聯)</strong></td>
      </tr>
      <tr>
        <td colspan="2"><table width="100%" class="std" border="1">
            <tr>
              <td>消費項目</td>
              <td>流水編號</td>
              <td>繳費金額</td>
              <td>代收店舖收訖章</td>
            </tr>
            <tr>
              <td  height ="40">{$data_id}</td>
              <td rowspan="3" style="vertical-align: middle;">{$barcode2}</td>
              <td rowspan="3">{if $total}NT${/if}{$total}</td>
              <td rowspan="4">&nbsp;</td>
            </tr>
            <tr>
              <td>店舖訂單編號</td>
            </tr>
            <tr>
              <td  height ="40">{$order_ref_id}</td>
            </tr>
            <tr>
              <td colspan="3">{l s='shop url：' mod='smilepay_csv'} {$shop_url} {if $shop_phone}{l s='services tel' mod='smilepay_csv'}{$shop_phone}{/if}</td>
            </tr>
          </table></td>
      </tr>

      <tr>
        <td height="24" colspan="2" style="border-style: hidden;"><hr style="border-style: dotted"></td>
      </tr>

      <tr><td colspan="2">
          <table class="std" >
            <td rowspan="3"><p>繳費注意事項： </p>
              <p>1. 本繳費單請盡量以雷射印表機列印 </p>
              <p>2.	若店鋪無法讀取條碼，煩請消費者以其他方式另行繳費</p>
            </td>
            <td >第一段條碼：<IMG style="vertical-align: middle;" SRC="{$this_path}barcode.php?&barcode={$barcode1}&width=350&height=50"></td>
            </tr>
            <tr>
              <td >第二段條碼：<IMG style="vertical-align: middle;" SRC="{$this_path}barcode.php?&barcode={$barcode2}&width=350&height=50"></td>
            </tr>
            <tr>
              <td >第三段條碼：<IMG style="vertical-align: middle;" SRC="{$this_path}barcode.php?&barcode={$barcode3}&width=350&height=50"></td>
            </tr>
            <tr>
              <td >	<strong>繳款截止期限：{$dead_date}</strong></td>
              <td><input style="border-width: 1.5px;" type="submit" name="printbtn" id="printbtn" onclick="printPaymentinfo()" value="列印本帳單" />
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

  </div>

  <script type="text/javascript">
      function printPaymentinfo() {
          var printwindow=window.open('','printwindow','location=0,status=0,toolbar=0');
          var innerscript ='<script type="text/javascript">function print_process(){ print();close(); }</scr'+'ipt>';


          printwindow.document.open();
          printwindow.document.write('<body onload=\'print_process();\'>');
          printwindow.document.write(document.getElementById('payment_info').innerHTML);

          printwindow.document.write(innerscript +'</body>');
          printwindow.document.close();
      }
  </script>

  <br><br>

{else}
  <p>{$Msg}</p>
{/if}

