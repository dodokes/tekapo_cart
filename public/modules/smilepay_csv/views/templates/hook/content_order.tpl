<div class="tab-pane" id="smilepay_csv_order_info">


    <h4 class="visible-print">{l s='Test'} <span class="badge">{$smilepay_csv_data_num}</span></h4>
    
    {if $smilepay_csv_Data}
    <table style="border: 1px solid black;border-collapse: collapse;">
        <tr>
            <td style="width: 200px;border: 1px solid black;text-align: center;">SmilePay 追蹤碼</td>
            <td style="width: 200px;border: 1px solid black;text-align: center;">繳費單</td>
        </tr>
        {foreach from=$smilepay_csv_Data  item=data}
         <tr>

            <td style="width: 200px;text-align: center;">{$data['smse_id']}</td>
            <form action="{$bill_gen_controller_url}" method="post" target="_blank">
            <input type="hidden" value="{$order_id}" name="id_order"> 
            <td style="width: 200px;text-align: center;"><input type="submit" value="產生繳費單" /></td>
             </form>
        </tr>
        {/foreach}
         
      
    </table>

    {/if}
</div>