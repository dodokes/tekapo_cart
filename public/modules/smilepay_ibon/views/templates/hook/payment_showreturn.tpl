{* 不確定是幹麻的 *}
<script language="JavaScript">
    function HideApiForm(){
        $("#apiform").hide()	;
        $("#apiform").submit()	;
        $("#spmsg").show();
        $('#od_sob').val('3');

    }
</script>

<h3 class="card-title h3">付款資訊</h3>

<dl>
    <dt>{l s='ibon statue' mod='smilepay_ibon'}：</dt>
    <dd>{$Status}</dd>
    <dt>{l s='ibon code' mod='smilepay_ibon'}：</dt>
    <dd>{$IbonNo}</dd>
    <dt>{l s='ibon PayEndDate' mod='smilepay_ibon'}：</dt>
    <dd>{$PayEndDate}</dd>
    <dt>{l s='ibon Amount' mod='smilepay_ibon'}：</dt>
    <dd>{if $Amount}NT${/if}{$Amount}</dd>
</dl>