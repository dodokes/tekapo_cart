<script data-keepinline>

    var gtmId = '{$gtm_id}';
    var gtmNoBoTrack = {$gtmNoBoTrack};

    var guaSettings = {$guaSettings|json_encode nofilter};
    var adwordsSettings = {$adwordsSettings|json_encode nofilter};
    var facebookSettings = {$facebookSettings|json_encode nofilter};
    var shopSettings = {$shopSettings|json_encode nofilter};

    var dataLayer = window.dataLayer || [];
    var initDataLayer = new Object();

    var gtmDoNotTrack = {$gtmDoNotTrack};
    gtmDoNotTrack = gtmDoNotTrack && (window.doNotTrack == "1" || navigator.doNotTrack == "yes" || navigator.doNotTrack == "1" || navigator.msDoNotTrack == "1");


    if (!gtmNoBoTrack) {

        if (guaSettings.trackingId) {
            initDataLayer.gua = {
                'trackingId': guaSettings.trackingId,
                'cookieDomain': 'auto',
                'allowLinker':  true,
                'linkAttribution': true,
                'anonymizeIp': guaSettings.anonymizeIp,
                'siteSpeedSampleRate': guaSettings.siteSpeedSampleRate,

                'dimensionProdId': guaSettings.ecommProdId,
                'dimensionPageType': guaSettings.ecommPageType,
                'dimensionTotalValue': guaSettings.ecommTotalValue,
                'dimensionCategory' : guaSettings.ecommCategory
            };

            if (guaSettings.unifyUserId) {
                initDataLayer.gua.userId = guaSettings.id_customer;
            }

        }

        if (adwordsSettings.conversionId) {
            initDataLayer.adWords = {
                'conversionId' : adwordsSettings.conversionId,
                'conversionLabel' : adwordsSettings.conversionLabel
            };
        }

        if (facebookSettings.trackingId) {
            initDataLayer.facebook = {
                'trackingId': facebookSettings.trackingId
            };
        }

        // init common values
        initDataLayer.common = {
            'currency' : shopSettings.currency,
            'langCode' : shopSettings.lang,
            'referrer' : document.referrer,
            'userAgent' : navigator.userAgent,
            'navigatorLang' : navigator.language,
            'doNotTrack' : gtmDoNotTrack
        };

        dataLayer.push(initDataLayer);
    }

    if (!gtmNoBoTrack) {
        {literal}
        (function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({'gtm.start': new Date().getTime(), event: 'gtm.js'});
            var f = d.getElementsByTagName(s)[0];
            var j = d.createElement(s), dl = l !== 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', gtmId);
        {/literal}
    }
</script>