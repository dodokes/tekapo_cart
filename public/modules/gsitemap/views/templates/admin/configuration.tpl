{if isset($smarty.get.validation)}
	<div class="alert alert-success">
		Sitemap 網址已完成建立。若還未在 Google Webmaster 帳戶設定網址，請記得前往設定喔。
	</div>
	{*<div class="conf confirm" style="width: 710px; margin: 0 auto;">
		{l s='Your Sitemaps were successfully created. Please do not forget to setup the URL' mod='gsitemap'} <a href="{$gsitemap_store_url|escape:'htmlall':'UTF-8'}{$shop->id|intval}_index_sitemap.xml" target="_blank"><span style="text-decoration: underline;">{$gsitemap_store_url|escape:'htmlall':'UTF-8'}{$shop->id|intval}_index_sitemap.xml</a></span> {l s='in your Google Webmaster account.' mod='gsitemap'}
	</div>*}
{/if}

{if isset($google_maps_error)}
	<div class="error" style="width: 710px; margin: 0 auto;">
		{$google_maps_error|escape:'htmlall':'UTF-8'}<br />
	</div>
{/if}

{if isset($gsitemap_refresh_page)}
	<div class="panel clearfix">
		<h3>網址設定</h3>
		<p>{$gsitemap_number|intval} {l s='Sitemaps were already created.' mod='gsitemap'}</p>
		<form action="{$gsitemap_refresh_page|escape:'htmlall':'UTF-8'}" method="post" id="gsitemap_generate_sitmap">
			<img src="../img/loader.gif" alt=""/>
			<input type="submit" class="button" value="{l s='Continue' mod='gsitemap'}" style="display: none;"/>
		</form>
	</div>

{else}

	<div class="panel clearfix">
		{if $gsitemap_links}
			<h3>網址設定</h3>
            {l s='Please set up the following Sitemap URL in your Google Webmaster account:' mod='gsitemap'}
			<a href="{$gsitemap_store_url|escape:'htmlall':'UTF-8'}{$shop->id|intval}_index_sitemap.xml" target="_blank"><span style="color: blue;">{$gsitemap_store_url|escape:'htmlall':'UTF-8'}{$shop->id|intval}_index_sitemap.xml</span></a> （只需設定一次）<br/>

			{*l s='This URL is the master Sitemaps file. It refers to the following sub-sitemap files:' mod='gsitemap'*}

			{*<div style="max-height: 220px; overflow: auto;">
				<ul>
					{foreach from=$gsitemap_links item=gsitemap_link}
					<li><a target="_blank" style="color: blue;" href="{$gsitemap_store_url|escape:'htmlall':'UTF-8'}{$gsitemap_link.link|escape:'htmlall':'UTF-8'}">{$gsitemap_link.link|escape:'htmlall':'UTF-8'}</a></li>
					{/foreach}
				</ul>
			</div>*}
			<p>{l s='Your last update was made on this date:' mod='gsitemap'} {$gsitemap_last_export|escape:'htmlall':'UTF-8'}</p>
		{/if}

		{if ($gsitemap_customer_limit.max_exec_time < 30 && $gsitemap_customer_limit.max_exec_time > 0) || ($gsitemap_customer_limit.memory_limit < 128 && $gsitemap_customer_limit.memory_limit > 0)}
		<div class="warn" style="width: 700px; margin: 0 auto;">
			<p>{l s='For a better use of the module, please make sure that you have' mod='gsitemap'}<br/>
			<ul>
				{if $gsitemap_customer_limit.memory_limit < 128 && $gsitemap_customer_limit.memory_limit > 0}
				<li>{l s='a minimum memory_limit value of 128 MB.' mod='gsitemap'}</li>
				{/if}
				{if $gsitemap_customer_limit.max_exec_time < 30 && $gsitemap_customer_limit.max_exec_time > 0}
				<li>{l s='a minimum max_execution_time value of 30 seconds.' mod='gsitemap'}</li>
				{/if}
			</ul>
			{l s='You can edit these limits in your php.ini file. For more details, please contact your hosting provider.' mod='gsitemap'}
			</p>
		</div>
		{/if}

	</div>
	<div class="panel clearfix">
		<form action="{$gsitemap_form|escape:'htmlall':'UTF-8'}" method="post">
			<h3>連結設定{*l s='Configure your Sitemap' mod='gsitemap'*}</h3>
			{*<p>{l s='Several Sitemaps files will be generated depending on how your server is configured and on the number of activated products in your catalog.' mod='gsitemap'}<br/></p>*}
			<div class="margin-form">
				<label for="gsitemap_frequency" style="width: 235px;">請選擇更新商店的頻率：{*l s='How often do you update your store?' mod='gsitemap'*}
					<select name="gsitemap_frequency">
						<option{if $gsitemap_frequency == 'always'} selected="selected"{/if} value='always'>{l s='always' mod='gsitemap'}</option>
						<option{if $gsitemap_frequency == 'hourly'} selected="selected"{/if} value='hourly'>{l s='hourly' mod='gsitemap'}</option>
						<option{if $gsitemap_frequency == 'daily'} selected="selected"{/if} value='daily'>{l s='daily' mod='gsitemap'}</option>
						<option{if $gsitemap_frequency == 'weekly' || $gsitemap_frequency == ''} selected="selected"{/if} value='weekly'>{l s='weekly' mod='gsitemap'}</option>
						<option{if $gsitemap_frequency == 'monthly'} selected="selected"{/if} value='monthly'>{l s='monthly' mod='gsitemap'}</option>
						<option{if $gsitemap_frequency == 'yearly'} selected="selected"{/if} value='yearly'>{l s='yearly' mod='gsitemap'}</option>
						<option{if $gsitemap_frequency == 'never'} selected="selected"{/if} value='never'>{l s='never' mod='gsitemap'}</option>
					</select>
				</label>
			</div>

			{*<label for="ggsitemap_check_image_file" style="width: 526px;">
				{l s='Check this box if you wish to check the presence of the image files on the server' mod='gsitemap'}
				<input type="checkbox" name="gsitemap_check_image_file" value="1" {if $gsitemap_check_image_file}checked{/if}>
			</label>*}

			{*<label for="ggsitemap_check_all" style="width: 526px;">
				<span>{l s='check all' mod='gsitemap'}</span>
				<input type="checkbox" name="gsitemap_check_all" value="1" class="check">
			</label>*}

			<br class="clear" />
			<p for="gsitemap_meta">
				Google Sitemap 功能自動為您產生：商品、商品分類、自訂頁面的連結，另外，下方五個頁面為選填。<br>
				請選擇您<strong>不希望</strong>在 Sitemap 檔案出現的連結：
				{*l s='Indicate the pages that you do not want to include in your Sitemaps file:' mod='gsitemap'*}
			</p>
			<ul>
            {foreach from=$store_metas key=key item=meta}
				<li style="float: left; width: 200px; margin: 1px;">
					<input type="checkbox" class="gsitemap_metas" name="gsitemap_meta[]"{if in_array($meta['id_meta'], $gsitemap_disable_metas)} checked="checked"{/if} value="{$meta['id_meta']|intval}" /> {$meta['title']|escape:'htmlall':'UTF-8'}
				</li>
            {/foreach}
			</ul>
			<br/>
			<div class="margin-form" style="clear: both;">
				<input type="submit" style="margin: 20px;" class="button" name="SubmitGsitemap" onclick="$('#gsitemap_loader').show();" value="{l s='Generate Sitemap' mod='gsitemap'}" />{l s='This can take several minutes' mod='gsitemap'}
			</div>
			<p id="gsitemap_loader" style="text-align: center; display: none;"><img src="../img/loader.gif" alt=""/></p>
		</form>

		<p>系統每天凌晨 3:00 自動更新，您也可以按上方「產生 Sitemap」立即更新。</p>
	</div>
{/if}
<script type="text/javascript">
$(document).ready(function() {

	if ($('.gsitemap_metas:checked').length == $('.gsitemap_metas').length)
		$('.check').parent('label').children('span').html("{l s='uncheck all' mod='gsitemap'}");


	$('.check').toggle(function() {
		$('.gsitemap_metas').attr('checked', 'checked');
		$(this).parent('label').children('span').html("{l s='uncheck all' mod='gsitemap'}");
	}, function() {
		$('.gsitemap_metas').removeAttr('checked');
		$(this).parent('label').children('span').html("{l s='check all' mod='gsitemap'}");
	});
});
</script>
