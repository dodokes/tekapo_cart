<div class="logo-wrapper hidden-sm-down" id="_desktop_logo">
    <a href="{$urls.base_url}">
        <img class="logo img-responsive" src="{$shop.logo}" alt="{$shop.name}">
    </a>
</div>