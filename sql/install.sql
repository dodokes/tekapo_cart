-- MySQL dump 10.13  Distrib 5.7.18-16, for osx10.12 (x86_64)
--
-- Host: localhost    Database: tekapocart
-- ------------------------------------------------------
-- Server version	5.7.18-16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*!50717 SELECT COUNT(*) INTO @rocksdb_has_p_s_session_variables FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'performance_schema' AND TABLE_NAME = 'session_variables' */;
/*!50717 SET @rocksdb_get_is_supported = IF (@rocksdb_has_p_s_session_variables, 'SELECT COUNT(*) INTO @rocksdb_is_supported FROM performance_schema.session_variables WHERE VARIABLE_NAME=\'rocksdb_bulk_load\'', 'SELECT 0') */;
/*!50717 PREPARE s FROM @rocksdb_get_is_supported */;
/*!50717 EXECUTE s */;
/*!50717 DEALLOCATE PREPARE s */;
/*!50717 SET @rocksdb_enable_bulk_load = IF (@rocksdb_is_supported, 'SET SESSION rocksdb_bulk_load = 1', 'SET @rocksdb_dummy_bulk_load = 0') */;
/*!50717 PREPARE s FROM @rocksdb_enable_bulk_load */;
/*!50717 EXECUTE s */;
/*!50717 DEALLOCATE PREPARE s */;

--
-- Current Database: `tekapocart`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `tekapocart` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `tekapocart`;

--
-- Table structure for table `ps_access`
--

DROP TABLE IF EXISTS `ps_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_access` (
  `id_profile` int(10) unsigned NOT NULL,
  `id_authorization_role` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_profile`,`id_authorization_role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_access`
--

LOCK TABLES `ps_access` WRITE;
/*!40000 ALTER TABLE `ps_access` DISABLE KEYS */;
INSERT INTO `ps_access` VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9),(1,10),(1,11),(1,12),(1,13),(1,14),(1,15),(1,16),(1,17),(1,18),(1,19),(1,20),(1,21),(1,22),(1,23),(1,24),(1,25),(1,26),(1,27),(1,28),(1,29),(1,30),(1,31),(1,32),(1,33),(1,34),(1,35),(1,36),(1,37),(1,38),(1,39),(1,40),(1,41),(1,42),(1,43),(1,44),(1,45),(1,46),(1,47),(1,48),(1,49),(1,50),(1,51),(1,52),(1,53),(1,54),(1,55),(1,56),(1,57),(1,58),(1,59),(1,60),(1,61),(1,62),(1,63),(1,64),(1,65),(1,66),(1,67),(1,68),(1,69),(1,70),(1,71),(1,72),(1,73),(1,74),(1,75),(1,76),(1,77),(1,78),(1,79),(1,80),(1,81),(1,82),(1,83),(1,84),(1,85),(1,86),(1,87),(1,88),(1,89),(1,90),(1,91),(1,92),(1,93),(1,94),(1,95),(1,96),(1,97),(1,98),(1,99),(1,100),(1,101),(1,102),(1,103),(1,104),(1,105),(1,106),(1,107),(1,108),(1,109),(1,110),(1,111),(1,112),(1,113),(1,114),(1,115),(1,116),(1,117),(1,118),(1,119),(1,120),(1,121),(1,122),(1,123),(1,124),(1,125),(1,126),(1,127),(1,128),(1,129),(1,130),(1,131),(1,132),(1,133),(1,134),(1,135),(1,136),(1,137),(1,138),(1,139),(1,140),(1,141),(1,142),(1,143),(1,144),(1,145),(1,146),(1,147),(1,148),(1,149),(1,150),(1,151),(1,152),(1,153),(1,154),(1,155),(1,156),(1,157),(1,158),(1,159),(1,160),(1,161),(1,162),(1,163),(1,164),(1,165),(1,166),(1,167),(1,168),(1,169),(1,170),(1,171),(1,172),(1,173),(1,174),(1,175),(1,176),(1,177),(1,178),(1,179),(1,180),(1,181),(1,182),(1,183),(1,184),(1,185),(1,186),(1,187),(1,188),(1,189),(1,190),(1,191),(1,192),(1,193),(1,194),(1,195),(1,196),(1,197),(1,198),(1,199),(1,200),(1,201),(1,202),(1,203),(1,204),(1,205),(1,206),(1,207),(1,208),(1,209),(1,210),(1,211),(1,212),(1,213),(1,214),(1,215),(1,216),(1,217),(1,218),(1,219),(1,220),(1,221),(1,222),(1,223),(1,224),(1,225),(1,226),(1,227),(1,228),(1,229),(1,230),(1,231),(1,232),(1,233),(1,234),(1,235),(1,236),(1,237),(1,238),(1,239),(1,240),(1,241),(1,242),(1,243),(1,244),(1,245),(1,246),(1,247),(1,248),(1,249),(1,250),(1,251),(1,252),(1,253),(1,254),(1,255),(1,256),(1,257),(1,258),(1,259),(1,260),(1,261),(1,262),(1,263),(1,264),(1,265),(1,266),(1,267),(1,268),(1,269),(1,270),(1,271),(1,272),(1,273),(1,274),(1,275),(1,276),(1,277),(1,278),(1,279),(1,280),(1,281),(1,282),(1,283),(1,284),(1,285),(1,286),(1,287),(1,288),(1,289),(1,290),(1,291),(1,292),(1,293),(1,294),(1,295),(1,296),(1,297),(1,298),(1,299),(1,300),(1,301),(1,302),(1,303),(1,304),(1,305),(1,306),(1,307),(1,308),(1,309),(1,310),(1,311),(1,312),(1,313),(1,314),(1,315),(1,316),(1,317),(1,318),(1,319),(1,320),(1,321),(1,322),(1,323),(1,324),(1,325),(1,326),(1,327),(1,328),(1,329),(1,330),(1,331),(1,332),(1,333),(1,334),(1,335),(1,336),(1,337),(1,338),(1,339),(1,340),(1,341),(1,342),(1,343),(1,344),(1,345),(1,346),(1,347),(1,348),(1,349),(1,350),(1,351),(1,352),(1,353),(1,354),(1,355),(1,356),(1,357),(1,358),(1,359),(1,360),(1,361),(1,362),(1,363),(1,364),(1,365),(1,366),(1,367),(1,368),(1,369),(1,370),(1,371),(1,372),(1,373),(1,374),(1,375),(1,376),(1,377),(1,378),(1,379),(1,380),(1,381),(1,382),(1,383),(1,384),(1,385),(1,386),(1,387),(1,388),(1,389),(1,390),(1,391),(1,392),(1,393),(1,394),(1,395),(1,396),(1,397),(1,398),(1,399),(1,400),(1,401),(1,402),(1,403),(1,404),(1,405),(1,406),(1,407),(1,408),(1,409),(1,410),(1,411),(1,412),(1,413),(1,414),(1,415),(1,416),(1,417),(1,418),(1,419),(1,420),(1,421),(1,422),(1,423),(1,424),(1,425),(1,426),(1,427),(1,428),(1,429),(1,430),(1,431),(1,432),(1,433),(1,434),(1,435),(1,436),(1,437),(1,438),(1,439),(1,440),(1,441),(1,442),(1,443),(1,444),(1,445),(1,446),(1,447),(1,448),(1,449),(1,450),(1,451),(1,452),(1,465),(1,466),(1,467),(1,468),(1,569),(1,570),(1,571),(1,572),(1,573),(1,574),(1,575),(1,576),(1,577),(1,578),(1,579),(1,580),(1,681),(1,682),(1,683),(1,684),(1,685),(1,686),(1,687),(1,688),(1,745),(1,746),(1,747),(1,748),(1,829),(1,830),(1,831),(1,832),(1,849),(1,850),(1,851),(1,852),(1,857),(1,858),(1,859),(1,860),(1,881),(1,882),(1,883),(1,884),(1,897),(2,234),(2,235),(2,265),(2,266),(2,267),(2,268),(2,365),(2,366),(2,367),(2,368),(2,381),(2,382),(2,383),(2,384),(2,393),(2,394),(2,395),(2,396),(2,425),(2,426),(2,427),(2,428),(4,0),(4,45),(4,46),(4,47),(4,48),(4,181),(4,182),(4,183),(4,184),(4,201),(4,202),(4,203),(4,204),(4,209),(4,210),(4,211),(4,212),(4,234),(4,235),(4,241),(4,242),(4,243),(4,244),(4,361),(4,362),(4,363),(4,364),(4,393),(4,394),(4,395),(4,396),(4,445),(4,446),(4,447),(4,448);
/*!40000 ALTER TABLE `ps_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_accessory`
--

DROP TABLE IF EXISTS `ps_accessory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_accessory` (
  `id_product_1` int(10) unsigned NOT NULL,
  `id_product_2` int(10) unsigned NOT NULL,
  KEY `accessory_product` (`id_product_1`,`id_product_2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_accessory`
--

LOCK TABLES `ps_accessory` WRITE;
/*!40000 ALTER TABLE `ps_accessory` DISABLE KEYS */;
INSERT INTO `ps_accessory` VALUES (1,2),(1,3),(1,4),(1,11),(1,12);
/*!40000 ALTER TABLE `ps_accessory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_address`
--

DROP TABLE IF EXISTS `ps_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_address` (
  `id_address` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_country` int(10) unsigned NOT NULL,
  `id_state` int(10) unsigned DEFAULT NULL,
  `id_customer` int(10) unsigned NOT NULL DEFAULT '0',
  `id_manufacturer` int(10) unsigned NOT NULL DEFAULT '0',
  `id_supplier` int(10) unsigned NOT NULL DEFAULT '0',
  `id_warehouse` int(10) unsigned NOT NULL DEFAULT '0',
  `alias` varchar(32) NOT NULL,
  `company` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `address1` varchar(128) NOT NULL,
  `address2` varchar(128) DEFAULT NULL,
  `postcode` varchar(12) DEFAULT NULL,
  `city` varchar(64) NOT NULL,
  `other` text,
  `phone` varchar(32) DEFAULT NULL,
  `phone_mobile` varchar(32) DEFAULT NULL,
  `vat_number` varchar(32) DEFAULT NULL,
  `dni` varchar(16) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_address`),
  KEY `address_customer` (`id_customer`),
  KEY `id_country` (`id_country`),
  KEY `id_state` (`id_state`),
  KEY `id_manufacturer` (`id_manufacturer`),
  KEY `id_supplier` (`id_supplier`),
  KEY `id_warehouse` (`id_warehouse`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_address`
--

LOCK TABLES `ps_address` WRITE;
/*!40000 ALTER TABLE `ps_address` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_address_format`
--

DROP TABLE IF EXISTS `ps_address_format`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_address_format` (
  `id_country` int(10) unsigned NOT NULL,
  `format` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_address_format`
--

LOCK TABLES `ps_address_format` WRITE;
/*!40000 ALTER TABLE `ps_address_format` DISABLE KEYS */;
INSERT INTO `ps_address_format` VALUES (1,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(2,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(3,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(4,'firstname lastname\ncompany\naddress1\naddress2\ncity State:name postcode\nCountry:name\nphone'),(5,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(6,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(7,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(8,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(9,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(10,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone'),(11,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone'),(12,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(13,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(14,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(15,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(16,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(17,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\ncity\npostcode\nCountry:name\nphone'),(18,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(19,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(20,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(21,'firstname lastname\ncompany\naddress1 address2\ncity, State:name postcode\nCountry:name\nphone'),(22,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(23,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(24,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\ncity State:iso_code postcode\nCountry:name\nphone'),(25,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(26,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(27,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(28,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(29,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(30,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(31,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(32,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(33,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(34,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(35,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(36,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(37,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(38,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(39,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(40,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(41,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(42,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(43,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(44,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone'),(45,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(46,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(47,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(48,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(49,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(50,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(51,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(52,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(53,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(54,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(55,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(56,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(57,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(58,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(59,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(60,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(61,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(62,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(63,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(64,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(65,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(66,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(67,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(68,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(69,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(70,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(71,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(72,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(73,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(74,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(75,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(76,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(77,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(78,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(79,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(80,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(81,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(82,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(83,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(84,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(85,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(86,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(87,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(88,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(89,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(90,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(91,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(92,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(93,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(94,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(95,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(96,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(97,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(98,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(99,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(100,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(101,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(102,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(103,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(104,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(105,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(106,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(107,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(108,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(109,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(110,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\ncity\npostcode\nState:name\nCountry:name\nphone'),(111,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone'),(112,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(113,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(114,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(115,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(116,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(117,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(118,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(119,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(120,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(121,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(122,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(123,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(124,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(125,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(126,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(127,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(128,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(129,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(130,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(131,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(132,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(133,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(134,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(135,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(136,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(137,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(138,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(139,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(140,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(141,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(142,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(143,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(144,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(145,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nState:name\nCountry:name\nphone'),(146,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(147,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(148,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(149,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(150,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(151,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(152,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(153,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(154,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(155,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(156,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(157,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(158,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(159,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(160,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(161,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(162,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(163,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(164,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(165,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(166,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(167,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(168,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(169,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(170,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(171,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(172,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(173,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(174,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(175,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(176,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(177,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(178,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(179,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(180,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(181,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(182,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(183,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(184,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(185,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(186,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(187,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(188,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(189,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(190,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(191,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(192,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(193,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(194,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(195,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(196,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(197,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(198,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(199,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(200,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(201,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(202,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(203,'lastname firstname\r\nphone_mobile\r\naddress1\r\ncity\r\npostcode\r\nCountry:name'),(204,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(205,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(206,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(207,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(208,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(209,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(210,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(211,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(212,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(213,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(214,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(215,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(216,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(217,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(218,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(219,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(220,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(221,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(222,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(223,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(224,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(225,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(226,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(227,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(228,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(229,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(230,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(231,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(232,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(233,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(234,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(235,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(236,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(237,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(238,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(239,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(240,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(241,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(242,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(243,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone'),(244,'firstname lastname\ncompany\nvat_number\naddress1\naddress2\npostcode city\nCountry:name\nphone');
/*!40000 ALTER TABLE `ps_address_format` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_admin_filter`
--

DROP TABLE IF EXISTS `ps_admin_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_admin_filter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee` int(11) NOT NULL,
  `shop` int(11) NOT NULL,
  `controller` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `filter` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_filter_search_idx` (`employee`,`shop`,`controller`,`action`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_admin_filter`
--

LOCK TABLES `ps_admin_filter` WRITE;
/*!40000 ALTER TABLE `ps_admin_filter` DISABLE KEYS */;
INSERT INTO `ps_admin_filter` VALUES (1,1,1,'ProductController','catalogAction','{\"filter_category\":\"2\",\"filter_column_id_product\":\"\",\"filter_column_name\":\"\",\"filter_column_reference\":\"\",\"filter_column_name_category\":\"\",\"filter_column_price\":\"\",\"filter_column_sav_quantity\":\"\",\"filter_column_active\":\"\",\"last_offset\":\"0\",\"last_limit\":\"300\",\"last_orderBy\":\"id_product\",\"last_sortOrder\":\"asc\"}'),(2,1,2,'ProductController','catalogAction','{\"filter_category\":\"\",\"filter_column_id_product\":\"\",\"filter_column_name\":\"\",\"filter_column_reference\":\"\",\"filter_column_name_category\":\"\",\"filter_column_price\":\"\",\"filter_column_sav_quantity\":\"\",\"filter_column_active\":\"\",\"last_offset\":\"0\",\"last_limit\":\"20\",\"last_orderBy\":\"id_product\",\"last_sortOrder\":\"desc\"}');
/*!40000 ALTER TABLE `ps_admin_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_alias`
--

DROP TABLE IF EXISTS `ps_alias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_alias` (
  `id_alias` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `alias` varchar(255) NOT NULL,
  `search` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_alias`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_alias`
--

LOCK TABLES `ps_alias` WRITE;
/*!40000 ALTER TABLE `ps_alias` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_alias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_attachment`
--

DROP TABLE IF EXISTS `ps_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_attachment` (
  `id_attachment` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `file` varchar(40) NOT NULL,
  `file_name` varchar(128) NOT NULL,
  `file_size` bigint(10) unsigned NOT NULL DEFAULT '0',
  `mime` varchar(128) NOT NULL,
  PRIMARY KEY (`id_attachment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_attachment`
--

LOCK TABLES `ps_attachment` WRITE;
/*!40000 ALTER TABLE `ps_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_attachment_lang`
--

DROP TABLE IF EXISTS `ps_attachment_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_attachment_lang` (
  `id_attachment` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id_attachment`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_attachment_lang`
--

LOCK TABLES `ps_attachment_lang` WRITE;
/*!40000 ALTER TABLE `ps_attachment_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_attachment_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_attribute`
--

DROP TABLE IF EXISTS `ps_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_attribute` (
  `id_attribute` int(11) NOT NULL AUTO_INCREMENT,
  `id_attribute_group` int(11) NOT NULL,
  `color` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id_attribute`),
  KEY `attribute_group` (`id_attribute_group`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_attribute`
--

LOCK TABLES `ps_attribute` WRITE;
/*!40000 ALTER TABLE `ps_attribute` DISABLE KEYS */;
INSERT INTO `ps_attribute` VALUES (1,1,'',0),(2,1,'',1),(3,1,'',2),(4,1,'',3),(5,1,'',4),(6,2,'',0),(7,2,'',1),(8,2,'',2),(9,3,'#2ddcbd',0),(10,3,'#2da4dc',1),(11,3,'#e72121',2),(12,3,'#fae603',3);
/*!40000 ALTER TABLE `ps_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_attribute_group`
--

DROP TABLE IF EXISTS `ps_attribute_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_attribute_group` (
  `id_attribute_group` int(11) NOT NULL AUTO_INCREMENT,
  `is_color_group` tinyint(1) NOT NULL,
  `group_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id_attribute_group`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_attribute_group`
--

LOCK TABLES `ps_attribute_group` WRITE;
/*!40000 ALTER TABLE `ps_attribute_group` DISABLE KEYS */;
INSERT INTO `ps_attribute_group` VALUES (1,0,'radio',0),(2,1,'color',1),(3,1,'color',2);
/*!40000 ALTER TABLE `ps_attribute_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_attribute_group_lang`
--

DROP TABLE IF EXISTS `ps_attribute_group_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_attribute_group_lang` (
  `id_attribute_group` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `public_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_attribute_group`,`id_lang`),
  KEY `IDX_4653726C67A664FB` (`id_attribute_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_attribute_group_lang`
--

LOCK TABLES `ps_attribute_group_lang` WRITE;
/*!40000 ALTER TABLE `ps_attribute_group_lang` DISABLE KEYS */;
INSERT INTO `ps_attribute_group_lang` VALUES (1,1,'尺寸','尺寸'),(1,2,'Size','Size'),(1,3,'尺寸','尺寸'),(1,4,'Size','Size'),(2,1,'紙料','紙料'),(2,2,'Material','Material'),(2,3,'纸料','纸料'),(2,4,'紙料','紙料'),(3,1,'顏色','顏色'),(3,2,'Color','Color'),(3,3,'颜色','颜色'),(3,4,'色','色');
/*!40000 ALTER TABLE `ps_attribute_group_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_attribute_group_shop`
--

DROP TABLE IF EXISTS `ps_attribute_group_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_attribute_group_shop` (
  `id_attribute_group` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  PRIMARY KEY (`id_attribute_group`,`id_shop`),
  KEY `IDX_DB30BAAC67A664FB` (`id_attribute_group`),
  KEY `IDX_DB30BAAC274A50A0` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_attribute_group_shop`
--

LOCK TABLES `ps_attribute_group_shop` WRITE;
/*!40000 ALTER TABLE `ps_attribute_group_shop` DISABLE KEYS */;
INSERT INTO `ps_attribute_group_shop` VALUES (1,1),(2,1),(3,1);
/*!40000 ALTER TABLE `ps_attribute_group_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_attribute_impact`
--

DROP TABLE IF EXISTS `ps_attribute_impact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_attribute_impact` (
  `id_attribute_impact` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_product` int(11) unsigned NOT NULL,
  `id_attribute` int(11) unsigned NOT NULL,
  `weight` decimal(20,6) NOT NULL,
  `price` decimal(17,2) NOT NULL,
  PRIMARY KEY (`id_attribute_impact`),
  UNIQUE KEY `id_product` (`id_product`,`id_attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_attribute_impact`
--

LOCK TABLES `ps_attribute_impact` WRITE;
/*!40000 ALTER TABLE `ps_attribute_impact` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_attribute_impact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_attribute_lang`
--

DROP TABLE IF EXISTS `ps_attribute_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_attribute_lang` (
  `id_attribute` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_attribute`,`id_lang`),
  KEY `IDX_3ABE46A77A4F53DC` (`id_attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_attribute_lang`
--

LOCK TABLES `ps_attribute_lang` WRITE;
/*!40000 ALTER TABLE `ps_attribute_lang` DISABLE KEYS */;
INSERT INTO `ps_attribute_lang` VALUES (1,1,'XS'),(1,2,'XS'),(1,3,'XS'),(1,4,'XS'),(2,1,'S'),(2,2,'S'),(2,3,'S'),(2,4,'S'),(3,1,'M'),(3,2,'M'),(3,3,'M'),(3,4,'M'),(4,1,'L'),(4,2,'L'),(4,3,'L'),(4,4,'L'),(5,1,'XL'),(5,2,'XL'),(5,3,'XL'),(5,4,'XL'),(6,1,'波面剛古紙'),(6,2,'波面剛古紙'),(6,3,'波面剛古紙'),(6,4,'波面剛古紙'),(7,1,'トーメイあらじま'),(7,2,'トーメイあらじま'),(7,3,'トーメイあらじま'),(7,4,'トーメイあらじま'),(8,1,'炫光紙'),(8,2,'炫光紙'),(8,3,'炫光紙'),(8,4,'炫光紙'),(9,1,'湖綠'),(9,2,'湖綠'),(9,3,'湖綠'),(9,4,'湖綠'),(10,1,'海藍'),(10,2,'海藍'),(10,3,'海藍'),(10,4,'海藍'),(11,1,'血紅'),(11,2,'血紅'),(11,3,'血紅'),(11,4,'血紅'),(12,1,'日金'),(12,2,'日金'),(12,3,'日金'),(12,4,'日金');
/*!40000 ALTER TABLE `ps_attribute_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_attribute_shop`
--

DROP TABLE IF EXISTS `ps_attribute_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_attribute_shop` (
  `id_attribute` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  PRIMARY KEY (`id_attribute`,`id_shop`),
  KEY `IDX_A7DD8E677A4F53DC` (`id_attribute`),
  KEY `IDX_A7DD8E67274A50A0` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_attribute_shop`
--

LOCK TABLES `ps_attribute_shop` WRITE;
/*!40000 ALTER TABLE `ps_attribute_shop` DISABLE KEYS */;
INSERT INTO `ps_attribute_shop` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1);
/*!40000 ALTER TABLE `ps_attribute_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_authorization_role`
--

DROP TABLE IF EXISTS `ps_authorization_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_authorization_role` (
  `id_authorization_role` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  PRIMARY KEY (`id_authorization_role`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=905 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_authorization_role`
--

LOCK TABLES `ps_authorization_role` WRITE;
/*!40000 ALTER TABLE `ps_authorization_role` DISABLE KEYS */;
INSERT INTO `ps_authorization_role` VALUES (809,'ROLE_MOD_MODULE_BLOCKREASSURANCE_CREATE'),(812,'ROLE_MOD_MODULE_BLOCKREASSURANCE_DELETE'),(810,'ROLE_MOD_MODULE_BLOCKREASSURANCE_READ'),(811,'ROLE_MOD_MODULE_BLOCKREASSURANCE_UPDATE'),(453,'ROLE_MOD_MODULE_CONTACTFORM_CREATE'),(456,'ROLE_MOD_MODULE_CONTACTFORM_DELETE'),(454,'ROLE_MOD_MODULE_CONTACTFORM_READ'),(455,'ROLE_MOD_MODULE_CONTACTFORM_UPDATE'),(457,'ROLE_MOD_MODULE_DASHACTIVITY_CREATE'),(460,'ROLE_MOD_MODULE_DASHACTIVITY_DELETE'),(458,'ROLE_MOD_MODULE_DASHACTIVITY_READ'),(459,'ROLE_MOD_MODULE_DASHACTIVITY_UPDATE'),(469,'ROLE_MOD_MODULE_DASHGOALS_CREATE'),(472,'ROLE_MOD_MODULE_DASHGOALS_DELETE'),(470,'ROLE_MOD_MODULE_DASHGOALS_READ'),(471,'ROLE_MOD_MODULE_DASHGOALS_UPDATE'),(473,'ROLE_MOD_MODULE_DASHPRODUCTS_CREATE'),(476,'ROLE_MOD_MODULE_DASHPRODUCTS_DELETE'),(474,'ROLE_MOD_MODULE_DASHPRODUCTS_READ'),(475,'ROLE_MOD_MODULE_DASHPRODUCTS_UPDATE'),(461,'ROLE_MOD_MODULE_DASHTRENDS_CREATE'),(464,'ROLE_MOD_MODULE_DASHTRENDS_DELETE'),(462,'ROLE_MOD_MODULE_DASHTRENDS_READ'),(463,'ROLE_MOD_MODULE_DASHTRENDS_UPDATE'),(477,'ROLE_MOD_MODULE_GRAPHNVD3_CREATE'),(480,'ROLE_MOD_MODULE_GRAPHNVD3_DELETE'),(478,'ROLE_MOD_MODULE_GRAPHNVD3_READ'),(479,'ROLE_MOD_MODULE_GRAPHNVD3_UPDATE'),(481,'ROLE_MOD_MODULE_GRIDHTML_CREATE'),(484,'ROLE_MOD_MODULE_GRIDHTML_DELETE'),(482,'ROLE_MOD_MODULE_GRIDHTML_READ'),(483,'ROLE_MOD_MODULE_GRIDHTML_UPDATE'),(889,'ROLE_MOD_MODULE_GSITEMAP_CREATE'),(892,'ROLE_MOD_MODULE_GSITEMAP_DELETE'),(890,'ROLE_MOD_MODULE_GSITEMAP_READ'),(891,'ROLE_MOD_MODULE_GSITEMAP_UPDATE'),(893,'ROLE_MOD_MODULE_PAYPAL_CREATE'),(896,'ROLE_MOD_MODULE_PAYPAL_DELETE'),(894,'ROLE_MOD_MODULE_PAYPAL_READ'),(895,'ROLE_MOD_MODULE_PAYPAL_UPDATE'),(793,'ROLE_MOD_MODULE_PSCLEANER_CREATE'),(796,'ROLE_MOD_MODULE_PSCLEANER_DELETE'),(794,'ROLE_MOD_MODULE_PSCLEANER_READ'),(795,'ROLE_MOD_MODULE_PSCLEANER_UPDATE'),(797,'ROLE_MOD_MODULE_PSPIXEL_CREATE'),(800,'ROLE_MOD_MODULE_PSPIXEL_DELETE'),(798,'ROLE_MOD_MODULE_PSPIXEL_READ'),(799,'ROLE_MOD_MODULE_PSPIXEL_UPDATE'),(485,'ROLE_MOD_MODULE_PS_BANNER_CREATE'),(488,'ROLE_MOD_MODULE_PS_BANNER_DELETE'),(486,'ROLE_MOD_MODULE_PS_BANNER_READ'),(487,'ROLE_MOD_MODULE_PS_BANNER_UPDATE'),(709,'ROLE_MOD_MODULE_PS_BESTSELLERS_CREATE'),(712,'ROLE_MOD_MODULE_PS_BESTSELLERS_DELETE'),(710,'ROLE_MOD_MODULE_PS_BESTSELLERS_READ'),(711,'ROLE_MOD_MODULE_PS_BESTSELLERS_UPDATE'),(773,'ROLE_MOD_MODULE_PS_BRANDLIST_CREATE'),(776,'ROLE_MOD_MODULE_PS_BRANDLIST_DELETE'),(774,'ROLE_MOD_MODULE_PS_BRANDLIST_READ'),(775,'ROLE_MOD_MODULE_PS_BRANDLIST_UPDATE'),(717,'ROLE_MOD_MODULE_PS_CASHONDELIVERY_CREATE'),(720,'ROLE_MOD_MODULE_PS_CASHONDELIVERY_DELETE'),(718,'ROLE_MOD_MODULE_PS_CASHONDELIVERY_READ'),(719,'ROLE_MOD_MODULE_PS_CASHONDELIVERY_UPDATE'),(489,'ROLE_MOD_MODULE_PS_CATEGORYTREE_CREATE'),(492,'ROLE_MOD_MODULE_PS_CATEGORYTREE_DELETE'),(490,'ROLE_MOD_MODULE_PS_CATEGORYTREE_READ'),(491,'ROLE_MOD_MODULE_PS_CATEGORYTREE_UPDATE'),(497,'ROLE_MOD_MODULE_PS_CONTACTINFO_CREATE'),(500,'ROLE_MOD_MODULE_PS_CONTACTINFO_DELETE'),(498,'ROLE_MOD_MODULE_PS_CONTACTINFO_READ'),(499,'ROLE_MOD_MODULE_PS_CONTACTINFO_UPDATE'),(725,'ROLE_MOD_MODULE_PS_CROSSSELLING_CREATE'),(728,'ROLE_MOD_MODULE_PS_CROSSSELLING_DELETE'),(726,'ROLE_MOD_MODULE_PS_CROSSSELLING_READ'),(727,'ROLE_MOD_MODULE_PS_CROSSSELLING_UPDATE'),(501,'ROLE_MOD_MODULE_PS_CURRENCYSELECTOR_CREATE'),(504,'ROLE_MOD_MODULE_PS_CURRENCYSELECTOR_DELETE'),(502,'ROLE_MOD_MODULE_PS_CURRENCYSELECTOR_READ'),(503,'ROLE_MOD_MODULE_PS_CURRENCYSELECTOR_UPDATE'),(805,'ROLE_MOD_MODULE_PS_CUSTOMERACCOUNTLINKS_CREATE'),(808,'ROLE_MOD_MODULE_PS_CUSTOMERACCOUNTLINKS_DELETE'),(806,'ROLE_MOD_MODULE_PS_CUSTOMERACCOUNTLINKS_READ'),(807,'ROLE_MOD_MODULE_PS_CUSTOMERACCOUNTLINKS_UPDATE'),(509,'ROLE_MOD_MODULE_PS_CUSTOMERSIGNIN_CREATE'),(512,'ROLE_MOD_MODULE_PS_CUSTOMERSIGNIN_DELETE'),(510,'ROLE_MOD_MODULE_PS_CUSTOMERSIGNIN_READ'),(511,'ROLE_MOD_MODULE_PS_CUSTOMERSIGNIN_UPDATE'),(513,'ROLE_MOD_MODULE_PS_CUSTOMTEXT_CREATE'),(516,'ROLE_MOD_MODULE_PS_CUSTOMTEXT_DELETE'),(514,'ROLE_MOD_MODULE_PS_CUSTOMTEXT_READ'),(515,'ROLE_MOD_MODULE_PS_CUSTOMTEXT_UPDATE'),(757,'ROLE_MOD_MODULE_PS_EMAILALERTS_CREATE'),(760,'ROLE_MOD_MODULE_PS_EMAILALERTS_DELETE'),(758,'ROLE_MOD_MODULE_PS_EMAILALERTS_READ'),(759,'ROLE_MOD_MODULE_PS_EMAILALERTS_UPDATE'),(517,'ROLE_MOD_MODULE_PS_EMAILSUBSCRIPTION_CREATE'),(520,'ROLE_MOD_MODULE_PS_EMAILSUBSCRIPTION_DELETE'),(518,'ROLE_MOD_MODULE_PS_EMAILSUBSCRIPTION_READ'),(519,'ROLE_MOD_MODULE_PS_EMAILSUBSCRIPTION_UPDATE'),(521,'ROLE_MOD_MODULE_PS_FACETEDSEARCH_CREATE'),(524,'ROLE_MOD_MODULE_PS_FACETEDSEARCH_DELETE'),(522,'ROLE_MOD_MODULE_PS_FACETEDSEARCH_READ'),(523,'ROLE_MOD_MODULE_PS_FACETEDSEARCH_UPDATE'),(525,'ROLE_MOD_MODULE_PS_FEATUREDPRODUCTS_CREATE'),(528,'ROLE_MOD_MODULE_PS_FEATUREDPRODUCTS_DELETE'),(526,'ROLE_MOD_MODULE_PS_FEATUREDPRODUCTS_READ'),(527,'ROLE_MOD_MODULE_PS_FEATUREDPRODUCTS_UPDATE'),(761,'ROLE_MOD_MODULE_PS_GOOGLEANALYTICS_CREATE'),(764,'ROLE_MOD_MODULE_PS_GOOGLEANALYTICS_DELETE'),(762,'ROLE_MOD_MODULE_PS_GOOGLEANALYTICS_READ'),(763,'ROLE_MOD_MODULE_PS_GOOGLEANALYTICS_UPDATE'),(529,'ROLE_MOD_MODULE_PS_IMAGESLIDER_CREATE'),(532,'ROLE_MOD_MODULE_PS_IMAGESLIDER_DELETE'),(530,'ROLE_MOD_MODULE_PS_IMAGESLIDER_READ'),(531,'ROLE_MOD_MODULE_PS_IMAGESLIDER_UPDATE'),(533,'ROLE_MOD_MODULE_PS_LANGUAGESELECTOR_CREATE'),(536,'ROLE_MOD_MODULE_PS_LANGUAGESELECTOR_DELETE'),(534,'ROLE_MOD_MODULE_PS_LANGUAGESELECTOR_READ'),(535,'ROLE_MOD_MODULE_PS_LANGUAGESELECTOR_UPDATE'),(537,'ROLE_MOD_MODULE_PS_LINKLIST_CREATE'),(540,'ROLE_MOD_MODULE_PS_LINKLIST_DELETE'),(538,'ROLE_MOD_MODULE_PS_LINKLIST_READ'),(539,'ROLE_MOD_MODULE_PS_LINKLIST_UPDATE'),(545,'ROLE_MOD_MODULE_PS_MAINMENU_CREATE'),(548,'ROLE_MOD_MODULE_PS_MAINMENU_DELETE'),(546,'ROLE_MOD_MODULE_PS_MAINMENU_READ'),(547,'ROLE_MOD_MODULE_PS_MAINMENU_UPDATE'),(729,'ROLE_MOD_MODULE_PS_NEWPRODUCTS_CREATE'),(732,'ROLE_MOD_MODULE_PS_NEWPRODUCTS_DELETE'),(730,'ROLE_MOD_MODULE_PS_NEWPRODUCTS_READ'),(731,'ROLE_MOD_MODULE_PS_NEWPRODUCTS_UPDATE'),(721,'ROLE_MOD_MODULE_PS_REMINDER_CREATE'),(724,'ROLE_MOD_MODULE_PS_REMINDER_DELETE'),(722,'ROLE_MOD_MODULE_PS_REMINDER_READ'),(723,'ROLE_MOD_MODULE_PS_REMINDER_UPDATE'),(549,'ROLE_MOD_MODULE_PS_SEARCHBAR_CREATE'),(552,'ROLE_MOD_MODULE_PS_SEARCHBAR_DELETE'),(550,'ROLE_MOD_MODULE_PS_SEARCHBAR_READ'),(551,'ROLE_MOD_MODULE_PS_SEARCHBAR_UPDATE'),(553,'ROLE_MOD_MODULE_PS_SHAREBUTTONS_CREATE'),(556,'ROLE_MOD_MODULE_PS_SHAREBUTTONS_DELETE'),(554,'ROLE_MOD_MODULE_PS_SHAREBUTTONS_READ'),(555,'ROLE_MOD_MODULE_PS_SHAREBUTTONS_UPDATE'),(557,'ROLE_MOD_MODULE_PS_SHOPPINGCART_CREATE'),(560,'ROLE_MOD_MODULE_PS_SHOPPINGCART_DELETE'),(558,'ROLE_MOD_MODULE_PS_SHOPPINGCART_READ'),(559,'ROLE_MOD_MODULE_PS_SHOPPINGCART_UPDATE'),(561,'ROLE_MOD_MODULE_PS_SOCIALFOLLOW_CREATE'),(564,'ROLE_MOD_MODULE_PS_SOCIALFOLLOW_DELETE'),(562,'ROLE_MOD_MODULE_PS_SOCIALFOLLOW_READ'),(563,'ROLE_MOD_MODULE_PS_SOCIALFOLLOW_UPDATE'),(713,'ROLE_MOD_MODULE_PS_SPECIALS_CREATE'),(716,'ROLE_MOD_MODULE_PS_SPECIALS_DELETE'),(714,'ROLE_MOD_MODULE_PS_SPECIALS_READ'),(715,'ROLE_MOD_MODULE_PS_SPECIALS_UPDATE'),(777,'ROLE_MOD_MODULE_PS_SUPPLIERLIST_CREATE'),(780,'ROLE_MOD_MODULE_PS_SUPPLIERLIST_DELETE'),(778,'ROLE_MOD_MODULE_PS_SUPPLIERLIST_READ'),(779,'ROLE_MOD_MODULE_PS_SUPPLIERLIST_UPDATE'),(565,'ROLE_MOD_MODULE_PS_THEMECUSTO_CREATE'),(568,'ROLE_MOD_MODULE_PS_THEMECUSTO_DELETE'),(566,'ROLE_MOD_MODULE_PS_THEMECUSTO_READ'),(567,'ROLE_MOD_MODULE_PS_THEMECUSTO_UPDATE'),(769,'ROLE_MOD_MODULE_PS_VIEWEDPRODUCT_CREATE'),(772,'ROLE_MOD_MODULE_PS_VIEWEDPRODUCT_DELETE'),(770,'ROLE_MOD_MODULE_PS_VIEWEDPRODUCT_READ'),(771,'ROLE_MOD_MODULE_PS_VIEWEDPRODUCT_UPDATE'),(581,'ROLE_MOD_MODULE_PS_WIREPAYMENT_CREATE'),(584,'ROLE_MOD_MODULE_PS_WIREPAYMENT_DELETE'),(582,'ROLE_MOD_MODULE_PS_WIREPAYMENT_READ'),(583,'ROLE_MOD_MODULE_PS_WIREPAYMENT_UPDATE'),(873,'ROLE_MOD_MODULE_SIMPLICITY_CMSBLOCK_CREATE'),(876,'ROLE_MOD_MODULE_SIMPLICITY_CMSBLOCK_DELETE'),(874,'ROLE_MOD_MODULE_SIMPLICITY_CMSBLOCK_READ'),(875,'ROLE_MOD_MODULE_SIMPLICITY_CMSBLOCK_UPDATE'),(877,'ROLE_MOD_MODULE_SIMPLICITY_FBMESSAGING_CREATE'),(880,'ROLE_MOD_MODULE_SIMPLICITY_FBMESSAGING_DELETE'),(878,'ROLE_MOD_MODULE_SIMPLICITY_FBMESSAGING_READ'),(879,'ROLE_MOD_MODULE_SIMPLICITY_FBMESSAGING_UPDATE'),(901,'ROLE_MOD_MODULE_SIMPLICITY_GTM_CREATE'),(904,'ROLE_MOD_MODULE_SIMPLICITY_GTM_DELETE'),(902,'ROLE_MOD_MODULE_SIMPLICITY_GTM_READ'),(903,'ROLE_MOD_MODULE_SIMPLICITY_GTM_UPDATE'),(885,'ROLE_MOD_MODULE_SIMPLICITY_HEADERBAR_CREATE'),(886,'ROLE_MOD_MODULE_SIMPLICITY_HEADERBAR_DELETE'),(887,'ROLE_MOD_MODULE_SIMPLICITY_HEADERBAR_READ'),(888,'ROLE_MOD_MODULE_SIMPLICITY_HEADERBAR_UPDATE'),(801,'ROLE_MOD_MODULE_SIMPLICITY_LOGO_CREATE'),(804,'ROLE_MOD_MODULE_SIMPLICITY_LOGO_DELETE'),(802,'ROLE_MOD_MODULE_SIMPLICITY_LOGO_READ'),(803,'ROLE_MOD_MODULE_SIMPLICITY_LOGO_UPDATE'),(813,'ROLE_MOD_MODULE_SIMPLICITY_SOCIALLOGIN_CREATE'),(816,'ROLE_MOD_MODULE_SIMPLICITY_SOCIALLOGIN_DELETE'),(814,'ROLE_MOD_MODULE_SIMPLICITY_SOCIALLOGIN_READ'),(815,'ROLE_MOD_MODULE_SIMPLICITY_SOCIALLOGIN_UPDATE'),(817,'ROLE_MOD_MODULE_SMILEPAYMSG_CREATE'),(820,'ROLE_MOD_MODULE_SMILEPAYMSG_DELETE'),(818,'ROLE_MOD_MODULE_SMILEPAYMSG_READ'),(819,'ROLE_MOD_MODULE_SMILEPAYMSG_UPDATE'),(869,'ROLE_MOD_MODULE_SMILEPAY_ATM_CREATE'),(872,'ROLE_MOD_MODULE_SMILEPAY_ATM_DELETE'),(870,'ROLE_MOD_MODULE_SMILEPAY_ATM_READ'),(871,'ROLE_MOD_MODULE_SMILEPAY_ATM_UPDATE'),(821,'ROLE_MOD_MODULE_SMILEPAY_C2CUP_CREATE'),(824,'ROLE_MOD_MODULE_SMILEPAY_C2CUP_DELETE'),(822,'ROLE_MOD_MODULE_SMILEPAY_C2CUP_READ'),(823,'ROLE_MOD_MODULE_SMILEPAY_C2CUP_UPDATE'),(833,'ROLE_MOD_MODULE_SMILEPAY_C2C_CREATE'),(836,'ROLE_MOD_MODULE_SMILEPAY_C2C_DELETE'),(834,'ROLE_MOD_MODULE_SMILEPAY_C2C_READ'),(835,'ROLE_MOD_MODULE_SMILEPAY_C2C_UPDATE'),(865,'ROLE_MOD_MODULE_SMILEPAY_CREDIT_CREATE'),(868,'ROLE_MOD_MODULE_SMILEPAY_CREDIT_DELETE'),(866,'ROLE_MOD_MODULE_SMILEPAY_CREDIT_READ'),(867,'ROLE_MOD_MODULE_SMILEPAY_CREDIT_UPDATE'),(861,'ROLE_MOD_MODULE_SMILEPAY_CSV_CREATE'),(864,'ROLE_MOD_MODULE_SMILEPAY_CSV_DELETE'),(862,'ROLE_MOD_MODULE_SMILEPAY_CSV_READ'),(863,'ROLE_MOD_MODULE_SMILEPAY_CSV_UPDATE'),(845,'ROLE_MOD_MODULE_SMILEPAY_EZCATUP_CREATE'),(848,'ROLE_MOD_MODULE_SMILEPAY_EZCATUP_DELETE'),(846,'ROLE_MOD_MODULE_SMILEPAY_EZCATUP_READ'),(847,'ROLE_MOD_MODULE_SMILEPAY_EZCATUP_UPDATE'),(853,'ROLE_MOD_MODULE_SMILEPAY_EZCAT_CREATE'),(856,'ROLE_MOD_MODULE_SMILEPAY_EZCAT_DELETE'),(854,'ROLE_MOD_MODULE_SMILEPAY_EZCAT_READ'),(855,'ROLE_MOD_MODULE_SMILEPAY_EZCAT_UPDATE'),(841,'ROLE_MOD_MODULE_SMILEPAY_FAMIPORT_CREATE'),(844,'ROLE_MOD_MODULE_SMILEPAY_FAMIPORT_DELETE'),(842,'ROLE_MOD_MODULE_SMILEPAY_FAMIPORT_READ'),(843,'ROLE_MOD_MODULE_SMILEPAY_FAMIPORT_UPDATE'),(837,'ROLE_MOD_MODULE_SMILEPAY_IBON_CREATE'),(840,'ROLE_MOD_MODULE_SMILEPAY_IBON_DELETE'),(838,'ROLE_MOD_MODULE_SMILEPAY_IBON_READ'),(839,'ROLE_MOD_MODULE_SMILEPAY_IBON_UPDATE'),(825,'ROLE_MOD_MODULE_SMILEPAY_PALMBOXC2CUP_CREATE'),(828,'ROLE_MOD_MODULE_SMILEPAY_PALMBOXC2CUP_DELETE'),(826,'ROLE_MOD_MODULE_SMILEPAY_PALMBOXC2CUP_READ'),(827,'ROLE_MOD_MODULE_SMILEPAY_PALMBOXC2CUP_UPDATE'),(597,'ROLE_MOD_MODULE_STATSBESTCUSTOMERS_CREATE'),(600,'ROLE_MOD_MODULE_STATSBESTCUSTOMERS_DELETE'),(598,'ROLE_MOD_MODULE_STATSBESTCUSTOMERS_READ'),(599,'ROLE_MOD_MODULE_STATSBESTCUSTOMERS_UPDATE'),(601,'ROLE_MOD_MODULE_STATSBESTPRODUCTS_CREATE'),(604,'ROLE_MOD_MODULE_STATSBESTPRODUCTS_DELETE'),(602,'ROLE_MOD_MODULE_STATSBESTPRODUCTS_READ'),(603,'ROLE_MOD_MODULE_STATSBESTPRODUCTS_UPDATE'),(625,'ROLE_MOD_MODULE_STATSDATA_CREATE'),(628,'ROLE_MOD_MODULE_STATSDATA_DELETE'),(626,'ROLE_MOD_MODULE_STATSDATA_READ'),(627,'ROLE_MOD_MODULE_STATSDATA_UPDATE'),(633,'ROLE_MOD_MODULE_STATSFORECAST_CREATE'),(636,'ROLE_MOD_MODULE_STATSFORECAST_DELETE'),(634,'ROLE_MOD_MODULE_STATSFORECAST_READ'),(635,'ROLE_MOD_MODULE_STATSFORECAST_UPDATE'),(637,'ROLE_MOD_MODULE_STATSLIVE_CREATE'),(640,'ROLE_MOD_MODULE_STATSLIVE_DELETE'),(638,'ROLE_MOD_MODULE_STATSLIVE_READ'),(639,'ROLE_MOD_MODULE_STATSLIVE_UPDATE'),(661,'ROLE_MOD_MODULE_STATSSALES_CREATE'),(664,'ROLE_MOD_MODULE_STATSSALES_DELETE'),(662,'ROLE_MOD_MODULE_STATSSALES_READ'),(663,'ROLE_MOD_MODULE_STATSSALES_UPDATE'),(673,'ROLE_MOD_MODULE_STATSVISITS_CREATE'),(676,'ROLE_MOD_MODULE_STATSVISITS_DELETE'),(674,'ROLE_MOD_MODULE_STATSVISITS_READ'),(675,'ROLE_MOD_MODULE_STATSVISITS_UPDATE'),(1,'ROLE_MOD_TAB_ADMINACCESS_CREATE'),(4,'ROLE_MOD_TAB_ADMINACCESS_DELETE'),(2,'ROLE_MOD_TAB_ADMINACCESS_READ'),(3,'ROLE_MOD_TAB_ADMINACCESS_UPDATE'),(5,'ROLE_MOD_TAB_ADMINADDONSCATALOG_CREATE'),(8,'ROLE_MOD_TAB_ADMINADDONSCATALOG_DELETE'),(6,'ROLE_MOD_TAB_ADMINADDONSCATALOG_READ'),(7,'ROLE_MOD_TAB_ADMINADDONSCATALOG_UPDATE'),(9,'ROLE_MOD_TAB_ADMINADDRESSES_CREATE'),(12,'ROLE_MOD_TAB_ADMINADDRESSES_DELETE'),(10,'ROLE_MOD_TAB_ADMINADDRESSES_READ'),(11,'ROLE_MOD_TAB_ADMINADDRESSES_UPDATE'),(13,'ROLE_MOD_TAB_ADMINADMINPREFERENCES_CREATE'),(16,'ROLE_MOD_TAB_ADMINADMINPREFERENCES_DELETE'),(14,'ROLE_MOD_TAB_ADMINADMINPREFERENCES_READ'),(15,'ROLE_MOD_TAB_ADMINADMINPREFERENCES_UPDATE'),(17,'ROLE_MOD_TAB_ADMINADVANCEDPARAMETERS_CREATE'),(20,'ROLE_MOD_TAB_ADMINADVANCEDPARAMETERS_DELETE'),(18,'ROLE_MOD_TAB_ADMINADVANCEDPARAMETERS_READ'),(19,'ROLE_MOD_TAB_ADMINADVANCEDPARAMETERS_UPDATE'),(21,'ROLE_MOD_TAB_ADMINATTACHMENTS_CREATE'),(24,'ROLE_MOD_TAB_ADMINATTACHMENTS_DELETE'),(22,'ROLE_MOD_TAB_ADMINATTACHMENTS_READ'),(23,'ROLE_MOD_TAB_ADMINATTACHMENTS_UPDATE'),(25,'ROLE_MOD_TAB_ADMINATTRIBUTESGROUPS_CREATE'),(28,'ROLE_MOD_TAB_ADMINATTRIBUTESGROUPS_DELETE'),(26,'ROLE_MOD_TAB_ADMINATTRIBUTESGROUPS_READ'),(27,'ROLE_MOD_TAB_ADMINATTRIBUTESGROUPS_UPDATE'),(29,'ROLE_MOD_TAB_ADMINBACKUP_CREATE'),(32,'ROLE_MOD_TAB_ADMINBACKUP_DELETE'),(30,'ROLE_MOD_TAB_ADMINBACKUP_READ'),(31,'ROLE_MOD_TAB_ADMINBACKUP_UPDATE'),(33,'ROLE_MOD_TAB_ADMINCARRIERS_CREATE'),(36,'ROLE_MOD_TAB_ADMINCARRIERS_DELETE'),(34,'ROLE_MOD_TAB_ADMINCARRIERS_READ'),(35,'ROLE_MOD_TAB_ADMINCARRIERS_UPDATE'),(37,'ROLE_MOD_TAB_ADMINCARTRULES_CREATE'),(40,'ROLE_MOD_TAB_ADMINCARTRULES_DELETE'),(38,'ROLE_MOD_TAB_ADMINCARTRULES_READ'),(39,'ROLE_MOD_TAB_ADMINCARTRULES_UPDATE'),(41,'ROLE_MOD_TAB_ADMINCARTS_CREATE'),(44,'ROLE_MOD_TAB_ADMINCARTS_DELETE'),(42,'ROLE_MOD_TAB_ADMINCARTS_READ'),(43,'ROLE_MOD_TAB_ADMINCARTS_UPDATE'),(45,'ROLE_MOD_TAB_ADMINCATALOG_CREATE'),(48,'ROLE_MOD_TAB_ADMINCATALOG_DELETE'),(46,'ROLE_MOD_TAB_ADMINCATALOG_READ'),(47,'ROLE_MOD_TAB_ADMINCATALOG_UPDATE'),(49,'ROLE_MOD_TAB_ADMINCATEGORIES_CREATE'),(52,'ROLE_MOD_TAB_ADMINCATEGORIES_DELETE'),(50,'ROLE_MOD_TAB_ADMINCATEGORIES_READ'),(51,'ROLE_MOD_TAB_ADMINCATEGORIES_UPDATE'),(53,'ROLE_MOD_TAB_ADMINCMSCONTENT_CREATE'),(56,'ROLE_MOD_TAB_ADMINCMSCONTENT_DELETE'),(54,'ROLE_MOD_TAB_ADMINCMSCONTENT_READ'),(55,'ROLE_MOD_TAB_ADMINCMSCONTENT_UPDATE'),(57,'ROLE_MOD_TAB_ADMINCONTACTS_CREATE'),(60,'ROLE_MOD_TAB_ADMINCONTACTS_DELETE'),(58,'ROLE_MOD_TAB_ADMINCONTACTS_READ'),(59,'ROLE_MOD_TAB_ADMINCONTACTS_UPDATE'),(61,'ROLE_MOD_TAB_ADMINCOUNTRIES_CREATE'),(64,'ROLE_MOD_TAB_ADMINCOUNTRIES_DELETE'),(62,'ROLE_MOD_TAB_ADMINCOUNTRIES_READ'),(63,'ROLE_MOD_TAB_ADMINCOUNTRIES_UPDATE'),(65,'ROLE_MOD_TAB_ADMINCURRENCIES_CREATE'),(68,'ROLE_MOD_TAB_ADMINCURRENCIES_DELETE'),(66,'ROLE_MOD_TAB_ADMINCURRENCIES_READ'),(67,'ROLE_MOD_TAB_ADMINCURRENCIES_UPDATE'),(69,'ROLE_MOD_TAB_ADMINCUSTOMERPREFERENCES_CREATE'),(72,'ROLE_MOD_TAB_ADMINCUSTOMERPREFERENCES_DELETE'),(70,'ROLE_MOD_TAB_ADMINCUSTOMERPREFERENCES_READ'),(71,'ROLE_MOD_TAB_ADMINCUSTOMERPREFERENCES_UPDATE'),(73,'ROLE_MOD_TAB_ADMINCUSTOMERS_CREATE'),(76,'ROLE_MOD_TAB_ADMINCUSTOMERS_DELETE'),(74,'ROLE_MOD_TAB_ADMINCUSTOMERS_READ'),(75,'ROLE_MOD_TAB_ADMINCUSTOMERS_UPDATE'),(77,'ROLE_MOD_TAB_ADMINCUSTOMERTHREADS_CREATE'),(80,'ROLE_MOD_TAB_ADMINCUSTOMERTHREADS_DELETE'),(78,'ROLE_MOD_TAB_ADMINCUSTOMERTHREADS_READ'),(79,'ROLE_MOD_TAB_ADMINCUSTOMERTHREADS_UPDATE'),(81,'ROLE_MOD_TAB_ADMINDASHBOARD_CREATE'),(84,'ROLE_MOD_TAB_ADMINDASHBOARD_DELETE'),(82,'ROLE_MOD_TAB_ADMINDASHBOARD_READ'),(83,'ROLE_MOD_TAB_ADMINDASHBOARD_UPDATE'),(465,'ROLE_MOD_TAB_ADMINDASHGOALS_CREATE'),(468,'ROLE_MOD_TAB_ADMINDASHGOALS_DELETE'),(466,'ROLE_MOD_TAB_ADMINDASHGOALS_READ'),(467,'ROLE_MOD_TAB_ADMINDASHGOALS_UPDATE'),(85,'ROLE_MOD_TAB_ADMINDELIVERYSLIP_CREATE'),(88,'ROLE_MOD_TAB_ADMINDELIVERYSLIP_DELETE'),(86,'ROLE_MOD_TAB_ADMINDELIVERYSLIP_READ'),(87,'ROLE_MOD_TAB_ADMINDELIVERYSLIP_UPDATE'),(89,'ROLE_MOD_TAB_ADMINEMAILS_CREATE'),(92,'ROLE_MOD_TAB_ADMINEMAILS_DELETE'),(90,'ROLE_MOD_TAB_ADMINEMAILS_READ'),(91,'ROLE_MOD_TAB_ADMINEMAILS_UPDATE'),(93,'ROLE_MOD_TAB_ADMINEMPLOYEES_CREATE'),(96,'ROLE_MOD_TAB_ADMINEMPLOYEES_DELETE'),(94,'ROLE_MOD_TAB_ADMINEMPLOYEES_READ'),(95,'ROLE_MOD_TAB_ADMINEMPLOYEES_UPDATE'),(97,'ROLE_MOD_TAB_ADMINFEATURES_CREATE'),(100,'ROLE_MOD_TAB_ADMINFEATURES_DELETE'),(98,'ROLE_MOD_TAB_ADMINFEATURES_READ'),(99,'ROLE_MOD_TAB_ADMINFEATURES_UPDATE'),(101,'ROLE_MOD_TAB_ADMINGENDERS_CREATE'),(104,'ROLE_MOD_TAB_ADMINGENDERS_DELETE'),(102,'ROLE_MOD_TAB_ADMINGENDERS_READ'),(103,'ROLE_MOD_TAB_ADMINGENDERS_UPDATE'),(105,'ROLE_MOD_TAB_ADMINGEOLOCATION_CREATE'),(108,'ROLE_MOD_TAB_ADMINGEOLOCATION_DELETE'),(106,'ROLE_MOD_TAB_ADMINGEOLOCATION_READ'),(107,'ROLE_MOD_TAB_ADMINGEOLOCATION_UPDATE'),(109,'ROLE_MOD_TAB_ADMINGROUPS_CREATE'),(112,'ROLE_MOD_TAB_ADMINGROUPS_DELETE'),(110,'ROLE_MOD_TAB_ADMINGROUPS_READ'),(111,'ROLE_MOD_TAB_ADMINGROUPS_UPDATE'),(113,'ROLE_MOD_TAB_ADMINIMAGES_CREATE'),(116,'ROLE_MOD_TAB_ADMINIMAGES_DELETE'),(114,'ROLE_MOD_TAB_ADMINIMAGES_READ'),(115,'ROLE_MOD_TAB_ADMINIMAGES_UPDATE'),(117,'ROLE_MOD_TAB_ADMINIMPORT_CREATE'),(120,'ROLE_MOD_TAB_ADMINIMPORT_DELETE'),(118,'ROLE_MOD_TAB_ADMINIMPORT_READ'),(119,'ROLE_MOD_TAB_ADMINIMPORT_UPDATE'),(121,'ROLE_MOD_TAB_ADMININFORMATION_CREATE'),(124,'ROLE_MOD_TAB_ADMININFORMATION_DELETE'),(122,'ROLE_MOD_TAB_ADMININFORMATION_READ'),(123,'ROLE_MOD_TAB_ADMININFORMATION_UPDATE'),(125,'ROLE_MOD_TAB_ADMININTERNATIONAL_CREATE'),(128,'ROLE_MOD_TAB_ADMININTERNATIONAL_DELETE'),(126,'ROLE_MOD_TAB_ADMININTERNATIONAL_READ'),(127,'ROLE_MOD_TAB_ADMININTERNATIONAL_UPDATE'),(129,'ROLE_MOD_TAB_ADMININVOICES_CREATE'),(132,'ROLE_MOD_TAB_ADMININVOICES_DELETE'),(130,'ROLE_MOD_TAB_ADMININVOICES_READ'),(131,'ROLE_MOD_TAB_ADMININVOICES_UPDATE'),(133,'ROLE_MOD_TAB_ADMINLANGUAGES_CREATE'),(136,'ROLE_MOD_TAB_ADMINLANGUAGES_DELETE'),(134,'ROLE_MOD_TAB_ADMINLANGUAGES_READ'),(135,'ROLE_MOD_TAB_ADMINLANGUAGES_UPDATE'),(137,'ROLE_MOD_TAB_ADMINLINKWIDGET_CREATE'),(140,'ROLE_MOD_TAB_ADMINLINKWIDGET_DELETE'),(138,'ROLE_MOD_TAB_ADMINLINKWIDGET_READ'),(139,'ROLE_MOD_TAB_ADMINLINKWIDGET_UPDATE'),(141,'ROLE_MOD_TAB_ADMINLOCALIZATION_CREATE'),(144,'ROLE_MOD_TAB_ADMINLOCALIZATION_DELETE'),(142,'ROLE_MOD_TAB_ADMINLOCALIZATION_READ'),(143,'ROLE_MOD_TAB_ADMINLOCALIZATION_UPDATE'),(145,'ROLE_MOD_TAB_ADMINLOGS_CREATE'),(148,'ROLE_MOD_TAB_ADMINLOGS_DELETE'),(146,'ROLE_MOD_TAB_ADMINLOGS_READ'),(147,'ROLE_MOD_TAB_ADMINLOGS_UPDATE'),(149,'ROLE_MOD_TAB_ADMINMAINTENANCE_CREATE'),(152,'ROLE_MOD_TAB_ADMINMAINTENANCE_DELETE'),(150,'ROLE_MOD_TAB_ADMINMAINTENANCE_READ'),(151,'ROLE_MOD_TAB_ADMINMAINTENANCE_UPDATE'),(153,'ROLE_MOD_TAB_ADMINMANUFACTURERS_CREATE'),(156,'ROLE_MOD_TAB_ADMINMANUFACTURERS_DELETE'),(154,'ROLE_MOD_TAB_ADMINMANUFACTURERS_READ'),(155,'ROLE_MOD_TAB_ADMINMANUFACTURERS_UPDATE'),(157,'ROLE_MOD_TAB_ADMINMETA_CREATE'),(160,'ROLE_MOD_TAB_ADMINMETA_DELETE'),(158,'ROLE_MOD_TAB_ADMINMETA_READ'),(159,'ROLE_MOD_TAB_ADMINMETA_UPDATE'),(165,'ROLE_MOD_TAB_ADMINMODULESPOSITIONS_CREATE'),(168,'ROLE_MOD_TAB_ADMINMODULESPOSITIONS_DELETE'),(166,'ROLE_MOD_TAB_ADMINMODULESPOSITIONS_READ'),(167,'ROLE_MOD_TAB_ADMINMODULESPOSITIONS_UPDATE'),(169,'ROLE_MOD_TAB_ADMINMODULESSF_CREATE'),(172,'ROLE_MOD_TAB_ADMINMODULESSF_DELETE'),(170,'ROLE_MOD_TAB_ADMINMODULESSF_READ'),(171,'ROLE_MOD_TAB_ADMINMODULESSF_UPDATE'),(161,'ROLE_MOD_TAB_ADMINMODULES_CREATE'),(164,'ROLE_MOD_TAB_ADMINMODULES_DELETE'),(162,'ROLE_MOD_TAB_ADMINMODULES_READ'),(163,'ROLE_MOD_TAB_ADMINMODULES_UPDATE'),(173,'ROLE_MOD_TAB_ADMINORDERMESSAGE_CREATE'),(176,'ROLE_MOD_TAB_ADMINORDERMESSAGE_DELETE'),(174,'ROLE_MOD_TAB_ADMINORDERMESSAGE_READ'),(175,'ROLE_MOD_TAB_ADMINORDERMESSAGE_UPDATE'),(177,'ROLE_MOD_TAB_ADMINORDERPREFERENCES_CREATE'),(180,'ROLE_MOD_TAB_ADMINORDERPREFERENCES_DELETE'),(178,'ROLE_MOD_TAB_ADMINORDERPREFERENCES_READ'),(179,'ROLE_MOD_TAB_ADMINORDERPREFERENCES_UPDATE'),(181,'ROLE_MOD_TAB_ADMINORDERS_CREATE'),(184,'ROLE_MOD_TAB_ADMINORDERS_DELETE'),(182,'ROLE_MOD_TAB_ADMINORDERS_READ'),(183,'ROLE_MOD_TAB_ADMINORDERS_UPDATE'),(185,'ROLE_MOD_TAB_ADMINOUTSTANDING_CREATE'),(188,'ROLE_MOD_TAB_ADMINOUTSTANDING_DELETE'),(186,'ROLE_MOD_TAB_ADMINOUTSTANDING_READ'),(187,'ROLE_MOD_TAB_ADMINOUTSTANDING_UPDATE'),(189,'ROLE_MOD_TAB_ADMINPARENTATTRIBUTESGROUPS_CREATE'),(192,'ROLE_MOD_TAB_ADMINPARENTATTRIBUTESGROUPS_DELETE'),(190,'ROLE_MOD_TAB_ADMINPARENTATTRIBUTESGROUPS_READ'),(191,'ROLE_MOD_TAB_ADMINPARENTATTRIBUTESGROUPS_UPDATE'),(193,'ROLE_MOD_TAB_ADMINPARENTCARTRULES_CREATE'),(196,'ROLE_MOD_TAB_ADMINPARENTCARTRULES_DELETE'),(194,'ROLE_MOD_TAB_ADMINPARENTCARTRULES_READ'),(195,'ROLE_MOD_TAB_ADMINPARENTCARTRULES_UPDATE'),(197,'ROLE_MOD_TAB_ADMINPARENTCOUNTRIES_CREATE'),(200,'ROLE_MOD_TAB_ADMINPARENTCOUNTRIES_DELETE'),(198,'ROLE_MOD_TAB_ADMINPARENTCOUNTRIES_READ'),(199,'ROLE_MOD_TAB_ADMINPARENTCOUNTRIES_UPDATE'),(205,'ROLE_MOD_TAB_ADMINPARENTCUSTOMERPREFERENCES_CREATE'),(208,'ROLE_MOD_TAB_ADMINPARENTCUSTOMERPREFERENCES_DELETE'),(206,'ROLE_MOD_TAB_ADMINPARENTCUSTOMERPREFERENCES_READ'),(207,'ROLE_MOD_TAB_ADMINPARENTCUSTOMERPREFERENCES_UPDATE'),(209,'ROLE_MOD_TAB_ADMINPARENTCUSTOMERTHREADS_CREATE'),(212,'ROLE_MOD_TAB_ADMINPARENTCUSTOMERTHREADS_DELETE'),(210,'ROLE_MOD_TAB_ADMINPARENTCUSTOMERTHREADS_READ'),(211,'ROLE_MOD_TAB_ADMINPARENTCUSTOMERTHREADS_UPDATE'),(201,'ROLE_MOD_TAB_ADMINPARENTCUSTOMER_CREATE'),(204,'ROLE_MOD_TAB_ADMINPARENTCUSTOMER_DELETE'),(202,'ROLE_MOD_TAB_ADMINPARENTCUSTOMER_READ'),(203,'ROLE_MOD_TAB_ADMINPARENTCUSTOMER_UPDATE'),(213,'ROLE_MOD_TAB_ADMINPARENTEMPLOYEES_CREATE'),(216,'ROLE_MOD_TAB_ADMINPARENTEMPLOYEES_DELETE'),(214,'ROLE_MOD_TAB_ADMINPARENTEMPLOYEES_READ'),(215,'ROLE_MOD_TAB_ADMINPARENTEMPLOYEES_UPDATE'),(217,'ROLE_MOD_TAB_ADMINPARENTLOCALIZATION_CREATE'),(220,'ROLE_MOD_TAB_ADMINPARENTLOCALIZATION_DELETE'),(218,'ROLE_MOD_TAB_ADMINPARENTLOCALIZATION_READ'),(219,'ROLE_MOD_TAB_ADMINPARENTLOCALIZATION_UPDATE'),(221,'ROLE_MOD_TAB_ADMINPARENTMANUFACTURERS_CREATE'),(224,'ROLE_MOD_TAB_ADMINPARENTMANUFACTURERS_DELETE'),(222,'ROLE_MOD_TAB_ADMINPARENTMANUFACTURERS_READ'),(223,'ROLE_MOD_TAB_ADMINPARENTMANUFACTURERS_UPDATE'),(229,'ROLE_MOD_TAB_ADMINPARENTMETA_CREATE'),(232,'ROLE_MOD_TAB_ADMINPARENTMETA_DELETE'),(230,'ROLE_MOD_TAB_ADMINPARENTMETA_READ'),(231,'ROLE_MOD_TAB_ADMINPARENTMETA_UPDATE'),(225,'ROLE_MOD_TAB_ADMINPARENTMODULESSF_CREATE'),(228,'ROLE_MOD_TAB_ADMINPARENTMODULESSF_DELETE'),(226,'ROLE_MOD_TAB_ADMINPARENTMODULESSF_READ'),(227,'ROLE_MOD_TAB_ADMINPARENTMODULESSF_UPDATE'),(233,'ROLE_MOD_TAB_ADMINPARENTMODULES_CREATE'),(236,'ROLE_MOD_TAB_ADMINPARENTMODULES_DELETE'),(234,'ROLE_MOD_TAB_ADMINPARENTMODULES_READ'),(235,'ROLE_MOD_TAB_ADMINPARENTMODULES_UPDATE'),(237,'ROLE_MOD_TAB_ADMINPARENTORDERPREFERENCES_CREATE'),(240,'ROLE_MOD_TAB_ADMINPARENTORDERPREFERENCES_DELETE'),(238,'ROLE_MOD_TAB_ADMINPARENTORDERPREFERENCES_READ'),(239,'ROLE_MOD_TAB_ADMINPARENTORDERPREFERENCES_UPDATE'),(241,'ROLE_MOD_TAB_ADMINPARENTORDERS_CREATE'),(244,'ROLE_MOD_TAB_ADMINPARENTORDERS_DELETE'),(242,'ROLE_MOD_TAB_ADMINPARENTORDERS_READ'),(243,'ROLE_MOD_TAB_ADMINPARENTORDERS_UPDATE'),(245,'ROLE_MOD_TAB_ADMINPARENTPAYMENT_CREATE'),(248,'ROLE_MOD_TAB_ADMINPARENTPAYMENT_DELETE'),(246,'ROLE_MOD_TAB_ADMINPARENTPAYMENT_READ'),(247,'ROLE_MOD_TAB_ADMINPARENTPAYMENT_UPDATE'),(249,'ROLE_MOD_TAB_ADMINPARENTPREFERENCES_CREATE'),(252,'ROLE_MOD_TAB_ADMINPARENTPREFERENCES_DELETE'),(250,'ROLE_MOD_TAB_ADMINPARENTPREFERENCES_READ'),(251,'ROLE_MOD_TAB_ADMINPARENTPREFERENCES_UPDATE'),(253,'ROLE_MOD_TAB_ADMINPARENTREQUESTSQL_CREATE'),(256,'ROLE_MOD_TAB_ADMINPARENTREQUESTSQL_DELETE'),(254,'ROLE_MOD_TAB_ADMINPARENTREQUESTSQL_READ'),(255,'ROLE_MOD_TAB_ADMINPARENTREQUESTSQL_UPDATE'),(257,'ROLE_MOD_TAB_ADMINPARENTSEARCHCONF_CREATE'),(260,'ROLE_MOD_TAB_ADMINPARENTSEARCHCONF_DELETE'),(258,'ROLE_MOD_TAB_ADMINPARENTSEARCHCONF_READ'),(259,'ROLE_MOD_TAB_ADMINPARENTSEARCHCONF_UPDATE'),(261,'ROLE_MOD_TAB_ADMINPARENTSHIPPING_CREATE'),(264,'ROLE_MOD_TAB_ADMINPARENTSHIPPING_DELETE'),(262,'ROLE_MOD_TAB_ADMINPARENTSHIPPING_READ'),(263,'ROLE_MOD_TAB_ADMINPARENTSHIPPING_UPDATE'),(265,'ROLE_MOD_TAB_ADMINPARENTSTOCKMANAGEMENT_CREATE'),(268,'ROLE_MOD_TAB_ADMINPARENTSTOCKMANAGEMENT_DELETE'),(266,'ROLE_MOD_TAB_ADMINPARENTSTOCKMANAGEMENT_READ'),(267,'ROLE_MOD_TAB_ADMINPARENTSTOCKMANAGEMENT_UPDATE'),(269,'ROLE_MOD_TAB_ADMINPARENTSTORES_CREATE'),(272,'ROLE_MOD_TAB_ADMINPARENTSTORES_DELETE'),(270,'ROLE_MOD_TAB_ADMINPARENTSTORES_READ'),(271,'ROLE_MOD_TAB_ADMINPARENTSTORES_UPDATE'),(273,'ROLE_MOD_TAB_ADMINPARENTTAXES_CREATE'),(276,'ROLE_MOD_TAB_ADMINPARENTTAXES_DELETE'),(274,'ROLE_MOD_TAB_ADMINPARENTTAXES_READ'),(275,'ROLE_MOD_TAB_ADMINPARENTTAXES_UPDATE'),(277,'ROLE_MOD_TAB_ADMINPARENTTHEMES_CREATE'),(280,'ROLE_MOD_TAB_ADMINPARENTTHEMES_DELETE'),(278,'ROLE_MOD_TAB_ADMINPARENTTHEMES_READ'),(279,'ROLE_MOD_TAB_ADMINPARENTTHEMES_UPDATE'),(285,'ROLE_MOD_TAB_ADMINPAYMENTPREFERENCES_CREATE'),(288,'ROLE_MOD_TAB_ADMINPAYMENTPREFERENCES_DELETE'),(286,'ROLE_MOD_TAB_ADMINPAYMENTPREFERENCES_READ'),(287,'ROLE_MOD_TAB_ADMINPAYMENTPREFERENCES_UPDATE'),(281,'ROLE_MOD_TAB_ADMINPAYMENT_CREATE'),(284,'ROLE_MOD_TAB_ADMINPAYMENT_DELETE'),(282,'ROLE_MOD_TAB_ADMINPAYMENT_READ'),(283,'ROLE_MOD_TAB_ADMINPAYMENT_UPDATE'),(289,'ROLE_MOD_TAB_ADMINPERFORMANCE_CREATE'),(292,'ROLE_MOD_TAB_ADMINPERFORMANCE_DELETE'),(290,'ROLE_MOD_TAB_ADMINPERFORMANCE_READ'),(291,'ROLE_MOD_TAB_ADMINPERFORMANCE_UPDATE'),(293,'ROLE_MOD_TAB_ADMINPPREFERENCES_CREATE'),(296,'ROLE_MOD_TAB_ADMINPPREFERENCES_DELETE'),(294,'ROLE_MOD_TAB_ADMINPPREFERENCES_READ'),(295,'ROLE_MOD_TAB_ADMINPPREFERENCES_UPDATE'),(297,'ROLE_MOD_TAB_ADMINPREFERENCES_CREATE'),(300,'ROLE_MOD_TAB_ADMINPREFERENCES_DELETE'),(298,'ROLE_MOD_TAB_ADMINPREFERENCES_READ'),(299,'ROLE_MOD_TAB_ADMINPREFERENCES_UPDATE'),(301,'ROLE_MOD_TAB_ADMINPRODUCTS_CREATE'),(304,'ROLE_MOD_TAB_ADMINPRODUCTS_DELETE'),(302,'ROLE_MOD_TAB_ADMINPRODUCTS_READ'),(303,'ROLE_MOD_TAB_ADMINPRODUCTS_UPDATE'),(305,'ROLE_MOD_TAB_ADMINPROFILES_CREATE'),(308,'ROLE_MOD_TAB_ADMINPROFILES_DELETE'),(306,'ROLE_MOD_TAB_ADMINPROFILES_READ'),(307,'ROLE_MOD_TAB_ADMINPROFILES_UPDATE'),(577,'ROLE_MOD_TAB_ADMINPSTHEMECUSTOADVANCED_CREATE'),(580,'ROLE_MOD_TAB_ADMINPSTHEMECUSTOADVANCED_DELETE'),(578,'ROLE_MOD_TAB_ADMINPSTHEMECUSTOADVANCED_READ'),(579,'ROLE_MOD_TAB_ADMINPSTHEMECUSTOADVANCED_UPDATE'),(573,'ROLE_MOD_TAB_ADMINPSTHEMECUSTOCONFIGURATION_CREATE'),(576,'ROLE_MOD_TAB_ADMINPSTHEMECUSTOCONFIGURATION_DELETE'),(574,'ROLE_MOD_TAB_ADMINPSTHEMECUSTOCONFIGURATION_READ'),(575,'ROLE_MOD_TAB_ADMINPSTHEMECUSTOCONFIGURATION_UPDATE'),(309,'ROLE_MOD_TAB_ADMINREFERRERS_CREATE'),(312,'ROLE_MOD_TAB_ADMINREFERRERS_DELETE'),(310,'ROLE_MOD_TAB_ADMINREFERRERS_READ'),(311,'ROLE_MOD_TAB_ADMINREFERRERS_UPDATE'),(313,'ROLE_MOD_TAB_ADMINREQUESTSQL_CREATE'),(316,'ROLE_MOD_TAB_ADMINREQUESTSQL_DELETE'),(314,'ROLE_MOD_TAB_ADMINREQUESTSQL_READ'),(315,'ROLE_MOD_TAB_ADMINREQUESTSQL_UPDATE'),(317,'ROLE_MOD_TAB_ADMINRETURN_CREATE'),(320,'ROLE_MOD_TAB_ADMINRETURN_DELETE'),(318,'ROLE_MOD_TAB_ADMINRETURN_READ'),(319,'ROLE_MOD_TAB_ADMINRETURN_UPDATE'),(321,'ROLE_MOD_TAB_ADMINSEARCHCONF_CREATE'),(324,'ROLE_MOD_TAB_ADMINSEARCHCONF_DELETE'),(322,'ROLE_MOD_TAB_ADMINSEARCHCONF_READ'),(323,'ROLE_MOD_TAB_ADMINSEARCHCONF_UPDATE'),(325,'ROLE_MOD_TAB_ADMINSEARCHENGINES_CREATE'),(328,'ROLE_MOD_TAB_ADMINSEARCHENGINES_DELETE'),(326,'ROLE_MOD_TAB_ADMINSEARCHENGINES_READ'),(327,'ROLE_MOD_TAB_ADMINSEARCHENGINES_UPDATE'),(329,'ROLE_MOD_TAB_ADMINSHIPPING_CREATE'),(332,'ROLE_MOD_TAB_ADMINSHIPPING_DELETE'),(330,'ROLE_MOD_TAB_ADMINSHIPPING_READ'),(331,'ROLE_MOD_TAB_ADMINSHIPPING_UPDATE'),(333,'ROLE_MOD_TAB_ADMINSHOPGROUP_CREATE'),(336,'ROLE_MOD_TAB_ADMINSHOPGROUP_DELETE'),(334,'ROLE_MOD_TAB_ADMINSHOPGROUP_READ'),(335,'ROLE_MOD_TAB_ADMINSHOPGROUP_UPDATE'),(337,'ROLE_MOD_TAB_ADMINSHOPURL_CREATE'),(340,'ROLE_MOD_TAB_ADMINSHOPURL_DELETE'),(338,'ROLE_MOD_TAB_ADMINSHOPURL_READ'),(339,'ROLE_MOD_TAB_ADMINSHOPURL_UPDATE'),(881,'ROLE_MOD_TAB_ADMINSIMPLICITYFBMESSAGING_CREATE'),(884,'ROLE_MOD_TAB_ADMINSIMPLICITYFBMESSAGING_DELETE'),(882,'ROLE_MOD_TAB_ADMINSIMPLICITYFBMESSAGING_READ'),(883,'ROLE_MOD_TAB_ADMINSIMPLICITYFBMESSAGING_UPDATE'),(341,'ROLE_MOD_TAB_ADMINSLIP_CREATE'),(344,'ROLE_MOD_TAB_ADMINSLIP_DELETE'),(342,'ROLE_MOD_TAB_ADMINSLIP_READ'),(343,'ROLE_MOD_TAB_ADMINSLIP_UPDATE'),(849,'ROLE_MOD_TAB_ADMINSMILEPAYEZCATUP_CREATE'),(852,'ROLE_MOD_TAB_ADMINSMILEPAYEZCATUP_DELETE'),(850,'ROLE_MOD_TAB_ADMINSMILEPAYEZCATUP_READ'),(851,'ROLE_MOD_TAB_ADMINSMILEPAYEZCATUP_UPDATE'),(857,'ROLE_MOD_TAB_ADMINSMILEPAYEZCAT_CREATE'),(860,'ROLE_MOD_TAB_ADMINSMILEPAYEZCAT_DELETE'),(858,'ROLE_MOD_TAB_ADMINSMILEPAYEZCAT_READ'),(859,'ROLE_MOD_TAB_ADMINSMILEPAYEZCAT_UPDATE'),(829,'ROLE_MOD_TAB_ADMINSMILEPAYPALMBOXC2CUP_CREATE'),(832,'ROLE_MOD_TAB_ADMINSMILEPAYPALMBOXC2CUP_DELETE'),(830,'ROLE_MOD_TAB_ADMINSMILEPAYPALMBOXC2CUP_READ'),(831,'ROLE_MOD_TAB_ADMINSMILEPAYPALMBOXC2CUP_UPDATE'),(345,'ROLE_MOD_TAB_ADMINSPECIFICPRICERULE_CREATE'),(348,'ROLE_MOD_TAB_ADMINSPECIFICPRICERULE_DELETE'),(346,'ROLE_MOD_TAB_ADMINSPECIFICPRICERULE_READ'),(347,'ROLE_MOD_TAB_ADMINSPECIFICPRICERULE_UPDATE'),(349,'ROLE_MOD_TAB_ADMINSTATES_CREATE'),(352,'ROLE_MOD_TAB_ADMINSTATES_DELETE'),(350,'ROLE_MOD_TAB_ADMINSTATES_READ'),(351,'ROLE_MOD_TAB_ADMINSTATES_UPDATE'),(353,'ROLE_MOD_TAB_ADMINSTATS_CREATE'),(356,'ROLE_MOD_TAB_ADMINSTATS_DELETE'),(354,'ROLE_MOD_TAB_ADMINSTATS_READ'),(355,'ROLE_MOD_TAB_ADMINSTATS_UPDATE'),(357,'ROLE_MOD_TAB_ADMINSTATUSES_CREATE'),(360,'ROLE_MOD_TAB_ADMINSTATUSES_DELETE'),(358,'ROLE_MOD_TAB_ADMINSTATUSES_READ'),(359,'ROLE_MOD_TAB_ADMINSTATUSES_UPDATE'),(365,'ROLE_MOD_TAB_ADMINSTOCKCONFIGURATION_CREATE'),(368,'ROLE_MOD_TAB_ADMINSTOCKCONFIGURATION_DELETE'),(366,'ROLE_MOD_TAB_ADMINSTOCKCONFIGURATION_READ'),(367,'ROLE_MOD_TAB_ADMINSTOCKCONFIGURATION_UPDATE'),(369,'ROLE_MOD_TAB_ADMINSTOCKCOVER_CREATE'),(372,'ROLE_MOD_TAB_ADMINSTOCKCOVER_DELETE'),(370,'ROLE_MOD_TAB_ADMINSTOCKCOVER_READ'),(371,'ROLE_MOD_TAB_ADMINSTOCKCOVER_UPDATE'),(373,'ROLE_MOD_TAB_ADMINSTOCKINSTANTSTATE_CREATE'),(376,'ROLE_MOD_TAB_ADMINSTOCKINSTANTSTATE_DELETE'),(374,'ROLE_MOD_TAB_ADMINSTOCKINSTANTSTATE_READ'),(375,'ROLE_MOD_TAB_ADMINSTOCKINSTANTSTATE_UPDATE'),(377,'ROLE_MOD_TAB_ADMINSTOCKMANAGEMENT_CREATE'),(380,'ROLE_MOD_TAB_ADMINSTOCKMANAGEMENT_DELETE'),(378,'ROLE_MOD_TAB_ADMINSTOCKMANAGEMENT_READ'),(379,'ROLE_MOD_TAB_ADMINSTOCKMANAGEMENT_UPDATE'),(381,'ROLE_MOD_TAB_ADMINSTOCKMVT_CREATE'),(384,'ROLE_MOD_TAB_ADMINSTOCKMVT_DELETE'),(382,'ROLE_MOD_TAB_ADMINSTOCKMVT_READ'),(383,'ROLE_MOD_TAB_ADMINSTOCKMVT_UPDATE'),(361,'ROLE_MOD_TAB_ADMINSTOCK_CREATE'),(364,'ROLE_MOD_TAB_ADMINSTOCK_DELETE'),(362,'ROLE_MOD_TAB_ADMINSTOCK_READ'),(363,'ROLE_MOD_TAB_ADMINSTOCK_UPDATE'),(385,'ROLE_MOD_TAB_ADMINSTORES_CREATE'),(388,'ROLE_MOD_TAB_ADMINSTORES_DELETE'),(386,'ROLE_MOD_TAB_ADMINSTORES_READ'),(387,'ROLE_MOD_TAB_ADMINSTORES_UPDATE'),(389,'ROLE_MOD_TAB_ADMINSUPPLIERS_CREATE'),(392,'ROLE_MOD_TAB_ADMINSUPPLIERS_DELETE'),(390,'ROLE_MOD_TAB_ADMINSUPPLIERS_READ'),(391,'ROLE_MOD_TAB_ADMINSUPPLIERS_UPDATE'),(393,'ROLE_MOD_TAB_ADMINSUPPLYORDERS_CREATE'),(396,'ROLE_MOD_TAB_ADMINSUPPLYORDERS_DELETE'),(394,'ROLE_MOD_TAB_ADMINSUPPLYORDERS_READ'),(395,'ROLE_MOD_TAB_ADMINSUPPLYORDERS_UPDATE'),(397,'ROLE_MOD_TAB_ADMINTAGS_CREATE'),(400,'ROLE_MOD_TAB_ADMINTAGS_DELETE'),(398,'ROLE_MOD_TAB_ADMINTAGS_READ'),(399,'ROLE_MOD_TAB_ADMINTAGS_UPDATE'),(401,'ROLE_MOD_TAB_ADMINTAXES_CREATE'),(404,'ROLE_MOD_TAB_ADMINTAXES_DELETE'),(402,'ROLE_MOD_TAB_ADMINTAXES_READ'),(403,'ROLE_MOD_TAB_ADMINTAXES_UPDATE'),(405,'ROLE_MOD_TAB_ADMINTAXRULESGROUP_CREATE'),(408,'ROLE_MOD_TAB_ADMINTAXRULESGROUP_DELETE'),(406,'ROLE_MOD_TAB_ADMINTAXRULESGROUP_READ'),(407,'ROLE_MOD_TAB_ADMINTAXRULESGROUP_UPDATE'),(413,'ROLE_MOD_TAB_ADMINTHEMESCATALOG_CREATE'),(416,'ROLE_MOD_TAB_ADMINTHEMESCATALOG_DELETE'),(414,'ROLE_MOD_TAB_ADMINTHEMESCATALOG_READ'),(415,'ROLE_MOD_TAB_ADMINTHEMESCATALOG_UPDATE'),(569,'ROLE_MOD_TAB_ADMINTHEMESPARENT_CREATE'),(572,'ROLE_MOD_TAB_ADMINTHEMESPARENT_DELETE'),(570,'ROLE_MOD_TAB_ADMINTHEMESPARENT_READ'),(571,'ROLE_MOD_TAB_ADMINTHEMESPARENT_UPDATE'),(409,'ROLE_MOD_TAB_ADMINTHEMES_CREATE'),(412,'ROLE_MOD_TAB_ADMINTHEMES_DELETE'),(410,'ROLE_MOD_TAB_ADMINTHEMES_READ'),(411,'ROLE_MOD_TAB_ADMINTHEMES_UPDATE'),(417,'ROLE_MOD_TAB_ADMINTRACKING_CREATE'),(420,'ROLE_MOD_TAB_ADMINTRACKING_DELETE'),(418,'ROLE_MOD_TAB_ADMINTRACKING_READ'),(419,'ROLE_MOD_TAB_ADMINTRACKING_UPDATE'),(421,'ROLE_MOD_TAB_ADMINTRANSLATIONS_CREATE'),(424,'ROLE_MOD_TAB_ADMINTRANSLATIONS_DELETE'),(422,'ROLE_MOD_TAB_ADMINTRANSLATIONS_READ'),(423,'ROLE_MOD_TAB_ADMINTRANSLATIONS_UPDATE'),(425,'ROLE_MOD_TAB_ADMINWAREHOUSES_CREATE'),(428,'ROLE_MOD_TAB_ADMINWAREHOUSES_DELETE'),(426,'ROLE_MOD_TAB_ADMINWAREHOUSES_READ'),(427,'ROLE_MOD_TAB_ADMINWAREHOUSES_UPDATE'),(429,'ROLE_MOD_TAB_ADMINWEBSERVICE_CREATE'),(432,'ROLE_MOD_TAB_ADMINWEBSERVICE_DELETE'),(430,'ROLE_MOD_TAB_ADMINWEBSERVICE_READ'),(431,'ROLE_MOD_TAB_ADMINWEBSERVICE_UPDATE'),(433,'ROLE_MOD_TAB_ADMINZONES_CREATE'),(436,'ROLE_MOD_TAB_ADMINZONES_DELETE'),(434,'ROLE_MOD_TAB_ADMINZONES_READ'),(435,'ROLE_MOD_TAB_ADMINZONES_UPDATE'),(437,'ROLE_MOD_TAB_CONFIGURE_CREATE'),(440,'ROLE_MOD_TAB_CONFIGURE_DELETE'),(438,'ROLE_MOD_TAB_CONFIGURE_READ'),(439,'ROLE_MOD_TAB_CONFIGURE_UPDATE'),(441,'ROLE_MOD_TAB_IMPROVE_CREATE'),(444,'ROLE_MOD_TAB_IMPROVE_DELETE'),(442,'ROLE_MOD_TAB_IMPROVE_READ'),(443,'ROLE_MOD_TAB_IMPROVE_UPDATE'),(445,'ROLE_MOD_TAB_SELL_CREATE'),(448,'ROLE_MOD_TAB_SELL_DELETE'),(446,'ROLE_MOD_TAB_SELL_READ'),(447,'ROLE_MOD_TAB_SELL_UPDATE'),(449,'ROLE_MOD_TAB_SHOPPARAMETERS_CREATE'),(452,'ROLE_MOD_TAB_SHOPPARAMETERS_DELETE'),(450,'ROLE_MOD_TAB_SHOPPARAMETERS_READ'),(451,'ROLE_MOD_TAB_SHOPPARAMETERS_UPDATE');
/*!40000 ALTER TABLE `ps_authorization_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_carrier`
--

DROP TABLE IF EXISTS `ps_carrier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_carrier` (
  `id_carrier` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_reference` int(10) unsigned NOT NULL,
  `id_tax_rules_group` int(10) unsigned DEFAULT '0',
  `name` varchar(64) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `shipping_handling` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `range_behavior` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_module` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_free` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `shipping_external` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `need_range` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `external_module_name` varchar(64) DEFAULT NULL,
  `shipping_method` int(2) NOT NULL DEFAULT '0',
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `max_width` int(10) DEFAULT '0',
  `max_height` int(10) DEFAULT '0',
  `max_depth` int(10) DEFAULT '0',
  `max_weight` decimal(20,6) DEFAULT '0.000000',
  `grade` int(10) DEFAULT '0',
  `need_address` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '訂單明細顯示用途',
  PRIMARY KEY (`id_carrier`),
  KEY `deleted` (`deleted`,`active`),
  KEY `id_tax_rules_group` (`id_tax_rules_group`),
  KEY `reference` (`id_reference`,`deleted`,`active`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_carrier`
--

LOCK TABLES `ps_carrier` WRITE;
/*!40000 ALTER TABLE `ps_carrier` DISABLE KEYS */;
INSERT INTO `ps_carrier` VALUES (101,101,0,'實體店','',1,0,0,0,0,1,0,0,'',1,0,0,0,0,0.000000,0,0),(102,102,0,'當面交易','',0,0,0,0,0,1,0,0,'',1,1,0,0,0,0.000000,3,0),(103,103,0,'7-11 超商純取貨','',1,0,0,1,1,0,0,1,'smilepay_c2cup',2,2,45,30,30,5.000000,0,0),(104,104,0,'7-11 超商取貨付款','',1,0,0,1,1,0,0,1,'smilepay_c2c',2,3,45,30,30,5.000000,0,0),(105,105,0,'全家超商純取貨','',1,0,0,1,1,0,0,1,'smilepay_c2cup',2,4,45,30,30,5.000000,0,0),(106,106,0,'全家超商取貨付款','',1,0,0,1,1,0,0,1,'smilepay_c2c',2,5,45,30,30,5.000000,0,0),(107,107,0,'黑貓宅配(常溫)','',1,0,0,0,1,0,0,1,'smilepay_ezcatup',2,6,0,0,0,20.000000,1,1),(108,108,0,'黑貓宅配(冷藏)','',1,0,0,0,1,0,0,1,'smilepay_ezcatup',2,7,0,0,0,20.000000,1,1),(109,109,0,'黑貓宅配(冷凍)','',1,0,0,0,1,0,0,1,'smilepay_ezcatup',2,8,0,0,0,20.000000,1,1),(110,110,0,'黑貓貨到付現(常溫)','',1,0,0,0,1,0,0,1,'smilepay_ezcat',2,9,0,0,0,20.000000,2,1),(111,111,0,'黑貓貨到付現(冷藏)','',1,0,0,0,1,0,0,1,'smilepay_ezcat',2,10,0,0,0,0.000000,2,1),(112,112,0,'黑貓貨到付現(冷凍)','',1,0,0,0,1,0,0,1,'smilepay_ezcat',2,11,0,0,0,20.000000,2,1),(113,113,0,'掌櫃櫃取貨(常溫)','',1,0,0,0,1,0,0,1,'smilepay_palmboxc2cup',2,12,31,34,41,20.000000,2,0),(114,114,0,'郵局','',1,0,0,0,0,0,0,0,'',2,13,0,0,0,0.000000,1,1);
/*!40000 ALTER TABLE `ps_carrier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_carrier_group`
--

DROP TABLE IF EXISTS `ps_carrier_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_carrier_group` (
  `id_carrier` int(10) unsigned NOT NULL,
  `id_group` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_carrier`,`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_carrier_group`
--

LOCK TABLES `ps_carrier_group` WRITE;
/*!40000 ALTER TABLE `ps_carrier_group` DISABLE KEYS */;
INSERT INTO `ps_carrier_group` VALUES (101,1),(101,2),(101,3),(102,1),(102,2),(102,3),(103,1),(103,2),(103,3),(104,1),(104,2),(104,3),(105,1),(105,2),(105,3),(106,1),(106,2),(106,3),(107,1),(107,2),(107,3),(108,1),(108,2),(108,3),(109,1),(109,2),(109,3),(110,1),(110,2),(110,3),(111,1),(111,2),(111,3),(112,1),(112,2),(112,3),(113,1),(113,2),(113,3),(114,1),(114,2),(114,3);
/*!40000 ALTER TABLE `ps_carrier_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_carrier_lang`
--

DROP TABLE IF EXISTS `ps_carrier_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_carrier_lang` (
  `id_carrier` int(10) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_lang` int(10) unsigned NOT NULL,
  `delay` varchar(512) DEFAULT NULL,
  `shipped_email_info` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '已出貨通知信補充資訊',
  PRIMARY KEY (`id_lang`,`id_shop`,`id_carrier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_carrier_lang`
--

LOCK TABLES `ps_carrier_lang` WRITE;
/*!40000 ALTER TABLE `ps_carrier_lang` DISABLE KEYS */;
INSERT INTO `ps_carrier_lang` VALUES (101,1,1,'店內取貨',''),(102,1,1,'限台北內湖南港，量大可送貨到府',''),(103,1,1,'2-3天','預計2日送達指定門市，取件人需出示身份證件，讓門市人員核對無誤，方可取貨。'),(104,1,1,'2-3天',''),(105,1,1,'2-3天',''),(106,1,1,'2-3天',''),(107,1,1,'2-3天',''),(108,1,1,'2-3天',''),(109,1,1,'2-3天',''),(110,1,1,'2-3天',''),(111,1,1,'2-3天',''),(112,1,1,'2-3天',''),(113,1,1,'2-3天，2018/6/1~12/31優惠期間運費35元',''),(114,1,1,'2-3天',''),(101,1,2,'Pick up in-store',''),(102,1,2,'限台北內湖南港，量大可送貨到府',''),(103,1,2,'2-3 days',''),(104,1,2,'2-3 days',''),(105,1,2,'2-3 days',''),(106,1,2,'2-3 days',''),(107,1,2,'2-3 days',''),(108,1,2,'2-3 days',''),(109,1,2,'2-3 days',''),(110,1,2,'2-3 days',''),(111,1,2,'2-3 days',''),(112,1,2,'2-3 days',''),(113,1,2,'2-3 days，2018/6/1~12/31優惠期間運費35元',''),(114,1,2,'2-3 days',''),(101,1,3,'Pick up in-store',''),(102,1,3,'限台北內湖南港，量大可送貨到府',''),(103,1,3,'2-3 days',''),(104,1,3,'2-3 days',''),(105,1,3,'2-3 days',''),(106,1,3,'2-3 days',''),(107,1,3,'2-3 days',''),(108,1,3,'2-3 days',''),(109,1,3,'2-3 days',''),(110,1,3,'2-3 days',''),(111,1,3,'2-3 days',''),(112,1,3,'2-3 days',''),(113,1,3,'2-3 days，2018/6/1~12/31優惠期間運費35元',''),(114,1,3,'2-3 days',''),(101,1,4,'直接実店舗で受取',''),(102,1,4,'限台北內湖南港，量大可送貨到府',''),(103,1,4,'2-3 days',''),(104,1,4,'2-3 days',''),(105,1,4,'2-3 days',''),(106,1,4,'2-3 days',''),(107,1,4,'2-3 days',''),(108,1,4,'2-3 days',''),(109,1,4,'2-3 days',''),(110,1,4,'2-3 days',''),(111,1,4,'2-3 days',''),(112,1,4,'2-3 days',''),(113,1,4,'2-3 days，2018/6/1~12/31優惠期間運費35元',''),(114,1,4,'2-3 days','');
/*!40000 ALTER TABLE `ps_carrier_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_carrier_shop`
--

DROP TABLE IF EXISTS `ps_carrier_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_carrier_shop` (
  `id_carrier` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_carrier`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_carrier_shop`
--

LOCK TABLES `ps_carrier_shop` WRITE;
/*!40000 ALTER TABLE `ps_carrier_shop` DISABLE KEYS */;
INSERT INTO `ps_carrier_shop` VALUES (101,1),(102,1),(103,1),(104,1),(105,1),(106,1),(107,1),(108,1),(109,1),(110,1),(111,1),(112,1),(113,1),(114,1);
/*!40000 ALTER TABLE `ps_carrier_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_carrier_tax_rules_group_shop`
--

DROP TABLE IF EXISTS `ps_carrier_tax_rules_group_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_carrier_tax_rules_group_shop` (
  `id_carrier` int(11) unsigned NOT NULL,
  `id_tax_rules_group` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_carrier`,`id_tax_rules_group`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_carrier_tax_rules_group_shop`
--

LOCK TABLES `ps_carrier_tax_rules_group_shop` WRITE;
/*!40000 ALTER TABLE `ps_carrier_tax_rules_group_shop` DISABLE KEYS */;
INSERT INTO `ps_carrier_tax_rules_group_shop` VALUES (101,0,1),(102,0,1),(103,0,1),(104,0,1),(105,0,1),(106,0,1),(107,0,1),(108,0,1),(109,0,1),(110,0,1),(111,0,1),(112,0,1),(113,0,1),(114,0,1);
/*!40000 ALTER TABLE `ps_carrier_tax_rules_group_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_carrier_zone`
--

DROP TABLE IF EXISTS `ps_carrier_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_carrier_zone` (
  `id_carrier` int(10) unsigned NOT NULL,
  `id_zone` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_carrier`,`id_zone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_carrier_zone`
--

LOCK TABLES `ps_carrier_zone` WRITE;
/*!40000 ALTER TABLE `ps_carrier_zone` DISABLE KEYS */;
INSERT INTO `ps_carrier_zone` VALUES (101,9),(102,9),(103,9),(104,9),(105,9),(106,9),(107,9),(108,9),(109,9),(110,9),(111,9),(112,9),(113,9),(114,9);
/*!40000 ALTER TABLE `ps_carrier_zone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_cart`
--

DROP TABLE IF EXISTS `ps_cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_cart` (
  `id_cart` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_group` int(11) unsigned NOT NULL DEFAULT '1',
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_carrier` int(10) unsigned NOT NULL,
  `delivery_option` text NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `id_address_delivery` int(10) unsigned NOT NULL,
  `id_address_invoice` int(10) unsigned NOT NULL,
  `id_currency` int(10) unsigned NOT NULL,
  `id_customer` int(10) unsigned NOT NULL,
  `id_guest` int(10) unsigned NOT NULL,
  `secure_key` varchar(32) NOT NULL DEFAULT '-1',
  `recyclable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `gift` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `gift_message` text,
  `mobile_theme` tinyint(1) NOT NULL DEFAULT '0',
  `allow_seperated_package` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `checkout_session_data` mediumtext,
  PRIMARY KEY (`id_cart`),
  KEY `cart_customer` (`id_customer`),
  KEY `id_address_delivery` (`id_address_delivery`),
  KEY `id_address_invoice` (`id_address_invoice`),
  KEY `id_carrier` (`id_carrier`),
  KEY `id_lang` (`id_lang`),
  KEY `id_currency` (`id_currency`),
  KEY `id_guest` (`id_guest`),
  KEY `id_shop_group` (`id_shop_group`),
  KEY `id_shop_2` (`id_shop`,`date_upd`),
  KEY `id_shop` (`id_shop`,`date_add`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_cart`
--

LOCK TABLES `ps_cart` WRITE;
/*!40000 ALTER TABLE `ps_cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_cart_cart_rule`
--

DROP TABLE IF EXISTS `ps_cart_cart_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_cart_cart_rule` (
  `id_cart` int(10) unsigned NOT NULL,
  `id_cart_rule` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_cart`,`id_cart_rule`),
  KEY `id_cart_rule` (`id_cart_rule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_cart_cart_rule`
--

LOCK TABLES `ps_cart_cart_rule` WRITE;
/*!40000 ALTER TABLE `ps_cart_cart_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_cart_cart_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_cart_product`
--

DROP TABLE IF EXISTS `ps_cart_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_cart_product` (
  `id_cart` int(10) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `id_address_delivery` int(10) unsigned NOT NULL DEFAULT '0',
  `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
  `id_product_attribute` int(10) unsigned NOT NULL DEFAULT '0',
  `id_customization` int(10) unsigned NOT NULL DEFAULT '0',
  `quantity` int(10) unsigned NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_cart`,`id_product`,`id_product_attribute`,`id_customization`,`id_address_delivery`),
  KEY `id_product_attribute` (`id_product_attribute`),
  KEY `id_cart_order` (`id_cart`,`date_add`,`id_product`,`id_product_attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_cart_product`
--

LOCK TABLES `ps_cart_product` WRITE;
/*!40000 ALTER TABLE `ps_cart_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_cart_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_cart_rule`
--

DROP TABLE IF EXISTS `ps_cart_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_cart_rule` (
  `id_cart_rule` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_customer` int(10) unsigned NOT NULL DEFAULT '0',
  `date_from` datetime NOT NULL,
  `date_to` datetime NOT NULL,
  `description` text,
  `quantity` int(10) unsigned NOT NULL DEFAULT '0',
  `quantity_per_user` int(10) unsigned NOT NULL DEFAULT '0',
  `priority` int(10) unsigned NOT NULL DEFAULT '1',
  `partial_use` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `code` varchar(254) NOT NULL,
  `minimum_amount` decimal(17,2) NOT NULL DEFAULT '0.00',
  `minimum_amount_tax` tinyint(1) NOT NULL DEFAULT '0',
  `minimum_amount_currency` int(10) unsigned NOT NULL DEFAULT '0',
  `minimum_amount_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `country_restriction` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `carrier_restriction` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `group_restriction` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `cart_rule_restriction` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `product_restriction` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `shop_restriction` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `free_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `reduction_percent` decimal(5,2) NOT NULL DEFAULT '0.00',
  `reduction_amount` decimal(17,2) NOT NULL DEFAULT '0.00',
  `reduction_tax` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `reduction_currency` int(10) unsigned NOT NULL DEFAULT '0',
  `reduction_product` int(10) NOT NULL DEFAULT '0',
  `reduction_exclude_special` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `gift_product` int(10) unsigned NOT NULL DEFAULT '0',
  `gift_product_attribute` int(10) unsigned NOT NULL DEFAULT '0',
  `highlight` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_cart_rule`),
  KEY `id_customer` (`id_customer`,`active`,`date_to`),
  KEY `group_restriction` (`group_restriction`,`active`,`date_to`),
  KEY `id_customer_2` (`id_customer`,`active`,`highlight`,`date_to`),
  KEY `group_restriction_2` (`group_restriction`,`active`,`highlight`,`date_to`),
  KEY `date_from` (`date_from`),
  KEY `date_to` (`date_to`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_cart_rule`
--

LOCK TABLES `ps_cart_rule` WRITE;
/*!40000 ALTER TABLE `ps_cart_rule` DISABLE KEYS */;
INSERT INTO `ps_cart_rule` VALUES (1,0,'2018-07-15 17:00:00','2019-08-31 00:00:00','全館7折限量（ 20 次）優惠，一個客戶只可使用一次。',19,1,1,1,'2018july',0.00,0,1,0,0,0,0,0,0,0,0,30.00,0.00,0,1,0,0,0,0,0,1,'2018-07-15 17:47:33','2018-10-28 22:46:38'),(2,0,'2018-07-28 23:00:00','2019-08-31 00:00:00','可使用總次數 100，單一客戶可使用 1 次。',98,10,1,1,'',0.00,0,1,0,0,0,0,0,1,0,0,15.00,0.00,0,1,-2,0,0,0,0,1,'2018-07-28 23:31:06','2018-10-28 22:42:22'),(3,0,'2018-07-29 09:00:00','2022-12-31 00:00:00','會員專屬，立即加入會員，前 1000 名可獲得優惠折扣碼，現抵 100 元。',999,1,1,1,'newmember',0.00,0,1,0,0,0,1,0,0,0,0,0.00,100.00,0,1,0,0,0,0,0,1,'2018-07-29 09:25:31','2018-10-28 22:46:46'),(4,0,'2018-07-29 09:00:00','2019-08-31 00:00:00','',1,1,1,1,'',0.00,0,1,0,0,0,0,0,1,0,0,20.00,0.00,0,1,-2,0,0,0,0,1,'2018-07-29 09:33:07','2018-10-28 22:52:17'),(5,0,'2018-07-29 09:00:00','2019-08-31 00:00:00','',1,1,1,1,'',0.00,0,1,0,0,0,0,0,1,0,0,0.00,0.00,0,1,-2,0,8,0,0,1,'2018-07-29 09:34:45','2018-10-28 22:51:08');
/*!40000 ALTER TABLE `ps_cart_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_cart_rule_carrier`
--

DROP TABLE IF EXISTS `ps_cart_rule_carrier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_cart_rule_carrier` (
  `id_cart_rule` int(10) unsigned NOT NULL,
  `id_carrier` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_cart_rule`,`id_carrier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_cart_rule_carrier`
--

LOCK TABLES `ps_cart_rule_carrier` WRITE;
/*!40000 ALTER TABLE `ps_cart_rule_carrier` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_cart_rule_carrier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_cart_rule_combination`
--

DROP TABLE IF EXISTS `ps_cart_rule_combination`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_cart_rule_combination` (
  `id_cart_rule_1` int(10) unsigned NOT NULL,
  `id_cart_rule_2` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_cart_rule_1`,`id_cart_rule_2`),
  KEY `id_cart_rule_1` (`id_cart_rule_1`),
  KEY `id_cart_rule_2` (`id_cart_rule_2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_cart_rule_combination`
--

LOCK TABLES `ps_cart_rule_combination` WRITE;
/*!40000 ALTER TABLE `ps_cart_rule_combination` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_cart_rule_combination` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_cart_rule_country`
--

DROP TABLE IF EXISTS `ps_cart_rule_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_cart_rule_country` (
  `id_cart_rule` int(10) unsigned NOT NULL,
  `id_country` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_cart_rule`,`id_country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_cart_rule_country`
--

LOCK TABLES `ps_cart_rule_country` WRITE;
/*!40000 ALTER TABLE `ps_cart_rule_country` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_cart_rule_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_cart_rule_group`
--

DROP TABLE IF EXISTS `ps_cart_rule_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_cart_rule_group` (
  `id_cart_rule` int(10) unsigned NOT NULL,
  `id_group` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_cart_rule`,`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_cart_rule_group`
--

LOCK TABLES `ps_cart_rule_group` WRITE;
/*!40000 ALTER TABLE `ps_cart_rule_group` DISABLE KEYS */;
INSERT INTO `ps_cart_rule_group` VALUES (3,3);
/*!40000 ALTER TABLE `ps_cart_rule_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_cart_rule_lang`
--

DROP TABLE IF EXISTS `ps_cart_rule_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_cart_rule_lang` (
  `id_cart_rule` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(254) NOT NULL,
  PRIMARY KEY (`id_cart_rule`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_cart_rule_lang`
--

LOCK TABLES `ps_cart_rule_lang` WRITE;
/*!40000 ALTER TABLE `ps_cart_rule_lang` DISABLE KEYS */;
INSERT INTO `ps_cart_rule_lang` VALUES (1,1,'全館優惠折扣碼 7折 限 20 名'),(1,2,'折扣'),(1,3,'折扣'),(1,4,'折扣'),(2,1,'任選「移動記」 3 件 85 折'),(2,2,'示範 - 任選 3 件 85 折'),(2,3,'示範 - 任選 3 件 85 折'),(2,4,'示範 - 任選 3 件 85 折'),(3,1,'「加入會員」優惠券'),(3,2,'加入會員贈送優惠折扣碼 newmember'),(3,3,'加入會員贈送優惠折扣碼 newmember'),(3,4,'加入會員贈送優惠折扣碼 newmember'),(4,1,'任選「清水斷崖」分類商品 滿 2 件 8 折'),(4,2,'示範 - 指定分類多階層 滿1件8折'),(4,3,'示範 - 指定分類多階層 滿1件8折'),(4,4,'示範 - 指定分類多階層 滿1件8折'),(5,1,'任選「清水斷崖」分類商品 滿 5 件 送「七星潭」明信片'),(5,2,'示範 - 多階層 指定分類 滿 5 件 6 折'),(5,3,'示範 - 多階層 指定分類 滿 5 件 6 折'),(5,4,'示範 - 多階層 指定分類 滿 5 件 6 折');
/*!40000 ALTER TABLE `ps_cart_rule_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_cart_rule_product_rule`
--

DROP TABLE IF EXISTS `ps_cart_rule_product_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_cart_rule_product_rule` (
  `id_product_rule` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_product_rule_group` int(10) unsigned NOT NULL,
  `type` enum('products','categories','attributes','manufacturers','suppliers') NOT NULL,
  PRIMARY KEY (`id_product_rule`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_cart_rule_product_rule`
--

LOCK TABLES `ps_cart_rule_product_rule` WRITE;
/*!40000 ALTER TABLE `ps_cart_rule_product_rule` DISABLE KEYS */;
INSERT INTO `ps_cart_rule_product_rule` VALUES (54,54,'products'),(62,62,'categories'),(64,64,'categories');
/*!40000 ALTER TABLE `ps_cart_rule_product_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_cart_rule_product_rule_group`
--

DROP TABLE IF EXISTS `ps_cart_rule_product_rule_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_cart_rule_product_rule_group` (
  `id_product_rule_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cart_rule` int(10) unsigned NOT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_product_rule_group`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_cart_rule_product_rule_group`
--

LOCK TABLES `ps_cart_rule_product_rule_group` WRITE;
/*!40000 ALTER TABLE `ps_cart_rule_product_rule_group` DISABLE KEYS */;
INSERT INTO `ps_cart_rule_product_rule_group` VALUES (54,2,3),(62,5,5),(64,4,2);
/*!40000 ALTER TABLE `ps_cart_rule_product_rule_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_cart_rule_product_rule_value`
--

DROP TABLE IF EXISTS `ps_cart_rule_product_rule_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_cart_rule_product_rule_value` (
  `id_product_rule` int(10) unsigned NOT NULL,
  `id_item` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_product_rule`,`id_item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_cart_rule_product_rule_value`
--

LOCK TABLES `ps_cart_rule_product_rule_value` WRITE;
/*!40000 ALTER TABLE `ps_cart_rule_product_rule_value` DISABLE KEYS */;
INSERT INTO `ps_cart_rule_product_rule_value` VALUES (54,1),(54,2),(54,3),(54,4),(62,17),(64,17);
/*!40000 ALTER TABLE `ps_cart_rule_product_rule_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_cart_rule_shop`
--

DROP TABLE IF EXISTS `ps_cart_rule_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_cart_rule_shop` (
  `id_cart_rule` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_cart_rule`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_cart_rule_shop`
--

LOCK TABLES `ps_cart_rule_shop` WRITE;
/*!40000 ALTER TABLE `ps_cart_rule_shop` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_cart_rule_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_category`
--

DROP TABLE IF EXISTS `ps_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_category` (
  `id_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_parent` int(10) unsigned NOT NULL,
  `id_shop_default` int(10) unsigned NOT NULL DEFAULT '1',
  `level_depth` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `nleft` int(10) unsigned NOT NULL DEFAULT '0',
  `nright` int(10) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `is_root_category` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_category`),
  KEY `category_parent` (`id_parent`),
  KEY `nleftrightactive` (`nleft`,`nright`,`active`),
  KEY `level_depth` (`level_depth`),
  KEY `nright` (`nright`),
  KEY `activenleft` (`active`,`nleft`),
  KEY `activenright` (`active`,`nright`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_category`
--

LOCK TABLES `ps_category` WRITE;
/*!40000 ALTER TABLE `ps_category` DISABLE KEYS */;
INSERT INTO `ps_category` VALUES (1,0,1,0,1,28,1,'2018-07-11 22:32:49','2018-07-11 22:32:49',0,0),(2,1,1,1,2,27,1,'2018-07-11 22:32:49','2018-07-20 19:44:08',0,1),(7,2,1,2,3,6,1,'2018-07-11 22:32:49','2018-10-10 19:58:47',0,0),(8,2,1,2,7,16,1,'2018-07-11 22:32:49','2018-09-23 15:29:26',1,0),(9,8,1,3,8,9,1,'2018-07-11 22:32:49','2018-07-13 17:39:07',0,0),(10,8,1,3,10,11,1,'2018-07-11 22:32:49','2018-07-13 17:39:58',1,0),(11,8,1,3,12,13,1,'2018-07-11 22:32:49','2018-07-13 17:41:02',2,0),(12,8,1,3,14,15,1,'2018-07-11 22:32:49','2018-07-13 17:41:46',3,0),(13,7,1,3,4,5,1,'2018-07-11 22:32:49','2018-12-09 23:24:35',0,0),(14,2,1,2,17,26,1,'2018-07-11 22:32:49','2018-09-23 15:29:26',2,0),(15,14,1,3,18,23,1,'2018-07-11 22:32:49','2018-07-13 17:45:43',0,0),(16,15,1,4,19,20,1,'2018-07-11 22:32:49','2018-07-13 17:47:03',0,0),(17,15,1,4,21,22,1,'2018-07-11 22:32:49','2018-07-13 17:48:50',1,0),(18,14,1,3,24,25,1,'2018-07-11 22:32:49','2018-07-13 17:46:36',1,0);
/*!40000 ALTER TABLE `ps_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_category_group`
--

DROP TABLE IF EXISTS `ps_category_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_category_group` (
  `id_category` int(10) unsigned NOT NULL,
  `id_group` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_category`,`id_group`),
  KEY `id_category` (`id_category`),
  KEY `id_group` (`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_category_group`
--

LOCK TABLES `ps_category_group` WRITE;
/*!40000 ALTER TABLE `ps_category_group` DISABLE KEYS */;
INSERT INTO `ps_category_group` VALUES (2,1),(2,2),(2,3),(7,1),(7,2),(7,3),(8,1),(8,2),(8,3),(9,1),(9,2),(9,3),(10,1),(10,2),(10,3),(11,1),(11,2),(11,3),(12,1),(12,2),(12,3),(13,1),(13,2),(13,3),(14,1),(14,2),(14,3),(15,1),(15,2),(15,3),(16,1),(16,2),(16,3),(17,1),(17,2),(17,3),(18,1),(18,2),(18,3);
/*!40000 ALTER TABLE `ps_category_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_category_lang`
--

DROP TABLE IF EXISTS `ps_category_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_category_lang` (
  `id_category` int(10) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `link_rewrite` varchar(128) NOT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_category`,`id_shop`,`id_lang`),
  KEY `category_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_category_lang`
--

LOCK TABLES `ps_category_lang` WRITE;
/*!40000 ALTER TABLE `ps_category_lang` DISABLE KEYS */;
INSERT INTO `ps_category_lang` VALUES (1,1,1,'Root','','root','','',''),(1,1,2,'Root','','root','','',''),(1,1,3,'Root','','root','','',''),(1,1,4,'ルート','','ルート','','',''),(1,2,1,'Root','','root','','',''),(1,2,2,'Root','','root','','',''),(1,2,3,'Root','','root','','',''),(1,2,4,'ルート','','ルート','','',''),(1,3,1,'Root','','root','','',''),(1,3,2,'Root','','root','','',''),(1,3,3,'Root','','root','','',''),(1,3,4,'ルート','','ルート','','',''),(1,4,1,'Root','','root','','',''),(1,4,2,'Root','','root','','',''),(1,4,3,'Root','','root','','',''),(1,4,4,'ルート','','ルート','','',''),(2,1,1,'所有商品','','all','','',''),(2,1,2,'All Products','','all','','',''),(2,1,3,'所有商品','','all','','',''),(2,1,4,'All Products','','all','','',''),(2,2,1,'所有商品','','all','','',''),(2,2,2,'All Products','','all','','',''),(2,2,3,'所有商品','','all','','',''),(2,2,4,'All Products','','all','','',''),(2,3,1,'所有商品','','all','','',''),(2,3,2,'All Products','','all','','',''),(2,3,3,'所有商品','','all','','',''),(2,3,4,'All Products','','all','','',''),(2,4,1,'所有商品','','all','','',''),(2,4,2,'All Products','','all','','',''),(2,4,3,'所有商品','','all','','',''),(2,4,4,'All Products','','all','','',''),(7,1,1,'作品','','project','','',''),(7,1,2,'Project','','project','','',''),(7,1,3,'作品','','project','','',''),(7,1,4,'Project','','project','','',''),(7,2,1,'作品','','project','','',''),(7,2,2,'Project','','project','','',''),(7,2,3,'作品','','project','','',''),(7,2,4,'Project','','project','','',''),(7,3,1,'作品','','project','','',''),(7,3,2,'Project','','project','','',''),(7,3,3,'作品','','project','','',''),(7,3,4,'Project','','project','','',''),(7,4,1,'作品','','project','','',''),(7,4,2,'Project','','project','','',''),(7,4,3,'作品','','project','','',''),(7,4,4,'Project','','project','','',''),(8,1,1,'衍生品','','souvenir','','',''),(8,1,2,'Souvenir','','souvenir','','',''),(8,1,3,'衍生品','','souvenir','','',''),(8,1,4,'Souvenir','','souvenir','','',''),(8,2,1,'衍生品','','souvenir','','',''),(8,2,2,'Souvenir','','souvenir','','',''),(8,2,3,'衍生品','','souvenir','','',''),(8,2,4,'Souvenir','','souvenir','','',''),(8,3,1,'衍生品','','souvenir','','',''),(8,3,2,'Souvenir','','souvenir','','',''),(8,3,3,'衍生品','','souvenir','','',''),(8,3,4,'Souvenir','','souvenir','','',''),(8,4,1,'衍生品','','souvenir','','',''),(8,4,2,'Souvenir','','souvenir','','',''),(8,4,3,'衍生品','','souvenir','','',''),(8,4,4,'Souvenir','','souvenir','','',''),(9,1,1,'明信片','','postcard','','',''),(9,1,2,'Postcard','','postcard','','',''),(9,1,3,'明信片','','postcard','','',''),(9,1,4,'Postcard','','postcard','','',''),(9,2,1,'明信片','','postcard','','',''),(9,2,2,'Postcard','','postcard','','',''),(9,2,3,'明信片','','postcard','','',''),(9,2,4,'Postcard','','postcard','','',''),(9,3,1,'明信片','','postcard','','',''),(9,3,2,'Postcard','','postcard','','',''),(9,3,3,'明信片','','postcard','','',''),(9,3,4,'Postcard','','postcard','','',''),(9,4,1,'明信片','','postcard','','',''),(9,4,2,'Postcard','','postcard','','',''),(9,4,3,'明信片','','postcard','','',''),(9,4,4,'Postcard','','postcard','','',''),(10,1,1,'紙膠帶','','tape','','',''),(10,1,2,'Tape','','tape','','',''),(10,1,3,'纸胶带','','tape','','',''),(10,1,4,'Tape','','tape','','',''),(10,2,1,'紙膠帶','','tape','','',''),(10,2,2,'Tape','','tape','','',''),(10,2,3,'纸胶带','','tape','','',''),(10,2,4,'Tape','','tape','','',''),(10,3,1,'紙膠帶','','tape','','',''),(10,3,2,'Tape','','tape','','',''),(10,3,3,'纸胶带','','tape','','',''),(10,3,4,'Tape','','tape','','',''),(10,4,1,'紙膠帶','','tape','','',''),(10,4,2,'Tape','','tape','','',''),(10,4,3,'纸胶带','','tape','','',''),(10,4,4,'Tape','','tape','','',''),(11,1,1,'瑜珈褲','','leggings','','',''),(11,1,2,'Leggings','','leggings','','',''),(11,1,3,'瑜珈裤','','leggings','','',''),(11,1,4,'Leggings','','leggings','','',''),(11,2,1,'瑜珈褲','','leggings','','',''),(11,2,2,'Leggings','','leggings','','',''),(11,2,3,'瑜珈裤','','leggings','','',''),(11,2,4,'Leggings','','leggings','','',''),(11,3,1,'瑜珈褲','','leggings','','',''),(11,3,2,'Leggings','','leggings','','',''),(11,3,3,'瑜珈裤','','leggings','','',''),(11,3,4,'Leggings','','leggings','','',''),(11,4,1,'瑜珈褲','','leggings','','',''),(11,4,2,'Leggings','','leggings','','',''),(11,4,3,'瑜珈裤','','leggings','','',''),(11,4,4,'Leggings','','leggings','','',''),(12,1,1,'戒指','','ring','','',''),(12,1,2,'Ring','','ring','','',''),(12,1,3,'戒指','','ring','','',''),(12,1,4,'Ring','','ring','','',''),(12,2,1,'戒指','','ring','','',''),(12,2,2,'Ring','','ring','','',''),(12,2,3,'戒指','','ring','','',''),(12,2,4,'Ring','','ring','','',''),(12,3,1,'戒指','','ring','','',''),(12,3,2,'Ring','','ring','','',''),(12,3,3,'戒指','','ring','','',''),(12,3,4,'Ring','','ring','','',''),(12,4,1,'戒指','','ring','','',''),(12,4,2,'Ring','','ring','','',''),(12,4,3,'戒指','','ring','','',''),(12,4,4,'Ring','','ring','','',''),(13,1,1,'藝術家的書','','artistsbook','','',''),(13,1,2,'Artists Book','','artistsbook','','',''),(13,1,3,'艺术家的书','','artistsbook','','',''),(13,1,4,'Artists Book','','artistsbook','','',''),(13,2,1,'藝術家的書','','artistsbook','','',''),(13,2,2,'Artists Book','','artistsbook','','',''),(13,2,3,'艺术家的书','','artistsbook','','',''),(13,2,4,'Artists Book','','artistsbook','','',''),(13,3,1,'藝術家的書','','artistsbook','','',''),(13,3,2,'Artists Book','','artistsbook','','',''),(13,3,3,'艺术家的书','','artistsbook','','',''),(13,3,4,'Artists Book','','artistsbook','','',''),(13,4,1,'藝術家的書','','artistsbook','','',''),(13,4,2,'Artists Book','','artistsbook','','',''),(13,4,3,'艺术家的书','','artistsbook','','',''),(13,4,4,'Artists Book','','artistsbook','','',''),(14,1,1,'台灣','','taiwan','','',''),(14,1,2,'Taiwan','','taiwan','','',''),(14,1,3,'台湾','','taiwan','','',''),(14,1,4,'台湾','','taiwan','','',''),(14,2,1,'台灣','','taiwan','','',''),(14,2,2,'Taiwan','','taiwan','','',''),(14,2,3,'台湾','','taiwan','','',''),(14,2,4,'台湾','','taiwan','','',''),(14,3,1,'台灣','','taiwan','','',''),(14,3,2,'Taiwan','','taiwan','','',''),(14,3,3,'台湾','','taiwan','','',''),(14,3,4,'台湾','','taiwan','','',''),(14,4,1,'台灣','','taiwan','','',''),(14,4,2,'Taiwan','','taiwan','','',''),(14,4,3,'台湾','','taiwan','','',''),(14,4,4,'台湾','','taiwan','','',''),(15,1,1,'花蓮','','hualien','','',''),(15,1,2,'Hualien','<p><span style=\"color:#444444;font-family:\'Helvetica Neue\', Helvetica, Arial, \'PingFang TC\', \'Heiti TC\', \'微軟正黑體\', \'Microsoft JhengHei\', \'Noto Sans T Chinese\', \'Droid Sans Fallback\', sans-serif;font-size:14px;font-weight:600;\">▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓</span></p>','hualien','','',''),(15,1,3,'花莲','','hualien','','',''),(15,1,4,'花蓮','','hualien','','',''),(15,2,1,'花蓮','','hualien','','',''),(15,2,2,'Hualien','<p><span style=\"color:#444444;font-family:\'Helvetica Neue\', Helvetica, Arial, \'PingFang TC\', \'Heiti TC\', \'微軟正黑體\', \'Microsoft JhengHei\', \'Noto Sans T Chinese\', \'Droid Sans Fallback\', sans-serif;font-size:14px;font-weight:600;\">▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓</span></p>','hualien','','',''),(15,2,3,'花莲','','hualien','','',''),(15,2,4,'花蓮','','hualien','','',''),(15,3,1,'花蓮','','hualien','','',''),(15,3,2,'Hualien','<p><span style=\"color:#444444;font-family:\'Helvetica Neue\', Helvetica, Arial, \'PingFang TC\', \'Heiti TC\', \'微軟正黑體\', \'Microsoft JhengHei\', \'Noto Sans T Chinese\', \'Droid Sans Fallback\', sans-serif;font-size:14px;font-weight:600;\">▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓</span></p>','hualien','','',''),(15,3,3,'花莲','','hualien','','',''),(15,3,4,'花蓮','','hualien','','',''),(15,4,1,'花蓮','','hualien','','',''),(15,4,2,'Hualien','<p><span style=\"color:#444444;font-family:\'Helvetica Neue\', Helvetica, Arial, \'PingFang TC\', \'Heiti TC\', \'微軟正黑體\', \'Microsoft JhengHei\', \'Noto Sans T Chinese\', \'Droid Sans Fallback\', sans-serif;font-size:14px;font-weight:600;\">▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓</span></p>','hualien','','',''),(15,4,3,'花莲','','hualien','','',''),(15,4,4,'花蓮','','hualien','','',''),(16,1,1,'七星潭','','chihsingtan','','',''),(16,1,2,'Chihsingtan','','chihsingtan','','',''),(16,1,3,'七星潭','','chihsingtan','','',''),(16,1,4,'七星潭','','chihsingtan','','',''),(16,2,1,'七星潭','','chihsingtan','','',''),(16,2,2,'Chihsingtan','','chihsingtan','','',''),(16,2,3,'七星潭','','chihsingtan','','',''),(16,2,4,'七星潭','','chihsingtan','','',''),(16,3,1,'七星潭','','chihsingtan','','',''),(16,3,2,'Chihsingtan','','chihsingtan','','',''),(16,3,3,'七星潭','','chihsingtan','','',''),(16,3,4,'七星潭','','chihsingtan','','',''),(16,4,1,'七星潭','','chihsingtan','','',''),(16,4,2,'Chihsingtan','','chihsingtan','','',''),(16,4,3,'七星潭','','chihsingtan','','',''),(16,4,4,'七星潭','','chihsingtan','','',''),(17,1,1,'清水斷崖','','chingshuicliff','','',''),(17,1,2,'Chingshui Cliff','','chingshuicliff','','',''),(17,1,3,'清水断崖','','chingshuicliff','','',''),(17,1,4,'清水斷崖','','chingshuicliff','','',''),(17,2,1,'清水斷崖','','chingshuicliff','','',''),(17,2,2,'Chingshui Cliff','','chingshuicliff','','',''),(17,2,3,'清水断崖','','chingshuicliff','','',''),(17,2,4,'清水斷崖','','chingshuicliff','','',''),(17,3,1,'清水斷崖','','chingshuicliff','','',''),(17,3,2,'Chingshui Cliff','','chingshuicliff','','',''),(17,3,3,'清水断崖','','chingshuicliff','','',''),(17,3,4,'清水斷崖','','chingshuicliff','','',''),(17,4,1,'清水斷崖','','chingshuicliff','','',''),(17,4,2,'Chingshui Cliff','','chingshuicliff','','',''),(17,4,3,'清水断崖','','chingshuicliff','','',''),(17,4,4,'清水斷崖','','chingshuicliff','','',''),(18,1,1,'九份','','九份','','',''),(18,1,2,'Jiufen','','jiufen','','',''),(18,1,3,'九份','','jiufen','','',''),(18,1,4,'九份','','jiufen','','',''),(18,2,1,'九份','','九份','','',''),(18,2,2,'Jiufen','','jiufen','','',''),(18,2,3,'九份','','jiufen','','',''),(18,2,4,'九份','','jiufen','','',''),(18,3,1,'九份','','九份','','',''),(18,3,2,'Jiufen','','jiufen','','',''),(18,3,3,'九份','','jiufen','','',''),(18,3,4,'九份','','jiufen','','',''),(18,4,1,'九份','','九份','','',''),(18,4,2,'Jiufen','','jiufen','','',''),(18,4,3,'九份','','jiufen','','',''),(18,4,4,'九份','','jiufen','','','');
/*!40000 ALTER TABLE `ps_category_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_category_product`
--

DROP TABLE IF EXISTS `ps_category_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_category_product` (
  `id_category` int(10) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_category`,`id_product`),
  KEY `id_product` (`id_product`),
  KEY `id_category` (`id_category`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_category_product`
--

LOCK TABLES `ps_category_product` WRITE;
/*!40000 ALTER TABLE `ps_category_product` DISABLE KEYS */;
INSERT INTO `ps_category_product` VALUES (2,1,1),(2,2,2),(2,3,3),(2,4,4),(2,5,5),(2,6,6),(2,8,7),(2,9,8),(2,10,9),(2,11,10),(2,12,11),(2,13,12),(2,14,13),(2,15,14),(2,16,15),(9,6,0),(9,8,1),(10,9,0),(10,10,1),(10,11,2),(10,12,3),(11,13,0),(12,14,0),(12,15,1),(13,1,0),(13,2,1),(13,3,2),(13,4,3),(13,5,4),(16,3,0),(16,8,1),(16,9,2),(16,10,3),(16,14,4),(16,15,5),(17,6,0),(17,1,1),(17,3,2),(17,4,3),(17,5,4),(17,13,5),(18,11,0),(18,12,1),(18,2,2);
/*!40000 ALTER TABLE `ps_category_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_category_shop`
--

DROP TABLE IF EXISTS `ps_category_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_category_shop` (
  `id_category` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_category`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_category_shop`
--

LOCK TABLES `ps_category_shop` WRITE;
/*!40000 ALTER TABLE `ps_category_shop` DISABLE KEYS */;
INSERT INTO `ps_category_shop` VALUES (1,1,0),(1,2,0),(1,3,0),(1,4,0),(2,1,0),(2,2,0),(2,3,0),(2,4,0),(7,1,0),(8,1,1),(9,1,0),(10,1,1),(11,1,2),(12,1,3),(13,1,0),(14,1,2),(15,1,0),(16,1,0),(17,1,1),(18,1,1);
/*!40000 ALTER TABLE `ps_category_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_cms`
--

DROP TABLE IF EXISTS `ps_cms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_cms` (
  `id_cms` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cms_category` int(10) unsigned NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `indexation` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_cms`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_cms`
--

LOCK TABLES `ps_cms` WRITE;
/*!40000 ALTER TABLE `ps_cms` DISABLE KEYS */;
INSERT INTO `ps_cms` VALUES (1,1,3,1,1),(2,1,5,1,1),(3,1,2,1,1),(4,1,0,1,1),(5,1,1,1,1),(6,1,4,1,1),(7,2,0,1,0);
/*!40000 ALTER TABLE `ps_cms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_cms_category`
--

DROP TABLE IF EXISTS `ps_cms_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_cms_category` (
  `id_cms_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_parent` int(10) unsigned NOT NULL,
  `level_depth` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_cms_category`),
  KEY `category_parent` (`id_parent`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_cms_category`
--

LOCK TABLES `ps_cms_category` WRITE;
/*!40000 ALTER TABLE `ps_cms_category` DISABLE KEYS */;
INSERT INTO `ps_cms_category` VALUES (1,0,1,1,'2018-07-11 11:57:33','2018-07-11 11:57:33',0),(2,1,2,1,'2018-10-09 22:35:30','2018-10-09 22:35:30',0);
/*!40000 ALTER TABLE `ps_cms_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_cms_category_lang`
--

DROP TABLE IF EXISTS `ps_cms_category_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_cms_category_lang` (
  `id_cms_category` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
  `name` varchar(128) NOT NULL,
  `description` text,
  `link_rewrite` varchar(128) NOT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_cms_category`,`id_shop`,`id_lang`),
  KEY `category_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_cms_category_lang`
--

LOCK TABLES `ps_cms_category_lang` WRITE;
/*!40000 ALTER TABLE `ps_cms_category_lang` DISABLE KEYS */;
INSERT INTO `ps_cms_category_lang` VALUES (1,1,1,'首頁','','首頁','','',''),(1,2,1,'首页','','首页','','',''),(1,3,1,'首页','','首页','','',''),(1,4,1,'首页','','首页','','',''),(1,1,2,'首頁','','首頁','','',''),(1,2,2,'首页','','首页','','',''),(1,3,2,'首页','','首页','','',''),(1,4,2,'首页','','首页','','',''),(1,1,3,'首頁','','首頁','','',''),(1,2,3,'首页','','首页','','',''),(1,3,3,'首页','','首页','','',''),(1,4,3,'首页','','首页','','',''),(1,1,4,'首頁','','首頁','','',''),(1,2,4,'首页','','首页','','',''),(1,3,4,'首页','','首页','','',''),(1,4,4,'首页','','首页','','',''),(2,1,1,'頁面分類1','','cat1','','',''),(2,2,1,'頁面分類1','','cat1','','',''),(2,3,1,'頁面分類1','','cat1','','',''),(2,4,1,'頁面分類1','','cat1','','','');
/*!40000 ALTER TABLE `ps_cms_category_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_cms_category_shop`
--

DROP TABLE IF EXISTS `ps_cms_category_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_cms_category_shop` (
  `id_cms_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_cms_category`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_cms_category_shop`
--

LOCK TABLES `ps_cms_category_shop` WRITE;
/*!40000 ALTER TABLE `ps_cms_category_shop` DISABLE KEYS */;
INSERT INTO `ps_cms_category_shop` VALUES (1,1),(2,1),(1,2),(1,3),(1,4);
/*!40000 ALTER TABLE `ps_cms_category_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_cms_lang`
--

DROP TABLE IF EXISTS `ps_cms_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_cms_lang` (
  `id_cms` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
  `meta_title` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `link_rewrite` varchar(128) NOT NULL,
  PRIMARY KEY (`id_cms`,`id_shop`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_cms_lang`
--

LOCK TABLES `ps_cms_lang` WRITE;
/*!40000 ALTER TABLE `ps_cms_lang` DISABLE KEYS */;
INSERT INTO `ps_cms_lang` VALUES (1,1,1,'購物說明','','','<h1>購物說明</h1>\n<h2>購物方式</h2>\n<p>《商店名稱》提供方便快速的購物方式。無需加入會員即可直接購物，也可以使用『Facebook 登入』綁定帳號，方便查詢訂單。</p>\n<h2>購物流程</h2>\n<p>選購商品　⇨　加入購物車　⇨　填寫資料　⇨　選擇配送方式　⇨　選擇付款方式　⇨　送出訂單</p>\n<h2>付款方式</h2>\n<ol>\n<li>ATM 轉帳（請於3日內付款）</li>\n<li>超商代碼繳費（請於7日內付款）</li>\n<li>貨到付款</li>\n</ol>\n<h2>運送方式＆運費規則</h2>\n<div style=\"min-width:700px;\">\n<table class=\"table table-striped table-bordered\">\n<tbody>\n<tr><th>運送方式</th><th>配送天數</th><th>適用地區</th><th>運費</th><th>免運門檻</th></tr>\n<tr>\n<td>實體店</td>\n<td></td>\n<td></td>\n<td>免運費</td>\n<td></td>\n</tr>\n<tr>\n<td>當面交易</td>\n<td></td>\n<td></td>\n<td>免運費</td>\n<td></td>\n</tr>\n<tr>\n<td>7-11 超商純取貨</td>\n<td>2-3天</td>\n<td>台灣本島</td>\n<td>$999 以下 = $60<br /> $1000~3999 = $30<br /> $4000 以上 停用</td>\n<td></td>\n</tr>\n<tr>\n<td>7-11 超商取貨付款</td>\n<td>2-3天</td>\n<td>台灣本島</td>\n<td>$999 以下 = $60<br /> $1000~19999 = $30<br /> $20000 以上 停用</td>\n<td></td>\n</tr>\n<tr>\n<td>全家超商純取貨</td>\n<td>2-3天</td>\n<td>台灣本島</td>\n<td>$999 以下 = $60<br /> $1000~3999 = $30<br /> $4000 以上 停用</td>\n<td></td>\n</tr>\n<tr>\n<td>全家超商取貨付款</td>\n<td>2-3天</td>\n<td>台灣本島</td>\n<td>$999 以下 = $60<br /> $1000~19999 = $30<br /> $20000 以上 停用</td>\n<td></td>\n</tr>\n<tr>\n<td>黑貓宅配(常溫)</td>\n<td>2-3天</td>\n<td>台灣本島</td>\n<td>$999 以下 = $150<br /> $1000~3499 = $120</td>\n<td>$3500</td>\n</tr>\n<tr>\n<td>黑貓宅配(冷藏/冷凍)</td>\n<td>2-3天</td>\n<td>台灣本島</td>\n<td>$999 以下 = $190<br /> $1000~3499 = $160</td>\n<td>$3500</td>\n</tr>\n<tr>\n<td>黑貓貨到付現(常溫)</td>\n<td>2-3天</td>\n<td>台灣本島</td>\n<td>$999 以下 = $180<br /> $1000~3499 = $150</td>\n<td>$3500</td>\n</tr>\n<tr>\n<td>黑貓貨到付現(冷藏/冷凍)</td>\n<td>2-3天</td>\n<td>台灣本島</td>\n<td>$999 以下 = $210<br /> $1000~3499 = $180</td>\n<td>$3500</td>\n</tr>\n</tbody>\n</table>\n</div>','guide'),(1,2,1,'Guide','','','<h2>Shipments and returns</h2>\n<h3>Your pack shipment</h3>\n<p>Packages are generally dispatched within 2 days after receipt of payment and are shipped via UPS with tracking and drop-off without signature. If you prefer delivery by UPS Extra with required signature, an additional cost will be applied, so please contact us before choosing this method. Whichever shipment choice you make, we will provide you with a link to track your package online.</p>\n<p>Shipping fees include handling and packing fees as well as postage costs. Handling fees are fixed, whereas transport fees vary according to total weight of the shipment. We advise you to group your items in one order. We cannot group two distinct orders placed separately, and shipping fees will apply to each of them. Your package will be dispatched at your own risk, but special care is taken to protect fragile objects.<br /><br />Boxes are amply sized and your items are well-protected.</p>','guide'),(1,3,1,'Guide','','','<h2>Shipments and returns</h2>\n<h3>Your pack shipment</h3>\n<p>Packages are generally dispatched within 2 days after receipt of payment and are shipped via UPS with tracking and drop-off without signature. If you prefer delivery by UPS Extra with required signature, an additional cost will be applied, so please contact us before choosing this method. Whichever shipment choice you make, we will provide you with a link to track your package online.</p>\n<p>Shipping fees include handling and packing fees as well as postage costs. Handling fees are fixed, whereas transport fees vary according to total weight of the shipment. We advise you to group your items in one order. We cannot group two distinct orders placed separately, and shipping fees will apply to each of them. Your package will be dispatched at your own risk, but special care is taken to protect fragile objects.<br /><br />Boxes are amply sized and your items are well-protected.</p>','delivery'),(1,4,1,'Guide','','','<h2>Shipments and returns</h2>\n<h3>Your pack shipment</h3>\n<p>Packages are generally dispatched within 2 days after receipt of payment and are shipped via UPS with tracking and drop-off without signature. If you prefer delivery by UPS Extra with required signature, an additional cost will be applied, so please contact us before choosing this method. Whichever shipment choice you make, we will provide you with a link to track your package online.</p>\n<p>Shipping fees include handling and packing fees as well as postage costs. Handling fees are fixed, whereas transport fees vary according to total weight of the shipment. We advise you to group your items in one order. We cannot group two distinct orders placed separately, and shipping fees will apply to each of them. Your package will be dispatched at your own risk, but special care is taken to protect fragile objects.<br /><br />Boxes are amply sized and your items are well-protected.</p>','guide'),(2,1,1,'銷售及退款','','','<h1 class=\"text-xs-center\">銷售及退款</h1>\n<ul class=\"card-block\" style=\"padding:50px 80px;border:1px solid #666;border-radius:10px;\">\n<li>根據消費者保護法規定，《商店名稱》提供您七日鑑賞期之權益。</li>\n<li>鑑賞期非試用期，所退回商品（贈品、隨貨附件）必須是全新完整的狀態。並請另加外包裝袋，避免破壞商品外盒或原包裝。</li>\n<li>如所退回商品非全新狀態，將無法為您辦理退貨，煩請留意以免影響您的退貨權益。</li>\n<li>如退回商品配件不全，需請您自費郵資補件。</li>\n<li>食品與衛生用品若一經拆封則無法享有七日鑑賞之權益，還請體諒。</li>\n<li>感謝您對我們的支持。</li>\n</ul>','sales-and-refunds'),(2,2,1,'Sales & Refund','','','','sales-and-refunds'),(2,3,1,'Sales & Refund','','','<h2>Legal</h2>\n<h3>Credits</h3>\n<p>Concept and production:</p>\n<p>This Web site was created using <a href=\"http://www.prestashop.com\">PrestaShop</a>™ open-source software.</p>','sales-and-refunds'),(2,4,1,'Sales & Refund','','','','sales-and-refunds'),(3,1,1,'隱私權政策','','','<h1>隱私權政策</h1>\n<h2>隱私權政策已於 2018 年 9 月 14 日更新。</h2>\n<h5>《商店名稱》 非常重視您的隱私，如有任何問題，敬請不吝告知。</h5>\n<h3>個人資訊的收集與使用</h3>\n<p>個人資訊僅限於提供於我們的相關服務，不會提供給第三方進行行銷。<br /> 提供第三方（例如：提供物流、金流之廠商）的資料，可能包括詳細聯絡資料等的資料，我們鼓勵您瞭解這些第三方的隱私權政策。</p>\n<h3>保護個人資訊</h3>\n<p>《商店名稱》 網站服務都會在資料傳輸期間，使用加密技術來保護您的個人資訊。</p>\n<h3>Cookie 及其他技術</h3>\n<p>我們使用「Cookie」，這類技術可協助我們更充分瞭解使用者行為。<br />我們和大多數網站一樣，會自動收集部分資訊，這類資訊包括網路 IP 位址、瀏覽器類型及語言、日期/時間及點選資料。</p>','privacy'),(3,2,1,'Privacy','','','<h1 class=\"page-heading\">Terms and conditions of use</h1>\n<h3 class=\"page-subheading\">Rule 1</h3>\n<p class=\"bottom-indent\">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n<h3 class=\"page-subheading\">Rule 2</h3>\n<p class=\"bottom-indent\">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniamю</p>\n<h3 class=\"page-subheading\">Rule 3</h3>\n<p class=\"bottom-indent\">Tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniamю</p>','privacy'),(3,3,1,'Privacy','','','<h1 class=\"page-heading\">Terms and conditions of use</h1>\n<h3 class=\"page-subheading\">Rule 1</h3>\n<p class=\"bottom-indent\">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n<h3 class=\"page-subheading\">Rule 2</h3>\n<p class=\"bottom-indent\">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniamю</p>\n<h3 class=\"page-subheading\">Rule 3</h3>\n<p class=\"bottom-indent\">Tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniamю</p>','privacy'),(3,4,1,'Privacy','','','','privacy'),(4,1,1,'品牌故事','','','','about'),(4,2,1,'About','','','','about'),(4,3,1,'About','','','','about'),(4,4,1,'About','','','','about'),(5,1,1,'最新消息','','','','news'),(5,2,1,'News','','','<h2>Secure payment</h2>\n<h3>Our secure payment</h3>\n<p>With SSL</p>\n<h3>Using Visa/Mastercard/Paypal</h3>\n<p>About this services</p>','news'),(5,3,1,'News','','','<h2>Secure payment</h2>\n<h3>Our secure payment</h3>\n<p>With SSL</p>\n<h3>Using Visa/Mastercard/Paypal</h3>\n<p>About this services</p>','news'),(5,4,1,'News','','','<h2>Secure payment</h2>\n<h3>Our secure payment</h3>\n<p>With SSL</p>\n<h3>Using Visa/Mastercard/Paypal</h3>\n<p>About this services</p>','news'),(6,1,1,'常見問題','','','','qa'),(6,2,1,'Q&A','','','','qa'),(6,3,1,'Q&A','','','','qa'),(6,4,1,'Q&A','','','','qa'),(6,1,2,'常見問題','','','','qa'),(6,2,2,'Q&A','','','','qa'),(6,3,2,'Q&A','','','','qa'),(6,4,2,'Q&A','','','','qa'),(6,1,3,'常見問題','','','','qa'),(6,2,3,'Q&A','','','','qa'),(6,3,3,'Q&A','','','','qa'),(6,4,3,'Q&A','','','','qa'),(6,1,4,'常見問題','','','','qa'),(6,2,4,'Q&A','','','','qa'),(6,3,4,'Q&A','','','','qa'),(6,4,4,'Q&A','','','','qa'),(7,1,1,'首頁櫥窗','','','<div class=\"mt-2\">\n<div>\n<h3 class=\"display3-size\" style=\"margin-bottom:20px;display:inline-block;\">首頁櫥窗</h3>\n</div>\n{product:13,14,2|desktop-3|mobile-1} {product:13,14,2,3} {product:13,14,2,3,1,8|desktop-6|mobile-3|no-flag}</div>\n<p>?????☀ ☀ ☀ ☀ ☀</p>','shopwindow'),(7,2,1,'首頁櫥窗','','','','shopwindow'),(7,3,1,'首頁櫥窗','','','','shopwindow'),(7,4,1,'首頁櫥窗','','','','shopwindow');
/*!40000 ALTER TABLE `ps_cms_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_cms_role`
--

DROP TABLE IF EXISTS `ps_cms_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_cms_role` (
  `id_cms_role` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `id_cms` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_cms_role`,`id_cms`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_cms_role`
--

LOCK TABLES `ps_cms_role` WRITE;
/*!40000 ALTER TABLE `ps_cms_role` DISABLE KEYS */;
INSERT INTO `ps_cms_role` VALUES (1,'LEGAL_CONDITIONS',3),(6,'LEGAL_ENVIRONMENTAL',9),(2,'LEGAL_NOTICE',2),(5,'LEGAL_PRIVACY',7),(3,'LEGAL_REVOCATION',6),(4,'LEGAL_REVOCATION_FORM',0),(7,'LEGAL_SHIP_PAY',8);
/*!40000 ALTER TABLE `ps_cms_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_cms_role_lang`
--

DROP TABLE IF EXISTS `ps_cms_role_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_cms_role_lang` (
  `id_cms_role` int(11) unsigned NOT NULL,
  `id_lang` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_cms_role`,`id_lang`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_cms_role_lang`
--

LOCK TABLES `ps_cms_role_lang` WRITE;
/*!40000 ALTER TABLE `ps_cms_role_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_cms_role_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_cms_shop`
--

DROP TABLE IF EXISTS `ps_cms_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_cms_shop` (
  `id_cms` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  `indexed` tinyint(1) NOT NULL DEFAULT '0' COMMENT '已索引 flag',
  PRIMARY KEY (`id_cms`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_cms_shop`
--

LOCK TABLES `ps_cms_shop` WRITE;
/*!40000 ALTER TABLE `ps_cms_shop` DISABLE KEYS */;
INSERT INTO `ps_cms_shop` VALUES (1,1,0),(2,1,0),(3,1,0),(4,1,0),(5,1,0),(6,1,0),(7,1,0);
/*!40000 ALTER TABLE `ps_cms_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_configuration`
--

DROP TABLE IF EXISTS `ps_configuration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_configuration` (
  `id_configuration` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_group` int(11) unsigned DEFAULT NULL,
  `id_shop` int(11) unsigned DEFAULT NULL,
  `name` varchar(254) NOT NULL,
  `value` text,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_configuration`),
  KEY `name` (`name`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_group` (`id_shop_group`)
) ENGINE=InnoDB AUTO_INCREMENT=756 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_configuration`
--

LOCK TABLES `ps_configuration` WRITE;
/*!40000 ALTER TABLE `ps_configuration` DISABLE KEYS */;
INSERT INTO `ps_configuration` VALUES (1,NULL,NULL,'PS_LANG_DEFAULT','1','2018-07-11 11:57:31','2018-07-12 09:29:04'),(2,NULL,NULL,'PS_VERSION_DB','1.7.4.0','2018-07-11 11:57:31','2018-07-11 11:57:31'),(3,NULL,NULL,'PS_INSTALL_VERSION','1.7.4.0','2018-07-11 11:57:31','2018-07-11 11:57:31'),(4,NULL,NULL,'PS_GROUP_FEATURE_ACTIVE','1','2018-07-11 11:57:34','2018-11-13 10:32:18'),(5,NULL,NULL,'PS_CARRIER_DEFAULT','-2','2018-07-11 11:57:35','2018-07-14 17:33:35'),(6,NULL,NULL,'PS_SEARCH_INDEXATION','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(7,NULL,NULL,'PS_CURRENCY_DEFAULT','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(8,NULL,NULL,'PS_COUNTRY_DEFAULT','203','0000-00-00 00:00:00','2018-07-11 11:57:38'),(9,NULL,NULL,'PS_REWRITING_SETTINGS','1','0000-00-00 00:00:00','2018-07-11 11:57:38'),(10,NULL,NULL,'PS_ORDER_OUT_OF_STOCK','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(11,NULL,NULL,'PS_LAST_QTIES','3','0000-00-00 00:00:00','0000-00-00 00:00:00'),(12,NULL,NULL,'PS_CONDITIONS','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(13,NULL,NULL,'PS_RECYCLABLE_PACK','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(14,NULL,NULL,'PS_GIFT_WRAPPING','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(15,NULL,NULL,'PS_GIFT_WRAPPING_PRICE','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(16,NULL,NULL,'PS_STOCK_MANAGEMENT','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(17,NULL,NULL,'PS_NAVIGATION_PIPE','>','0000-00-00 00:00:00','0000-00-00 00:00:00'),(18,NULL,NULL,'PS_PRODUCTS_PER_PAGE','12','0000-00-00 00:00:00','0000-00-00 00:00:00'),(19,NULL,NULL,'PS_PURCHASE_MINIMUM','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(20,NULL,NULL,'PS_PRODUCTS_ORDER_WAY','1','0000-00-00 00:00:00','2018-07-20 19:50:36'),(21,NULL,NULL,'PS_PRODUCTS_ORDER_BY','2','0000-00-00 00:00:00','2018-07-12 09:06:54'),(22,NULL,NULL,'PS_DISPLAY_QTIES','0','0000-00-00 00:00:00','2018-12-31 19:50:20'),(23,NULL,NULL,'PS_SHIPPING_HANDLING','0','0000-00-00 00:00:00','2018-07-14 09:17:46'),(24,NULL,NULL,'PS_SHIPPING_FREE_PRICE','0','0000-00-00 00:00:00','2018-07-14 11:35:29'),(25,NULL,NULL,'PS_SHIPPING_FREE_WEIGHT','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(26,NULL,NULL,'PS_SHIPPING_METHOD','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(27,NULL,NULL,'PS_TAX','0','0000-00-00 00:00:00','2018-07-13 14:21:27'),(28,NULL,NULL,'PS_SHOP_ENABLE','1','0000-00-00 00:00:00','2018-12-31 14:23:17'),(29,NULL,NULL,'PS_NB_DAYS_NEW_PRODUCT','20','0000-00-00 00:00:00','0000-00-00 00:00:00'),(30,NULL,NULL,'PS_SSL_ENABLED','1','0000-00-00 00:00:00','2018-10-15 23:14:09'),(31,NULL,NULL,'PS_WEIGHT_UNIT','kg','0000-00-00 00:00:00','2018-07-27 18:46:07'),(32,NULL,NULL,'PS_BLOCK_CART_AJAX','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(33,NULL,NULL,'PS_ORDER_RETURN','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(34,NULL,NULL,'PS_ORDER_RETURN_NB_DAYS','14','0000-00-00 00:00:00','0000-00-00 00:00:00'),(35,NULL,NULL,'PS_MAIL_TYPE','1','0000-00-00 00:00:00','2018-09-05 22:08:07'),(36,NULL,NULL,'PS_PRODUCT_PICTURE_MAX_SIZE','8388608','0000-00-00 00:00:00','0000-00-00 00:00:00'),(37,NULL,NULL,'PS_PRODUCT_PICTURE_WIDTH','64','0000-00-00 00:00:00','0000-00-00 00:00:00'),(38,NULL,NULL,'PS_PRODUCT_PICTURE_HEIGHT','64','0000-00-00 00:00:00','0000-00-00 00:00:00'),(39,NULL,NULL,'PS_INVOICE_PREFIX','#IN','0000-00-00 00:00:00','0000-00-00 00:00:00'),(40,NULL,NULL,'PS_INVCE_INVOICE_ADDR_RULES','{\"avoid\":[]}','0000-00-00 00:00:00','0000-00-00 00:00:00'),(41,NULL,NULL,'PS_INVCE_DELIVERY_ADDR_RULES','{\"avoid\":[]}','0000-00-00 00:00:00','0000-00-00 00:00:00'),(42,NULL,NULL,'PS_DELIVERY_PREFIX','#DE','0000-00-00 00:00:00','0000-00-00 00:00:00'),(43,NULL,NULL,'PS_DELIVERY_NUMBER',NULL,'0000-00-00 00:00:00','2018-08-01 22:13:28'),(44,NULL,NULL,'PS_RETURN_PREFIX','#RE','0000-00-00 00:00:00','0000-00-00 00:00:00'),(45,NULL,NULL,'PS_INVOICE','0','0000-00-00 00:00:00','2018-08-01 21:49:21'),(46,NULL,NULL,'PS_PASSWD_TIME_BACK','360','0000-00-00 00:00:00','0000-00-00 00:00:00'),(47,NULL,NULL,'PS_PASSWD_TIME_FRONT','360','0000-00-00 00:00:00','0000-00-00 00:00:00'),(48,NULL,NULL,'PS_PASSWD_RESET_VALIDITY','1440','0000-00-00 00:00:00','0000-00-00 00:00:00'),(49,NULL,NULL,'PS_DISP_UNAVAILABLE_ATTR','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(50,NULL,NULL,'PS_SEARCH_MINWORDLEN','3','0000-00-00 00:00:00','0000-00-00 00:00:00'),(51,NULL,NULL,'PS_SEARCH_BLACKLIST','','0000-00-00 00:00:00','0000-00-00 00:00:00'),(52,NULL,NULL,'PS_SEARCH_WEIGHT_PNAME','6','0000-00-00 00:00:00','0000-00-00 00:00:00'),(53,NULL,NULL,'PS_SEARCH_WEIGHT_REF','10','0000-00-00 00:00:00','0000-00-00 00:00:00'),(54,NULL,NULL,'PS_SEARCH_WEIGHT_SHORTDESC','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(55,NULL,NULL,'PS_SEARCH_WEIGHT_DESC','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(56,NULL,NULL,'PS_SEARCH_WEIGHT_CNAME','3','0000-00-00 00:00:00','0000-00-00 00:00:00'),(57,NULL,NULL,'PS_SEARCH_WEIGHT_MNAME','3','0000-00-00 00:00:00','0000-00-00 00:00:00'),(58,NULL,NULL,'PS_SEARCH_WEIGHT_TAG','4','0000-00-00 00:00:00','0000-00-00 00:00:00'),(59,NULL,NULL,'PS_SEARCH_WEIGHT_ATTRIBUTE','2','0000-00-00 00:00:00','0000-00-00 00:00:00'),(60,NULL,NULL,'PS_SEARCH_WEIGHT_FEATURE','2','0000-00-00 00:00:00','0000-00-00 00:00:00'),(61,NULL,NULL,'PS_SEARCH_AJAX','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(62,NULL,NULL,'PS_TIMEZONE','Asia/Taipei','0000-00-00 00:00:00','2018-07-11 11:57:38'),(63,NULL,NULL,'PS_THEME_V11','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(64,NULL,NULL,'PRESTASTORE_LIVE','1','0000-00-00 00:00:00','2018-09-27 19:30:43'),(65,NULL,NULL,'PS_TIN_ACTIVE','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(66,NULL,NULL,'PS_SHOW_ALL_MODULES','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(67,NULL,NULL,'PS_BACKUP_ALL','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(68,NULL,NULL,'PS_1_3_UPDATE_DATE','2011-12-27 10:20:42','0000-00-00 00:00:00','0000-00-00 00:00:00'),(69,NULL,NULL,'PS_PRICE_ROUND_MODE','2','0000-00-00 00:00:00','2018-07-13 15:25:18'),(70,NULL,NULL,'PS_1_3_2_UPDATE_DATE','2011-12-27 10:20:42','0000-00-00 00:00:00','0000-00-00 00:00:00'),(71,NULL,NULL,'PS_CONDITIONS_CMS_ID','1','0000-00-00 00:00:00','2018-07-13 14:37:16'),(72,NULL,NULL,'TRACKING_DIRECT_TRAFFIC','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(73,NULL,NULL,'PS_VOLUME_UNIT','l','0000-00-00 00:00:00','2018-07-27 18:46:07'),(74,NULL,NULL,'PS_CIPHER_ALGORITHM','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(75,NULL,NULL,'PS_ATTRIBUTE_CATEGORY_DISPLAY','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(76,NULL,NULL,'PS_CUSTOMER_SERVICE_FILE_UPLOAD','0','0000-00-00 00:00:00','2018-09-21 23:55:22'),(77,NULL,NULL,'PS_CUSTOMER_SERVICE_SIGNATURE','','0000-00-00 00:00:00','0000-00-00 00:00:00'),(78,NULL,NULL,'PS_BLOCK_BESTSELLERS_DISPLAY','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(79,NULL,NULL,'PS_BLOCK_NEWPRODUCTS_DISPLAY','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(80,NULL,NULL,'PS_BLOCK_SPECIALS_DISPLAY','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(81,NULL,NULL,'PS_STOCK_MVT_REASON_DEFAULT','3','0000-00-00 00:00:00','0000-00-00 00:00:00'),(82,NULL,NULL,'PS_SPECIFIC_PRICE_PRIORITIES','id_shop;id_currency;id_country;id_group','0000-00-00 00:00:00','0000-00-00 00:00:00'),(83,NULL,NULL,'PS_TAX_DISPLAY','0','0000-00-00 00:00:00','2018-07-13 14:21:27'),(84,NULL,NULL,'PS_SMARTY_FORCE_COMPILE','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(85,NULL,NULL,'PS_DISTANCE_UNIT','km','0000-00-00 00:00:00','2018-07-27 18:46:07'),(86,NULL,NULL,'PS_STORES_DISPLAY_CMS','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(87,NULL,NULL,'SHOP_LOGO_WIDTH','1830','0000-00-00 00:00:00','2018-12-26 10:59:31'),(88,NULL,NULL,'SHOP_LOGO_HEIGHT','380','0000-00-00 00:00:00','2018-12-26 10:59:31'),(89,NULL,NULL,'EDITORIAL_IMAGE_WIDTH','530','0000-00-00 00:00:00','0000-00-00 00:00:00'),(90,NULL,NULL,'EDITORIAL_IMAGE_HEIGHT','228','0000-00-00 00:00:00','0000-00-00 00:00:00'),(91,NULL,NULL,'PS_STATSDATA_CUSTOMER_PAGESVIEWS','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(92,NULL,NULL,'PS_STATSDATA_PAGESVIEWS','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(93,NULL,NULL,'PS_STATSDATA_PLUGINS','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(94,NULL,NULL,'PS_GEOLOCATION_ENABLED','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(95,NULL,NULL,'PS_ALLOWED_COUNTRIES','AF;ZA;AX;AL;DZ;DE;AD;AO;AI;AQ;AG;AN;SA;AR;AM;AW;AU;AT;AZ;BS;BH;BD;BB;BY;BE;BZ;BJ;BM;BT;BO;BA;BW;BV;BR;BN;BG;BF;MM;BI;KY;KH;CM;CA;CV;CF;CL;CN;CX;CY;CC;CO;KM;CG;CD;CK;KR;KP;CR;CI;HR;CU;DK;DJ;DM;EG;IE;SV;AE;EC;ER;ES;EE;ET;FK;FO;FJ;FI;FR;GA;GM;GE;GS;GH;GI;GR;GD;GL;GP;GU;GT;GG;GN;GQ;GW;GY;GF;HT;HM;HN;HK;HU;IM;MU;VG;VI;IN;ID;IR;IQ;IS;IL;IT;JM;JP;JE;JO;KZ;KE;KG;KI;KW;LA;LS;LV;LB;LR;LY;LI;LT;LU;MO;MK;MG;MY;MW;MV;ML;MT;MP;MA;MH;MQ;MR;YT;MX;FM;MD;MC;MN;ME;MS;MZ;NA;NR;NP;NI;NE;NG;NU;NF;NO;NC;NZ;IO;OM;UG;UZ;PK;PW;PS;PA;PG;PY;NL;PE;PH;PN;PL;PF;PR;PT;QA;DO;CZ;RE;RO;GB;RU;RW;EH;BL;KN;SM;MF;PM;VA;VC;LC;SB;WS;AS;ST;SN;RS;SC;SL;SG;SK;SI;SO;SD;LK;SE;CH;SR;SJ;SZ;SY;TJ;TW;TZ;TD;TF;TH;TL;TG;TK;TO;TT;TN;TM;TC;TR;TV;UA;UY;US;VU;VE;VN;WF;YE;ZM;ZW','0000-00-00 00:00:00','0000-00-00 00:00:00'),(96,NULL,NULL,'PS_GEOLOCATION_BEHAVIOR','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(97,NULL,NULL,'PS_LOCALE_LANGUAGE','tw','0000-00-00 00:00:00','2018-07-11 11:57:38'),(98,NULL,NULL,'PS_LOCALE_COUNTRY','tw','0000-00-00 00:00:00','2018-07-11 11:57:38'),(99,NULL,NULL,'PS_ATTACHMENT_MAXIMUM_SIZE','8','0000-00-00 00:00:00','0000-00-00 00:00:00'),(100,NULL,NULL,'PS_SMARTY_CACHE','1','0000-00-00 00:00:00','2018-11-13 10:32:18'),(101,NULL,NULL,'PS_DIMENSION_UNIT','cm','0000-00-00 00:00:00','2018-07-27 18:46:07'),(102,NULL,NULL,'PS_GUEST_CHECKOUT_ENABLED','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(103,NULL,NULL,'PS_DISPLAY_SUPPLIERS',NULL,'0000-00-00 00:00:00','2018-10-15 23:14:09'),(104,NULL,NULL,'PS_DISPLAY_BEST_SELLERS','1','0000-00-00 00:00:00','2018-10-15 23:14:09'),(105,NULL,NULL,'PS_CATALOG_MODE','0','0000-00-00 00:00:00','2018-09-18 15:56:31'),(106,NULL,NULL,'PS_GEOLOCATION_WHITELIST','127;209.185.108;209.185.253;209.85.238;209.85.238.11;209.85.238.4;216.239.33.96;216.239.33.97;216.239.33.98;216.239.33.99;216.239.37.98;216.239.37.99;216.239.39.98;216.239.39.99;216.239.41.96;216.239.41.97;216.239.41.98;216.239.41.99;216.239.45.4;216.239.46;216.239.51.96;216.239.51.97;216.239.51.98;216.239.51.99;216.239.53.98;216.239.53.99;216.239.57.96;91.240.109;216.239.57.97;216.239.57.98;216.239.57.99;216.239.59.98;216.239.59.99;216.33.229.163;64.233.173.193;64.233.173.194;64.233.173.195;64.233.173.196;64.233.173.197;64.233.173.198;64.233.173.199;64.233.173.200;64.233.173.201;64.233.173.202;64.233.173.203;64.233.173.204;64.233.173.205;64.233.173.206;64.233.173.207;64.233.173.208;64.233.173.209;64.233.173.210;64.233.173.211;64.233.173.212;64.233.173.213;64.233.173.214;64.233.173.215;64.233.173.216;64.233.173.217;64.233.173.218;64.233.173.219;64.233.173.220;64.233.173.221;64.233.173.222;64.233.173.223;64.233.173.224;64.233.173.225;64.233.173.226;64.233.173.227;64.233.173.228;64.233.173.229;64.233.173.230;64.233.173.231;64.233.173.232;64.233.173.233;64.233.173.234;64.233.173.235;64.233.173.236;64.233.173.237;64.233.173.238;64.233.173.239;64.233.173.240;64.233.173.241;64.233.173.242;64.233.173.243;64.233.173.244;64.233.173.245;64.233.173.246;64.233.173.247;64.233.173.248;64.233.173.249;64.233.173.250;64.233.173.251;64.233.173.252;64.233.173.253;64.233.173.254;64.233.173.255;64.68.80;64.68.81;64.68.82;64.68.83;64.68.84;64.68.85;64.68.86;64.68.87;64.68.88;64.68.89;64.68.90.1;64.68.90.10;64.68.90.11;64.68.90.12;64.68.90.129;64.68.90.13;64.68.90.130;64.68.90.131;64.68.90.132;64.68.90.133;64.68.90.134;64.68.90.135;64.68.90.136;64.68.90.137;64.68.90.138;64.68.90.139;64.68.90.14;64.68.90.140;64.68.90.141;64.68.90.142;64.68.90.143;64.68.90.144;64.68.90.145;64.68.90.146;64.68.90.147;64.68.90.148;64.68.90.149;64.68.90.15;64.68.90.150;64.68.90.151;64.68.90.152;64.68.90.153;64.68.90.154;64.68.90.155;64.68.90.156;64.68.90.157;64.68.90.158;64.68.90.159;64.68.90.16;64.68.90.160;64.68.90.161;64.68.90.162;64.68.90.163;64.68.90.164;64.68.90.165;64.68.90.166;64.68.90.167;64.68.90.168;64.68.90.169;64.68.90.17;64.68.90.170;64.68.90.171;64.68.90.172;64.68.90.173;64.68.90.174;64.68.90.175;64.68.90.176;64.68.90.177;64.68.90.178;64.68.90.179;64.68.90.18;64.68.90.180;64.68.90.181;64.68.90.182;64.68.90.183;64.68.90.184;64.68.90.185;64.68.90.186;64.68.90.187;64.68.90.188;64.68.90.189;64.68.90.19;64.68.90.190;64.68.90.191;64.68.90.192;64.68.90.193;64.68.90.194;64.68.90.195;64.68.90.196;64.68.90.197;64.68.90.198;64.68.90.199;64.68.90.2;64.68.90.20;64.68.90.200;64.68.90.201;64.68.90.202;64.68.90.203;64.68.90.204;64.68.90.205;64.68.90.206;64.68.90.207;64.68.90.208;64.68.90.21;64.68.90.22;64.68.90.23;64.68.90.24;64.68.90.25;64.68.90.26;64.68.90.27;64.68.90.28;64.68.90.29;64.68.90.3;64.68.90.30;64.68.90.31;64.68.90.32;64.68.90.33;64.68.90.34;64.68.90.35;64.68.90.36;64.68.90.37;64.68.90.38;64.68.90.39;64.68.90.4;64.68.90.40;64.68.90.41;64.68.90.42;64.68.90.43;64.68.90.44;64.68.90.45;64.68.90.46;64.68.90.47;64.68.90.48;64.68.90.49;64.68.90.5;64.68.90.50;64.68.90.51;64.68.90.52;64.68.90.53;64.68.90.54;64.68.90.55;64.68.90.56;64.68.90.57;64.68.90.58;64.68.90.59;64.68.90.6;64.68.90.60;64.68.90.61;64.68.90.62;64.68.90.63;64.68.90.64;64.68.90.65;64.68.90.66;64.68.90.67;64.68.90.68;64.68.90.69;64.68.90.7;64.68.90.70;64.68.90.71;64.68.90.72;64.68.90.73;64.68.90.74;64.68.90.75;64.68.90.76;64.68.90.77;64.68.90.78;64.68.90.79;64.68.90.8;64.68.90.80;64.68.90.9;64.68.91;64.68.92;66.249.64;66.249.65;66.249.66;66.249.67;66.249.68;66.249.69;66.249.70;66.249.71;66.249.72;66.249.73;66.249.78;66.249.79;72.14.199;8.6.48','0000-00-00 00:00:00','0000-00-00 00:00:00'),(107,NULL,NULL,'PS_LOGS_BY_EMAIL','5','0000-00-00 00:00:00','0000-00-00 00:00:00'),(108,NULL,NULL,'PS_COOKIE_CHECKIP',NULL,'0000-00-00 00:00:00','2018-09-27 19:30:43'),(109,NULL,NULL,'PS_USE_ECOTAX','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(110,NULL,NULL,'PS_CANONICAL_REDIRECT','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(111,NULL,NULL,'PS_IMG_UPDATE_TIME','1545793166','0000-00-00 00:00:00','2018-12-26 10:59:26'),(112,NULL,NULL,'PS_BACKUP_DROP_TABLE','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(113,NULL,NULL,'PS_OS_CHEQUE','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(114,NULL,NULL,'PS_OS_PAYMENT','2','0000-00-00 00:00:00','0000-00-00 00:00:00'),(115,NULL,NULL,'PS_OS_PREPARATION','3','0000-00-00 00:00:00','0000-00-00 00:00:00'),(116,NULL,NULL,'PS_OS_SHIPPING','4','0000-00-00 00:00:00','0000-00-00 00:00:00'),(117,NULL,NULL,'PS_OS_DELIVERED','5','0000-00-00 00:00:00','0000-00-00 00:00:00'),(118,NULL,NULL,'PS_OS_CANCELED','6','0000-00-00 00:00:00','0000-00-00 00:00:00'),(119,NULL,NULL,'PS_OS_REFUND','7','0000-00-00 00:00:00','0000-00-00 00:00:00'),(120,NULL,NULL,'PS_OS_ERROR','8','0000-00-00 00:00:00','0000-00-00 00:00:00'),(121,NULL,NULL,'PS_OS_OUTOFSTOCK','9','0000-00-00 00:00:00','0000-00-00 00:00:00'),(122,NULL,NULL,'PS_OS_BANKWIRE','10','0000-00-00 00:00:00','0000-00-00 00:00:00'),(123,NULL,NULL,'PS_OS_WS_PAYMENT','11','0000-00-00 00:00:00','0000-00-00 00:00:00'),(124,NULL,NULL,'PS_OS_OUTOFSTOCK_PAID','9','0000-00-00 00:00:00','0000-00-00 00:00:00'),(125,NULL,NULL,'PS_OS_OUTOFSTOCK_UNPAID','12','0000-00-00 00:00:00','0000-00-00 00:00:00'),(126,NULL,NULL,'PS_OS_COD_VALIDATION','13','0000-00-00 00:00:00','0000-00-00 00:00:00'),(127,NULL,NULL,'PS_LEGACY_IMAGES','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(128,NULL,NULL,'PS_IMAGE_QUALITY','png_all','0000-00-00 00:00:00','2018-09-23 14:27:19'),(129,NULL,NULL,'PS_PNG_QUALITY','7','0000-00-00 00:00:00','2018-09-23 14:27:19'),(130,NULL,NULL,'PS_JPEG_QUALITY','90','0000-00-00 00:00:00','2018-09-23 14:27:19'),(131,NULL,NULL,'PS_COOKIE_LIFETIME_FO','480','0000-00-00 00:00:00','0000-00-00 00:00:00'),(132,NULL,NULL,'PS_COOKIE_LIFETIME_BO','480','0000-00-00 00:00:00','0000-00-00 00:00:00'),(133,NULL,NULL,'PS_RESTRICT_DELIVERED_COUNTRIES','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(134,NULL,NULL,'PS_SHOW_NEW_ORDERS','1','0000-00-00 00:00:00','2018-09-27 19:30:43'),(135,NULL,NULL,'PS_SHOW_NEW_CUSTOMERS','1','0000-00-00 00:00:00','2018-09-27 19:30:43'),(136,NULL,NULL,'PS_SHOW_NEW_MESSAGES','1','0000-00-00 00:00:00','2018-09-27 19:30:43'),(137,NULL,NULL,'PS_FEATURE_FEATURE_ACTIVE','1','0000-00-00 00:00:00','2018-11-13 10:32:18'),(138,NULL,NULL,'PS_COMBINATION_FEATURE_ACTIVE','1','0000-00-00 00:00:00','2018-11-13 10:32:18'),(139,NULL,NULL,'PS_SPECIFIC_PRICE_FEATURE_ACTIVE','1','0000-00-00 00:00:00','2018-10-28 16:02:03'),(140,NULL,NULL,'PS_VIRTUAL_PROD_FEATURE_ACTIVE','1','0000-00-00 00:00:00','2018-07-11 11:57:44'),(141,NULL,NULL,'PS_CUSTOMIZATION_FEATURE_ACTIVE','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(142,NULL,NULL,'PS_CART_RULE_FEATURE_ACTIVE','1','0000-00-00 00:00:00','2018-10-28 22:52:17'),(143,NULL,NULL,'PS_PACK_FEATURE_ACTIVE',NULL,'0000-00-00 00:00:00','2019-01-21 23:12:40'),(144,NULL,NULL,'PS_ALIAS_FEATURE_ACTIVE',NULL,'0000-00-00 00:00:00','2018-10-29 17:32:57'),(145,NULL,NULL,'PS_TAX_ADDRESS_TYPE','id_address_delivery','0000-00-00 00:00:00','0000-00-00 00:00:00'),(146,NULL,NULL,'PS_SHOP_DEFAULT','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(147,NULL,NULL,'PS_CARRIER_DEFAULT_SORT','1','0000-00-00 00:00:00','2018-07-14 09:19:59'),(148,NULL,NULL,'PS_STOCK_MVT_INC_REASON_DEFAULT','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(149,NULL,NULL,'PS_STOCK_MVT_DEC_REASON_DEFAULT','2','0000-00-00 00:00:00','0000-00-00 00:00:00'),(150,NULL,NULL,'PS_ADVANCED_STOCK_MANAGEMENT','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(151,NULL,NULL,'PS_STOCK_MVT_TRANSFER_TO','7','0000-00-00 00:00:00','0000-00-00 00:00:00'),(152,NULL,NULL,'PS_STOCK_MVT_TRANSFER_FROM','6','0000-00-00 00:00:00','0000-00-00 00:00:00'),(153,NULL,NULL,'PS_CARRIER_DEFAULT_ORDER','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(154,NULL,NULL,'PS_STOCK_MVT_SUPPLY_ORDER','8','0000-00-00 00:00:00','0000-00-00 00:00:00'),(155,NULL,NULL,'PS_STOCK_CUSTOMER_ORDER_CANCEL_REASON','9','0000-00-00 00:00:00','0000-00-00 00:00:00'),(156,NULL,NULL,'PS_STOCK_CUSTOMER_RETURN_REASON','10','0000-00-00 00:00:00','0000-00-00 00:00:00'),(157,NULL,NULL,'PS_STOCK_MVT_INC_EMPLOYEE_EDITION','11','0000-00-00 00:00:00','0000-00-00 00:00:00'),(158,NULL,NULL,'PS_STOCK_MVT_DEC_EMPLOYEE_EDITION','12','0000-00-00 00:00:00','0000-00-00 00:00:00'),(159,NULL,NULL,'PS_STOCK_CUSTOMER_ORDER_REASON','3','0000-00-00 00:00:00','0000-00-00 00:00:00'),(160,NULL,NULL,'PS_UNIDENTIFIED_GROUP','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(161,NULL,NULL,'PS_GUEST_GROUP','2','0000-00-00 00:00:00','0000-00-00 00:00:00'),(162,NULL,NULL,'PS_CUSTOMER_GROUP','3','0000-00-00 00:00:00','0000-00-00 00:00:00'),(163,NULL,NULL,'PS_SMARTY_CONSOLE','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(164,NULL,NULL,'PS_INVOICE_MODEL','invoice','0000-00-00 00:00:00','0000-00-00 00:00:00'),(165,NULL,NULL,'PS_LIMIT_UPLOAD_IMAGE_VALUE','2','0000-00-00 00:00:00','0000-00-00 00:00:00'),(166,NULL,NULL,'PS_LIMIT_UPLOAD_FILE_VALUE','2','0000-00-00 00:00:00','0000-00-00 00:00:00'),(167,NULL,NULL,'MB_PAY_TO_EMAIL','','0000-00-00 00:00:00','0000-00-00 00:00:00'),(168,NULL,NULL,'MB_SECRET_WORD','','0000-00-00 00:00:00','0000-00-00 00:00:00'),(169,NULL,NULL,'MB_HIDE_LOGIN','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(170,NULL,NULL,'MB_ID_LOGO','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(171,NULL,NULL,'MB_ID_LOGO_WALLET','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(172,NULL,NULL,'MB_PARAMETERS','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(173,NULL,NULL,'MB_PARAMETERS_2','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(174,NULL,NULL,'MB_DISPLAY_MODE','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(175,NULL,NULL,'MB_CANCEL_URL','http://www.yoursite.com','0000-00-00 00:00:00','0000-00-00 00:00:00'),(176,NULL,NULL,'MB_LOCAL_METHODS','2','0000-00-00 00:00:00','0000-00-00 00:00:00'),(177,NULL,NULL,'MB_INTER_METHODS','5','0000-00-00 00:00:00','0000-00-00 00:00:00'),(178,NULL,NULL,'BANK_WIRE_CURRENCIES','2,1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(179,NULL,NULL,'CHEQUE_CURRENCIES','2,1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(180,NULL,NULL,'PRODUCTS_VIEWED_NBR','8','0000-00-00 00:00:00','2018-07-11 14:39:24'),(181,NULL,NULL,'BLOCK_CATEG_DHTML','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(182,NULL,NULL,'BLOCK_CATEG_MAX_DEPTH','3','0000-00-00 00:00:00','2018-07-12 23:04:21'),(183,NULL,NULL,'MANUFACTURER_DISPLAY_FORM','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(184,NULL,NULL,'MANUFACTURER_DISPLAY_TEXT','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(185,NULL,NULL,'MANUFACTURER_DISPLAY_TEXT_NB','5','0000-00-00 00:00:00','0000-00-00 00:00:00'),(186,NULL,NULL,'NEW_PRODUCTS_NBR','4','0000-00-00 00:00:00','2018-07-11 14:33:56'),(187,NULL,NULL,'PS_TOKEN_ENABLE','1','0000-00-00 00:00:00','2018-10-15 23:14:09'),(188,NULL,NULL,'PS_STATS_RENDER','graphnvd3','0000-00-00 00:00:00','0000-00-00 00:00:00'),(189,NULL,NULL,'PS_STATS_OLD_CONNECT_AUTO_CLEAN','never','0000-00-00 00:00:00','0000-00-00 00:00:00'),(190,NULL,NULL,'PS_STATS_GRID_RENDER','gridhtml','0000-00-00 00:00:00','0000-00-00 00:00:00'),(191,NULL,NULL,'BLOCKTAGS_NBR','10','0000-00-00 00:00:00','0000-00-00 00:00:00'),(192,NULL,NULL,'CHECKUP_DESCRIPTIONS_LT','100','0000-00-00 00:00:00','0000-00-00 00:00:00'),(193,NULL,NULL,'CHECKUP_DESCRIPTIONS_GT','400','0000-00-00 00:00:00','0000-00-00 00:00:00'),(194,NULL,NULL,'CHECKUP_IMAGES_LT','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(195,NULL,NULL,'CHECKUP_IMAGES_GT','2','0000-00-00 00:00:00','0000-00-00 00:00:00'),(196,NULL,NULL,'CHECKUP_SALES_LT','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(197,NULL,NULL,'CHECKUP_SALES_GT','2','0000-00-00 00:00:00','0000-00-00 00:00:00'),(198,NULL,NULL,'CHECKUP_STOCK_LT','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(199,NULL,NULL,'CHECKUP_STOCK_GT','3','0000-00-00 00:00:00','0000-00-00 00:00:00'),(200,NULL,NULL,'FOOTER_CMS','0_3|0_4','0000-00-00 00:00:00','0000-00-00 00:00:00'),(201,NULL,NULL,'FOOTER_BLOCK_ACTIVATION','0_3|0_4','0000-00-00 00:00:00','0000-00-00 00:00:00'),(202,NULL,NULL,'FOOTER_POWEREDBY','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(203,NULL,NULL,'BLOCKADVERT_LINK','http://www.prestashop.com','0000-00-00 00:00:00','0000-00-00 00:00:00'),(204,NULL,NULL,'BLOCKSTORE_IMG','store.jpg','0000-00-00 00:00:00','0000-00-00 00:00:00'),(205,NULL,NULL,'BLOCKADVERT_IMG_EXT','jpg','0000-00-00 00:00:00','0000-00-00 00:00:00'),(206,NULL,NULL,'MOD_BLOCKTOPMENU_ITEMS','CAT7,CAT8,CAT14,LNK1','0000-00-00 00:00:00','2018-12-26 13:26:10'),(207,NULL,NULL,'MOD_BLOCKTOPMENU_SEARCH','','0000-00-00 00:00:00','0000-00-00 00:00:00'),(208,NULL,NULL,'BLOCKSOCIAL_FACEBOOK','https://www.facebook.com','0000-00-00 00:00:00','2018-10-08 22:12:51'),(209,NULL,NULL,'BLOCKSOCIAL_TWITTER',NULL,'0000-00-00 00:00:00','2018-07-11 11:58:45'),(210,NULL,NULL,'BLOCKSOCIAL_RSS',NULL,'0000-00-00 00:00:00','2018-07-11 11:58:45'),(211,NULL,NULL,'BLOCKCONTACTINFOS_COMPANY','Your company','0000-00-00 00:00:00','0000-00-00 00:00:00'),(212,NULL,NULL,'BLOCKCONTACTINFOS_ADDRESS','Address line 1\nCity\nCountry','0000-00-00 00:00:00','0000-00-00 00:00:00'),(213,NULL,NULL,'BLOCKCONTACTINFOS_PHONE','0123-456-789','0000-00-00 00:00:00','0000-00-00 00:00:00'),(214,NULL,NULL,'BLOCKCONTACTINFOS_EMAIL','pub@prestashop.com','0000-00-00 00:00:00','0000-00-00 00:00:00'),(215,NULL,NULL,'BLOCKCONTACT_TELNUMBER','0123-456-789','0000-00-00 00:00:00','0000-00-00 00:00:00'),(216,NULL,NULL,'BLOCKCONTACT_EMAIL','pub@prestashop.com','0000-00-00 00:00:00','0000-00-00 00:00:00'),(217,NULL,NULL,'SUPPLIER_DISPLAY_TEXT','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(218,NULL,NULL,'SUPPLIER_DISPLAY_TEXT_NB','5','0000-00-00 00:00:00','0000-00-00 00:00:00'),(219,NULL,NULL,'SUPPLIER_DISPLAY_FORM','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(220,NULL,NULL,'BLOCK_CATEG_NBR_COLUMN_FOOTER','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(221,NULL,NULL,'UPGRADER_BACKUPDB_FILENAME','','0000-00-00 00:00:00','0000-00-00 00:00:00'),(222,NULL,NULL,'UPGRADER_BACKUPFILES_FILENAME','','0000-00-00 00:00:00','0000-00-00 00:00:00'),(223,NULL,NULL,'BLOCKREINSURANCE_NBBLOCKS','5','0000-00-00 00:00:00','0000-00-00 00:00:00'),(224,NULL,NULL,'HOMESLIDER_WIDTH','535','0000-00-00 00:00:00','0000-00-00 00:00:00'),(225,NULL,NULL,'HOMESLIDER_SPEED','5000','0000-00-00 00:00:00','2018-07-11 11:58:38'),(226,NULL,NULL,'HOMESLIDER_PAUSE','7700','0000-00-00 00:00:00','0000-00-00 00:00:00'),(227,NULL,NULL,'HOMESLIDER_LOOP','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(228,NULL,NULL,'PS_BASE_DISTANCE_UNIT','里','0000-00-00 00:00:00','2018-07-13 14:35:07'),(229,NULL,NULL,'PS_SHOP_DOMAIN','shop1.presta.shop','0000-00-00 00:00:00','2018-07-11 11:57:38'),(230,NULL,NULL,'PS_SHOP_DOMAIN_SSL','shop1.presta.shop','0000-00-00 00:00:00','2018-07-11 11:57:38'),(231,NULL,NULL,'PS_SHOP_NAME','TekapoCart 商店','0000-00-00 00:00:00','2018-10-15 23:12:07'),(232,NULL,NULL,'PS_SHOP_EMAIL','admin@example.com','0000-00-00 00:00:00','2018-07-11 11:57:39'),(233,NULL,NULL,'PS_MAIL_METHOD','2','0000-00-00 00:00:00','2018-09-21 14:36:12'),(234,NULL,NULL,'PS_SHOP_ACTIVITY',NULL,'0000-00-00 00:00:00','2018-10-15 23:14:09'),(235,NULL,NULL,'PS_LOGO','logo.jpg','0000-00-00 00:00:00','2018-12-26 10:59:26'),(236,NULL,NULL,'PS_FAVICON','favicon.png','0000-00-00 00:00:00','2018-10-19 18:00:00'),(237,NULL,NULL,'PS_STORES_ICON','logo_stores.png','0000-00-00 00:00:00','0000-00-00 00:00:00'),(238,NULL,NULL,'PS_ROOT_CATEGORY','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(239,NULL,NULL,'PS_HOME_CATEGORY','2','0000-00-00 00:00:00','0000-00-00 00:00:00'),(240,NULL,NULL,'PS_CONFIGURATION_AGREMENT','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(241,NULL,NULL,'PS_MAIL_SERVER','smtp.gmail.com','0000-00-00 00:00:00','2018-07-28 05:01:00'),(242,NULL,NULL,'PS_MAIL_USER','e.tekapo@gmail.com','0000-00-00 00:00:00','2018-07-28 05:01:00'),(243,NULL,NULL,'PS_MAIL_PASSWD',NULL,'0000-00-00 00:00:00','2018-09-11 15:08:14'),(244,NULL,NULL,'PS_MAIL_SMTP_ENCRYPTION','tls','0000-00-00 00:00:00','2018-09-12 01:38:31'),(245,NULL,NULL,'PS_MAIL_SMTP_PORT','587','0000-00-00 00:00:00','2018-09-12 01:38:31'),(246,NULL,NULL,'PS_MAIL_COLOR','#db3484','0000-00-00 00:00:00','0000-00-00 00:00:00'),(247,NULL,NULL,'NW_SALT','mtLr3E6UYAB4JqYe','0000-00-00 00:00:00','2018-07-11 11:58:34'),(248,NULL,NULL,'PS_PAYMENT_LOGO_CMS_ID','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(249,NULL,NULL,'HOME_FEATURED_NBR','8','0000-00-00 00:00:00','0000-00-00 00:00:00'),(250,NULL,NULL,'SEK_MIN_OCCURENCES','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(251,NULL,NULL,'SEK_FILTER_KW','','0000-00-00 00:00:00','0000-00-00 00:00:00'),(252,NULL,NULL,'PS_ALLOW_MOBILE_DEVICE','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(253,NULL,NULL,'PS_CUSTOMER_CREATION_EMAIL','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(254,NULL,NULL,'PS_SMARTY_CONSOLE_KEY','SMARTY_DEBUG','0000-00-00 00:00:00','0000-00-00 00:00:00'),(255,NULL,NULL,'PS_DASHBOARD_USE_PUSH','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(256,NULL,NULL,'PS_ATTRIBUTE_ANCHOR_SEPARATOR','-','0000-00-00 00:00:00','0000-00-00 00:00:00'),(257,NULL,NULL,'CONF_AVERAGE_PRODUCT_MARGIN','40','0000-00-00 00:00:00','0000-00-00 00:00:00'),(258,NULL,NULL,'PS_DASHBOARD_SIMULATION','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(259,NULL,NULL,'PS_USE_HTMLPURIFIER','1','0000-00-00 00:00:00','2018-10-15 23:14:09'),(260,NULL,NULL,'PS_SMARTY_CACHING_TYPE','filesystem','0000-00-00 00:00:00','0000-00-00 00:00:00'),(261,NULL,NULL,'PS_SMARTY_LOCAL',NULL,'0000-00-00 00:00:00','2018-11-13 10:32:18'),(262,NULL,NULL,'PS_SMARTY_CLEAR_CACHE','everytime','0000-00-00 00:00:00','0000-00-00 00:00:00'),(263,NULL,NULL,'PS_DETECT_LANG','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(264,NULL,NULL,'PS_DETECT_COUNTRY','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(265,NULL,NULL,'PS_ROUND_TYPE','1','0000-00-00 00:00:00','2018-07-13 15:25:03'),(266,NULL,NULL,'PS_PRICE_DISPLAY_PRECISION','2','0000-00-00 00:00:00','2018-07-13 15:25:03'),(267,NULL,NULL,'PS_LOG_EMAILS','0','0000-00-00 00:00:00','2018-09-05 22:38:31'),(268,NULL,NULL,'PS_CUSTOMER_OPTIN','0','0000-00-00 00:00:00','2018-07-12 09:15:30'),(269,NULL,NULL,'PS_CUSTOMER_BIRTHDATE','0','0000-00-00 00:00:00','2018-07-12 09:11:56'),(270,NULL,NULL,'PS_PACK_STOCK_TYPE','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(271,NULL,NULL,'PS_LOG_MODULE_PERFS_MODULO','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(272,NULL,NULL,'PS_DISALLOW_HISTORY_REORDERING',NULL,'0000-00-00 00:00:00','2018-07-11 14:37:40'),(273,NULL,NULL,'PS_DISPLAY_PRODUCT_WEIGHT','0','0000-00-00 00:00:00','0000-00-00 00:00:00'),(274,NULL,NULL,'PS_PRODUCT_WEIGHT_PRECISION','2','0000-00-00 00:00:00','0000-00-00 00:00:00'),(275,NULL,NULL,'PS_ACTIVE_CRONJOB_EXCHANGE_RATE','1','0000-00-00 00:00:00','2018-07-30 16:07:21'),(276,NULL,NULL,'PS_ORDER_RECALCULATE_SHIPPING','1','0000-00-00 00:00:00','0000-00-00 00:00:00'),(277,NULL,NULL,'PS_MAINTENANCE_TEXT','','0000-00-00 00:00:00','0000-00-00 00:00:00'),(278,NULL,NULL,'PS_PRODUCT_SHORT_DESC_LIMIT','800','0000-00-00 00:00:00','0000-00-00 00:00:00'),(279,NULL,NULL,'PS_LABEL_IN_STOCK_PRODUCTS','In Stock','0000-00-00 00:00:00','0000-00-00 00:00:00'),(280,NULL,NULL,'PS_LABEL_OOS_PRODUCTS_BOA','Product available for orders','0000-00-00 00:00:00','0000-00-00 00:00:00'),(281,NULL,NULL,'PS_LABEL_OOS_PRODUCTS_BOD','Out-of-Stock','0000-00-00 00:00:00','0000-00-00 00:00:00'),(282,NULL,NULL,'PS_SSL_ENABLED_EVERYWHERE','1','2018-07-11 11:57:38','2018-10-15 23:14:09'),(283,NULL,NULL,'DASHACTIVITY_CART_ACTIVE','30','2018-07-11 11:58:19','2018-07-11 11:58:19'),(284,NULL,NULL,'DASHACTIVITY_CART_ABANDONED_MIN','24','2018-07-11 11:58:19','2018-07-11 11:58:19'),(285,NULL,NULL,'DASHACTIVITY_CART_ABANDONED_MAX','48','2018-07-11 11:58:19','2018-07-11 11:58:19'),(286,NULL,NULL,'DASHACTIVITY_VISITOR_ONLINE','30','2018-07-11 11:58:19','2018-07-11 11:58:19'),(287,NULL,NULL,'PS_DASHGOALS_CURRENT_YEAR','2018','2018-07-11 11:58:21','2018-07-11 11:58:21'),(288,NULL,NULL,'DASHPRODUCT_NBR_SHOW_LAST_ORDER','10','2018-07-11 11:58:22','2018-07-11 11:58:22'),(289,NULL,NULL,'DASHPRODUCT_NBR_SHOW_BEST_SELLER','10','2018-07-11 11:58:22','2018-07-11 11:58:22'),(290,NULL,NULL,'DASHPRODUCT_NBR_SHOW_MOST_VIEWED','10','2018-07-11 11:58:22','2018-07-11 11:58:22'),(291,NULL,NULL,'DASHPRODUCT_NBR_SHOW_TOP_SEARCH','10','2018-07-11 11:58:22','2018-07-11 11:58:22'),(292,NULL,NULL,'BANNER_IMG',NULL,'2018-07-11 11:58:26','2018-07-11 11:58:26'),(293,NULL,NULL,'BANNER_LINK',NULL,'2018-07-11 11:58:26','2018-07-11 11:58:26'),(294,NULL,NULL,'BANNER_DESC',NULL,'2018-07-11 11:58:26','2018-07-11 11:58:26'),(295,NULL,NULL,'BLOCK_CATEG_ROOT_CATEGORY','0','2018-07-11 11:58:27','2018-07-12 23:04:21'),(296,NULL,NULL,'CONF_PS_CHECKPAYMENT_FIXED','0.2','2018-07-11 11:58:28','2018-07-11 11:58:28'),(297,NULL,NULL,'CONF_PS_CHECKPAYMENT_VAR','2','2018-07-11 11:58:28','2018-07-11 11:58:28'),(298,NULL,NULL,'CONF_PS_CHECKPAYMENT_FIXED_FOREIGN','0.2','2018-07-11 11:58:28','2018-07-11 11:58:28'),(299,NULL,NULL,'CONF_PS_CHECKPAYMENT_VAR_FOREIGN','2','2018-07-11 11:58:28','2018-07-11 11:58:28'),(300,NULL,NULL,'PS_NEWSLETTER_RAND','1121608291545472679','2018-07-11 11:58:34','2018-07-11 14:34:03'),(301,NULL,NULL,'NW_CONDITIONS',NULL,'2018-07-11 11:58:34','2018-07-11 11:58:34'),(302,NULL,NULL,'PS_LAYERED_SHOW_QTIES','1','2018-07-11 11:58:36','2018-07-11 11:58:36'),(303,NULL,NULL,'PS_LAYERED_FULL_TREE','1','2018-07-11 11:58:36','2018-07-11 11:58:36'),(304,NULL,NULL,'PS_LAYERED_FILTER_PRICE_USETAX','0','2018-07-11 11:58:36','2019-01-24 00:38:46'),(305,NULL,NULL,'PS_LAYERED_FILTER_CATEGORY_DEPTH','1','2018-07-11 11:58:36','2018-07-11 11:58:36'),(306,NULL,NULL,'PS_LAYERED_FILTER_PRICE_ROUNDING','1','2018-07-11 11:58:36','2018-07-11 11:58:36'),(307,NULL,NULL,'PS_LAYERED_INDEXED','1','2018-07-11 11:58:36','2018-07-11 11:58:36'),(308,NULL,NULL,'HOME_FEATURED_CAT','2','2018-07-11 11:58:37','2018-07-11 11:58:37'),(309,NULL,NULL,'HOMESLIDER_PAUSE_ON_HOVER','1','2018-07-11 11:58:38','2018-07-11 11:58:38'),(310,NULL,NULL,'HOMESLIDER_WRAP','1','2018-07-11 11:58:38','2018-09-14 01:19:21'),(311,NULL,NULL,'PS_SC_TWITTER','1','2018-07-11 11:58:43','2018-07-12 17:36:17'),(312,NULL,NULL,'PS_SC_FACEBOOK','1','2018-07-11 11:58:43','2018-07-12 17:36:17'),(313,NULL,NULL,'PS_SC_GOOGLE','1','2018-07-11 11:58:43','2018-07-12 17:36:17'),(314,NULL,NULL,'PS_SC_PINTEREST','1','2018-07-11 11:58:43','2018-07-11 11:58:43'),(315,NULL,NULL,'BLOCKSOCIAL_YOUTUBE',NULL,'2018-07-11 11:58:45','2018-07-11 11:58:45'),(316,NULL,NULL,'BLOCKSOCIAL_GOOGLE_PLUS',NULL,'2018-07-11 11:58:45','2018-07-11 11:58:45'),(317,NULL,NULL,'BLOCKSOCIAL_PINTEREST',NULL,'2018-07-11 11:58:45','2018-07-11 11:58:45'),(318,NULL,NULL,'BLOCKSOCIAL_VIMEO',NULL,'2018-07-11 11:58:45','2018-07-11 11:58:45'),(319,NULL,NULL,'BLOCKSOCIAL_INSTAGRAM','https://instagram.com','2018-07-11 11:58:45','2018-10-08 21:07:12'),(320,NULL,NULL,'BANK_WIRE_PAYMENT_INVITE','1','2018-07-11 11:58:47','2018-07-11 21:56:55'),(321,NULL,NULL,'CONF_PS_WIREPAYMENT_FIXED','0.2','2018-07-11 11:58:47','2018-07-11 11:58:47'),(322,NULL,NULL,'CONF_PS_WIREPAYMENT_VAR','2','2018-07-11 11:58:47','2018-07-11 11:58:47'),(323,NULL,NULL,'CONF_PS_WIREPAYMENT_FIXED_FOREIGN','0.2','2018-07-11 11:58:47','2018-07-11 11:58:47'),(324,NULL,NULL,'CONF_PS_WIREPAYMENT_VAR_FOREIGN','2','2018-07-11 11:58:47','2018-07-11 11:58:47'),(325,NULL,NULL,'GF_INSTALL_CALC','0','2018-07-11 11:59:18','2018-07-11 12:33:31'),(326,NULL,NULL,'GF_CURRENT_LEVEL','1','2018-07-11 11:59:18','2018-07-11 11:59:18'),(327,NULL,NULL,'GF_CURRENT_LEVEL_PERCENT','0','2018-07-11 11:59:18','2018-07-11 12:33:31'),(328,NULL,NULL,'GF_NOTIFICATION','0','2018-07-11 11:59:18','2018-07-11 12:33:31'),(335,NULL,NULL,'GF_NOT_VIEWED_BADGE','122','2018-07-11 12:31:30','2018-07-11 12:31:40'),(336,NULL,NULL,'ONBOARDINGV2_CURRENT_STEP','0','2018-07-11 12:31:34','2018-07-11 12:33:59'),(337,NULL,NULL,'ONBOARDINGV2_SHUT_DOWN','1','2018-07-11 12:32:21','2018-07-11 12:32:54'),(343,NULL,NULL,'PS_BLOCK_BESTSELLERS_TO_DISPLAY','5','2018-07-11 12:51:21','2018-07-11 12:51:21'),(344,NULL,NULL,'BLOCKSPECIALS_SPECIALS_NBR','4','2018-07-11 12:51:27','2018-07-11 12:51:27'),(345,NULL,NULL,'CONF_PS_CASHONDELIVERY_FIXED','0.2','2018-07-11 12:53:02','2018-07-11 12:53:02'),(346,NULL,NULL,'CONF_PS_CASHONDELIVERY_VAR','2','2018-07-11 12:53:02','2018-07-11 12:53:02'),(347,NULL,NULL,'CONF_PS_CASHONDELIVERY_FIXED_FOREIGN','0.2','2018-07-11 12:53:02','2018-07-11 12:53:02'),(348,NULL,NULL,'CONF_PS_CASHONDELIVERY_VAR_FOREIGN','2','2018-07-11 12:53:02','2018-07-11 12:53:02'),(349,NULL,NULL,'PS_FOLLOWUP_SECURE_KEY','ABBZIVXH0P4M1JFI','2018-07-11 12:53:38','2018-07-11 12:53:38'),(350,NULL,NULL,'PS_FOLLOW_UP_ENABLE_1','0','2018-07-11 12:53:38','2018-07-11 12:53:38'),(351,NULL,NULL,'PS_FOLLOW_UP_ENABLE_2','0','2018-07-11 12:53:38','2018-07-11 12:53:38'),(352,NULL,NULL,'PS_FOLLOW_UP_ENABLE_3','0','2018-07-11 12:53:38','2018-07-11 12:53:38'),(353,NULL,NULL,'PS_FOLLOW_UP_ENABLE_4','0','2018-07-11 12:53:38','2018-07-11 12:53:38'),(354,NULL,NULL,'PS_FOLLOW_UP_AMOUNT_1','0','2018-07-11 12:53:38','2018-07-11 12:53:38'),(355,NULL,NULL,'PS_FOLLOW_UP_AMOUNT_2','0','2018-07-11 12:53:38','2018-07-11 12:53:38'),(356,NULL,NULL,'PS_FOLLOW_UP_AMOUNT_3','0','2018-07-11 12:53:38','2018-07-11 12:53:38'),(357,NULL,NULL,'PS_FOLLOW_UP_AMOUNT_4','0','2018-07-11 12:53:38','2018-07-11 12:53:38'),(358,NULL,NULL,'PS_FOLLOW_UP_DAYS_1','0','2018-07-11 12:53:38','2018-07-11 12:53:38'),(359,NULL,NULL,'PS_FOLLOW_UP_DAYS_2','0','2018-07-11 12:53:38','2018-07-11 12:53:38'),(360,NULL,NULL,'PS_FOLLOW_UP_DAYS_3','0','2018-07-11 12:53:38','2018-07-11 12:53:38'),(361,NULL,NULL,'PS_FOLLOW_UP_DAYS_4','0','2018-07-11 12:53:38','2018-07-11 12:53:38'),(362,NULL,NULL,'PS_FOLLOW_UP_THRESHOLD_3','0','2018-07-11 12:53:38','2018-07-11 12:53:38'),(363,NULL,NULL,'PS_FOLLOW_UP_DAYS_THRESHOLD_4','0','2018-07-11 12:53:38','2018-07-11 12:53:38'),(364,NULL,NULL,'PS_FOLLOW_UP_CLEAN_DB','0','2018-07-11 12:53:38','2018-07-11 12:53:38'),(365,NULL,NULL,'CROSSSELLING_DISPLAY_PRICE','1','2018-07-11 12:54:08','2018-07-11 12:54:08'),(366,NULL,NULL,'CROSSSELLING_NBR','8','2018-07-11 12:54:08','2018-07-11 12:54:08'),(375,NULL,NULL,'MA_MERCHANT_ORDER','1','2018-07-11 14:36:50','2018-07-11 14:36:50'),(376,NULL,NULL,'MA_MERCHANT_OOS','0','2018-07-11 14:36:50','2018-12-09 15:42:21'),(377,NULL,NULL,'MA_CUSTOMER_QTY','1','2018-07-11 14:36:50','2018-07-11 14:36:50'),(378,NULL,NULL,'MA_ORDER_EDIT','1','2018-07-11 14:36:50','2018-07-11 14:36:50'),(379,NULL,NULL,'MA_RETURN_SLIP','1','2018-07-11 14:36:50','2018-07-11 14:36:50'),(380,NULL,NULL,'MA_MERCHANT_MAILS','admin@example.com','2018-07-11 14:36:50','2018-07-11 14:36:50'),(381,NULL,NULL,'MA_LAST_QTIES','3','2018-07-11 14:36:50','2018-07-11 14:36:50'),(382,NULL,NULL,'MA_MERCHANT_COVERAGE','0','2018-07-11 14:36:50','2018-07-11 14:36:50'),(383,NULL,NULL,'MA_PRODUCT_COVERAGE','0','2018-07-11 14:36:50','2018-07-11 14:36:50'),(389,NULL,NULL,'PS_FINAL_SUMMARY_ENABLED','0','2018-07-11 14:37:40','2018-07-12 08:58:59'),(390,NULL,NULL,'BRAND_DISPLAY_TYPE','brand_form','2018-07-11 14:39:29','2018-08-21 16:15:45'),(391,NULL,NULL,'BRAND_DISPLAY_TEXT_NB','5','2018-07-11 14:39:29','2018-07-11 14:39:29'),(392,NULL,NULL,'SUPPLIER_DISPLAY_TYPE','supplier_text','2018-07-11 14:39:35','2018-07-11 14:39:35'),(395,NULL,NULL,'PS_PIXEL_ID',NULL,'2018-07-11 15:02:47','2018-07-11 15:02:47'),(396,NULL,NULL,'PS_CCCJS_VERSION','1560','2018-07-11 22:21:25','2019-01-21 23:26:12'),(397,NULL,NULL,'PS_CCCCSS_VERSION','1561','2018-07-11 22:21:25','2019-01-21 23:26:12'),(398,NULL,NULL,'PS_ALLOW_ACCENTED_CHARS_URL','1','2018-07-11 22:42:14','2018-07-12 08:51:11'),(399,NULL,NULL,'PS_HTACCESS_DISABLE_MULTIVIEWS','0','2018-07-11 22:42:14','2018-07-11 22:42:14'),(400,NULL,NULL,'PS_HTACCESS_DISABLE_MODSEC','0','2018-07-11 22:42:14','2018-07-11 22:42:14'),(401,NULL,NULL,'PS_ROUTE_product_rule','{category}/{id}{/:id_product_attribute}/{rewrite}{/:ean13}','2018-07-11 22:42:14','2018-09-27 15:42:46'),(402,NULL,NULL,'PS_ROUTE_category_rule',NULL,'2018-07-11 22:42:14','2018-07-11 22:42:14'),(403,NULL,NULL,'PS_ROUTE_layered_rule',NULL,'2018-07-11 22:42:14','2018-07-11 22:42:14'),(404,NULL,NULL,'PS_ROUTE_supplier_rule','supplier/{id}__{rewrite}','2018-07-11 22:42:14','2018-07-12 23:42:53'),(405,NULL,NULL,'PS_ROUTE_manufacturer_rule','brand/{id}_{rewrite}','2018-07-11 22:42:14','2018-07-12 23:42:53'),(406,NULL,NULL,'PS_ROUTE_cms_rule','page/{id}-{rewrite}','2018-07-11 22:42:14','2018-07-12 23:42:53'),(407,NULL,NULL,'PS_ROUTE_cms_category_rule','page/category/{id}-{rewrite}','2018-07-11 22:42:14','2018-07-12 23:42:53'),(408,NULL,NULL,'PS_ROUTE_module','module/{module}{/:controller}','2018-07-11 22:42:14','2018-07-11 22:42:14'),(409,NULL,NULL,'PS_SHIP_WHEN_AVAILABLE','0','2018-07-12 08:58:59','2018-07-12 08:58:59'),(410,NULL,NULL,'PS_GIFT_WRAPPING_TAX_RULES_GROUP','0','2018-07-12 08:58:59','2018-07-12 08:58:59'),(411,NULL,NULL,'PS_QTY_DISCOUNT_ON_COMBINATION','0','2018-07-12 09:06:54','2018-07-12 09:06:54'),(412,NULL,NULL,'PS_FORCE_FRIENDLY_PRODUCT','0','2018-07-12 09:06:54','2018-07-12 09:06:54'),(413,NULL,NULL,'PS_PRODUCT_ACTIVATION_DEFAULT','0','2018-07-12 09:06:54','2018-07-12 09:06:54'),(414,NULL,NULL,'PS_DISPLAY_DISCOUNT_PRICE','1','2018-07-12 09:06:54','2018-07-30 11:14:14'),(415,NULL,NULL,'PS_LABEL_DELIVERY_TIME_AVAILABLE',NULL,'2018-07-12 09:06:54','2018-07-12 09:06:54'),(416,NULL,NULL,'PS_LABEL_DELIVERY_TIME_OOSBOA',NULL,'2018-07-12 09:06:54','2018-07-12 09:06:54'),(417,NULL,NULL,'PS_CART_FOLLOWING','1','2018-07-12 09:11:56','2018-07-27 20:26:23'),(418,NULL,NULL,'PS_B2B_ENABLE','0','2018-07-12 09:11:56','2018-07-12 09:11:56'),(419,NULL,NULL,'PS_SEARCH_START','0','2018-07-12 09:24:15','2018-07-12 09:24:15'),(420,NULL,NULL,'PS_SEARCH_END','0','2018-07-12 09:24:15','2018-07-12 09:24:15'),(421,NULL,NULL,'BANK_WIRE_DETAILS','銀行代號：1XX\r\n銀行帳號：XXXX-XXXX-XXXXX','2018-07-12 09:42:57','2018-07-12 09:42:57'),(422,NULL,NULL,'BANK_WIRE_OWNER','OO股份有限公司','2018-07-12 09:42:57','2018-07-12 09:42:57'),(423,NULL,NULL,'BANK_WIRE_ADDRESS','OO銀行-XX分行','2018-07-12 09:42:57','2018-07-12 09:42:57'),(424,NULL,NULL,'BANK_WIRE_RESERVATION_DAYS','3','2018-07-12 09:42:57','2018-07-12 09:42:57'),(425,NULL,NULL,'BANK_WIRE_CUSTOM_TEXT',NULL,'2018-07-12 09:42:57','2018-07-12 09:42:57'),(426,NULL,NULL,'BLOCKSOCIAL_LINE',NULL,'2018-07-12 17:35:24','2018-12-09 20:47:06'),(427,NULL,NULL,'BLOCKSOCIAL_WEIBO','https://weibo.com','2018-07-12 17:35:24','2018-10-08 21:07:12'),(429,NULL,NULL,'PS_SC_LINE','1','2018-07-12 17:36:17','2018-07-12 17:36:17'),(430,NULL,NULL,'PS_SC_WEIBO','1','2018-07-12 17:36:17','2018-07-12 17:36:17'),(431,NULL,NULL,'PS_SHOP_DETAILS','星期一：14:00-21:30\r\n星期二～日：11:30-21:30\r\n','2018-07-12 18:39:42','2018-09-06 23:04:02'),(432,NULL,NULL,'PS_SHOP_ADDR1','信義路五段7號','2018-07-12 18:39:42','2018-09-07 00:29:53'),(433,NULL,NULL,'PS_SHOP_ADDR2',NULL,'2018-07-12 18:39:42','2018-07-12 18:44:06'),(434,NULL,NULL,'PS_SHOP_CODE','114','2018-07-12 18:39:42','2018-09-06 23:00:01'),(435,NULL,NULL,'PS_SHOP_CITY','台北市','2018-07-12 18:39:42','2018-09-06 23:00:20'),(436,NULL,NULL,'PS_SHOP_COUNTRY_ID','203','2018-07-12 18:39:42','2018-07-12 18:39:42'),(437,NULL,NULL,'PS_SHOP_COUNTRY','台灣','2018-07-12 18:39:42','2018-10-15 23:12:07'),(438,NULL,NULL,'PS_SHOP_PHONE','0928888888','2018-07-12 18:39:42','2018-07-12 18:42:24'),(439,NULL,NULL,'PS_SHOP_FAX',NULL,'2018-07-12 18:39:42','2018-07-12 18:39:42'),(440,NULL,NULL,'BLOCK_CATEG_SORT_WAY','0','2018-07-12 23:04:21','2018-07-12 23:04:21'),(441,NULL,NULL,'BLOCK_CATEG_SORT','0','2018-07-12 23:04:21','2018-07-12 23:04:21'),(442,NULL,NULL,'SIMPLICITY_FB_APP_ID',NULL,'2018-07-13 01:40:39','2018-07-13 01:40:39'),(443,NULL,NULL,'SMILEPAY_C2CUP_711_CARRIER_ID','103','2018-07-13 03:44:36','2018-07-14 11:21:17'),(444,NULL,NULL,'SMILEPAY_C2CUP_711_CARRIER_ID_REF','103','2018-07-13 03:44:36','2018-07-13 03:44:36'),(445,NULL,NULL,'SMILEPAY_C2CUP_FAMI_CARRIER_ID','105','2018-07-13 03:44:36','2018-07-14 11:21:26'),(446,NULL,NULL,'SMILEPAY_C2CUP_FAMI_CARRIER_ID_REF','105','2018-07-13 03:44:36','2018-07-13 03:44:36'),(447,NULL,NULL,'SMILEPAY_PALMBOXC2CUP_NORMAL_CARRIER_ID','113','2018-07-13 03:44:44','2018-07-14 11:22:05'),(448,NULL,NULL,'SMILEPAY_PALMBOXC2CUP_NORMAL_CARRIER_ID_REF','5','2018-07-13 03:44:44','2018-07-13 03:44:44'),(449,NULL,NULL,'CONF_SMILEPAY_C2C_FIXED','0.2','2018-07-13 03:44:51','2018-07-13 03:44:51'),(450,NULL,NULL,'CONF_SMILEPAY_C2C_VAR','2','2018-07-13 03:44:51','2018-07-13 03:44:51'),(451,NULL,NULL,'CONF_SMILEPAY_C2C_FIXED_FOREIGN','0.2','2018-07-13 03:44:51','2018-07-13 03:44:51'),(452,NULL,NULL,'CONF_SMILEPAY_C2C_VAR_FOREIGN','2','2018-07-13 03:44:51','2018-07-13 03:44:51'),(453,NULL,NULL,'SMILEPAY_C2CP_711_CARRIER_ID','104','2018-07-13 03:44:51','2018-07-14 11:21:21'),(454,NULL,NULL,'SMILEPAY_C2CP_711_CARRIER_ID_REF','6','2018-07-13 03:44:51','2018-07-13 03:44:51'),(455,NULL,NULL,'SMILEPAY_C2CP_FAMI_CARRIER_ID','106','2018-07-13 03:44:51','2018-07-14 11:21:31'),(456,NULL,NULL,'SMILEPAY_C2CP_FAMI_CARRIER_ID_REF','7','2018-07-13 03:44:51','2018-07-13 03:44:51'),(457,NULL,NULL,'CONF_SMILEPAY_IBON_FIXED','0.2','2018-07-13 03:44:59','2018-07-13 03:44:59'),(458,NULL,NULL,'CONF_SMILEPAY_IBON_VAR','2','2018-07-13 03:44:59','2018-07-13 03:44:59'),(459,NULL,NULL,'CONF_SMILEPAY_IBON_FIXED_FOREIGN','0.2','2018-07-13 03:44:59','2018-07-13 03:44:59'),(460,NULL,NULL,'CONF_SMILEPAY_IBON_VAR_FOREIGN','2','2018-07-13 03:44:59','2018-07-13 03:44:59'),(461,NULL,NULL,'CONF_SMILEPAY_FAMIPORT_FIXED','0.2','2018-07-13 03:45:05','2018-07-13 03:45:05'),(462,NULL,NULL,'CONF_SMILEPAY_FAMIPORT_VAR','2','2018-07-13 03:45:05','2018-07-13 03:45:05'),(463,NULL,NULL,'CONF_SMILEPAY_FAMIPORT_FIXED_FOREIGN','0.2','2018-07-13 03:45:05','2018-07-13 03:45:05'),(464,NULL,NULL,'CONF_SMILEPAY_FAMIPORT_VAR_FOREIGN','2','2018-07-13 03:45:05','2018-07-13 03:45:05'),(465,NULL,NULL,'SMILEPAY_EZCATUP_NORMAL_CARRIER_ID','107','2018-07-13 03:45:13','2018-07-14 11:21:36'),(466,NULL,NULL,'SMILEPAY_EZCATUP_NORMAL_CARRIER_ID_REF','8','2018-07-13 03:45:13','2018-07-13 03:45:13'),(467,NULL,NULL,'SMILEPAY_EZCATUP_FRIDGE_CARRIER_ID','108','2018-07-13 03:45:13','2018-07-14 11:21:41'),(468,NULL,NULL,'SMILEPAY_EZCATUP_FRIDGE_CARRIER_ID_REF','9','2018-07-13 03:45:13','2018-07-13 03:45:13'),(469,NULL,NULL,'SMILEPAY_EZCATUP_FREEZE_CARRIER_ID','109','2018-07-13 03:45:13','2018-07-14 11:21:46'),(470,NULL,NULL,'SMILEPAY_EZCATUP_FREEZE_CARRIER_ID_REF','10','2018-07-13 03:45:13','2018-07-13 03:45:13'),(471,NULL,NULL,'CONF_SMILEPAY_EZCAT_FIXED','0.2','2018-07-13 03:45:20','2018-07-13 03:45:20'),(472,NULL,NULL,'CONF_SMILEPAY_EZCAT_VAR','2','2018-07-13 03:45:20','2018-07-13 03:45:20'),(473,NULL,NULL,'CONF_SMILEPAY_EZCAT_FIXED_FOREIGN','0.2','2018-07-13 03:45:20','2018-07-13 03:45:20'),(474,NULL,NULL,'CONF_SMILEPAY_EZCAT_VAR_FOREIGN','2','2018-07-13 03:45:20','2018-07-13 03:45:20'),(475,NULL,NULL,'SMILEPAY_EZCATP_NORMAL_CARRIER_ID','110','2018-07-13 03:45:20','2018-07-14 11:21:50'),(476,NULL,NULL,'SMILEPAY_EZCATP_NORMAL_CARRIER_ID_REF','110','2018-07-13 03:45:20','2018-07-13 03:45:20'),(477,NULL,NULL,'SMILEPAY_EZCATP_FRIDGE_CARRIER_ID','111','2018-07-13 03:45:20','2018-07-14 11:21:56'),(478,NULL,NULL,'SMILEPAY_EZCATP_FRIDGE_CARRIER_ID_REF','111','2018-07-13 03:45:20','2018-07-13 03:45:20'),(479,NULL,NULL,'SMILEPAY_EZCATP_FREEZE_CARRIER_ID','112','2018-07-13 03:45:20','2018-07-14 11:22:01'),(480,NULL,NULL,'SMILEPAY_EZCATP_FREEZE_CARRIER_ID_REF','112','2018-07-13 03:45:20','2018-07-13 03:45:20'),(481,NULL,NULL,'CONF_SMILEPAY_CSV_FIXED','0.2','2018-07-13 03:45:29','2018-07-13 03:45:29'),(482,NULL,NULL,'CONF_SMILEPAY_CSV_VAR','2','2018-07-13 03:45:29','2018-07-13 03:45:29'),(483,NULL,NULL,'CONF_SMILEPAY_CSV_FIXED_FOREIGN','0.2','2018-07-13 03:45:29','2018-07-13 03:45:29'),(484,NULL,NULL,'CONF_SMILEPAY_CSV_VAR_FOREIGN','2','2018-07-13 03:45:29','2018-07-13 03:45:29'),(485,NULL,NULL,'CONF_SMILEPAY_CREDIT_FIXED','0.2','2018-07-13 03:45:37','2018-07-13 03:45:37'),(486,NULL,NULL,'CONF_SMILEPAY_CREDIT_VAR','2','2018-07-13 03:45:37','2018-07-13 03:45:37'),(487,NULL,NULL,'CONF_SMILEPAY_CREDIT_FIXED_FOREIGN','0.2','2018-07-13 03:45:37','2018-07-13 03:45:37'),(488,NULL,NULL,'CONF_SMILEPAY_CREDIT_VAR_FOREIGN','2','2018-07-13 03:45:37','2018-07-13 03:45:37'),(489,NULL,NULL,'CONF_SMILEPAY_ATM_FIXED','0.2','2018-07-13 03:45:43','2018-07-13 03:45:43'),(490,NULL,NULL,'CONF_SMILEPAY_ATM_VAR','2','2018-07-13 03:45:43','2018-07-13 03:45:43'),(491,NULL,NULL,'CONF_SMILEPAY_ATM_FIXED_FOREIGN','0.2','2018-07-13 03:45:43','2018-07-13 03:45:43'),(492,NULL,NULL,'CONF_SMILEPAY_ATM_VAR_FOREIGN','2','2018-07-13 03:45:43','2018-07-13 03:45:43'),(493,NULL,NULL,'SMILEPAY_c2c_DCVC','8182','2018-07-13 03:48:44','2018-07-13 03:48:44'),(494,NULL,NULL,'SMILEPAY_c2c_MID','0000','2018-07-13 03:48:44','2018-07-13 03:48:44'),(495,NULL,NULL,'SMILEPAY_c2c_Rvg2c','1','2018-07-13 03:48:44','2018-07-13 03:48:44'),(496,NULL,NULL,'SMILEPAY_c2c_VKey','7C3B96E4ECB899E9CF5BB784CF8D684E','2018-07-13 03:48:44','2018-07-13 03:48:44'),(497,NULL,NULL,'SMILEPAY_c2c_paymentName','超商取貨付款','2018-07-13 03:48:44','2018-07-14 13:05:46'),(498,NULL,NULL,'SMILEPAY_c2c_Spdesc','於超商門市取貨同時付款給小7店員。','2018-07-13 03:48:44','2018-08-01 16:54:50'),(499,NULL,NULL,'PRICE_DISPLAY_METHOD','1','2018-07-13 14:32:13','2018-07-13 14:32:13'),(500,NULL,NULL,'PS_REFERRERS_CACHE_LIKE',' \'2018-06-11 00:00:00\' AND \'2018-07-11 23:59:59\' ','2018-07-13 17:33:59','2018-07-13 17:33:59'),(501,NULL,NULL,'PS_REFERRERS_CACHE_DATE','2018-07-13 17:33:59','2018-07-13 17:33:59','2018-07-13 17:33:59'),(502,NULL,NULL,'SMILEPAY_famiport_DCVC','8182','2018-07-14 12:39:39','2018-07-14 12:39:39'),(503,NULL,NULL,'SMILEPAY_famiport_MID','0000','2018-07-14 12:39:39','2018-07-14 12:39:39'),(504,NULL,NULL,'SMILEPAY_famiport_VKey','7C3B96E4ECB899E9CF5BB784CF8D684E','2018-07-14 12:39:39','2018-07-14 12:39:39'),(505,NULL,NULL,'SMILEPAY_famiport_Rvg2c','1','2018-07-14 12:39:39','2018-07-14 12:39:39'),(506,NULL,NULL,'SMILEPAY_famiport_paymentName','FamiPort 全家繳費代碼','2018-07-14 12:39:39','2018-08-01 16:55:30'),(507,NULL,NULL,'SMILEPAY_famiport_Spdesc','攜帶 FamiPort 繳費代碼至全家超商繳費，無須列印帳單。','2018-07-14 12:39:39','2018-08-01 16:56:34'),(508,NULL,NULL,'SMILEPAY_ezcatup_DCVC','8182','2018-07-14 12:40:16','2018-07-14 12:40:16'),(509,NULL,NULL,'SMILEPAY_ezcatup_Rvg2c','1','2018-07-14 12:40:16','2018-07-14 12:40:16'),(510,NULL,NULL,'SMILEPAY_ezcatup_VKey','7C3B96E4ECB899E9CF5BB784CF8D684E','2018-07-14 12:40:16','2018-07-14 12:40:16'),(511,NULL,NULL,'SMILEPAY_c2cup_DCVC','8182','2018-07-14 12:40:43','2018-07-14 12:40:43'),(512,NULL,NULL,'SMILEPAY_c2cup_Rvg2c','1','2018-07-14 12:40:43','2018-07-14 12:40:43'),(513,NULL,NULL,'SMILEPAY_c2cup_VKey','7C3B96E4ECB899E9CF5BB784CF8D684E','2018-07-14 12:40:43','2018-07-14 12:40:43'),(514,NULL,NULL,'SMILEPAY_credit_DCVC','8182','2018-07-14 12:41:14','2018-07-14 12:41:14'),(515,NULL,NULL,'SMILEPAY_credit_Rvg2c','1','2018-07-14 12:41:14','2018-07-14 12:41:14'),(516,NULL,NULL,'SMILEPAY_credit_MID','0000','2018-07-14 12:41:14','2018-07-14 13:19:54'),(517,NULL,NULL,'SMILEPAY_credit_paymentName','線上刷卡','2018-07-14 12:41:14','2018-07-14 13:04:42'),(518,NULL,NULL,'SMILEPAY_credit_Spdesc','支援 VISA、MasterCard、JCB、銀聯卡','2018-07-14 12:41:14','2018-08-01 16:52:53'),(519,NULL,NULL,'SMILEPAY_ATM_DCVC','8182','2018-07-14 12:41:44','2018-07-14 12:41:44'),(520,NULL,NULL,'SMILEPAY_ATM_MID','0000','2018-07-14 12:41:44','2018-07-14 12:41:44'),(521,NULL,NULL,'SMILEPAY_ATM_Rvg2c','1','2018-07-14 12:41:44','2018-07-14 12:41:44'),(522,NULL,NULL,'SMILEPAY_ATM_VKey','7C3B96E4ECB899E9CF5BB784CF8D684E','2018-07-14 12:41:44','2018-07-14 12:41:44'),(523,NULL,NULL,'SMILEPAY_ATM_paymentName','ATM虛擬帳號','2018-07-14 12:41:44','2018-07-14 13:05:58'),(524,NULL,NULL,'SMILEPAY_ATM_Spdesc','取得「16 碼繳虛擬帳號」至臨櫃繳款、ATM 櫃員機轉帳、Web ATM 轉帳（三選一）。','2018-07-14 12:41:44','2018-08-01 17:03:33'),(525,NULL,NULL,'SMILEPAY_palmboxc2cup_DCVC','8182','2018-07-14 12:42:33','2018-07-14 12:42:33'),(526,NULL,NULL,'SMILEPAY_palmboxc2cup_Rvg2c','1','2018-07-14 12:42:33','2018-07-14 12:42:33'),(527,NULL,NULL,'SMILEPAY_palmboxc2cup_VKey','7C3B96E4ECB899E9CF5BB784CF8D684E','2018-07-14 12:42:33','2018-07-14 12:42:33'),(528,NULL,NULL,'SMILEPAY_ibon_DCVC','8182','2018-07-14 12:43:09','2018-07-14 12:43:09'),(529,NULL,NULL,'SMILEPAY_ibon_Rvg2c','1','2018-07-14 12:43:09','2018-07-14 12:43:09'),(530,NULL,NULL,'SMILEPAY_ibon_VKey','7C3B96E4ECB899E9CF5BB784CF8D684E','2018-07-14 12:43:09','2018-07-14 12:43:09'),(531,NULL,NULL,'SMILEPAY_ibon_MID','0000','2018-07-14 12:43:09','2018-08-01 19:31:38'),(532,NULL,NULL,'SMILEPAY_ibon_paymentName','7-11 ibon 繳費代碼','2018-07-14 12:43:09','2018-07-14 13:06:39'),(533,NULL,NULL,'SMILEPAY_ibon_Spdesc','攜帶 ibon 繳費代碼至 7-11 繳費，無須列印帳單。','2018-07-14 12:43:09','2018-08-01 16:56:14'),(534,NULL,NULL,'SMILEPAY_ezcat_DCVC','8182','2018-07-14 13:44:50','2018-07-14 13:44:50'),(535,NULL,NULL,'SMILEPAY_ezcat_MID','0000','2018-07-14 13:44:50','2018-08-01 19:31:02'),(536,NULL,NULL,'SMILEPAY_ezcat_Rvg2c','1','2018-07-14 13:44:50','2018-07-14 13:44:50'),(537,NULL,NULL,'SMILEPAY_ezcat_VKey','7C3B96E4ECB899E9CF5BB784CF8D684E','2018-07-14 13:44:50','2018-07-14 13:44:50'),(538,NULL,NULL,'SMILEPAY_ezcat_paymentName','黑貓貨到付現','2018-07-14 13:44:50','2018-07-14 13:44:50'),(539,NULL,NULL,'SMILEPAY_ezcat_Spdesc','商品配送至買家指定地址，貨到付現給宅配人員。','2018-07-14 13:44:50','2018-08-01 16:58:19'),(540,NULL,NULL,'SMILEPAY_csv_DCVC','8182','2018-07-14 13:45:30','2018-07-14 13:45:30'),(541,NULL,NULL,'SMILEPAY_csv_Rvg2c','1','2018-07-14 13:45:30','2018-07-14 13:45:30'),(542,NULL,NULL,'SMILEPAY_csv_VKey','7C3B96E4ECB899E9CF5BB784CF8D684E','2018-07-14 13:45:30','2018-07-14 13:45:30'),(543,NULL,NULL,'SMILEPAY_csv_MID','0000','2018-07-14 13:45:30','2018-07-14 13:45:30'),(544,NULL,NULL,'SMILEPAY_csv_StorePhone','0928000000','2018-07-14 13:45:30','2018-07-14 13:48:47'),(545,NULL,NULL,'SMILEPAY_csv_paymentName','超商帳單繳費','2018-07-14 13:45:30','2018-07-14 13:45:30'),(546,NULL,NULL,'SMILEPAY_csv_Spdesc','線上列印帳單後至全國超商（7-11、全家、OK、萊爾富）繳費。','2018-07-14 13:45:30','2018-08-01 16:57:04'),(549,NULL,NULL,'SIMPLICITY_CMSBLOCK_ID','7','2018-07-15 02:10:57','2018-10-08 13:06:10'),(550,NULL,NULL,'updatev_cmshomepage','1.4.1','2018-07-15 02:11:01','2018-07-15 02:11:01'),(551,NULL,NULL,'PS_LAST_VERSION_CHECK','1534677915','2018-07-20 03:14:19','2018-08-19 19:25:15'),(552,NULL,NULL,'PS_UPGRADE_CHANNEL','major','2018-07-20 03:14:22','2018-07-20 03:14:22'),(553,NULL,NULL,'PS_AUTOUP_UPDATE_DEFAULT_THEME','1','2018-07-20 03:14:22','2018-07-20 03:14:22'),(554,NULL,NULL,'PS_AUTOUP_CHANGE_DEFAULT_THEME','0','2018-07-20 03:14:22','2018-07-20 03:14:22'),(555,NULL,NULL,'PS_AUTOUP_KEEP_MAILS','0','2018-07-20 03:14:22','2018-07-20 03:14:22'),(556,NULL,NULL,'PS_AUTOUP_CUSTOM_MOD_DESACT','1','2018-07-20 03:14:22','2018-07-20 03:14:22'),(557,NULL,NULL,'PS_AUTOUP_MANUAL_MODE','0','2018-07-20 03:14:22','2018-07-20 03:14:22'),(558,NULL,NULL,'PS_AUTOUP_PERFORMANCE','1','2018-07-20 03:14:22','2018-07-20 03:14:22'),(559,NULL,NULL,'PS_DISPLAY_ERRORS','0','2018-07-20 03:14:22','2018-07-20 03:14:22'),(560,NULL,NULL,'PS_MAIL_EMAIL_MESSAGE','1','2018-07-28 04:58:21','2018-09-11 14:52:02'),(561,NULL,NULL,'PS_MAIL_DOMAIN',NULL,'2018-07-28 04:58:21','2018-07-28 04:58:21'),(562,NULL,NULL,'PS_INVOICE_TAXES_BREAKDOWN','0','2018-08-01 21:49:21','2018-08-01 21:49:21'),(563,NULL,NULL,'PS_PDF_IMG_INVOICE','0','2018-08-01 21:49:21','2018-08-01 21:49:21'),(564,NULL,NULL,'PS_INVOICE_USE_YEAR','0','2018-08-01 21:49:21','2018-08-01 21:49:21'),(565,NULL,NULL,'PS_INVOICE_RESET','0','2018-08-01 21:49:21','2018-08-01 21:49:21'),(566,NULL,NULL,'PS_INVOICE_YEAR_POS','0','2018-08-01 21:49:21','2018-08-01 21:49:21'),(567,NULL,NULL,'PS_INVOICE_START_NUMBER','0','2018-08-01 21:49:21','2018-08-01 21:49:21'),(568,NULL,NULL,'PS_INVOICE_LEGAL_FREE_TEXT',NULL,'2018-08-01 21:49:21','2018-08-01 21:49:21'),(569,NULL,NULL,'PS_INVOICE_FREE_TEXT',NULL,'2018-08-01 21:49:21','2018-08-01 21:49:21'),(570,NULL,NULL,'PS_PDF_USE_CACHE','0','2018-08-01 21:49:21','2018-08-01 21:49:21'),(571,NULL,NULL,'SIMPLICITY_FB_MESSAGING_TOKEN','85523ba310509e4a6db0471c502b9c1f','2018-08-19 17:38:03','2018-08-19 17:38:03'),(576,NULL,NULL,'SIMPLICITY_FB_PAGE_ID','2','2018-08-19 17:38:03','2018-08-25 01:06:45'),(577,NULL,NULL,'SIMPLICITY_FB_PAGE_TOKEN','3','2018-08-19 17:38:03','2018-08-25 01:06:45'),(589,NULL,NULL,'SIMPLICITY_HEADERBAR_HTML',NULL,'2018-08-26 16:46:00','2018-08-26 16:46:00'),(590,NULL,NULL,'GSITEMAP_PRIORITY_HOME','1','2018-08-26 23:26:44','2018-08-26 23:26:44'),(591,NULL,NULL,'GSITEMAP_PRIORITY_PRODUCT','0.9','2018-08-26 23:26:44','2018-08-26 23:26:44'),(592,NULL,NULL,'GSITEMAP_PRIORITY_CATEGORY','0.8','2018-08-26 23:26:44','2018-08-26 23:26:44'),(593,NULL,NULL,'GSITEMAP_PRIORITY_MANUFACTURER','0.7','2018-08-26 23:26:44','2018-08-26 23:26:44'),(594,NULL,NULL,'GSITEMAP_PRIORITY_SUPPLIER','0.6','2018-08-26 23:26:44','2018-08-26 23:26:44'),(595,NULL,NULL,'GSITEMAP_PRIORITY_CMS','0.5','2018-08-26 23:26:44','2018-08-26 23:26:44'),(596,NULL,NULL,'GSITEMAP_FREQUENCY','weekly','2018-08-26 23:26:44','2018-08-26 23:26:44'),(597,NULL,NULL,'GSITEMAP_INDEX_CHECK',NULL,'2018-08-26 23:30:27','2018-08-26 23:30:27'),(598,NULL,NULL,'GSITEMAP_CHECK_IMAGE_FILE',NULL,'2018-08-26 23:30:27','2018-08-26 23:30:27'),(599,NULL,NULL,'GSITEMAP_DISABLE_LINKS',NULL,'2018-08-26 23:30:27','2018-10-10 19:54:53'),(600,NULL,NULL,'GSITEMAP_LAST_EXPORT','Mon, 12 Nov 2018 17:11:22 +0800','2018-08-26 23:30:28','2018-11-12 17:11:22'),(601,NULL,NULL,'SIMPLICITY_FB_MESSAGING_TEST_IP','1','2018-09-07 16:46:00','2018-09-11 01:00:15'),(602,NULL,NULL,'SIMPLICITY_FB_MESSAGING_ACTIVE','0','2018-09-07 16:46:00','2018-09-07 16:26:56'),(603,NULL,NULL,'PS_LOGO_MAIL','logo_mail.jpg','2018-09-13 01:33:40','2018-09-13 03:01:17'),(604,NULL,NULL,'PS_IMAGE_GENERATION_METHOD','0','2018-09-13 02:57:01','2018-09-26 16:50:49'),(605,NULL,NULL,'PS_HIGHT_DPI','0','2018-09-13 02:57:01','2018-09-20 15:29:54'),(606,NULL,NULL,'CONF_PAYPAL_FIXED','0.2','2018-09-18 15:05:37','2018-09-18 15:05:37'),(607,NULL,NULL,'CONF_PAYPAL_VAR','2','2018-09-18 15:05:37','2018-09-18 15:05:37'),(608,NULL,NULL,'CONF_PAYPAL_FIXED_FOREIGN','0.2','2018-09-18 15:05:37','2018-09-18 15:05:37'),(609,NULL,NULL,'CONF_PAYPAL_VAR_FOREIGN','2','2018-09-18 15:05:37','2018-09-18 15:05:37'),(610,NULL,NULL,'PAYPAL_OS_WAITING','23','2018-09-18 15:05:37','2018-09-18 15:05:37'),(611,NULL,NULL,'PAYPAL_BRAINTREE_OS_AWAITING','24','2018-09-18 15:05:37','2018-09-18 15:05:37'),(612,NULL,NULL,'PAYPAL_BRAINTREE_OS_AWAITING_VALIDATION','25','2018-09-18 15:05:37','2018-09-18 15:05:37'),(613,NULL,NULL,'PAYPAL_MERCHANT_ID_SANDBOX',NULL,'2018-09-18 15:05:37','2018-09-18 15:05:37'),(614,NULL,NULL,'PAYPAL_MERCHANT_ID_LIVE',NULL,'2018-09-18 15:05:37','2018-09-18 15:05:37'),(615,NULL,NULL,'PAYPAL_USERNAME_SANDBOX',NULL,'2018-09-18 15:05:37','2018-09-18 15:05:37'),(616,NULL,NULL,'PAYPAL_PSWD_SANDBOX',NULL,'2018-09-18 15:05:37','2018-09-18 15:05:37'),(617,NULL,NULL,'PAYPAL_SIGNATURE_SANDBOX',NULL,'2018-09-18 15:05:37','2018-09-18 15:05:37'),(618,NULL,NULL,'PAYPAL_SANDBOX_ACCESS','0','2018-09-18 15:05:37','2018-09-18 15:05:37'),(619,NULL,NULL,'PAYPAL_USERNAME_LIVE',NULL,'2018-09-18 15:05:37','2018-09-18 15:05:37'),(620,NULL,NULL,'PAYPAL_PSWD_LIVE',NULL,'2018-09-18 15:05:37','2018-09-18 15:05:37'),(621,NULL,NULL,'PAYPAL_SIGNATURE_LIVE',NULL,'2018-09-18 15:05:37','2018-09-18 15:05:37'),(622,NULL,NULL,'PAYPAL_LIVE_ACCESS','0','2018-09-18 15:05:37','2018-09-18 15:05:37'),(623,NULL,NULL,'PAYPAL_SANDBOX','0','2018-09-18 15:05:37','2018-09-18 15:05:37'),(624,NULL,NULL,'PAYPAL_API_INTENT','sale','2018-09-18 15:05:37','2018-09-18 15:05:37'),(625,NULL,NULL,'PAYPAL_API_ADVANTAGES','1','2018-09-18 15:05:37','2018-09-18 15:05:37'),(626,NULL,NULL,'PAYPAL_API_CARD','0','2018-09-18 15:05:37','2018-09-18 15:05:37'),(627,NULL,NULL,'PAYPAL_METHOD',NULL,'2018-09-18 15:05:37','2018-09-18 15:05:37'),(628,NULL,NULL,'PAYPAL_EXPRESS_CHECKOUT_SHORTCUT','0','2018-09-18 15:05:37','2018-09-18 15:05:37'),(629,NULL,NULL,'PAYPAL_EXPRESS_CHECKOUT_SHORTCUT_CART','1','2018-09-18 15:05:37','2018-09-18 15:05:37'),(630,NULL,NULL,'PAYPAL_CRON_TIME','2018-09-18 15:09:37','2018-09-18 15:05:37','2018-09-18 15:05:37'),(631,NULL,NULL,'PAYPAL_BY_BRAINTREE','0','2018-09-18 15:05:37','2018-09-18 15:05:37'),(632,NULL,NULL,'PAYPAL_EXPRESS_CHECKOUT_IN_CONTEXT','0','2018-09-18 15:05:37','2018-09-18 15:05:37'),(633,NULL,NULL,'PAYPAL_VAULTING','0','2018-09-18 15:05:37','2018-09-18 15:05:37'),(634,NULL,NULL,'SIMPLICITY_TINYPNG_API_KEY_1',NULL,'2018-09-20 15:00:00','2018-09-26 17:27:26'),(635,NULL,NULL,'SIMPLICITY_IMAGE_USE_CROP','0','2018-09-20 15:00:00','2018-09-26 16:50:37'),(636,NULL,NULL,'CONTACTFORM_SEND_CONFIRMATION_EMAIL','0','2018-09-27 01:29:03','2018-09-27 01:29:03'),(637,NULL,NULL,'CONTACTFORM_SEND_NOTIFICATION_EMAIL','0','2018-09-27 01:29:03','2018-09-27 01:29:03'),(639,NULL,NULL,'SIMPLICITY_GTM_GUA_SITE_SPEED_SAMPLE_RATE','1','2018-09-27 23:44:54','2018-10-09 17:48:05'),(640,NULL,NULL,'SIMPLICITY_GTM_GUA_ECOMM_PRODID','1','2018-09-27 23:44:54','2018-09-27 23:44:54'),(641,NULL,NULL,'SIMPLICITY_GTM_GUA_ECOMM_PAGETYPE','2','2018-09-27 23:44:54','2018-09-27 23:44:54'),(642,NULL,NULL,'SIMPLICITY_GTM_GUA_ECOMM_TOTALVALUE','3','2018-09-27 23:44:54','2018-09-27 23:44:54'),(643,NULL,NULL,'SIMPLICITY_GTM_GUA_ECOMM_CATEGORY','4','2018-09-27 23:44:54','2018-09-27 23:44:54'),(644,NULL,NULL,'SIMPLICITY_GTM_EXCLUDED_ORDER_STATES','6,8,7','2018-09-27 23:44:54','2018-09-28 02:15:47'),(645,NULL,NULL,'SIMPLICITY_GTM_REFUND_ORDER_STATES','6,7','2018-09-27 23:44:54','2018-09-27 23:44:54'),(650,NULL,NULL,'SIMPLICITY_GTM_NO_BO_TRACK',NULL,'2018-09-28 02:15:47','2018-09-28 02:15:47'),(651,NULL,NULL,'SIMPLICITY_GTM_DO_NOT_TRACK','1','2018-09-28 02:15:47','2018-10-09 17:48:00'),(652,NULL,NULL,'SIMPLICITY_GTM_ID',NULL,'2018-09-28 02:15:47','2018-09-28 02:15:47'),(653,NULL,NULL,'SIMPLICITY_GTM_GUA_ID',NULL,'2018-09-28 02:15:47','2018-09-28 02:18:18'),(654,NULL,NULL,'SIMPLICITY_GTM_GUA_ANONYMIZE_IP',NULL,'2018-09-28 02:15:47','2018-09-28 02:15:47'),(655,NULL,NULL,'SIMPLICITY_GTM_GUA_UNIFY_USER_ID',NULL,'2018-09-28 02:15:47','2018-10-09 18:01:38'),(657,NULL,NULL,'SIMPLICITY_GTM_GUA_DYNAMIC_REMARKETING',NULL,'2018-09-28 02:15:47','2018-09-28 02:15:47'),(660,NULL,NULL,'SIMPLICITY_GTM_GUA_MERCHANT_VARIANT',NULL,'2018-09-28 02:15:47','2018-09-28 02:15:47'),(662,NULL,NULL,'SIMPLICITY_GTM_ADWORDS_ID',NULL,'2018-09-28 02:15:47','2018-09-28 02:19:12'),(663,NULL,NULL,'SIMPLICITY_GTM_ADWORDS_LABEL',NULL,'2018-09-28 02:15:47','2018-09-28 02:19:12'),(665,NULL,NULL,'SIMPLICITY_GTM_FACEBOOK_ID',NULL,'2018-09-28 02:15:47','2018-09-28 02:15:47'),(668,NULL,NULL,'SIMPLICITY_GTM_FACEBOOK_CATALOG_VARIANT',NULL,'2018-09-28 02:15:47','2018-09-28 02:15:47'),(676,NULL,NULL,'BLOCKSOCIAL_FLICKR','https://www.flickr.com','2018-10-08 11:08:32','2018-10-08 21:07:12'),(677,NULL,NULL,'BLOCKSOCIAL_ETSY','https://www.etsy.com','2018-10-08 11:08:32','2018-10-08 21:07:12'),(678,NULL,NULL,'SIMPLICITY_GTM_NO_BO_TRACKING',NULL,'2018-10-09 18:01:38','2018-10-09 18:01:38'),(679,NULL,NULL,'PS_MULTISHOP_FEATURE_ACTIVE',NULL,'2018-10-15 23:12:39','2018-10-15 23:14:09'),(683,1,1,'PS_REFERRERS_CACHE_LIKE',' \'2018-01-01 00:00:00\' AND \'2018-10-09 23:59:59\' ','2018-10-19 02:17:03','2018-10-19 02:17:03'),(684,1,1,'PS_REFERRERS_CACHE_DATE','2018-10-19 02:17:03','2018-10-19 02:17:03','2018-10-19 02:17:03'),(685,NULL,NULL,'PS_FAVICON_IPHONE','touch-icon-iphone.png','2018-10-19 18:00:00','2018-10-19 18:00:00'),(686,NULL,NULL,'PS_FAVICON_IPAD','touch-icon-ipad.png','2018-10-19 18:00:00','2018-10-19 18:00:00'),(687,NULL,NULL,'PS_FAVICON_IPAD_RETINA','touch-icon-ipad-retina.png','2018-10-19 18:00:00','2018-10-19 18:00:00'),(688,NULL,NULL,'PS_FAVICON_IPHONE_RETINA','touch-icon-iphone-retina.png','2018-10-19 18:00:00','2018-10-19 18:00:00'),(689,NULL,NULL,'PS_FAVICON_ANDROID','android-icon.png','2018-10-19 18:00:00','2018-10-19 18:00:00'),(690,NULL,NULL,'PS_FAVICON_MICROSOFT','microsoft-icon.png','2018-10-19 18:00:00','2018-10-19 18:00:00'),(691,NULL,NULL,'TC_THEME_COLOR_SIMPLICITY','#67C8C2','2018-10-19 18:00:00','2018-10-19 18:00:00'),(692,NULL,NULL,'TC_THEME_COLOR_SIMPLICITY_BLACK','#000','2018-10-19 18:00:00','2018-10-19 18:00:00'),(693,NULL,NULL,'TC_THEME_COLOR_SIMPLICITY_BLUE','#2fb5d2','2018-10-19 18:00:00','2018-10-19 18:00:00'),(694,NULL,NULL,'TC_THEME_COLOR_SIMPLICITY_PINK','#F0648F','2018-10-19 18:00:00','2018-10-19 18:00:00'),(695,NULL,NULL,'PS_MAINTENANCE_IP',NULL,'2018-10-22 22:43:34','2018-10-22 22:43:34'),(697,1,1,'PS_MAINTENANCE_TEXT',NULL,'2018-10-22 23:11:12','2018-10-22 23:11:12'),(698,1,1,'PS_SSL_ENABLED','1','2018-10-22 23:55:14','2018-10-23 00:18:14'),(699,1,1,'PS_SSL_ENABLED_EVERYWHERE','1','2018-10-22 23:55:14','2018-10-23 00:18:14'),(700,1,1,'PS_TOKEN_ENABLE','1','2018-10-22 23:55:14','2018-10-23 00:18:14'),(701,NULL,NULL,'PS_ALLOW_HTML_IFRAME','1','2018-10-22 23:55:14','2018-10-23 00:18:14'),(702,1,1,'PS_USE_HTMLPURIFIER','1','2018-10-22 23:55:14','2018-10-23 00:18:14'),(703,1,1,'PS_DISPLAY_SUPPLIERS',NULL,'2018-10-22 23:55:14','2018-10-23 00:18:14'),(704,1,1,'PS_DISPLAY_BEST_SELLERS','1','2018-10-22 23:55:14','2018-10-23 00:18:14'),(706,1,1,'PS_SHOP_ACTIVITY',NULL,'2018-10-22 23:55:14','2018-10-23 00:18:14'),(709,1,1,'SIMPLICITY_GTM_GUA_ANONYMIZE_IP','1','2018-10-26 13:43:20','2018-10-26 13:43:20'),(710,1,1,'SIMPLICITY_GTM_GUA_UNIFY_USER_ID','1','2018-10-26 13:43:20','2018-10-26 13:43:20'),(711,1,1,'SIMPLICITY_GTM_GUA_DYNAMIC_REMARKETING',NULL,'2018-10-26 13:43:20','2018-10-26 15:23:00'),(712,1,1,'SIMPLICITY_GTM_ADWORDS_ID',NULL,'2018-10-26 13:43:20','2018-10-26 15:23:00'),(713,1,1,'SIMPLICITY_GTM_ADWORDS_LABEL',NULL,'2018-10-26 13:43:20','2018-10-26 15:23:00'),(714,1,1,'SIMPLICITY_GTM_FACEBOOK_ID',NULL,'2018-10-26 14:09:03','2018-10-26 14:09:03'),(716,1,1,'SIMPLICITY_CMSBLOCK_ID','7','2018-10-27 16:00:27','2018-10-27 16:00:39'),(717,1,1,'BANK_WIRE_ADDRESS','OO銀行-XX分行','2018-10-28 19:39:41','2018-10-28 19:39:58'),(718,1,1,'BANK_WIRE_CUSTOM_TEXT',NULL,'2018-10-28 20:11:53','2018-10-28 20:11:53'),(719,NULL,NULL,'PS_SEARCH_WEIGHT_CMS_META_TITLE','5','2018-10-30 15:25:28','2018-10-30 15:25:28'),(720,NULL,NULL,'PS_SEARCH_WEIGHT_CMS_CONTENT','5','2018-10-30 15:26:33','2018-10-30 15:26:33'),(721,1,1,'PS_SMARTY_CACHE','1','2018-11-10 20:54:38','2018-11-10 21:38:50'),(722,1,1,'PS_SMARTY_LOCAL',NULL,'2018-11-10 20:54:38','2018-11-10 21:38:50'),(723,1,1,'PS_COMBINATION_FEATURE_ACTIVE','1','2018-11-10 20:54:38','2018-11-10 21:38:50'),(724,1,1,'PS_FEATURE_FEATURE_ACTIVE','1','2018-11-10 20:54:38','2018-11-10 21:38:50'),(725,1,1,'PS_GROUP_FEATURE_ACTIVE','1','2018-11-10 20:54:38','2018-11-10 21:38:50'),(731,NULL,NULL,'PS_CSS_THEME_CACHE',NULL,'2018-11-10 20:56:48','2018-11-13 10:32:18'),(732,NULL,NULL,'PS_JS_THEME_CACHE',NULL,'2018-11-10 21:05:39','2018-11-13 10:32:18'),(733,NULL,NULL,'PS_MEDIA_SERVER_1',NULL,'2018-11-11 10:47:41','2018-11-13 10:32:18'),(734,NULL,NULL,'PS_MEDIA_SERVER_2',NULL,'2018-11-11 10:47:41','2018-11-11 10:47:41'),(735,NULL,NULL,'PS_MEDIA_SERVER_3',NULL,'2018-11-11 10:47:41','2018-11-11 10:47:41'),(736,NULL,NULL,'PS_MEDIA_SERVERS','0','2018-11-11 10:47:41','2018-11-13 10:32:18'),(737,NULL,NULL,'TC_GCP_KEY','/var/bak/cdn_key.json','2018-11-11 11:11:11','2018-11-11 11:11:11'),(738,1,1,'PRODUCTS_VIEWED_NBR','24','2018-11-14 01:11:53','2018-11-14 01:11:53'),(739,1,1,'MOD_BLOCKTOPMENU_ITEMS','CAT7,CAT8,CAT14','2018-11-14 07:20:57','2018-11-14 07:21:33'),(740,1,1,'GSITEMAP_LAST_EXPORT','Wed, 14 Nov 2018 08:58:32 +0800','2018-11-14 08:58:32','2018-11-14 08:58:32'),(741,1,1,'SMILEPAY_c2c_Spdesc','於超商門市取貨同時付款，取件人需出示身份證件，請務必填寫真實姓名，讓門市人員核對無誤 ，方可取貨。','2018-11-14 13:46:57','2018-11-14 20:24:04'),(743,1,1,'SMILEPAY_ATM_Spdesc','取得「14 碼繳虛擬帳號」至臨櫃繳款、ATM 櫃員機轉帳、Web ATM 轉帳（三選一）。','2018-11-14 20:54:00','2018-11-14 20:54:00'),(745,1,1,'SMILEPAY_ibon_Spdesc','攜帶 ibon 繳費代碼至 7-11 繳費，無須列印帳單。','2018-11-14 23:04:18','2018-11-14 23:04:41'),(746,1,1,'PS_NB_DAYS_NEW_PRODUCT','100','2018-11-15 11:15:24','2018-11-15 11:15:54'),(749,NULL,NULL,'HOME_FEATURED_LABEL',NULL,'2018-11-15 13:53:20','2018-11-15 13:53:20'),(751,NULL,NULL,'PS_BLOCK_BESTSELLERS_LABEL',NULL,'2018-11-17 16:02:11','2018-11-17 16:02:11'),(752,NULL,NULL,'PS_NB_LABEL',NULL,'2018-11-17 16:09:57','2018-11-17 16:09:57'),(753,NULL,NULL,'BLOCKSPECIALS_LABEL',NULL,'2018-11-17 16:17:39','2018-11-17 16:17:39'),(754,NULL,NULL,'BLOCKSOCIAL_PINKOI','https://www.pinkoi.com','2018-12-09 17:40:32','2018-12-09 17:40:32'),(755,NULL,NULL,'BLOCKSOCIAL_LINKEDIN','https://www.linkedin.com','2018-12-09 17:40:32','2018-12-09 17:40:32');
/*!40000 ALTER TABLE `ps_configuration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_configuration_kpi`
--

DROP TABLE IF EXISTS `ps_configuration_kpi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_configuration_kpi` (
  `id_configuration_kpi` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_group` int(11) unsigned DEFAULT NULL,
  `id_shop` int(11) unsigned DEFAULT NULL,
  `name` varchar(64) NOT NULL,
  `value` text,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_configuration_kpi`),
  KEY `name` (`name`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_group` (`id_shop_group`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_configuration_kpi`
--

LOCK TABLES `ps_configuration_kpi` WRITE;
/*!40000 ALTER TABLE `ps_configuration_kpi` DISABLE KEYS */;
INSERT INTO `ps_configuration_kpi` VALUES (1,NULL,NULL,'DASHGOALS_TRAFFIC_01_2018','600','2018-07-11 11:58:21','2018-07-11 11:58:21'),(2,NULL,NULL,'DASHGOALS_CONVERSION_01_2018','2','2018-07-11 11:58:21','2018-07-11 11:58:21'),(3,NULL,NULL,'DASHGOALS_AVG_CART_VALUE_01_2018','80','2018-07-11 11:58:21','2018-07-11 11:58:21'),(4,NULL,NULL,'DASHGOALS_TRAFFIC_02_2018','600','2018-07-11 11:58:21','2018-07-11 11:58:21'),(5,NULL,NULL,'DASHGOALS_CONVERSION_02_2018','2','2018-07-11 11:58:21','2018-07-11 11:58:21'),(6,NULL,NULL,'DASHGOALS_AVG_CART_VALUE_02_2018','80','2018-07-11 11:58:21','2018-07-11 11:58:21'),(7,NULL,NULL,'DASHGOALS_TRAFFIC_03_2018','600','2018-07-11 11:58:21','2018-07-11 11:58:21'),(8,NULL,NULL,'DASHGOALS_CONVERSION_03_2018','2','2018-07-11 11:58:21','2018-07-11 11:58:21'),(9,NULL,NULL,'DASHGOALS_AVG_CART_VALUE_03_2018','80','2018-07-11 11:58:21','2018-07-11 11:58:21'),(10,NULL,NULL,'DASHGOALS_TRAFFIC_04_2018','600','2018-07-11 11:58:21','2018-07-11 11:58:21'),(11,NULL,NULL,'DASHGOALS_CONVERSION_04_2018','2','2018-07-11 11:58:21','2018-07-11 11:58:21'),(12,NULL,NULL,'DASHGOALS_AVG_CART_VALUE_04_2018','80','2018-07-11 11:58:21','2018-07-11 11:58:21'),(13,NULL,NULL,'DASHGOALS_TRAFFIC_05_2018','600','2018-07-11 11:58:21','2018-07-11 11:58:21'),(14,NULL,NULL,'DASHGOALS_CONVERSION_05_2018','2','2018-07-11 11:58:21','2018-07-11 11:58:21'),(15,NULL,NULL,'DASHGOALS_AVG_CART_VALUE_05_2018','80','2018-07-11 11:58:21','2018-07-11 11:58:21'),(16,NULL,NULL,'DASHGOALS_TRAFFIC_06_2018','600','2018-07-11 11:58:21','2018-07-11 11:58:21'),(17,NULL,NULL,'DASHGOALS_CONVERSION_06_2018','2','2018-07-11 11:58:21','2018-07-11 11:58:21'),(18,NULL,NULL,'DASHGOALS_AVG_CART_VALUE_06_2018','80','2018-07-11 11:58:21','2018-07-11 11:58:21'),(19,NULL,NULL,'DASHGOALS_TRAFFIC_07_2018','600','2018-07-11 11:58:21','2018-07-11 11:58:21'),(20,NULL,NULL,'DASHGOALS_CONVERSION_07_2018','2','2018-07-11 11:58:21','2018-07-11 11:58:21'),(21,NULL,NULL,'DASHGOALS_AVG_CART_VALUE_07_2018','80','2018-07-11 11:58:21','2018-07-11 11:58:21'),(22,NULL,NULL,'DASHGOALS_TRAFFIC_08_2018','600','2018-07-11 11:58:21','2018-07-11 11:58:21'),(23,NULL,NULL,'DASHGOALS_CONVERSION_08_2018','2','2018-07-11 11:58:21','2018-07-11 11:58:21'),(24,NULL,NULL,'DASHGOALS_AVG_CART_VALUE_08_2018','80','2018-07-11 11:58:21','2018-07-11 11:58:21'),(25,NULL,NULL,'DASHGOALS_TRAFFIC_09_2018','600','2018-07-11 11:58:21','2018-07-11 11:58:21'),(26,NULL,NULL,'DASHGOALS_CONVERSION_09_2018','2','2018-07-11 11:58:21','2018-07-11 11:58:21'),(27,NULL,NULL,'DASHGOALS_AVG_CART_VALUE_09_2018','80','2018-07-11 11:58:21','2018-07-11 11:58:21'),(28,NULL,NULL,'DASHGOALS_TRAFFIC_10_2018','600','2018-07-11 11:58:21','2018-07-11 11:58:21'),(29,NULL,NULL,'DASHGOALS_CONVERSION_10_2018','2','2018-07-11 11:58:21','2018-07-11 11:58:21'),(30,NULL,NULL,'DASHGOALS_AVG_CART_VALUE_10_2018','80','2018-07-11 11:58:21','2018-07-11 11:58:21'),(31,NULL,NULL,'DASHGOALS_TRAFFIC_11_2018','600','2018-07-11 11:58:21','2018-07-11 11:58:21'),(32,NULL,NULL,'DASHGOALS_CONVERSION_11_2018','2','2018-07-11 11:58:21','2018-07-11 11:58:21'),(33,NULL,NULL,'DASHGOALS_AVG_CART_VALUE_11_2018','80','2018-07-11 11:58:21','2018-07-11 11:58:21'),(34,NULL,NULL,'DASHGOALS_TRAFFIC_12_2018','600','2018-07-11 11:58:21','2018-07-11 11:58:21'),(35,NULL,NULL,'DASHGOALS_CONVERSION_12_2018','2','2018-07-11 11:58:21','2018-07-11 11:58:21'),(36,NULL,NULL,'DASHGOALS_AVG_CART_VALUE_12_2018','80','2018-07-11 11:58:21','2018-07-11 11:58:21'),(37,NULL,NULL,'TOP_CATEGORY',NULL,'2018-07-11 12:32:21','2018-07-11 12:32:21'),(38,NULL,NULL,'TOP_CATEGORY_EXPIRE',NULL,'2018-07-11 12:32:21','2018-07-11 12:32:21'),(39,NULL,NULL,'PRODUCTS_PER_CATEGORY','2','2018-07-11 12:32:21','2018-07-11 12:32:21'),(40,NULL,NULL,'PRODUCTS_PER_CATEGORY_EXPIRE','1531287141','2018-07-11 12:32:21','2018-07-11 12:32:21'),(41,NULL,NULL,'DISABLED_CATEGORIES','0','2018-07-11 12:32:21','2018-07-11 12:32:21'),(42,NULL,NULL,'DISABLED_CATEGORIES_EXPIRE','1531290741','2018-07-11 12:32:21','2018-07-11 12:32:21'),(43,NULL,NULL,'EMPTY_CATEGORIES','0','2018-07-11 12:32:21','2018-07-11 12:32:21'),(44,NULL,NULL,'EMPTY_CATEGORIES_EXPIRE','1531290741','2018-07-11 12:32:21','2018-07-11 12:32:21'),(45,NULL,NULL,'ORDERS_PER_CUSTOMER','0','2018-07-11 13:12:36','2018-07-11 13:12:36'),(46,NULL,NULL,'ORDERS_PER_CUSTOMER_EXPIRE','1531372356','2018-07-11 13:12:36','2018-07-11 13:12:36'),(47,NULL,NULL,'NEWSLETTER_REGISTRATIONS','0','2018-07-11 13:12:36','2018-07-11 13:12:36'),(48,NULL,NULL,'NEWSLETTER_REGISTRATIONS_EXPIRE','1531307556','2018-07-11 13:12:36','2018-07-11 13:12:36'),(49,NULL,NULL,'CUSTOMER_MAIN_GENDER',NULL,'2018-07-11 13:12:36','2018-07-11 13:12:36'),(50,NULL,NULL,'CUSTOMER_MAIN_GENDER_EXPIRE',NULL,'2018-07-11 13:12:36','2018-07-11 13:12:36'),(51,NULL,NULL,'AVG_CUSTOMER_AGE',NULL,'2018-07-11 13:12:36','2018-07-11 13:12:36'),(52,NULL,NULL,'AVG_CUSTOMER_AGE_EXPIRE',NULL,'2018-07-11 13:12:36','2018-07-11 13:12:36'),(53,NULL,NULL,'ABANDONED_CARTS','0','2018-07-11 13:13:43','2018-07-11 13:13:43'),(54,NULL,NULL,'ABANDONED_CARTS_EXPIRE','1531289623','2018-07-11 13:13:43','2018-07-11 13:13:43'),(55,NULL,NULL,'CONVERSION_RATE','0%','2018-07-11 13:13:43','2018-07-11 13:13:43'),(56,NULL,NULL,'CONVERSION_RATE_EXPIRE','1531324800','2018-07-11 13:13:43','2018-07-11 13:13:43'),(57,NULL,NULL,'NETPROFIT_VISIT','NT$0.00','2018-07-11 13:13:43','2018-07-11 13:13:43'),(58,NULL,NULL,'NETPROFIT_VISIT_EXPIRE','1531324800','2018-07-11 13:13:43','2018-07-11 13:13:43'),(59,NULL,NULL,'AVG_ORDER_VALUE','NT$0.00','2018-07-11 13:13:43','2018-07-11 13:13:43'),(60,NULL,NULL,'AVG_ORDER_VALUE_EXPIRE','1531324800','2018-07-11 13:13:43','2018-07-11 13:13:43'),(61,NULL,NULL,'MAIN_COUNTRY',NULL,'2018-07-11 23:04:50','2018-07-11 23:04:50'),(62,NULL,NULL,'MAIN_COUNTRY_EXPIRE',NULL,'2018-07-11 23:04:50','2018-07-11 23:04:50'),(63,NULL,NULL,'FRONTOFFICE_TRANSLATIONS','0%','2018-07-11 23:04:50','2018-07-11 23:04:50'),(64,NULL,NULL,'FRONTOFFICE_TRANSLATIONS_EXPIRE','1531321610','2018-07-11 23:04:50','2018-07-11 23:04:50'),(65,NULL,NULL,'ENABLED_LANGUAGES','1','2018-07-11 23:04:51','2018-07-11 23:04:51'),(66,NULL,NULL,'ENABLED_LANGUAGES_EXPIRE','1531321551','2018-07-11 23:04:51','2018-07-11 23:04:51'),(67,NULL,NULL,'AVG_MSG_RESPONSE_TIME','0 hours','2018-07-11 23:08:02','2018-07-11 23:08:02'),(68,NULL,NULL,'AVG_MSG_RESPONSE_TIME_EXPIRE','1531336082','2018-07-11 23:08:02','2018-07-11 23:08:02'),(69,NULL,NULL,'PENDING_MESSAGES','0','2018-07-11 23:08:02','2018-07-11 23:08:02'),(70,NULL,NULL,'PENDING_MESSAGES_EXPIRE','1531321982','2018-07-11 23:08:02','2018-07-11 23:08:02'),(71,NULL,NULL,'MESSAGES_PER_THREAD','0','2018-07-11 23:08:02','2018-07-11 23:08:02'),(72,NULL,NULL,'MESSAGES_PER_THREAD_EXPIRE','1531364882','2018-07-11 23:08:02','2018-07-11 23:08:02'),(73,1,1,'AVG_ORDER_VALUE','NT$0','2018-10-27 15:00:08','2018-10-27 15:00:08'),(74,1,1,'AVG_ORDER_VALUE_EXPIRE','1540656000','2018-10-27 15:00:08','2018-10-27 15:00:08'),(75,1,1,'ABANDONED_CARTS','0','2018-10-27 15:00:08','2018-10-27 15:00:08'),(76,1,1,'ABANDONED_CARTS_EXPIRE','1540627208','2018-10-27 15:00:08','2018-10-27 15:00:08'),(77,1,1,'CONVERSION_RATE','0%','2018-10-27 15:00:08','2018-10-27 15:00:08'),(78,1,1,'CONVERSION_RATE_EXPIRE','1540656000','2018-10-27 15:00:08','2018-10-27 15:00:08'),(79,1,1,'MESSAGES_PER_THREAD','0','2018-10-27 16:04:29','2018-10-27 16:04:29'),(80,1,1,'MESSAGES_PER_THREAD_EXPIRE','1540670669','2018-10-27 16:04:29','2018-10-27 16:04:29'),(81,1,1,'PENDING_MESSAGES','5','2018-10-27 16:04:29','2018-10-27 16:04:29'),(82,1,1,'PENDING_MESSAGES_EXPIRE','1540627769','2018-10-27 16:04:29','2018-10-27 16:04:29'),(83,1,1,'AVG_MSG_RESPONSE_TIME','0 hours','2018-10-27 16:04:29','2018-10-27 16:04:29'),(84,1,1,'AVG_MSG_RESPONSE_TIME_EXPIRE','1540641869','2018-10-27 16:04:29','2018-10-27 16:04:29');
/*!40000 ALTER TABLE `ps_configuration_kpi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_configuration_kpi_lang`
--

DROP TABLE IF EXISTS `ps_configuration_kpi_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_configuration_kpi_lang` (
  `id_configuration_kpi` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `value` text,
  `date_upd` datetime DEFAULT NULL,
  PRIMARY KEY (`id_configuration_kpi`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_configuration_kpi_lang`
--

LOCK TABLES `ps_configuration_kpi_lang` WRITE;
/*!40000 ALTER TABLE `ps_configuration_kpi_lang` DISABLE KEYS */;
INSERT INTO `ps_configuration_kpi_lang` VALUES (37,1,'Art','2018-07-11 12:32:21'),(37,2,NULL,NULL),(37,3,NULL,NULL),(37,4,NULL,NULL),(38,1,'1531369941','2018-07-11 12:32:21'),(38,2,NULL,NULL),(38,3,NULL,NULL),(38,4,NULL,NULL),(49,1,'無客戶','2018-07-11 13:12:36'),(49,2,NULL,NULL),(49,3,NULL,NULL),(49,4,NULL,NULL),(50,1,'1531372356','2018-07-11 13:12:36'),(50,2,NULL,NULL),(50,3,NULL,NULL),(50,4,NULL,NULL),(51,1,'0 years','2018-07-11 13:12:36'),(51,2,NULL,NULL),(51,3,NULL,NULL),(51,4,NULL,NULL),(52,1,'1531372356','2018-07-11 13:12:36'),(52,2,NULL,NULL),(52,3,NULL,NULL),(52,4,NULL,NULL),(61,1,'No orders','2018-07-11 23:04:50'),(62,1,'1531407890','2018-07-11 23:04:50');
/*!40000 ALTER TABLE `ps_configuration_kpi_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_configuration_lang`
--

DROP TABLE IF EXISTS `ps_configuration_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_configuration_lang` (
  `id_configuration` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `value` text,
  `date_upd` datetime DEFAULT NULL,
  PRIMARY KEY (`id_configuration`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_configuration_lang`
--

LOCK TABLES `ps_configuration_lang` WRITE;
/*!40000 ALTER TABLE `ps_configuration_lang` DISABLE KEYS */;
INSERT INTO `ps_configuration_lang` VALUES (39,1,'#IN',NULL),(39,2,'#IN',NULL),(39,3,'#IN',NULL),(39,4,'PS#-',NULL),(42,1,'#DE',NULL),(42,2,'#DE',NULL),(42,3,'#DE',NULL),(42,4,'RE#-',NULL),(44,1,'#RE',NULL),(44,2,'#RE',NULL),(44,3,'#RE',NULL),(44,4,'#RE',NULL),(51,1,'a|about|above|after|again|against|all|am|an|and|any|are|aren|as|at|be|because|been|before|being|below|between|both|but|by|can|cannot|could|couldn|did|didn|do|does|doesn|doing|don|down|during|each|few|for|from|further|had|hadn|has|hasn|have|haven|having|he|ll|her|here|hers|herself|him|himself|his|how|ve|if|in|into|is|isn|it|its|itself|let|me|more|most|mustn|my|myself|no|nor|not|of|off|on|once|only|or|other|ought|our|ours|ourselves|out|over|own|same|shan|she|should|shouldn|so|some|such|than|that|the|their|theirs|them|themselves|then|there|these|they|re|this|those|through|to|too|under|until|up|very|was|wasn|we|were|weren|what|when|where|which|while|who|whom|why|with|won|would|wouldn|you|your|yours|yourself|yourselves',NULL),(51,2,'a|about|above|after|again|against|all|am|an|and|any|are|aren|as|at|be|because|been|before|being|below|between|both|but|by|can|cannot|could|couldn|did|didn|do|does|doesn|doing|don|down|during|each|few|for|from|further|had|hadn|has|hasn|have|haven|having|he|ll|her|here|hers|herself|him|himself|his|how|ve|if|in|into|is|isn|it|its|itself|let|me|more|most|mustn|my|myself|no|nor|not|of|off|on|once|only|or|other|ought|our|ours|ourselves|out|over|own|same|shan|she|should|shouldn|so|some|such|than|that|the|their|theirs|them|themselves|then|there|these|they|re|this|those|through|to|too|under|until|up|very|was|wasn|we|were|weren|what|when|where|which|while|who|whom|why|with|won|would|wouldn|you|your|yours|yourself|yourselves',NULL),(51,3,'a|about|above|after|again|against|all|am|an|and|any|are|aren|as|at|be|because|been|before|being|below|between|both|but|by|can|cannot|could|couldn|did|didn|do|does|doesn|doing|don|down|during|each|few|for|from|further|had|hadn|has|hasn|have|haven|having|he|ll|her|here|hers|herself|him|himself|his|how|ve|if|in|into|is|isn|it|its|itself|let|me|more|most|mustn|my|myself|no|nor|not|of|off|on|once|only|or|other|ought|our|ours|ourselves|out|over|own|same|shan|she|should|shouldn|so|some|such|than|that|the|their|theirs|them|themselves|then|there|these|they|re|this|those|through|to|too|under|until|up|very|was|wasn|we|were|weren|what|when|where|which|while|who|whom|why|with|won|would|wouldn|you|your|yours|yourself|yourselves',NULL),(51,4,'a|about|above|after|again|against|all|am|an|and|any|are|aren|as|at|be|because|been|before|being|below|between|both|but|by|can|cannot|could|couldn|did|didn|do|does|doesn|doing|don|down|during|each|few|for|from|further|had|hadn|has|hasn|have|haven|having|he|ll|her|here|hers|herself|him|himself|his|how|ve|if|in|into|is|isn|it|its|itself|let|me|more|most|mustn|my|myself|no|nor|not|of|off|on|once|only|or|other|ought|our|ours|ourselves|out|over|own|same|shan|she|should|shouldn|so|some|such|than|that|the|their|theirs|them|themselves|then|there|these|they|re|this|those|through|to|too|under|until|up|very|was|wasn|we|were|weren|what|when|where|which|while|who|whom|why|with|won|would|wouldn|you|your|yours|yourself|yourselves',NULL),(77,1,'Dear Customer,\r\n\r\nRegards,\r\nCustomer service','2018-09-22 00:19:59'),(77,2,'Dear Customer,\r\n\r\nRegards,\r\nCustomer service',NULL),(77,3,'Dear Customer,\r\n\r\nRegards,\r\nCustomer service',NULL),(77,4,'Dear Customer,\r\n\r\nRegards,\r\nCustomer service',NULL),(277,1,'<p>We are currently updating our shop and will be back really soon. Thanks for your patience.</p>','2018-12-31 13:52:25'),(277,2,'We are currently updating our shop and will be back really soon.\r\nThanks for your patience.',NULL),(277,3,'We are currently updating our shop and will be back really soon.\r\nThanks for your patience.',NULL),(277,4,'We are currently updating our shop and will be back really soon.\r\nThanks for your patience.',NULL),(279,1,'','2018-07-30 11:16:15'),(279,2,'',NULL),(279,3,'',NULL),(279,4,'',NULL),(280,1,'補貨中','2018-07-30 11:15:24'),(280,2,'',NULL),(280,3,'',NULL),(280,4,'',NULL),(281,1,'缺貨',NULL),(281,2,'Out-of-Stock',NULL),(281,3,'Out-of-Stock',NULL),(281,4,'Out-of-Stock',NULL),(292,1,'44b573c7ffddf53991849439282d4ddc.png','2018-07-11 11:58:26'),(292,2,'44b573c7ffddf53991849439282d4ddc.png','2018-07-11 11:58:26'),(292,3,'44b573c7ffddf53991849439282d4ddc.png','2018-07-11 11:58:26'),(292,4,'44b573c7ffddf53991849439282d4ddc.png','2018-07-11 22:16:02'),(293,1,'','2018-07-11 11:58:26'),(293,2,'','2018-07-11 11:58:26'),(293,3,'','2018-07-11 11:58:26'),(293,4,'','2018-07-11 22:16:02'),(294,1,'','2018-07-11 11:58:26'),(294,2,'','2018-07-11 11:58:26'),(294,3,'','2018-07-11 11:58:26'),(294,4,'','2018-07-11 22:16:02'),(301,1,'','2018-07-11 11:58:34'),(301,2,'You may unsubscribe at any moment. For that purpose, please find our contact info in the legal notice.','2018-07-11 11:58:34'),(301,3,'You may unsubscribe at any moment. For that purpose, please find our contact info in the legal notice.','2018-07-11 11:58:34'),(301,4,'いつでも購読解除していただけます。特定商取引法に基づく表記より、当店の連絡先情報をご確認いただけます。','2018-07-11 11:58:34'),(415,1,'','2018-07-12 09:06:54'),(415,2,'','2018-07-12 09:06:54'),(415,3,'','2018-07-20 19:50:36'),(415,4,'','2018-07-20 19:50:36'),(416,1,'','2018-07-12 09:06:54'),(416,2,'','2018-07-12 09:06:54'),(416,3,'','2018-07-20 19:50:36'),(416,4,'','2018-07-20 19:50:36'),(425,1,'','2018-07-12 09:42:57'),(425,2,'','2018-07-12 09:42:57'),(425,3,'','2018-07-12 09:42:57'),(425,4,'','2018-07-12 09:42:57'),(568,1,'','2018-08-01 21:49:21'),(568,2,'','2018-08-01 21:49:21'),(568,3,'','2018-08-01 21:49:21'),(568,4,'','2018-08-01 21:49:21'),(569,1,'','2018-08-01 21:49:21'),(569,2,'','2018-08-01 21:49:21'),(569,3,'','2018-08-01 21:49:21'),(569,4,'','2018-08-01 21:49:21'),(589,1,'<div style=\"background:#b31a20;color:#ffffff;height:50px;line-height:50px;text-align:center;\">全館購物滿 1099元 免運費</div>','2018-09-06 19:37:03'),(589,2,'','2018-08-26 18:38:39'),(589,3,'','2018-08-26 18:38:39'),(589,4,'','2018-08-26 18:38:39'),(697,1,'<p>We are currently updating our shop and will be back really soon. Thanks for your patience.</p>','2018-10-22 23:11:12'),(718,1,'轉帳匯款完成之後，請從訂單紀錄回傳下方資訊：\r\n1. 付款日期\r\n2. 帳號末五碼 或 臨櫃匯款人姓名\r\n謝謝','2018-11-02 01:32:38'),(749,1,'','2018-11-15 13:54:21'),(749,2,'','2018-11-15 13:54:21'),(749,3,'','2018-11-15 13:54:21'),(749,4,'','2018-11-15 13:54:21'),(751,1,'熱銷商品','2018-11-17 16:29:10'),(751,2,'Best Sellers','2018-11-17 16:29:10'),(751,3,'Best Sellers','2018-11-17 16:29:10'),(751,4,'Best Sellers','2018-11-17 16:29:10'),(752,1,'新品上架','2018-11-17 16:12:01'),(752,2,'New Products','2018-11-17 16:12:01'),(752,3,'New Products','2018-11-17 16:12:01'),(752,4,'New Products','2018-11-17 16:12:01'),(753,1,'特價商品','2018-11-17 16:17:24'),(753,2,'Specials','2018-11-17 16:17:24'),(753,3,'Specials','2018-11-17 16:17:24'),(753,4,'Specials','2018-11-17 16:17:24');
/*!40000 ALTER TABLE `ps_configuration_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_connections`
--

DROP TABLE IF EXISTS `ps_connections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_connections` (
  `id_connections` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_group` int(11) unsigned NOT NULL DEFAULT '1',
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_guest` int(10) unsigned NOT NULL,
  `id_page` int(10) unsigned NOT NULL,
  `ip_address` bigint(20) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `http_referer` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_connections`),
  KEY `id_guest` (`id_guest`),
  KEY `date_add` (`date_add`),
  KEY `id_page` (`id_page`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_connections`
--

LOCK TABLES `ps_connections` WRITE;
/*!40000 ALTER TABLE `ps_connections` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_connections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_connections_page`
--

DROP TABLE IF EXISTS `ps_connections_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_connections_page` (
  `id_connections` int(10) unsigned NOT NULL,
  `id_page` int(10) unsigned NOT NULL,
  `time_start` datetime NOT NULL,
  `time_end` datetime DEFAULT NULL,
  PRIMARY KEY (`id_connections`,`id_page`,`time_start`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_connections_page`
--

LOCK TABLES `ps_connections_page` WRITE;
/*!40000 ALTER TABLE `ps_connections_page` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_connections_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_connections_source`
--

DROP TABLE IF EXISTS `ps_connections_source`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_connections_source` (
  `id_connections_source` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_connections` int(10) unsigned NOT NULL,
  `http_referer` varchar(255) DEFAULT NULL,
  `request_uri` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_connections_source`),
  KEY `connections` (`id_connections`),
  KEY `orderby` (`date_add`),
  KEY `http_referer` (`http_referer`),
  KEY `request_uri` (`request_uri`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_connections_source`
--

LOCK TABLES `ps_connections_source` WRITE;
/*!40000 ALTER TABLE `ps_connections_source` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_connections_source` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_contact`
--

DROP TABLE IF EXISTS `ps_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_contact` (
  `id_contact` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL,
  `customer_service` tinyint(1) NOT NULL DEFAULT '0',
  `position` tinyint(2) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_contact`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_contact`
--

LOCK TABLES `ps_contact` WRITE;
/*!40000 ALTER TABLE `ps_contact` DISABLE KEYS */;
INSERT INTO `ps_contact` VALUES (1,'admin@example.com',1,0);
/*!40000 ALTER TABLE `ps_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_contact_lang`
--

DROP TABLE IF EXISTS `ps_contact_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_contact_lang` (
  `id_contact` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` text,
  PRIMARY KEY (`id_contact`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_contact_lang`
--

LOCK TABLES `ps_contact_lang` WRITE;
/*!40000 ALTER TABLE `ps_contact_lang` DISABLE KEYS */;
INSERT INTO `ps_contact_lang` VALUES (1,1,'商店客服','關於商品或訂購問題'),(1,2,'网站管理员','If a technical problem occurs on this website'),(1,3,'网站管理员','If a technical problem occurs on this website'),(1,4,'网站管理员','ウェブサイトで技術的問題が発生したら');
/*!40000 ALTER TABLE `ps_contact_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_contact_shop`
--

DROP TABLE IF EXISTS `ps_contact_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_contact_shop` (
  `id_contact` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_contact`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_contact_shop`
--

LOCK TABLES `ps_contact_shop` WRITE;
/*!40000 ALTER TABLE `ps_contact_shop` DISABLE KEYS */;
INSERT INTO `ps_contact_shop` VALUES (1,1);
/*!40000 ALTER TABLE `ps_contact_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_country`
--

DROP TABLE IF EXISTS `ps_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_country` (
  `id_country` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_zone` int(10) unsigned NOT NULL,
  `id_currency` int(10) unsigned NOT NULL DEFAULT '0',
  `iso_code` varchar(3) NOT NULL,
  `call_prefix` int(10) NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `contains_states` tinyint(1) NOT NULL DEFAULT '0',
  `need_identification_number` tinyint(1) NOT NULL DEFAULT '0',
  `need_zip_code` tinyint(1) NOT NULL DEFAULT '1',
  `zip_code_format` varchar(12) NOT NULL DEFAULT '',
  `display_tax_label` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_country`),
  KEY `country_iso_code` (`iso_code`),
  KEY `country_` (`id_zone`)
) ENGINE=InnoDB AUTO_INCREMENT=245 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_country`
--

LOCK TABLES `ps_country` WRITE;
/*!40000 ALTER TABLE `ps_country` DISABLE KEYS */;
INSERT INTO `ps_country` VALUES (1,1,0,'DE',49,0,0,0,1,'NNNNN',1),(2,1,0,'AT',43,0,0,0,1,'NNNN',1),(3,1,0,'BE',32,0,0,0,1,'NNNN',1),(4,2,0,'CA',1,0,1,0,1,'LNL NLN',0),(5,3,0,'CN',86,1,0,0,1,'NNNNNN',1),(6,1,0,'ES',34,0,0,1,1,'NNNNN',1),(7,1,0,'FI',358,0,0,0,1,'NNNNN',1),(8,1,0,'FR',33,0,0,0,1,'NNNNN',1),(9,1,0,'GR',30,0,0,0,1,'NNNNN',1),(10,1,0,'IT',39,0,1,0,1,'NNNNN',1),(11,3,0,'JP',81,1,1,0,1,'NNN-NNNN',1),(12,1,0,'LU',352,0,0,0,1,'NNNN',1),(13,1,0,'NL',31,0,0,0,1,'NNNN LL',1),(14,1,0,'PL',48,0,0,0,1,'NN-NNN',1),(15,1,0,'PT',351,0,0,0,1,'NNNN-NNN',1),(16,1,0,'CZ',420,0,0,0,1,'NNN NN',1),(17,1,0,'GB',44,0,0,0,1,'',1),(18,1,0,'SE',46,0,0,0,1,'NNN NN',1),(19,7,0,'CH',41,0,0,0,1,'NNNN',1),(20,1,0,'DK',45,0,0,0,1,'NNNN',1),(21,2,0,'US',1,1,1,0,1,'NNNNN',0),(22,3,0,'HK',852,1,0,0,0,'',1),(23,7,0,'NO',47,0,0,0,1,'NNNN',1),(24,5,0,'AU',61,0,1,0,1,'NNNN',1),(25,3,0,'SG',65,1,0,0,1,'NNNNNN',1),(26,1,0,'IE',353,0,0,0,0,'',1),(27,5,0,'NZ',64,0,0,0,1,'NNNN',1),(28,3,0,'KR',82,0,0,0,1,'NNN-NNN',1),(29,3,0,'IL',972,0,0,0,1,'NNNNNNN',1),(30,4,0,'ZA',27,0,0,0,1,'NNNN',1),(31,4,0,'NG',234,0,0,0,1,'',1),(32,4,0,'CI',225,0,0,0,1,'',1),(33,4,0,'TG',228,0,0,0,1,'',1),(34,6,0,'BO',591,0,0,0,1,'',1),(35,4,0,'MU',230,0,0,0,1,'',1),(36,1,0,'RO',40,0,0,0,1,'NNNNNN',1),(37,1,0,'SK',421,0,0,0,1,'NNN NN',1),(38,4,0,'DZ',213,0,0,0,1,'NNNNN',1),(39,2,0,'AS',0,0,0,0,1,'',1),(40,7,0,'AD',376,0,0,0,1,'CNNN',1),(41,4,0,'AO',244,0,0,0,0,'',1),(42,8,0,'AI',0,0,0,0,1,'',1),(43,2,0,'AG',0,0,0,0,1,'',1),(44,6,0,'AR',54,0,1,0,1,'LNNNNLLL',1),(45,3,0,'AM',374,0,0,0,1,'NNNN',1),(46,8,0,'AW',297,0,0,0,1,'',1),(47,3,0,'AZ',994,0,0,0,1,'CNNNN',1),(48,2,0,'BS',0,0,0,0,1,'',1),(49,3,0,'BH',973,0,0,0,1,'',1),(50,3,0,'BD',880,0,0,0,1,'NNNN',1),(51,2,0,'BB',0,0,0,0,1,'CNNNNN',1),(52,7,0,'BY',0,0,0,0,1,'NNNNNN',1),(53,8,0,'BZ',501,0,0,0,0,'',1),(54,4,0,'BJ',229,0,0,0,0,'',1),(55,2,0,'BM',0,0,0,0,1,'',1),(56,3,0,'BT',975,0,0,0,1,'',1),(57,4,0,'BW',267,0,0,0,1,'',1),(58,6,0,'BR',55,0,0,0,1,'NNNNN-NNN',1),(59,3,0,'BN',673,0,0,0,1,'LLNNNN',1),(60,4,0,'BF',226,0,0,0,1,'',1),(61,3,0,'MM',95,0,0,0,1,'',1),(62,4,0,'BI',257,0,0,0,1,'',1),(63,3,0,'KH',855,0,0,0,1,'NNNNN',1),(64,4,0,'CM',237,0,0,0,1,'',1),(65,4,0,'CV',238,0,0,0,1,'NNNN',1),(66,4,0,'CF',236,0,0,0,1,'',1),(67,4,0,'TD',235,0,0,0,1,'',1),(68,6,0,'CL',56,0,0,0,1,'NNN-NNNN',1),(69,6,0,'CO',57,0,0,0,1,'NNNNNN',1),(70,4,0,'KM',269,0,0,0,1,'',1),(71,4,0,'CD',242,0,0,0,1,'',1),(72,4,0,'CG',243,0,0,0,1,'',1),(73,8,0,'CR',506,0,0,0,1,'NNNNN',1),(74,7,0,'HR',385,0,0,0,1,'NNNNN',1),(75,8,0,'CU',53,0,0,0,1,'',1),(76,1,0,'CY',357,0,0,0,1,'NNNN',1),(77,4,0,'DJ',253,0,0,0,1,'',1),(78,8,0,'DM',0,0,0,0,1,'',1),(79,8,0,'DO',0,0,0,0,1,'',1),(80,3,0,'TL',670,0,0,0,1,'',1),(81,6,0,'EC',593,0,0,0,1,'CNNNNNN',1),(82,4,0,'EG',20,0,0,0,1,'NNNNN',1),(83,8,0,'SV',503,0,0,0,1,'',1),(84,4,0,'GQ',240,0,0,0,1,'',1),(85,4,0,'ER',291,0,0,0,1,'',1),(86,1,0,'EE',372,0,0,0,1,'NNNNN',1),(87,4,0,'ET',251,0,0,0,1,'',1),(88,8,0,'FK',0,0,0,0,1,'LLLL NLL',1),(89,7,0,'FO',298,0,0,0,1,'',1),(90,5,0,'FJ',679,0,0,0,1,'',1),(91,4,0,'GA',241,0,0,0,1,'',1),(92,4,0,'GM',220,0,0,0,1,'',1),(93,3,0,'GE',995,0,0,0,1,'NNNN',1),(94,4,0,'GH',233,0,0,0,1,'',1),(95,8,0,'GD',0,0,0,0,1,'',1),(96,7,0,'GL',299,0,0,0,1,'',1),(97,7,0,'GI',350,0,0,0,1,'',1),(98,8,0,'GP',590,0,0,0,1,'',1),(99,5,0,'GU',0,0,0,0,1,'',1),(100,8,0,'GT',502,0,0,0,1,'',1),(101,7,0,'GG',0,0,0,0,1,'LLN NLL',1),(102,4,0,'GN',224,0,0,0,1,'',1),(103,4,0,'GW',245,0,0,0,1,'',1),(104,6,0,'GY',592,0,0,0,1,'',1),(105,8,0,'HT',509,0,0,0,1,'',1),(106,5,0,'HM',0,0,0,0,1,'',1),(107,7,0,'VA',379,0,0,0,1,'NNNNN',1),(108,8,0,'HN',504,0,0,0,1,'',1),(109,7,0,'IS',354,0,0,0,1,'NNN',1),(110,3,0,'IN',91,0,0,0,1,'NNN NNN',1),(111,3,0,'ID',62,0,1,0,1,'NNNNN',1),(112,3,0,'IR',98,0,0,0,1,'NNNNN-NNNNN',1),(113,3,0,'IQ',964,0,0,0,1,'NNNNN',1),(114,7,0,'IM',0,0,0,0,1,'CN NLL',1),(115,8,0,'JM',0,0,0,0,1,'',1),(116,7,0,'JE',0,0,0,0,1,'CN NLL',1),(117,3,0,'JO',962,0,0,0,1,'',1),(118,3,0,'KZ',7,0,0,0,1,'NNNNNN',1),(119,4,0,'KE',254,0,0,0,1,'',1),(120,5,0,'KI',686,0,0,0,1,'',1),(121,3,0,'KP',850,0,0,0,1,'',1),(122,3,0,'KW',965,0,0,0,1,'',1),(123,3,0,'KG',996,0,0,0,1,'',1),(124,3,0,'LA',856,0,0,0,1,'',1),(125,1,0,'LV',371,0,0,0,1,'C-NNNN',1),(126,3,0,'LB',961,0,0,0,1,'',1),(127,4,0,'LS',266,0,0,0,1,'',1),(128,4,0,'LR',231,0,0,0,1,'',1),(129,4,0,'LY',218,0,0,0,1,'',1),(130,1,0,'LI',423,0,0,0,1,'NNNN',1),(131,1,0,'LT',370,0,0,0,1,'NNNNN',1),(132,3,0,'MO',853,0,0,0,0,'',1),(133,7,0,'MK',389,0,0,0,1,'',1),(134,4,0,'MG',261,0,0,0,1,'',1),(135,4,0,'MW',265,0,0,0,1,'',1),(136,3,0,'MY',60,1,0,0,1,'NNNNN',1),(137,3,0,'MV',960,0,0,0,1,'',1),(138,4,0,'ML',223,0,0,0,1,'',1),(139,1,0,'MT',356,0,0,0,1,'LLL NNNN',1),(140,5,0,'MH',692,0,0,0,1,'',1),(141,8,0,'MQ',596,0,0,0,1,'',1),(142,4,0,'MR',222,0,0,0,1,'',1),(143,1,0,'HU',36,0,0,0,1,'NNNN',1),(144,4,0,'YT',262,0,0,0,1,'',1),(145,2,0,'MX',52,0,1,1,1,'NNNNN',1),(146,5,0,'FM',691,0,0,0,1,'',1),(147,7,0,'MD',373,0,0,0,1,'C-NNNN',1),(148,7,0,'MC',377,0,0,0,1,'980NN',1),(149,3,0,'MN',976,0,0,0,1,'',1),(150,7,0,'ME',382,0,0,0,1,'NNNNN',1),(151,8,0,'MS',0,0,0,0,1,'',1),(152,4,0,'MA',212,0,0,0,1,'NNNNN',1),(153,4,0,'MZ',258,0,0,0,1,'',1),(154,4,0,'NA',264,0,0,0,1,'',1),(155,5,0,'NR',674,0,0,0,1,'',1),(156,3,0,'NP',977,0,0,0,1,'',1),(157,8,0,'AN',599,0,0,0,1,'',1),(158,5,0,'NC',687,0,0,0,1,'',1),(159,8,0,'NI',505,0,0,0,1,'NNNNNN',1),(160,4,0,'NE',227,0,0,0,1,'',1),(161,5,0,'NU',683,0,0,0,1,'',1),(162,5,0,'NF',0,0,0,0,1,'',1),(163,5,0,'MP',0,0,0,0,1,'',1),(164,3,0,'OM',968,0,0,0,1,'',1),(165,3,0,'PK',92,0,0,0,1,'',1),(166,5,0,'PW',680,0,0,0,1,'',1),(167,3,0,'PS',0,0,0,0,1,'',1),(168,8,0,'PA',507,0,0,0,1,'NNNNNN',1),(169,5,0,'PG',675,0,0,0,1,'',1),(170,6,0,'PY',595,0,0,0,1,'',1),(171,6,0,'PE',51,0,0,0,1,'',1),(172,3,0,'PH',63,0,0,0,1,'NNNN',1),(173,5,0,'PN',0,0,0,0,1,'LLLL NLL',1),(174,8,0,'PR',0,0,0,0,1,'NNNNN',1),(175,3,0,'QA',974,0,0,0,1,'',1),(176,4,0,'RE',262,0,0,0,1,'',1),(177,7,0,'RU',7,0,0,0,1,'NNNNNN',1),(178,4,0,'RW',250,0,0,0,1,'',1),(179,8,0,'BL',0,0,0,0,1,'',1),(180,8,0,'KN',0,0,0,0,1,'',1),(181,8,0,'LC',0,0,0,0,1,'',1),(182,8,0,'MF',0,0,0,0,1,'',1),(183,8,0,'PM',508,0,0,0,1,'',1),(184,8,0,'VC',0,0,0,0,1,'',1),(185,5,0,'WS',685,0,0,0,1,'',1),(186,7,0,'SM',378,0,0,0,1,'NNNNN',1),(187,4,0,'ST',239,0,0,0,1,'',1),(188,3,0,'SA',966,0,0,0,1,'',1),(189,4,0,'SN',221,0,0,0,1,'',1),(190,7,0,'RS',381,0,0,0,1,'NNNNN',1),(191,4,0,'SC',248,0,0,0,1,'',1),(192,4,0,'SL',232,0,0,0,1,'',1),(193,1,0,'SI',386,0,0,0,1,'C-NNNN',1),(194,5,0,'SB',677,0,0,0,1,'',1),(195,4,0,'SO',252,0,0,0,1,'',1),(196,8,0,'GS',0,0,0,0,1,'LLLL NLL',1),(197,3,0,'LK',94,0,0,0,1,'NNNNN',1),(198,4,0,'SD',249,0,0,0,1,'',1),(199,8,0,'SR',597,0,0,0,1,'',1),(200,7,0,'SJ',0,0,0,0,1,'',1),(201,4,0,'SZ',268,0,0,0,1,'',1),(202,3,0,'SY',963,0,0,0,1,'',1),(203,9,1,'TW',886,1,0,0,1,'',0),(204,3,0,'TJ',992,0,0,0,1,'',1),(205,4,0,'TZ',255,0,0,0,1,'',1),(206,3,0,'TH',66,0,0,0,1,'NNNNN',1),(207,5,0,'TK',690,0,0,0,1,'',1),(208,5,0,'TO',676,0,0,0,1,'',1),(209,6,0,'TT',0,0,0,0,1,'',1),(210,4,0,'TN',216,0,0,0,1,'',1),(211,7,0,'TR',90,0,0,0,1,'NNNNN',1),(212,3,0,'TM',993,0,0,0,1,'',1),(213,8,0,'TC',0,0,0,0,1,'LLLL NLL',1),(214,5,0,'TV',688,0,0,0,1,'',1),(215,4,0,'UG',256,0,0,0,1,'',1),(216,1,0,'UA',380,0,0,0,1,'NNNNN',1),(217,3,0,'AE',971,0,0,0,1,'',1),(218,6,0,'UY',598,0,0,0,1,'',1),(219,3,0,'UZ',998,0,0,0,1,'',1),(220,5,0,'VU',678,0,0,0,1,'',1),(221,6,0,'VE',58,0,0,0,1,'',1),(222,3,0,'VN',84,0,0,0,1,'NNNNNN',1),(223,2,0,'VG',0,0,0,0,1,'CNNNN',1),(224,2,0,'VI',0,0,0,0,1,'',1),(225,5,0,'WF',681,0,0,0,1,'',1),(226,4,0,'EH',0,0,0,0,1,'',1),(227,3,0,'YE',967,0,0,0,1,'',1),(228,4,0,'ZM',260,0,0,0,1,'',1),(229,4,0,'ZW',263,0,0,0,1,'',1),(230,7,0,'AL',355,0,0,0,1,'NNNN',1),(231,3,0,'AF',93,0,0,0,1,'NNNN',1),(232,5,0,'AQ',0,0,0,0,1,'',1),(233,1,0,'BA',387,0,0,0,1,'',1),(234,5,0,'BV',0,0,0,0,1,'',1),(235,5,0,'IO',0,0,0,0,1,'LLLL NLL',1),(236,1,0,'BG',359,0,0,0,1,'NNNN',1),(237,8,0,'KY',0,0,0,0,1,'',1),(238,3,0,'CX',0,0,0,0,1,'',1),(239,3,0,'CC',0,0,0,0,1,'',1),(240,5,0,'CK',682,0,0,0,1,'',1),(241,6,0,'GF',594,0,0,0,1,'',1),(242,5,0,'PF',689,0,0,0,1,'',1),(243,5,0,'TF',0,0,0,0,1,'',1),(244,7,0,'AX',0,0,0,0,1,'NNNNN',1);
/*!40000 ALTER TABLE `ps_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_country_lang`
--

DROP TABLE IF EXISTS `ps_country_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_country_lang` (
  `id_country` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id_country`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_country_lang`
--

LOCK TABLES `ps_country_lang` WRITE;
/*!40000 ALTER TABLE `ps_country_lang` DISABLE KEYS */;
INSERT INTO `ps_country_lang` VALUES (1,1,'Germany'),(1,2,'Germany'),(1,3,'Germany'),(1,4,'Germany'),(2,1,'Austria'),(2,2,'Austria'),(2,3,'Austria'),(2,4,'Austria'),(3,1,'Belgium'),(3,2,'Belgium'),(3,3,'Belgium'),(3,4,'Belgium'),(4,1,'Canada'),(4,2,'Canada'),(4,3,'Canada'),(4,4,'Canada'),(5,1,'中國'),(5,2,'China'),(5,3,'China'),(5,4,'China'),(6,1,'Spain'),(6,2,'Spain'),(6,3,'Spain'),(6,4,'Spain'),(7,1,'Finland'),(7,2,'Finland'),(7,3,'Finland'),(7,4,'Finland'),(8,1,'France'),(8,2,'France'),(8,3,'France'),(8,4,'France'),(9,1,'Greece'),(9,2,'Greece'),(9,3,'Greece'),(9,4,'Greece'),(10,1,'Italy'),(10,2,'Italy'),(10,3,'Italy'),(10,4,'Italy'),(11,1,'日本'),(11,2,'Japan'),(11,3,'Japan'),(11,4,'Japan'),(12,1,'Luxemburg'),(12,2,'Luxembourg'),(12,3,'Luxemburg'),(12,4,'Luxemburg'),(13,1,'Netherlands'),(13,2,'Netherlands'),(13,3,'Netherlands'),(13,4,'Netherlands'),(14,1,'Poland'),(14,2,'Poland'),(14,3,'Poland'),(14,4,'Poland'),(15,1,'Portugal'),(15,2,'Portugal'),(15,3,'Portugal'),(15,4,'Portugal'),(16,1,'Czech Republic'),(16,2,'Czech Republic'),(16,3,'Czech Republic'),(16,4,'Czech Republic'),(17,1,'United Kingdom'),(17,2,'United Kingdom'),(17,3,'United Kingdom'),(17,4,'United Kingdom'),(18,1,'Sweden'),(18,2,'Sweden'),(18,3,'Sweden'),(18,4,'Sweden'),(19,1,'Switzerland'),(19,2,'Switzerland'),(19,3,'Switzerland'),(19,4,'Switzerland'),(20,1,'Denmark'),(20,2,'Denmark'),(20,3,'Denmark'),(20,4,'Denmark'),(21,1,'美國'),(21,2,'United States'),(21,3,'United States'),(21,4,'United States'),(22,1,'香港'),(22,2,'Hong Kong'),(22,3,'Hong Kong'),(22,4,'Hong Kong'),(23,1,'Norway'),(23,2,'Norway'),(23,3,'Norway'),(23,4,'Norway'),(24,1,'Australia'),(24,2,'Australia'),(24,3,'Australia'),(24,4,'Australia'),(25,1,'新加坡'),(25,2,'Singapore'),(25,3,'Singapore'),(25,4,'Singapore'),(26,1,'Ireland'),(26,2,'Ireland'),(26,3,'Ireland'),(26,4,'Ireland'),(27,1,'New Zealand'),(27,2,'New Zealand'),(27,3,'New Zealand'),(27,4,'New Zealand'),(28,1,'South Korea'),(28,2,'South Korea'),(28,3,'South Korea'),(28,4,'South Korea'),(29,1,'Israel'),(29,2,'Israel'),(29,3,'Israel'),(29,4,'Israel'),(30,1,'South Africa'),(30,2,'South Africa'),(30,3,'South Africa'),(30,4,'South Africa'),(31,1,'Nigeria'),(31,2,'Nigeria'),(31,3,'Nigeria'),(31,4,'Nigeria'),(32,1,'Ivory Coast'),(32,2,'Côte D’Ivoire'),(32,3,'Ivory Coast'),(32,4,'Ivory Coast'),(33,1,'Togo'),(33,2,'Togo'),(33,3,'Togo'),(33,4,'Togo'),(34,1,'Bolivia'),(34,2,'Bolivia'),(34,3,'Bolivia'),(34,4,'Bolivia'),(35,1,'Mauritius'),(35,2,'Mauritius'),(35,3,'Mauritius'),(35,4,'Mauritius'),(36,1,'Romania'),(36,2,'Romania'),(36,3,'Romania'),(36,4,'Romania'),(37,1,'Slovakia'),(37,2,'Slovakia'),(37,3,'Slovakia'),(37,4,'Slovakia'),(38,1,'Algeria'),(38,2,'Algeria'),(38,3,'Algeria'),(38,4,'Algeria'),(39,1,'American Samoa'),(39,2,'American Samoa'),(39,3,'American Samoa'),(39,4,'American Samoa'),(40,1,'Andorra'),(40,2,'Andorra'),(40,3,'Andorra'),(40,4,'Andorra'),(41,1,'Angola'),(41,2,'Angola'),(41,3,'Angola'),(41,4,'Angola'),(42,1,'Anguilla'),(42,2,'Anguilla'),(42,3,'Anguilla'),(42,4,'Anguilla'),(43,1,'Antigua and Barbuda'),(43,2,'Antigua & Barbuda'),(43,3,'Antigua and Barbuda'),(43,4,'Antigua and Barbuda'),(44,1,'Argentina'),(44,2,'Argentina'),(44,3,'Argentina'),(44,4,'Argentina'),(45,1,'Armenia'),(45,2,'Armenia'),(45,3,'Armenia'),(45,4,'Armenia'),(46,1,'Aruba'),(46,2,'Aruba'),(46,3,'Aruba'),(46,4,'Aruba'),(47,1,'Azerbaijan'),(47,2,'Azerbaijan'),(47,3,'Azerbaijan'),(47,4,'Azerbaijan'),(48,1,'Bahamas'),(48,2,'Bahamas'),(48,3,'Bahamas'),(48,4,'Bahamas'),(49,1,'Bahrain'),(49,2,'Bahrain'),(49,3,'Bahrain'),(49,4,'Bahrain'),(50,1,'Bangladesh'),(50,2,'Bangladesh'),(50,3,'Bangladesh'),(50,4,'Bangladesh'),(51,1,'Barbados'),(51,2,'Barbados'),(51,3,'Barbados'),(51,4,'Barbados'),(52,1,'Belarus'),(52,2,'Belarus'),(52,3,'Belarus'),(52,4,'Belarus'),(53,1,'Belize'),(53,2,'Belize'),(53,3,'Belize'),(53,4,'Belize'),(54,1,'Benin'),(54,2,'Benin'),(54,3,'Benin'),(54,4,'Benin'),(55,1,'Bermuda'),(55,2,'Bermuda'),(55,3,'Bermuda'),(55,4,'Bermuda'),(56,1,'Bhutan'),(56,2,'Bhutan'),(56,3,'Bhutan'),(56,4,'Bhutan'),(57,1,'Botswana'),(57,2,'Botswana'),(57,3,'Botswana'),(57,4,'Botswana'),(58,1,'Brazil'),(58,2,'Brazil'),(58,3,'Brazil'),(58,4,'Brazil'),(59,1,'Brunei'),(59,2,'Brunei'),(59,3,'Brunei'),(59,4,'Brunei'),(60,1,'Burkina Faso'),(60,2,'Burkina Faso'),(60,3,'Burkina Faso'),(60,4,'Burkina Faso'),(61,1,'Burma (Myanmar)'),(61,2,'Myanmar (Burma)'),(61,3,'Burma (Myanmar)'),(61,4,'Burma (Myanmar)'),(62,1,'Burundi'),(62,2,'Burundi'),(62,3,'Burundi'),(62,4,'Burundi'),(63,1,'Cambodia'),(63,2,'Cambodia'),(63,3,'Cambodia'),(63,4,'Cambodia'),(64,1,'Cameroon'),(64,2,'Cameroon'),(64,3,'Cameroon'),(64,4,'Cameroon'),(65,1,'Cape Verde'),(65,2,'Cape Verde'),(65,3,'Cape Verde'),(65,4,'Cape Verde'),(66,1,'Central African Republic'),(66,2,'Central African Republic'),(66,3,'Central African Republic'),(66,4,'Central African Republic'),(67,1,'Chad'),(67,2,'Chad'),(67,3,'Chad'),(67,4,'Chad'),(68,1,'Chile'),(68,2,'Chile'),(68,3,'Chile'),(68,4,'Chile'),(69,1,'Colombia'),(69,2,'Colombia'),(69,3,'Colombia'),(69,4,'Colombia'),(70,1,'Comoros'),(70,2,'Comoros'),(70,3,'Comoros'),(70,4,'Comoros'),(71,1,'Congo, Dem. Republic'),(71,2,'Congo - Kinshasa'),(71,3,'Congo, Dem. Republic'),(71,4,'Congo, Dem. Republic'),(72,1,'Congo, Republic'),(72,2,'Congo - Brazzaville'),(72,3,'Congo, Republic'),(72,4,'Congo, Republic'),(73,1,'Costa Rica'),(73,2,'Costa Rica'),(73,3,'Costa Rica'),(73,4,'Costa Rica'),(74,1,'Croatia'),(74,2,'Croatia'),(74,3,'Croatia'),(74,4,'Croatia'),(75,1,'Cuba'),(75,2,'Cuba'),(75,3,'Cuba'),(75,4,'Cuba'),(76,1,'Cyprus'),(76,2,'Cyprus'),(76,3,'Cyprus'),(76,4,'Cyprus'),(77,1,'Djibouti'),(77,2,'Djibouti'),(77,3,'Djibouti'),(77,4,'Djibouti'),(78,1,'Dominica'),(78,2,'Dominica'),(78,3,'Dominica'),(78,4,'Dominica'),(79,1,'Dominican Republic'),(79,2,'Dominican Republic'),(79,3,'Dominican Republic'),(79,4,'Dominican Republic'),(80,1,'East Timor'),(80,2,'Timor-Leste'),(80,3,'East Timor'),(80,4,'East Timor'),(81,1,'Ecuador'),(81,2,'Ecuador'),(81,3,'Ecuador'),(81,4,'Ecuador'),(82,1,'Egypt'),(82,2,'Egypt'),(82,3,'Egypt'),(82,4,'Egypt'),(83,1,'El Salvador'),(83,2,'El Salvador'),(83,3,'El Salvador'),(83,4,'El Salvador'),(84,1,'Equatorial Guinea'),(84,2,'Equatorial Guinea'),(84,3,'Equatorial Guinea'),(84,4,'Equatorial Guinea'),(85,1,'Eritrea'),(85,2,'Eritrea'),(85,3,'Eritrea'),(85,4,'Eritrea'),(86,1,'Estonia'),(86,2,'Estonia'),(86,3,'Estonia'),(86,4,'Estonia'),(87,1,'Ethiopia'),(87,2,'Ethiopia'),(87,3,'Ethiopia'),(87,4,'Ethiopia'),(88,1,'Falkland Islands'),(88,2,'Falkland Islands'),(88,3,'Falkland Islands'),(88,4,'Falkland Islands'),(89,1,'Faroe Islands'),(89,2,'Faroe Islands'),(89,3,'Faroe Islands'),(89,4,'Faroe Islands'),(90,1,'Fiji'),(90,2,'Fiji'),(90,3,'Fiji'),(90,4,'Fiji'),(91,1,'Gabon'),(91,2,'Gabon'),(91,3,'Gabon'),(91,4,'Gabon'),(92,1,'Gambia'),(92,2,'Gambia'),(92,3,'Gambia'),(92,4,'Gambia'),(93,1,'Georgia'),(93,2,'Georgia'),(93,3,'Georgia'),(93,4,'Georgia'),(94,1,'Ghana'),(94,2,'Ghana'),(94,3,'Ghana'),(94,4,'Ghana'),(95,1,'Grenada'),(95,2,'Grenada'),(95,3,'Grenada'),(95,4,'Grenada'),(96,1,'Greenland'),(96,2,'Greenland'),(96,3,'Greenland'),(96,4,'Greenland'),(97,1,'Gibraltar'),(97,2,'Gibraltar'),(97,3,'Gibraltar'),(97,4,'Gibraltar'),(98,1,'Guadeloupe'),(98,2,'Guadeloupe'),(98,3,'Guadeloupe'),(98,4,'Guadeloupe'),(99,1,'Guam'),(99,2,'Guam'),(99,3,'Guam'),(99,4,'Guam'),(100,1,'Guatemala'),(100,2,'Guatemala'),(100,3,'Guatemala'),(100,4,'Guatemala'),(101,1,'Guernsey'),(101,2,'Guernsey'),(101,3,'Guernsey'),(101,4,'Guernsey'),(102,1,'Guinea'),(102,2,'Guinea'),(102,3,'Guinea'),(102,4,'Guinea'),(103,1,'Guinea-Bissau'),(103,2,'Guinea-Bissau'),(103,3,'Guinea-Bissau'),(103,4,'Guinea-Bissau'),(104,1,'Guyana'),(104,2,'Guyana'),(104,3,'Guyana'),(104,4,'Guyana'),(105,1,'Haiti'),(105,2,'Haiti'),(105,3,'Haiti'),(105,4,'Haiti'),(106,1,'Heard Island and McDonald Islands'),(106,2,'Heard & McDonald Islands'),(106,3,'Heard Island and McDonald Islands'),(106,4,'Heard Island and McDonald Islands'),(107,1,'Vatican City State'),(107,2,'Vatican City'),(107,3,'Vatican City State'),(107,4,'Vatican City State'),(108,1,'Honduras'),(108,2,'Honduras'),(108,3,'Honduras'),(108,4,'Honduras'),(109,1,'Iceland'),(109,2,'Iceland'),(109,3,'Iceland'),(109,4,'Iceland'),(110,1,'India'),(110,2,'India'),(110,3,'India'),(110,4,'India'),(111,1,'Indonesia'),(111,2,'Indonesia'),(111,3,'Indonesia'),(111,4,'Indonesia'),(112,1,'Iran'),(112,2,'Iran'),(112,3,'Iran'),(112,4,'Iran'),(113,1,'Iraq'),(113,2,'Iraq'),(113,3,'Iraq'),(113,4,'Iraq'),(114,1,'Man Island'),(114,2,'Isle Of Man'),(114,3,'Man Island'),(114,4,'Man Island'),(115,1,'Jamaica'),(115,2,'Jamaica'),(115,3,'Jamaica'),(115,4,'Jamaica'),(116,1,'Jersey'),(116,2,'Jersey'),(116,3,'Jersey'),(116,4,'Jersey'),(117,1,'Jordan'),(117,2,'Jordan'),(117,3,'Jordan'),(117,4,'Jordan'),(118,1,'Kazakhstan'),(118,2,'Kazakhstan'),(118,3,'Kazakhstan'),(118,4,'Kazakhstan'),(119,1,'Kenya'),(119,2,'Kenya'),(119,3,'Kenya'),(119,4,'Kenya'),(120,1,'Kiribati'),(120,2,'Kiribati'),(120,3,'Kiribati'),(120,4,'Kiribati'),(121,1,'Korea, Dem. Republic of'),(121,2,'North Korea'),(121,3,'Korea, Dem. Republic of'),(121,4,'Korea, Dem. Republic of'),(122,1,'Kuwait'),(122,2,'Kuwait'),(122,3,'Kuwait'),(122,4,'Kuwait'),(123,1,'Kyrgyzstan'),(123,2,'Kyrgyzstan'),(123,3,'Kyrgyzstan'),(123,4,'Kyrgyzstan'),(124,1,'Laos'),(124,2,'Laos'),(124,3,'Laos'),(124,4,'Laos'),(125,1,'Latvia'),(125,2,'Latvia'),(125,3,'Latvia'),(125,4,'Latvia'),(126,1,'Lebanon'),(126,2,'Lebanon'),(126,3,'Lebanon'),(126,4,'Lebanon'),(127,1,'Lesotho'),(127,2,'Lesotho'),(127,3,'Lesotho'),(127,4,'Lesotho'),(128,1,'Liberia'),(128,2,'Liberia'),(128,3,'Liberia'),(128,4,'Liberia'),(129,1,'Libya'),(129,2,'Libya'),(129,3,'Libya'),(129,4,'Libya'),(130,1,'Liechtenstein'),(130,2,'Liechtenstein'),(130,3,'Liechtenstein'),(130,4,'Liechtenstein'),(131,1,'Lithuania'),(131,2,'Lithuania'),(131,3,'Lithuania'),(131,4,'Lithuania'),(132,1,'Macau'),(132,2,'Macau SAR China'),(132,3,'Macau'),(132,4,'Macau'),(133,1,'Macedonia'),(133,2,'Macedonia'),(133,3,'Macedonia'),(133,4,'Macedonia'),(134,1,'Madagascar'),(134,2,'Madagascar'),(134,3,'Madagascar'),(134,4,'Madagascar'),(135,1,'Malawi'),(135,2,'Malawi'),(135,3,'Malawi'),(135,4,'Malawi'),(136,1,'馬來西亞'),(136,2,'Malaysia'),(136,3,'Malaysia'),(136,4,'Malaysia'),(137,1,'Maldives'),(137,2,'Maldives'),(137,3,'Maldives'),(137,4,'Maldives'),(138,1,'Mali'),(138,2,'Mali'),(138,3,'Mali'),(138,4,'Mali'),(139,1,'Malta'),(139,2,'Malta'),(139,3,'Malta'),(139,4,'Malta'),(140,1,'Marshall Islands'),(140,2,'Marshall Islands'),(140,3,'Marshall Islands'),(140,4,'Marshall Islands'),(141,1,'Martinique'),(141,2,'Martinique'),(141,3,'Martinique'),(141,4,'Martinique'),(142,1,'Mauritania'),(142,2,'Mauritania'),(142,3,'Mauritania'),(142,4,'Mauritania'),(143,1,'Hungary'),(143,2,'Hungary'),(143,3,'Hungary'),(143,4,'Hungary'),(144,1,'Mayotte'),(144,2,'Mayotte'),(144,3,'Mayotte'),(144,4,'Mayotte'),(145,1,'Mexico'),(145,2,'Mexico'),(145,3,'Mexico'),(145,4,'Mexico'),(146,1,'Micronesia'),(146,2,'Micronesia'),(146,3,'Micronesia'),(146,4,'Micronesia'),(147,1,'Moldova'),(147,2,'Moldova'),(147,3,'Moldova'),(147,4,'Moldova'),(148,1,'Monaco'),(148,2,'Monaco'),(148,3,'Monaco'),(148,4,'Monaco'),(149,1,'Mongolia'),(149,2,'Mongolia'),(149,3,'Mongolia'),(149,4,'Mongolia'),(150,1,'Montenegro'),(150,2,'Montenegro'),(150,3,'Montenegro'),(150,4,'Montenegro'),(151,1,'Montserrat'),(151,2,'Montserrat'),(151,3,'Montserrat'),(151,4,'Montserrat'),(152,1,'Morocco'),(152,2,'Morocco'),(152,3,'Morocco'),(152,4,'Morocco'),(153,1,'Mozambique'),(153,2,'Mozambique'),(153,3,'Mozambique'),(153,4,'Mozambique'),(154,1,'Namibia'),(154,2,'Namibia'),(154,3,'Namibia'),(154,4,'Namibia'),(155,1,'Nauru'),(155,2,'Nauru'),(155,3,'Nauru'),(155,4,'Nauru'),(156,1,'Nepal'),(156,2,'Nepal'),(156,3,'Nepal'),(156,4,'Nepal'),(157,1,'Netherlands Antilles'),(157,2,'Netherlands Antilles'),(157,3,'Netherlands Antilles'),(157,4,'Netherlands Antilles'),(158,1,'New Caledonia'),(158,2,'New Caledonia'),(158,3,'New Caledonia'),(158,4,'New Caledonia'),(159,1,'Nicaragua'),(159,2,'Nicaragua'),(159,3,'Nicaragua'),(159,4,'Nicaragua'),(160,1,'Niger'),(160,2,'Niger'),(160,3,'Niger'),(160,4,'Niger'),(161,1,'Niue'),(161,2,'Niue'),(161,3,'Niue'),(161,4,'Niue'),(162,1,'Norfolk Island'),(162,2,'Norfolk Island'),(162,3,'Norfolk Island'),(162,4,'Norfolk Island'),(163,1,'Northern Mariana Islands'),(163,2,'Northern Mariana Islands'),(163,3,'Northern Mariana Islands'),(163,4,'Northern Mariana Islands'),(164,1,'Oman'),(164,2,'Oman'),(164,3,'Oman'),(164,4,'Oman'),(165,1,'Pakistan'),(165,2,'Pakistan'),(165,3,'Pakistan'),(165,4,'Pakistan'),(166,1,'Palau'),(166,2,'Palau'),(166,3,'Palau'),(166,4,'Palau'),(167,1,'Palestinian Territories'),(167,2,'Palestinian Territories'),(167,3,'Palestinian Territories'),(167,4,'Palestinian Territories'),(168,1,'Panama'),(168,2,'Panama'),(168,3,'Panama'),(168,4,'Panama'),(169,1,'Papua New Guinea'),(169,2,'Papua New Guinea'),(169,3,'Papua New Guinea'),(169,4,'Papua New Guinea'),(170,1,'Paraguay'),(170,2,'Paraguay'),(170,3,'Paraguay'),(170,4,'Paraguay'),(171,1,'Peru'),(171,2,'Peru'),(171,3,'Peru'),(171,4,'Peru'),(172,1,'Philippines'),(172,2,'Philippines'),(172,3,'Philippines'),(172,4,'Philippines'),(173,1,'Pitcairn'),(173,2,'Pitcairn Islands'),(173,3,'Pitcairn'),(173,4,'Pitcairn'),(174,1,'Puerto Rico'),(174,2,'Puerto Rico'),(174,3,'Puerto Rico'),(174,4,'Puerto Rico'),(175,1,'Qatar'),(175,2,'Qatar'),(175,3,'Qatar'),(175,4,'Qatar'),(176,1,'Reunion Island'),(176,2,'Réunion'),(176,3,'Reunion Island'),(176,4,'Reunion Island'),(177,1,'Russian Federation'),(177,2,'Russia'),(177,3,'Russian Federation'),(177,4,'Russian Federation'),(178,1,'Rwanda'),(178,2,'Rwanda'),(178,3,'Rwanda'),(178,4,'Rwanda'),(179,1,'Saint Barthelemy'),(179,2,'St. Barthélemy'),(179,3,'Saint Barthelemy'),(179,4,'Saint Barthelemy'),(180,1,'Saint Kitts and Nevis'),(180,2,'St. Kitts & Nevis'),(180,3,'Saint Kitts and Nevis'),(180,4,'Saint Kitts and Nevis'),(181,1,'Saint Lucia'),(181,2,'St. Lucia'),(181,3,'Saint Lucia'),(181,4,'Saint Lucia'),(182,1,'Saint Martin'),(182,2,'St. Martin'),(182,3,'Saint Martin'),(182,4,'Saint Martin'),(183,1,'Saint Pierre and Miquelon'),(183,2,'St. Pierre & Miquelon'),(183,3,'Saint Pierre and Miquelon'),(183,4,'Saint Pierre and Miquelon'),(184,1,'Saint Vincent and the Grenadines'),(184,2,'St. Vincent & Grenadines'),(184,3,'Saint Vincent and the Grenadines'),(184,4,'Saint Vincent and the Grenadines'),(185,1,'Samoa'),(185,2,'Samoa'),(185,3,'Samoa'),(185,4,'Samoa'),(186,1,'San Marino'),(186,2,'San Marino'),(186,3,'San Marino'),(186,4,'San Marino'),(187,1,'São Tomé and Príncipe'),(187,2,'São Tomé & Príncipe'),(187,3,'São Tomé and Príncipe'),(187,4,'São Tomé and Príncipe'),(188,1,'Saudi Arabia'),(188,2,'Saudi Arabia'),(188,3,'Saudi Arabia'),(188,4,'Saudi Arabia'),(189,1,'Senegal'),(189,2,'Senegal'),(189,3,'Senegal'),(189,4,'Senegal'),(190,1,'Serbia'),(190,2,'Serbia'),(190,3,'Serbia'),(190,4,'Serbia'),(191,1,'Seychelles'),(191,2,'Seychelles'),(191,3,'Seychelles'),(191,4,'Seychelles'),(192,1,'Sierra Leone'),(192,2,'Sierra Leone'),(192,3,'Sierra Leone'),(192,4,'Sierra Leone'),(193,1,'Slovenia'),(193,2,'Slovenia'),(193,3,'Slovenia'),(193,4,'Slovenia'),(194,1,'Solomon Islands'),(194,2,'Solomon Islands'),(194,3,'Solomon Islands'),(194,4,'Solomon Islands'),(195,1,'Somalia'),(195,2,'Somalia'),(195,3,'Somalia'),(195,4,'Somalia'),(196,1,'South Georgia and the South Sandwich Islands'),(196,2,'South Georgia & South Sandwich Islands'),(196,3,'South Georgia and the South Sandwich Islands'),(196,4,'South Georgia and the South Sandwich Islands'),(197,1,'Sri Lanka'),(197,2,'Sri Lanka'),(197,3,'Sri Lanka'),(197,4,'Sri Lanka'),(198,1,'Sudan'),(198,2,'Sudan'),(198,3,'Sudan'),(198,4,'Sudan'),(199,1,'Suriname'),(199,2,'Suriname'),(199,3,'Suriname'),(199,4,'Suriname'),(200,1,'Svalbard and Jan Mayen'),(200,2,'Svalbard & Jan Mayen'),(200,3,'Svalbard and Jan Mayen'),(200,4,'Svalbard and Jan Mayen'),(201,1,'Swaziland'),(201,2,'Swaziland'),(201,3,'Swaziland'),(201,4,'Swaziland'),(202,1,'Syria'),(202,2,'Syria'),(202,3,'Syria'),(202,4,'Syria'),(203,1,'台灣'),(203,2,'Taiwan'),(203,3,'Taiwan'),(203,4,'Taiwan'),(204,1,'Tajikistan'),(204,2,'Tajikistan'),(204,3,'Tajikistan'),(204,4,'Tajikistan'),(205,1,'Tanzania'),(205,2,'Tanzania'),(205,3,'Tanzania'),(205,4,'Tanzania'),(206,1,'Thailand'),(206,2,'Thailand'),(206,3,'Thailand'),(206,4,'Thailand'),(207,1,'Tokelau'),(207,2,'Tokelau'),(207,3,'Tokelau'),(207,4,'Tokelau'),(208,1,'Tonga'),(208,2,'Tonga'),(208,3,'Tonga'),(208,4,'Tonga'),(209,1,'Trinidad and Tobago'),(209,2,'Trinidad & Tobago'),(209,3,'Trinidad and Tobago'),(209,4,'Trinidad and Tobago'),(210,1,'Tunisia'),(210,2,'Tunisia'),(210,3,'Tunisia'),(210,4,'Tunisia'),(211,1,'Turkey'),(211,2,'Turkey'),(211,3,'Turkey'),(211,4,'Turkey'),(212,1,'Turkmenistan'),(212,2,'Turkmenistan'),(212,3,'Turkmenistan'),(212,4,'Turkmenistan'),(213,1,'Turks and Caicos Islands'),(213,2,'Turks & Caicos Islands'),(213,3,'Turks and Caicos Islands'),(213,4,'Turks and Caicos Islands'),(214,1,'Tuvalu'),(214,2,'Tuvalu'),(214,3,'Tuvalu'),(214,4,'Tuvalu'),(215,1,'Uganda'),(215,2,'Uganda'),(215,3,'Uganda'),(215,4,'Uganda'),(216,1,'Ukraine'),(216,2,'Ukraine'),(216,3,'Ukraine'),(216,4,'Ukraine'),(217,1,'United Arab Emirates'),(217,2,'United Arab Emirates'),(217,3,'United Arab Emirates'),(217,4,'United Arab Emirates'),(218,1,'Uruguay'),(218,2,'Uruguay'),(218,3,'Uruguay'),(218,4,'Uruguay'),(219,1,'Uzbekistan'),(219,2,'Uzbekistan'),(219,3,'Uzbekistan'),(219,4,'Uzbekistan'),(220,1,'Vanuatu'),(220,2,'Vanuatu'),(220,3,'Vanuatu'),(220,4,'Vanuatu'),(221,1,'Venezuela'),(221,2,'Venezuela'),(221,3,'Venezuela'),(221,4,'Venezuela'),(222,1,'Vietnam'),(222,2,'Vietnam'),(222,3,'Vietnam'),(222,4,'Vietnam'),(223,1,'Virgin Islands (British)'),(223,2,'British Virgin Islands'),(223,3,'Virgin Islands (British)'),(223,4,'Virgin Islands (British)'),(224,1,'Virgin Islands (U.S.)'),(224,2,'U.S. Virgin Islands'),(224,3,'Virgin Islands (U.S.)'),(224,4,'Virgin Islands (U.S.)'),(225,1,'Wallis and Futuna'),(225,2,'Wallis & Futuna'),(225,3,'Wallis and Futuna'),(225,4,'Wallis and Futuna'),(226,1,'Western Sahara'),(226,2,'Western Sahara'),(226,3,'Western Sahara'),(226,4,'Western Sahara'),(227,1,'Yemen'),(227,2,'Yemen'),(227,3,'Yemen'),(227,4,'Yemen'),(228,1,'Zambia'),(228,2,'Zambia'),(228,3,'Zambia'),(228,4,'Zambia'),(229,1,'Zimbabwe'),(229,2,'Zimbabwe'),(229,3,'Zimbabwe'),(229,4,'Zimbabwe'),(230,1,'Albania'),(230,2,'Albania'),(230,3,'Albania'),(230,4,'Albania'),(231,1,'Afghanistan'),(231,2,'Afghanistan'),(231,3,'Afghanistan'),(231,4,'Afghanistan'),(232,1,'Antarctica'),(232,2,'Antarctica'),(232,3,'Antarctica'),(232,4,'Antarctica'),(233,1,'Bosnia and Herzegovina'),(233,2,'Bosnia & Herzegovina'),(233,3,'Bosnia and Herzegovina'),(233,4,'Bosnia and Herzegovina'),(234,1,'Bouvet Island'),(234,2,'Bouvet Island'),(234,3,'Bouvet Island'),(234,4,'Bouvet Island'),(235,1,'British Indian Ocean Territory'),(235,2,'British Indian Ocean Territory'),(235,3,'British Indian Ocean Territory'),(235,4,'British Indian Ocean Territory'),(236,1,'Bulgaria'),(236,2,'Bulgaria'),(236,3,'Bulgaria'),(236,4,'Bulgaria'),(237,1,'Cayman Islands'),(237,2,'Cayman Islands'),(237,3,'Cayman Islands'),(237,4,'Cayman Islands'),(238,1,'Christmas Island'),(238,2,'Christmas Island'),(238,3,'Christmas Island'),(238,4,'Christmas Island'),(239,1,'Cocos (Keeling) Islands'),(239,2,'Cocos (Keeling) Islands'),(239,3,'Cocos (Keeling) Islands'),(239,4,'Cocos (Keeling) Islands'),(240,1,'Cook Islands'),(240,2,'Cook Islands'),(240,3,'Cook Islands'),(240,4,'Cook Islands'),(241,1,'French Guiana'),(241,2,'French Guiana'),(241,3,'French Guiana'),(241,4,'French Guiana'),(242,1,'French Polynesia'),(242,2,'French Polynesia'),(242,3,'French Polynesia'),(242,4,'French Polynesia'),(243,1,'French Southern Territories'),(243,2,'French Southern Territories'),(243,3,'French Southern Territories'),(243,4,'French Southern Territories'),(244,1,'Åland Islands'),(244,2,'Åland Islands'),(244,3,'Åland Islands'),(244,4,'Åland Islands');
/*!40000 ALTER TABLE `ps_country_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_country_shop`
--

DROP TABLE IF EXISTS `ps_country_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_country_shop` (
  `id_country` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_country`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_country_shop`
--

LOCK TABLES `ps_country_shop` WRITE;
/*!40000 ALTER TABLE `ps_country_shop` DISABLE KEYS */;
INSERT INTO `ps_country_shop` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1),(37,1),(38,1),(39,1),(40,1),(41,1),(42,1),(43,1),(44,1),(45,1),(46,1),(47,1),(48,1),(49,1),(50,1),(51,1),(52,1),(53,1),(54,1),(55,1),(56,1),(57,1),(58,1),(59,1),(60,1),(61,1),(62,1),(63,1),(64,1),(65,1),(66,1),(67,1),(68,1),(69,1),(70,1),(71,1),(72,1),(73,1),(74,1),(75,1),(76,1),(77,1),(78,1),(79,1),(80,1),(81,1),(82,1),(83,1),(84,1),(85,1),(86,1),(87,1),(88,1),(89,1),(90,1),(91,1),(92,1),(93,1),(94,1),(95,1),(96,1),(97,1),(98,1),(99,1),(100,1),(101,1),(102,1),(103,1),(104,1),(105,1),(106,1),(107,1),(108,1),(109,1),(110,1),(111,1),(112,1),(113,1),(114,1),(115,1),(116,1),(117,1),(118,1),(119,1),(120,1),(121,1),(122,1),(123,1),(124,1),(125,1),(126,1),(127,1),(128,1),(129,1),(130,1),(131,1),(132,1),(133,1),(134,1),(135,1),(136,1),(137,1),(138,1),(139,1),(140,1),(141,1),(142,1),(143,1),(144,1),(145,1),(146,1),(147,1),(148,1),(149,1),(150,1),(151,1),(152,1),(153,1),(154,1),(155,1),(156,1),(157,1),(158,1),(159,1),(160,1),(161,1),(162,1),(163,1),(164,1),(165,1),(166,1),(167,1),(168,1),(169,1),(170,1),(171,1),(172,1),(173,1),(174,1),(175,1),(176,1),(177,1),(178,1),(179,1),(180,1),(181,1),(182,1),(183,1),(184,1),(185,1),(186,1),(187,1),(188,1),(189,1),(190,1),(191,1),(192,1),(193,1),(194,1),(195,1),(196,1),(197,1),(198,1),(199,1),(200,1),(201,1),(202,1),(203,1),(204,1),(205,1),(206,1),(207,1),(208,1),(209,1),(210,1),(211,1),(212,1),(213,1),(214,1),(215,1),(216,1),(217,1),(218,1),(219,1),(220,1),(221,1),(222,1),(223,1),(224,1),(225,1),(226,1),(227,1),(228,1),(229,1),(230,1),(231,1),(232,1),(233,1),(234,1),(235,1),(236,1),(237,1),(238,1),(239,1),(240,1),(241,1),(242,1),(243,1),(244,1),(1,2),(2,2),(3,2),(4,2),(5,2),(6,2),(7,2),(8,2),(9,2),(10,2),(11,2),(12,2),(13,2),(14,2),(15,2),(16,2),(17,2),(18,2),(19,2),(20,2),(21,2),(22,2),(23,2),(24,2),(25,2),(26,2),(27,2),(28,2),(29,2),(30,2),(31,2),(32,2),(33,2),(34,2),(35,2),(36,2),(37,2),(38,2),(39,2),(40,2),(41,2),(42,2),(43,2),(44,2),(45,2),(46,2),(47,2),(48,2),(49,2),(50,2),(51,2),(52,2),(53,2),(54,2),(55,2),(56,2),(57,2),(58,2),(59,2),(60,2),(61,2),(62,2),(63,2),(64,2),(65,2),(66,2),(67,2),(68,2),(69,2),(70,2),(71,2),(72,2),(73,2),(74,2),(75,2),(76,2),(77,2),(78,2),(79,2),(80,2),(81,2),(82,2),(83,2),(84,2),(85,2),(86,2),(87,2),(88,2),(89,2),(90,2),(91,2),(92,2),(93,2),(94,2),(95,2),(96,2),(97,2),(98,2),(99,2),(100,2),(101,2),(102,2),(103,2),(104,2),(105,2),(106,2),(107,2),(108,2),(109,2),(110,2),(111,2),(112,2),(113,2),(114,2),(115,2),(116,2),(117,2),(118,2),(119,2),(120,2),(121,2),(122,2),(123,2),(124,2),(125,2),(126,2),(127,2),(128,2),(129,2),(130,2),(131,2),(132,2),(133,2),(134,2),(135,2),(136,2),(137,2),(138,2),(139,2),(140,2),(141,2),(142,2),(143,2),(144,2),(145,2),(146,2),(147,2),(148,2),(149,2),(150,2),(151,2),(152,2),(153,2),(154,2),(155,2),(156,2),(157,2),(158,2),(159,2),(160,2),(161,2),(162,2),(163,2),(164,2),(165,2),(166,2),(167,2),(168,2),(169,2),(170,2),(171,2),(172,2),(173,2),(174,2),(175,2),(176,2),(177,2),(178,2),(179,2),(180,2),(181,2),(182,2),(183,2),(184,2),(185,2),(186,2),(187,2),(188,2),(189,2),(190,2),(191,2),(192,2),(193,2),(194,2),(195,2),(196,2),(197,2),(198,2),(199,2),(200,2),(201,2),(202,2),(203,2),(204,2),(205,2),(206,2),(207,2),(208,2),(209,2),(210,2),(211,2),(212,2),(213,2),(214,2),(215,2),(216,2),(217,2),(218,2),(219,2),(220,2),(221,2),(222,2),(223,2),(224,2),(225,2),(226,2),(227,2),(228,2),(229,2),(230,2),(231,2),(232,2),(233,2),(234,2),(235,2),(236,2),(237,2),(238,2),(239,2),(240,2),(241,2),(242,2),(243,2),(244,2),(1,3),(2,3),(3,3),(4,3),(5,3),(6,3),(7,3),(8,3),(9,3),(10,3),(11,3),(12,3),(13,3),(14,3),(15,3),(16,3),(17,3),(18,3),(19,3),(20,3),(21,3),(22,3),(23,3),(24,3),(25,3),(26,3),(27,3),(28,3),(29,3),(30,3),(31,3),(32,3),(33,3),(34,3),(35,3),(36,3),(37,3),(38,3),(39,3),(40,3),(41,3),(42,3),(43,3),(44,3),(45,3),(46,3),(47,3),(48,3),(49,3),(50,3),(51,3),(52,3),(53,3),(54,3),(55,3),(56,3),(57,3),(58,3),(59,3),(60,3),(61,3),(62,3),(63,3),(64,3),(65,3),(66,3),(67,3),(68,3),(69,3),(70,3),(71,3),(72,3),(73,3),(74,3),(75,3),(76,3),(77,3),(78,3),(79,3),(80,3),(81,3),(82,3),(83,3),(84,3),(85,3),(86,3),(87,3),(88,3),(89,3),(90,3),(91,3),(92,3),(93,3),(94,3),(95,3),(96,3),(97,3),(98,3),(99,3),(100,3),(101,3),(102,3),(103,3),(104,3),(105,3),(106,3),(107,3),(108,3),(109,3),(110,3),(111,3),(112,3),(113,3),(114,3),(115,3),(116,3),(117,3),(118,3),(119,3),(120,3),(121,3),(122,3),(123,3),(124,3),(125,3),(126,3),(127,3),(128,3),(129,3),(130,3),(131,3),(132,3),(133,3),(134,3),(135,3),(136,3),(137,3),(138,3),(139,3),(140,3),(141,3),(142,3),(143,3),(144,3),(145,3),(146,3),(147,3),(148,3),(149,3),(150,3),(151,3),(152,3),(153,3),(154,3),(155,3),(156,3),(157,3),(158,3),(159,3),(160,3),(161,3),(162,3),(163,3),(164,3),(165,3),(166,3),(167,3),(168,3),(169,3),(170,3),(171,3),(172,3),(173,3),(174,3),(175,3),(176,3),(177,3),(178,3),(179,3),(180,3),(181,3),(182,3),(183,3),(184,3),(185,3),(186,3),(187,3),(188,3),(189,3),(190,3),(191,3),(192,3),(193,3),(194,3),(195,3),(196,3),(197,3),(198,3),(199,3),(200,3),(201,3),(202,3),(203,3),(204,3),(205,3),(206,3),(207,3),(208,3),(209,3),(210,3),(211,3),(212,3),(213,3),(214,3),(215,3),(216,3),(217,3),(218,3),(219,3),(220,3),(221,3),(222,3),(223,3),(224,3),(225,3),(226,3),(227,3),(228,3),(229,3),(230,3),(231,3),(232,3),(233,3),(234,3),(235,3),(236,3),(237,3),(238,3),(239,3),(240,3),(241,3),(242,3),(243,3),(244,3),(1,4),(2,4),(3,4),(4,4),(5,4),(6,4),(7,4),(8,4),(9,4),(10,4),(11,4),(12,4),(13,4),(14,4),(15,4),(16,4),(17,4),(18,4),(19,4),(20,4),(21,4),(22,4),(23,4),(24,4),(25,4),(26,4),(27,4),(28,4),(29,4),(30,4),(31,4),(32,4),(33,4),(34,4),(35,4),(36,4),(37,4),(38,4),(39,4),(40,4),(41,4),(42,4),(43,4),(44,4),(45,4),(46,4),(47,4),(48,4),(49,4),(50,4),(51,4),(52,4),(53,4),(54,4),(55,4),(56,4),(57,4),(58,4),(59,4),(60,4),(61,4),(62,4),(63,4),(64,4),(65,4),(66,4),(67,4),(68,4),(69,4),(70,4),(71,4),(72,4),(73,4),(74,4),(75,4),(76,4),(77,4),(78,4),(79,4),(80,4),(81,4),(82,4),(83,4),(84,4),(85,4),(86,4),(87,4),(88,4),(89,4),(90,4),(91,4),(92,4),(93,4),(94,4),(95,4),(96,4),(97,4),(98,4),(99,4),(100,4),(101,4),(102,4),(103,4),(104,4),(105,4),(106,4),(107,4),(108,4),(109,4),(110,4),(111,4),(112,4),(113,4),(114,4),(115,4),(116,4),(117,4),(118,4),(119,4),(120,4),(121,4),(122,4),(123,4),(124,4),(125,4),(126,4),(127,4),(128,4),(129,4),(130,4),(131,4),(132,4),(133,4),(134,4),(135,4),(136,4),(137,4),(138,4),(139,4),(140,4),(141,4),(142,4),(143,4),(144,4),(145,4),(146,4),(147,4),(148,4),(149,4),(150,4),(151,4),(152,4),(153,4),(154,4),(155,4),(156,4),(157,4),(158,4),(159,4),(160,4),(161,4),(162,4),(163,4),(164,4),(165,4),(166,4),(167,4),(168,4),(169,4),(170,4),(171,4),(172,4),(173,4),(174,4),(175,4),(176,4),(177,4),(178,4),(179,4),(180,4),(181,4),(182,4),(183,4),(184,4),(185,4),(186,4),(187,4),(188,4),(189,4),(190,4),(191,4),(192,4),(193,4),(194,4),(195,4),(196,4),(197,4),(198,4),(199,4),(200,4),(201,4),(202,4),(203,4),(204,4),(205,4),(206,4),(207,4),(208,4),(209,4),(210,4),(211,4),(212,4),(213,4),(214,4),(215,4),(216,4),(217,4),(218,4),(219,4),(220,4),(221,4),(222,4),(223,4),(224,4),(225,4),(226,4),(227,4),(228,4),(229,4),(230,4),(231,4),(232,4),(233,4),(234,4),(235,4),(236,4),(237,4),(238,4),(239,4),(240,4),(241,4),(242,4),(243,4),(244,4);
/*!40000 ALTER TABLE `ps_country_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_currency`
--

DROP TABLE IF EXISTS `ps_currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_currency` (
  `id_currency` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `iso_code` varchar(3) NOT NULL DEFAULT '0',
  `conversion_rate` decimal(13,6) NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_currency`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_currency`
--

LOCK TABLES `ps_currency` WRITE;
/*!40000 ALTER TABLE `ps_currency` DISABLE KEYS */;
INSERT INTO `ps_currency` VALUES (1,'新臺幣','TWD',1.000000,0,1),(2,'日圓','JPY',3.691822,0,0),(3,'美元','USD',0.032673,0,1),(4,'新加坡幣','SGD',0.044639,0,0),(5,'馬來西亞令吉','MYR',0.135168,0,0),(6,'港幣','HKD',0.255183,0,1),(7,'人民幣','CNY',0.224434,0,1);
/*!40000 ALTER TABLE `ps_currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_currency_shop`
--

DROP TABLE IF EXISTS `ps_currency_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_currency_shop` (
  `id_currency` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  `conversion_rate` decimal(13,6) NOT NULL,
  PRIMARY KEY (`id_currency`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_currency_shop`
--

LOCK TABLES `ps_currency_shop` WRITE;
/*!40000 ALTER TABLE `ps_currency_shop` DISABLE KEYS */;
INSERT INTO `ps_currency_shop` VALUES (1,1,1.000000),(1,2,1.000000),(1,3,1.000000),(1,4,1.000000),(2,1,3.691822),(2,2,3.691822),(2,3,3.691822),(2,4,3.691822),(3,1,0.032673),(3,2,0.032673),(3,3,0.032673),(3,4,0.032673),(4,1,0.044639),(4,2,0.044639),(4,3,0.044639),(4,4,0.044639),(5,1,0.135168),(5,2,0.135168),(5,3,0.135168),(5,4,0.135168),(6,1,0.255183),(6,2,0.255183),(6,3,0.255183),(6,4,0.255183),(7,1,0.224434),(7,2,0.224434),(7,3,0.224434),(7,4,0.224434);
/*!40000 ALTER TABLE `ps_currency_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_customer`
--

DROP TABLE IF EXISTS `ps_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_customer` (
  `id_customer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop_group` int(11) unsigned NOT NULL DEFAULT '1',
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_gender` int(10) unsigned NOT NULL,
  `id_default_group` int(10) unsigned NOT NULL DEFAULT '1',
  `id_lang` int(10) unsigned DEFAULT NULL,
  `id_risk` int(10) unsigned NOT NULL DEFAULT '1',
  `company` varchar(64) DEFAULT NULL,
  `siret` varchar(14) DEFAULT NULL,
  `ape` varchar(5) DEFAULT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(128) NOT NULL,
  `passwd` varchar(60) NOT NULL,
  `last_passwd_gen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `birthday` date DEFAULT NULL,
  `newsletter` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ip_registration_newsletter` varchar(15) DEFAULT NULL,
  `newsletter_date_add` datetime DEFAULT NULL,
  `optin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `website` varchar(128) DEFAULT NULL,
  `outstanding_allow_amount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `show_public_prices` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `max_payment_days` int(10) unsigned NOT NULL DEFAULT '60',
  `secure_key` varchar(32) NOT NULL DEFAULT '-1',
  `note` text,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_guest` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `reset_password_token` varchar(40) DEFAULT NULL,
  `reset_password_validity` datetime DEFAULT NULL,
  PRIMARY KEY (`id_customer`),
  KEY `customer_email` (`email`),
  KEY `customer_login` (`email`,`passwd`),
  KEY `id_customer_passwd` (`id_customer`,`passwd`),
  KEY `id_gender` (`id_gender`),
  KEY `id_shop_group` (`id_shop_group`),
  KEY `id_shop` (`id_shop`,`date_add`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_customer`
--

LOCK TABLES `ps_customer` WRITE;
/*!40000 ALTER TABLE `ps_customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_customer_group`
--

DROP TABLE IF EXISTS `ps_customer_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_customer_group` (
  `id_customer` int(10) unsigned NOT NULL,
  `id_group` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_customer`,`id_group`),
  KEY `customer_login` (`id_group`),
  KEY `id_customer` (`id_customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_customer_group`
--

LOCK TABLES `ps_customer_group` WRITE;
/*!40000 ALTER TABLE `ps_customer_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_customer_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_customer_message`
--

DROP TABLE IF EXISTS `ps_customer_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_customer_message` (
  `id_customer_message` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_customer_thread` int(11) DEFAULT NULL,
  `id_employee` int(10) unsigned DEFAULT NULL,
  `message` mediumtext NOT NULL,
  `file_name` varchar(18) DEFAULT NULL,
  `ip_address` varchar(16) DEFAULT NULL,
  `user_agent` varchar(128) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `private` tinyint(4) NOT NULL DEFAULT '0',
  `read` tinyint(1) NOT NULL DEFAULT '0',
  `system` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否為系統訊息',
  PRIMARY KEY (`id_customer_message`),
  KEY `id_customer_thread` (`id_customer_thread`),
  KEY `id_employee` (`id_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_customer_message`
--

LOCK TABLES `ps_customer_message` WRITE;
/*!40000 ALTER TABLE `ps_customer_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_customer_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_customer_message_sync_imap`
--

DROP TABLE IF EXISTS `ps_customer_message_sync_imap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_customer_message_sync_imap` (
  `md5_header` varbinary(32) NOT NULL,
  KEY `md5_header_index` (`md5_header`(4))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_customer_message_sync_imap`
--

LOCK TABLES `ps_customer_message_sync_imap` WRITE;
/*!40000 ALTER TABLE `ps_customer_message_sync_imap` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_customer_message_sync_imap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_customer_thread`
--

DROP TABLE IF EXISTS `ps_customer_thread`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_customer_thread` (
  `id_customer_thread` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_lang` int(10) unsigned NOT NULL,
  `id_contact` int(10) unsigned NOT NULL,
  `id_customer` int(10) unsigned DEFAULT NULL,
  `id_order` int(10) unsigned DEFAULT NULL,
  `id_product` int(10) unsigned DEFAULT NULL,
  `status` enum('open','closed','pending1','pending2') NOT NULL DEFAULT 'open',
  `email` varchar(128) NOT NULL,
  `token` varchar(12) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_customer_thread`),
  KEY `id_shop` (`id_shop`),
  KEY `id_lang` (`id_lang`),
  KEY `id_contact` (`id_contact`),
  KEY `id_customer` (`id_customer`),
  KEY `id_order` (`id_order`),
  KEY `id_product` (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_customer_thread`
--

LOCK TABLES `ps_customer_thread` WRITE;
/*!40000 ALTER TABLE `ps_customer_thread` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_customer_thread` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_customization`
--

DROP TABLE IF EXISTS `ps_customization`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_customization` (
  `id_customization` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_product_attribute` int(10) unsigned NOT NULL DEFAULT '0',
  `id_address_delivery` int(10) unsigned NOT NULL DEFAULT '0',
  `id_cart` int(10) unsigned NOT NULL,
  `id_product` int(10) NOT NULL,
  `quantity` int(10) NOT NULL,
  `quantity_refunded` int(11) NOT NULL DEFAULT '0',
  `quantity_returned` int(11) NOT NULL DEFAULT '0',
  `in_cart` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_customization`,`id_cart`,`id_product`,`id_address_delivery`),
  KEY `id_product_attribute` (`id_product_attribute`),
  KEY `id_cart_product` (`id_cart`,`id_product`,`id_product_attribute`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_customization`
--

LOCK TABLES `ps_customization` WRITE;
/*!40000 ALTER TABLE `ps_customization` DISABLE KEYS */;
INSERT INTO `ps_customization` VALUES (1,0,1,1,12,1,0,0,1),(3,0,2,2,12,1,0,0,1);
/*!40000 ALTER TABLE `ps_customization` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_customization_field`
--

DROP TABLE IF EXISTS `ps_customization_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_customization_field` (
  `id_customization_field` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_product` int(10) unsigned NOT NULL,
  `type` tinyint(1) NOT NULL,
  `required` tinyint(1) NOT NULL,
  `is_module` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_customization_field`),
  KEY `id_product` (`id_product`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_customization_field`
--

LOCK TABLES `ps_customization_field` WRITE;
/*!40000 ALTER TABLE `ps_customization_field` DISABLE KEYS */;
INSERT INTO `ps_customization_field` VALUES (1,12,1,0,0,0),(2,12,0,0,0,0);
/*!40000 ALTER TABLE `ps_customization_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_customization_field_lang`
--

DROP TABLE IF EXISTS `ps_customization_field_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_customization_field_lang` (
  `id_customization_field` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_customization_field`,`id_lang`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_customization_field_lang`
--

LOCK TABLES `ps_customization_field_lang` WRITE;
/*!40000 ALTER TABLE `ps_customization_field_lang` DISABLE KEYS */;
INSERT INTO `ps_customization_field_lang` VALUES (1,1,1,'刻字'),(2,1,1,'圖案');
/*!40000 ALTER TABLE `ps_customization_field_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_customized_data`
--

DROP TABLE IF EXISTS `ps_customized_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_customized_data` (
  `id_customization` int(10) unsigned NOT NULL,
  `type` tinyint(1) NOT NULL,
  `index` int(3) NOT NULL,
  `value` varchar(255) NOT NULL,
  `id_module` int(10) NOT NULL DEFAULT '0',
  `price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `weight` decimal(20,6) NOT NULL DEFAULT '0.000000',
  PRIMARY KEY (`id_customization`,`type`,`index`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_customized_data`
--

LOCK TABLES `ps_customized_data` WRITE;
/*!40000 ALTER TABLE `ps_customized_data` DISABLE KEYS */;
INSERT INTO `ps_customized_data` VALUES (1,1,1,'123',0,0.000000,0.000000),(3,0,2,'bc7324c49d3eeae46735406183ac40d0',0,0.000000,0.000000),(3,1,1,'123',0,0.000000,0.000000);
/*!40000 ALTER TABLE `ps_customized_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_date_range`
--

DROP TABLE IF EXISTS `ps_date_range`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_date_range` (
  `id_date_range` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `time_start` datetime NOT NULL,
  `time_end` datetime NOT NULL,
  PRIMARY KEY (`id_date_range`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_date_range`
--

LOCK TABLES `ps_date_range` WRITE;
/*!40000 ALTER TABLE `ps_date_range` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_date_range` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_delivery`
--

DROP TABLE IF EXISTS `ps_delivery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_delivery` (
  `id_delivery` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned DEFAULT NULL,
  `id_shop_group` int(10) unsigned DEFAULT NULL,
  `id_carrier` int(10) unsigned NOT NULL,
  `id_range_price` int(10) unsigned DEFAULT NULL,
  `id_range_weight` int(10) unsigned DEFAULT NULL,
  `id_zone` int(10) unsigned NOT NULL,
  `price` decimal(20,6) NOT NULL,
  PRIMARY KEY (`id_delivery`),
  KEY `id_zone` (`id_zone`),
  KEY `id_carrier` (`id_carrier`,`id_zone`),
  KEY `id_range_price` (`id_range_price`),
  KEY `id_range_weight` (`id_range_weight`)
) ENGINE=InnoDB AUTO_INCREMENT=253 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_delivery`
--

LOCK TABLES `ps_delivery` WRITE;
/*!40000 ALTER TABLE `ps_delivery` DISABLE KEYS */;
INSERT INTO `ps_delivery` VALUES (222,NULL,NULL,103,7,NULL,9,60.000000),(223,NULL,NULL,103,38,NULL,9,30.000000),(224,NULL,NULL,104,9,NULL,9,60.000000),(225,NULL,NULL,104,10,NULL,9,30.000000),(226,NULL,NULL,105,34,NULL,9,60.000000),(227,NULL,NULL,105,35,NULL,9,30.000000),(228,NULL,NULL,106,36,NULL,9,60.000000),(229,NULL,NULL,106,37,NULL,9,30.000000),(230,NULL,NULL,107,19,NULL,9,150.000000),(231,NULL,NULL,107,25,NULL,9,120.000000),(232,NULL,NULL,107,26,NULL,9,0.000000),(233,NULL,NULL,108,39,NULL,9,190.000000),(234,NULL,NULL,108,40,NULL,9,160.000000),(235,NULL,NULL,108,41,NULL,9,0.000000),(236,NULL,NULL,109,42,NULL,9,190.000000),(237,NULL,NULL,109,43,NULL,9,160.000000),(238,NULL,NULL,109,44,NULL,9,0.000000),(239,NULL,NULL,110,21,NULL,9,180.000000),(240,NULL,NULL,110,32,NULL,9,150.000000),(241,NULL,NULL,110,33,NULL,9,0.000000),(242,NULL,NULL,111,29,NULL,9,210.000000),(243,NULL,NULL,111,30,NULL,9,180.000000),(244,NULL,NULL,111,31,NULL,9,0.000000),(245,NULL,NULL,112,23,NULL,9,210.000000),(246,NULL,NULL,112,27,NULL,9,180.000000),(247,NULL,NULL,112,28,NULL,9,0.000000),(248,NULL,NULL,113,11,NULL,9,35.000000),(249,NULL,NULL,113,12,NULL,9,0.000000),(251,NULL,NULL,114,45,NULL,9,200.000000),(252,NULL,NULL,115,45,NULL,9,200.000000);
/*!40000 ALTER TABLE `ps_delivery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_emailsubscription`
--

DROP TABLE IF EXISTS `ps_emailsubscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_emailsubscription` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
  `id_shop_group` int(10) unsigned NOT NULL DEFAULT '1',
  `email` varchar(255) NOT NULL,
  `newsletter_date_add` datetime DEFAULT NULL,
  `ip_registration_newsletter` varchar(15) NOT NULL,
  `http_referer` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_emailsubscription`
--

LOCK TABLES `ps_emailsubscription` WRITE;
/*!40000 ALTER TABLE `ps_emailsubscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_emailsubscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_employee`
--

DROP TABLE IF EXISTS `ps_employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_employee` (
  `id_employee` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_profile` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL DEFAULT '0',
  `lastname` varchar(32) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `email` varchar(128) NOT NULL,
  `passwd` varchar(60) NOT NULL,
  `last_passwd_gen` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `stats_date_from` date DEFAULT NULL,
  `stats_date_to` date DEFAULT NULL,
  `stats_compare_from` date DEFAULT NULL,
  `stats_compare_to` date DEFAULT NULL,
  `stats_compare_option` int(1) unsigned NOT NULL DEFAULT '1',
  `preselect_date_range` varchar(32) DEFAULT NULL,
  `bo_color` varchar(32) DEFAULT NULL,
  `bo_theme` varchar(32) DEFAULT NULL,
  `bo_css` varchar(64) DEFAULT NULL,
  `default_tab` int(10) unsigned NOT NULL DEFAULT '0',
  `bo_width` int(10) unsigned NOT NULL DEFAULT '0',
  `bo_menu` tinyint(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `optin` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `id_last_order` int(10) unsigned NOT NULL DEFAULT '0',
  `id_last_customer_message` int(10) unsigned NOT NULL DEFAULT '0',
  `id_last_customer` int(10) unsigned NOT NULL DEFAULT '0',
  `last_connection_date` date DEFAULT NULL,
  `reset_password_token` varchar(40) DEFAULT NULL,
  `reset_password_validity` datetime DEFAULT NULL,
  PRIMARY KEY (`id_employee`),
  KEY `employee_login` (`email`,`passwd`),
  KEY `id_employee_passwd` (`id_employee`,`passwd`),
  KEY `id_profile` (`id_profile`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_employee`
--

LOCK TABLES `ps_employee` WRITE;
/*!40000 ALTER TABLE `ps_employee` DISABLE KEYS */;
INSERT INTO `ps_employee` VALUES (1,1,1,'管','理員','admin@example.com','$2y$10$VdeZo1fw.gdf4yCkyLowKuZ74d.TKH79JJBZL0ahGjZCQSOc4YSbW','2018-07-10 21:57:39','2018-11-01','2018-11-30','0000-00-00','0000-00-00',1,'year','','default','theme.css',1,0,1,1,1,1,0,1,NULL,'','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `ps_employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_employee_shop`
--

DROP TABLE IF EXISTS `ps_employee_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_employee_shop` (
  `id_employee` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_employee`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_employee_shop`
--

LOCK TABLES `ps_employee_shop` WRITE;
/*!40000 ALTER TABLE `ps_employee_shop` DISABLE KEYS */;
INSERT INTO `ps_employee_shop` VALUES (1,1),(1,2),(1,3),(1,4);
/*!40000 ALTER TABLE `ps_employee_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_fbmessenger_message`
--

DROP TABLE IF EXISTS `ps_fbmessenger_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_fbmessenger_message` (
  `id_message` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_subscription` int(10) unsigned DEFAULT NULL,
  `type` tinyint(1) unsigned DEFAULT NULL,
  `app_id` int(10) unsigned DEFAULT NULL,
  `ref` varchar(100) DEFAULT NULL,
  `message_id` varchar(100) DEFAULT NULL,
  `message_text` text,
  `click` int(10) unsigned NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_message`),
  KEY `id_subscription` (`id_subscription`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_fbmessenger_message`
--

LOCK TABLES `ps_fbmessenger_message` WRITE;
/*!40000 ALTER TABLE `ps_fbmessenger_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_fbmessenger_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_fbmessenger_subscription`
--

DROP TABLE IF EXISTS `ps_fbmessenger_subscription`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_fbmessenger_subscription` (
  `id_subscription` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cart` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `id_order` int(10) unsigned DEFAULT NULL,
  `order_date_add` datetime DEFAULT NULL,
  `ref` varchar(255) NOT NULL COMMENT '輔 ref',
  `user_ref` varchar(255) NOT NULL COMMENT '主 ref',
  `token` varchar(50) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_subscription`),
  KEY `id_cart` (`id_cart`),
  KEY `id_order` (`id_order`),
  KEY `user_ref` (`user_ref`),
  KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_fbmessenger_subscription`
--

LOCK TABLES `ps_fbmessenger_subscription` WRITE;
/*!40000 ALTER TABLE `ps_fbmessenger_subscription` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_fbmessenger_subscription` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_feature`
--

DROP TABLE IF EXISTS `ps_feature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_feature` (
  `id_feature` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_feature`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_feature`
--

LOCK TABLES `ps_feature` WRITE;
/*!40000 ALTER TABLE `ps_feature` DISABLE KEYS */;
INSERT INTO `ps_feature` VALUES (1,0);
/*!40000 ALTER TABLE `ps_feature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_feature_lang`
--

DROP TABLE IF EXISTS `ps_feature_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_feature_lang` (
  `id_feature` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_feature`,`id_lang`),
  KEY `id_lang` (`id_lang`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_feature_lang`
--

LOCK TABLES `ps_feature_lang` WRITE;
/*!40000 ALTER TABLE `ps_feature_lang` DISABLE KEYS */;
INSERT INTO `ps_feature_lang` VALUES (1,1,'規格1'),(1,2,'特性'),(1,3,'特性'),(1,4,'特性');
/*!40000 ALTER TABLE `ps_feature_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_feature_product`
--

DROP TABLE IF EXISTS `ps_feature_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_feature_product` (
  `id_feature` int(10) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `id_feature_value` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_feature`,`id_product`,`id_feature_value`),
  KEY `id_feature_value` (`id_feature_value`),
  KEY `id_product` (`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_feature_product`
--

LOCK TABLES `ps_feature_product` WRITE;
/*!40000 ALTER TABLE `ps_feature_product` DISABLE KEYS */;
INSERT INTO `ps_feature_product` VALUES (1,12,1);
/*!40000 ALTER TABLE `ps_feature_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_feature_shop`
--

DROP TABLE IF EXISTS `ps_feature_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_feature_shop` (
  `id_feature` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_feature`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_feature_shop`
--

LOCK TABLES `ps_feature_shop` WRITE;
/*!40000 ALTER TABLE `ps_feature_shop` DISABLE KEYS */;
INSERT INTO `ps_feature_shop` VALUES (1,1);
/*!40000 ALTER TABLE `ps_feature_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_feature_value`
--

DROP TABLE IF EXISTS `ps_feature_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_feature_value` (
  `id_feature_value` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_feature` int(10) unsigned NOT NULL,
  `custom` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_feature_value`),
  KEY `feature` (`id_feature`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_feature_value`
--

LOCK TABLES `ps_feature_value` WRITE;
/*!40000 ALTER TABLE `ps_feature_value` DISABLE KEYS */;
INSERT INTO `ps_feature_value` VALUES (1,1,0),(2,1,0);
/*!40000 ALTER TABLE `ps_feature_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_feature_value_lang`
--

DROP TABLE IF EXISTS `ps_feature_value_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_feature_value_lang` (
  `id_feature_value` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_feature_value`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_feature_value_lang`
--

LOCK TABLES `ps_feature_value_lang` WRITE;
/*!40000 ALTER TABLE `ps_feature_value_lang` DISABLE KEYS */;
INSERT INTO `ps_feature_value_lang` VALUES (1,1,'規格數值1'),(1,2,'特性數值1'),(1,3,'特性數值1'),(1,4,'特性數值1'),(2,1,'規格數值2'),(2,2,'特性數值2'),(2,3,'特性數值2'),(2,4,'特性數值2');
/*!40000 ALTER TABLE `ps_feature_value_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_ganalytics`
--

DROP TABLE IF EXISTS `ps_ganalytics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_ganalytics` (
  `id_google_analytics` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) NOT NULL,
  `id_customer` int(10) NOT NULL,
  `id_shop` int(11) NOT NULL,
  `sent` tinyint(1) DEFAULT NULL,
  `date_add` datetime DEFAULT NULL,
  PRIMARY KEY (`id_google_analytics`),
  KEY `id_order` (`id_order`),
  KEY `sent` (`sent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_ganalytics`
--

LOCK TABLES `ps_ganalytics` WRITE;
/*!40000 ALTER TABLE `ps_ganalytics` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_ganalytics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_ganalytics_data`
--

DROP TABLE IF EXISTS `ps_ganalytics_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_ganalytics_data` (
  `id_cart` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  `data` text,
  PRIMARY KEY (`id_cart`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_ganalytics_data`
--

LOCK TABLES `ps_ganalytics_data` WRITE;
/*!40000 ALTER TABLE `ps_ganalytics_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_ganalytics_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_gender`
--

DROP TABLE IF EXISTS `ps_gender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_gender` (
  `id_gender` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_gender`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_gender`
--

LOCK TABLES `ps_gender` WRITE;
/*!40000 ALTER TABLE `ps_gender` DISABLE KEYS */;
INSERT INTO `ps_gender` VALUES (1,0),(2,1);
/*!40000 ALTER TABLE `ps_gender` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_gender_lang`
--

DROP TABLE IF EXISTS `ps_gender_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_gender_lang` (
  `id_gender` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id_gender`,`id_lang`),
  KEY `id_gender` (`id_gender`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_gender_lang`
--

LOCK TABLES `ps_gender_lang` WRITE;
/*!40000 ALTER TABLE `ps_gender_lang` DISABLE KEYS */;
INSERT INTO `ps_gender_lang` VALUES (1,1,'先生'),(1,2,'Mr.'),(1,3,'先生'),(1,4,'男性'),(2,1,'女士'),(2,2,'夫人'),(2,3,'夫人'),(2,4,'夫人');
/*!40000 ALTER TABLE `ps_gender_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_group`
--

DROP TABLE IF EXISTS `ps_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_group` (
  `id_group` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reduction` decimal(17,2) NOT NULL DEFAULT '0.00',
  `price_display_method` tinyint(4) NOT NULL DEFAULT '0',
  `show_prices` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_group`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_group`
--

LOCK TABLES `ps_group` WRITE;
/*!40000 ALTER TABLE `ps_group` DISABLE KEYS */;
INSERT INTO `ps_group` VALUES (1,0.00,1,1,'2018-07-11 11:57:34','2018-07-13 14:32:13'),(2,0.00,1,1,'2018-07-11 11:57:34','2018-07-13 14:32:13'),(3,0.00,1,1,'2018-07-11 11:57:34','2018-07-13 14:32:13');
/*!40000 ALTER TABLE `ps_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_group_lang`
--

DROP TABLE IF EXISTS `ps_group_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_group_lang` (
  `id_group` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id_group`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_group_lang`
--

LOCK TABLES `ps_group_lang` WRITE;
/*!40000 ALTER TABLE `ps_group_lang` DISABLE KEYS */;
INSERT INTO `ps_group_lang` VALUES (1,1,'訪客'),(1,2,'Visitor'),(1,3,'访客'),(1,4,'Visitor'),(2,1,'顧客'),(2,2,'Customer'),(2,3,'顾客'),(2,4,'Customer'),(3,1,'會員'),(3,2,'Member'),(3,3,'会员'),(3,4,'Member');
/*!40000 ALTER TABLE `ps_group_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_group_reduction`
--

DROP TABLE IF EXISTS `ps_group_reduction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_group_reduction` (
  `id_group_reduction` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `id_group` int(10) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  `reduction` decimal(4,3) NOT NULL,
  PRIMARY KEY (`id_group_reduction`),
  UNIQUE KEY `id_group` (`id_group`,`id_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_group_reduction`
--

LOCK TABLES `ps_group_reduction` WRITE;
/*!40000 ALTER TABLE `ps_group_reduction` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_group_reduction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_group_shop`
--

DROP TABLE IF EXISTS `ps_group_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_group_shop` (
  `id_group` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_group`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_group_shop`
--

LOCK TABLES `ps_group_shop` WRITE;
/*!40000 ALTER TABLE `ps_group_shop` DISABLE KEYS */;
INSERT INTO `ps_group_shop` VALUES (1,1),(2,1),(3,1),(1,2),(2,2),(3,2),(1,3),(2,3),(3,3),(1,4),(2,4),(3,4);
/*!40000 ALTER TABLE `ps_group_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_gsitemap_sitemap`
--

DROP TABLE IF EXISTS `ps_gsitemap_sitemap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_gsitemap_sitemap` (
  `link` varchar(255) DEFAULT NULL,
  `id_shop` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_gsitemap_sitemap`
--

LOCK TABLES `ps_gsitemap_sitemap` WRITE;
/*!40000 ALTER TABLE `ps_gsitemap_sitemap` DISABLE KEYS */;
INSERT INTO `ps_gsitemap_sitemap` VALUES ('1_tw_0_sitemap.xml',1);
/*!40000 ALTER TABLE `ps_gsitemap_sitemap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_gtm_client`
--

DROP TABLE IF EXISTS `ps_gtm_client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_gtm_client` (
  `id_customer` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_client` varchar(50) NOT NULL,
  PRIMARY KEY (`id_customer`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_gtm_client`
--

LOCK TABLES `ps_gtm_client` WRITE;
/*!40000 ALTER TABLE `ps_gtm_client` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_gtm_client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_gtm_orders`
--

DROP TABLE IF EXISTS `ps_gtm_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_gtm_orders` (
  `id_order` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `via` enum('admin','shop') DEFAULT NULL,
  `date_add` datetime DEFAULT NULL,
  PRIMARY KEY (`id_order`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_gtm_orders`
--

LOCK TABLES `ps_gtm_orders` WRITE;
/*!40000 ALTER TABLE `ps_gtm_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_gtm_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_guest`
--

DROP TABLE IF EXISTS `ps_guest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_guest` (
  `id_guest` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_operating_system` int(10) unsigned DEFAULT NULL,
  `id_web_browser` int(10) unsigned DEFAULT NULL,
  `id_customer` int(10) unsigned DEFAULT NULL,
  `javascript` tinyint(1) DEFAULT '0',
  `screen_resolution_x` smallint(5) unsigned DEFAULT NULL,
  `screen_resolution_y` smallint(5) unsigned DEFAULT NULL,
  `screen_color` tinyint(3) unsigned DEFAULT NULL,
  `sun_java` tinyint(1) DEFAULT NULL,
  `adobe_flash` tinyint(1) DEFAULT NULL,
  `adobe_director` tinyint(1) DEFAULT NULL,
  `apple_quicktime` tinyint(1) DEFAULT NULL,
  `real_player` tinyint(1) DEFAULT NULL,
  `windows_media` tinyint(1) DEFAULT NULL,
  `accept_language` varchar(8) DEFAULT NULL,
  `mobile_theme` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_guest`),
  KEY `id_customer` (`id_customer`),
  KEY `id_operating_system` (`id_operating_system`),
  KEY `id_web_browser` (`id_web_browser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_guest`
--

LOCK TABLES `ps_guest` WRITE;
/*!40000 ALTER TABLE `ps_guest` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_guest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_homeslider`
--

DROP TABLE IF EXISTS `ps_homeslider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_homeslider` (
  `id_homeslider_slides` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_homeslider_slides`,`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_homeslider`
--

LOCK TABLES `ps_homeslider` WRITE;
/*!40000 ALTER TABLE `ps_homeslider` DISABLE KEYS */;
INSERT INTO `ps_homeslider` VALUES (5,1),(7,1),(8,1);
/*!40000 ALTER TABLE `ps_homeslider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_homeslider_slides`
--

DROP TABLE IF EXISTS `ps_homeslider_slides`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_homeslider_slides` (
  `id_homeslider_slides` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_homeslider_slides`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_homeslider_slides`
--

LOCK TABLES `ps_homeslider_slides` WRITE;
/*!40000 ALTER TABLE `ps_homeslider_slides` DISABLE KEYS */;
INSERT INTO `ps_homeslider_slides` VALUES (5,0,1),(7,0,0),(8,0,0);
/*!40000 ALTER TABLE `ps_homeslider_slides` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_homeslider_slides_lang`
--

DROP TABLE IF EXISTS `ps_homeslider_slides_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_homeslider_slides_lang` (
  `id_homeslider_slides` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `legend` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id_homeslider_slides`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_homeslider_slides_lang`
--

LOCK TABLES `ps_homeslider_slides_lang` WRITE;
/*!40000 ALTER TABLE `ps_homeslider_slides_lang` DISABLE KEYS */;
INSERT INTO `ps_homeslider_slides_lang` VALUES (5,1,'','','','https://tekapo.io','603a4df522c906ea62e0972f820bbda4be22838e_slider-wave-aqua.png'),(5,2,'','','','https://tekapo.io',''),(5,3,'','','','https://tekapo.io',''),(5,4,'','','','https://tekapo.io',''),(7,1,'','','','https://tekapo.io','0255cacb8f4a6d97ff89f0f3ad6374cc5c47417b_GX日日-slider-tekapo-text-shrink.png'),(7,2,'','','','https://tekapo.io',''),(7,3,'','','','https://tekapo.io',''),(7,4,'','','','https://tekapo.io',''),(8,1,'','<div style=\"text-align:center;\"><a href=\"#\" style=\"color:#2fb5d2;font-weight:bold;\">＼TekapoCart 誕生！ ／<br />限時活動正式開跑</a>\n<div style=\"margin-top:12px;\"><a href=\"#\" style=\"background-color:#f3cb54;color:#fff;padding:8px 23px;border-radius:50px;\">完整活動看這裡 <i class=\"material-icons\" style=\"border-radius:50px;background-color:#2fb5d2;\">navigate_next</i></a></div>\n</div>','','https://tekapo.io/2-all','ac4c44d44f3f92587d3d9a7180ec61de75ac7a26_pebble.png'),(8,2,'','','','https://tekapo.io',''),(8,3,'','','','https://tekapo.io',''),(8,4,'','','','https://tekapo.io','');
/*!40000 ALTER TABLE `ps_homeslider_slides_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_hook`
--

DROP TABLE IF EXISTS `ps_hook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_hook` (
  `id_hook` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `title` varchar(64) NOT NULL,
  `description` text,
  `position` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_hook`),
  UNIQUE KEY `hook_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=249 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_hook`
--

LOCK TABLES `ps_hook` WRITE;
/*!40000 ALTER TABLE `ps_hook` DISABLE KEYS */;
INSERT INTO `ps_hook` VALUES (1,'actionValidateOrder','New orders','',1,0),(2,'displayMaintenance','Maintenance Page','This hook displays new elements on the maintenance page',1,0),(3,'displayProductPageDrawer','Product Page Drawer','This hook displays content in the right sidebar of the product page',1,0),(4,'actionPaymentConfirmation','Payment confirmation','This hook displays new elements after the payment is validated',1,0),(5,'displayPaymentReturn','Payment return','',1,0),(6,'actionUpdateQuantity','Quantity update','Quantity is updated only when a customer effectively places their order',1,0),(7,'displayRightColumn','Right column blocks','This hook displays new elements in the right-hand column',1,0),(8,'displayWrapperTop','Main wrapper section (top)','This hook displays new elements in the top of the main wrapper',1,0),(9,'displayWrapperBottom','全站 頁尾滿版','預設模組：無',1,6),(10,'displayContentWrapperTop','Content wrapper section (top)','This hook displays new elements in the top of the content wrapper',1,0),(11,'displayContentWrapperBottom','Content wrapper section (bottom)','This hook displays new elements in the bottom of the content wrapper',1,0),(12,'displayLeftColumn','分類頁、活動頁 左側欄','預設模組：分類連結、Faceted search',1,30),(13,'displayHome','首頁 主內容','預設模組：推薦分類商品區塊、圖片區塊、文字區塊',1,20),(14,'Header','Pages html head section','This hook adds additional elements in the head section of your pages (head section of html)',1,0),(15,'actionCartSave','Cart creation and update','This hook is displayed when a product is added to the cart or if the cart\'s content is modified',1,0),(16,'actionAuthentication','Successful customer authentication','This hook is displayed after a customer successfully signs in',1,0),(17,'actionProductAdd','Product creation','This hook is displayed after a product is created',1,0),(18,'actionProductUpdate','Product update','This hook is displayed after a product has been updated',1,0),(19,'displayAfterBodyOpeningTag','Very top of pages','Use this hook for advertisement or modals you want to load first',1,0),(20,'displayBeforeBodyClosingTag','Very bottom of pages','Use this hook for your modals or any content you want to load at the very end',1,0),(21,'displayTop','全站 頁首中區','預設模組：無',1,4),(22,'displayNavFullWidth','全站 頁首滿版','預設模組：無',1,5),(23,'displayRightColumnProduct','New elements on the product page (right column)','This hook displays new elements in the right-hand column of the product page',1,0),(24,'actionProductDelete','Product deletion','This hook is called when a product is deleted',1,0),(25,'actionObjectProductInCartDeleteBefore','Cart product removal','This hook is called before a product is removed from a cart',1,0),(26,'actionObjectProductInCartDeleteAfter','Cart product removal','This hook is called after a product is removed from a cart',1,0),(27,'displayFooterProduct','商品頁 頁尾','預設模組：交叉銷售、已瀏覽商品區塊',1,42),(28,'displayInvoice','Invoice','This hook displays new blocks on the invoice (order)',1,0),(29,'actionOrderStatusUpdate','Order status update - Event','This hook launches modules when the status of an order changes',1,0),(30,'displayAdminOrder','Display new elements in the Back Office, tab AdminOrder','This hook launches modules when the AdminOrder tab is displayed in the Back Office',1,0),(31,'displayAdminOrderTabOrder','Display new elements in Back Office, AdminOrder, panel Order','This hook launches modules when the AdminOrder tab is displayed in the Back Office and extends / override Order panel tabs',1,0),(32,'displayAdminOrderTabShip','Display new elements in Back Office, AdminOrder, panel Shipping','This hook launches modules when the AdminOrder tab is displayed in the Back Office and extends / override Shipping panel tabs',1,0),(33,'displayAdminOrderContentOrder','Display new elements in Back Office, AdminOrder, panel Order','This hook launches modules when the AdminOrder tab is displayed in the Back Office and extends / override Order panel content',1,0),(34,'displayAdminOrderContentShip','Display new elements in Back Office, AdminOrder, panel Shipping','This hook launches modules when the AdminOrder tab is displayed in the Back Office and extends / override Shipping panel content',1,0),(35,'displayFooter','全站 頁尾','預設模組：連結列表、聯絡資訊',1,8),(36,'displayPDFInvoice','PDF Invoice','This hook allows you to display additional information on PDF invoices',1,0),(37,'displayInvoiceLegalFreeText','PDF Invoice - Legal Free Text','This hook allows you to modify the legal free text on PDF invoices',1,0),(38,'displayAdminCustomers','Display new elements in the Back Office, tab AdminCustomers','This hook launches modules when the AdminCustomers tab is displayed in the Back Office',1,0),(39,'displayAdminCustomersAddressesItemAction','Display new elements in the Back Office, tab AdminCustomers, Add','This hook launches modules when the Addresses list into the AdminCustomers tab is displayed in the Back Office',1,0),(40,'displayOrderConfirmation','Order confirmation page','This hook is called within an order\'s confirmation page',1,0),(41,'actionCustomerAccountAdd','Successful customer account creation','This hook is called when a new customer creates an account successfully',1,0),(42,'actionCustomerAccountUpdate','Successful customer account update','This hook is called when a customer updates its account successfully',1,0),(43,'displayCustomerAccount','Customer account displayed in Front Office','This hook displays new elements on the customer account page',1,0),(44,'actionOrderSlipAdd','Order slip creation','This hook is called when a new credit slip is added regarding client order',1,0),(45,'displayShoppingCartFooter','Shopping cart footer','This hook displays some specific information on the shopping cart\'s page',1,0),(46,'displayCreateAccountEmailFormBottom','Customer authentication form','This hook displays some information on the bottom of the email form',1,0),(47,'displayAuthenticateFormBottom','Customer authentication form','This hook displays some information on the bottom of the authentication form',1,0),(48,'displayCustomerAccountForm','Customer account creation form','This hook displays some information on the form to create a customer account',1,0),(49,'displayAdminStatsModules','Stats - Modules','',1,0),(50,'displayAdminStatsGraphEngine','Graph engines','',1,0),(51,'actionOrderReturn','Returned product','This hook is displayed when a customer returns a product ',1,0),(52,'displayProductAdditionalInfo','商品頁 額外資訊','預設模組：社群分享連結',1,41),(53,'displayBackOfficeHome','Administration panel homepage','This hook is displayed on the admin panel\'s homepage',1,0),(54,'displayAdminStatsGridEngine','Grid engines','',1,0),(55,'actionWatermark','Watermark','',1,0),(56,'actionProductCancel','Product cancelled','This hook is called when you cancel a product in an order',1,0),(57,'displayLeftColumnProduct','商品頁 左側欄','預設模組：分類連結',1,40),(58,'actionProductOutOfStock','Out-of-stock product','This hook displays new action buttons if a product is out of stock',1,0),(59,'actionProductAttributeUpdate','Product attribute update','This hook is displayed when a product\'s attribute is updated',1,0),(60,'displayCarrierList','Extra carrier (module mode)','',1,0),(61,'displayShoppingCart','Shopping cart - Additional button','This hook displays new action buttons within the shopping cart',1,0),(62,'actionCarrierUpdate','Carrier Update','This hook is called when a carrier is updated',1,0),(63,'actionOrderStatusPostUpdate','Post update of order status','',1,0),(64,'displayCustomerAccountFormTop','Block above the form for create an account','This hook is displayed above the customer\'s account creation form',1,0),(65,'displayBackOfficeHeader','Administration panel header','This hook is displayed in the header of the admin panel',1,0),(66,'displayBackOfficeTop','Administration panel hover the tabs','This hook is displayed on the roll hover of the tabs within the admin panel',1,0),(67,'displayAdminEndContent','Administration end of content','This hook is displayed at the end of the main content, before the footer',1,0),(68,'displayBackOfficeFooter','Administration panel footer','This hook is displayed within the admin panel\'s footer',1,0),(69,'actionProductAttributeDelete','Product attribute deletion','This hook is displayed when a product\'s attribute is deleted',1,0),(70,'actionCarrierProcess','Carrier process','',1,0),(71,'displayBeforeCarrier','Before carriers list','This hook is displayed before the carrier list in Front Office',1,0),(72,'displayAfterCarrier','After carriers list','This hook is displayed after the carrier list in Front Office',1,0),(73,'displayOrderDetail','Order detail','This hook is displayed within the order\'s details in Front Office',1,0),(74,'actionPaymentCCAdd','Payment CC added','',1,0),(75,'actionCategoryAdd','Category creation','This hook is displayed when a category is created',1,0),(76,'actionCategoryUpdate','Category modification','This hook is displayed when a category is modified',1,0),(77,'actionCategoryDelete','Category deletion','This hook is displayed when a category is deleted',1,0),(78,'displayPaymentTop','Top of payment page','This hook is displayed at the top of the payment page',1,0),(79,'actionHtaccessCreate','After htaccess creation','This hook is displayed after the htaccess creation',1,0),(80,'actionAdminMetaSave','After saving the configuration in AdminMeta','This hook is displayed after saving the configuration in AdminMeta',1,0),(81,'displayAttributeGroupForm','Add fields to the form \'attribute group\'','This hook adds fields to the form \'attribute group\'',1,0),(82,'actionAttributeGroupSave','Saving an attribute group','This hook is called while saving an attributes group',1,0),(83,'actionAttributeGroupDelete','Deleting attribute group','This hook is called while deleting an attributes  group',1,0),(84,'displayFeatureForm','Add fields to the form \'feature\'','This hook adds fields to the form \'feature\'',1,0),(85,'actionFeatureSave','Saving attributes\' features','This hook is called while saving an attributes features',1,0),(86,'actionFeatureDelete','Deleting attributes\' features','This hook is called while deleting an attributes features',1,0),(87,'actionProductSave','Saving products','This hook is called while saving products',1,0),(88,'displayAttributeGroupPostProcess','On post-process in admin attribute group','This hook is called on post-process in admin attribute group',1,0),(89,'displayFeaturePostProcess','On post-process in admin feature','This hook is called on post-process in admin feature',1,0),(90,'displayFeatureValueForm','Add fields to the form \'feature value\'','This hook adds fields to the form \'feature value\'',1,0),(91,'displayFeatureValuePostProcess','On post-process in admin feature value','This hook is called on post-process in admin feature value',1,0),(92,'actionFeatureValueDelete','Deleting attributes\' features\' values','This hook is called while deleting an attributes features value',1,0),(93,'actionFeatureValueSave','Saving an attributes features value','This hook is called while saving an attributes features value',1,0),(94,'displayAttributeForm','Add fields to the form \'attribute value\'','This hook adds fields to the form \'attribute value\'',1,0),(95,'actionAttributePostProcess','On post-process in admin feature value','This hook is called on post-process in admin feature value',1,0),(96,'actionAttributeDelete','Deleting an attributes features value','This hook is called while deleting an attributes features value',1,0),(97,'actionAttributeSave','Saving an attributes features value','This hook is called while saving an attributes features value',1,0),(98,'actionTaxManager','Tax Manager Factory','',1,0),(99,'displayMyAccountBlock','My account block','This hook displays extra information within the \'my account\' block\"',1,0),(100,'actionModuleInstallBefore','actionModuleInstallBefore','',1,0),(101,'actionModuleInstallAfter','actionModuleInstallAfter','',1,0),(102,'displayTopColumn','Top column blocks','This hook displays new elements in the top of columns',1,0),(103,'displayBackOfficeCategory','Display new elements in the Back Office, tab AdminCategories','This hook launches modules when the AdminCategories tab is displayed in the Back Office',1,0),(104,'displayProductListFunctionalButtons','Display new elements in the Front Office, products list','This hook launches modules when the products list is displayed in the Front Office',1,0),(105,'displayNav','Navigation','',1,0),(106,'displayOverrideTemplate','Change the default template of current controller','',1,0),(107,'actionAdminLoginControllerSetMedia','Set media on admin login page header','This hook is called after adding media to admin login page header',1,0),(108,'actionOrderEdited','Order edited','This hook is called when an order is edited',1,0),(109,'actionEmailAddBeforeContent','Add extra content before mail content','This hook is called just before fetching mail template',1,0),(110,'actionEmailAddAfterContent','Add extra content after mail content','This hook is called just after fetching mail template',1,0),(111,'sendMailAlterTemplateVars','Alter template vars on the fly','This hook is called when Mail::send() is called',1,0),(112,'displayCartExtraProductActions','Extra buttons in shopping cart','This hook adds extra buttons to the product lines, in the shopping cart',1,0),(113,'displayPaymentByBinaries','Payment form generated by binaries','This hook displays form generated by binaries during the checkout',1,0),(114,'additionalCustomerFormFields','Add fields to the Customer form','This hook returns an array of FormFields to add them to the customer registration form',1,0),(115,'addWebserviceResources','Add extra webservice resource','This hook is called when webservice resources list in webservice controller',1,0),(116,'displayCustomerLoginFormAfter','Display elements after login form','This hook displays new elements after the login form',1,0),(117,'actionClearCache','Clear smarty cache','This hook is called when smarty\'s cache is cleared',1,0),(118,'actionClearCompileCache','Clear smarty compile cache','This hook is called when smarty\'s compile cache is cleared',1,0),(119,'actionClearSf2Cache','Clear Sf2 cache','This hook is called when the Symfony cache is cleared',1,0),(120,'actionValidateCustomerAddressForm','Customer address form validation','This hook is called when a customer submit its address form',1,0),(121,'displayCarrierExtraContent','Display additional content for a carrier (e.g pickup points)','This hook calls only the module related to the carrier, in order to add options when needed',1,0),(122,'validateCustomerFormFields','Customer registration form validation','This hook is called to a module when it has sent additional fields with additionalCustomerFormFields',1,0),(123,'displayProductExtraContent','Display extra content on the product page','This hook expects ProductExtraContent instances, which will be properly displayed by the template on the product page',1,0),(124,'filterCmsContent','Filter the content page','This hook is called just before fetching content page',1,0),(125,'filterCmsCategoryContent','Filter the content page category','This hook is called just before fetching content page category',1,0),(126,'filterProductContent','Filter the content page product','This hook is called just before fetching content page product',1,0),(127,'filterCategoryContent','Filter the content page category','This hook is called just before fetching content page category',1,0),(128,'filterManufacturerContent','Filter the content page manufacturer','This hook is called just before fetching content page manufacturer',1,0),(129,'filterSupplierContent','Filter the content page supplier','This hook is called just before fetching content page supplier',1,0),(130,'filterHtmlContent','Filter HTML field before rending a page','This hook is called just before fetching a page on HTML field',1,0),(131,'displayDashboardTop','Dashboard Top','Displays the content in the dashboard\'s top area',1,0),(132,'actionUpdateLangAfter','Update \"lang\" tables','Update \"lang\" tables after adding or updating a language',1,0),(133,'actionOutputHTMLBefore','Before HTML output','This hook is used to filter the whole HTML page before it is rendered (only front)',1,0),(134,'displayAfterProductThumbs','Display extra content below product thumbs','This hook displays new elements below product images ex. additional media',1,0),(135,'actionDispatcherBefore','Before dispatch','This hook is called at the beginning of the dispatch method of the Dispatcher',1,0),(136,'actionDispatcherAfter','After dispatch','This hook is called at the end of the dispatch method of the Dispatcher',1,0),(137,'filterProductSearch','Filter search products result','This hook is called in order to allow to modify search product result',1,0),(138,'actionProductSearchAfter','Event triggered after search product completed','This hook is called after the product search. Parameters are already filter',1,0),(139,'actionEmailSendBefore','Before sending an email','This hook is used to filter the content or the metadata of an email before sending it or even prevent its sending',1,0),(140,'displayAdminProductsMainStepLeftColumnMiddle','Display new elements in back office product page, left column of','This hook launches modules when the back office product page is displayed',1,0),(141,'displayAdminProductsMainStepLeftColumnBottom','Display new elements in back office product page, left column of','This hook launches modules when the back office product page is displayed',1,0),(142,'displayAdminProductsMainStepRightColumnBottom','Display new elements in back office product page, right column o','This hook launches modules when the back office product page is displayed',1,0),(143,'displayAdminProductsQuantitiesStepBottom','Display new elements in back office product page, Quantities/Com','This hook launches modules when the back office product page is displayed',1,0),(144,'displayAdminProductsPriceStepBottom','Display new elements in back office product page, Price tab','This hook launches modules when the back office product page is displayed',1,0),(145,'displayAdminProductsOptionsStepTop','Display new elements in back office product page, Options tab','This hook launches modules when the back office product page is displayed',1,0),(146,'displayAdminProductsOptionsStepBottom','Display new elements in back office product page, Options tab','This hook launches modules when the back office product page is displayed',1,0),(147,'displayAdminProductsSeoStepBottom','Display new elements in back office product page, SEO tab','This hook launches modules when the back office product page is displayed',1,0),(148,'displayAdminProductsShippingStepBottom','Display new elements in back office product page, Shipping tab','This hook launches modules when the back office product page is displayed',1,0),(149,'displayAdminProductsCombinationBottom','Display new elements in back office product page, Combination ta','This hook launches modules when the back office product page is displayed',1,0),(150,'displayDashboardToolbarTopMenu','Display new elements in back office page with a dashboard, on to','This hook launches modules when a page with a dashboard is displayed',1,0),(151,'displayDashboardToolbarIcons','Display new elements in back office page with dashboard, on icon','This hook launches modules when the back office with dashboard is displayed',1,0),(152,'actionBuildFrontEndObject','Manage elements added to the \"prestashop\" javascript object','This hook allows you to customize the \"prestashop\" javascript object that is included in all front office pages',1,0),(153,'actionFrontControllerAfterInit','Perform actions after front office controller initialization','This hook is launched after the initialization of all front office controllers',1,0),(154,'actionAdministrationPageForm','Manage Administration Page form fields','This hook adds, update or remove fields of the Administration Page form',1,0),(155,'actionAdministrationPageFormSave','Processing Administration page form','This hook is called when the Administration Page form is processed',1,0),(156,'actionPerformancePageForm','Manage Performance Page form fields','This hook adds, update or remove fields of the Performance Page form',1,0),(157,'actionPerformancePageFormSave','Processing Performance page form','This hook is called when the Performance Page form is processed',1,0),(158,'actionMaintenancePageForm','Manage Maintenance Page form fields','This hook adds, update or remove fields of the Maintenance Page form',1,0),(159,'actionMaintenancePageFormSave','Processing Maintenance page form','This hook is called when the Maintenance Page form is processed',1,0),(160,'registerGDPRConsent','registerGDPRConsent','',1,0),(161,'dashboardZoneOne','dashboardZoneOne','',1,0),(162,'dashboardData','dashboardData','',1,0),(163,'actionObjectOrderAddAfter','actionObjectOrderAddAfter','',1,0),(164,'actionObjectCustomerAddAfter','actionObjectCustomerAddAfter','',1,0),(165,'actionObjectCustomerMessageAddAfter','actionObjectCustomerMessageAddAfter','',1,0),(166,'actionObjectCustomerThreadAddAfter','actionObjectCustomerThreadAddAfter','',1,0),(167,'actionObjectOrderReturnAddAfter','actionObjectOrderReturnAddAfter','',1,0),(168,'actionAdminControllerSetMedia','actionAdminControllerSetMedia','',1,0),(169,'dashboardZoneTwo','dashboardZoneTwo','',1,0),(170,'actionSearch','actionSearch','',1,0),(171,'actionObjectLanguageAddAfter','actionObjectLanguageAddAfter','',1,0),(172,'paymentOptions','paymentOptions','',1,0),(173,'displayNav1','全站 頁首左區','預設模組：Logo、主選單',1,2),(174,'actionAdminStoresControllerUpdate_optionsAfter','actionAdminStoresControllerUpdate_optionsAfter','',1,0),(175,'actionAdminCurrenciesControllerSaveAfter','actionAdminCurrenciesControllerSaveAfter','',1,0),(176,'actionModuleRegisterHookAfter','actionModuleRegisterHookAfter','',1,0),(177,'actionModuleUnRegisterHookAfter','actionModuleUnRegisterHookAfter','',1,0),(178,'actionShopDataDuplication','actionShopDataDuplication','',1,0),(179,'displayFooterBefore','全站 頁尾上方','預設模組：訂閱電子報、官方社群連結',1,7),(180,'displayAdminCustomersForm','displayAdminCustomersForm','',1,0),(181,'actionDeleteGDPRCustomer','actionDeleteGDPRCustomer','',1,0),(182,'actionExportGDPRData','actionExportGDPRData','',1,0),(183,'productSearchProvider','productSearchProvider','',1,0),(184,'displayOrderConfirmation2','displayOrderConfirmation2','',1,0),(185,'displayCrossSellingShoppingCart','displayCrossSellingShoppingCart','',1,0),(186,'actionAdminGroupsControllerSaveAfter','actionAdminGroupsControllerSaveAfter','',1,0),(187,'actionObjectCategoryUpdateAfter','actionObjectCategoryUpdateAfter','',1,0),(188,'actionObjectCategoryDeleteAfter','actionObjectCategoryDeleteAfter','',1,0),(189,'actionObjectCategoryAddAfter','actionObjectCategoryAddAfter','',1,0),(190,'actionObjectCmsUpdateAfter','actionObjectCmsUpdateAfter','',1,0),(191,'actionObjectCmsDeleteAfter','actionObjectCmsDeleteAfter','',1,0),(192,'actionObjectCmsAddAfter','actionObjectCmsAddAfter','',1,0),(193,'actionObjectSupplierUpdateAfter','actionObjectSupplierUpdateAfter','',1,0),(194,'actionObjectSupplierDeleteAfter','actionObjectSupplierDeleteAfter','',1,0),(195,'actionObjectSupplierAddAfter','actionObjectSupplierAddAfter','',1,0),(196,'actionObjectManufacturerUpdateAfter','actionObjectManufacturerUpdateAfter','',1,0),(197,'actionObjectManufacturerDeleteAfter','actionObjectManufacturerDeleteAfter','',1,0),(198,'actionObjectManufacturerAddAfter','actionObjectManufacturerAddAfter','',1,0),(199,'actionObjectProductUpdateAfter','actionObjectProductUpdateAfter','',1,0),(200,'actionObjectProductDeleteAfter','actionObjectProductDeleteAfter','',1,0),(201,'actionObjectProductAddAfter','actionObjectProductAddAfter','',1,0),(202,'displaySearch','displaySearch','',1,0),(203,'displayAdminNavBarBeforeEnd','displayAdminNavBarBeforeEnd','',1,0),(204,'displayAdminAfterHeader','displayAdminAfterHeader','',1,0),(205,'displayNav2','全站 頁首右區','預設模組：搜索欄、語言選擇區塊、貨幣區塊、登入連結、購物車',1,3),(206,'displayReassurance','','',1,0),(207,'actionAdminMetaControllerUpdate_optionsAfter','actionAdminMetaControllerUpdate_optionsAfter','',1,0),(208,'actionAdminPerformanceControllerSaveAfter','actionAdminPerformanceControllerSaveAfter','',1,0),(209,'actionObjectCarrierAddAfter','actionObjectCarrierAddAfter','',1,0),(210,'actionObjectContactAddAfter','actionObjectContactAddAfter','',1,0),(211,'actionAdminThemesControllerUpdate_optionsAfter','actionAdminThemesControllerUpdate_optionsAfter','',1,0),(212,'actionObjectShopUpdateAfter','actionObjectShopUpdateAfter','',1,0),(213,'actionAdminPreferencesControllerUpdate_optionsAfter','actionAdminPreferencesControllerUpdate_optionsAfter','',1,0),(214,'actionObjectShopAddAfter','actionObjectShopAddAfter','',1,0),(215,'actionObjectShopGroupAddAfter','actionObjectShopGroupAddAfter','',1,0),(216,'actionObjectCartAddAfter','actionObjectCartAddAfter','',1,0),(217,'actionObjectEmployeeAddAfter','actionObjectEmployeeAddAfter','',1,0),(218,'actionObjectImageAddAfter','actionObjectImageAddAfter','',1,0),(219,'actionObjectCartRuleAddAfter','actionObjectCartRuleAddAfter','',1,0),(220,'actionAdminStoresControllerSaveAfter','actionAdminStoresControllerSaveAfter','',1,0),(221,'actionAdminWebserviceControllerSaveAfter','actionAdminWebserviceControllerSaveAfter','',1,0),(222,'actionObjectSpecificPriceCoreDeleteAfter','actionObjectSpecificPriceCoreDeleteAfter','',1,0),(223,'actionObjectSpecificPriceCoreAddAfter','actionObjectSpecificPriceCoreAddAfter','',1,0),(224,'actionObjectSpecificPriceCoreUpdateAfter','actionObjectSpecificPriceCoreUpdateAfter','',1,0),(225,'actionProductCoverage','actionProductCoverage','',1,0),(226,'displayPaymentEu','displayPaymentEu','Hook to display payment options',1,0),(227,'displayProductPriceBlock','displayProductPriceBlock','',1,0),(228,'displayCheckoutSubtotalDetails','displayCheckoutSubtotalDetails','',1,0),(229,'displayFooterAfter','displayFooterAfter','',1,0),(230,'advancedPaymentOptions','advancedPaymentOptions','',1,0),(231,'displayCartTotalPriceLabel','displayCartTotalPriceLabel','',1,0),(232,'displayCMSPrintButton','displayCMSPrintButton','',1,0),(233,'displayCMSDisputeInformation','displayCMSDisputeInformation','',1,0),(234,'termsAndConditions','termsAndConditions','',1,0),(235,'displayCheckoutSummaryTop','displayCheckoutSummaryTop','',1,0),(236,'actionFrontControllerSetMedia','actionFrontControllerSetMedia','',1,0),(237,'actionAjaxDieProductControllerdisplayAjaxQuickviewBefore','actionAjaxDieProductControllerdisplayAjaxQuickviewBefore','',1,0),(238,'displayBanner','全站 頁首頂端','預設模組：頁首置頂區塊、圖片輪播',1,1),(239,'displayCheckoutStepOneNotLogged','結帳頁第一步未登入情況','',1,0),(240,'displayOrderConfirmation1','displayOrderConfirmation1','',1,0),(241,'displayCheckoutButtonBefore','displayCheckoutButtonBefore','',1,0),(242,'gSitemapAppendUrls','GSitemap Append URLs','This hook allows a module to add URLs to a generated sitemap',1,0),(243,'actionObjectCurrencyAddAfter','actionObjectCurrencyAddAfter','',1,0),(244,'actionCartUpdateQuantityBefore','actionCartUpdateQuantityBefore','',1,0),(245,'displayCustomerLoginLink','頁首登入按鈕','',1,0),(246,'displayLeftColumnContact','聯絡我們 左側欄','',1,20),(247,'actionPsAdminCustoConfigurationControllerUpdateModuleAfter','actionPsAdminCustoConfigurationControllerUpdateModuleAfter','',1,0),(248,'actionAdminModulesPositionsControllerEditGraftAfter','actionAdminModulesPositionsControllerEditGraftAfter','',1,0);
/*!40000 ALTER TABLE `ps_hook` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_hook_alias`
--

DROP TABLE IF EXISTS `ps_hook_alias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_hook_alias` (
  `id_hook_alias` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `alias` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id_hook_alias`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_hook_alias`
--

LOCK TABLES `ps_hook_alias` WRITE;
/*!40000 ALTER TABLE `ps_hook_alias` DISABLE KEYS */;
INSERT INTO `ps_hook_alias` VALUES (1,'newOrder','actionValidateOrder'),(2,'paymentConfirm','actionPaymentConfirmation'),(3,'paymentReturn','displayPaymentReturn'),(4,'updateQuantity','actionUpdateQuantity'),(5,'rightColumn','displayRightColumn'),(6,'leftColumn','displayLeftColumn'),(7,'home','displayHome'),(8,'displayHeader','Header'),(9,'cart','actionCartSave'),(10,'authentication','actionAuthentication'),(11,'addproduct','actionProductAdd'),(12,'updateproduct','actionProductUpdate'),(13,'top','displayTop'),(14,'extraRight','displayRightColumnProduct'),(15,'deleteproduct','actionProductDelete'),(16,'productfooter','displayFooterProduct'),(17,'invoice','displayInvoice'),(18,'updateOrderStatus','actionOrderStatusUpdate'),(19,'adminOrder','displayAdminOrder'),(20,'footer','displayFooter'),(21,'PDFInvoice','displayPDFInvoice'),(22,'adminCustomers','displayAdminCustomers'),(23,'orderConfirmation','displayOrderConfirmation'),(24,'createAccount','actionCustomerAccountAdd'),(25,'customerAccount','displayCustomerAccount'),(26,'orderSlip','actionOrderSlipAdd'),(27,'shoppingCart','displayShoppingCartFooter'),(28,'createAccountForm','displayCustomerAccountForm'),(29,'AdminStatsModules','displayAdminStatsModules'),(30,'GraphEngine','displayAdminStatsGraphEngine'),(31,'orderReturn','actionOrderReturn'),(32,'productActions','displayProductAdditionalInfo'),(33,'displayProductButtons','displayProductAdditionalInfo'),(34,'backOfficeHome','displayBackOfficeHome'),(35,'GridEngine','displayAdminStatsGridEngine'),(36,'watermark','actionWatermark'),(37,'cancelProduct','actionProductCancel'),(38,'extraLeft','displayLeftColumnProduct'),(39,'productOutOfStock','actionProductOutOfStock'),(40,'updateProductAttribute','actionProductAttributeUpdate'),(41,'extraCarrier','displayCarrierList'),(42,'shoppingCartExtra','displayShoppingCart'),(43,'updateCarrier','actionCarrierUpdate'),(44,'postUpdateOrderStatus','actionOrderStatusPostUpdate'),(45,'createAccountTop','displayCustomerAccountFormTop'),(46,'backOfficeHeader','displayBackOfficeHeader'),(47,'backOfficeTop','displayBackOfficeTop'),(48,'backOfficeFooter','displayBackOfficeFooter'),(49,'deleteProductAttribute','actionProductAttributeDelete'),(50,'processCarrier','actionCarrierProcess'),(51,'beforeCarrier','displayBeforeCarrier'),(52,'orderDetailDisplayed','displayOrderDetail'),(53,'paymentCCAdded','actionPaymentCCAdd'),(54,'categoryAddition','actionCategoryAdd'),(55,'categoryUpdate','actionCategoryUpdate'),(56,'categoryDeletion','actionCategoryDelete'),(57,'paymentTop','displayPaymentTop'),(58,'afterCreateHtaccess','actionHtaccessCreate'),(59,'afterSaveAdminMeta','actionAdminMetaSave'),(60,'attributeGroupForm','displayAttributeGroupForm'),(61,'afterSaveAttributeGroup','actionAttributeGroupSave'),(62,'afterDeleteAttributeGroup','actionAttributeGroupDelete'),(63,'featureForm','displayFeatureForm'),(64,'afterSaveFeature','actionFeatureSave'),(65,'afterDeleteFeature','actionFeatureDelete'),(66,'afterSaveProduct','actionProductSave'),(67,'postProcessAttributeGroup','displayAttributeGroupPostProcess'),(68,'postProcessFeature','displayFeaturePostProcess'),(69,'featureValueForm','displayFeatureValueForm'),(70,'postProcessFeatureValue','displayFeatureValuePostProcess'),(71,'afterDeleteFeatureValue','actionFeatureValueDelete'),(72,'afterSaveFeatureValue','actionFeatureValueSave'),(73,'attributeForm','displayAttributeForm'),(74,'postProcessAttribute','actionAttributePostProcess'),(75,'afterDeleteAttribute','actionAttributeDelete'),(76,'afterSaveAttribute','actionAttributeSave'),(77,'taxManager','actionTaxManager'),(78,'myAccountBlock','displayMyAccountBlock'),(79,'actionBeforeCartUpdateQty','actionCartUpdateQuantityBefore'),(80,'actionBeforeAjaxDie','actionAjaxDieBefore'),(81,'actionBeforeAuthentication','actionAuthenticationBefore'),(82,'actionBeforeSubmitAccount','actionSubmitAccountBefore'),(83,'actionAfterDeleteProductInCart','actionDeleteProductInCartAfter');
/*!40000 ALTER TABLE `ps_hook_alias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_hook_module`
--

DROP TABLE IF EXISTS `ps_hook_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_hook_module` (
  `id_module` int(10) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_hook` int(10) unsigned NOT NULL,
  `position` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`id_module`,`id_hook`,`id_shop`),
  KEY `id_hook` (`id_hook`),
  KEY `id_module` (`id_module`),
  KEY `position` (`id_shop`,`position`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_hook_module`
--

LOCK TABLES `ps_hook_module` WRITE;
/*!40000 ALTER TABLE `ps_hook_module` DISABLE KEYS */;
INSERT INTO `ps_hook_module` VALUES (1,1,160,1),(2,1,161,1),(2,1,162,1),(2,1,163,1),(2,1,164,1),(2,1,165,1),(2,1,166,1),(2,1,167,1),(2,1,168,1),(3,1,63,1),(3,1,169,1),(5,1,170,1),(6,1,50,1),(7,1,54,1),(8,1,171,1),(9,1,12,1),(9,1,57,1),(11,1,174,1),(11,1,246,1),(12,1,175,1),(15,1,178,1),(16,1,41,1),(16,1,114,1),(16,1,179,1),(16,1,181,1),(16,1,182,1),(17,1,75,1),(17,1,76,1),(17,1,77,1),(17,1,82,1),(17,1,83,1),(17,1,85,1),(17,1,86,1),(17,1,87,1),(17,1,92,1),(17,1,93,1),(17,1,95,1),(17,1,96,1),(17,1,97,1),(17,1,183,1),(18,1,13,1),(18,1,17,1),(18,1,18,1),(18,1,24,1),(18,1,186,1),(19,1,14,1),(19,1,22,1),(21,1,35,1),(21,1,132,1),(22,1,187,1),(22,1,188,1),(22,1,189,1),(22,1,190,1),(22,1,191,1),(22,1,192,1),(22,1,193,1),(22,1,194,1),(22,1,195,1),(22,1,196,1),(22,1,197,1),(22,1,198,1),(22,1,199,1),(22,1,200,1),(22,1,201,1),(23,1,202,1),(23,1,205,1),(24,1,52,1),(26,1,247,1),(26,1,248,1),(28,1,5,1),(32,1,49,1),(39,1,16,1),(39,1,20,1),(58,1,222,1),(58,1,223,1),(58,1,224,1),(61,1,27,1),(68,1,1,1),(68,1,6,1),(68,1,43,1),(68,1,51,1),(68,1,58,1),(68,1,59,1),(68,1,69,1),(68,1,99,1),(68,1,108,1),(68,1,225,1),(69,1,15,1),(69,1,30,1),(69,1,40,1),(69,1,56,1),(69,1,65,1),(69,1,70,1),(72,1,7,1),(78,1,78,1),(78,1,236,1),(78,1,237,1),(79,1,173,1),(82,1,239,1),(82,1,245,1),(83,1,31,1),(83,1,33,1),(84,1,121,1),(91,1,73,1),(95,1,241,1),(96,1,238,1),(98,1,29,1),(98,1,37,1),(98,1,45,1),(98,1,243,1),(98,1,244,1),(3,1,162,2),(3,1,168,2),(4,1,169,2),(5,1,163,2),(8,1,13,2),(16,1,160,2),(17,1,12,2),(18,1,76,2),(19,1,178,2),(19,1,238,2),(20,1,205,2),(21,1,45,2),(22,1,173,2),(23,1,14,2),(26,1,179,2),(28,1,172,2),(33,1,49,2),(39,1,41,2),(57,1,17,2),(57,1,18,2),(57,1,24,2),(57,1,63,2),(59,1,5,2),(59,1,172,2),(68,1,181,2),(68,1,182,2),(69,1,27,2),(71,1,199,2),(71,1,200,2),(72,1,196,2),(72,1,197,2),(72,1,198,2),(73,1,7,2),(73,1,193,2),(73,1,194,2),(73,1,195,2),(78,1,40,2),(80,1,176,2),(80,1,177,2),(81,1,132,2),(84,1,31,2),(84,1,33,2),(84,1,62,2),(84,1,70,2),(84,1,236,2),(85,1,1,2),(85,1,121,2),(98,1,30,2),(98,1,43,2),(98,1,65,2),(98,1,99,2),(100,1,20,2),(100,1,29,2),(4,1,162,3),(4,1,168,3),(5,1,169,3),(12,1,205,3),(15,1,13,3),(22,1,76,3),(22,1,178,3),(25,1,14,3),(41,1,49,3),(58,1,17,3),(58,1,18,3),(58,1,24,3),(61,1,63,3),(68,1,160,3),(71,1,27,3),(84,1,40,3),(85,1,31,3),(85,1,33,3),(85,1,62,3),(85,1,70,3),(85,1,236,3),(86,1,121,3),(86,1,172,3),(87,1,5,3),(95,1,1,3),(95,1,63,3),(100,1,65,3),(5,1,162,4),(6,1,168,4),(14,1,205,4),(42,1,49,4),(62,1,17,4),(62,1,18,4),(62,1,24,4),(68,1,14,4),(86,1,62,4),(86,1,70,4),(86,1,236,4),(87,1,172,4),(88,1,5,4),(89,1,31,4),(89,1,33,4),(89,1,40,4),(89,1,121,4),(98,1,27,4),(98,1,63,4),(25,1,205,5),(43,1,49,5),(68,1,24,5),(69,1,14,5),(85,1,40,5),(88,1,172,5),(89,1,62,5),(89,1,70,5),(90,1,31,5),(90,1,33,5),(90,1,121,5),(91,1,5,5),(98,1,168,5),(48,1,49,6),(90,1,62,6),(90,1,70,6),(90,1,172,6),(91,1,31,6),(91,1,33,6),(93,1,5,6),(98,1,40,6),(51,1,49,7),(78,1,14,7),(90,1,5,7),(91,1,172,7),(100,1,31,7),(100,1,33,7),(100,1,40,7),(82,1,14,8),(86,1,5,8),(92,1,172,8),(92,1,5,9),(93,1,172,9),(98,1,14,9),(98,1,5,10),(98,1,172,10),(100,1,14,10),(1,2,160,1),(2,2,161,1),(2,2,162,1),(2,2,163,1),(2,2,164,1),(2,2,165,1),(2,2,166,1),(2,2,167,1),(2,2,168,1),(3,2,63,1),(3,2,169,1),(5,2,170,1),(6,2,50,1),(7,2,54,1),(8,2,171,1),(9,2,12,1),(9,2,57,1),(11,2,174,1),(11,2,246,1),(12,2,175,1),(15,2,178,1),(16,2,41,1),(16,2,114,1),(16,2,179,1),(16,2,181,1),(16,2,182,1),(17,2,75,1),(17,2,76,1),(17,2,77,1),(17,2,82,1),(17,2,83,1),(17,2,85,1),(17,2,86,1),(17,2,87,1),(17,2,92,1),(17,2,93,1),(17,2,95,1),(17,2,96,1),(17,2,97,1),(17,2,183,1),(18,2,13,1),(18,2,17,1),(18,2,18,1),(18,2,24,1),(18,2,186,1),(19,2,14,1),(21,2,35,1),(21,2,132,1),(22,2,187,1),(22,2,188,1),(22,2,189,1),(22,2,190,1),(22,2,191,1),(22,2,192,1),(22,2,193,1),(22,2,194,1),(22,2,195,1),(22,2,196,1),(22,2,197,1),(22,2,198,1),(22,2,199,1),(22,2,200,1),(22,2,201,1),(23,2,202,1),(23,2,205,1),(24,2,52,1),(26,2,247,1),(26,2,248,1),(28,2,5,1),(39,2,16,1),(39,2,20,1),(57,2,13,1),(58,2,13,1),(58,2,222,1),(58,2,223,1),(58,2,224,1),(61,2,27,1),(62,2,13,1),(68,2,1,1),(68,2,6,1),(68,2,43,1),(68,2,51,1),(68,2,58,1),(68,2,59,1),(68,2,69,1),(68,2,99,1),(68,2,108,1),(68,2,225,1),(69,2,15,1),(69,2,30,1),(69,2,40,1),(69,2,56,1),(69,2,65,1),(69,2,70,1),(72,2,7,1),(78,2,78,1),(78,2,236,1),(78,2,237,1),(79,2,173,1),(82,2,239,1),(82,2,245,1),(83,2,31,1),(83,2,33,1),(84,2,121,1),(95,2,241,1),(96,2,238,1),(98,2,29,1),(98,2,37,1),(98,2,45,1),(98,2,206,1),(98,2,243,1),(98,2,244,1),(3,2,162,2),(3,2,168,2),(4,2,169,2),(5,2,163,2),(8,2,13,2),(16,2,160,2),(17,2,12,2),(18,2,76,2),(19,2,178,2),(19,2,238,2),(20,2,205,2),(21,2,45,2),(22,2,173,2),(23,2,14,2),(26,2,179,2),(28,2,172,2),(32,2,49,2),(39,2,41,2),(57,2,17,2),(57,2,18,2),(57,2,24,2),(57,2,63,2),(59,2,5,2),(59,2,172,2),(68,2,181,2),(68,2,182,2),(69,2,27,2),(71,2,52,2),(71,2,199,2),(71,2,200,2),(72,2,196,2),(72,2,197,2),(72,2,198,2),(73,2,7,2),(73,2,193,2),(73,2,194,2),(73,2,195,2),(78,2,40,2),(80,2,176,2),(80,2,177,2),(81,2,132,2),(84,2,31,2),(84,2,33,2),(84,2,62,2),(84,2,70,2),(84,2,236,2),(85,2,1,2),(85,2,121,2),(91,2,73,2),(98,2,30,2),(98,2,43,2),(98,2,65,2),(98,2,99,2),(100,2,20,2),(100,2,29,2),(4,2,162,3),(4,2,168,3),(5,2,169,3),(12,2,205,3),(15,2,13,3),(22,2,76,3),(22,2,178,3),(25,2,14,3),(33,2,49,3),(58,2,17,3),(58,2,18,3),(58,2,24,3),(61,2,63,3),(68,2,160,3),(71,2,27,3),(84,2,40,3),(85,2,31,3),(85,2,33,3),(85,2,62,3),(85,2,70,3),(85,2,236,3),(86,2,121,3),(86,2,172,3),(87,2,5,3),(95,2,1,3),(95,2,63,3),(100,2,65,3),(5,2,162,4),(6,2,168,4),(14,2,205,4),(62,2,17,4),(62,2,18,4),(62,2,24,4),(68,2,14,4),(86,2,62,4),(86,2,70,4),(86,2,236,4),(87,2,172,4),(88,2,5,4),(89,2,31,4),(89,2,33,4),(89,2,40,4),(89,2,121,4),(94,2,13,4),(98,2,27,4),(98,2,63,4),(25,2,205,5),(68,2,24,5),(69,2,14,5),(85,2,40,5),(88,2,172,5),(89,2,62,5),(89,2,70,5),(90,2,31,5),(90,2,33,5),(90,2,121,5),(91,2,5,5),(98,2,168,5),(41,2,49,6),(90,2,62,6),(90,2,70,6),(90,2,172,6),(91,2,31,6),(91,2,33,6),(93,2,5,6),(98,2,40,6),(42,2,49,7),(78,2,14,7),(90,2,5,7),(91,2,172,7),(100,2,31,7),(100,2,33,7),(100,2,40,7),(43,2,49,8),(82,2,14,8),(86,2,5,8),(92,2,172,8),(48,2,49,9),(92,2,5,9),(93,2,172,9),(98,2,14,9),(98,2,5,10),(98,2,172,10),(100,2,14,10),(51,2,49,11),(1,3,160,1),(2,3,161,1),(2,3,162,1),(2,3,163,1),(2,3,164,1),(2,3,165,1),(2,3,166,1),(2,3,167,1),(2,3,168,1),(3,3,63,1),(3,3,169,1),(5,3,170,1),(6,3,50,1),(7,3,54,1),(8,3,171,1),(9,3,12,1),(9,3,57,1),(11,3,174,1),(11,3,246,1),(12,3,175,1),(15,3,178,1),(16,3,41,1),(16,3,114,1),(16,3,179,1),(16,3,181,1),(16,3,182,1),(17,3,75,1),(17,3,76,1),(17,3,77,1),(17,3,82,1),(17,3,83,1),(17,3,85,1),(17,3,86,1),(17,3,87,1),(17,3,92,1),(17,3,93,1),(17,3,95,1),(17,3,96,1),(17,3,97,1),(17,3,183,1),(18,3,13,1),(18,3,17,1),(18,3,18,1),(18,3,24,1),(18,3,186,1),(19,3,14,1),(21,3,35,1),(21,3,132,1),(22,3,187,1),(22,3,188,1),(22,3,189,1),(22,3,190,1),(22,3,191,1),(22,3,192,1),(22,3,193,1),(22,3,194,1),(22,3,195,1),(22,3,196,1),(22,3,197,1),(22,3,198,1),(22,3,199,1),(22,3,200,1),(22,3,201,1),(23,3,202,1),(23,3,205,1),(24,3,52,1),(26,3,247,1),(26,3,248,1),(28,3,5,1),(39,3,16,1),(39,3,20,1),(57,3,13,1),(58,3,13,1),(58,3,222,1),(58,3,223,1),(58,3,224,1),(61,3,27,1),(62,3,13,1),(68,3,1,1),(68,3,6,1),(68,3,43,1),(68,3,51,1),(68,3,58,1),(68,3,59,1),(68,3,69,1),(68,3,99,1),(68,3,108,1),(68,3,225,1),(69,3,15,1),(69,3,30,1),(69,3,40,1),(69,3,56,1),(69,3,65,1),(69,3,70,1),(72,3,7,1),(78,3,78,1),(78,3,236,1),(78,3,237,1),(79,3,173,1),(82,3,239,1),(82,3,245,1),(83,3,31,1),(83,3,33,1),(84,3,121,1),(95,3,241,1),(96,3,238,1),(98,3,29,1),(98,3,37,1),(98,3,45,1),(98,3,206,1),(98,3,243,1),(98,3,244,1),(3,3,162,2),(3,3,168,2),(4,3,169,2),(5,3,163,2),(8,3,13,2),(16,3,160,2),(17,3,12,2),(18,3,76,2),(19,3,178,2),(19,3,238,2),(20,3,205,2),(21,3,45,2),(22,3,173,2),(23,3,14,2),(26,3,179,2),(28,3,172,2),(32,3,49,2),(39,3,41,2),(57,3,17,2),(57,3,18,2),(57,3,24,2),(57,3,63,2),(59,3,5,2),(59,3,172,2),(68,3,181,2),(68,3,182,2),(69,3,27,2),(71,3,52,2),(71,3,199,2),(71,3,200,2),(72,3,196,2),(72,3,197,2),(72,3,198,2),(73,3,7,2),(73,3,193,2),(73,3,194,2),(73,3,195,2),(78,3,40,2),(80,3,176,2),(80,3,177,2),(81,3,132,2),(84,3,31,2),(84,3,33,2),(84,3,62,2),(84,3,70,2),(84,3,236,2),(85,3,1,2),(85,3,121,2),(91,3,73,2),(98,3,30,2),(98,3,43,2),(98,3,65,2),(98,3,99,2),(100,3,20,2),(100,3,29,2),(4,3,162,3),(4,3,168,3),(5,3,169,3),(12,3,205,3),(15,3,13,3),(22,3,76,3),(22,3,178,3),(25,3,14,3),(33,3,49,3),(58,3,17,3),(58,3,18,3),(58,3,24,3),(61,3,63,3),(68,3,160,3),(71,3,27,3),(84,3,40,3),(85,3,31,3),(85,3,33,3),(85,3,62,3),(85,3,70,3),(85,3,236,3),(86,3,121,3),(86,3,172,3),(87,3,5,3),(95,3,1,3),(95,3,63,3),(100,3,65,3),(5,3,162,4),(6,3,168,4),(14,3,205,4),(62,3,17,4),(62,3,18,4),(62,3,24,4),(68,3,14,4),(86,3,62,4),(86,3,70,4),(86,3,236,4),(87,3,172,4),(88,3,5,4),(89,3,31,4),(89,3,33,4),(89,3,40,4),(89,3,121,4),(94,3,13,4),(98,3,27,4),(98,3,63,4),(25,3,205,5),(68,3,24,5),(69,3,14,5),(85,3,40,5),(88,3,172,5),(89,3,62,5),(89,3,70,5),(90,3,31,5),(90,3,33,5),(90,3,121,5),(91,3,5,5),(98,3,168,5),(41,3,49,6),(90,3,62,6),(90,3,70,6),(90,3,172,6),(91,3,31,6),(91,3,33,6),(93,3,5,6),(98,3,40,6),(42,3,49,7),(78,3,14,7),(90,3,5,7),(91,3,172,7),(100,3,31,7),(100,3,33,7),(100,3,40,7),(43,3,49,8),(82,3,14,8),(86,3,5,8),(92,3,172,8),(48,3,49,9),(92,3,5,9),(93,3,172,9),(98,3,14,9),(98,3,5,10),(98,3,172,10),(100,3,14,10),(51,3,49,11),(1,4,160,1),(2,4,161,1),(2,4,162,1),(2,4,163,1),(2,4,164,1),(2,4,165,1),(2,4,166,1),(2,4,167,1),(2,4,168,1),(3,4,63,1),(3,4,169,1),(5,4,170,1),(6,4,50,1),(7,4,54,1),(8,4,171,1),(9,4,12,1),(9,4,57,1),(11,4,174,1),(11,4,246,1),(12,4,175,1),(15,4,178,1),(16,4,41,1),(16,4,114,1),(16,4,179,1),(16,4,181,1),(16,4,182,1),(17,4,75,1),(17,4,76,1),(17,4,77,1),(17,4,82,1),(17,4,83,1),(17,4,85,1),(17,4,86,1),(17,4,87,1),(17,4,92,1),(17,4,93,1),(17,4,95,1),(17,4,96,1),(17,4,97,1),(17,4,183,1),(18,4,13,1),(18,4,17,1),(18,4,18,1),(18,4,24,1),(18,4,186,1),(19,4,14,1),(21,4,35,1),(21,4,132,1),(22,4,187,1),(22,4,188,1),(22,4,189,1),(22,4,190,1),(22,4,191,1),(22,4,192,1),(22,4,193,1),(22,4,194,1),(22,4,195,1),(22,4,196,1),(22,4,197,1),(22,4,198,1),(22,4,199,1),(22,4,200,1),(22,4,201,1),(23,4,202,1),(23,4,205,1),(24,4,52,1),(26,4,247,1),(26,4,248,1),(28,4,5,1),(39,4,16,1),(39,4,20,1),(57,4,13,1),(58,4,13,1),(58,4,222,1),(58,4,223,1),(58,4,224,1),(61,4,27,1),(62,4,13,1),(68,4,1,1),(68,4,6,1),(68,4,43,1),(68,4,51,1),(68,4,58,1),(68,4,59,1),(68,4,69,1),(68,4,99,1),(68,4,108,1),(68,4,225,1),(69,4,15,1),(69,4,30,1),(69,4,40,1),(69,4,56,1),(69,4,65,1),(69,4,70,1),(72,4,7,1),(78,4,78,1),(78,4,236,1),(78,4,237,1),(79,4,173,1),(82,4,239,1),(82,4,245,1),(83,4,31,1),(83,4,33,1),(84,4,121,1),(95,4,241,1),(96,4,238,1),(98,4,29,1),(98,4,37,1),(98,4,45,1),(98,4,206,1),(98,4,243,1),(98,4,244,1),(3,4,162,2),(3,4,168,2),(4,4,169,2),(5,4,163,2),(8,4,13,2),(16,4,160,2),(17,4,12,2),(18,4,76,2),(19,4,178,2),(19,4,238,2),(20,4,205,2),(21,4,45,2),(22,4,173,2),(23,4,14,2),(26,4,179,2),(28,4,172,2),(32,4,49,2),(39,4,41,2),(57,4,17,2),(57,4,18,2),(57,4,24,2),(57,4,63,2),(59,4,5,2),(59,4,172,2),(68,4,181,2),(68,4,182,2),(69,4,27,2),(71,4,52,2),(71,4,199,2),(71,4,200,2),(72,4,196,2),(72,4,197,2),(72,4,198,2),(73,4,7,2),(73,4,193,2),(73,4,194,2),(73,4,195,2),(78,4,40,2),(80,4,176,2),(80,4,177,2),(81,4,132,2),(84,4,31,2),(84,4,33,2),(84,4,62,2),(84,4,70,2),(84,4,236,2),(85,4,1,2),(85,4,121,2),(91,4,73,2),(98,4,30,2),(98,4,43,2),(98,4,65,2),(98,4,99,2),(100,4,20,2),(100,4,29,2),(4,4,162,3),(4,4,168,3),(5,4,169,3),(12,4,205,3),(15,4,13,3),(22,4,76,3),(22,4,178,3),(25,4,14,3),(33,4,49,3),(58,4,17,3),(58,4,18,3),(58,4,24,3),(61,4,63,3),(68,4,160,3),(71,4,27,3),(84,4,40,3),(85,4,31,3),(85,4,33,3),(85,4,62,3),(85,4,70,3),(85,4,236,3),(86,4,121,3),(86,4,172,3),(87,4,5,3),(95,4,1,3),(95,4,63,3),(100,4,65,3),(5,4,162,4),(6,4,168,4),(14,4,205,4),(62,4,17,4),(62,4,18,4),(62,4,24,4),(68,4,14,4),(86,4,62,4),(86,4,70,4),(86,4,236,4),(87,4,172,4),(88,4,5,4),(89,4,31,4),(89,4,33,4),(89,4,40,4),(89,4,121,4),(94,4,13,4),(98,4,27,4),(98,4,63,4),(25,4,205,5),(68,4,24,5),(69,4,14,5),(85,4,40,5),(88,4,172,5),(89,4,62,5),(89,4,70,5),(90,4,31,5),(90,4,33,5),(90,4,121,5),(91,4,5,5),(98,4,168,5),(41,4,49,6),(90,4,62,6),(90,4,70,6),(90,4,172,6),(91,4,31,6),(91,4,33,6),(93,4,5,6),(98,4,40,6),(42,4,49,7),(78,4,14,7),(90,4,5,7),(91,4,172,7),(100,4,31,7),(100,4,33,7),(100,4,40,7),(43,4,49,8),(82,4,14,8),(86,4,5,8),(92,4,172,8),(48,4,49,9),(92,4,5,9),(93,4,172,9),(98,4,14,9),(98,4,5,10),(98,4,172,10),(100,4,14,10),(51,4,49,11);
/*!40000 ALTER TABLE `ps_hook_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_hook_module_exceptions`
--

DROP TABLE IF EXISTS `ps_hook_module_exceptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_hook_module_exceptions` (
  `id_hook_module_exceptions` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_module` int(10) unsigned NOT NULL,
  `id_hook` int(10) unsigned NOT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_hook_module_exceptions`),
  KEY `id_module` (`id_module`),
  KEY `id_hook` (`id_hook`)
) ENGINE=InnoDB AUTO_INCREMENT=778 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_hook_module_exceptions`
--

LOCK TABLES `ps_hook_module_exceptions` WRITE;
/*!40000 ALTER TABLE `ps_hook_module_exceptions` DISABLE KEYS */;
INSERT INTO `ps_hook_module_exceptions` VALUES (355,2,19,238,'address'),(356,2,19,238,'addresses'),(357,2,19,238,'attachment'),(358,2,19,238,'auth'),(359,2,19,238,'bestsales'),(360,2,19,238,'cart'),(361,2,19,238,'category'),(362,2,19,238,'cms'),(363,2,19,238,'contact'),(364,2,19,238,'discount'),(365,2,19,238,'getfile'),(366,2,19,238,'guesttracking'),(367,2,19,238,'history'),(368,2,19,238,'identity'),(369,2,19,238,'myaccount'),(370,2,19,238,'newproducts'),(371,2,19,238,'order'),(372,2,19,238,'orderconfirmation'),(373,2,19,238,'orderdetail'),(374,2,19,238,'orderfollow'),(375,2,19,238,'orderreturn'),(376,2,19,238,'orderslip'),(377,2,19,238,'pagenotfound'),(378,2,19,238,'pdfinvoice'),(379,2,19,238,'pdforderreturn'),(380,2,19,238,'pdforderslip'),(381,2,19,238,'pricesdrop'),(382,2,19,238,'product'),(383,2,19,238,'search'),(384,2,16,179,'address'),(385,2,16,179,'addresses'),(386,2,16,179,'attachment'),(387,2,16,179,'auth'),(388,2,16,179,'bestsales'),(389,2,16,179,'cart'),(390,2,16,179,'category'),(391,2,16,179,'cms'),(392,2,16,179,'contact'),(393,2,16,179,'discount'),(394,2,16,179,'getfile'),(395,2,16,179,'guesttracking'),(396,2,16,179,'history'),(397,2,16,179,'identity'),(398,2,16,179,'myaccount'),(399,2,16,179,'newproducts'),(400,2,16,179,'order'),(401,2,16,179,'orderconfirmation'),(402,2,16,179,'orderdetail'),(403,2,16,179,'orderfollow'),(404,2,16,179,'orderreturn'),(405,2,16,179,'orderslip'),(406,2,16,179,'pagenotfound'),(407,2,16,179,'pdfinvoice'),(408,2,16,179,'pdforderreturn'),(409,2,16,179,'pdforderslip'),(410,2,16,179,'pricesdrop'),(411,2,16,179,'product'),(412,2,16,179,'search'),(413,2,71,52,'cart'),(414,2,71,52,'order'),(415,2,26,179,'cart'),(418,3,19,238,'address'),(419,3,19,238,'addresses'),(420,3,19,238,'attachment'),(421,3,19,238,'auth'),(422,3,19,238,'bestsales'),(423,3,19,238,'cart'),(424,3,19,238,'category'),(425,3,19,238,'cms'),(426,3,19,238,'contact'),(427,3,19,238,'discount'),(428,3,19,238,'getfile'),(429,3,19,238,'guesttracking'),(430,3,19,238,'history'),(431,3,19,238,'identity'),(432,3,19,238,'myaccount'),(433,3,19,238,'newproducts'),(434,3,19,238,'order'),(435,3,19,238,'orderconfirmation'),(436,3,19,238,'orderdetail'),(437,3,19,238,'orderfollow'),(438,3,19,238,'orderreturn'),(439,3,19,238,'orderslip'),(440,3,19,238,'pagenotfound'),(441,3,19,238,'pdfinvoice'),(442,3,19,238,'pdforderreturn'),(443,3,19,238,'pdforderslip'),(444,3,19,238,'pricesdrop'),(445,3,19,238,'product'),(446,3,19,238,'search'),(447,3,16,179,'address'),(448,3,16,179,'addresses'),(449,3,16,179,'attachment'),(450,3,16,179,'auth'),(451,3,16,179,'bestsales'),(452,3,16,179,'cart'),(453,3,16,179,'category'),(454,3,16,179,'cms'),(455,3,16,179,'contact'),(456,3,16,179,'discount'),(457,3,16,179,'getfile'),(458,3,16,179,'guesttracking'),(459,3,16,179,'history'),(460,3,16,179,'identity'),(461,3,16,179,'myaccount'),(462,3,16,179,'newproducts'),(463,3,16,179,'order'),(464,3,16,179,'orderconfirmation'),(465,3,16,179,'orderdetail'),(466,3,16,179,'orderfollow'),(467,3,16,179,'orderreturn'),(468,3,16,179,'orderslip'),(469,3,16,179,'pagenotfound'),(470,3,16,179,'pdfinvoice'),(471,3,16,179,'pdforderreturn'),(472,3,16,179,'pdforderslip'),(473,3,16,179,'pricesdrop'),(474,3,16,179,'product'),(475,3,16,179,'search'),(476,3,71,52,'cart'),(477,3,71,52,'order'),(478,3,26,179,'cart'),(481,4,19,238,'address'),(482,4,19,238,'addresses'),(483,4,19,238,'attachment'),(484,4,19,238,'auth'),(485,4,19,238,'bestsales'),(486,4,19,238,'cart'),(487,4,19,238,'category'),(488,4,19,238,'cms'),(489,4,19,238,'contact'),(490,4,19,238,'discount'),(491,4,19,238,'getfile'),(492,4,19,238,'guesttracking'),(493,4,19,238,'history'),(494,4,19,238,'identity'),(495,4,19,238,'myaccount'),(496,4,19,238,'newproducts'),(497,4,19,238,'order'),(498,4,19,238,'orderconfirmation'),(499,4,19,238,'orderdetail'),(500,4,19,238,'orderfollow'),(501,4,19,238,'orderreturn'),(502,4,19,238,'orderslip'),(503,4,19,238,'pagenotfound'),(504,4,19,238,'pdfinvoice'),(505,4,19,238,'pdforderreturn'),(506,4,19,238,'pdforderslip'),(507,4,19,238,'pricesdrop'),(508,4,19,238,'product'),(509,4,19,238,'search'),(510,4,16,179,'address'),(511,4,16,179,'addresses'),(512,4,16,179,'attachment'),(513,4,16,179,'auth'),(514,4,16,179,'bestsales'),(515,4,16,179,'cart'),(516,4,16,179,'category'),(517,4,16,179,'cms'),(518,4,16,179,'contact'),(519,4,16,179,'discount'),(520,4,16,179,'getfile'),(521,4,16,179,'guesttracking'),(522,4,16,179,'history'),(523,4,16,179,'identity'),(524,4,16,179,'myaccount'),(525,4,16,179,'newproducts'),(526,4,16,179,'order'),(527,4,16,179,'orderconfirmation'),(528,4,16,179,'orderdetail'),(529,4,16,179,'orderfollow'),(530,4,16,179,'orderreturn'),(531,4,16,179,'orderslip'),(532,4,16,179,'pagenotfound'),(533,4,16,179,'pdfinvoice'),(534,4,16,179,'pdforderreturn'),(535,4,16,179,'pdforderslip'),(536,4,16,179,'pricesdrop'),(537,4,16,179,'product'),(538,4,16,179,'search'),(539,4,71,52,'cart'),(540,4,71,52,'order'),(541,4,26,179,'cart');
/*!40000 ALTER TABLE `ps_hook_module_exceptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_image`
--

DROP TABLE IF EXISTS `ps_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_image` (
  `id_image` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_product` int(10) unsigned NOT NULL,
  `position` smallint(2) unsigned NOT NULL DEFAULT '0',
  `cover` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_image`),
  UNIQUE KEY `id_product_cover` (`id_product`,`cover`),
  UNIQUE KEY `idx_product_image` (`id_image`,`id_product`,`cover`),
  KEY `image_product` (`id_product`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_image`
--

LOCK TABLES `ps_image` WRITE;
/*!40000 ALTER TABLE `ps_image` DISABLE KEYS */;
INSERT INTO `ps_image` VALUES (1,1,1,NULL),(2,1,2,1),(3,2,1,1),(6,4,1,1),(7,6,1,1),(10,9,1,1),(11,3,1,1),(12,10,1,1),(13,11,1,NULL),(14,11,2,NULL),(15,11,3,NULL),(16,11,4,1),(17,12,1,1),(18,12,2,NULL),(19,14,1,1),(20,15,1,1),(21,13,1,1),(22,13,2,NULL),(23,13,3,NULL),(28,2,2,NULL),(29,2,3,NULL),(30,2,5,NULL),(31,2,4,NULL),(32,8,1,1),(33,15,2,NULL),(34,15,3,NULL),(35,15,4,NULL),(36,15,5,NULL);
/*!40000 ALTER TABLE `ps_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_image_lang`
--

DROP TABLE IF EXISTS `ps_image_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_image_lang` (
  `id_image` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `legend` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_image`,`id_lang`),
  KEY `id_image` (`id_image`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_image_lang`
--

LOCK TABLES `ps_image_lang` WRITE;
/*!40000 ALTER TABLE `ps_image_lang` DISABLE KEYS */;
INSERT INTO `ps_image_lang` VALUES (1,1,''),(1,2,''),(1,3,''),(1,4,''),(2,1,''),(2,2,''),(2,3,''),(2,4,''),(3,1,''),(3,2,''),(3,3,''),(3,4,''),(6,1,''),(6,2,''),(6,3,''),(6,4,''),(7,1,''),(7,2,''),(7,3,''),(7,4,''),(10,1,''),(10,2,''),(10,3,''),(10,4,''),(11,1,''),(11,2,''),(11,3,''),(11,4,''),(12,1,''),(12,2,''),(12,3,''),(12,4,''),(13,1,''),(13,2,''),(13,3,''),(13,4,''),(14,1,''),(14,2,''),(14,3,''),(14,4,''),(15,1,''),(15,2,''),(15,3,''),(15,4,''),(16,1,''),(16,2,''),(16,3,''),(16,4,''),(17,1,''),(17,2,''),(17,3,''),(17,4,''),(18,1,''),(18,2,''),(18,3,''),(18,4,''),(19,1,''),(19,2,''),(19,3,''),(19,4,''),(20,1,''),(20,2,''),(20,3,''),(20,4,''),(21,1,''),(21,2,''),(21,3,''),(21,4,''),(22,1,''),(22,2,''),(22,3,''),(22,4,''),(23,1,''),(23,2,''),(23,3,''),(23,4,''),(28,1,''),(28,2,''),(28,3,''),(28,4,''),(29,1,''),(29,2,'波面剛古紙'),(29,3,''),(29,4,''),(30,1,''),(30,2,'炫光紙'),(30,3,''),(30,4,''),(31,1,''),(31,2,'トーメイあらじま'),(31,3,''),(31,4,''),(32,1,''),(32,2,''),(32,3,''),(32,4,''),(33,1,''),(33,2,''),(33,3,''),(33,4,''),(34,1,''),(34,2,''),(34,3,''),(34,4,''),(35,1,''),(35,2,''),(35,3,''),(35,4,''),(36,1,''),(36,2,''),(36,3,''),(36,4,'');
/*!40000 ALTER TABLE `ps_image_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_image_shop`
--

DROP TABLE IF EXISTS `ps_image_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_image_shop` (
  `id_product` int(10) unsigned NOT NULL,
  `id_image` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  `cover` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_image`,`id_shop`),
  UNIQUE KEY `id_product` (`id_product`,`id_shop`,`cover`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_image_shop`
--

LOCK TABLES `ps_image_shop` WRITE;
/*!40000 ALTER TABLE `ps_image_shop` DISABLE KEYS */;
INSERT INTO `ps_image_shop` VALUES (1,1,1,NULL),(1,2,1,1),(2,28,1,NULL),(2,29,1,NULL),(2,30,1,NULL),(2,31,1,NULL),(2,3,1,1),(3,11,1,1),(4,6,1,1),(6,7,1,1),(8,32,1,1),(9,10,1,1),(10,12,1,1),(11,13,1,NULL),(11,14,1,NULL),(11,15,1,NULL),(11,16,1,1),(12,18,1,NULL),(12,17,1,1),(13,22,1,NULL),(13,23,1,NULL),(13,21,1,1),(14,19,1,1),(15,33,1,NULL),(15,34,1,NULL),(15,35,1,NULL),(15,36,1,NULL),(15,20,1,1);
/*!40000 ALTER TABLE `ps_image_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_image_type`
--

DROP TABLE IF EXISTS `ps_image_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_image_type` (
  `id_image_type` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `width` int(10) unsigned NOT NULL,
  `height` int(10) unsigned NOT NULL,
  `products` tinyint(1) NOT NULL DEFAULT '1',
  `categories` tinyint(1) NOT NULL DEFAULT '1',
  `manufacturers` tinyint(1) NOT NULL DEFAULT '1',
  `suppliers` tinyint(1) NOT NULL DEFAULT '1',
  `stores` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_image_type`),
  KEY `image_type_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_image_type`
--

LOCK TABLES `ps_image_type` WRITE;
/*!40000 ALTER TABLE `ps_image_type` DISABLE KEYS */;
INSERT INTO `ps_image_type` VALUES (1,'cart_default',125,125,0,0,0,0,0),(2,'small_default',300,300,1,1,1,1,0),(3,'medium_default',452,452,0,0,1,1,0),(4,'home_default',500,500,1,0,0,0,0),(5,'large_default',800,800,1,0,1,1,0),(6,'category_default',141,180,0,1,0,0,0),(7,'stores_default',170,115,0,0,0,0,1);
/*!40000 ALTER TABLE `ps_image_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_import_match`
--

DROP TABLE IF EXISTS `ps_import_match`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_import_match` (
  `id_import_match` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `match` text NOT NULL,
  `skip` int(2) NOT NULL,
  PRIMARY KEY (`id_import_match`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_import_match`
--

LOCK TABLES `ps_import_match` WRITE;
/*!40000 ALTER TABLE `ps_import_match` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_import_match` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_info`
--

DROP TABLE IF EXISTS `ps_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_info` (
  `id_info` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_info`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_info`
--

LOCK TABLES `ps_info` WRITE;
/*!40000 ALTER TABLE `ps_info` DISABLE KEYS */;
INSERT INTO `ps_info` VALUES (1);
/*!40000 ALTER TABLE `ps_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_info_lang`
--

DROP TABLE IF EXISTS `ps_info_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_info_lang` (
  `id_info` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id_info`,`id_lang`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_info_lang`
--

LOCK TABLES `ps_info_lang` WRITE;
/*!40000 ALTER TABLE `ps_info_lang` DISABLE KEYS */;
INSERT INTO `ps_info_lang` VALUES (1,1,1,'<h3 class=\"display3-size\">移動記</h3>\n<p>手工縫製一本隨身畫冊．在路上邊走邊畫</p>\n<p class=\"gray-darker\">書冊體積小，輕便易攜帶，能收取到繪畫者面對大自然時最感動的線條和筆觸；其頁面接續頁面的結構易於表述時間，呈現宛如記憶綿延的連續動態想像。書冊需要近觀、徒手翻頁的特質，則提供了較為私密、親近人身體的感官體驗。</p>\n<p>在 HTML 編輯器用 Bootstrap Grid 實作「RWD」：</p>\n<div class=\"row\" style=\"margin-bottom:10px;\">\n<div class=\"col-xs-12 col-lg-3\">\n<div class=\"row\">\n<div class=\"col-xs-6 col-lg-12\"><img src=\"https://cdn.tekapo.io/img/cms/sample/grid-sample.jpg\" alt=\"移動記《山脈》 清水斷崖\" style=\"width:100%;margin:10px 0;\" /></div>\n<div class=\"col-xs-6 col-lg-12\"><img src=\"https://cdn.tekapo.io/img/cms/sample/grid-sample.jpg\" alt=\"移動記《山脈》 清水斷崖\" style=\"width:100%;margin:10px 0;\" /></div>\n</div>\n</div>\n<div class=\"col-xs-12 col-lg-6\"><img src=\"https://cdn.tekapo.io/img/cms/sample/grid-sample.jpg\" alt=\"移動記《山脈》 清水斷崖\" style=\"width:100%;margin:10px 0;\" /></div>\n<div class=\"col-xs-6 col-lg-3\"><img src=\"https://cdn.tekapo.io/img/cms/sample/grid-sample.jpg\" alt=\"移動記《山脈》 清水斷崖\" style=\"width:100%;margin:10px 0;\" /></div>\n</div>'),(1,1,2,'<h3>移動記</h3>\n<p><strong class=\"dark\">手工縫製一本隨身畫冊．在路上邊走邊畫</strong></p>\n<p>書冊體積小，輕便易攜帶，能收取到繪畫者面對大自然時最感動的線條和筆觸；其頁面接續頁面的結構易於表述時間，呈現宛如記憶綿延的連續動態想像。書冊需要近觀、徒手翻頁的特質，則提供了較為私密、親近人身體的感官體驗。</p>'),(1,1,3,'<h3>移動記</h3>\n<p><strong class=\"dark\">手工縫製一本隨身畫冊．在路上邊走邊畫</strong></p>\n<p>書冊體積小，輕便易攜帶，能收取到繪畫者面對大自然時最感動的線條和筆觸；其頁面接續頁面的結構易於表述時間，呈現宛如記憶綿延的連續動態想像。書冊需要近觀、徒手翻頁的特質，則提供了較為私密、親近人身體的感官體驗。</p>'),(1,1,4,'<h3>移動記</h3>\n<p><strong class=\"dark\">手工縫製一本隨身畫冊．在路上邊走邊畫</strong></p>\n<p>書冊體積小，輕便易攜帶，能收取到繪畫者面對大自然時最感動的線條和筆觸；其頁面接續頁面的結構易於表述時間，呈現宛如記憶綿延的連續動態想像。書冊需要近觀、徒手翻頁的特質，則提供了較為私密、親近人身體的感官體驗。</p>');
/*!40000 ALTER TABLE `ps_info_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_info_shop`
--

DROP TABLE IF EXISTS `ps_info_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_info_shop` (
  `id_info` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_info`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_info_shop`
--

LOCK TABLES `ps_info_shop` WRITE;
/*!40000 ALTER TABLE `ps_info_shop` DISABLE KEYS */;
INSERT INTO `ps_info_shop` VALUES (1,1);
/*!40000 ALTER TABLE `ps_info_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_lang`
--

DROP TABLE IF EXISTS `ps_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_lang` (
  `id_lang` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `iso_code` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `language_code` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `date_format_lite` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `date_format_full` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `is_rtl` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_lang`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_lang`
--

LOCK TABLES `ps_lang` WRITE;
/*!40000 ALTER TABLE `ps_lang` DISABLE KEYS */;
INSERT INTO `ps_lang` VALUES (1,'繁體',1,'tw','zh-tw','zh-TW','Y-m-d','Y-m-d H:i:s',0),(2,'English',0,'en','en-us','en-US','m/d/Y','m/d/Y H:i:s',0),(3,'簡体',0,'zh','zh-cn','zh-CN','Y-m-d','Y-m-d H:i:s',0),(4,'日本語',0,'ja','ja-jp','ja-JP','Y-m-d','Y-m-d H:i:s',0);
/*!40000 ALTER TABLE `ps_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_lang_shop`
--

DROP TABLE IF EXISTS `ps_lang_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_lang_shop` (
  `id_lang` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  PRIMARY KEY (`id_lang`,`id_shop`),
  KEY `IDX_2F43BFC7BA299860` (`id_lang`),
  KEY `IDX_2F43BFC7274A50A0` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_lang_shop`
--

LOCK TABLES `ps_lang_shop` WRITE;
/*!40000 ALTER TABLE `ps_lang_shop` DISABLE KEYS */;
INSERT INTO `ps_lang_shop` VALUES (1,1),(1,2),(1,3),(1,4),(2,1),(2,2),(2,3),(2,4),(3,1),(3,2),(3,3),(3,4),(4,1),(4,2),(4,3),(4,4);
/*!40000 ALTER TABLE `ps_lang_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_layered_category`
--

DROP TABLE IF EXISTS `ps_layered_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_layered_category` (
  `id_layered_category` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) unsigned NOT NULL,
  `id_category` int(10) unsigned NOT NULL,
  `id_value` int(10) unsigned DEFAULT '0',
  `type` enum('category','id_feature','id_attribute_group','quantity','condition','manufacturer','weight','price') NOT NULL,
  `position` int(10) unsigned NOT NULL,
  `filter_type` int(10) unsigned NOT NULL DEFAULT '0',
  `filter_show_limit` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_layered_category`),
  KEY `id_category` (`id_category`,`type`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_layered_category`
--

LOCK TABLES `ps_layered_category` WRITE;
/*!40000 ALTER TABLE `ps_layered_category` DISABLE KEYS */;
INSERT INTO `ps_layered_category` VALUES (1,1,2,NULL,'quantity',1,0,0),(2,1,2,NULL,'price',2,1,0),(3,1,2,1,'id_attribute_group',3,0,0),(4,1,2,2,'id_attribute_group',4,0,0),(5,1,2,3,'id_attribute_group',5,0,0),(6,1,7,NULL,'quantity',1,0,0),(7,1,7,NULL,'price',2,1,0),(8,1,7,1,'id_attribute_group',3,0,0),(9,1,7,2,'id_attribute_group',4,0,0),(10,1,7,3,'id_attribute_group',5,0,0),(11,1,8,NULL,'quantity',1,0,0),(12,1,8,NULL,'price',2,1,0),(13,1,8,1,'id_attribute_group',3,0,0),(14,1,8,2,'id_attribute_group',4,0,0),(15,1,8,3,'id_attribute_group',5,0,0),(16,1,14,NULL,'quantity',1,0,0),(17,1,14,NULL,'price',2,1,0),(18,1,14,1,'id_attribute_group',3,0,0),(19,1,14,2,'id_attribute_group',4,0,0),(20,1,14,3,'id_attribute_group',5,0,0);
/*!40000 ALTER TABLE `ps_layered_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_layered_filter`
--

DROP TABLE IF EXISTS `ps_layered_filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_layered_filter` (
  `id_layered_filter` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `filters` longtext,
  `n_categories` int(10) unsigned NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_layered_filter`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_layered_filter`
--

LOCK TABLES `ps_layered_filter` WRITE;
/*!40000 ALTER TABLE `ps_layered_filter` DISABLE KEYS */;
INSERT INTO `ps_layered_filter` VALUES (1,'我的模板 2018-07-11','a:7:{s:10:\"categories\";a:4:{i:0;i:2;i:1;i:7;i:2;i:8;i:3;i:14;}s:9:\"shop_list\";a:1:{i:0;i:1;}s:23:\"layered_selection_stock\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:30:\"layered_selection_price_slider\";a:2:{s:11:\"filter_type\";i:1;s:17:\"filter_show_limit\";i:0;}s:22:\"layered_selection_ag_1\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:22:\"layered_selection_ag_2\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}s:22:\"layered_selection_ag_3\";a:2:{s:11:\"filter_type\";i:0;s:17:\"filter_show_limit\";i:0;}}',4,'2019-01-24 00:47:25');
/*!40000 ALTER TABLE `ps_layered_filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_layered_filter_shop`
--

DROP TABLE IF EXISTS `ps_layered_filter_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_layered_filter_shop` (
  `id_layered_filter` int(10) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_layered_filter`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_layered_filter_shop`
--

LOCK TABLES `ps_layered_filter_shop` WRITE;
/*!40000 ALTER TABLE `ps_layered_filter_shop` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_layered_filter_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_layered_indexable_attribute_group`
--

DROP TABLE IF EXISTS `ps_layered_indexable_attribute_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_layered_indexable_attribute_group` (
  `id_attribute_group` int(11) NOT NULL,
  `indexable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_attribute_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_layered_indexable_attribute_group`
--

LOCK TABLES `ps_layered_indexable_attribute_group` WRITE;
/*!40000 ALTER TABLE `ps_layered_indexable_attribute_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_layered_indexable_attribute_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_layered_indexable_attribute_group_lang_value`
--

DROP TABLE IF EXISTS `ps_layered_indexable_attribute_group_lang_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_layered_indexable_attribute_group_lang_value` (
  `id_attribute_group` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `url_name` varchar(128) DEFAULT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_attribute_group`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_layered_indexable_attribute_group_lang_value`
--

LOCK TABLES `ps_layered_indexable_attribute_group_lang_value` WRITE;
/*!40000 ALTER TABLE `ps_layered_indexable_attribute_group_lang_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_layered_indexable_attribute_group_lang_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_layered_indexable_attribute_lang_value`
--

DROP TABLE IF EXISTS `ps_layered_indexable_attribute_lang_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_layered_indexable_attribute_lang_value` (
  `id_attribute` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `url_name` varchar(128) DEFAULT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_attribute`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_layered_indexable_attribute_lang_value`
--

LOCK TABLES `ps_layered_indexable_attribute_lang_value` WRITE;
/*!40000 ALTER TABLE `ps_layered_indexable_attribute_lang_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_layered_indexable_attribute_lang_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_layered_indexable_feature`
--

DROP TABLE IF EXISTS `ps_layered_indexable_feature`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_layered_indexable_feature` (
  `id_feature` int(11) NOT NULL,
  `indexable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_feature`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_layered_indexable_feature`
--

LOCK TABLES `ps_layered_indexable_feature` WRITE;
/*!40000 ALTER TABLE `ps_layered_indexable_feature` DISABLE KEYS */;
INSERT INTO `ps_layered_indexable_feature` VALUES (1,1),(2,1),(3,1),(4,1),(5,1);
/*!40000 ALTER TABLE `ps_layered_indexable_feature` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_layered_indexable_feature_lang_value`
--

DROP TABLE IF EXISTS `ps_layered_indexable_feature_lang_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_layered_indexable_feature_lang_value` (
  `id_feature` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `url_name` varchar(128) NOT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_feature`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_layered_indexable_feature_lang_value`
--

LOCK TABLES `ps_layered_indexable_feature_lang_value` WRITE;
/*!40000 ALTER TABLE `ps_layered_indexable_feature_lang_value` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_layered_indexable_feature_lang_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_layered_indexable_feature_value_lang_value`
--

DROP TABLE IF EXISTS `ps_layered_indexable_feature_value_lang_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_layered_indexable_feature_value_lang_value` (
  `id_feature_value` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `url_name` varchar(128) DEFAULT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_feature_value`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_layered_indexable_feature_value_lang_value`
--

LOCK TABLES `ps_layered_indexable_feature_value_lang_value` WRITE;
/*!40000 ALTER TABLE `ps_layered_indexable_feature_value_lang_value` DISABLE KEYS */;
INSERT INTO `ps_layered_indexable_feature_value_lang_value` VALUES (1,1,'',''),(1,2,'',''),(1,3,'',''),(1,4,'',''),(2,1,'',''),(2,2,'',''),(2,3,'',''),(2,4,'','');
/*!40000 ALTER TABLE `ps_layered_indexable_feature_value_lang_value` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_layered_price_index`
--

DROP TABLE IF EXISTS `ps_layered_price_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_layered_price_index` (
  `id_product` int(11) NOT NULL,
  `id_currency` int(11) NOT NULL,
  `id_shop` int(11) NOT NULL,
  `price_min` int(11) NOT NULL,
  `price_max` int(11) NOT NULL,
  PRIMARY KEY (`id_product`,`id_currency`,`id_shop`),
  KEY `id_currency` (`id_currency`),
  KEY `price_min` (`price_min`),
  KEY `price_max` (`price_max`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_layered_price_index`
--

LOCK TABLES `ps_layered_price_index` WRITE;
/*!40000 ALTER TABLE `ps_layered_price_index` DISABLE KEYS */;
INSERT INTO `ps_layered_price_index` VALUES (1,1,1,9000,18000),(1,1,2,9000,18000),(1,1,3,9000,18000),(1,1,4,9000,18000),(1,3,1,294,588),(1,3,2,294,588),(1,3,3,294,588),(1,3,4,294,588),(1,6,1,2296,4593),(1,6,2,2296,4593),(1,6,3,2296,4593),(1,6,4,2296,4593),(1,7,1,2019,4039),(1,7,2,2019,4039),(1,7,3,2019,4039),(1,7,4,2019,4039),(2,1,1,23330,46660),(2,1,2,0,0),(2,1,3,0,0),(2,1,4,0,0),(2,3,1,762,1524),(2,3,2,0,0),(2,3,3,0,0),(2,3,4,0,0),(2,6,1,5953,11906),(2,6,2,0,0),(2,6,3,0,0),(2,6,4,0,0),(2,7,1,5236,10472),(2,7,2,0,0),(2,7,3,0,0),(2,7,4,0,0),(3,1,1,1500,3000),(3,1,2,0,0),(3,1,3,0,0),(3,1,4,0,0),(3,3,1,49,98),(3,3,2,0,0),(3,3,3,0,0),(3,3,4,0,0),(3,6,1,382,765),(3,6,2,0,0),(3,6,3,0,0),(3,6,4,0,0),(3,7,1,336,673),(3,7,2,0,0),(3,7,3,0,0),(3,7,4,0,0),(4,1,1,10000,20000),(4,1,2,0,0),(4,1,3,0,0),(4,1,4,0,0),(4,3,1,326,653),(4,3,2,0,0),(4,3,3,0,0),(4,3,4,0,0),(4,6,1,2551,5103),(4,6,2,0,0),(4,6,3,0,0),(4,6,4,0,0),(4,7,1,2244,4488),(4,7,2,0,0),(4,7,3,0,0),(4,7,4,0,0),(5,1,1,1000000,1000000),(5,1,2,0,0),(5,1,3,0,0),(5,1,4,0,0),(5,3,1,32673,32673),(5,3,2,0,0),(5,3,3,0,0),(5,3,4,0,0),(5,6,1,255183,255183),(5,6,2,0,0),(5,6,3,0,0),(5,6,4,0,0),(5,7,1,224434,224434),(5,7,2,0,0),(5,7,3,0,0),(5,7,4,0,0),(6,1,1,50,100),(6,1,2,0,0),(6,1,3,0,0),(6,1,4,0,0),(6,3,1,1,3),(6,3,2,0,0),(6,3,3,0,0),(6,3,4,0,0),(6,6,1,12,25),(6,6,2,0,0),(6,6,3,0,0),(6,6,4,0,0),(6,7,1,11,22),(6,7,2,0,0),(6,7,3,0,0),(6,7,4,0,0),(7,1,1,11,12),(8,1,1,20,40),(8,1,2,0,0),(8,1,3,0,0),(8,1,4,0,0),(8,3,1,0,1),(8,3,2,0,0),(8,3,3,0,0),(8,3,4,0,0),(8,6,1,5,10),(8,6,2,0,0),(8,6,3,0,0),(8,6,4,0,0),(8,7,1,4,8),(8,7,2,0,0),(8,7,3,0,0),(8,7,4,0,0),(9,1,1,180,360),(9,1,2,0,0),(9,1,3,0,0),(9,1,4,0,0),(9,3,1,5,11),(9,3,2,0,0),(9,3,3,0,0),(9,3,4,0,0),(9,6,1,45,91),(9,6,2,0,0),(9,6,3,0,0),(9,6,4,0,0),(9,7,1,40,80),(9,7,2,0,0),(9,7,3,0,0),(9,7,4,0,0),(10,1,1,180,360),(10,1,2,0,0),(10,1,3,0,0),(10,1,4,0,0),(10,3,1,5,11),(10,3,2,0,0),(10,3,3,0,0),(10,3,4,0,0),(10,6,1,45,91),(10,6,2,0,0),(10,6,3,0,0),(10,6,4,0,0),(10,7,1,40,80),(10,7,2,0,0),(10,7,3,0,0),(10,7,4,0,0),(11,1,1,300,600),(11,1,2,0,0),(11,1,3,0,0),(11,1,4,0,0),(11,3,1,9,19),(11,3,2,0,0),(11,3,3,0,0),(11,3,4,0,0),(11,6,1,76,153),(11,6,2,0,0),(11,6,3,0,0),(11,6,4,0,0),(11,7,1,67,134),(11,7,2,0,0),(11,7,3,0,0),(11,7,4,0,0),(12,1,1,150,300),(12,1,2,0,0),(12,1,3,0,0),(12,1,4,0,0),(12,3,1,4,9),(12,3,2,0,0),(12,3,3,0,0),(12,3,4,0,0),(12,6,1,38,76),(12,6,2,0,0),(12,6,3,0,0),(12,6,4,0,0),(12,7,1,33,67),(12,7,2,0,0),(12,7,3,0,0),(12,7,4,0,0),(13,1,1,1800,3600),(13,1,2,0,0),(13,1,3,0,0),(13,1,4,0,0),(13,3,1,58,117),(13,3,2,0,0),(13,3,3,0,0),(13,3,4,0,0),(13,6,1,459,918),(13,6,2,0,0),(13,6,3,0,0),(13,6,4,0,0),(13,7,1,403,807),(13,7,2,0,0),(13,7,3,0,0),(13,7,4,0,0),(14,1,1,1620,3240),(14,1,2,1620,3240),(14,1,3,1620,3240),(14,1,4,1620,3240),(14,3,1,52,105),(14,3,2,52,105),(14,3,3,52,105),(14,3,4,52,105),(14,6,1,413,826),(14,6,2,413,826),(14,6,3,413,826),(14,6,4,413,826),(14,7,1,363,727),(14,7,2,363,727),(14,7,3,363,727),(14,7,4,363,727),(15,1,1,50050,100100),(15,1,2,0,0),(15,1,3,0,0),(15,1,4,0,0),(15,3,1,1635,3270),(15,3,2,0,0),(15,3,3,0,0),(15,3,4,0,0),(15,6,1,12771,25543),(15,6,2,0,0),(15,6,3,0,0),(15,6,4,0,0),(15,7,1,11232,22465),(15,7,2,0,0),(15,7,3,0,0),(15,7,4,0,0),(16,1,1,0,0),(16,1,2,0,0),(16,1,3,0,0),(16,1,4,0,0),(16,3,1,0,0),(16,3,2,0,0),(16,3,3,0,0),(16,3,4,0,0),(16,6,1,0,0),(16,6,2,0,0),(16,6,3,0,0),(16,6,4,0,0),(16,7,1,0,0),(16,7,2,0,0),(16,7,3,0,0),(16,7,4,0,0),(17,1,1,0,0),(17,3,1,0,0),(17,6,1,0,0),(17,7,1,0,0),(18,1,1,0,0),(18,3,1,0,0),(18,6,1,0,0),(18,7,1,0,0),(19,1,1,0,0),(19,3,1,0,0),(19,6,1,0,0),(19,7,1,0,0);
/*!40000 ALTER TABLE `ps_layered_price_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_layered_product_attribute`
--

DROP TABLE IF EXISTS `ps_layered_product_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_layered_product_attribute` (
  `id_attribute` int(10) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `id_attribute_group` int(10) unsigned NOT NULL DEFAULT '0',
  `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_attribute`,`id_product`,`id_shop`),
  UNIQUE KEY `id_attribute_group` (`id_attribute_group`,`id_attribute`,`id_product`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_layered_product_attribute`
--

LOCK TABLES `ps_layered_product_attribute` WRITE;
/*!40000 ALTER TABLE `ps_layered_product_attribute` DISABLE KEYS */;
INSERT INTO `ps_layered_product_attribute` VALUES (1,13,1,1),(2,13,1,1),(3,13,1,1),(4,13,1,1),(5,13,1,1),(6,2,2,1),(7,2,2,1),(8,2,2,1),(9,15,3,1),(10,15,3,1),(11,15,3,1),(12,15,3,1);
/*!40000 ALTER TABLE `ps_layered_product_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_link_block`
--

DROP TABLE IF EXISTS `ps_link_block`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_link_block` (
  `id_link_block` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_hook` int(1) unsigned DEFAULT NULL,
  `position` int(10) unsigned NOT NULL DEFAULT '0',
  `content` text,
  PRIMARY KEY (`id_link_block`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_link_block`
--

LOCK TABLES `ps_link_block` WRITE;
/*!40000 ALTER TABLE `ps_link_block` DISABLE KEYS */;
INSERT INTO `ps_link_block` VALUES (1,35,1,'{\"cms\":[false],\"product\":[\"prices-drop\",\"new-products\",\"best-sales\",\"viewed-products\"],\"static\":[false]}'),(2,35,2,'{\"cms\":[\"4\",\"5\",\"3\"],\"product\":[false],\"static\":[false]}'),(3,35,3,'{\"cms\":[\"1\",\"6\",\"2\"],\"product\":[false],\"static\":[false]}'),(4,35,4,'{\"cms\":[false],\"product\":[false],\"static\":[false]}');
/*!40000 ALTER TABLE `ps_link_block` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_link_block_lang`
--

DROP TABLE IF EXISTS `ps_link_block_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_link_block_lang` (
  `id_link_block` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `custom_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id_link_block`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_link_block_lang`
--

LOCK TABLES `ps_link_block_lang` WRITE;
/*!40000 ALTER TABLE `ps_link_block_lang` DISABLE KEYS */;
INSERT INTO `ps_link_block_lang` VALUES (1,1,'商品','[]'),(1,2,'Products',NULL),(1,3,'单件商品',NULL),(1,4,'商品',NULL),(2,1,'商店','[]'),(2,2,'Store','[]'),(2,3,'Store','[]'),(2,4,'Store','[]'),(3,1,'購物','[]'),(3,2,'FAQ','[]'),(4,1,'客服','{\"1\":{\"title\":\"\\u806f\\u7d61\\u6211\\u5011\",\"url\":\"\\/contact\"},\"2\":{\"title\":\"Facebook \\u79c1\\u8a0a\",\"url\":\"#\"},\"3\":{\"title\":\"LINE@\",\"url\":\"#\"}}'),(4,2,'Service','[]');
/*!40000 ALTER TABLE `ps_link_block_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_link_block_shop`
--

DROP TABLE IF EXISTS `ps_link_block_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_link_block_shop` (
  `id_link_block` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_link_block`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_link_block_shop`
--

LOCK TABLES `ps_link_block_shop` WRITE;
/*!40000 ALTER TABLE `ps_link_block_shop` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_link_block_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_linksmenutop`
--

DROP TABLE IF EXISTS `ps_linksmenutop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_linksmenutop` (
  `id_linksmenutop` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) unsigned NOT NULL,
  `new_window` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_linksmenutop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_linksmenutop`
--

LOCK TABLES `ps_linksmenutop` WRITE;
/*!40000 ALTER TABLE `ps_linksmenutop` DISABLE KEYS */;
INSERT INTO `ps_linksmenutop` VALUES (1,1,0);
/*!40000 ALTER TABLE `ps_linksmenutop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_linksmenutop_lang`
--

DROP TABLE IF EXISTS `ps_linksmenutop_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_linksmenutop_lang` (
  `id_linksmenutop` int(11) unsigned NOT NULL,
  `id_lang` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  `label` varchar(128) NOT NULL,
  `link` varchar(128) NOT NULL,
  KEY `id_linksmenutop` (`id_linksmenutop`,`id_lang`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_linksmenutop_lang`
--

LOCK TABLES `ps_linksmenutop_lang` WRITE;
/*!40000 ALTER TABLE `ps_linksmenutop_lang` DISABLE KEYS */;
INSERT INTO `ps_linksmenutop_lang` VALUES (1,1,1,'聯絡我們','https://shop1.presta.shop/contact'),(1,2,1,'','http://'),(1,3,1,'','http://'),(1,4,1,'','http://');
/*!40000 ALTER TABLE `ps_linksmenutop_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_log`
--

DROP TABLE IF EXISTS `ps_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_log` (
  `id_log` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `severity` tinyint(1) NOT NULL,
  `error_code` int(11) DEFAULT NULL,
  `message` text NOT NULL,
  `object_type` varchar(32) DEFAULT NULL,
  `object_id` int(10) unsigned DEFAULT NULL,
  `id_employee` int(10) unsigned DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_log`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_log`
--

LOCK TABLES `ps_log` WRITE;
/*!40000 ALTER TABLE `ps_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_log_email`
--

DROP TABLE IF EXISTS `ps_log_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_log_email` (
  `id_log_email` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_email_type` int(10) unsigned NOT NULL,
  `id_cart_rule` int(10) unsigned NOT NULL,
  `id_customer` int(10) unsigned DEFAULT NULL,
  `id_cart` int(10) unsigned DEFAULT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_log_email`),
  KEY `date_add` (`date_add`),
  KEY `id_cart` (`id_cart`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_log_email`
--

LOCK TABLES `ps_log_email` WRITE;
/*!40000 ALTER TABLE `ps_log_email` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_log_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_mail`
--

DROP TABLE IF EXISTS `ps_mail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_mail` (
  `id_mail` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `recipient` varchar(126) NOT NULL,
  `template` varchar(62) NOT NULL,
  `subject` varchar(254) NOT NULL,
  `id_lang` int(11) unsigned NOT NULL,
  `date_add` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_mail`),
  KEY `recipient` (`recipient`(10))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_mail`
--

LOCK TABLES `ps_mail` WRITE;
/*!40000 ALTER TABLE `ps_mail` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_mail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_mailalert_customer_oos`
--

DROP TABLE IF EXISTS `ps_mailalert_customer_oos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_mailalert_customer_oos` (
  `id_customer` int(10) unsigned NOT NULL,
  `customer_email` varchar(128) NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `id_product_attribute` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_customer`,`customer_email`,`id_product`,`id_product_attribute`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_mailalert_customer_oos`
--

LOCK TABLES `ps_mailalert_customer_oos` WRITE;
/*!40000 ALTER TABLE `ps_mailalert_customer_oos` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_mailalert_customer_oos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_manufacturer`
--

DROP TABLE IF EXISTS `ps_manufacturer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_manufacturer` (
  `id_manufacturer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_manufacturer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_manufacturer`
--

LOCK TABLES `ps_manufacturer` WRITE;
/*!40000 ALTER TABLE `ps_manufacturer` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_manufacturer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_manufacturer_lang`
--

DROP TABLE IF EXISTS `ps_manufacturer_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_manufacturer_lang` (
  `id_manufacturer` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `description` text,
  `short_description` text,
  `meta_title` varchar(128) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_manufacturer`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_manufacturer_lang`
--

LOCK TABLES `ps_manufacturer_lang` WRITE;
/*!40000 ALTER TABLE `ps_manufacturer_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_manufacturer_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_manufacturer_shop`
--

DROP TABLE IF EXISTS `ps_manufacturer_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_manufacturer_shop` (
  `id_manufacturer` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_manufacturer`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_manufacturer_shop`
--

LOCK TABLES `ps_manufacturer_shop` WRITE;
/*!40000 ALTER TABLE `ps_manufacturer_shop` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_manufacturer_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_memcached_servers`
--

DROP TABLE IF EXISTS `ps_memcached_servers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_memcached_servers` (
  `id_memcached_server` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip` varchar(254) NOT NULL,
  `port` int(11) unsigned NOT NULL,
  `weight` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_memcached_server`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_memcached_servers`
--

LOCK TABLES `ps_memcached_servers` WRITE;
/*!40000 ALTER TABLE `ps_memcached_servers` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_memcached_servers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_message`
--

DROP TABLE IF EXISTS `ps_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_message` (
  `id_message` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cart` int(10) unsigned DEFAULT NULL,
  `id_customer` int(10) unsigned NOT NULL,
  `id_employee` int(10) unsigned DEFAULT NULL,
  `id_order` int(10) unsigned NOT NULL,
  `message` text NOT NULL,
  `private` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_message`),
  KEY `message_order` (`id_order`),
  KEY `id_cart` (`id_cart`),
  KEY `id_customer` (`id_customer`),
  KEY `id_employee` (`id_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_message`
--

LOCK TABLES `ps_message` WRITE;
/*!40000 ALTER TABLE `ps_message` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_message_readed`
--

DROP TABLE IF EXISTS `ps_message_readed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_message_readed` (
  `id_message` int(10) unsigned NOT NULL,
  `id_employee` int(10) unsigned NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_message`,`id_employee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_message_readed`
--

LOCK TABLES `ps_message_readed` WRITE;
/*!40000 ALTER TABLE `ps_message_readed` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_message_readed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_meta`
--

DROP TABLE IF EXISTS `ps_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_meta` (
  `id_meta` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page` varchar(64) NOT NULL,
  `configurable` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_meta`),
  UNIQUE KEY `page` (`page`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_meta`
--

LOCK TABLES `ps_meta` WRITE;
/*!40000 ALTER TABLE `ps_meta` DISABLE KEYS */;
INSERT INTO `ps_meta` VALUES (1,'pagenotfound',1),(2,'best-sales',1),(3,'contact',1),(4,'index',1),(5,'manufacturer',1),(6,'new-products',1),(7,'password',1),(8,'prices-drop',1),(9,'sitemap',1),(10,'supplier',1),(11,'address',1),(12,'addresses',1),(13,'authentication',1),(14,'cart',1),(15,'discount',1),(16,'history',1),(17,'identity',1),(18,'my-account',1),(19,'order-follow',1),(20,'order-slip',1),(21,'order',1),(22,'search',1),(23,'stores',1),(24,'guest-tracking',1),(25,'order-confirmation',1),(26,'product',0),(27,'category',0),(28,'cms',0),(29,'module-cheque-payment',0),(30,'module-cheque-validation',0),(31,'module-bankwire-validation',0),(32,'module-bankwire-payment',0),(33,'module-cashondelivery-validation',0),(36,'module-ps_emailsubscription-verification',1),(37,'module-ps_emailsubscription-subscription',1),(38,'module-ps_shoppingcart-ajax',1),(39,'module-ps_wirepayment-payment',1),(40,'module-ps_wirepayment-validation',1),(42,'module-ps_cashondelivery-validation',1),(43,'module-ps_emailalerts-account',1),(45,'module-simplicity_fbmessaging-webhook',1),(46,'module-simplicity_fbmessaging-cart',1),(47,'module-simplicity_fbmessaging-cron',1),(48,'module-simplicity_fbmessaging-restore',1),(49,'module-paypal-payment',1),(50,'module-paypal-validation',1),(51,'viewed-products',1);
/*!40000 ALTER TABLE `ps_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_meta_lang`
--

DROP TABLE IF EXISTS `ps_meta_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_meta_lang` (
  `id_meta` int(10) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_lang` int(10) unsigned NOT NULL,
  `title` varchar(128) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `url_rewrite` varchar(254) NOT NULL,
  PRIMARY KEY (`id_meta`,`id_shop`,`id_lang`),
  KEY `id_shop` (`id_shop`),
  KEY `id_lang` (`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_meta_lang`
--

LOCK TABLES `ps_meta_lang` WRITE;
/*!40000 ALTER TABLE `ps_meta_lang` DISABLE KEYS */;
INSERT INTO `ps_meta_lang` VALUES (1,1,1,'404 錯誤','找不到此頁','','page-not-found'),(1,1,2,'404 error','This page cannot be found','','page-not-found'),(1,1,3,'404 error','This page cannot be found','','page-not-found'),(1,1,4,'404エラー','ページが見つかりませんでした。','','page-not-found'),(2,1,1,'最近熱門','今日熱門商品','','best-sales'),(2,1,2,'Best sales','Our best sales','','best-sales'),(2,1,3,'Best sales','Our best sales','','best-sales'),(2,1,4,'ベストセラー','当店のベストセラー','','best-sales'),(3,1,1,'聯絡我們','歡迎透過「聯絡我們」詢問，我們會盡快答覆您。','','contact'),(3,1,2,'Contact','Use our form to contact us','','contact'),(3,1,3,'Contact','Use our form to contact us','','contact'),(3,1,4,'Contact','ご質問は問合せフォームをご利用ください。','','contact'),(3,2,1,'聯絡我們','歡迎透過「聯絡我們」詢問，我們會盡快答覆您。','','contact'),(3,2,2,'Contact','Use our form to contact us','','contact'),(3,2,3,'Contact','Use our form to contact us','','contact'),(3,2,4,'Contact','ご質問は問合せフォームをご利用ください。','','contact'),(3,3,1,'聯絡我們','歡迎透過「聯絡我們」詢問，我們會盡快答覆您。','','contact'),(3,3,2,'Contact','Use our form to contact us','','contact'),(3,3,3,'Contact','Use our form to contact us','','contact'),(3,3,4,'Contact','ご質問は問合せフォームをご利用ください。','','contact'),(3,4,1,'聯絡我們','歡迎透過「聯絡我們」詢問，我們會盡快答覆您。','','contact'),(3,4,2,'Contact','Use our form to contact us','','contact'),(3,4,3,'Contact','Use our form to contact us','','contact'),(3,4,4,'Contact','ご質問は問合せフォームをご利用ください。','','contact'),(4,1,1,'我的 TekapoCart','本站於 _TIME_ 建立，全站 A+ SSL 加密，顧客可以安心的購物網站。','',''),(4,1,2,'My TekapoCart Online Store','Started on _TIME_. A secure and reliable online store.','',''),(4,1,3,'','Software maintained by TekapoCart','',''),(4,1,4,'','Software maintained by TekapoCart','',''),(5,1,1,'Brands','Brands list','','brands'),(5,1,2,'Brands','Brands list','','brands'),(5,1,3,'Brands','Brands list','','brands'),(5,1,4,'Brands','Brands list','','brands'),(6,1,1,'新品上架','質感新鮮品','','new-products'),(6,1,2,'New Products','Our new products','','new-products'),(6,1,3,'New Products','Our new products','','new-products'),(6,1,4,'New Products','新着商品','','new-products'),(7,1,1,'忘記密碼','輸入您用於登錄的電郵地址以接收具有新密碼的電子郵件','','password-recovery'),(7,1,2,'Forgot Password','Enter your e-mail address used to register in goal to get e-mail with your new password','','password-recovery'),(7,1,3,'Forgot Password','Enter your e-mail address used to register in goal to get e-mail with your new password','','password-recovery'),(7,1,4,'Forgot Password','Enter your e-mail address used to register in goal to get e-mail with your new password','','password-recovery'),(8,1,1,'更多優惠','限時限量折扣專區','','prices-drop'),(8,1,2,'Prices drop','Our special products','','prices-drop'),(8,1,3,'Prices drop','Our special products','','prices-drop'),(8,1,4,'値下げ商品','Our special products','','prices-drop'),(9,1,1,'網站導覽','再次搜索','','網站導覽'),(9,1,2,'Sitemap','Lost ? Find what your are looking for','','网站地图'),(9,1,3,'站点地图','Lost ? Find what your are looking for','','网站地图'),(9,1,4,'Sitemap','お探しのものが見つかりませんか？','','网站地图'),(10,1,1,'供應商','供應商列表','','supplier'),(10,1,2,'Supplier','Suppliers list','','supplier'),(10,1,3,'Supplier','Suppliers list','','supplier'),(10,1,4,'Supplier','サプライヤー一覧','','supplier'),(11,1,1,'配送資訊','','','address'),(11,1,2,'Address','','','address'),(11,1,3,'地址','','','address'),(11,1,4,'Address','','','address'),(12,1,1,'配送資訊','','','addresses'),(12,1,2,'Addresses','','','addresses'),(12,1,3,'地址','','','addresses'),(12,1,4,'Addresses','','','addresses'),(13,1,1,'登入','','','login'),(13,1,2,'Login','','','login'),(13,1,3,'登录','','','login'),(13,1,4,'Login','','','login'),(14,1,1,'購物車','','','cart'),(14,1,2,'Cart','','','cart'),(14,1,3,'购物车','','','cart'),(14,1,4,'Cart','','','cart'),(15,1,1,'優惠券','','','discount'),(15,1,2,'Discount','','','discount'),(15,1,3,'折扣','','','discount'),(15,1,4,'割引','','','discount'),(16,1,1,'訂單記錄','','','order-history'),(16,1,2,'Order History','','','order-history'),(16,1,3,'Order History','','','order-history'),(16,1,4,'Order History','','','order-history'),(17,1,1,'個人資料','','','identity'),(17,1,2,'Personal Info','','','identity'),(17,1,3,'Personal Info','','','identity'),(17,1,4,'Personal Info','','','identity'),(18,1,1,'會員中心','','','my-account'),(18,1,2,'Member Area','','','my-account'),(18,1,3,'Member Area','','','my-account'),(18,1,4,'Member Area','','','my-account'),(19,1,1,'訂單追蹤','','','order-follow'),(19,1,2,'Order Follow','','','order-follow'),(19,1,3,'Order Follow','','','order-follow'),(19,1,4,'注文対応','','','order-follow'),(20,1,1,'折讓單','','','order-slip'),(20,1,2,'Order Slip','','','order-slip'),(20,1,3,'Order Slip','','','order-slip'),(20,1,4,'Order Slip','','','order-slip'),(21,1,1,'訂單','','','order'),(21,1,2,'Order','','','order'),(21,1,3,'购买','','','订单'),(21,1,4,'Order','','','order'),(22,1,1,'搜尋結果','用關鍵字搜尋','','search'),(22,1,2,'Search','','','search'),(22,1,3,'搜索','','','search'),(22,1,4,'Search','','','search'),(23,1,1,'商店','','','商店'),(23,1,2,'Store','','','stores'),(23,1,3,'Store','','','stores'),(23,1,4,'Store','','','店舗'),(24,1,1,'非會員訂單追蹤','','','guest-tracking'),(24,1,2,'Guest tracking','','','guest-tracking'),(24,1,3,'Guest tracking','','','guest-tracking'),(24,1,4,'ゲストトラッキング','','','guest-tracking'),(25,1,1,'訂單確認','','','order-confirmation'),(25,1,2,'Order Confirmation','','','order-confirmation'),(25,1,3,'Order Confirmation','','','order-confirmation'),(25,1,4,'Order Confirmation','','','order-confirmation'),(26,1,1,'商品頁','','','product'),(26,1,2,'Product','','','product'),(26,1,3,'Product','','','product'),(26,1,4,'Product','','','product'),(27,1,1,'商品分類','','','category'),(27,1,2,'Category','','','category'),(27,1,3,'Category','','','category'),(27,1,4,'Category','','','category'),(28,1,1,'自訂頁面','','','cms'),(28,1,2,'Cms','','','cms'),(28,1,3,'Cms','','','cms'),(28,1,4,'Cms','','','cms'),(36,1,1,'','','',''),(36,1,2,'','','',''),(36,1,3,'','','',''),(36,1,4,'','','',''),(37,1,1,'','','',''),(37,1,2,'','','',''),(37,1,3,'','','',''),(37,1,4,'','','',''),(38,1,1,'','','',''),(38,1,2,'','','',''),(38,1,3,'','','',''),(38,1,4,'','','',''),(39,1,1,'','','',''),(39,1,2,'','','',''),(39,1,3,'','','',''),(39,1,4,'','','',''),(40,1,1,'','','',''),(40,1,2,'','','',''),(40,1,3,'','','',''),(40,1,4,'','','',''),(42,1,1,'','','',''),(42,1,2,'','','',''),(42,1,3,'','','',''),(42,1,4,'','','',''),(43,1,1,'','','',''),(43,1,2,'','','',''),(43,1,3,'','','',''),(43,1,4,'','','',''),(45,1,1,'','','',''),(45,1,2,'','','',''),(45,1,3,'','','',''),(45,1,4,'','','',''),(46,1,1,'','','',''),(46,1,2,'','','',''),(46,1,3,'','','',''),(46,1,4,'','','',''),(47,1,1,'','','',''),(47,1,2,'','','',''),(47,1,3,'','','',''),(47,1,4,'','','',''),(48,1,1,'','','',''),(48,1,2,'','','',''),(48,1,3,'','','',''),(48,1,4,'','','',''),(49,1,1,'','','',''),(49,1,2,'','','',''),(49,1,3,'','','',''),(49,1,4,'','','',''),(50,1,1,'','','',''),(50,1,2,'','','',''),(50,1,3,'','','',''),(50,1,4,'','','',''),(51,1,1,'瀏覽紀錄','','','viewed-products'),(51,1,2,'Viewed Products','','','viewed-products'),(51,1,3,'Viewed Products','','','viewed-products'),(51,1,4,'Viewed Products','','','viewed-products');
/*!40000 ALTER TABLE `ps_meta_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_module`
--

DROP TABLE IF EXISTS `ps_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_module` (
  `id_module` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `version` varchar(8) NOT NULL,
  PRIMARY KEY (`id_module`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_module`
--

LOCK TABLES `ps_module` WRITE;
/*!40000 ALTER TABLE `ps_module` DISABLE KEYS */;
INSERT INTO `ps_module` VALUES (1,'contactform',1,'4.1.1'),(2,'dashactivity',1,'2.0.2'),(3,'dashtrends',1,'2.0.2'),(4,'dashgoals',1,'2.0.2'),(5,'dashproducts',1,'2.0.3'),(6,'graphnvd3',1,'2.0.0'),(7,'gridhtml',1,'2.0.0'),(8,'ps_banner',1,'2.1.0'),(9,'ps_categorytree',1,'2.0.0'),(11,'ps_contactinfo',1,'3.1.0'),(12,'ps_currencyselector',1,'2.0.0'),(14,'ps_customersignin',1,'2.0.2'),(15,'ps_customtext',1,'4.1.0'),(16,'ps_emailsubscription',1,'2.3.0'),(17,'ps_facetedsearch',1,'2.2.0'),(18,'ps_featuredproducts',1,'2.0.0'),(19,'ps_imageslider',1,'3.0.0'),(20,'ps_languageselector',1,'2.0.2'),(21,'ps_linklist',1,'2.1.5'),(22,'ps_mainmenu',1,'2.1.1'),(23,'ps_searchbar',1,'2.0.1'),(24,'ps_sharebuttons',1,'2.0.1'),(25,'ps_shoppingcart',1,'2.0.1'),(26,'ps_socialfollow',1,'2.0.0'),(27,'ps_themecusto',1,'1.0.5'),(28,'ps_wirepayment',1,'2.0.4'),(32,'statsbestcustomers',1,'2.0.2'),(33,'statsbestproducts',1,'2.0.0'),(35,'statsbestvouchers',1,'2.0.0'),(36,'statscarrier',1,'2.0.0'),(39,'statsdata',1,'2.0.0'),(41,'statsforecast',1,'2.0.3'),(42,'statslive',1,'2.0.2'),(43,'statsnewsletter',1,'2.0.2'),(48,'statssales',1,'2.0.0'),(51,'statsvisits',1,'2.0.2'),(57,'ps_bestsellers',1,'1.0.3'),(58,'ps_specials',1,'1.0.1'),(59,'ps_cashondelivery',1,'1.0.6'),(60,'ps_reminder',1,'2.0.0'),(61,'ps_crossselling',1,'2.0.0'),(62,'ps_newproducts',1,'1.0.1'),(68,'ps_emailalerts',1,'2.1.0'),(69,'ps_googleanalytics',1,'3.1.1'),(71,'ps_viewedproduct',1,'1.1.0'),(72,'ps_brandlist',1,'1.0.2'),(73,'ps_supplierlist',1,'1.0.3'),(77,'pscleaner',1,'2.0.0'),(78,'pspixel',1,'1.0.5'),(79,'simplicity_logo',1,'1.0.0'),(80,'ps_customeraccountlinks',1,'3.1.0'),(81,'blockreassurance',1,'3.0.1'),(82,'simplicity_sociallogin',1,'1.0.0'),(83,'smilepaymsg',1,'2.1.10'),(84,'smilepay_c2cup',1,'2.2.5'),(85,'smilepay_palmboxc2cup',1,'2.2.5'),(86,'smilepay_c2c',1,'2.2.5'),(87,'smilepay_ibon',1,'2.2.5'),(88,'smilepay_famiport',1,'2.2.5'),(89,'smilepay_ezcatup',1,'2.2.5'),(90,'smilepay_ezcat',1,'2.2.5'),(91,'smilepay_csv',1,'2.2.5'),(92,'smilepay_credit',1,'2.2.5'),(93,'smilepay_atm',1,'2.2.5'),(94,'simplicity_cmsblock',1,'1.0.0'),(95,'simplicity_fbmessaging',1,'1.0.0'),(96,'simplicity_headerbar',1,'1.0.0'),(97,'gsitemap',1,'3.2.2'),(98,'paypal',1,'4.4.2'),(100,'simplicity_gtm',1,'1.0.0');
/*!40000 ALTER TABLE `ps_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_module_access`
--

DROP TABLE IF EXISTS `ps_module_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_module_access` (
  `id_profile` int(10) unsigned NOT NULL,
  `id_authorization_role` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_profile`,`id_authorization_role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_module_access`
--

LOCK TABLES `ps_module_access` WRITE;
/*!40000 ALTER TABLE `ps_module_access` DISABLE KEYS */;
INSERT INTO `ps_module_access` VALUES (1,453),(1,454),(1,455),(1,456),(1,457),(1,458),(1,459),(1,460),(1,461),(1,462),(1,463),(1,464),(1,469),(1,470),(1,471),(1,472),(1,473),(1,474),(1,475),(1,476),(1,477),(1,478),(1,479),(1,480),(1,481),(1,482),(1,483),(1,484),(1,485),(1,486),(1,487),(1,488),(1,489),(1,490),(1,491),(1,492),(1,497),(1,498),(1,499),(1,500),(1,501),(1,502),(1,503),(1,504),(1,509),(1,510),(1,511),(1,512),(1,513),(1,514),(1,515),(1,516),(1,517),(1,518),(1,519),(1,520),(1,521),(1,522),(1,523),(1,524),(1,525),(1,526),(1,527),(1,528),(1,529),(1,530),(1,531),(1,532),(1,533),(1,534),(1,535),(1,536),(1,537),(1,538),(1,539),(1,540),(1,545),(1,546),(1,547),(1,548),(1,549),(1,550),(1,551),(1,552),(1,553),(1,554),(1,555),(1,556),(1,557),(1,558),(1,559),(1,560),(1,561),(1,562),(1,563),(1,564),(1,565),(1,566),(1,567),(1,568),(1,581),(1,582),(1,583),(1,584),(1,597),(1,598),(1,599),(1,600),(1,601),(1,602),(1,603),(1,604),(1,625),(1,626),(1,627),(1,628),(1,633),(1,634),(1,635),(1,636),(1,637),(1,638),(1,639),(1,640),(1,661),(1,662),(1,663),(1,664),(1,673),(1,674),(1,675),(1,676),(1,709),(1,710),(1,711),(1,712),(1,713),(1,714),(1,715),(1,716),(1,717),(1,718),(1,719),(1,720),(1,721),(1,722),(1,723),(1,724),(1,725),(1,726),(1,727),(1,728),(1,729),(1,730),(1,731),(1,732),(1,757),(1,758),(1,759),(1,760),(1,761),(1,762),(1,763),(1,764),(1,769),(1,770),(1,771),(1,772),(1,773),(1,774),(1,775),(1,776),(1,777),(1,778),(1,779),(1,780),(1,789),(1,790),(1,791),(1,792),(1,793),(1,794),(1,795),(1,796),(1,797),(1,798),(1,799),(1,800),(1,801),(1,802),(1,803),(1,804),(1,805),(1,806),(1,807),(1,808),(1,809),(1,810),(1,811),(1,812),(1,813),(1,814),(1,815),(1,816),(1,817),(1,818),(1,819),(1,820),(1,821),(1,822),(1,823),(1,824),(1,825),(1,826),(1,827),(1,828),(1,833),(1,834),(1,835),(1,836),(1,837),(1,838),(1,839),(1,840),(1,841),(1,842),(1,843),(1,844),(1,845),(1,846),(1,847),(1,848),(1,853),(1,854),(1,855),(1,856),(1,861),(1,862),(1,863),(1,864),(1,865),(1,866),(1,867),(1,868),(1,869),(1,870),(1,871),(1,872),(1,873),(1,874),(1,875),(1,876),(1,877),(1,878),(1,879),(1,880),(1,889),(1,890),(1,891),(1,892),(1,893),(1,894),(1,895),(1,896),(1,901),(1,902),(1,903),(1,904);
/*!40000 ALTER TABLE `ps_module_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_module_carrier`
--

DROP TABLE IF EXISTS `ps_module_carrier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_module_carrier` (
  `id_module` int(10) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_reference` int(11) NOT NULL,
  PRIMARY KEY (`id_module`,`id_shop`,`id_reference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_module_carrier`
--

LOCK TABLES `ps_module_carrier` WRITE;
/*!40000 ALTER TABLE `ps_module_carrier` DISABLE KEYS */;
INSERT INTO `ps_module_carrier` VALUES (28,1,101),(28,1,102),(28,1,103),(28,1,105),(28,1,107),(28,1,108),(28,1,109),(28,1,113),(28,1,114),(59,1,101),(59,1,102),(86,1,104),(86,1,106),(87,1,103),(87,1,107),(87,1,108),(87,1,109),(87,1,113),(87,1,114),(88,1,105),(88,1,107),(88,1,108),(88,1,109),(88,1,113),(88,1,114),(90,1,110),(90,1,111),(90,1,112),(91,1,107),(91,1,108),(91,1,109),(91,1,113),(91,1,114),(92,1,101),(92,1,103),(92,1,105),(92,1,107),(92,1,108),(92,1,109),(92,1,114),(93,1,107),(93,1,108),(93,1,109),(93,1,113),(93,1,114),(98,1,114);
/*!40000 ALTER TABLE `ps_module_carrier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_module_country`
--

DROP TABLE IF EXISTS `ps_module_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_module_country` (
  `id_module` int(10) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_country` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_module`,`id_shop`,`id_country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_module_country`
--

LOCK TABLES `ps_module_country` WRITE;
/*!40000 ALTER TABLE `ps_module_country` DISABLE KEYS */;
INSERT INTO `ps_module_country` VALUES (28,1,203),(28,2,203),(28,3,203),(28,4,203),(59,1,203),(59,2,203),(59,3,203),(59,4,203),(86,1,203),(86,2,203),(86,3,203),(86,4,203),(87,1,203),(87,2,203),(87,3,203),(87,4,203),(88,1,203),(88,2,203),(88,3,203),(88,4,203),(90,1,203),(90,2,203),(90,3,203),(90,4,203),(91,1,203),(91,2,203),(91,3,203),(91,4,203),(92,1,203),(92,2,203),(92,3,203),(92,4,203),(93,1,203),(93,2,203),(93,3,203),(93,4,203),(98,1,5),(98,1,11),(98,1,21),(98,1,22),(98,1,25),(98,1,136),(98,1,203),(98,2,5),(98,2,11),(98,2,21),(98,2,22),(98,2,25),(98,2,136),(98,2,203),(98,3,5),(98,3,11),(98,3,21),(98,3,22),(98,3,25),(98,3,136),(98,3,203),(98,4,5),(98,4,11),(98,4,21),(98,4,22),(98,4,25),(98,4,136),(98,4,203);
/*!40000 ALTER TABLE `ps_module_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_module_currency`
--

DROP TABLE IF EXISTS `ps_module_currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_module_currency` (
  `id_module` int(10) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_currency` int(11) NOT NULL,
  PRIMARY KEY (`id_module`,`id_shop`,`id_currency`),
  KEY `id_module` (`id_module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_module_currency`
--

LOCK TABLES `ps_module_currency` WRITE;
/*!40000 ALTER TABLE `ps_module_currency` DISABLE KEYS */;
INSERT INTO `ps_module_currency` VALUES (28,1,1),(28,2,1),(28,3,1),(28,4,1),(59,1,1),(59,1,3),(59,1,6),(59,1,7),(59,2,1),(59,2,3),(59,2,6),(59,2,7),(59,3,1),(59,3,3),(59,3,6),(59,3,7),(59,4,1),(59,4,3),(59,4,6),(59,4,7),(86,1,1),(86,2,1),(86,3,1),(86,4,1),(87,1,1),(87,2,1),(87,3,1),(87,4,1),(88,1,1),(88,2,1),(88,3,1),(88,4,1),(90,1,1),(90,2,1),(90,3,1),(90,4,1),(91,1,1),(91,2,1),(91,3,1),(91,4,1),(92,1,1),(92,1,3),(92,1,6),(92,1,7),(92,2,1),(92,2,3),(92,2,6),(92,2,7),(92,3,1),(92,3,3),(92,3,6),(92,3,7),(92,4,1),(92,4,3),(92,4,6),(92,4,7),(93,1,1),(93,2,1),(93,3,1),(93,4,1),(98,1,-1),(98,2,-1),(98,3,-1),(98,4,-1);
/*!40000 ALTER TABLE `ps_module_currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_module_group`
--

DROP TABLE IF EXISTS `ps_module_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_module_group` (
  `id_module` int(10) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_group` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_module`,`id_shop`,`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_module_group`
--

LOCK TABLES `ps_module_group` WRITE;
/*!40000 ALTER TABLE `ps_module_group` DISABLE KEYS */;
INSERT INTO `ps_module_group` VALUES (1,1,1),(1,1,2),(1,1,3),(1,2,1),(1,2,2),(1,2,3),(1,3,1),(1,3,2),(1,3,3),(1,4,1),(1,4,2),(1,4,3),(2,1,1),(2,1,2),(2,1,3),(2,2,1),(2,2,2),(2,2,3),(2,3,1),(2,3,2),(2,3,3),(2,4,1),(2,4,2),(2,4,3),(3,1,1),(3,1,2),(3,1,3),(3,2,1),(3,2,2),(3,2,3),(3,3,1),(3,3,2),(3,3,3),(3,4,1),(3,4,2),(3,4,3),(4,1,1),(4,1,2),(4,1,3),(4,2,1),(4,2,2),(4,2,3),(4,3,1),(4,3,2),(4,3,3),(4,4,1),(4,4,2),(4,4,3),(5,1,1),(5,1,2),(5,1,3),(5,2,1),(5,2,2),(5,2,3),(5,3,1),(5,3,2),(5,3,3),(5,4,1),(5,4,2),(5,4,3),(6,1,1),(6,1,2),(6,1,3),(6,2,1),(6,2,2),(6,2,3),(6,3,1),(6,3,2),(6,3,3),(6,4,1),(6,4,2),(6,4,3),(7,1,1),(7,1,2),(7,1,3),(7,2,1),(7,2,2),(7,2,3),(7,3,1),(7,3,2),(7,3,3),(7,4,1),(7,4,2),(7,4,3),(8,1,1),(8,1,2),(8,1,3),(8,2,1),(8,2,2),(8,2,3),(8,3,1),(8,3,2),(8,3,3),(8,4,1),(8,4,2),(8,4,3),(9,1,1),(9,1,2),(9,1,3),(9,2,1),(9,2,2),(9,2,3),(9,3,1),(9,3,2),(9,3,3),(9,4,1),(9,4,2),(9,4,3),(11,1,1),(11,1,2),(11,1,3),(11,2,1),(11,2,2),(11,2,3),(11,3,1),(11,3,2),(11,3,3),(11,4,1),(11,4,2),(11,4,3),(12,1,1),(12,1,2),(12,1,3),(12,2,1),(12,2,2),(12,2,3),(12,3,1),(12,3,2),(12,3,3),(12,4,1),(12,4,2),(12,4,3),(14,1,1),(14,1,2),(14,1,3),(14,2,1),(14,2,2),(14,2,3),(14,3,1),(14,3,2),(14,3,3),(14,4,1),(14,4,2),(14,4,3),(15,1,1),(15,1,2),(15,1,3),(15,2,1),(15,2,2),(15,2,3),(15,3,1),(15,3,2),(15,3,3),(15,4,1),(15,4,2),(15,4,3),(16,1,1),(16,1,2),(16,1,3),(16,2,1),(16,2,2),(16,2,3),(16,3,1),(16,3,2),(16,3,3),(16,4,1),(16,4,2),(16,4,3),(17,1,1),(17,1,2),(17,1,3),(17,2,1),(17,2,2),(17,2,3),(17,3,1),(17,3,2),(17,3,3),(17,4,1),(17,4,2),(17,4,3),(18,1,1),(18,1,2),(18,1,3),(18,2,1),(18,2,2),(18,2,3),(18,3,1),(18,3,2),(18,3,3),(18,4,1),(18,4,2),(18,4,3),(19,1,1),(19,1,2),(19,1,3),(19,2,1),(19,2,2),(19,2,3),(19,3,1),(19,3,2),(19,3,3),(19,4,1),(19,4,2),(19,4,3),(20,1,1),(20,1,2),(20,1,3),(20,2,1),(20,2,2),(20,2,3),(20,3,1),(20,3,2),(20,3,3),(20,4,1),(20,4,2),(20,4,3),(21,1,1),(21,1,2),(21,1,3),(21,2,1),(21,2,2),(21,2,3),(21,3,1),(21,3,2),(21,3,3),(21,4,1),(21,4,2),(21,4,3),(22,1,1),(22,1,2),(22,1,3),(22,2,1),(22,2,2),(22,2,3),(22,3,1),(22,3,2),(22,3,3),(22,4,1),(22,4,2),(22,4,3),(23,1,1),(23,1,2),(23,1,3),(23,2,1),(23,2,2),(23,2,3),(23,3,1),(23,3,2),(23,3,3),(23,4,1),(23,4,2),(23,4,3),(24,1,1),(24,1,2),(24,1,3),(24,2,1),(24,2,2),(24,2,3),(24,3,1),(24,3,2),(24,3,3),(24,4,1),(24,4,2),(24,4,3),(25,1,1),(25,1,2),(25,1,3),(25,2,1),(25,2,2),(25,2,3),(25,3,1),(25,3,2),(25,3,3),(25,4,1),(25,4,2),(25,4,3),(26,1,1),(26,1,2),(26,1,3),(26,2,1),(26,2,2),(26,2,3),(26,3,1),(26,3,2),(26,3,3),(26,4,1),(26,4,2),(26,4,3),(27,1,1),(27,1,2),(27,1,3),(27,2,1),(27,2,2),(27,2,3),(27,3,1),(27,3,2),(27,3,3),(27,4,1),(27,4,2),(27,4,3),(28,1,1),(28,1,2),(28,1,3),(28,2,1),(28,2,2),(28,2,3),(28,3,1),(28,3,2),(28,3,3),(28,4,1),(28,4,2),(28,4,3),(32,1,1),(32,1,2),(32,1,3),(32,2,1),(32,2,2),(32,2,3),(32,3,1),(32,3,2),(32,3,3),(32,4,1),(32,4,2),(32,4,3),(33,1,1),(33,1,2),(33,1,3),(33,2,1),(33,2,2),(33,2,3),(33,3,1),(33,3,2),(33,3,3),(33,4,1),(33,4,2),(33,4,3),(39,1,1),(39,1,2),(39,1,3),(39,2,1),(39,2,2),(39,2,3),(39,3,1),(39,3,2),(39,3,3),(39,4,1),(39,4,2),(39,4,3),(41,1,1),(41,1,2),(41,1,3),(41,2,1),(41,2,2),(41,2,3),(41,3,1),(41,3,2),(41,3,3),(41,4,1),(41,4,2),(41,4,3),(42,1,1),(42,1,2),(42,1,3),(42,2,1),(42,2,2),(42,2,3),(42,3,1),(42,3,2),(42,3,3),(42,4,1),(42,4,2),(42,4,3),(43,1,1),(43,1,2),(43,1,3),(43,2,1),(43,2,2),(43,2,3),(43,3,1),(43,3,2),(43,3,3),(43,4,1),(43,4,2),(43,4,3),(48,1,1),(48,1,2),(48,1,3),(48,2,1),(48,2,2),(48,2,3),(48,3,1),(48,3,2),(48,3,3),(48,4,1),(48,4,2),(48,4,3),(51,1,1),(51,1,2),(51,1,3),(51,2,1),(51,2,2),(51,2,3),(51,3,1),(51,3,2),(51,3,3),(51,4,1),(51,4,2),(51,4,3),(57,1,1),(57,1,2),(57,1,3),(57,2,1),(57,2,2),(57,2,3),(57,3,1),(57,3,2),(57,3,3),(57,4,1),(57,4,2),(57,4,3),(58,1,1),(58,1,2),(58,1,3),(58,2,1),(58,2,2),(58,2,3),(58,3,1),(58,3,2),(58,3,3),(58,4,1),(58,4,2),(58,4,3),(59,1,1),(59,1,2),(59,1,3),(59,2,1),(59,2,2),(59,2,3),(59,3,1),(59,3,2),(59,3,3),(59,4,1),(59,4,2),(59,4,3),(60,1,1),(60,1,2),(60,1,3),(60,2,1),(60,2,2),(60,2,3),(60,3,1),(60,3,2),(60,3,3),(60,4,1),(60,4,2),(60,4,3),(61,1,1),(61,1,2),(61,1,3),(61,2,1),(61,2,2),(61,2,3),(61,3,1),(61,3,2),(61,3,3),(61,4,1),(61,4,2),(61,4,3),(62,1,1),(62,1,2),(62,1,3),(62,2,1),(62,2,2),(62,2,3),(62,3,1),(62,3,2),(62,3,3),(62,4,1),(62,4,2),(62,4,3),(68,1,1),(68,1,2),(68,1,3),(68,2,1),(68,2,2),(68,2,3),(68,3,1),(68,3,2),(68,3,3),(68,4,1),(68,4,2),(68,4,3),(69,1,1),(69,1,2),(69,1,3),(69,2,1),(69,2,2),(69,2,3),(69,3,1),(69,3,2),(69,3,3),(69,4,1),(69,4,2),(69,4,3),(71,1,1),(71,1,2),(71,1,3),(71,2,1),(71,2,2),(71,2,3),(71,3,1),(71,3,2),(71,3,3),(71,4,1),(71,4,2),(71,4,3),(72,1,1),(72,1,2),(72,1,3),(72,2,1),(72,2,2),(72,2,3),(72,3,1),(72,3,2),(72,3,3),(72,4,1),(72,4,2),(72,4,3),(73,1,1),(73,1,2),(73,1,3),(73,2,1),(73,2,2),(73,2,3),(73,3,1),(73,3,2),(73,3,3),(73,4,1),(73,4,2),(73,4,3),(77,1,1),(77,1,2),(77,1,3),(77,2,1),(77,2,2),(77,2,3),(77,3,1),(77,3,2),(77,3,3),(77,4,1),(77,4,2),(77,4,3),(78,1,1),(78,1,2),(78,1,3),(78,2,1),(78,2,2),(78,2,3),(78,3,1),(78,3,2),(78,3,3),(78,4,1),(78,4,2),(78,4,3),(79,1,1),(79,1,2),(79,1,3),(79,2,1),(79,2,2),(79,2,3),(79,3,1),(79,3,2),(79,3,3),(79,4,1),(79,4,2),(79,4,3),(80,1,1),(80,1,2),(80,1,3),(80,2,1),(80,2,2),(80,2,3),(80,3,1),(80,3,2),(80,3,3),(80,4,1),(80,4,2),(80,4,3),(81,1,1),(81,1,2),(81,1,3),(81,2,1),(81,2,2),(81,2,3),(81,3,1),(81,3,2),(81,3,3),(81,4,1),(81,4,2),(81,4,3),(82,1,1),(82,1,2),(82,1,3),(82,2,1),(82,2,2),(82,2,3),(82,3,1),(82,3,2),(82,3,3),(82,4,1),(82,4,2),(82,4,3),(83,1,1),(83,1,2),(83,1,3),(83,2,1),(83,2,2),(83,2,3),(83,3,1),(83,3,2),(83,3,3),(83,4,1),(83,4,2),(83,4,3),(84,1,1),(84,1,2),(84,1,3),(84,2,1),(84,2,2),(84,2,3),(84,3,1),(84,3,2),(84,3,3),(84,4,1),(84,4,2),(84,4,3),(85,1,1),(85,1,2),(85,1,3),(85,2,1),(85,2,2),(85,2,3),(85,3,1),(85,3,2),(85,3,3),(85,4,1),(85,4,2),(85,4,3),(86,1,1),(86,1,2),(86,1,3),(86,2,1),(86,2,2),(86,2,3),(86,3,1),(86,3,2),(86,3,3),(86,4,1),(86,4,2),(86,4,3),(87,1,1),(87,1,2),(87,1,3),(87,2,1),(87,2,2),(87,2,3),(87,3,1),(87,3,2),(87,3,3),(87,4,1),(87,4,2),(87,4,3),(88,1,1),(88,1,2),(88,1,3),(88,2,1),(88,2,2),(88,2,3),(88,3,1),(88,3,2),(88,3,3),(88,4,1),(88,4,2),(88,4,3),(89,1,1),(89,1,2),(89,1,3),(89,2,1),(89,2,2),(89,2,3),(89,3,1),(89,3,2),(89,3,3),(89,4,1),(89,4,2),(89,4,3),(90,1,1),(90,1,2),(90,1,3),(90,2,1),(90,2,2),(90,2,3),(90,3,1),(90,3,2),(90,3,3),(90,4,1),(90,4,2),(90,4,3),(91,1,1),(91,1,2),(91,1,3),(91,2,1),(91,2,2),(91,2,3),(91,3,1),(91,3,2),(91,3,3),(91,4,1),(91,4,2),(91,4,3),(92,1,1),(92,1,2),(92,1,3),(92,2,1),(92,2,2),(92,2,3),(92,3,1),(92,3,2),(92,3,3),(92,4,1),(92,4,2),(92,4,3),(93,1,1),(93,1,2),(93,1,3),(93,2,1),(93,2,2),(93,2,3),(93,3,1),(93,3,2),(93,3,3),(93,4,1),(93,4,2),(93,4,3),(94,1,1),(94,1,2),(94,1,3),(94,2,1),(94,2,2),(94,2,3),(94,3,1),(94,3,2),(94,3,3),(94,4,1),(94,4,2),(94,4,3),(95,1,1),(95,1,2),(95,1,3),(95,2,1),(95,2,2),(95,2,3),(95,3,1),(95,3,2),(95,3,3),(95,4,1),(95,4,2),(95,4,3),(96,1,1),(96,1,2),(96,1,3),(96,2,1),(96,2,2),(96,2,3),(96,3,1),(96,3,2),(96,3,3),(96,4,1),(96,4,2),(96,4,3),(97,1,1),(97,1,2),(97,1,3),(97,2,1),(97,2,2),(97,2,3),(97,3,1),(97,3,2),(97,3,3),(97,4,1),(97,4,2),(97,4,3),(98,1,1),(98,1,2),(98,1,3),(98,2,1),(98,2,2),(98,2,3),(98,3,1),(98,3,2),(98,3,3),(98,4,1),(98,4,2),(98,4,3),(100,1,1),(100,1,2),(100,1,3),(100,2,1),(100,2,2),(100,2,3),(100,3,1),(100,3,2),(100,3,3),(100,4,1),(100,4,2),(100,4,3);
/*!40000 ALTER TABLE `ps_module_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_module_history`
--

DROP TABLE IF EXISTS `ps_module_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_module_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_employee` int(11) NOT NULL,
  `id_module` int(11) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_module_history`
--

LOCK TABLES `ps_module_history` WRITE;
/*!40000 ALTER TABLE `ps_module_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_module_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_module_preference`
--

DROP TABLE IF EXISTS `ps_module_preference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_module_preference` (
  `id_module_preference` int(11) NOT NULL AUTO_INCREMENT,
  `id_employee` int(11) NOT NULL,
  `module` varchar(255) NOT NULL,
  `interest` tinyint(1) DEFAULT NULL,
  `favorite` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_module_preference`),
  UNIQUE KEY `employee_module` (`id_employee`,`module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_module_preference`
--

LOCK TABLES `ps_module_preference` WRITE;
/*!40000 ALTER TABLE `ps_module_preference` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_module_preference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_module_shop`
--

DROP TABLE IF EXISTS `ps_module_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_module_shop` (
  `id_module` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  `enable_device` tinyint(1) NOT NULL DEFAULT '7',
  PRIMARY KEY (`id_module`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_module_shop`
--

LOCK TABLES `ps_module_shop` WRITE;
/*!40000 ALTER TABLE `ps_module_shop` DISABLE KEYS */;
INSERT INTO `ps_module_shop` VALUES (1,1,7),(1,2,7),(1,3,7),(1,4,7),(5,1,7),(5,2,7),(5,3,7),(5,4,7),(8,1,7),(8,2,7),(8,3,7),(8,4,7),(9,1,7),(9,2,7),(9,3,7),(9,4,7),(11,1,7),(11,2,7),(11,3,7),(11,4,7),(12,1,7),(12,2,7),(12,3,7),(12,4,7),(14,1,7),(14,2,7),(14,3,7),(14,4,7),(15,1,7),(15,2,7),(15,3,7),(15,4,7),(16,1,7),(16,2,7),(16,3,7),(16,4,7),(17,1,7),(17,2,7),(17,3,7),(17,4,7),(18,1,7),(18,2,7),(18,3,7),(18,4,7),(19,1,7),(19,2,7),(19,3,7),(19,4,7),(20,1,7),(20,2,7),(20,3,7),(20,4,7),(21,1,7),(21,2,7),(21,3,7),(21,4,7),(22,1,7),(22,2,7),(22,3,7),(22,4,7),(23,1,7),(23,2,7),(23,3,7),(23,4,7),(24,1,7),(24,2,7),(24,3,7),(24,4,7),(25,1,7),(25,2,7),(25,3,7),(25,4,7),(26,1,7),(26,2,7),(26,3,7),(26,4,7),(27,1,7),(27,2,7),(27,3,7),(27,4,7),(28,1,7),(28,2,7),(28,3,7),(28,4,7),(32,1,7),(32,2,7),(32,3,7),(32,4,7),(33,1,7),(33,2,7),(33,3,7),(33,4,7),(39,1,7),(39,2,7),(39,3,7),(39,4,7),(41,1,7),(41,2,7),(41,3,7),(41,4,7),(42,1,7),(42,2,7),(42,3,7),(42,4,7),(43,1,7),(43,2,7),(43,3,7),(43,4,7),(48,1,7),(48,2,7),(48,3,7),(48,4,7),(51,1,7),(51,2,7),(51,3,7),(51,4,7),(57,1,7),(57,2,7),(57,3,7),(57,4,7),(58,2,7),(58,3,7),(58,4,7),(59,1,7),(59,2,7),(59,3,7),(59,4,7),(60,1,7),(60,2,7),(60,3,7),(60,4,7),(61,1,7),(61,2,7),(61,3,7),(61,4,7),(62,2,7),(62,3,7),(62,4,7),(68,1,7),(68,2,7),(68,3,7),(68,4,7),(69,1,7),(69,2,7),(69,3,7),(69,4,7),(71,1,7),(71,2,7),(71,3,7),(71,4,7),(77,1,7),(77,2,7),(77,3,7),(77,4,7),(78,1,7),(78,2,7),(78,3,7),(78,4,7),(79,1,7),(79,2,7),(79,3,7),(79,4,7),(80,1,7),(80,2,7),(80,3,7),(80,4,7),(81,1,7),(81,2,7),(81,3,7),(81,4,7),(82,1,7),(82,2,7),(82,3,7),(82,4,7),(83,1,7),(83,2,7),(83,3,7),(83,4,7),(84,1,7),(84,2,7),(84,3,7),(84,4,7),(85,1,7),(85,2,7),(85,3,7),(85,4,7),(86,1,7),(86,2,7),(86,3,7),(86,4,7),(87,1,7),(87,2,7),(87,3,7),(87,4,7),(88,1,7),(88,2,7),(88,3,7),(88,4,7),(89,1,7),(89,2,7),(89,3,7),(89,4,7),(90,1,7),(90,2,7),(90,3,7),(90,4,7),(91,1,7),(91,2,7),(91,3,7),(91,4,7),(92,1,7),(92,2,7),(92,3,7),(92,4,7),(93,1,7),(93,2,7),(93,3,7),(93,4,7),(94,1,7),(94,2,7),(94,3,7),(94,4,7),(95,1,7),(95,2,7),(95,3,7),(95,4,7),(96,1,7),(96,2,7),(96,3,7),(96,4,7),(97,1,7),(97,2,7),(97,3,7),(97,4,7),(98,1,7),(98,2,7),(98,3,7),(98,4,7),(100,1,7),(100,2,7),(100,3,7),(100,4,7);
/*!40000 ALTER TABLE `ps_module_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_operating_system`
--

DROP TABLE IF EXISTS `ps_operating_system`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_operating_system` (
  `id_operating_system` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id_operating_system`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_operating_system`
--

LOCK TABLES `ps_operating_system` WRITE;
/*!40000 ALTER TABLE `ps_operating_system` DISABLE KEYS */;
INSERT INTO `ps_operating_system` VALUES (1,'Windows XP'),(2,'Windows Vista'),(3,'Windows 7'),(4,'Windows 8'),(5,'Windows 8.1'),(6,'Windows 10'),(7,'MacOsX'),(8,'Linux'),(9,'Android');
/*!40000 ALTER TABLE `ps_operating_system` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_order_carrier`
--

DROP TABLE IF EXISTS `ps_order_carrier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_order_carrier` (
  `id_order_carrier` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) unsigned NOT NULL,
  `id_carrier` int(11) unsigned NOT NULL,
  `id_order_invoice` int(11) unsigned DEFAULT NULL,
  `weight` decimal(20,6) DEFAULT NULL,
  `shipping_cost_tax_excl` decimal(20,6) DEFAULT NULL,
  `shipping_cost_tax_incl` decimal(20,6) DEFAULT NULL,
  `tracking_number` varchar(64) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_order_carrier`),
  KEY `id_order` (`id_order`),
  KEY `id_carrier` (`id_carrier`),
  KEY `id_order_invoice` (`id_order_invoice`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_order_carrier`
--

LOCK TABLES `ps_order_carrier` WRITE;
/*!40000 ALTER TABLE `ps_order_carrier` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_carrier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_order_cart_rule`
--

DROP TABLE IF EXISTS `ps_order_cart_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_order_cart_rule` (
  `id_order_cart_rule` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_order` int(10) unsigned NOT NULL,
  `id_cart_rule` int(10) unsigned NOT NULL,
  `id_order_invoice` int(10) unsigned DEFAULT '0',
  `name` varchar(254) NOT NULL,
  `value` decimal(17,2) NOT NULL DEFAULT '0.00',
  `value_tax_excl` decimal(17,2) NOT NULL DEFAULT '0.00',
  `free_shipping` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_order_cart_rule`),
  KEY `id_order` (`id_order`),
  KEY `id_cart_rule` (`id_cart_rule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_order_cart_rule`
--

LOCK TABLES `ps_order_cart_rule` WRITE;
/*!40000 ALTER TABLE `ps_order_cart_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_cart_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_order_detail`
--

DROP TABLE IF EXISTS `ps_order_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_order_detail` (
  `id_order_detail` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_order` int(10) unsigned NOT NULL,
  `id_order_invoice` int(11) DEFAULT NULL,
  `id_warehouse` int(10) unsigned DEFAULT '0',
  `id_shop` int(11) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `product_attribute_id` int(10) unsigned DEFAULT NULL,
  `id_customization` int(10) unsigned DEFAULT '0',
  `product_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_quantity` int(10) unsigned NOT NULL DEFAULT '0',
  `product_quantity_in_stock` int(10) NOT NULL DEFAULT '0',
  `product_quantity_refunded` int(10) unsigned NOT NULL DEFAULT '0',
  `product_quantity_return` int(10) unsigned NOT NULL DEFAULT '0',
  `product_quantity_reinjected` int(10) unsigned NOT NULL DEFAULT '0',
  `product_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `reduction_percent` decimal(10,2) NOT NULL DEFAULT '0.00',
  `reduction_amount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `reduction_amount_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `reduction_amount_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `group_reduction` decimal(10,2) NOT NULL DEFAULT '0.00',
  `product_quantity_discount` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `product_ean13` varchar(13) DEFAULT NULL,
  `product_isbn` varchar(32) DEFAULT NULL,
  `product_upc` varchar(12) DEFAULT NULL,
  `product_reference` varchar(32) DEFAULT NULL,
  `product_supplier_reference` varchar(32) DEFAULT NULL,
  `product_weight` decimal(20,6) NOT NULL,
  `id_tax_rules_group` int(11) unsigned DEFAULT '0',
  `tax_computation_method` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `tax_name` varchar(16) NOT NULL,
  `tax_rate` decimal(10,3) NOT NULL DEFAULT '0.000',
  `ecotax` decimal(21,6) NOT NULL DEFAULT '0.000000',
  `ecotax_tax_rate` decimal(5,3) NOT NULL DEFAULT '0.000',
  `discount_quantity_applied` tinyint(1) NOT NULL DEFAULT '0',
  `download_hash` varchar(255) DEFAULT NULL,
  `download_nb` int(10) unsigned DEFAULT '0',
  `download_deadline` datetime DEFAULT NULL,
  `total_price_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_price_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `unit_price_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `unit_price_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping_price_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping_price_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `purchase_supplier_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `original_product_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `original_wholesale_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  PRIMARY KEY (`id_order_detail`),
  KEY `order_detail_order` (`id_order`),
  KEY `product_id` (`product_id`,`product_attribute_id`),
  KEY `product_attribute_id` (`product_attribute_id`),
  KEY `id_tax_rules_group` (`id_tax_rules_group`),
  KEY `id_order_id_order_detail` (`id_order`,`id_order_detail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_order_detail`
--

LOCK TABLES `ps_order_detail` WRITE;
/*!40000 ALTER TABLE `ps_order_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_order_detail_tax`
--

DROP TABLE IF EXISTS `ps_order_detail_tax`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_order_detail_tax` (
  `id_order_detail` int(11) NOT NULL,
  `id_tax` int(11) NOT NULL,
  `unit_amount` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `total_amount` decimal(16,6) NOT NULL DEFAULT '0.000000',
  KEY `id_order_detail` (`id_order_detail`),
  KEY `id_tax` (`id_tax`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_order_detail_tax`
--

LOCK TABLES `ps_order_detail_tax` WRITE;
/*!40000 ALTER TABLE `ps_order_detail_tax` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_detail_tax` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_order_history`
--

DROP TABLE IF EXISTS `ps_order_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_order_history` (
  `id_order_history` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_employee` int(10) unsigned NOT NULL,
  `id_order` int(10) unsigned NOT NULL,
  `id_order_state` int(10) unsigned NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_order_history`),
  KEY `order_history_order` (`id_order`),
  KEY `id_employee` (`id_employee`),
  KEY `id_order_state` (`id_order_state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_order_history`
--

LOCK TABLES `ps_order_history` WRITE;
/*!40000 ALTER TABLE `ps_order_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_order_invoice`
--

DROP TABLE IF EXISTS `ps_order_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_order_invoice` (
  `id_order_invoice` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_order` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `delivery_number` int(11) NOT NULL,
  `delivery_date` datetime DEFAULT NULL,
  `total_discount_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_discount_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_products` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_products_wt` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `shipping_tax_computation_method` int(10) unsigned NOT NULL,
  `total_wrapping_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_wrapping_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `shop_address` text,
  `note` text,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_order_invoice`),
  KEY `id_order` (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_order_invoice`
--

LOCK TABLES `ps_order_invoice` WRITE;
/*!40000 ALTER TABLE `ps_order_invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_invoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_order_invoice_payment`
--

DROP TABLE IF EXISTS `ps_order_invoice_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_order_invoice_payment` (
  `id_order_invoice` int(11) unsigned NOT NULL,
  `id_order_payment` int(11) unsigned NOT NULL,
  `id_order` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_order_invoice`,`id_order_payment`),
  KEY `order_payment` (`id_order_payment`),
  KEY `id_order` (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_order_invoice_payment`
--

LOCK TABLES `ps_order_invoice_payment` WRITE;
/*!40000 ALTER TABLE `ps_order_invoice_payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_invoice_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_order_invoice_tax`
--

DROP TABLE IF EXISTS `ps_order_invoice_tax`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_order_invoice_tax` (
  `id_order_invoice` int(11) NOT NULL,
  `type` varchar(15) NOT NULL,
  `id_tax` int(11) NOT NULL,
  `amount` decimal(10,6) NOT NULL DEFAULT '0.000000',
  KEY `id_tax` (`id_tax`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_order_invoice_tax`
--

LOCK TABLES `ps_order_invoice_tax` WRITE;
/*!40000 ALTER TABLE `ps_order_invoice_tax` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_invoice_tax` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_order_message`
--

DROP TABLE IF EXISTS `ps_order_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_order_message` (
  `id_order_message` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_order_message`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_order_message`
--

LOCK TABLES `ps_order_message` WRITE;
/*!40000 ALTER TABLE `ps_order_message` DISABLE KEYS */;
INSERT INTO `ps_order_message` VALUES (1,'2018-09-21 23:55:06');
/*!40000 ALTER TABLE `ps_order_message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_order_message_lang`
--

DROP TABLE IF EXISTS `ps_order_message_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_order_message_lang` (
  `id_order_message` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`id_order_message`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_order_message_lang`
--

LOCK TABLES `ps_order_message_lang` WRITE;
/*!40000 ALTER TABLE `ps_order_message_lang` DISABLE KEYS */;
INSERT INTO `ps_order_message_lang` VALUES (1,1,'7-11 已出貨','您好，包裹已寄出，7-11交貨便追蹤號碼：OOOOOOOO，預計2日送達指定門市，取件人需出示身份證件，讓門市人員核對無誤 ，方可取貨。感謝支持！'),(1,2,'test','test'),(1,3,'test','test'),(1,4,'test','test');
/*!40000 ALTER TABLE `ps_order_message_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_order_payment`
--

DROP TABLE IF EXISTS `ps_order_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_order_payment` (
  `id_order_payment` int(11) NOT NULL AUTO_INCREMENT,
  `order_reference` varchar(9) DEFAULT NULL,
  `id_currency` int(10) unsigned NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  `conversion_rate` decimal(13,6) NOT NULL DEFAULT '1.000000',
  `transaction_id` varchar(254) DEFAULT NULL,
  `card_number` varchar(254) DEFAULT NULL,
  `card_brand` varchar(254) DEFAULT NULL,
  `card_expiration` char(7) DEFAULT NULL,
  `card_holder` varchar(254) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_order_payment`),
  KEY `order_reference` (`order_reference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_order_payment`
--

LOCK TABLES `ps_order_payment` WRITE;
/*!40000 ALTER TABLE `ps_order_payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_order_return`
--

DROP TABLE IF EXISTS `ps_order_return`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_order_return` (
  `id_order_return` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_customer` int(10) unsigned NOT NULL,
  `id_order` int(10) unsigned NOT NULL,
  `state` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `question` text NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_order_return`),
  KEY `order_return_customer` (`id_customer`),
  KEY `id_order` (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_order_return`
--

LOCK TABLES `ps_order_return` WRITE;
/*!40000 ALTER TABLE `ps_order_return` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_return` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_order_return_detail`
--

DROP TABLE IF EXISTS `ps_order_return_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_order_return_detail` (
  `id_order_return` int(10) unsigned NOT NULL,
  `id_order_detail` int(10) unsigned NOT NULL,
  `id_customization` int(10) unsigned NOT NULL DEFAULT '0',
  `product_quantity` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_order_return`,`id_order_detail`,`id_customization`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_order_return_detail`
--

LOCK TABLES `ps_order_return_detail` WRITE;
/*!40000 ALTER TABLE `ps_order_return_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_return_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_order_return_state`
--

DROP TABLE IF EXISTS `ps_order_return_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_order_return_state` (
  `id_order_return_state` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `color` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id_order_return_state`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_order_return_state`
--

LOCK TABLES `ps_order_return_state` WRITE;
/*!40000 ALTER TABLE `ps_order_return_state` DISABLE KEYS */;
INSERT INTO `ps_order_return_state` VALUES (1,'#4169E1'),(2,'#8A2BE2'),(3,'#32CD32'),(4,'#DC143C'),(5,'#108510');
/*!40000 ALTER TABLE `ps_order_return_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_order_return_state_lang`
--

DROP TABLE IF EXISTS `ps_order_return_state_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_order_return_state_lang` (
  `id_order_return_state` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id_order_return_state`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_order_return_state_lang`
--

LOCK TABLES `ps_order_return_state_lang` WRITE;
/*!40000 ALTER TABLE `ps_order_return_state_lang` DISABLE KEYS */;
INSERT INTO `ps_order_return_state_lang` VALUES (1,1,'等待確認'),(1,2,'Waiting for confirmation'),(1,3,'Waiting for confirmation'),(1,4,'確認待ち'),(2,1,'等待包裹'),(2,2,'Waiting for package'),(2,3,'Waiting for package'),(2,4,'梱包作業待ち'),(3,1,'包裹已收到'),(3,2,'Package received'),(3,3,'Package received'),(3,4,'受取完了'),(4,1,'退貨被拒絕'),(4,2,'Return denied'),(4,3,'Return denied'),(4,4,'返品拒否'),(5,1,'退貨完成'),(5,2,'Return completed'),(5,3,'Return completed'),(5,4,'返品完了');
/*!40000 ALTER TABLE `ps_order_return_state_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_order_slip`
--

DROP TABLE IF EXISTS `ps_order_slip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_order_slip` (
  `id_order_slip` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `conversion_rate` decimal(13,6) NOT NULL DEFAULT '1.000000',
  `id_customer` int(10) unsigned NOT NULL,
  `id_order` int(10) unsigned NOT NULL,
  `total_products_tax_excl` decimal(20,6) DEFAULT NULL,
  `total_products_tax_incl` decimal(20,6) DEFAULT NULL,
  `total_shipping_tax_excl` decimal(20,6) DEFAULT NULL,
  `total_shipping_tax_incl` decimal(20,6) DEFAULT NULL,
  `shipping_cost` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `amount` decimal(10,2) NOT NULL,
  `shipping_cost_amount` decimal(10,2) NOT NULL,
  `partial` tinyint(1) NOT NULL,
  `order_slip_type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_order_slip`),
  KEY `order_slip_customer` (`id_customer`),
  KEY `id_order` (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_order_slip`
--

LOCK TABLES `ps_order_slip` WRITE;
/*!40000 ALTER TABLE `ps_order_slip` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_slip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_order_slip_detail`
--

DROP TABLE IF EXISTS `ps_order_slip_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_order_slip_detail` (
  `id_order_slip` int(10) unsigned NOT NULL,
  `id_order_detail` int(10) unsigned NOT NULL,
  `product_quantity` int(10) unsigned NOT NULL DEFAULT '0',
  `unit_price_tax_excl` decimal(20,6) DEFAULT NULL,
  `unit_price_tax_incl` decimal(20,6) DEFAULT NULL,
  `total_price_tax_excl` decimal(20,6) DEFAULT NULL,
  `total_price_tax_incl` decimal(20,6) DEFAULT NULL,
  `amount_tax_excl` decimal(20,6) DEFAULT NULL,
  `amount_tax_incl` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`id_order_slip`,`id_order_detail`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_order_slip_detail`
--

LOCK TABLES `ps_order_slip_detail` WRITE;
/*!40000 ALTER TABLE `ps_order_slip_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_slip_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_order_slip_detail_tax`
--

DROP TABLE IF EXISTS `ps_order_slip_detail_tax`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_order_slip_detail_tax` (
  `id_order_slip_detail` int(11) unsigned NOT NULL,
  `id_tax` int(11) unsigned NOT NULL,
  `unit_amount` decimal(16,6) NOT NULL DEFAULT '0.000000',
  `total_amount` decimal(16,6) NOT NULL DEFAULT '0.000000',
  KEY `id_order_slip_detail` (`id_order_slip_detail`),
  KEY `id_tax` (`id_tax`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_order_slip_detail_tax`
--

LOCK TABLES `ps_order_slip_detail_tax` WRITE;
/*!40000 ALTER TABLE `ps_order_slip_detail_tax` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_order_slip_detail_tax` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_order_state`
--

DROP TABLE IF EXISTS `ps_order_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_order_state` (
  `id_order_state` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice` tinyint(1) unsigned DEFAULT '0',
  `send_email` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `module_name` varchar(255) DEFAULT NULL,
  `color` varchar(32) DEFAULT NULL,
  `unremovable` tinyint(1) unsigned NOT NULL,
  `hidden` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `logable` tinyint(1) NOT NULL DEFAULT '0',
  `delivery` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `shipped` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `paid` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pdf_invoice` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `pdf_delivery` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sort` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_order_state`),
  KEY `module_name` (`module_name`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_order_state`
--

LOCK TABLES `ps_order_state` WRITE;
/*!40000 ALTER TABLE `ps_order_state` DISABLE KEYS */;
INSERT INTO `ps_order_state` VALUES (1,0,0,'ps_checkpayment','#FDA729',1,0,0,0,0,0,0,0,0,9),(2,1,0,'','#f37382',1,0,1,0,0,1,1,0,0,31),(3,1,0,'','#f37382',1,0,1,1,0,1,0,0,0,61),(4,1,1,'','#1aa3d9',1,0,1,1,1,1,0,0,0,62),(5,1,0,'','#92c325',1,0,1,1,1,1,0,0,0,64),(6,0,1,'','#666e7f',1,0,0,0,0,0,0,0,0,91),(7,1,1,'','#666e7f',1,0,0,0,0,0,0,0,0,122),(8,0,1,'','#666e7f',1,0,0,0,0,0,0,0,0,92),(9,1,0,'','#f37382',1,0,0,0,0,1,0,0,0,33),(10,0,0,'ps_wirepayment','#FDA729',1,0,0,0,0,0,0,0,0,1),(11,1,0,'','#FDA729',1,0,1,0,0,1,0,0,0,0),(12,0,0,'','#FDA729',1,0,0,0,0,0,0,0,0,9),(13,0,0,'ps_cashondelivery','#8992C4',1,0,0,0,0,0,0,0,0,0),(14,0,0,'smilepay_c2c','#8992C4',0,0,0,0,0,0,0,0,0,8),(15,0,0,'smilepay_ibon','#FDA729',0,0,0,0,0,0,0,0,0,3),(16,0,0,'smilepay_famiport','#FDA729',0,0,0,0,0,0,0,0,0,4),(17,0,0,'smilepay_EZCAT','#8992C4',0,0,0,0,0,0,0,0,0,7),(18,0,0,'smilepay_csv','#FDA729',0,0,0,0,0,0,0,0,0,5),(19,0,0,'smilepay_credit','#FDA729',0,0,0,0,0,0,0,0,0,6),(20,0,0,'smilepay_atm','#FDA729',0,0,0,0,0,0,0,0,0,2),(21,1,0,'','#666e7f',1,0,0,0,0,0,0,0,0,93),(22,1,0,'','#92c325',1,0,0,0,0,0,0,0,0,121),(23,0,0,'','#4169E1',0,0,0,0,0,0,0,0,0,0),(24,0,0,'','#4169E1',0,0,0,0,0,0,0,0,0,0),(25,0,0,'','#4169E1',0,0,0,0,0,0,0,0,0,0),(26,0,0,'','#3b5aa1',1,0,0,0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `ps_order_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_order_state_lang`
--

DROP TABLE IF EXISTS `ps_order_state_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_order_state_lang` (
  `id_order_state` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  `template` varchar(64) NOT NULL,
  PRIMARY KEY (`id_order_state`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_order_state_lang`
--

LOCK TABLES `ps_order_state_lang` WRITE;
/*!40000 ALTER TABLE `ps_order_state_lang` DISABLE KEYS */;
INSERT INTO `ps_order_state_lang` VALUES (1,1,'等待支票付款','cheque'),(1,2,'Awaiting check payment','cheque'),(1,3,'等待支票付款','cheque'),(1,4,'支払い確認待ち','cheque'),(2,1,'付款已被接受','payment'),(2,2,'Payment accepted','payment'),(2,3,'Payment accepted','payment'),(2,4,'支払完了','payment'),(3,1,'準備出貨','preparation'),(3,2,'Processing in progress','preparation'),(3,3,'Processing in progress','preparation'),(3,4,'手続き進行中','preparation'),(4,1,'已出貨','shipped'),(4,2,'Shipped','shipped'),(4,3,'Shipped','shipped'),(4,4,'Shipped','shipped'),(5,1,'已送達',''),(5,2,'Delivered',''),(5,3,'Delivered',''),(5,4,'お届け完了',''),(6,1,'已取消','order_canceled'),(6,2,'Canceled','order_canceled'),(6,3,'Canceled','order_canceled'),(6,4,'キャンセル済','order_canceled'),(7,1,'已退款','refund'),(7,2,'Refund','refund'),(7,3,'退款','refund'),(7,4,'払い戻し','refund'),(8,1,'付款失敗','payment_error'),(8,2,'Payment error','payment_error'),(8,3,'Payment error','payment_error'),(8,4,'支払いエラー','payment_error'),(9,1,'已付款（無庫存）','outofstock'),(9,2,'On backorder (paid)','outofstock'),(9,3,'On backorder (paid)','outofstock'),(9,4,'商品取り寄せ中（支払済）','outofstock'),(10,1,'等待轉帳匯款','bankwire'),(10,2,'Awaiting bank wire payment','bankwire'),(10,3,'Awaiting bank wire payment','bankwire'),(10,4,'銀行振込み待ち','bankwire'),(11,1,'接受遠程付款','payment'),(11,2,'Remote payment accepted','payment'),(11,3,'Remote payment accepted','payment'),(11,4,'リモートペイメント確認済','payment'),(12,1,'等待付款（無庫存）','outofstock'),(12,2,'On backorder (not paid)','outofstock'),(12,3,'On backorder (not paid)','outofstock'),(12,4,'商品取り寄せ中（未払い）','outofstock'),(13,1,'貨到付款 等待驗證','cashondelivery'),(13,2,'Remote payment accepted','cashondelivery'),(13,3,'Remote payment accepted','cashondelivery'),(13,4,'リモートペイメント確認済','cashondelivery'),(14,1,'超商取貨付款 處理中','SmilePay_c2c_status'),(14,2,'超商取貨付款 處理中','SmilePay_c2c_status'),(15,1,'ibon 等待付款','SmilePay_ibon_status'),(15,2,'ibon 等待付款','SmilePay_ibon_status'),(16,1,'FamiPort 等待付款','SmilePay_famiport_status'),(16,2,'FamiPort 等待付款','SmilePay_famiport_status'),(17,1,'黑貓貨到付現 處理中','SmilePay_ezcat_status'),(17,2,'黑貓貨到付現處理中','SmilePay_ezcat_status'),(18,1,'超商條碼 等待付款','SmilePay_csv_status'),(18,2,'超商條碼等待付款','SmilePay_csv_status'),(19,1,'信用卡 付款中','SmilePay_credit_status'),(19,2,'信用卡付款中','SmilePay_credit_status'),(20,1,'ATM 等待付款','SmilePay_atm_status'),(20,2,'ATM 等待付款','SmilePay_atm_status'),(21,1,'等待退貨',''),(21,2,'Return in progress',''),(21,3,'Return in progress',''),(21,4,'Return in progress',''),(22,1,'已歸檔',''),(22,2,'Archived',''),(22,3,'Archived',''),(22,4,'Archived',''),(23,1,'PayPal 等待付款',''),(23,2,'Awaiting for PayPal payment',''),(23,3,'Awaiting for PayPal payment',''),(23,4,'Awaiting for PayPal payment',''),(24,1,'Braintree 等待付款',''),(24,2,'Awaiting for Braintree payment',''),(24,3,'Awaiting for Braintree payment',''),(24,4,'Awaiting for Braintree payment',''),(25,1,'Braintree 等待驗證',''),(25,2,'Awaiting for Braintree validation',''),(25,3,'Awaiting for Braintree validation',''),(25,4,'Awaiting for Braintree validation',''),(26,1,'售完','outofstock'),(26,2,'Sold out','outofstock'),(26,3,'Sold out','outofstock'),(26,4,'Sold out','outofstock');
/*!40000 ALTER TABLE `ps_order_state_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_orders`
--

DROP TABLE IF EXISTS `ps_orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_orders` (
  `id_order` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `reference` varchar(16) DEFAULT NULL,
  `id_shop_group` int(11) unsigned NOT NULL DEFAULT '1',
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_carrier` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `id_customer` int(10) unsigned NOT NULL,
  `id_cart` int(10) unsigned NOT NULL,
  `id_currency` int(10) unsigned NOT NULL,
  `id_address_delivery` int(10) unsigned NOT NULL,
  `id_address_invoice` int(10) unsigned NOT NULL,
  `current_state` int(10) unsigned NOT NULL,
  `secure_key` varchar(32) NOT NULL DEFAULT '-1',
  `payment` varchar(255) NOT NULL,
  `conversion_rate` decimal(13,6) NOT NULL DEFAULT '1.000000',
  `module` varchar(255) DEFAULT NULL,
  `recyclable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `gift` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `gift_message` text,
  `mobile_theme` tinyint(1) NOT NULL DEFAULT '0',
  `shipping_number` varchar(64) DEFAULT NULL,
  `total_discounts` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_discounts_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_discounts_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_paid_real` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_products` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_products_wt` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_shipping_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `carrier_tax_rate` decimal(10,3) NOT NULL DEFAULT '0.000',
  `total_wrapping` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_wrapping_tax_incl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `total_wrapping_tax_excl` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `round_mode` tinyint(1) NOT NULL DEFAULT '2',
  `round_type` tinyint(1) NOT NULL DEFAULT '1',
  `invoice_number` int(10) unsigned NOT NULL DEFAULT '0',
  `delivery_number` int(10) unsigned NOT NULL DEFAULT '0',
  `invoice_date` datetime NOT NULL,
  `delivery_date` datetime NOT NULL,
  `valid` int(1) unsigned NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `smilepayc2ctable` text,
  PRIMARY KEY (`id_order`),
  KEY `reference` (`reference`),
  KEY `id_customer` (`id_customer`),
  KEY `id_cart` (`id_cart`),
  KEY `invoice_number` (`invoice_number`),
  KEY `id_carrier` (`id_carrier`),
  KEY `id_lang` (`id_lang`),
  KEY `id_currency` (`id_currency`),
  KEY `id_address_delivery` (`id_address_delivery`),
  KEY `id_address_invoice` (`id_address_invoice`),
  KEY `id_shop_group` (`id_shop_group`),
  KEY `current_state` (`current_state`),
  KEY `id_shop` (`id_shop`),
  KEY `date_add` (`date_add`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_orders`
--

LOCK TABLES `ps_orders` WRITE;
/*!40000 ALTER TABLE `ps_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_pack`
--

DROP TABLE IF EXISTS `ps_pack`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_pack` (
  `id_product_pack` int(10) unsigned NOT NULL,
  `id_product_item` int(10) unsigned NOT NULL,
  `id_product_attribute_item` int(10) unsigned NOT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_product_pack`,`id_product_item`,`id_product_attribute_item`),
  KEY `product_item` (`id_product_item`,`id_product_attribute_item`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_pack`
--

LOCK TABLES `ps_pack` WRITE;
/*!40000 ALTER TABLE `ps_pack` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_pack` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_page`
--

DROP TABLE IF EXISTS `ps_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_page` (
  `id_page` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_page_type` int(10) unsigned NOT NULL,
  `id_object` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id_page`),
  KEY `id_page_type` (`id_page_type`),
  KEY `id_object` (`id_object`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_page`
--

LOCK TABLES `ps_page` WRITE;
/*!40000 ALTER TABLE `ps_page` DISABLE KEYS */;
INSERT INTO `ps_page` VALUES (1,1,NULL),(2,2,NULL),(3,3,NULL),(4,4,NULL),(5,5,NULL),(6,6,14),(7,7,0),(8,7,2),(9,8,2),(10,9,NULL),(11,6,1),(12,10,NULL),(13,11,NULL),(14,12,NULL),(15,13,NULL),(16,14,NULL),(17,15,NULL),(18,16,NULL),(19,8,23),(20,6,6),(21,17,NULL),(22,6,5),(23,6,15),(24,6,11),(25,8,7),(26,18,NULL),(27,6,8),(28,19,NULL),(29,20,NULL),(30,8,8),(31,8,17),(32,6,13),(33,8,9),(34,8,13);
/*!40000 ALTER TABLE `ps_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_page_type`
--

DROP TABLE IF EXISTS `ps_page_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_page_type` (
  `id_page_type` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_page_type`),
  KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_page_type`
--

LOCK TABLES `ps_page_type` WRITE;
/*!40000 ALTER TABLE `ps_page_type` DISABLE KEYS */;
INSERT INTO `ps_page_type` VALUES (5,'authentication'),(10,'cart'),(8,'category'),(4,'cms'),(14,'contact'),(16,'discount'),(20,'guesttracking'),(11,'history'),(17,'identity'),(1,'index'),(12,'myaccount'),(7,'order'),(15,'orderconfirmation'),(13,'orderdetail'),(3,'pagenotfound'),(18,'pricesdrop'),(6,'product'),(9,'search'),(2,'sitemap'),(19,'viewedproducts');
/*!40000 ALTER TABLE `ps_page_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_page_viewed`
--

DROP TABLE IF EXISTS `ps_page_viewed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_page_viewed` (
  `id_page` int(10) unsigned NOT NULL,
  `id_shop_group` int(10) unsigned NOT NULL DEFAULT '1',
  `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
  `id_date_range` int(10) unsigned NOT NULL,
  `counter` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_page`,`id_date_range`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_page_viewed`
--

LOCK TABLES `ps_page_viewed` WRITE;
/*!40000 ALTER TABLE `ps_page_viewed` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_page_viewed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_pagenotfound`
--

DROP TABLE IF EXISTS `ps_pagenotfound`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_pagenotfound` (
  `id_pagenotfound` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
  `id_shop_group` int(10) unsigned NOT NULL DEFAULT '1',
  `request_uri` varchar(256) NOT NULL,
  `http_referer` varchar(256) NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_pagenotfound`),
  KEY `date_add` (`date_add`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_pagenotfound`
--

LOCK TABLES `ps_pagenotfound` WRITE;
/*!40000 ALTER TABLE `ps_pagenotfound` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_pagenotfound` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_paypal_capture`
--

DROP TABLE IF EXISTS `ps_paypal_capture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_paypal_capture` (
  `id_paypal_capture` int(11) NOT NULL AUTO_INCREMENT,
  `id_capture` varchar(55) DEFAULT NULL,
  `id_paypal_order` int(11) DEFAULT NULL,
  `capture_amount` float DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `date_add` datetime DEFAULT NULL,
  `date_upd` datetime DEFAULT NULL,
  PRIMARY KEY (`id_paypal_capture`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_paypal_capture`
--

LOCK TABLES `ps_paypal_capture` WRITE;
/*!40000 ALTER TABLE `ps_paypal_capture` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_paypal_capture` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_paypal_customer`
--

DROP TABLE IF EXISTS `ps_paypal_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_paypal_customer` (
  `id_paypal_customer` int(11) NOT NULL AUTO_INCREMENT,
  `id_customer` int(11) DEFAULT NULL,
  `reference` varchar(55) DEFAULT NULL,
  `method` varchar(55) DEFAULT NULL,
  `date_add` datetime DEFAULT NULL,
  `date_upd` datetime DEFAULT NULL,
  PRIMARY KEY (`id_paypal_customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_paypal_customer`
--

LOCK TABLES `ps_paypal_customer` WRITE;
/*!40000 ALTER TABLE `ps_paypal_customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_paypal_customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_paypal_order`
--

DROP TABLE IF EXISTS `ps_paypal_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_paypal_order` (
  `id_paypal_order` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) DEFAULT NULL,
  `id_cart` int(11) DEFAULT NULL,
  `id_transaction` varchar(55) DEFAULT NULL,
  `id_payment` varchar(55) DEFAULT NULL,
  `client_token` varchar(255) DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `currency` varchar(21) DEFAULT NULL,
  `total_paid` float DEFAULT NULL,
  `payment_status` varchar(255) DEFAULT NULL,
  `total_prestashop` float DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `payment_tool` varchar(255) DEFAULT NULL,
  `date_add` datetime DEFAULT NULL,
  `date_upd` datetime DEFAULT NULL,
  PRIMARY KEY (`id_paypal_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_paypal_order`
--

LOCK TABLES `ps_paypal_order` WRITE;
/*!40000 ALTER TABLE `ps_paypal_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_paypal_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_paypal_vaulting`
--

DROP TABLE IF EXISTS `ps_paypal_vaulting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_paypal_vaulting` (
  `id_paypal_vaulting` int(11) NOT NULL AUTO_INCREMENT,
  `id_paypal_customer` int(11) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `info` varchar(255) DEFAULT NULL,
  `payment_tool` varchar(255) DEFAULT NULL,
  `date_add` datetime DEFAULT NULL,
  `date_upd` datetime DEFAULT NULL,
  PRIMARY KEY (`id_paypal_vaulting`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_paypal_vaulting`
--

LOCK TABLES `ps_paypal_vaulting` WRITE;
/*!40000 ALTER TABLE `ps_paypal_vaulting` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_paypal_vaulting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_product`
--

DROP TABLE IF EXISTS `ps_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_product` (
  `id_product` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_supplier` int(10) unsigned DEFAULT NULL,
  `id_manufacturer` int(10) unsigned DEFAULT NULL,
  `id_category_default` int(10) unsigned DEFAULT NULL,
  `id_shop_default` int(10) unsigned NOT NULL DEFAULT '1',
  `id_tax_rules_group` int(11) unsigned NOT NULL,
  `on_sale` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `online_only` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ean13` varchar(13) DEFAULT NULL,
  `isbn` varchar(32) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `ecotax` decimal(17,6) NOT NULL DEFAULT '0.000000',
  `quantity` int(10) NOT NULL DEFAULT '0',
  `minimal_quantity` int(10) unsigned NOT NULL DEFAULT '1',
  `low_stock_threshold` int(10) DEFAULT NULL,
  `low_stock_alert` tinyint(1) NOT NULL DEFAULT '0',
  `price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `wholesale_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `unity` varchar(255) DEFAULT NULL,
  `unit_price_ratio` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `additional_shipping_cost` decimal(20,2) NOT NULL DEFAULT '0.00',
  `reference` varchar(32) DEFAULT NULL,
  `supplier_reference` varchar(32) DEFAULT NULL,
  `location` varchar(64) DEFAULT NULL,
  `width` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `height` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `depth` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `weight` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `out_of_stock` int(10) unsigned NOT NULL DEFAULT '2',
  `additional_delivery_times` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `quantity_discount` tinyint(1) DEFAULT '0',
  `customizable` tinyint(2) NOT NULL DEFAULT '0',
  `uploadable_files` tinyint(4) NOT NULL DEFAULT '0',
  `text_fields` tinyint(4) NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `redirect_type` enum('','404','301-product','302-product','301-category','302-category') NOT NULL DEFAULT '',
  `id_type_redirected` int(10) unsigned NOT NULL DEFAULT '0',
  `available_for_order` tinyint(1) NOT NULL DEFAULT '1',
  `available_date` date DEFAULT NULL,
  `show_condition` tinyint(1) NOT NULL DEFAULT '0',
  `condition` enum('new','used','refurbished') NOT NULL DEFAULT 'new',
  `show_price` tinyint(1) NOT NULL DEFAULT '1',
  `indexed` tinyint(1) NOT NULL DEFAULT '0',
  `visibility` enum('both','catalog','search','none') NOT NULL DEFAULT 'both',
  `cache_is_pack` tinyint(1) NOT NULL DEFAULT '0',
  `cache_has_attachments` tinyint(1) NOT NULL DEFAULT '0',
  `is_virtual` tinyint(1) NOT NULL DEFAULT '0',
  `cache_default_attribute` int(10) unsigned DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `advanced_stock_management` tinyint(1) NOT NULL DEFAULT '0',
  `pack_stock_type` int(11) unsigned NOT NULL DEFAULT '3',
  `state` int(11) unsigned NOT NULL DEFAULT '1',
  `sticker` varchar(32) NOT NULL COMMENT '貼紙',
  PRIMARY KEY (`id_product`),
  KEY `product_supplier` (`id_supplier`),
  KEY `product_manufacturer` (`id_manufacturer`,`id_product`),
  KEY `id_category_default` (`id_category_default`),
  KEY `indexed` (`indexed`),
  KEY `date_add` (`date_add`),
  KEY `state` (`state`,`date_upd`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_product`
--

LOCK TABLES `ps_product` WRITE;
/*!40000 ALTER TABLE `ps_product` DISABLE KEYS */;
INSERT INTO `ps_product` VALUES (1,0,0,13,1,0,0,0,'','','',0.000000,0,1,NULL,0,10000.000000,0.000000,'本',0.000000,0.00,'','','',0.000000,0.000000,0.000000,0.000000,2,1,0,0,0,0,1,'404',0,1,'0000-00-00',0,'new',1,1,'both',0,0,0,0,'2018-06-17 15:18:52','2018-11-18 00:33:44',0,3,1,''),(2,0,0,13,1,0,0,0,'','','',0.000000,0,1,NULL,0,23100.000000,0.000000,'',0.000000,0.00,'','','',0.000000,0.000000,0.000000,0.000000,2,1,0,0,0,0,1,'404',0,1,'0000-00-00',0,'new',1,1,'both',0,0,0,6,'2018-06-17 18:32:17','2018-07-29 19:20:06',0,3,1,''),(3,0,0,13,1,0,0,0,'','','',0.000000,0,1,NULL,0,1500.000000,0.000000,'',0.000000,0.00,'','','',0.000000,0.000000,0.000000,0.000000,2,1,0,0,0,0,1,'404',0,1,'0000-00-00',0,'new',1,1,'both',0,0,0,0,'2018-06-17 18:35:12','2018-09-23 17:47:21',0,3,1,''),(4,0,0,13,1,0,0,0,'','','',0.000000,0,1,NULL,0,10000.000000,0.000000,'',0.000000,0.00,'','','',0.000000,0.000000,0.000000,0.000000,2,1,0,0,0,0,1,'404',0,1,'0000-00-00',0,'new',1,1,'both',0,0,0,0,'2018-06-17 18:35:23','2018-09-23 17:47:32',0,3,1,''),(5,0,0,13,1,0,0,0,'','','',0.000000,0,1,NULL,0,1000000.000000,0.000000,'',0.000000,0.00,'','','',0.000000,0.000000,0.000000,0.000000,2,1,0,0,0,0,0,'404',0,1,'0000-00-00',0,'new',1,0,'both',0,0,0,0,'2018-06-17 18:38:03','2018-12-09 23:10:31',0,3,1,''),(6,0,0,9,1,0,0,0,'','','',0.000000,0,1,NULL,0,50.000000,0.000000,'',0.000000,0.00,'','','',0.000000,0.000000,0.000000,0.000000,2,1,0,0,0,0,1,'404',0,1,'0000-00-00',0,'new',1,1,'both',0,0,0,0,'2018-06-17 18:38:11','2018-09-23 17:47:42',0,3,1,''),(8,0,0,9,1,0,0,0,'','','',0.000000,0,5,NULL,0,20.000000,0.000000,'',0.000000,0.00,'','','',0.000000,0.000000,0.000000,0.000000,2,1,0,0,0,0,1,'404',0,1,'0000-00-00',0,'new',1,1,'both',0,0,0,0,'2018-06-17 18:38:21','2018-11-10 11:58:02',0,3,1,''),(9,0,0,10,1,0,0,0,'','','',0.000000,0,1,NULL,0,180.000000,0.000000,'',0.000000,0.00,'','','',0.000000,0.000000,0.000000,0.000000,2,1,0,0,0,0,1,'404',0,1,'0000-00-00',0,'new',1,1,'both',0,0,0,0,'2018-06-17 18:38:25','2018-09-23 17:47:52',0,3,1,''),(10,0,0,10,1,0,0,0,'','','',0.000000,0,1,NULL,0,180.000000,0.000000,'',0.000000,0.00,'','','',0.000000,0.000000,0.000000,0.000000,2,1,0,0,0,0,1,'404',0,1,'0000-00-00',0,'new',1,1,'both',0,0,0,0,'2018-06-17 18:38:30','2018-09-23 17:48:01',0,3,1,''),(11,0,0,10,1,0,0,0,'','','',0.000000,0,1,NULL,0,300.000000,0.000000,'',0.000000,0.00,'','','',0.000000,0.000000,0.000000,0.000000,2,1,0,0,0,0,1,'404',0,1,'0000-00-00',0,'new',1,1,'both',0,0,0,0,'2018-06-17 18:38:37','2018-11-15 18:37:09',0,3,1,''),(12,0,0,10,1,0,0,0,'','','',0.000000,0,1,NULL,0,150.000000,0.000000,'',0.000000,0.00,'T123456-789(b)','','',0.000000,0.000000,0.000000,0.000000,2,1,0,1,1,1,1,'404',0,1,'2018-11-30',1,'used',1,1,'both',0,0,0,0,'2018-06-17 18:38:42','2018-10-09 01:01:01',0,3,1,''),(13,0,0,11,1,0,0,0,'','','',0.000000,0,1,NULL,0,1800.000000,0.000000,'',0.000000,0.00,'','','',0.000000,0.000000,0.000000,0.000000,2,1,0,0,0,0,1,'404',0,1,'2018-06-17',0,'new',1,1,'both',0,0,0,3,'2018-06-17 18:38:47','2018-07-29 13:51:10',0,3,1,''),(14,0,0,12,1,0,0,0,'','','',0.000000,0,1,NULL,0,1800.000000,0.000000,'',0.000000,0.00,'','','',0.000000,0.000000,0.000000,0.000000,2,1,0,0,0,0,1,'404',0,1,'0000-00-00',0,'new',1,1,'both',0,0,1,0,'2018-09-14 19:30:23','2019-01-21 23:12:40',0,3,1,''),(15,0,0,12,1,0,0,0,'','','',0.000000,0,1,NULL,0,50000.000000,0.000000,'',0.000000,0.00,'','','',0.000000,0.000000,0.000000,0.000000,2,1,0,0,0,0,1,'404',0,1,'0000-00-00',1,'new',1,1,'both',0,0,0,9,'2018-09-14 19:30:23','2019-01-06 22:18:42',0,3,1,''),(16,0,0,2,1,0,0,0,'','','',0.000000,0,1,NULL,0,0.000000,0.000000,'',0.000000,0.00,'','','',0.000000,0.000000,0.000000,0.000000,2,1,0,0,0,0,0,'',0,1,'0000-00-00',0,'new',1,0,'both',0,0,0,0,'2019-01-23 00:09:27','2019-01-23 00:09:27',0,3,0,'');
/*!40000 ALTER TABLE `ps_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_product_attachment`
--

DROP TABLE IF EXISTS `ps_product_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_product_attachment` (
  `id_product` int(10) unsigned NOT NULL,
  `id_attachment` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_product`,`id_attachment`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_product_attachment`
--

LOCK TABLES `ps_product_attachment` WRITE;
/*!40000 ALTER TABLE `ps_product_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_product_attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_product_attribute`
--

DROP TABLE IF EXISTS `ps_product_attribute`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_product_attribute` (
  `id_product_attribute` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_product` int(10) unsigned NOT NULL,
  `reference` varchar(32) DEFAULT NULL,
  `supplier_reference` varchar(32) DEFAULT NULL,
  `location` varchar(64) DEFAULT NULL,
  `ean13` varchar(13) DEFAULT NULL,
  `isbn` varchar(32) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `wholesale_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `ecotax` decimal(17,6) NOT NULL DEFAULT '0.000000',
  `quantity` int(10) NOT NULL DEFAULT '0',
  `weight` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `unit_price_impact` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `default_on` tinyint(1) unsigned DEFAULT NULL,
  `minimal_quantity` int(10) unsigned NOT NULL DEFAULT '1',
  `low_stock_threshold` int(10) DEFAULT NULL,
  `low_stock_alert` tinyint(1) NOT NULL DEFAULT '0',
  `available_date` date DEFAULT NULL,
  PRIMARY KEY (`id_product_attribute`),
  UNIQUE KEY `product_default` (`id_product`,`default_on`),
  KEY `product_attribute_product` (`id_product`),
  KEY `reference` (`reference`),
  KEY `supplier_reference` (`supplier_reference`),
  KEY `id_product_id_product_attribute` (`id_product_attribute`,`id_product`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_product_attribute`
--

LOCK TABLES `ps_product_attribute` WRITE;
/*!40000 ALTER TABLE `ps_product_attribute` DISABLE KEYS */;
INSERT INTO `ps_product_attribute` VALUES (1,13,'','','','','','',0.000000,0.000000,0.000000,0,0.000000,0.000000,NULL,1,NULL,0,'0000-00-00'),(2,13,'','','','','','',0.000000,0.000000,0.000000,0,0.000000,0.000000,NULL,1,NULL,0,'0000-00-00'),(3,13,'','','','','','',0.000000,0.000000,0.000000,0,0.000000,0.000000,1,1,NULL,0,'0000-00-00'),(4,13,'','','','','','',0.000000,0.000000,0.000000,0,0.000000,0.000000,NULL,1,NULL,0,'0000-00-00'),(5,13,'','','','','','',0.000000,0.000000,0.000000,0,0.000000,0.000000,NULL,1,NULL,0,'0000-00-00'),(6,2,'','','','','','',0.000000,230.000000,0.000000,0,0.000000,0.000000,1,1,NULL,0,'0000-00-00'),(7,2,'','','','','','',0.000000,2300.000000,0.000000,0,0.000000,0.000000,NULL,1,NULL,0,'0000-00-00'),(8,2,'','','','','','',0.000000,23100.000000,0.000000,0,0.000000,0.000000,NULL,1,NULL,0,'0000-00-00'),(9,15,'','','','','','',0.000000,50.000000,0.000000,0,0.000000,0.000000,1,1,NULL,0,'0000-00-00'),(10,15,'','','','','','',0.000000,0.000000,0.000000,0,0.000000,0.000000,NULL,1,NULL,0,'0000-00-00'),(11,15,'','','','','','',0.000000,100.000000,0.000000,0,0.000000,0.000000,NULL,1,NULL,0,'0000-00-00'),(12,15,'','','','','','',0.000000,0.000000,0.000000,0,0.000000,0.000000,NULL,1,NULL,0,'0000-00-00');
/*!40000 ALTER TABLE `ps_product_attribute` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_product_attribute_combination`
--

DROP TABLE IF EXISTS `ps_product_attribute_combination`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_product_attribute_combination` (
  `id_attribute` int(10) unsigned NOT NULL,
  `id_product_attribute` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_attribute`,`id_product_attribute`),
  KEY `id_product_attribute` (`id_product_attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_product_attribute_combination`
--

LOCK TABLES `ps_product_attribute_combination` WRITE;
/*!40000 ALTER TABLE `ps_product_attribute_combination` DISABLE KEYS */;
INSERT INTO `ps_product_attribute_combination` VALUES (1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,8),(9,9),(10,10),(11,11),(12,12);
/*!40000 ALTER TABLE `ps_product_attribute_combination` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_product_attribute_image`
--

DROP TABLE IF EXISTS `ps_product_attribute_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_product_attribute_image` (
  `id_product_attribute` int(10) unsigned NOT NULL,
  `id_image` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_product_attribute`,`id_image`),
  KEY `id_image` (`id_image`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_product_attribute_image`
--

LOCK TABLES `ps_product_attribute_image` WRITE;
/*!40000 ALTER TABLE `ps_product_attribute_image` DISABLE KEYS */;
INSERT INTO `ps_product_attribute_image` VALUES (9,20),(9,33),(12,34),(10,35),(11,36);
/*!40000 ALTER TABLE `ps_product_attribute_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_product_attribute_shop`
--

DROP TABLE IF EXISTS `ps_product_attribute_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_product_attribute_shop` (
  `id_product` int(10) unsigned NOT NULL,
  `id_product_attribute` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `wholesale_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `ecotax` decimal(17,6) NOT NULL DEFAULT '0.000000',
  `weight` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `unit_price_impact` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `default_on` tinyint(1) unsigned DEFAULT NULL,
  `minimal_quantity` int(10) unsigned NOT NULL DEFAULT '1',
  `low_stock_threshold` int(10) DEFAULT NULL,
  `low_stock_alert` tinyint(1) NOT NULL DEFAULT '0',
  `available_date` date DEFAULT NULL,
  PRIMARY KEY (`id_product_attribute`,`id_shop`),
  UNIQUE KEY `id_product` (`id_product`,`id_shop`,`default_on`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_product_attribute_shop`
--

LOCK TABLES `ps_product_attribute_shop` WRITE;
/*!40000 ALTER TABLE `ps_product_attribute_shop` DISABLE KEYS */;
INSERT INTO `ps_product_attribute_shop` VALUES (13,1,1,0.000000,0.000000,0.000000,0.000000,0.000000,NULL,1,NULL,0,'0000-00-00'),(13,2,1,0.000000,0.000000,0.000000,0.000000,0.000000,NULL,1,NULL,0,'0000-00-00'),(13,3,1,0.000000,0.000000,0.000000,0.000000,0.000000,1,1,NULL,0,'0000-00-00'),(13,4,1,0.000000,0.000000,0.000000,0.000000,0.000000,NULL,1,NULL,0,'0000-00-00'),(13,5,1,0.000000,0.000000,0.000000,0.000000,0.000000,NULL,1,NULL,0,'0000-00-00'),(2,6,1,0.000000,230.000000,0.000000,0.000000,0.000000,1,1,NULL,0,'0000-00-00'),(2,7,1,0.000000,2300.000000,0.000000,0.000000,0.000000,NULL,1,NULL,0,'0000-00-00'),(2,8,1,0.000000,23100.000000,0.000000,0.000000,0.000000,NULL,1,NULL,0,'0000-00-00'),(15,9,1,0.000000,50.000000,0.000000,0.000000,0.000000,1,1,NULL,0,'0000-00-00'),(15,10,1,0.000000,0.000000,0.000000,0.000000,0.000000,NULL,1,NULL,0,'0000-00-00'),(15,11,1,0.000000,100.000000,0.000000,0.000000,0.000000,NULL,1,NULL,0,'0000-00-00'),(15,12,1,0.000000,0.000000,0.000000,0.000000,0.000000,NULL,1,NULL,0,'0000-00-00');
/*!40000 ALTER TABLE `ps_product_attribute_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_product_carrier`
--

DROP TABLE IF EXISTS `ps_product_carrier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_product_carrier` (
  `id_product` int(10) unsigned NOT NULL,
  `id_carrier_reference` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_product`,`id_carrier_reference`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_product_carrier`
--

LOCK TABLES `ps_product_carrier` WRITE;
/*!40000 ALTER TABLE `ps_product_carrier` DISABLE KEYS */;
INSERT INTO `ps_product_carrier` VALUES (2,1,1);
/*!40000 ALTER TABLE `ps_product_carrier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_product_country_tax`
--

DROP TABLE IF EXISTS `ps_product_country_tax`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_product_country_tax` (
  `id_product` int(11) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_tax` int(11) NOT NULL,
  PRIMARY KEY (`id_product`,`id_country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_product_country_tax`
--

LOCK TABLES `ps_product_country_tax` WRITE;
/*!40000 ALTER TABLE `ps_product_country_tax` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_product_country_tax` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_product_download`
--

DROP TABLE IF EXISTS `ps_product_download`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_product_download` (
  `id_product_download` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_product` int(10) unsigned NOT NULL,
  `display_filename` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `date_add` datetime NOT NULL,
  `date_expiration` datetime DEFAULT NULL,
  `nb_days_accessible` int(10) unsigned DEFAULT NULL,
  `nb_downloadable` int(10) unsigned DEFAULT '1',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `is_shareable` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_product_download`),
  UNIQUE KEY `id_product` (`id_product`),
  KEY `product_active` (`id_product`,`active`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_product_download`
--

LOCK TABLES `ps_product_download` WRITE;
/*!40000 ALTER TABLE `ps_product_download` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_product_download` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_product_group_reduction_cache`
--

DROP TABLE IF EXISTS `ps_product_group_reduction_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_product_group_reduction_cache` (
  `id_product` int(10) unsigned NOT NULL,
  `id_group` int(10) unsigned NOT NULL,
  `reduction` decimal(4,3) NOT NULL,
  PRIMARY KEY (`id_product`,`id_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_product_group_reduction_cache`
--

LOCK TABLES `ps_product_group_reduction_cache` WRITE;
/*!40000 ALTER TABLE `ps_product_group_reduction_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_product_group_reduction_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_product_lang`
--

DROP TABLE IF EXISTS `ps_product_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_product_lang` (
  `id_product` int(10) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_lang` int(10) unsigned NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `description_short` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `link_rewrite` varchar(128) NOT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_title` varchar(128) DEFAULT NULL,
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `available_now` varchar(255) DEFAULT NULL,
  `available_later` varchar(255) DEFAULT NULL,
  `delivery_in_stock` varchar(255) DEFAULT NULL,
  `delivery_out_stock` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_product`,`id_shop`,`id_lang`),
  KEY `id_lang` (`id_lang`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_product_lang`
--

LOCK TABLES `ps_product_lang` WRITE;
/*!40000 ALTER TABLE `ps_product_lang` DISABLE KEYS */;
INSERT INTO `ps_product_lang` VALUES (1,1,1,'<p>Reproduction published in 2010 <br /> First edition of 20<br /> Full color digital offset printing by Chyaulun with HP indigo 5000 (Hp electrolnk)<br /> 130 gsm white fine art paper - inner page<br /> 187.5 kg Egyptian gold Zipang metallic paper - cover<br /> Black uncoated woodfree paper - cover<br /> Section sewn Coptic binding with exposed spine, larger hole and flat back cover by Yoshosu<br /> 15 cm W x 21 cm H <br /> 143 pages + cover</p>\n<p>複製畫冊於2010年出版<br /> 初版20本<br /> 全彩 HP indigo 數位平版印刷 印刷者僑倫<br /> 厚磅數美術紙內頁，埃及金ジパング紙封面，內襯黑色道林紙<br /> 科普特裝訂 裝訂者盧亭璇<br /> 寬 15 公分 x 長 21 公分<br /> 143頁內頁裸露書背，並有較大的洞口使疊頁之間間隔較窄 ，外加方背封面</p>\n<p>ISBN 978-957-41-7331-0</p>\n<p></p>\n<p><span>In Collection</span> <span>典藏</span></p>\n<ul>\n<li><span>National Central Library 國家圖書館 - Taipei, Taiwan <br /> </span></li>\n<li><span>Taipei National University of the Arts Library 國立臺北藝術大學圖書館 - Taipei, Taiwan <br /> </span></li>\n<li><span>Bank Street Arts - Sheffield, UK <br /> </span></li>\n<li><span>Private Collectors</span></li>\n</ul>\n<p><span>Related Fair</span> <span>展會</span></p>\n<ul>\n<li><span>2015，「Liuyingchieh Books : CODEX V 國際雙年展」，Craneway Pavilion，Richmond，CA，USA</span></li>\n<li><span>2017，「Liuyingchieh Books : CODEX VI 國際雙年展」，Craneway Pavilion，Richmond，CA，USA</span></li>\n</ul>\n<p><span>Related Group Exhibition</span> <span>聯展</span></p>\n<ul>\n<li><span>2013，「4th Sheffield International Artist\'s Book Prize and Exhibition 第四屆雪菲爾國際藝術家的書評選展」，Bank Street Arts，Sheffield，英國</span></li>\n</ul>\n<p><span>Related Solo Exhibition</span> <span>個展</span></p>\n<ul>\n<li><span>2012，「移動記之旅」，清大藝術工坊，新竹，台灣 </span></li>\n<li><span>2014，「Torres del Paine 2004-2014」，書香醒園藝術空間，台北，台灣</span></li>\n<li><span>2016，「移動記─藝術家的書」，國立台灣圖書館雙和藝廊，中和，台灣</span></li>\n</ul>','','travelbook1','','','','《移動記 I ：Venezia，台灣》','','','',''),(1,1,2,'<p>Published in September, 2010.<br />Reproduction of a hand drawn book.<br /><br />Jan.-Feb. 2002, I went to Venice for 2 weeks. That was the first time I stayed in a youth hostel. I was shocked, when I saw bathroom without door curtain in female dorm, when I saw naked body between bedroom and bathroom.<br /><br />I started to draw on this book since September 2002, the beginning of this book is about my memories from Venice youth hostel at Giudecca, and a blonde hair roommate.<br /><br />After several monthes, I started to draw my travel diaries/road comics/road movies book when I traveled around Taiwan by train: Fangliao , Pinghsi - Sandiaoling - Yilan - Hualien - Chingshui Cliff - Alishan ... (枋寮，平溪 - 三貂嶺 - 宜蘭 - 花蓮 - 清水斷崖 - 阿里山 ...)<br /><br />Statement:<br /><br />Interested in preserving the duration of memory, I use books to be the carrier of my creations; through the characteristic of page flipping, showing the movement of time and space, resulting in continuous dynamic imagination.<br /><br />The motivation of my travel is to fulfill the image creation mode with books as the carrier, through the moving of the real body, trying to let my real life imitate my duration of memory.<br /><br />On the road, I opened my book and accumulated my strokes page by page. Pages carry the fragments of time, page flipping creates the flux of becoming, books show the duration of the entire life.<br /><br />\"Creative journey\" is really intended to let the external journey imitate the internal imagination. However, the state of travel itself also lets the creator in contact with unexpected strong stimulation. Experience of these incidents will not disappear overnight. After a long period of precipitation, memories of the last trip will become the base of the next trip; reality and imagination will imitate and promote each other.<br /><br />Through the mode of growth and stacking, repetition and extension to structure the image. Intend to show a spiritual depth instead of the virtual image of three-dimensional distance. Frames in the pages show the sequences of time and space; montage the complex, staggered time and space to express dreams and memories.<br /><br />The binding forms of the carrier become the visual elements throughout the entire artwork; the spread of the book is the moment now, the left pages link to the past and the right pages undertake the future.<br /><br />熱衷於保存記憶的綿延狀態，於是選擇以書冊為創作載體；透過書頁的翻頁特質，呈現時空的轉移，造成連續性的動態想像。<br /><br />旅行的動機是為了成全以書冊為載體的圖像創作模式，透過身體的真正移動，讓真實生活模仿記憶的綿延。<br /><br />在旅途中，翻開書冊，逐頁累積筆觸。書頁承載了片段時刻，翻頁造成變化之流，書冊呈現生命整體的綿延狀態。<br /><br />「創作之旅」確實意圖讓外部旅程模仿內部想像。然而，旅行狀態本身，同時也讓創作者不斷接觸意料之外的強烈刺激。這些意外的經歷並不會隔夜就消失；經過長時間沈澱，上一段旅行的回憶，成為下一段旅行的基底，現實與想像互相模仿、交互推進。<br /><br />透過生長與堆疊、重複與延伸的模式來結構畫面。意圖以心靈空間深度取代三度空間距離的虛像。書頁中的分格，呈現對於純粹繪畫過於複雜的時空序列；以蒙太奇手法並置複雜、交錯的時空，表現夢境、回憶的特質。<br /><br />創作載體之裝訂形式，成為貫穿整本書冊的視覺元素；書冊被攤開的雙頁即是當下，對於左翻書而言，左頁聯繫著過去，右頁承接未來。<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />劉穎捷 2002 2003 = 移動記 Travel Book I （Venezia, 台灣）<br /><br />Memories： Venice<br />*Youth hostel<br />*Bedroom（Dorm bed）<br />*Bathroom（Without door）<br />*Traveler<br />*Roomate（Blonde hair）<br />*Body（Naked）<br />*Wave<br />*Gondola<br /><br />移動記 Road book（旅行寫生 Travel sketch）：台灣 Taiwan<br />*親吻岩 Kissing Stone（鵝鑾鼻 Eluanbi）<br />*從枋寮上車之後 Fangliao<br />*松山車站→平溪線 Pinghsi Line（綠樹）<br />*三貂嶺車站 Sandiaoling Train Station→宜蘭 Yilan（藍海）<br />*第411號列車 Ordinary express train No.411（Stop running）<br />*花蓮 Hualien：一步又一步，接近清水斷崖之中<br />*幸福水泥．和仁 Heren<br />*清水斷崖 Chingshui Cliff（Moonrise）<br />*太平洋日出 Pacific Ocean : Sunrise<br />*奮起湖 Fenqihu（阿里山小火車 Alishan Railway）<br />*花東縱谷 East Rift Valley<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />Full color digital lithography printing by HP indigo 5000.<br />Coptic binding with special hole and flat back cover(hand bound by book artist Yoshosu, 9 signatures, section sewn, open flat).<br />130 gsm white fine art paper.<br />187.5 kg Egyptian gold Zipang(ジパング) metallic paper.<br />Black uncoated woodfree paper.<br />14.8 cm x 21 cm.<br />143 pages + cover.<br />Limited first edition of 20, numbered. This listing is for № 14.<br /><br />ISBN 978-957-41-7331-0<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />In Collection 典藏<br />◆ National Central Library 國家圖書館 - Taipei, Taiwan<br />◆ Taipei National University of the Arts Library 國立臺北藝術大學圖書館 - Taipei, Taiwan<br />◆ Bank Street Arts - Sheffield, UK<br />◆ Private Collectors<br /><br />Related Fair 展會<br />◆ 2015，「Liuyingchieh Books : CODEX V 國際雙年展」，Craneway Pavilion，Richmond，CA，USA<br />◆ 2017，「Liuyingchieh Books : CODEX VI 國際雙年展」，Craneway Pavilion，Richmond，CA，USA<br /><br />Related Group Exhibition 聯展<br />◆ 2013，「4th Sheffield International Artist\'s Book Prize and Exhibition 第四屆雪菲爾國際藝術家的書評選展」，Bank Street Arts，Sheffield，英國<br /><br />Related Solo Exhibition 個展<br />◆ 2012，「移動記之旅」，清大藝術工坊，新竹，台灣<br />◆ 2014，「Torres del Paine 2004-2014」，書香醒園藝術空間，台北，台灣<br />◆ 2016，「移動記─藝術家的書」，國立台灣圖書館雙和藝廊，中和，台灣<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />Original artwork remains the property of Ying-Chieh Liu and the buyer is not entitled to reproduction rights.<br /><br />© Ying-Chieh Liu</p>','','travelbook1','','','','Travel Book I : Venice, Taiwan','','','',''),(1,1,3,'<p>Reproduction published in 2010 <br /> First edition of 20<br /> Full color digital offset printing by Chyaulun with HP indigo 5000 (Hp electrolnk)<br /> 130 gsm white fine art paper - inner page<br /> 187.5 kg Egyptian gold Zipang metallic paper - cover<br /> Black uncoated woodfree paper - cover<br /> Section sewn Coptic binding with exposed spine, larger hole and flat back cover by Yoshosu<br /> 15 cm W x 21 cm H <br /> 143 pages + cover</p>\n<p>複製画册于2010年出版<br /> 初版20本<br /> 全彩 HP indigo 数位平版印刷 印刷者侨伦<br /> 厚磅数美术纸内页，埃及金ジパング纸封面，内衬黑色道林纸<br /> 科普特装订 装订者卢亭璇<br /> 宽 15 公分 x 长 21 公分<br /> 143页内页裸露书背，并有较大的洞口使叠页之间间隔较窄 ，外加方背封面</p>\n<p>ISBN 978-957-41-7331-0</p>\n<p></p>\n<p><span>In Collection</span> <span>典藏</span></p>\n<ul>\n<li><span>National Central Library 国家图书馆 - Taipei, Taiwan <br /> </span></li>\n<li><span>Taipei National University of the Arts Library 国立臺北艺术大学图书馆 - Taipei, Taiwan <br /> </span></li>\n<li><span>Bank Street Arts - Sheffield, UK <br /> </span></li>\n<li><span>Private Collectors</span></li>\n</ul>\n<p><span>Related Fair</span> <span>展会</span></p>\n<ul>\n<li><span>2015，「Liuyingchieh Books : CODEX V 国际双年展」，Craneway Pavilion，Richmond，CA，USA</span></li>\n<li><span>2017，「Liuyingchieh Books : CODEX VI 国际双年展」，Craneway Pavilion，Richmond，CA，USA</span></li>\n</ul>\n<p><span>Related Group Exhibition</span> <span>联展</span></p>\n<ul>\n<li><span>2013，「4th Sheffield International Artist\'s Book Prize and Exhibition 第四届雪菲尔国际艺术家的书评选展」，Bank Street Arts，Sheffield，英国</span></li>\n</ul>\n<p><span>Related Solo Exhibition</span> <span>个展</span></p>\n<ul>\n<li><span>2012，「移动记之旅」，清大艺术工坊，新竹，台湾 </span></li>\n<li><span>2014，「Torres del Paine 2004-2014」，书香醒园艺术空间，台北，台湾</span></li>\n<li><span>2016，「移动记─艺术家的书」，国立台湾图书馆双和艺廊，中和，台湾</span></li>\n</ul>','','travelbook1','','','','《移动记 I ：Venezia，台湾》','','','',''),(1,1,4,'<p>Published in September, 2010.<br />Reproduction of a hand drawn book.<br /><br />Jan.-Feb. 2002, I went to Venice for 2 weeks. That was the first time I stayed in a youth hostel. I was shocked, when I saw bathroom without door curtain in female dorm, when I saw naked body between bedroom and bathroom.<br /><br />I started to draw on this book since September 2002, the beginning of this book is about my memories from Venice youth hostel at Giudecca, and a blonde hair roommate.<br /><br />After several monthes, I started to draw my travel diaries/road comics/road movies book when I traveled around Taiwan by train: Fangliao , Pinghsi - Sandiaoling - Yilan - Hualien - Chingshui Cliff - Alishan ... (枋寮，平溪 - 三貂嶺 - 宜蘭 - 花蓮 - 清水斷崖 - 阿里山 ...)<br /><br />Statement:<br /><br />Interested in preserving the duration of memory, I use books to be the carrier of my creations; through the characteristic of page flipping, showing the movement of time and space, resulting in continuous dynamic imagination.<br /><br />The motivation of my travel is to fulfill the image creation mode with books as the carrier, through the moving of the real body, trying to let my real life imitate my duration of memory.<br /><br />On the road, I opened my book and accumulated my strokes page by page. Pages carry the fragments of time, page flipping creates the flux of becoming, books show the duration of the entire life.<br /><br />\"Creative journey\" is really intended to let the external journey imitate the internal imagination. However, the state of travel itself also lets the creator in contact with unexpected strong stimulation. Experience of these incidents will not disappear overnight. After a long period of precipitation, memories of the last trip will become the base of the next trip; reality and imagination will imitate and promote each other.<br /><br />Through the mode of growth and stacking, repetition and extension to structure the image. Intend to show a spiritual depth instead of the virtual image of three-dimensional distance. Frames in the pages show the sequences of time and space; montage the complex, staggered time and space to express dreams and memories.<br /><br />The binding forms of the carrier become the visual elements throughout the entire artwork; the spread of the book is the moment now, the left pages link to the past and the right pages undertake the future.<br /><br />熱衷於保存記憶的綿延狀態，於是選擇以書冊為創作載體；透過書頁的翻頁特質，呈現時空的轉移，造成連續性的動態想像。<br /><br />旅行的動機是為了成全以書冊為載體的圖像創作模式，透過身體的真正移動，讓真實生活模仿記憶的綿延。<br /><br />在旅途中，翻開書冊，逐頁累積筆觸。書頁承載了片段時刻，翻頁造成變化之流，書冊呈現生命整體的綿延狀態。<br /><br />「創作之旅」確實意圖讓外部旅程模仿內部想像。然而，旅行狀態本身，同時也讓創作者不斷接觸意料之外的強烈刺激。這些意外的經歷並不會隔夜就消失；經過長時間沈澱，上一段旅行的回憶，成為下一段旅行的基底，現實與想像互相模仿、交互推進。<br /><br />透過生長與堆疊、重複與延伸的模式來結構畫面。意圖以心靈空間深度取代三度空間距離的虛像。書頁中的分格，呈現對於純粹繪畫過於複雜的時空序列；以蒙太奇手法並置複雜、交錯的時空，表現夢境、回憶的特質。<br /><br />創作載體之裝訂形式，成為貫穿整本書冊的視覺元素；書冊被攤開的雙頁即是當下，對於左翻書而言，左頁聯繫著過去，右頁承接未來。<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />劉穎捷 2002 2003 = 移動記 Travel Book I （Venezia, 台灣）<br /><br />Memories： Venice<br />*Youth hostel<br />*Bedroom（Dorm bed）<br />*Bathroom（Without door）<br />*Traveler<br />*Roomate（Blonde hair）<br />*Body（Naked）<br />*Wave<br />*Gondola<br /><br />移動記 Road book（旅行寫生 Travel sketch）：台灣 Taiwan<br />*親吻岩 Kissing Stone（鵝鑾鼻 Eluanbi）<br />*從枋寮上車之後 Fangliao<br />*松山車站→平溪線 Pinghsi Line（綠樹）<br />*三貂嶺車站 Sandiaoling Train Station→宜蘭 Yilan（藍海）<br />*第411號列車 Ordinary express train No.411（Stop running）<br />*花蓮 Hualien：一步又一步，接近清水斷崖之中<br />*幸福水泥．和仁 Heren<br />*清水斷崖 Chingshui Cliff（Moonrise）<br />*太平洋日出 Pacific Ocean : Sunrise<br />*奮起湖 Fenqihu（阿里山小火車 Alishan Railway）<br />*花東縱谷 East Rift Valley<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />Full color digital lithography printing by HP indigo 5000.<br />Coptic binding with special hole and flat back cover(hand bound by book artist Yoshosu, 9 signatures, section sewn, open flat).<br />130 gsm white fine art paper.<br />187.5 kg Egyptian gold Zipang(ジパング) metallic paper.<br />Black uncoated woodfree paper.<br />14.8 cm x 21 cm.<br />143 pages + cover.<br />Limited first edition of 20, numbered. This listing is for № 14.<br /><br />ISBN 978-957-41-7331-0<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />In Collection 典藏<br />◆ National Central Library 國家圖書館 - Taipei, Taiwan<br />◆ Taipei National University of the Arts Library 國立臺北藝術大學圖書館 - Taipei, Taiwan<br />◆ Bank Street Arts - Sheffield, UK<br />◆ Private Collectors<br /><br />Related Fair 展會<br />◆ 2015，「Liuyingchieh Books : CODEX V 國際雙年展」，Craneway Pavilion，Richmond，CA，USA<br />◆ 2017，「Liuyingchieh Books : CODEX VI 國際雙年展」，Craneway Pavilion，Richmond，CA，USA<br /><br />Related Group Exhibition 聯展<br />◆ 2013，「4th Sheffield International Artist\'s Book Prize and Exhibition 第四屆雪菲爾國際藝術家的書評選展」，Bank Street Arts，Sheffield，英國<br /><br />Related Solo Exhibition 個展<br />◆ 2012，「移動記之旅」，清大藝術工坊，新竹，台灣<br />◆ 2014，「Torres del Paine 2004-2014」，書香醒園藝術空間，台北，台灣<br />◆ 2016，「移動記─藝術家的書」，國立台灣圖書館雙和藝廊，中和，台灣<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />Original artwork remains the property of Ying-Chieh Liu and the buyer is not entitled to reproduction rights.<br /><br />© Ying-Chieh Liu</p>','','travelbook1','','','','Travel Book I : Venice, Taiwan','','','',''),(2,1,1,'','','travelbook4','','','','移動記四《The Significant Travel》','','','',''),(2,1,2,'','','travelbook4','','','','移動記四《The Significant Travel》','','','',''),(2,1,3,'','','travelbook4','','','','移動記四《The Significant Travel》','','','',''),(2,1,4,'','','travelbook4','','','','移動記四《The Significant Travel》','','','',''),(3,1,1,'','','travelbook11','','','','移動記十一《海．洋》','','','',''),(3,1,2,'','','travelbook11','','','','移動記十一《海．洋》','','','',''),(3,1,3,'','','travelbook11','','','','移動記十一《海．洋》','','','',''),(3,1,4,'','','travelbook11','','','','移動記十一《海．洋》','','','',''),(4,1,1,'','','travelbook12','','','','移動記十二《山脈》','','','',''),(4,1,2,'<p><span style=\"font-family:\'-apple-system\', \'system-ui\', \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif;font-size:14px;\">手工縫製一本隨身畫冊，在路上邊走邊畫。</span><br style=\"font-family:\'-apple-system\', \'system-ui\', \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif;font-size:14px;\" /><br style=\"font-family:\'-apple-system\', \'system-ui\', \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif;font-size:14px;\" /><span style=\"font-family:\'-apple-system\', \'system-ui\', \'Segoe UI\', Roboto, Helvetica, Arial, sans-serif;font-size:14px;\">書冊體積小，輕便易攜帶，能收取到繪畫者面對大自然時最感動的線條和筆觸；其頁面接續頁面的結構易於表述時間，呈現宛如記憶綿延的連續動態想像。書冊需要近觀、徒手翻頁的特質，則提供了較為私密、親近人身體的感官體驗。</span></p>','','travelbook12','','','','移動記十二《山脈》','','','',''),(4,1,3,'','','travelbook12','','','','移動記十二《山脈》','','','',''),(4,1,4,'','','travelbook12','','','','移動記十二《山脈》','','','',''),(5,1,1,'<br />','<br />','地球山水冊頁ㄌㄏㄌㄑㄏㄌ','','','','地球山水冊頁—ㄌㄏㄌㄑㄏㄌ#','','','',''),(5,1,2,'','','earthlandscapealbum','','','','地球山水冊頁—ㄌㄏㄌㄑㄏㄌ','','','',''),(5,1,3,'','','link','','','','','','','',''),(5,1,4,'','','link','','','','','','','',''),(6,1,1,'<p>Travel Sketch : 2015 清水斷崖 Ching-Shui Cliff Taiwan<br />from Travel Book 11 《海‧洋》 (\"O-cean\")<br /><br />-------------------------------------<br /><br />240g 象牙卡精印<br />105 x 148 mm<br />燙金+燙銀<br /><br />240g Ivory card.<br />105 x 148 mm.<br />Full color offset printing.<br />Gold hot foil stamping.<br />Silver hot foil stamping.<br /><br />-------------------------------------<br /><br />© Ying-Chieh Liu<br />產地/製造方式<br />台灣 手工製作</p>','','goldfoil','','','','清水斷崖（燙金）','','','',''),(6,1,2,'<p>Set of 4 Postcards = Travel Sketches from Travel Book 11 《海．洋》 (\"O-cean\")<br /><br />清水斷崖 Ching-Shui Cliff <br />&amp;<br />七星潭 Chihsingtan Beach<br />@<br />Hualien Taiwan<br /><br />240g Ivory card and metallic card.<br />105 x 148 mm.<br />Full color offset printing.<br />Gold hot foil stamping.<br />Silver hot foil stamping.<br /><br />© Ying-Chieh Liu</p>','','goldfoil','','','','清水斷崖（燙金）','','','',''),(6,1,3,'<p>Travel Sketch : 2015 清水断崖 Ching-Shui Cliff Taiwan<br />from Travel Book 11 《海‧洋》 (\"O-cean\")<br /><br />-------------------------------------<br /><br />240g 象牙卡精印<br />105 x 148 mm<br />烫金+烫银<br /><br />240g Ivory card.<br />105 x 148 mm.<br />Full color offset printing.<br />Gold hot foil stamping.<br />Silver hot foil stamping.<br /><br />-------------------------------------<br /><br />© Ying-Chieh Liu<br />产地/製造方式<br />台湾 手工製作</p>','','goldfoil','','','','清水斷崖（燙金）','','','',''),(6,1,4,'<p>Set of 4 Postcards = Travel Sketches from Travel Book 11 《海．洋》 (\"O-cean\")<br /><br />清水斷崖 Ching-Shui Cliff <br />&amp;<br />七星潭 Chihsingtan Beach<br />@<br />Hualien Taiwan<br /><br />240g Ivory card and metallic card.<br />105 x 148 mm.<br />Full color offset printing.<br />Gold hot foil stamping.<br />Silver hot foil stamping.<br /><br />© Ying-Chieh Liu</p>','','goldfoil','','','','清水斷崖（燙金）','','','',''),(8,1,1,'<p>?<br />Travel Sketches from Travel Book 11 《海．洋》 (\"O-cean\")<br /><br />清水斷崖 Ching-Shui Cliff <br />&amp;<br />七星潭 Chihsingtan Beach<br />@<br />Hualien Taiwan<br /><br />240g Ivory card and metallic card.<br />105 x 148 mm.<br />Full color offset printing.<br />Gold hot foil stamping.<br />Silver hot foil stamping.<br /><br />© Ying-Chieh Liu</p>','','ivorycard','','','','七星潭','','','',''),(8,1,2,'<p>Travel Sketches from Travel Book 11 《海．洋》 (\"O-cean\")<br /><br />清水斷崖 Ching-Shui Cliff <br />&amp;<br />七星潭 Chihsingtan Beach<br />@<br />Hualien Taiwan<br /><br />240g Ivory card and metallic card.<br />105 x 148 mm.<br />Full color offset printing.<br />Gold hot foil stamping.<br />Silver hot foil stamping.<br /><br />© Ying-Chieh Liu</p>','','ivorycard','','','','七星潭','','','',''),(8,1,3,'<p>Travel Sketches from Travel Book 11 《海．洋》 (\"O-cean\")<br /><br />清水斷崖 Ching-Shui Cliff <br />&amp;<br />七星潭 Chihsingtan Beach<br />@<br />Hualien Taiwan<br /><br />240g Ivory card and metallic card.<br />105 x 148 mm.<br />Full color offset printing.<br />Gold hot foil stamping.<br />Silver hot foil stamping.<br /><br />© Ying-Chieh Liu</p>','','七星潭','','','','七星潭','','','',''),(8,1,4,'<p>Travel Sketches from Travel Book 11 《海．洋》 (\"O-cean\")<br /><br />清水斷崖 Ching-Shui Cliff <br />&amp;<br />七星潭 Chihsingtan Beach<br />@<br />Hualien Taiwan<br /><br />240g Ivory card and metallic card.<br />105 x 148 mm.<br />Full color offset printing.<br />Gold hot foil stamping.<br />Silver hot foil stamping.<br /><br />© Ying-Chieh Liu</p>','','七星潭','','','','七星潭','','','',''),(9,1,1,'<p>^___^<br /><br />NEW! SEALED! ONE OF A KIND!<br /><br />Designed from my original drawing / artist\'s book \"Travel Book 11 : O-cean\" （《海．洋》） in 2014, a spread of a travel sketch with pen and ink, in Chishingtan Beach, Hualien, Taiwan （七星潭海灘，花蓮）, a rocky pebble beach with clear waves face to the Pacific Ocean.<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />Taiwanese Washi paper<br /><br />1 roll = 25 mm wide × 10 m long (10 cm loop)<br /><br />Second edition of 66 (June 2017 printed by Yuanpao)<br /><br />Made in Taiwan<br /><br />✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿<br /><br />First edition of 30 printed in July, 2016, by Tain, Taiwan.<br />Second edition of 66 printed in June, 2017, by Yuanpao, Taiwan.<br /><br />© Ying-Chieh Liu</p>','','ocean','','','','海．洋','','','',''),(9,1,2,'<p>^___^<br /><br />NEW! SEALED! ONE OF A KIND!<br /><br />Designed from my original drawing / artist\'s book \"Travel Book 11 : O-cean\" （《海．洋》） in 2014, a spread of a travel sketch with pen and ink, in Chishingtan Beach, Hualien, Taiwan （七星潭海灘，花蓮）, a rocky pebble beach with clear waves face to the Pacific Ocean.<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />Taiwanese Washi paper<br /><br />1 roll = 25 mm wide × 10 m long (10 cm loop)<br /><br />Second edition of 66 (June 2017 printed by Yuanpao)<br /><br />Made in Taiwan<br /><br />✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿<br /><br />First edition of 30 printed in July, 2016, by Tain, Taiwan.<br />Second edition of 66 printed in June, 2017, by Yuanpao, Taiwan.<br /><br />© Ying-Chieh Liu</p>','','ocean','','','','O-cean','','','',''),(9,1,3,'<p>^___^<br /><br />NEW! SEALED! ONE OF A KIND!<br /><br />Designed from my original drawing / artist\'s book \"Travel Book 11 : O-cean\" （《海．洋》） in 2014, a spread of a travel sketch with pen and ink, in Chishingtan Beach, Hualien, Taiwan （七星潭海滩，花莲）, a rocky pebble beach with clear waves face to the Pacific Ocean.<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />Taiwanese Washi paper<br /><br />1 roll = 25 mm wide × 10 m long (10 cm loop)<br /><br />Second edition of 66 (June 2017 printed by Yuanpao)<br /><br />Made in Taiwan<br /><br />✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿<br /><br />First edition of 30 printed in July, 2016, by Tain, Taiwan.<br />Second edition of 66 printed in June, 2017, by Yuanpao, Taiwan.<br /><br />© Ying-Chieh Liu</p>','','ocean','','','','海．洋','','','',''),(9,1,4,'<p>^___^<br /><br />NEW! SEALED! ONE OF A KIND!<br /><br />Designed from my original drawing / artist\'s book \"Travel Book 11 : O-cean\" （《海．洋》） in 2014, a spread of a travel sketch with pen and ink, in Chishingtan Beach, Hualien, Taiwan （七星潭海灘，花蓮）, a rocky pebble beach with clear waves face to the Pacific Ocean.<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />Taiwanese Washi paper<br /><br />1 roll = 25 mm wide × 10 m long (10 cm loop)<br /><br />Second edition of 66 (June 2017 printed by Yuanpao)<br /><br />Made in Taiwan<br /><br />✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿<br /><br />First edition of 30 printed in July, 2016, by Tain, Taiwan.<br />Second edition of 66 printed in June, 2017, by Yuanpao, Taiwan.<br /><br />© Ying-Chieh Liu</p>','','ocean','','','','O-cean','','','',''),(10,1,1,'<p>^___^<br /><br />NEW! SEALED! ONE OF A KIND!<br /><br />Designed from my original drawing / artist\'s book \"Travel Book 11 : O-cean\" （《海．洋》） in 2014, a spread of a travel sketch with pen and ink, in Chishingtan Beach, Hualien, Taiwan （七星潭海灘，花蓮）, a rocky pebble beach with clear waves face to the Pacific Ocean.<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />Japanese Washi paper<br /><br />Set of 2 Rolls :<br />1 roll = 25 mm wide × 10 m long (10 cm loop)<br /><br />First edition of 30 (July 2016 printed by Tain)<br /><br />Made in Taiwan<br /><br />✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿<br /><br />First edition of 30 printed in July, 2016, by Tain, Taiwan.<br /><br />© Ying-Chieh Liu</p>','','wave','','','','海．洋：淼','','','',''),(10,1,2,'<p>^___^<br /><br />NEW! SEALED! ONE OF A KIND!<br /><br />Designed from my original drawing / artist\'s book \"Travel Book 11 : O-cean\" （《海．洋》） in 2014, a spread of a travel sketch with pen and ink, in Chishingtan Beach, Hualien, Taiwan （七星潭海灘，花蓮）, a rocky pebble beach with clear waves face to the Pacific Ocean.<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />Japanese Washi paper<br /><br />Set of 2 Rolls :<br />1 roll = 25 mm wide × 10 m long (10 cm loop)<br /><br />First edition of 30 (July 2016 printed by Tain)<br /><br />Made in Taiwan<br /><br />✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿<br /><br />First edition of 30 printed in July, 2016, by Tain, Taiwan.<br /><br />© Ying-Chieh Liu</p>','','wave','','','','O-cean','','','',''),(10,1,3,'<p>^___^<br /><br />NEW! SEALED! ONE OF A KIND!<br /><br />Designed from my original drawing / artist\'s book \"Travel Book 11 : O-cean\" （《海．洋》） in 2014, a spread of a travel sketch with pen and ink, in Chishingtan Beach, Hualien, Taiwan （七星潭海灘，花蓮）, a rocky pebble beach with clear waves face to the Pacific Ocean.<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />Japanese Washi paper<br /><br />Set of 2 Rolls :<br />1 roll = 25 mm wide × 10 m long (10 cm loop)<br /><br />First edition of 30 (July 2016 printed by Tain)<br /><br />Made in Taiwan<br /><br />✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿<br /><br />First edition of 30 printed in July, 2016, by Tain, Taiwan.<br /><br />© Ying-Chieh Liu</p>','','wave','','','','海．洋：淼','','','',''),(10,1,4,'<p>^___^<br /><br />NEW! SEALED! ONE OF A KIND!<br /><br />Designed from my original drawing / artist\'s book \"Travel Book 11 : O-cean\" （《海．洋》） in 2014, a spread of a travel sketch with pen and ink, in Chishingtan Beach, Hualien, Taiwan （七星潭海灘，花蓮）, a rocky pebble beach with clear waves face to the Pacific Ocean.<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />Japanese Washi paper<br /><br />Set of 2 Rolls :<br />1 roll = 25 mm wide × 10 m long (10 cm loop)<br /><br />First edition of 30 (July 2016 printed by Tain)<br /><br />Made in Taiwan<br /><br />✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿<br /><br />First edition of 30 printed in July, 2016, by Tain, Taiwan.<br /><br />© Ying-Chieh Liu</p>','','wave','','','','O-cean','','','',''),(11,1,1,'<p>內心山水交錯台灣風景寫生和紙膠帶，<br />繪畫內容來自 Liuyingchieh 原創作品<br />移動記系列之四《The Significant Travel》 <a class=\"m-richtext-el m-richtext-link\" href=\"https://www.pinkoi.com/product/1Y432PEL\" target=\"_blank\">www.pinkoi.com/product/1Y432PEL</a><br /><br />山水手卷卷首卷尾無縫接軌無限循環~<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />✐寫生地點:<br />台灣 (九份/陽明山冷水坑/花蓮/三芝淺水灣/新竹內灣/故宮博物院/書桌前)<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />使用台灣和紙精印<br /><br />初版108捲 每捲編號 (2016.9原寶承印)<br /><br />一捲 = 45 mm 寬 x 10 m 長 (90 cm 循環)<br /><br />Made in Taiwan<br /><br />© Ying-Chieh Liu<br />產地/製造方式<br />台灣</p>','','significanttravel45','','','','The Significant Travel (45mm)','','','',''),(11,1,2,'<p>^___^<br /><br />NEW! SEALED! ONE OF A KIND!<br /><br />Taiwanese landscape + Inner landscape masking tape from Liuyingchieh\'s original painting Travel Book Series 4 《The Significant Travel》 : <a href=\"http://www.etsy.com/listing/57177974/the-significant-travel-travel-sketchbook\" target=\"_blank\">http://www.etsy.com/listing/57177974</a><br /><br />山水手卷卷首卷尾無縫接軌無限循環~<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />✐寫生地點：<br />九份<br />陽明山冷水坑<br />花蓮<br />三芝淺水灣<br />新竹內灣<br />故宮博物院<br />書桌前<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />Taiwanese Washi paper<br /><br />1 roll = 45 mm wide × 10 m long (90 cm loop)<br /><br />First edition of 108 (September 2016 printed by Yuanpao)<br /><br />Made in Taiwan<br /><br />✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿<br /><br />First edition of 108 printed in September, 2016, by Yuanpao, Taiwan.<br /><br />© Ying-Chieh Liu</p>','','significanttravel45','','','','The Significant Travel (45mm)','','','',''),(11,1,3,'<p>^___^<br /><br />NEW! SEALED! ONE OF A KIND!<br /><br />Taiwanese landscape + Inner landscape masking tape from Liuyingchieh\'s original painting Travel Book Series 4 《The Significant Travel》 : <a href=\"http://www.etsy.com/listing/57177974/the-significant-travel-travel-sketchbook\" target=\"_blank\">http://www.etsy.com/listing/57177974</a><br /><br />山水手卷卷首卷尾無縫接軌無限循環~<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />✐寫生地點：<br />九份<br />陽明山冷水坑<br />花蓮<br />三芝淺水灣<br />新竹內灣<br />故宮博物院<br />書桌前<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />Taiwanese Washi paper<br /><br />1 roll = 45 mm wide × 10 m long (90 cm loop)<br /><br />First edition of 108 (September 2016 printed by Yuanpao)<br /><br />Made in Taiwan<br /><br />✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿<br /><br />First edition of 108 printed in September, 2016, by Yuanpao, Taiwan.<br /><br />© Ying-Chieh Liu</p>','','significanttravel45','','','','The Significant Travel (45mm)','','','',''),(11,1,4,'<p>^___^<br /><br />NEW! SEALED! ONE OF A KIND!<br /><br />Taiwanese landscape + Inner landscape masking tape from Liuyingchieh\'s original painting Travel Book Series 4 《The Significant Travel》 : <a href=\"http://www.etsy.com/listing/57177974/the-significant-travel-travel-sketchbook\" target=\"_blank\">http://www.etsy.com/listing/57177974</a><br /><br />山水手卷卷首卷尾無縫接軌無限循環~<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />✐寫生地點：<br />九份<br />陽明山冷水坑<br />花蓮<br />三芝淺水灣<br />新竹內灣<br />故宮博物院<br />書桌前<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />Taiwanese Washi paper<br /><br />1 roll = 45 mm wide × 10 m long (90 cm loop)<br /><br />First edition of 108 (September 2016 printed by Yuanpao)<br /><br />Made in Taiwan<br /><br />✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿<br /><br />First edition of 108 printed in September, 2016, by Yuanpao, Taiwan.<br /><br />© Ying-Chieh Liu</p>','','significanttravel45','','','','The Significant Travel (45mm)','','','',''),(12,1,1,'<p>^___^<br /><br />NEW! SEALED! ONE OF A KIND!<br /><br />Taiwanese landscape + Inner landscape masking tape from Liuyingchieh\'s original painting Travel Book Series 4 《The Significant Travel》 : <a href=\"http://www.etsy.com/listing/57177974/the-significant-travel-travel-sketchbook\" target=\"_blank\">http://www.etsy.com/listing/57177974</a><br /><br />山水手卷卷首卷尾無縫接軌無限循環~<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />✐寫生地點：<br />九份<br />陽明山冷水坑<br />花蓮<br />三芝淺水灣<br />新竹內灣<br />故宮博物院<br />書桌前<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />Taiwanese Washi paper<br /><br />1 roll = 15 mm wide × 10 m long (30 cm loop)<br /><br />Second edition of 200* (July 2016 printed by Yuanpao)<br /><br />*each one numbered on the back round sticker<br /><br />Made in Taiwan<br /><br />✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿<br /><br />First edition of 20 printed in December, 2015, by Dergi, Taiwan. (sold out)<br />Second edition of 200 printed in July, 2016, by Yuanpao, Taiwan.<br /><br />© Ying-Chieh Liu</p>','','significanttravel15','','','','The Significant Travel (15mm)','','此商品為預購商品','',''),(12,1,2,'<p>^___^<br /><br />NEW! SEALED! ONE OF A KIND!<br /><br />Taiwanese landscape + Inner landscape masking tape from Liuyingchieh\'s original painting Travel Book Series 4 《The Significant Travel》 : <a href=\"http://www.etsy.com/listing/57177974/the-significant-travel-travel-sketchbook\" target=\"_blank\">http://www.etsy.com/listing/57177974</a><br /><br />山水手卷卷首卷尾無縫接軌無限循環~<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />✐寫生地點：<br />九份<br />陽明山冷水坑<br />花蓮<br />三芝淺水灣<br />新竹內灣<br />故宮博物院<br />書桌前<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />Taiwanese Washi paper<br /><br />1 roll = 15 mm wide × 10 m long (30 cm loop)<br /><br />Second edition of 200* (July 2016 printed by Yuanpao)<br /><br />*each one numbered on the back round sticker<br /><br />Made in Taiwan<br /><br />✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿<br /><br />First edition of 20 printed in December, 2015, by Dergi, Taiwan. (sold out)<br />Second edition of 200 printed in July, 2016, by Yuanpao, Taiwan.<br /><br />© Ying-Chieh Liu</p>','','significanttravel15','','','','The Significant Travel (15mm)','','','',''),(12,1,3,'<p>^___^<br /><br />NEW! SEALED! ONE OF A KIND!<br /><br />Taiwanese landscape + Inner landscape masking tape from Liuyingchieh\'s original painting Travel Book Series 4 《The Significant Travel》 : <a href=\"http://www.etsy.com/listing/57177974/the-significant-travel-travel-sketchbook\" target=\"_blank\">http://www.etsy.com/listing/57177974</a><br /><br />山水手卷卷首卷尾無縫接軌無限循環~<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />✐寫生地點：<br />九份<br />陽明山冷水坑<br />花蓮<br />三芝淺水灣<br />新竹內灣<br />故宮博物院<br />書桌前<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />Taiwanese Washi paper<br /><br />1 roll = 15 mm wide × 10 m long (30 cm loop)<br /><br />Second edition of 200* (July 2016 printed by Yuanpao)<br /><br />*each one numbered on the back round sticker<br /><br />Made in Taiwan<br /><br />✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿<br /><br />First edition of 20 printed in December, 2015, by Dergi, Taiwan. (sold out)<br />Second edition of 200 printed in July, 2016, by Yuanpao, Taiwan.<br /><br />© Ying-Chieh Liu</p>','','significanttravel15','','','','The Significant Travel (15mm)','','','',''),(12,1,4,'<p>^___^<br /><br />NEW! SEALED! ONE OF A KIND!<br /><br />Taiwanese landscape + Inner landscape masking tape from Liuyingchieh\'s original painting Travel Book Series 4 《The Significant Travel》 : <a href=\"http://www.etsy.com/listing/57177974/the-significant-travel-travel-sketchbook\" target=\"_blank\">http://www.etsy.com/listing/57177974</a><br /><br />山水手卷卷首卷尾無縫接軌無限循環~<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />✐寫生地點：<br />九份<br />陽明山冷水坑<br />花蓮<br />三芝淺水灣<br />新竹內灣<br />故宮博物院<br />書桌前<br /><br />▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░<br /><br />Taiwanese Washi paper<br /><br />1 roll = 15 mm wide × 10 m long (30 cm loop)<br /><br />Second edition of 200* (July 2016 printed by Yuanpao)<br /><br />*each one numbered on the back round sticker<br /><br />Made in Taiwan<br /><br />✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿°º°✿¨¨¨¨¨¨✿<br /><br />First edition of 20 printed in December, 2015, by Dergi, Taiwan. (sold out)<br />Second edition of 200 printed in July, 2016, by Yuanpao, Taiwan.<br /><br />© Ying-Chieh Liu</p>','','significanttravel15','','','','The Significant Travel (15mm)','','','',''),(13,1,1,'<p>:▏ :▏ :▏:▏ Travel Light ╳ Liuyingchieh :▏ :▏ :▏ :▏吸濕排汗彈性機能褲<br />材質：88 % 聚酯纖維〔輕量吸濕排汗原紗（moisture wicking fabric）〕＋ 12 % 彈性纖維〔四向彈力（4 way stretch）〕、高品質無毒數位熱昇華墨水<br /><br /><img class=\"m-richtext-el m-richtext-img\" src=\"https://img1.etsystatic.com/133/0/5326321/il_fullxfull.1032940305_c4rw.jpg\" alt=\"il_fullxfull.1032940305_c4rw.jpg\" /><br />室內戶外運動 ? 輕裝旅行 ? 日常穿著 ? 均適宜<br />　⚡⚡⚡⚡⚡　保護你不感冒，滿身大汗也不怕吹風～㊣ <br /><img class=\"m-richtext-el m-richtext-img\" src=\"https://img1.etsystatic.com/137/0/5326321/il_fullxfull.1071529753_1oiq.jpg\" alt=\"il_fullxfull.1071529753_1oiq.jpg\" /><br />獨一無二的布料圖案來自 Liuyingchieh 書冊繪畫作品<br />移動記十一《海 ‧ 洋》 2015 清水斷崖匯德遊憩區寫生 <br />寫生地點是在蘇花公路匯德步道<br /><img class=\"m-richtext-el m-richtext-img\" src=\"https://img0.etsystatic.com/115/0/5326321/il_fullxfull.1055945788_79fo.jpg\" alt=\"il_fullxfull.1055945788_79fo.jpg\" /><br /><br />瑜珈動作示範照片提供： 玩瑜珈教室 © Frankie Wang<br />好榮幸【清水斷崖瑜珈褲】可以陪伴瑜珈高手做出高難度動作！<br /><br /><br /><br />:▏ :▏ :▏:▏ Travel Light ╳ Liuyingchieh :▏ :▏ :▏ :▏<br />清水斷崖 吸濕排汗彈性機能褲<br />產地/製造方式<br />台灣設計　㊣　加拿大製作</p>','','yoga','','','','清水斷崖','','','',''),(13,1,2,'<p>Summer 2016.<br /><br />For sport, dancing, yoga, spinning, outdoor, running, hiking, travel light, daily use.<br /><br />88% polyester 12% spandex performance wear fabric, moisture wicking, 4 way stretch, anti microbial.<br />Eco-friendly non-toxic high quality digital sublimation printed.<br />Made to order for your size, XS, S, M, L, XL.<br /><br />:▏ :▏ :▏ :▏ Travel Light ╳ Liuyingchieh :▏ :▏ :▏ :▏<br /><br />Designed in Taiwan - Manufactured in Canada<br /><br />Designed from my original artwork. The one of a kind pattern on the fabric is from my original drawing / artist\'s book \"Travel Book 11 : O-cean\" （《海．洋》） in 2015, a spread of a travel sketch with pen and ink, in Ching-Shui Cliff, Hualien, Taiwan （清水斷崖，花蓮）, a series of mountain coastal cliffs with rocky pebble beach and clear waves face to the Pacific Ocean.<br /><br />Top Photo by 玩瑜珈教室 © Frankie Wang<br /><br />© Ying-Chieh Liu</p>','','yoga','','','','清水斷崖','','','',''),(13,1,3,'<p>:▏ :▏ :▏:▏ Travel Light ╳ Liuyingchieh :▏ :▏ :▏ :▏吸湿排汗弹性机能裤<br />材质：88 % 聚酯纤维〔轻量吸湿排汗原纱（moisture wicking fabric）〕＋ 12 % 弹性纤维〔四向弹力（4 way stretch）〕、高品质无毒数位热昇华墨水<br /><br /><img class=\"m-richtext-el m-richtext-img\" src=\"https://img1.etsystatic.com/133/0/5326321/il_fullxfull.1032940305_c4rw.jpg\" alt=\"il_fullxfull.1032940305_c4rw.jpg\" /><br />室内户外运动 ? 轻装旅行 ? 日常穿着 ? 均适宜<br />　⚡⚡⚡⚡⚡　保护你不感冒，满身大汗也不怕吹风～㊣ <br /><img class=\"m-richtext-el m-richtext-img\" src=\"https://img1.etsystatic.com/137/0/5326321/il_fullxfull.1071529753_1oiq.jpg\" alt=\"il_fullxfull.1071529753_1oiq.jpg\" /><br />独一无二的布料图案来自 Liuyingchieh 书册绘画作品<br />移动记十一《海 ‧ 洋》 2015 清水断崖汇德游憩区写生 <br />写生地点是在苏花公路汇德步道<br /><img class=\"m-richtext-el m-richtext-img\" src=\"https://img0.etsystatic.com/115/0/5326321/il_fullxfull.1055945788_79fo.jpg\" alt=\"il_fullxfull.1055945788_79fo.jpg\" /><br /><br />瑜珈动作示范照片提供： 玩瑜珈教室 © Frankie Wang<br />好荣幸【清水断崖瑜珈裤】可以陪伴瑜珈高手做出高难度动作！<br /><br /><br /><br />:▏ :▏ :▏:▏ Travel Light ╳ Liuyingchieh :▏ :▏ :▏ :▏<br />清水断崖 吸湿排汗弹性机能裤<br />产地/製造方式<br />台湾设计　㊣　加拿大製作</p>','','yoga','','','','清水斷崖','','','',''),(13,1,4,'<p>Summer 2016.<br /><br />For sport, dancing, yoga, spinning, outdoor, running, hiking, travel light, daily use.<br /><br />88% polyester 12% spandex performance wear fabric, moisture wicking, 4 way stretch, anti microbial.<br />Eco-friendly non-toxic high quality digital sublimation printed.<br />Made to order for your size, XS, S, M, L, XL.<br /><br />:▏ :▏ :▏ :▏ Travel Light ╳ Liuyingchieh :▏ :▏ :▏ :▏<br /><br />Designed in Taiwan - Manufactured in Canada<br /><br />Designed from my original artwork. The one of a kind pattern on the fabric is from my original drawing / artist\'s book \"Travel Book 11 : O-cean\" （《海．洋》） in 2015, a spread of a travel sketch with pen and ink, in Ching-Shui Cliff, Hualien, Taiwan （清水斷崖，花蓮）, a series of mountain coastal cliffs with rocky pebble beach and clear waves face to the Pacific Ocean.<br /><br />Top Photo by 玩瑜珈教室 © Frankie Wang<br /><br />© Ying-Chieh Liu</p>','','yoga','','','','清水斷崖','','','',''),(14,1,1,'<p>銀 / Ying / 戒 / Chieh / #153<br />〈海．洋〉 (O-cean)<br /><br />Inspired by the ocean, the eastern coast of Taiwan. Design from my original drawing / artist\'s book \"Travel Book 11 : O-cean\" （《海．洋》） in 2014, a spread of a travel sketch with pen and ink, in Chishingtan Beach, Hualien, Taiwan （七星潭海灘，花蓮）, a rocky pebble beach with clear waves face to the Pacific Ocean.<br /><br />3D Printed + Handmade Original = Each One Slightly Different<br /><br />Integrally formed, sturdy, for daily use purpose.<br /><br />Ring size: <br />US 4<br /><br />Band width:<br />2~3 mm<br /><br />Sterling silver (925) * + ☆　★<br /><br />© Ying-Chieh Liu</p>\n<p>銀 / Ying / 戒 / Chieh / #153<br />〈海．洋〉 (O-cean)<br /><br />Inspired by the ocean, the eastern coast of Taiwan. Design from my original drawing / artist\'s book \"Travel Book 11 : O-cean\" （《海．洋》） in 2014, a spread of a travel sketch with pen and ink, in Chishingtan Beach, Hualien, Taiwan （七星潭海灘，花蓮）, a rocky pebble beach with clear waves face to the Pacific Ocean.<br /><br />3D Printed + Handmade Original = Each One Slightly Different<br /><br />Integrally formed, sturdy, for daily use purpose.<br /><br />Ring size: <br />US 4<br /><br />Band width:<br />2~3 mm<br /><br />Sterling silver (925) * + ☆　★<br /><br />© Ying-Chieh Liu</p>','','silver','','','','海．洋','','','',''),(14,1,2,'<p>銀 / Ying / 戒 / Chieh / #153<br />〈海．洋〉 (O-cean)<br /><br />Inspired by the ocean, the eastern coast of Taiwan. Design from my original drawing / artist\'s book \"Travel Book 11 : O-cean\" （《海．洋》） in 2014, a spread of a travel sketch with pen and ink, in Chishingtan Beach, Hualien, Taiwan （七星潭海灘，花蓮）, a rocky pebble beach with clear waves face to the Pacific Ocean.<br /><br />3D Printed + Handmade Original = Each One Slightly Different<br /><br />Integrally formed, sturdy, for daily use purpose.<br /><br />Ring size: <br />US 4<br /><br />Band width:<br />2~3 mm<br /><br />Sterling silver (925) * + ☆　★<br /><br />© Ying-Chieh Liu</p>','','silver','','','','O-cean','','','',''),(14,1,3,'<p>銀 / Ying / 戒 / Chieh / #153<br />〈海．洋〉 (O-cean)<br /><br />Inspired by the ocean, the eastern coast of Taiwan. Design from my original drawing / artist\'s book \"Travel Book 11 : O-cean\" （《海．洋》） in 2014, a spread of a travel sketch with pen and ink, in Chishingtan Beach, Hualien, Taiwan （七星潭海灘，花蓮）, a rocky pebble beach with clear waves face to the Pacific Ocean.<br /><br />3D Printed + Handmade Original = Each One Slightly Different<br /><br />Integrally formed, sturdy, for daily use purpose.<br /><br />Ring size: <br />US 4<br /><br />Band width:<br />2~3 mm<br /><br />Sterling silver (925) * + ☆　★<br /><br />© Ying-Chieh Liu</p>','','silver','','','','海．洋','','','',''),(14,1,4,'<p>銀 / Ying / 戒 / Chieh / #153<br />〈海．洋〉 (O-cean)<br /><br />Inspired by the ocean, the eastern coast of Taiwan. Design from my original drawing / artist\'s book \"Travel Book 11 : O-cean\" （《海．洋》） in 2014, a spread of a travel sketch with pen and ink, in Chishingtan Beach, Hualien, Taiwan （七星潭海灘，花蓮）, a rocky pebble beach with clear waves face to the Pacific Ocean.<br /><br />3D Printed + Handmade Original = Each One Slightly Different<br /><br />Integrally formed, sturdy, for daily use purpose.<br /><br />Ring size: <br />US 4<br /><br />Band width:<br />2~3 mm<br /><br />Sterling silver (925) * + ☆　★<br /><br />© Ying-Chieh Liu</p>','','silver','','','','O-cean','','','',''),(15,1,1,'','?','enamel','','','','海．洋（琺瑯）','','','',''),(15,1,2,'','','enamel','','','','海．洋（琺瑯）','','','',''),(15,1,3,'','','enamel','','','','海．洋（琺瑯）','','','',''),(15,1,4,'','','enamel','','','','海．洋（琺瑯）','','','',''),(16,1,1,'','','','','','','','','','',''),(16,1,2,'','','','','','','','','','',''),(16,1,3,'','','','','','','','','','',''),(16,1,4,'','','','','','','','','','',''),(16,2,1,'','','','','','','','','','',''),(16,2,2,'','','','','','','','','','',''),(16,2,3,'','','','','','','','','','',''),(16,2,4,'','','','','','','','','','',''),(16,3,1,'','','','','','','','','','',''),(16,3,2,'','','','','','','','','','',''),(16,3,3,'','','','','','','','','','',''),(16,3,4,'','','','','','','','','','',''),(16,4,1,'','','','','','','','','','',''),(16,4,2,'','','','','','','','','','',''),(16,4,3,'','','','','','','','','','',''),(16,4,4,'','','','','','','','','','','');
/*!40000 ALTER TABLE `ps_product_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_product_sale`
--

DROP TABLE IF EXISTS `ps_product_sale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_product_sale` (
  `id_product` int(10) unsigned NOT NULL,
  `quantity` int(10) unsigned NOT NULL DEFAULT '0',
  `sale_nbr` int(10) unsigned NOT NULL DEFAULT '0',
  `date_upd` date DEFAULT NULL,
  PRIMARY KEY (`id_product`),
  KEY `quantity` (`quantity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_product_sale`
--

LOCK TABLES `ps_product_sale` WRITE;
/*!40000 ALTER TABLE `ps_product_sale` DISABLE KEYS */;
INSERT INTO `ps_product_sale` VALUES (2,1,1,'2018-09-07'),(3,1,1,'2018-09-08'),(4,1,1,'2018-10-11'),(6,4,4,'2018-09-21'),(8,4,4,'2018-09-18'),(9,5,5,'2018-12-08'),(10,1,1,'2018-12-08'),(11,7,3,'2018-10-11'),(14,2,2,'2018-09-22');
/*!40000 ALTER TABLE `ps_product_sale` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_product_shop`
--

DROP TABLE IF EXISTS `ps_product_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_product_shop` (
  `id_product` int(10) unsigned NOT NULL,
  `id_shop` int(10) unsigned NOT NULL,
  `id_category_default` int(10) unsigned DEFAULT NULL,
  `id_tax_rules_group` int(11) unsigned NOT NULL,
  `on_sale` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `online_only` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ecotax` decimal(17,6) NOT NULL DEFAULT '0.000000',
  `minimal_quantity` int(10) unsigned NOT NULL DEFAULT '1',
  `low_stock_threshold` int(10) DEFAULT NULL,
  `low_stock_alert` tinyint(1) NOT NULL DEFAULT '0',
  `price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `wholesale_price` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `unity` varchar(255) DEFAULT NULL,
  `unit_price_ratio` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `additional_shipping_cost` decimal(20,2) NOT NULL DEFAULT '0.00',
  `customizable` tinyint(2) NOT NULL DEFAULT '0',
  `uploadable_files` tinyint(4) NOT NULL DEFAULT '0',
  `text_fields` tinyint(4) NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `redirect_type` enum('','404','301-product','302-product','301-category','302-category') NOT NULL DEFAULT '',
  `id_type_redirected` int(10) unsigned NOT NULL DEFAULT '0',
  `available_for_order` tinyint(1) NOT NULL DEFAULT '1',
  `available_date` date DEFAULT NULL,
  `show_condition` tinyint(1) NOT NULL DEFAULT '1',
  `condition` enum('new','used','refurbished') NOT NULL DEFAULT 'new',
  `show_price` tinyint(1) NOT NULL DEFAULT '1',
  `indexed` tinyint(1) NOT NULL DEFAULT '0',
  `visibility` enum('both','catalog','search','none') NOT NULL DEFAULT 'both',
  `cache_default_attribute` int(10) unsigned DEFAULT NULL,
  `advanced_stock_management` tinyint(1) NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `pack_stock_type` int(11) unsigned NOT NULL DEFAULT '3',
  PRIMARY KEY (`id_product`,`id_shop`),
  KEY `id_category_default` (`id_category_default`),
  KEY `date_add` (`date_add`,`active`,`visibility`),
  KEY `indexed` (`indexed`,`active`,`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_product_shop`
--

LOCK TABLES `ps_product_shop` WRITE;
/*!40000 ALTER TABLE `ps_product_shop` DISABLE KEYS */;
INSERT INTO `ps_product_shop` VALUES (1,1,13,0,0,0,0.000000,1,NULL,0,10000.000000,0.000000,'本',0.000000,0.00,0,0,0,1,'404',0,1,'0000-00-00',0,'new',1,1,'both',0,0,'2018-06-17 15:18:52','2018-11-18 00:33:44',3),(2,1,13,0,0,0,0.000000,1,NULL,0,23100.000000,0.000000,'',0.000000,0.00,0,0,0,1,'404',0,1,'0000-00-00',0,'new',1,1,'both',6,0,'2018-06-17 18:32:17','2018-07-29 19:20:06',3),(3,1,13,0,0,0,0.000000,1,NULL,0,1500.000000,0.000000,'',0.000000,0.00,0,0,0,1,'404',0,1,'0000-00-00',0,'new',1,1,'both',0,0,'2018-06-17 18:35:12','2018-09-23 17:47:21',3),(4,1,13,0,0,0,0.000000,1,NULL,0,10000.000000,0.000000,'',0.000000,0.00,0,0,0,1,'404',0,1,'0000-00-00',0,'new',1,1,'both',0,0,'2018-06-17 18:35:23','2018-09-23 17:47:32',3),(5,1,13,0,0,0,0.000000,1,NULL,0,1000000.000000,0.000000,'',0.000000,0.00,0,0,0,0,'404',0,1,'0000-00-00',0,'new',1,0,'both',0,0,'2018-06-17 18:38:03','2018-12-09 23:10:31',3),(6,1,9,0,0,0,0.000000,1,NULL,0,50.000000,0.000000,'',0.000000,0.00,0,0,0,1,'404',0,1,'0000-00-00',0,'new',1,1,'both',0,0,'2018-06-17 18:38:11','2018-09-23 17:47:42',3),(8,1,9,0,0,0,0.000000,5,NULL,0,20.000000,0.000000,'',0.000000,0.00,0,0,0,1,'404',0,1,'0000-00-00',0,'new',1,1,'both',0,0,'2018-06-17 18:38:21','2018-11-10 11:58:02',3),(9,1,10,0,0,0,0.000000,1,NULL,0,180.000000,0.000000,'',0.000000,0.00,0,0,0,1,'404',0,1,'0000-00-00',0,'new',1,1,'both',0,0,'2018-06-17 18:38:25','2018-09-23 17:47:52',3),(10,1,10,0,0,0,0.000000,1,NULL,0,180.000000,0.000000,'',0.000000,0.00,0,0,0,1,'404',0,1,'0000-00-00',0,'new',1,1,'both',0,0,'2018-06-17 18:38:30','2018-09-23 17:48:01',3),(11,1,10,0,0,0,0.000000,1,NULL,0,300.000000,0.000000,'',0.000000,0.00,0,0,0,1,'404',0,1,'0000-00-00',0,'new',1,1,'both',0,0,'2018-06-17 18:38:37','2018-11-15 18:37:09',3),(12,1,10,0,0,0,0.000000,1,NULL,0,150.000000,0.000000,'',0.000000,0.00,1,1,1,1,'404',0,1,'2018-11-30',1,'used',1,1,'both',0,0,'2018-06-17 18:38:42','2018-10-09 01:01:01',3),(13,1,11,0,0,0,0.000000,1,NULL,0,1800.000000,0.000000,'',0.000000,0.00,0,0,0,1,'404',0,1,'0000-00-00',0,'new',1,1,'both',3,0,'2018-06-17 18:38:47','2018-07-29 13:51:10',3),(14,1,12,0,0,0,0.000000,1,NULL,0,1800.000000,0.000000,'',0.000000,0.00,0,0,0,1,'404',0,1,'0000-00-00',0,'new',1,1,'both',0,0,'2018-09-14 19:30:23','2019-01-21 23:12:40',3),(15,1,12,0,0,0,0.000000,1,NULL,0,50000.000000,0.000000,'',0.000000,0.00,0,0,0,1,'404',0,1,'0000-00-00',1,'new',1,1,'both',9,0,'2018-09-14 19:30:23','2019-01-06 22:18:42',3),(16,1,2,0,0,0,0.000000,1,NULL,0,0.000000,0.000000,'',0.000000,0.00,0,0,0,0,'',0,1,'0000-00-00',0,'new',1,0,'both',0,0,'2019-01-23 00:09:27','2019-01-23 00:09:27',3);
/*!40000 ALTER TABLE `ps_product_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_product_supplier`
--

DROP TABLE IF EXISTS `ps_product_supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_product_supplier` (
  `id_product_supplier` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_product` int(11) unsigned NOT NULL,
  `id_product_attribute` int(11) unsigned NOT NULL DEFAULT '0',
  `id_supplier` int(11) unsigned NOT NULL,
  `product_supplier_reference` varchar(32) DEFAULT NULL,
  `product_supplier_price_te` decimal(20,6) NOT NULL DEFAULT '0.000000',
  `id_currency` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_product_supplier`),
  UNIQUE KEY `id_product` (`id_product`,`id_product_attribute`,`id_supplier`),
  KEY `id_supplier` (`id_supplier`,`id_product`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_product_supplier`
--

LOCK TABLES `ps_product_supplier` WRITE;
/*!40000 ALTER TABLE `ps_product_supplier` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_product_supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_product_tag`
--

DROP TABLE IF EXISTS `ps_product_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_product_tag` (
  `id_product` int(10) unsigned NOT NULL,
  `id_tag` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_product`,`id_tag`),
  KEY `id_tag` (`id_tag`),
  KEY `id_lang` (`id_lang`,`id_tag`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_product_tag`
--

LOCK TABLES `ps_product_tag` WRITE;
/*!40000 ALTER TABLE `ps_product_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_product_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_profile`
--

DROP TABLE IF EXISTS `ps_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_profile` (
  `id_profile` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_profile`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_profile`
--

LOCK TABLES `ps_profile` WRITE;
/*!40000 ALTER TABLE `ps_profile` DISABLE KEYS */;
INSERT INTO `ps_profile` VALUES (1),(2),(3),(4);
/*!40000 ALTER TABLE `ps_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_profile_lang`
--

DROP TABLE IF EXISTS `ps_profile_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_profile_lang` (
  `id_lang` int(10) unsigned NOT NULL,
  `id_profile` int(10) unsigned NOT NULL,
  `name` varchar(128) NOT NULL,
  PRIMARY KEY (`id_profile`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_profile_lang`
--

LOCK TABLES `ps_profile_lang` WRITE;
/*!40000 ALTER TABLE `ps_profile_lang` DISABLE KEYS */;
INSERT INTO `ps_profile_lang` VALUES (1,1,'超級管理員'),(2,1,'超级管理员'),(3,1,'超级管理员'),(4,1,'超级管理员'),(1,2,'商品部'),(2,2,'物流员'),(3,2,'物流员'),(4,2,'物流员'),(1,3,'廣告部'),(2,3,'翻译者'),(3,3,'翻译者'),(4,3,'翻译者'),(1,4,'出貨部'),(2,4,'销售人员'),(3,4,'销售人员'),(4,4,'销售人员');
/*!40000 ALTER TABLE `ps_profile_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_quick_access`
--

DROP TABLE IF EXISTS `ps_quick_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_quick_access` (
  `id_quick_access` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `new_window` tinyint(1) NOT NULL DEFAULT '0',
  `link` varchar(255) NOT NULL,
  PRIMARY KEY (`id_quick_access`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_quick_access`
--

LOCK TABLES `ps_quick_access` WRITE;
/*!40000 ALTER TABLE `ps_quick_access` DISABLE KEYS */;
INSERT INTO `ps_quick_access` VALUES (1,0,'index.php/product/new');
/*!40000 ALTER TABLE `ps_quick_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_quick_access_lang`
--

DROP TABLE IF EXISTS `ps_quick_access_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_quick_access_lang` (
  `id_quick_access` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id_quick_access`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_quick_access_lang`
--

LOCK TABLES `ps_quick_access_lang` WRITE;
/*!40000 ALTER TABLE `ps_quick_access_lang` DISABLE KEYS */;
INSERT INTO `ps_quick_access_lang` VALUES (1,1,'新增商品'),(1,2,'New product'),(1,3,'新增商品'),(1,4,'新規商品');
/*!40000 ALTER TABLE `ps_quick_access_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_range_price`
--

DROP TABLE IF EXISTS `ps_range_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_range_price` (
  `id_range_price` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_carrier` int(10) unsigned NOT NULL,
  `delimiter1` decimal(20,6) NOT NULL,
  `delimiter2` decimal(20,6) NOT NULL,
  PRIMARY KEY (`id_range_price`),
  UNIQUE KEY `id_carrier` (`id_carrier`,`delimiter1`,`delimiter2`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_range_price`
--

LOCK TABLES `ps_range_price` WRITE;
/*!40000 ALTER TABLE `ps_range_price` DISABLE KEYS */;
INSERT INTO `ps_range_price` VALUES (7,103,0.000000,1000.000000),(38,103,1000.000000,4000.000000),(9,104,0.000000,1000.000000),(10,104,1000.000000,20000.000000),(34,105,0.000000,1000.000000),(35,105,1000.000000,5000.000000),(36,106,0.000000,1000.000000),(37,106,1000.000000,20000.000000),(19,107,0.000000,1000.000000),(25,107,1000.000000,3500.000000),(26,107,3500.000000,20000.000000),(39,108,0.000000,1000.000000),(40,108,1000.000000,3500.000000),(41,108,3500.000000,20000.000000),(42,109,0.000000,1000.000000),(43,109,1000.000000,3500.000000),(44,109,3500.000000,20000.000000),(21,110,0.000000,1000.000000),(32,110,1000.000000,3500.000000),(33,110,3500.000000,20000.000000),(29,111,0.000000,1000.000000),(30,111,1000.000000,3500.000000),(31,111,3500.000000,20000.000000),(23,112,0.000000,1000.000000),(27,112,1000.000000,3500.000000),(28,112,3500.000000,20000.000000),(11,113,0.000000,1000.000000),(12,113,1000.000000,20000.000000),(45,115,0.000000,1000.000000);
/*!40000 ALTER TABLE `ps_range_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_range_weight`
--

DROP TABLE IF EXISTS `ps_range_weight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_range_weight` (
  `id_range_weight` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_carrier` int(10) unsigned NOT NULL,
  `delimiter1` decimal(20,6) NOT NULL,
  `delimiter2` decimal(20,6) NOT NULL,
  PRIMARY KEY (`id_range_weight`),
  UNIQUE KEY `id_carrier` (`id_carrier`,`delimiter1`,`delimiter2`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_range_weight`
--

LOCK TABLES `ps_range_weight` WRITE;
/*!40000 ALTER TABLE `ps_range_weight` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_range_weight` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_referrer`
--

DROP TABLE IF EXISTS `ps_referrer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_referrer` (
  `id_referrer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `passwd` varchar(32) DEFAULT NULL,
  `http_referer_regexp` varchar(64) DEFAULT NULL,
  `http_referer_like` varchar(64) DEFAULT NULL,
  `request_uri_regexp` varchar(64) DEFAULT NULL,
  `request_uri_like` varchar(64) DEFAULT NULL,
  `http_referer_regexp_not` varchar(64) DEFAULT NULL,
  `http_referer_like_not` varchar(64) DEFAULT NULL,
  `request_uri_regexp_not` varchar(64) DEFAULT NULL,
  `request_uri_like_not` varchar(64) DEFAULT NULL,
  `base_fee` decimal(5,2) NOT NULL DEFAULT '0.00',
  `percent_fee` decimal(5,2) NOT NULL DEFAULT '0.00',
  `click_fee` decimal(5,2) NOT NULL DEFAULT '0.00',
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_referrer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_referrer`
--

LOCK TABLES `ps_referrer` WRITE;
/*!40000 ALTER TABLE `ps_referrer` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_referrer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_referrer_cache`
--

DROP TABLE IF EXISTS `ps_referrer_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_referrer_cache` (
  `id_connections_source` int(11) unsigned NOT NULL,
  `id_referrer` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_connections_source`,`id_referrer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_referrer_cache`
--

LOCK TABLES `ps_referrer_cache` WRITE;
/*!40000 ALTER TABLE `ps_referrer_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_referrer_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_referrer_shop`
--

DROP TABLE IF EXISTS `ps_referrer_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_referrer_shop` (
  `id_referrer` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(10) unsigned NOT NULL DEFAULT '1',
  `cache_visitors` int(11) DEFAULT NULL,
  `cache_visits` int(11) DEFAULT NULL,
  `cache_pages` int(11) DEFAULT NULL,
  `cache_registrations` int(11) DEFAULT NULL,
  `cache_orders` int(11) DEFAULT NULL,
  `cache_sales` decimal(17,2) DEFAULT NULL,
  `cache_reg_rate` decimal(5,4) DEFAULT NULL,
  `cache_order_rate` decimal(5,4) DEFAULT NULL,
  PRIMARY KEY (`id_referrer`,`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_referrer_shop`
--

LOCK TABLES `ps_referrer_shop` WRITE;
/*!40000 ALTER TABLE `ps_referrer_shop` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_referrer_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_request_sql`
--

DROP TABLE IF EXISTS `ps_request_sql`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_request_sql` (
  `id_request_sql` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `sql` text NOT NULL,
  PRIMARY KEY (`id_request_sql`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_request_sql`
--

LOCK TABLES `ps_request_sql` WRITE;
/*!40000 ALTER TABLE `ps_request_sql` DISABLE KEYS */;
INSERT INTO `ps_request_sql` VALUES (1,'LOG','SELECT * FROM ps_log;'),(2,'FbMessenger Subscription','SELECT * FROM ps_fbmessenger_subscription;'),(3,'FbMessenger Message','SELECT * FROM ps_fbmessenger_message;'),(4,'Email Subscription','SELECT * FROM ps_emailsubscription;');
/*!40000 ALTER TABLE `ps_request_sql` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_required_field`
--

DROP TABLE IF EXISTS `ps_required_field`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_required_field` (
  `id_required_field` int(11) NOT NULL AUTO_INCREMENT,
  `object_name` varchar(32) NOT NULL,
  `field_name` varchar(32) NOT NULL,
  PRIMARY KEY (`id_required_field`),
  KEY `object_name` (`object_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_required_field`
--

LOCK TABLES `ps_required_field` WRITE;
/*!40000 ALTER TABLE `ps_required_field` DISABLE KEYS */;
INSERT INTO `ps_required_field` VALUES (1,'CustomerAddress','phone_mobile');
/*!40000 ALTER TABLE `ps_required_field` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_risk`
--

DROP TABLE IF EXISTS `ps_risk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_risk` (
  `id_risk` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `percent` tinyint(3) NOT NULL,
  `color` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id_risk`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_risk`
--

LOCK TABLES `ps_risk` WRITE;
/*!40000 ALTER TABLE `ps_risk` DISABLE KEYS */;
INSERT INTO `ps_risk` VALUES (1,0,'#32CD32'),(2,35,'#FF8C00'),(3,75,'#DC143C'),(4,100,'#ec2e15');
/*!40000 ALTER TABLE `ps_risk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_risk_lang`
--

DROP TABLE IF EXISTS `ps_risk_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_risk_lang` (
  `id_risk` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  PRIMARY KEY (`id_risk`,`id_lang`),
  KEY `id_risk` (`id_risk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_risk_lang`
--

LOCK TABLES `ps_risk_lang` WRITE;
/*!40000 ALTER TABLE `ps_risk_lang` DISABLE KEYS */;
INSERT INTO `ps_risk_lang` VALUES (1,1,'無'),(1,2,'无'),(1,3,'无'),(1,4,'无'),(2,1,'低'),(2,2,'Low'),(2,3,'低'),(2,4,'低'),(3,1,'中'),(3,2,'Medium'),(3,3,'中'),(3,4,'中'),(4,1,'高'),(4,2,'High'),(4,3,'高'),(4,4,'高い');
/*!40000 ALTER TABLE `ps_risk_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_search_engine`
--

DROP TABLE IF EXISTS `ps_search_engine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_search_engine` (
  `id_search_engine` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `server` varchar(64) NOT NULL,
  `getvar` varchar(16) NOT NULL,
  PRIMARY KEY (`id_search_engine`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_search_engine`
--

LOCK TABLES `ps_search_engine` WRITE;
/*!40000 ALTER TABLE `ps_search_engine` DISABLE KEYS */;
INSERT INTO `ps_search_engine` VALUES (1,'google','q'),(2,'aol','q'),(3,'yandex','text'),(4,'ask.com','q'),(5,'nhl.com','q'),(6,'yahoo','p'),(7,'baidu','wd'),(8,'lycos','query'),(9,'exalead','q'),(10,'search.live','q'),(11,'voila','rdata'),(12,'altavista','q'),(13,'bing','q'),(14,'daum','q'),(15,'eniro','search_word'),(16,'naver','query'),(17,'msn','q'),(18,'netscape','query'),(19,'cnn','query'),(20,'about','terms'),(21,'mamma','query'),(22,'alltheweb','q'),(23,'virgilio','qs'),(24,'alice','qs'),(25,'najdi','q'),(26,'mama','query'),(27,'seznam','q'),(28,'onet','qt'),(29,'szukacz','q'),(30,'yam','k'),(31,'pchome','q'),(32,'kvasir','q'),(33,'sesam','q'),(34,'ozu','q'),(35,'terra','query'),(36,'mynet','q'),(37,'ekolay','q'),(38,'rambler','words');
/*!40000 ALTER TABLE `ps_search_engine` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_search_index`
--

DROP TABLE IF EXISTS `ps_search_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_search_index` (
  `id_product` int(11) unsigned NOT NULL,
  `id_word` int(11) unsigned NOT NULL,
  `weight` smallint(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_word`,`id_product`),
  KEY `id_product` (`id_product`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_search_index`
--

LOCK TABLES `ps_search_index` WRITE;
/*!40000 ALTER TABLE `ps_search_index` DISABLE KEYS */;
INSERT INTO `ps_search_index` VALUES (1,42947,1),(1,42948,1),(1,42949,1),(1,42950,1),(1,42951,1),(1,42952,1),(1,42953,1),(1,42954,1),(1,42955,1),(1,42956,1),(1,42957,1),(1,42958,1),(1,42961,1),(1,42962,1),(1,42963,1),(1,42964,1),(1,42965,1),(1,42966,1),(1,42967,1),(1,42969,1),(1,42970,1),(1,42971,1),(1,42972,1),(1,42973,1),(1,42974,1),(1,42975,1),(1,42976,1),(1,42978,1),(1,42979,1),(1,42980,1),(1,42981,1),(1,42982,1),(1,42983,1),(1,42984,1),(1,42985,1),(1,42986,1),(1,42987,1),(1,42988,1),(1,42989,1),(1,42990,1),(1,42991,1),(1,42994,1),(1,42997,1),(1,42998,1),(1,42999,1),(1,43000,1),(1,43001,1),(1,43002,1),(1,43004,1),(1,43008,1),(1,43013,1),(1,43014,1),(1,43015,1),(1,43017,1),(1,43018,1),(1,43022,1),(1,43028,1),(1,43029,1),(1,43030,1),(1,43032,1),(1,43033,1),(1,43034,1),(1,43035,1),(1,43036,1),(1,43037,1),(1,43038,1),(1,43039,1),(1,43040,1),(1,43041,1),(1,43042,1),(1,43043,1),(1,43044,1),(1,43045,1),(1,43046,1),(1,43047,1),(1,43048,1),(1,43049,1),(1,43050,1),(1,43054,1),(1,43056,1),(1,43057,1),(1,43058,1),(1,43059,1),(1,43061,1),(1,43062,1),(1,43066,1),(1,43067,1),(1,43068,1),(1,43069,1),(1,43070,1),(1,43075,1),(1,43076,1),(1,43077,1),(1,43078,1),(1,43079,1),(1,43080,1),(1,43081,1),(1,43084,1),(1,43085,1),(1,43086,1),(1,43087,1),(1,43088,1),(1,43089,1),(1,43090,1),(1,43091,1),(1,43094,1),(1,43095,1),(1,43096,1),(1,43097,1),(1,43100,1),(1,43101,1),(1,43102,1),(1,43103,1),(1,43104,1),(1,43107,1),(1,43108,1),(1,43112,1),(1,43113,1),(1,43114,1),(1,43115,1),(1,43118,1),(1,43119,1),(1,43120,1),(1,43121,1),(1,43122,1),(1,43123,1),(1,43124,1),(1,43130,1),(1,43133,1),(1,43135,1),(1,43138,1),(1,43139,1),(1,43140,1),(1,43141,1),(1,43142,1),(1,43143,1),(1,43144,1),(1,43145,1),(1,43146,1),(1,43147,1),(1,43148,1),(1,43149,1),(1,43150,1),(1,43151,1),(1,43152,1),(1,43153,1),(1,43154,1),(1,43155,1),(1,43156,1),(1,43157,1),(1,43158,1),(1,43160,1),(1,43161,1),(1,43166,1),(1,43168,1),(1,43171,1),(1,43172,1),(1,43173,1),(1,43175,1),(1,43176,1),(1,43179,1),(1,43182,1),(1,43187,1),(1,43188,1),(1,43195,1),(1,43196,1),(1,43198,1),(1,43201,1),(1,43202,1),(1,43203,1),(1,43204,1),(1,43206,1),(1,43207,1),(1,43208,1),(1,43209,1),(1,43219,1),(1,43221,1),(1,43222,1),(1,43223,1),(1,43224,1),(1,43227,1),(1,43230,1),(1,43231,1),(1,43234,1),(1,43235,1),(1,43237,1),(1,43238,1),(1,43239,1),(1,43241,1),(1,43242,1),(1,43244,1),(1,43246,1),(1,43248,1),(1,43251,1),(1,43252,1),(1,43253,1),(1,43255,1),(1,43256,1),(1,43257,1),(1,43258,1),(1,43259,1),(1,43262,1),(1,43264,1),(1,43265,1),(1,43266,1),(1,43267,1),(1,43268,1),(1,43269,1),(1,43270,1),(1,43271,1),(1,43272,1),(1,43273,1),(1,43274,1),(1,43275,1),(1,43276,1),(1,43277,1),(1,43278,1),(1,43280,1),(1,43281,1),(1,43282,1),(1,43283,1),(1,43284,1),(1,43285,1),(1,43288,1),(1,43289,1),(1,43290,1),(1,43291,1),(1,43292,1),(1,43293,1),(1,43294,1),(1,43295,1),(1,43296,1),(1,43297,1),(1,43298,1),(1,43299,1),(1,43300,1),(1,43301,1),(1,43302,1),(1,43303,1),(1,43304,1),(1,43305,1),(1,43306,1),(1,43307,1),(1,43308,1),(1,43309,1),(1,43311,1),(1,43313,1),(1,43314,1),(1,43315,1),(1,43316,1),(1,43318,1),(1,43319,1),(1,43320,1),(1,43321,1),(1,43322,1),(1,43323,1),(1,43324,1),(1,43325,1),(1,43326,1),(1,43327,1),(1,43328,1),(1,43329,1),(1,43330,1),(1,43331,1),(1,43332,1),(1,43333,1),(1,43334,1),(1,43335,1),(1,43336,1),(1,43337,1),(1,43338,1),(1,43339,1),(1,43340,1),(1,43341,1),(1,43342,1),(1,43343,1),(1,43344,1),(1,43345,1),(1,43346,1),(1,43347,1),(1,43348,1),(1,43349,1),(1,43350,1),(1,43351,1),(1,43352,1),(1,43353,1),(1,43354,1),(1,43355,1),(1,43356,1),(1,43357,1),(1,43358,1),(1,43359,1),(1,43360,1),(1,43361,1),(1,43362,1),(1,43363,1),(1,43364,1),(1,43365,1),(1,43366,1),(1,43367,1),(1,43369,1),(1,43370,1),(1,43371,1),(1,43372,1),(1,43373,1),(1,43374,1),(1,43375,1),(1,43376,1),(1,43377,1),(1,43378,1),(1,43379,1),(1,43380,1),(1,43381,1),(1,43382,1),(1,43383,1),(1,43384,1),(1,43385,1),(1,43386,1),(1,43387,1),(1,43388,1),(1,43389,1),(1,43390,1),(1,43391,1),(1,43392,1),(1,43393,1),(1,43394,1),(1,43395,1),(1,43396,1),(1,43397,1),(1,43398,1),(1,43399,1),(1,43400,1),(1,43401,1),(1,43402,1),(1,43403,1),(1,43404,1),(1,43405,1),(1,43406,1),(1,43407,1),(1,43408,1),(1,43409,1),(1,43410,1),(1,43411,1),(1,43412,1),(1,43413,1),(1,43414,1),(1,43415,1),(1,43416,1),(1,43417,1),(1,43418,1),(1,43419,1),(1,43421,1),(1,43423,1),(1,43425,1),(1,43426,1),(1,43427,1),(1,43428,1),(1,43429,1),(1,43430,1),(1,43431,1),(1,43432,1),(1,43433,1),(1,43434,1),(1,43436,1),(1,43437,1),(1,43438,1),(1,43439,1),(1,43440,1),(1,43441,1),(1,43442,1),(1,43443,1),(1,43444,1),(1,43445,1),(1,43446,1),(1,43447,1),(1,43448,1),(1,43449,1),(1,43450,1),(1,43451,1),(1,43452,1),(1,43453,1),(1,43455,1),(1,43457,1),(1,43459,1),(1,43461,1),(1,43465,1),(1,43466,1),(1,43467,1),(1,43468,1),(1,43475,1),(1,43476,1),(1,43477,1),(1,43478,1),(1,43480,1),(1,43481,1),(1,43482,1),(1,43483,1),(1,43484,1),(1,43485,1),(1,43486,1),(1,43487,1),(1,43488,1),(1,43489,1),(1,43490,1),(1,43491,1),(1,43492,1),(1,43493,1),(1,43494,1),(1,43495,1),(1,43496,1),(1,43497,1),(1,43498,1),(1,43499,1),(1,43500,1),(1,43504,1),(1,43505,1),(1,43506,1),(1,43515,1),(1,43516,1),(1,43517,1),(1,43518,1),(1,43519,1),(1,43520,1),(1,43521,1),(1,43522,1),(1,43523,1),(1,43524,1),(1,43525,1),(1,43526,1),(1,43529,1),(1,43530,1),(1,43531,1),(1,43532,1),(1,43533,1),(1,43534,1),(1,43535,1),(1,43537,1),(1,43538,1),(1,43539,1),(1,43540,1),(1,43541,1),(1,43542,1),(1,43543,1),(1,43544,1),(1,43546,1),(1,43547,1),(1,43548,1),(1,43549,1),(1,43550,1),(1,43551,1),(1,43552,1),(1,43553,1),(1,43554,1),(1,43555,1),(1,43556,1),(1,43557,1),(1,43558,1),(1,43559,1),(1,43562,1),(1,43565,1),(1,43566,1),(1,43567,1),(1,43568,1),(1,43569,1),(1,43570,1),(1,43572,1),(1,43576,1),(1,43581,1),(1,43582,1),(1,43583,1),(1,43585,1),(1,43586,1),(1,43590,1),(1,43596,1),(1,43597,1),(1,43598,1),(1,43600,1),(1,43601,1),(1,43602,1),(1,43603,1),(1,43604,1),(1,43605,1),(1,43606,1),(1,43607,1),(1,43608,1),(1,43609,1),(1,43610,1),(1,43611,1),(1,43612,1),(1,43613,1),(1,43614,1),(1,43615,1),(1,43616,1),(1,43617,1),(1,43618,1),(1,43622,1),(1,43624,1),(1,43625,1),(1,43626,1),(1,43627,1),(1,43629,1),(1,43630,1),(1,43634,1),(1,43635,1),(1,43636,1),(1,43637,1),(1,43638,1),(1,43643,1),(1,43644,1),(1,43645,1),(1,43646,1),(1,43647,1),(1,43648,1),(1,43649,1),(1,43652,1),(1,43653,1),(1,43654,1),(1,43655,1),(1,43656,1),(1,43657,1),(1,43658,1),(1,43659,1),(1,43662,1),(1,43663,1),(1,43664,1),(1,43665,1),(1,43668,1),(1,43669,1),(1,43670,1),(1,43671,1),(1,43672,1),(1,43675,1),(1,43676,1),(1,43680,1),(1,43681,1),(1,43682,1),(1,43683,1),(1,43686,1),(1,43687,1),(1,43688,1),(1,43689,1),(1,43690,1),(1,43691,1),(1,43692,1),(1,43698,1),(1,43701,1),(1,43703,1),(1,43706,1),(1,43707,1),(1,43708,1),(1,43709,1),(1,43710,1),(1,43711,1),(1,43712,1),(1,43713,1),(1,43714,1),(1,43715,1),(1,43716,1),(1,43717,1),(1,43718,1),(1,43719,1),(1,43720,1),(1,43721,1),(1,43722,1),(1,43723,1),(1,43724,1),(1,43725,1),(1,43726,1),(1,43728,1),(1,43729,1),(1,43735,1),(1,43737,1),(1,43740,1),(1,43741,1),(1,43742,1),(1,43744,1),(1,43745,1),(1,43746,1),(1,43749,1),(1,43752,1),(1,43757,1),(1,43758,1),(1,43765,1),(1,43766,1),(1,43768,1),(1,43771,1),(1,43772,1),(1,43773,1),(1,43774,1),(1,43776,1),(1,43777,1),(1,43778,1),(1,43779,1),(1,43789,1),(1,43790,1),(1,43791,1),(1,43794,1),(1,43797,1),(1,43798,1),(1,43801,1),(1,43802,1),(1,43804,1),(1,43805,1),(1,43806,1),(1,43808,1),(1,43809,1),(1,43811,1),(1,43813,1),(1,43815,1),(1,43818,1),(1,43819,1),(1,43820,1),(1,43822,1),(1,43823,1),(1,43824,1),(1,43825,1),(1,43826,1),(1,43829,1),(1,43831,1),(1,43832,1),(1,43833,1),(1,43834,1),(1,43835,1),(1,43836,1),(1,43837,1),(1,43838,1),(1,43839,1),(1,43840,1),(1,43841,1),(1,43842,1),(1,43843,1),(1,43844,1),(1,43845,1),(1,43847,1),(1,43848,1),(1,43849,1),(1,43850,1),(1,43851,1),(1,43852,1),(1,43855,1),(1,43856,1),(1,43857,1),(1,43858,1),(1,43859,1),(1,43860,1),(1,43861,1),(1,43862,1),(1,43863,1),(1,43864,1),(1,43865,1),(1,43866,1),(1,43867,1),(1,43868,1),(1,43869,1),(1,43870,1),(1,43871,1),(1,43872,1),(1,43873,1),(1,43874,1),(1,43875,1),(1,43876,1),(1,43878,1),(1,43880,1),(1,43881,1),(1,43882,1),(1,43883,1),(1,43885,1),(1,43886,1),(1,43887,1),(1,43888,1),(1,43889,1),(1,43890,1),(1,43891,1),(1,43892,1),(1,43893,1),(1,43894,1),(1,43895,1),(1,43896,1),(1,43897,1),(1,43898,1),(1,43899,1),(1,43900,1),(1,43901,1),(1,43902,1),(1,43903,1),(1,43904,1),(1,43905,1),(1,43906,1),(1,43907,1),(1,43908,1),(1,43909,1),(1,43910,1),(1,43911,1),(1,43912,1),(1,43913,1),(1,43914,1),(1,43915,1),(1,43916,1),(1,43917,1),(1,43918,1),(1,43919,1),(1,43920,1),(1,43921,1),(1,43922,1),(1,43923,1),(1,43924,1),(1,43925,1),(1,43926,1),(1,43927,1),(1,43928,1),(1,43929,1),(1,43930,1),(1,43932,1),(1,43934,1),(1,43936,1),(1,43937,1),(1,43938,1),(1,43939,1),(1,43940,1),(1,43941,1),(1,43942,1),(1,43943,1),(1,43944,1),(1,43945,1),(1,43946,1),(1,43948,1),(1,43949,1),(1,43950,1),(1,43951,1),(1,43952,1),(1,43953,1),(1,43954,1),(1,43955,1),(1,43956,1),(1,43957,1),(1,43959,1),(1,43960,1),(1,43961,1),(1,43962,1),(1,43963,1),(1,43964,1),(1,43965,1),(1,43966,1),(1,43967,1),(1,43968,1),(1,43969,1),(1,43970,1),(1,43972,1),(1,43975,1),(1,43980,1),(1,43981,1),(1,43982,1),(1,43983,1),(1,43984,1),(1,43987,1),(1,43992,1),(1,43993,1),(1,43994,1),(1,43995,1),(1,43996,1),(1,43998,1),(1,43999,1),(1,44000,1),(1,44001,1),(1,44002,1),(1,44003,1),(1,44004,1),(1,44005,1),(1,44006,1),(1,44007,1),(1,44008,1),(1,44009,1),(1,44010,1),(1,44011,1),(1,44012,1),(1,44016,1),(1,44017,1),(1,44018,1),(1,44037,1),(1,44038,1),(1,44040,1),(1,44041,1),(1,44051,1),(1,44070,1),(1,44074,1),(1,44075,1),(1,44076,1),(1,44082,1),(1,44084,1),(1,44090,1),(1,44094,1),(1,44096,1),(1,44097,1),(1,44100,1),(1,44101,1),(1,44102,1),(1,44103,1),(1,44106,1),(1,44108,1),(1,44109,1),(1,44110,1),(1,44112,1),(1,44113,1),(1,44115,1),(1,44119,1),(1,44120,1),(1,44121,1),(1,44124,1),(1,44125,1),(1,44126,1),(1,44129,1),(1,44130,1),(1,44131,1),(1,44132,1),(1,44133,1),(1,44134,1),(1,44135,1),(1,44137,1),(1,44140,1),(1,44141,1),(1,44142,1),(1,44143,1),(1,44144,1),(1,44147,1),(1,44148,1),(1,44153,1),(1,44154,1),(1,44157,1),(1,44159,1),(1,44160,1),(1,44161,1),(1,44162,1),(1,44163,1),(1,44165,1),(1,44167,1),(1,44168,1),(1,44170,1),(1,44171,1),(1,44172,1),(1,44173,1),(1,44175,1),(1,44176,1),(1,44177,1),(1,44178,1),(1,44179,1),(1,44180,1),(1,44181,1),(1,44183,1),(1,44184,1),(1,44185,1),(1,44187,1),(1,44189,1),(1,44191,1),(1,44192,1),(1,44193,1),(1,44194,1),(1,44195,1),(1,44196,1),(1,44197,1),(1,44198,1),(1,44199,1),(1,44200,1),(1,44201,1),(1,44202,1),(1,44203,1),(1,44204,1),(1,44205,1),(1,44206,1),(1,44207,1),(1,44208,1),(1,44209,1),(1,44211,1),(1,44212,1),(1,44214,1),(1,44216,1),(1,44217,1),(1,44218,1),(1,44219,1),(1,44220,1),(1,44221,1),(1,44222,1),(1,44223,1),(1,44226,1),(1,44228,1),(1,44232,1),(1,44233,1),(1,44234,1),(1,44235,1),(1,44236,1),(1,44237,1),(1,44238,1),(1,44239,1),(1,44241,1),(1,44242,1),(1,44244,1),(1,44245,1),(1,44246,1),(1,44247,1),(1,44248,1),(1,44250,1),(1,44252,1),(1,44254,1),(1,44255,1),(1,44256,1),(1,44257,1),(1,44258,1),(1,44260,1),(1,44261,1),(1,44262,1),(1,44263,1),(1,44264,1),(1,44265,1),(1,44266,1),(1,44267,1),(1,44268,1),(1,44269,1),(1,44270,1),(1,44271,1),(1,44272,1),(1,44273,1),(1,44274,1),(1,44275,1),(1,44276,1),(1,44277,1),(1,44282,1),(1,44287,1),(1,44291,1),(1,44292,1),(1,44293,1),(1,44294,1),(1,44295,1),(1,44296,1),(1,44297,1),(1,44298,1),(1,44300,1),(1,44301,1),(1,44302,1),(1,44303,1),(1,44304,1),(1,44305,1),(1,44306,1),(1,44307,1),(1,44308,1),(1,44309,1),(1,42960,2),(1,42992,2),(1,42993,2),(1,42995,2),(1,42996,2),(1,43003,2),(1,43005,2),(1,43007,2),(1,43010,2),(1,43011,2),(1,43019,2),(1,43020,2),(1,43021,2),(1,43023,2),(1,43024,2),(1,43025,2),(1,43026,2),(1,43027,2),(1,43051,2),(1,43052,2),(1,43060,2),(1,43063,2),(1,43064,2),(1,43065,2),(1,43082,2),(1,43083,2),(1,43092,2),(1,43093,2),(1,43098,2),(1,43099,2),(1,43106,2),(1,43109,2),(1,43116,2),(1,43129,2),(1,43131,2),(1,43159,2),(1,43167,2),(1,43169,2),(1,43170,2),(1,43177,2),(1,43183,2),(1,43185,2),(1,43186,2),(1,43189,2),(1,43190,2),(1,43192,2),(1,43193,2),(1,43194,2),(1,43199,2),(1,43200,2),(1,43211,2),(1,43212,2),(1,43213,2),(1,43214,2),(1,43215,2),(1,43216,2),(1,43217,2),(1,43218,2),(1,43220,2),(1,43226,2),(1,43233,2),(1,43245,2),(1,43247,2),(1,43249,2),(1,43261,2),(1,43263,2),(1,43286,2),(1,43287,2),(1,43310,2),(1,43312,2),(1,43317,2),(1,43368,2),(1,43420,2),(1,43422,2),(1,43424,2),(1,43454,2),(1,43456,2),(1,43462,2),(1,43463,2),(1,43469,2),(1,43470,2),(1,43471,2),(1,43472,2),(1,43473,2),(1,43474,2),(1,43501,2),(1,43502,2),(1,43503,2),(1,43528,2),(1,43560,2),(1,43561,2),(1,43563,2),(1,43564,2),(1,43571,2),(1,43573,2),(1,43575,2),(1,43578,2),(1,43579,2),(1,43587,2),(1,43588,2),(1,43589,2),(1,43591,2),(1,43592,2),(1,43593,2),(1,43594,2),(1,43595,2),(1,43619,2),(1,43620,2),(1,43628,2),(1,43631,2),(1,43632,2),(1,43633,2),(1,43650,2),(1,43651,2),(1,43660,2),(1,43661,2),(1,43666,2),(1,43667,2),(1,43674,2),(1,43677,2),(1,43684,2),(1,43697,2),(1,43699,2),(1,43727,2),(1,43736,2),(1,43738,2),(1,43739,2),(1,43747,2),(1,43753,2),(1,43755,2),(1,43756,2),(1,43759,2),(1,43760,2),(1,43762,2),(1,43763,2),(1,43764,2),(1,43769,2),(1,43770,2),(1,43781,2),(1,43782,2),(1,43783,2),(1,43784,2),(1,43785,2),(1,43786,2),(1,43787,2),(1,43788,2),(1,43793,2),(1,43800,2),(1,43812,2),(1,43814,2),(1,43816,2),(1,43828,2),(1,43830,2),(1,43853,2),(1,43854,2),(1,43877,2),(1,43879,2),(1,43884,2),(1,43931,2),(1,43933,2),(1,43935,2),(1,43958,2),(1,43971,2),(1,43973,2),(1,43977,2),(1,43978,2),(1,43985,2),(1,43986,2),(1,43988,2),(1,43989,2),(1,43990,2),(1,43991,2),(1,44013,2),(1,44014,2),(1,44015,2),(1,44019,2),(1,44020,2),(1,44022,2),(1,44024,2),(1,44025,2),(1,44026,2),(1,44027,2),(1,44029,2),(1,44034,2),(1,44035,2),(1,44050,2),(1,44064,2),(1,44065,2),(1,44072,2),(1,44083,2),(1,44088,2),(1,44089,2),(1,44099,2),(1,44104,2),(1,44105,2),(1,44114,2),(1,44118,2),(1,44122,2),(1,44123,2),(1,44127,2),(1,44136,2),(1,44138,2),(1,44139,2),(1,44145,2),(1,44149,2),(1,44151,2),(1,44152,2),(1,44156,2),(1,44158,2),(1,44166,2),(1,44169,2),(1,44174,2),(1,44182,2),(1,44186,2),(1,44188,2),(1,44190,2),(1,44213,2),(1,44215,2),(1,44227,2),(1,44229,2),(1,44230,2),(1,44231,2),(1,44243,2),(1,44249,2),(1,44251,2),(1,44253,2),(1,44281,2),(1,44283,2),(1,44286,2),(1,44290,2),(1,44299,2),(1,42959,3),(1,42968,3),(1,43006,3),(1,43009,3),(1,43012,3),(1,43016,3),(1,43031,3),(1,43053,3),(1,43055,3),(1,43072,3),(1,43073,3),(1,43110,3),(1,43117,3),(1,43127,3),(1,43128,3),(1,43136,3),(1,43137,3),(1,43174,3),(1,43180,3),(1,43181,3),(1,43184,3),(1,43191,3),(1,43210,3),(1,43225,3),(1,43229,3),(1,43236,3),(1,43240,3),(1,43243,3),(1,43250,3),(1,43260,3),(1,43279,3),(1,43435,3),(1,43458,3),(1,43460,3),(1,43464,3),(1,43479,3),(1,43507,3),(1,43527,3),(1,43536,3),(1,43574,3),(1,43577,3),(1,43580,3),(1,43584,3),(1,43599,3),(1,43621,3),(1,43623,3),(1,43640,3),(1,43641,3),(1,43678,3),(1,43685,3),(1,43695,3),(1,43696,3),(1,43704,3),(1,43705,3),(1,43743,3),(1,43750,3),(1,43751,3),(1,43754,3),(1,43761,3),(1,43780,3),(1,43792,3),(1,43796,3),(1,43803,3),(1,43807,3),(1,43810,3),(1,43817,3),(1,43827,3),(1,43846,3),(1,43947,3),(1,43974,3),(1,43976,3),(1,43979,3),(1,43997,3),(1,44021,3),(1,44023,3),(1,44028,3),(1,44033,3),(1,44036,3),(1,44045,3),(1,44047,3),(1,44049,3),(1,44060,3),(1,44066,3),(1,44078,3),(1,44087,3),(1,44091,3),(1,44092,3),(1,44095,3),(1,44107,3),(1,44117,3),(1,44128,3),(1,44155,3),(1,44164,3),(1,44259,3),(1,44279,3),(1,44280,3),(1,44289,3),(1,44310,3),(1,42977,4),(1,43074,4),(1,43197,4),(1,43205,4),(1,43232,4),(1,43545,4),(1,43642,4),(1,43767,4),(1,43775,4),(1,43799,4),(1,44030,4),(1,44031,4),(1,44032,4),(1,44043,4),(1,44046,4),(1,44048,4),(1,44052,4),(1,44058,4),(1,44063,4),(1,44093,4),(1,44098,4),(1,44116,4),(1,44146,4),(1,44150,4),(1,44210,4),(1,43178,5),(1,43228,5),(1,43254,5),(1,43748,5),(1,43795,5),(1,43821,5),(1,44039,5),(1,44055,5),(1,44056,5),(1,44057,5),(1,44059,5),(1,44067,5),(1,44073,5),(1,44079,5),(1,44081,5),(1,44086,5),(1,44240,5),(1,44285,5),(1,42940,6),(1,42941,6),(1,43111,6),(1,43126,6),(1,43134,6),(1,43508,6),(1,43509,6),(1,43679,6),(1,43694,6),(1,43702,6),(1,44042,6),(1,44054,6),(1,44061,6),(1,44068,6),(1,44069,6),(1,44071,6),(1,44085,6),(1,44225,6),(1,44284,6),(1,44288,6),(1,43125,7),(1,43693,7),(1,44077,7),(1,44111,7),(1,44224,7),(1,44278,7),(1,42942,8),(1,42943,8),(1,42944,8),(1,43510,8),(1,43511,8),(1,43512,8),(1,43071,9),(1,43132,9),(1,43164,9),(1,43639,9),(1,43700,9),(1,43733,9),(1,44062,9),(1,44080,9),(1,42946,10),(1,43105,10),(1,43165,10),(1,43514,10),(1,43673,10),(1,43734,10),(1,42945,11),(1,43162,11),(1,43513,11),(1,43730,11),(1,44053,16),(1,43732,17),(1,43163,19),(1,43731,19),(1,44044,23),(2,43083,2),(2,43651,2),(2,44170,2),(2,44313,2),(2,44314,2),(2,44315,2),(2,44316,2),(2,44317,2),(2,44318,2),(2,44319,2),(2,44320,2),(2,44321,2),(2,44322,2),(2,44323,2),(2,44324,2),(2,44325,2),(2,44339,2),(2,44340,2),(2,44341,2),(2,44349,2),(2,44350,2),(2,44351,2),(2,44352,2),(2,44353,2),(2,44354,2),(2,44355,2),(2,44356,2),(2,44357,2),(2,44358,2),(2,44360,2),(2,44361,2),(2,44362,2),(2,44372,2),(2,44373,2),(2,44374,2),(2,44375,2),(2,44376,2),(2,44377,2),(2,44378,2),(2,44379,2),(2,44380,2),(2,44381,2),(2,44383,2),(2,44384,2),(2,44385,2),(2,945,3),(2,43071,3),(2,43105,3),(2,43111,3),(2,43126,3),(2,43132,3),(2,43163,3),(2,43507,3),(2,43639,3),(2,43673,3),(2,43679,3),(2,43694,3),(2,43700,3),(2,43731,3),(2,44310,3),(2,43072,4),(2,44359,4),(2,44382,4),(2,42942,6),(2,42943,6),(2,42944,6),(2,43140,6),(2,43162,6),(2,43510,6),(2,43708,6),(2,43730,6),(2,44042,6),(2,44071,6),(2,44077,6),(2,44291,6),(2,44311,6),(2,44312,6),(2,44337,6),(2,44338,6),(2,44345,6),(2,44346,6),(2,44347,6),(2,44348,6),(2,44371,6),(3,43071,3),(3,43105,3),(3,43111,3),(3,43126,3),(3,43132,3),(3,43163,3),(3,43507,3),(3,43639,3),(3,43673,3),(3,43679,3),(3,43694,3),(3,43700,3),(3,43731,3),(3,44310,3),(3,42942,6),(3,42943,6),(3,42944,6),(3,43510,6),(3,44042,6),(3,44071,6),(3,44077,6),(3,44150,6),(3,44248,6),(3,44261,6),(3,44347,6),(3,44348,6),(3,44394,6),(3,44395,6),(3,44396,6),(3,44397,6),(3,44406,6),(3,44409,6),(3,44410,6),(3,44411,6),(3,44412,6),(3,44421,6),(4,44443,1),(4,44444,1),(4,44445,1),(4,44446,1),(4,44447,1),(4,44448,1),(4,44449,1),(4,44450,1),(4,44451,1),(4,44452,1),(4,44453,1),(4,43071,3),(4,43105,3),(4,43111,3),(4,43126,3),(4,43132,3),(4,43163,3),(4,43507,3),(4,43639,3),(4,43673,3),(4,43679,3),(4,43694,3),(4,43700,3),(4,43731,3),(4,44310,3),(4,42942,6),(4,42943,6),(4,42944,6),(4,43510,6),(4,44036,6),(4,44042,6),(4,44071,6),(4,44077,6),(4,44347,6),(4,44348,6),(4,44394,6),(4,44409,6),(4,44421,6),(4,44430,6),(4,44431,6),(4,44432,6),(4,44442,6),(4,44456,6),(4,44457,6),(4,44458,6),(4,44468,6),(4,44469,6),(5,43071,3),(5,43105,3),(5,43111,3),(5,43126,3),(5,43132,3),(5,43163,3),(5,43507,3),(5,43639,3),(5,43673,3),(5,43679,3),(5,43694,3),(5,43700,3),(5,43731,3),(5,44310,3),(5,26388,6),(5,26391,6),(5,26402,6),(5,26403,6),(5,43049,6),(5,43074,6),(5,44431,6),(5,44477,6),(5,44512,6),(5,26390,12),(5,26389,18),(6,42940,1),(6,42945,1),(6,42946,1),(6,42953,1),(6,42954,1),(6,42956,1),(6,42957,1),(6,42974,1),(6,43018,1),(6,43036,1),(6,43051,1),(6,43052,1),(6,43063,1),(6,43101,1),(6,43122,1),(6,43151,1),(6,43163,1),(6,43215,1),(6,43217,1),(6,43410,1),(6,43411,1),(6,43414,1),(6,43438,1),(6,43441,1),(6,43445,1),(6,43501,1),(6,43502,1),(6,43503,1),(6,43508,1),(6,43513,1),(6,43514,1),(6,43521,1),(6,43522,1),(6,43524,1),(6,43525,1),(6,43542,1),(6,43586,1),(6,43604,1),(6,43619,1),(6,43620,1),(6,43631,1),(6,43669,1),(6,43690,1),(6,43719,1),(6,43731,1),(6,43785,1),(6,43787,1),(6,43920,1),(6,43921,1),(6,43924,1),(6,43951,1),(6,43953,1),(6,43957,1),(6,43959,1),(6,44013,1),(6,44014,1),(6,44015,1),(6,44227,1),(6,44248,1),(6,44250,1),(6,44261,1),(6,44396,1),(6,44397,1),(6,44411,1),(6,44412,1),(6,44481,1),(6,44482,1),(6,44483,1),(6,44484,1),(6,44485,1),(6,44486,1),(6,44487,1),(6,44488,1),(6,44489,1),(6,44491,1),(6,44492,1),(6,44493,1),(6,44494,1),(6,44498,1),(6,44499,1),(6,44500,1),(6,44501,1),(6,44502,1),(6,44503,1),(6,44504,1),(6,44505,1),(6,44507,1),(6,44508,1),(6,44509,1),(6,44510,1),(6,44511,1),(6,44512,1),(6,44513,1),(6,44514,1),(6,44515,1),(6,44516,1),(6,44543,1),(6,44544,1),(6,44545,1),(6,44546,1),(6,44547,1),(6,44548,1),(6,44549,1),(6,44550,1),(6,44551,1),(6,44552,1),(6,44553,1),(6,44555,1),(6,44556,1),(6,44560,1),(6,44580,1),(6,44581,1),(6,44582,1),(6,44583,1),(6,44584,1),(6,44585,1),(6,44586,1),(6,44587,1),(6,44588,1),(6,44590,1),(6,44591,1),(6,44592,1),(6,44593,1),(6,44597,1),(6,44598,1),(6,44599,1),(6,44600,1),(6,44601,1),(6,44602,1),(6,44603,1),(6,44604,1),(6,44605,1),(6,44608,1),(6,44609,1),(6,44610,1),(6,44611,1),(6,44612,1),(6,44613,1),(6,44614,1),(6,44615,1),(6,44616,1),(6,44617,1),(6,44646,1),(6,44647,1),(6,44648,1),(6,44649,1),(6,44650,1),(6,44651,1),(6,44652,1),(6,44653,1),(6,44654,1),(6,44655,1),(6,44656,1),(6,44658,1),(6,44659,1),(6,44660,1),(6,44664,1),(6,44665,1),(6,44666,1),(6,44667,1),(6,42995,2),(6,43047,2),(6,43162,2),(6,43563,2),(6,43615,2),(6,43730,2),(6,44312,2),(6,44346,2),(6,44490,2),(6,44495,2),(6,44496,2),(6,44497,2),(6,44506,2),(6,44554,2),(6,44557,2),(6,44558,2),(6,44559,2),(6,44589,2),(6,44594,2),(6,44595,2),(6,44596,2),(6,44606,2),(6,44607,2),(6,44657,2),(6,44661,2),(6,44662,2),(6,44663,2),(6,44517,3),(6,44518,3),(6,44519,3),(6,44561,3),(6,44618,3),(6,44619,3),(6,44620,3),(6,44668,3),(6,44577,6),(6,44579,6),(6,44644,6),(6,44645,6),(6,43077,7),(6,43150,7),(6,43220,7),(6,43645,7),(6,43718,7),(6,44030,7),(6,44031,7),(6,44032,7),(6,44033,7),(6,44477,7),(6,44478,7),(6,44479,7),(6,44576,7),(6,44578,7),(6,44480,8),(8,43163,1),(8,43215,1),(8,43217,1),(8,43220,1),(8,43410,1),(8,43411,1),(8,43414,1),(8,43438,1),(8,43441,1),(8,43445,1),(8,43501,1),(8,43502,1),(8,43503,1),(8,43521,1),(8,43522,1),(8,43524,1),(8,43525,1),(8,43542,1),(8,43544,1),(8,43563,1),(8,43604,1),(8,43718,1),(8,43731,1),(8,43785,1),(8,43787,1),(8,43920,1),(8,43921,1),(8,43924,1),(8,43951,1),(8,43953,1),(8,43957,1),(8,43959,1),(8,44013,1),(8,44014,1),(8,44015,1),(8,44030,1),(8,44031,1),(8,44032,1),(8,44033,1),(8,44227,1),(8,44248,1),(8,44261,1),(8,44411,1),(8,44412,1),(8,44545,1),(8,44546,1),(8,44547,1),(8,44548,1),(8,44550,1),(8,44551,1),(8,44552,1),(8,44553,1),(8,44555,1),(8,44556,1),(8,44560,1),(8,44576,1),(8,44577,1),(8,44578,1),(8,44581,1),(8,44582,1),(8,44583,1),(8,44585,1),(8,44586,1),(8,44587,1),(8,44589,1),(8,44590,1),(8,44592,1),(8,44593,1),(8,44597,1),(8,44598,1),(8,44599,1),(8,44648,1),(8,44649,1),(8,44650,1),(8,44651,1),(8,44652,1),(8,44653,1),(8,44654,1),(8,44655,1),(8,44656,1),(8,44658,1),(8,44659,1),(8,44660,1),(8,44664,1),(8,44769,1),(8,44770,1),(8,44771,1),(8,44772,1),(8,44773,1),(8,44774,1),(8,43162,2),(8,43730,2),(8,44346,2),(8,44554,2),(8,44557,2),(8,44558,2),(8,44559,2),(8,44591,2),(8,44594,2),(8,44595,2),(8,44596,2),(8,44657,2),(8,44661,2),(8,44662,2),(8,44663,2),(8,44561,3),(8,44618,3),(8,44619,3),(8,44620,3),(8,44668,3),(8,44549,7),(8,44665,7),(8,44666,7),(8,44667,7),(8,44766,7),(8,44767,7),(8,44768,7),(9,42950,1),(9,42993,1),(9,43034,1),(9,43035,1),(9,43040,1),(9,43045,1),(9,43177,1),(9,43215,1),(9,43282,1),(9,43318,1),(9,43376,1),(9,43399,1),(9,43400,1),(9,43424,1),(9,43489,1),(9,43495,1),(9,43498,1),(9,43501,1),(9,43502,1),(9,43503,1),(9,43518,1),(9,43561,1),(9,43602,1),(9,43603,1),(9,43608,1),(9,43613,1),(9,43747,1),(9,43785,1),(9,43849,1),(9,43885,1),(9,43900,1),(9,43912,1),(9,43913,1),(9,43935,1),(9,43958,1),(9,44000,1),(9,44004,1),(9,44009,1),(9,44010,1),(9,44013,1),(9,44014,1),(9,44015,1),(9,44028,1),(9,44029,1),(9,44261,1),(9,44481,1),(9,44486,1),(9,44487,1),(9,44488,1),(9,44494,1),(9,44499,1),(9,44500,1),(9,44506,1),(9,44580,1),(9,44585,1),(9,44586,1),(9,44587,1),(9,44593,1),(9,44598,1),(9,44599,1),(9,44606,1),(9,44649,1),(9,44659,1),(9,44665,1),(9,44666,1),(9,44667,1),(9,44691,1),(9,44692,1),(9,44693,1),(9,44697,1),(9,44699,1),(9,44766,1),(9,44767,1),(9,44768,1),(9,44772,1),(9,44774,1),(9,44852,1),(9,44853,1),(9,44854,1),(9,44855,1),(9,44856,1),(9,44857,1),(9,44858,1),(9,44859,1),(9,44860,1),(9,44861,1),(9,44862,1),(9,44863,1),(9,44864,1),(9,44865,1),(9,44866,1),(9,44867,1),(9,44868,1),(9,44869,1),(9,44870,1),(9,44871,1),(9,44872,1),(9,44873,1),(9,44874,1),(9,44875,1),(9,44877,1),(9,44878,1),(9,44884,1),(9,44885,1),(9,44886,1),(9,44887,1),(9,44888,1),(9,44889,1),(9,44890,1),(9,44921,1),(9,44922,1),(9,44923,1),(9,44924,1),(9,44925,1),(9,44926,1),(9,44927,1),(9,44928,1),(9,44929,1),(9,44930,1),(9,44931,1),(9,44932,1),(9,44933,1),(9,44934,1),(9,44935,1),(9,44936,1),(9,44937,1),(9,44938,1),(9,44939,1),(9,44940,1),(9,44941,1),(9,44946,1),(9,44947,1),(9,44948,1),(9,44971,1),(9,44972,1),(9,44973,1),(9,44974,1),(9,44975,1),(9,44976,1),(9,44977,1),(9,44978,1),(9,44979,1),(9,44980,1),(9,44981,1),(9,44982,1),(9,44983,1),(9,44984,1),(9,44985,1),(9,44986,1),(9,44987,1),(9,44988,1),(9,44989,1),(9,44990,1),(9,44991,1),(9,44992,1),(9,44993,1),(9,44994,1),(9,44996,1),(9,44997,1),(9,45003,1),(9,45004,1),(9,45005,1),(9,45006,1),(9,45007,1),(9,45008,1),(9,45009,1),(9,45040,1),(9,45041,1),(9,45042,1),(9,45043,1),(9,45044,1),(9,45045,1),(9,45046,1),(9,45047,1),(9,45048,1),(9,45049,1),(9,45050,1),(9,45051,1),(9,45052,1),(9,45053,1),(9,45054,1),(9,45055,1),(9,45056,1),(9,45057,1),(9,45058,1),(9,45059,1),(9,45061,1),(9,45062,1),(9,45068,1),(9,45069,1),(9,45070,1),(9,45071,1),(9,45072,1),(9,43028,2),(9,43036,2),(9,43162,2),(9,43163,2),(9,43476,2),(9,43596,2),(9,43604,2),(9,43730,2),(9,43731,2),(9,43993,2),(9,44248,2),(9,44312,2),(9,44346,2),(9,44551,2),(9,44654,2),(9,44696,2),(9,44771,2),(9,44876,2),(9,44879,2),(9,44880,2),(9,44881,2),(9,44883,2),(9,44942,2),(9,44943,2),(9,44945,2),(9,44995,2),(9,44998,2),(9,44999,2),(9,45000,2),(9,45002,2),(9,45060,2),(9,45063,2),(9,45064,2),(9,45065,2),(9,45067,2),(9,42951,3),(9,43072,3),(9,43448,3),(9,43519,3),(9,43640,3),(9,43963,3),(9,44882,3),(9,44891,3),(9,44892,3),(9,44944,3),(9,44949,3),(9,45001,3),(9,45010,3),(9,45011,3),(9,45066,3),(9,45073,3),(9,43007,4),(9,43165,4),(9,43575,4),(9,43734,4),(9,44227,7),(9,44397,7),(9,44412,7),(9,44546,7),(9,44650,7),(9,44396,8),(9,44411,8),(10,42993,1),(10,43034,1),(10,43035,1),(10,43040,1),(10,43051,1),(10,43052,1),(10,43215,1),(10,43282,1),(10,43318,1),(10,43376,1),(10,43399,1),(10,43400,1),(10,43424,1),(10,43489,1),(10,43498,1),(10,43501,1),(10,43502,1),(10,43503,1),(10,43561,1),(10,43602,1),(10,43603,1),(10,43608,1),(10,43619,1),(10,43620,1),(10,43745,1),(10,43785,1),(10,43849,1),(10,43885,1),(10,43900,1),(10,43912,1),(10,43913,1),(10,43935,1),(10,43958,1),(10,44000,1),(10,44004,1),(10,44010,1),(10,44013,1),(10,44014,1),(10,44015,1),(10,44028,1),(10,44029,1),(10,44251,1),(10,44261,1),(10,44481,1),(10,44486,1),(10,44487,1),(10,44488,1),(10,44494,1),(10,44499,1),(10,44500,1),(10,44506,1),(10,44580,1),(10,44585,1),(10,44586,1),(10,44587,1),(10,44593,1),(10,44598,1),(10,44599,1),(10,44606,1),(10,44649,1),(10,44659,1),(10,44665,1),(10,44666,1),(10,44667,1),(10,44691,1),(10,44692,1),(10,44693,1),(10,44697,1),(10,44699,1),(10,44766,1),(10,44767,1),(10,44768,1),(10,44772,1),(10,44774,1),(10,44852,1),(10,44853,1),(10,44854,1),(10,44855,1),(10,44856,1),(10,44857,1),(10,44858,1),(10,44859,1),(10,44860,1),(10,44861,1),(10,44862,1),(10,44863,1),(10,44864,1),(10,44865,1),(10,44866,1),(10,44867,1),(10,44868,1),(10,44869,1),(10,44871,1),(10,44873,1),(10,44874,1),(10,44875,1),(10,44877,1),(10,44878,1),(10,44884,1),(10,44888,1),(10,44889,1),(10,44890,1),(10,44921,1),(10,44922,1),(10,44923,1),(10,44924,1),(10,44925,1),(10,44926,1),(10,44927,1),(10,44928,1),(10,44929,1),(10,44930,1),(10,44931,1),(10,44932,1),(10,44933,1),(10,44934,1),(10,44935,1),(10,44937,1),(10,44939,1),(10,44940,1),(10,44941,1),(10,44946,1),(10,44971,1),(10,44972,1),(10,44973,1),(10,44974,1),(10,44975,1),(10,44976,1),(10,44977,1),(10,44978,1),(10,44979,1),(10,44980,1),(10,44981,1),(10,44982,1),(10,44983,1),(10,44984,1),(10,44985,1),(10,44986,1),(10,44987,1),(10,44988,1),(10,44990,1),(10,44992,1),(10,44993,1),(10,44994,1),(10,44996,1),(10,44997,1),(10,45003,1),(10,45008,1),(10,45040,1),(10,45041,1),(10,45042,1),(10,45043,1),(10,45044,1),(10,45045,1),(10,45046,1),(10,45047,1),(10,45048,1),(10,45049,1),(10,45050,1),(10,45051,1),(10,45052,1),(10,45053,1),(10,45055,1),(10,45057,1),(10,45058,1),(10,45059,1),(10,45061,1),(10,45062,1),(10,45068,1),(10,45072,1),(10,45108,1),(10,45109,1),(10,45110,1),(10,45175,1),(10,45176,1),(10,45177,1),(10,45223,1),(10,45224,1),(10,45225,1),(10,45226,1),(10,45227,1),(10,45290,1),(10,45291,1),(10,45292,1),(10,42950,2),(10,42951,2),(10,43036,2),(10,43045,2),(10,43162,2),(10,43163,2),(10,43177,2),(10,43448,2),(10,43495,2),(10,43518,2),(10,43519,2),(10,43604,2),(10,43613,2),(10,43730,2),(10,43731,2),(10,43747,2),(10,43963,2),(10,44009,2),(10,44248,2),(10,44312,2),(10,44346,2),(10,44551,2),(10,44654,2),(10,44696,2),(10,44771,2),(10,44876,2),(10,44882,2),(10,44885,2),(10,44886,2),(10,44887,2),(10,44944,2),(10,44947,2),(10,44948,2),(10,44995,2),(10,45001,2),(10,45004,2),(10,45005,2),(10,45006,2),(10,45060,2),(10,45066,2),(10,45069,2),(10,45070,2),(10,45071,2),(10,43007,3),(10,43072,3),(10,43165,3),(10,43575,3),(10,43640,3),(10,43734,3),(10,44891,3),(10,44892,3),(10,44949,3),(10,45010,3),(10,45011,3),(10,45073,3),(10,45107,6),(10,45222,6),(10,44227,7),(10,44397,7),(10,44412,7),(10,44546,7),(10,44650,7),(10,44396,8),(10,44411,8),(11,42940,1),(11,42942,1),(11,42943,1),(11,42944,1),(11,42993,1),(11,42995,1),(11,43007,1),(11,43019,1),(11,43047,1),(11,43048,1),(11,43055,1),(11,43056,1),(11,43097,1),(11,43100,1),(11,43101,1),(11,43102,1),(11,43105,1),(11,43114,1),(11,43116,1),(11,43122,1),(11,43140,1),(11,43153,1),(11,43154,1),(11,43163,1),(11,43282,1),(11,43450,1),(11,43469,1),(11,43498,1),(11,43501,1),(11,43502,1),(11,43503,1),(11,43537,1),(11,43561,1),(11,43587,1),(11,43603,1),(11,43604,1),(11,43620,1),(11,43721,1),(11,43722,1),(11,43731,1),(11,43849,1),(11,43937,1),(11,43958,1),(11,43966,1),(11,43985,1),(11,44000,1),(11,44010,1),(11,44013,1),(11,44014,1),(11,44015,1),(11,44023,1),(11,44028,1),(11,44029,1),(11,44053,1),(11,44093,1),(11,44120,1),(11,44128,1),(11,44192,1),(11,44232,1),(11,44251,1),(11,44304,1),(11,44305,1),(11,44395,1),(11,44494,1),(11,44499,1),(11,44500,1),(11,44504,1),(11,44506,1),(11,44510,1),(11,44511,1),(11,44513,1),(11,44514,1),(11,44515,1),(11,44516,1),(11,44517,1),(11,44593,1),(11,44598,1),(11,44599,1),(11,44605,1),(11,44606,1),(11,44613,1),(11,44616,1),(11,44618,1),(11,44659,1),(11,44774,1),(11,44876,1),(11,44884,1),(11,44889,1),(11,44890,1),(11,44921,1),(11,44922,1),(11,44923,1),(11,44924,1),(11,44937,1),(11,44938,1),(11,44939,1),(11,44940,1),(11,44941,1),(11,44946,1),(11,44971,1),(11,44972,1),(11,44973,1),(11,44974,1),(11,44976,1),(11,44990,1),(11,44991,1),(11,44992,1),(11,44994,1),(11,44995,1),(11,44996,1),(11,44997,1),(11,45003,1),(11,45008,1),(11,45040,1),(11,45041,1),(11,45042,1),(11,45043,1),(11,45055,1),(11,45056,1),(11,45057,1),(11,45059,1),(11,45060,1),(11,45061,1),(11,45062,1),(11,45068,1),(11,45227,1),(11,45356,1),(11,45357,1),(11,45358,1),(11,45359,1),(11,45360,1),(11,45361,1),(11,45362,1),(11,45363,1),(11,45364,1),(11,45365,1),(11,45366,1),(11,45369,1),(11,45370,1),(11,45371,1),(11,45372,1),(11,45374,1),(11,45375,1),(11,45376,1),(11,45377,1),(11,45378,1),(11,45379,1),(11,45380,1),(11,45382,1),(11,45383,1),(11,45385,1),(11,45386,1),(11,45387,1),(11,45388,1),(11,45391,1),(11,45392,1),(11,45393,1),(11,45394,1),(11,45395,1),(11,45396,1),(11,45397,1),(11,45398,1),(11,45399,1),(11,45400,1),(11,45401,1),(11,45402,1),(11,45403,1),(11,45404,1),(11,45405,1),(11,45406,1),(11,45407,1),(11,45408,1),(11,45410,1),(11,45411,1),(11,45412,1),(11,45413,1),(11,45414,1),(11,45415,1),(11,45416,1),(11,45475,1),(11,45476,1),(11,45477,1),(11,45478,1),(11,45479,1),(11,45480,1),(11,45481,1),(11,45482,1),(11,45483,1),(11,45484,1),(11,45485,1),(11,45518,1),(11,45519,1),(11,45520,1),(11,45521,1),(11,45522,1),(11,45523,1),(11,45524,1),(11,45525,1),(11,45526,1),(11,45527,1),(11,45528,1),(11,45532,1),(11,45534,1),(11,45535,1),(11,45537,1),(11,45538,1),(11,45539,1),(11,45540,1),(11,45541,1),(11,45542,1),(11,45543,1),(11,45544,1),(11,45545,1),(11,45546,1),(11,45547,1),(11,45548,1),(11,45549,1),(11,45550,1),(11,45551,1),(11,45552,1),(11,45553,1),(11,45555,1),(11,45556,1),(11,45557,1),(11,45558,1),(11,45559,1),(11,45560,1),(11,45561,1),(11,45562,1),(11,45563,1),(11,45613,1),(11,45614,1),(11,45615,1),(11,45616,1),(11,45617,1),(11,45618,1),(11,45619,1),(11,45620,1),(11,45621,1),(11,45622,1),(11,45624,1),(11,45627,1),(11,45628,1),(11,45630,1),(11,45631,1),(11,45632,1),(11,45633,1),(11,45634,1),(11,45635,1),(11,45636,1),(11,45637,1),(11,45638,1),(11,45639,1),(11,45640,1),(11,45641,1),(11,45642,1),(11,45643,1),(11,45644,1),(11,45645,1),(11,45646,1),(11,45647,1),(11,45648,1),(11,45649,1),(11,45650,1),(11,45651,1),(11,43051,2),(11,43063,2),(11,43159,2),(11,43165,2),(11,43167,2),(11,43177,2),(11,43448,2),(11,43495,2),(11,43518,2),(11,43519,2),(11,43575,2),(11,43613,2),(11,43669,2),(11,43734,2),(11,43736,2),(11,43747,2),(11,43963,2),(11,44009,2),(11,44036,2),(11,44225,2),(11,44250,2),(11,44457,2),(11,44509,2),(11,44512,2),(11,44936,2),(11,44944,2),(11,44945,2),(11,44989,2),(11,45001,2),(11,45002,2),(11,45054,2),(11,45066,2),(11,45067,2),(11,45367,2),(11,45368,2),(11,45373,2),(11,45384,2),(11,45389,2),(11,45390,2),(11,45474,2),(11,45486,2),(11,45517,2),(11,45529,2),(11,45530,2),(11,45536,2),(11,45554,2),(11,45612,2),(11,45623,2),(11,45629,2),(11,43052,3),(11,43073,3),(11,43640,3),(11,44031,3),(11,44431,3),(11,44576,3),(11,45010,3),(11,45011,3),(11,45381,3),(11,45409,3),(11,45533,3),(11,45626,3),(11,42945,4),(11,44477,4),(11,44891,4),(11,44892,4),(11,44949,4),(11,45073,4),(11,45531,4),(11,45625,4),(11,43072,5),(11,42946,6),(11,45355,6),(11,45473,6),(11,45516,6),(11,45611,6),(11,44311,7),(11,44312,7),(11,44338,7),(11,44345,7),(11,44371,7),(11,43162,8),(11,43730,8),(11,44346,8),(12,42950,1),(12,42952,1),(12,42969,1),(12,42990,1),(12,42992,1),(12,42993,1),(12,43018,1),(12,43019,1),(12,43035,1),(12,43036,1),(12,43073,1),(12,43105,1),(12,43153,1),(12,43154,1),(12,43163,1),(12,43177,1),(12,43282,1),(12,43421,1),(12,43449,1),(12,43450,1),(12,43468,1),(12,43469,1),(12,43498,1),(12,43501,1),(12,43502,1),(12,43503,1),(12,43518,1),(12,43520,1),(12,43537,1),(12,43558,1),(12,43560,1),(12,43561,1),(12,43586,1),(12,43587,1),(12,43603,1),(12,43604,1),(12,43721,1),(12,43722,1),(12,43731,1),(12,43747,1),(12,43849,1),(12,43932,1),(12,43937,1),(12,43958,1),(12,43964,1),(12,43965,1),(12,43966,1),(12,43984,1),(12,43985,1),(12,44000,1),(12,44010,1),(12,44013,1),(12,44014,1),(12,44015,1),(12,44023,1),(12,44028,1),(12,44029,1),(12,44053,1),(12,44093,1),(12,44120,1),(12,44128,1),(12,44192,1),(12,44232,1),(12,44304,1),(12,44305,1),(12,44494,1),(12,44499,1),(12,44500,1),(12,44505,1),(12,44506,1),(12,44512,1),(12,44515,1),(12,44517,1),(12,44593,1),(12,44598,1),(12,44599,1),(12,44605,1),(12,44606,1),(12,44613,1),(12,44616,1),(12,44618,1),(12,44659,1),(12,44699,1),(12,44774,1),(12,44852,1),(12,44853,1),(12,44855,1),(12,44857,1),(12,44871,1),(12,44872,1),(12,44873,1),(12,44875,1),(12,44876,1),(12,44877,1),(12,44878,1),(12,44885,1),(12,44889,1),(12,44890,1),(12,44921,1),(12,44922,1),(12,44924,1),(12,44937,1),(12,44938,1),(12,44939,1),(12,44940,1),(12,44941,1),(12,44971,1),(12,44972,1),(12,44974,1),(12,44976,1),(12,44990,1),(12,44991,1),(12,44992,1),(12,44994,1),(12,44995,1),(12,44996,1),(12,44997,1),(12,45004,1),(12,45008,1),(12,45040,1),(12,45041,1),(12,45043,1),(12,45055,1),(12,45056,1),(12,45057,1),(12,45059,1),(12,45060,1),(12,45061,1),(12,45062,1),(12,45069,1),(12,45227,1),(12,45356,1),(12,45358,1),(12,45367,1),(12,45368,1),(12,45382,1),(12,45383,1),(12,45385,1),(12,45386,1),(12,45387,1),(12,45388,1),(12,45389,1),(12,45390,1),(12,45391,1),(12,45392,1),(12,45393,1),(12,45394,1),(12,45395,1),(12,45396,1),(12,45397,1),(12,45398,1),(12,45399,1),(12,45400,1),(12,45401,1),(12,45402,1),(12,45403,1),(12,45404,1),(12,45405,1),(12,45406,1),(12,45414,1),(12,45475,1),(12,45476,1),(12,45477,1),(12,45478,1),(12,45479,1),(12,45480,1),(12,45481,1),(12,45482,1),(12,45483,1),(12,45484,1),(12,45485,1),(12,45518,1),(12,45519,1),(12,45520,1),(12,45521,1),(12,45522,1),(12,45523,1),(12,45524,1),(12,45525,1),(12,45526,1),(12,45532,1),(12,45534,1),(12,45535,1),(12,45537,1),(12,45538,1),(12,45539,1),(12,45540,1),(12,45541,1),(12,45542,1),(12,45543,1),(12,45544,1),(12,45545,1),(12,45546,1),(12,45547,1),(12,45548,1),(12,45549,1),(12,45550,1),(12,45551,1),(12,45552,1),(12,45553,1),(12,45555,1),(12,45556,1),(12,45557,1),(12,45558,1),(12,45559,1),(12,45560,1),(12,45561,1),(12,45562,1),(12,45563,1),(12,45613,1),(12,45614,1),(12,45615,1),(12,45616,1),(12,45617,1),(12,45618,1),(12,45619,1),(12,45620,1),(12,45624,1),(12,45627,1),(12,45628,1),(12,45630,1),(12,45631,1),(12,45632,1),(12,45633,1),(12,45634,1),(12,45635,1),(12,45636,1),(12,45637,1),(12,45638,1),(12,45639,1),(12,45640,1),(12,45641,1),(12,45642,1),(12,45643,1),(12,45644,1),(12,45645,1),(12,45646,1),(12,45647,1),(12,45648,1),(12,45649,1),(12,45650,1),(12,45651,1),(12,45707,1),(12,45708,1),(12,45709,1),(12,45710,1),(12,45711,1),(12,45712,1),(12,45713,1),(12,45715,1),(12,45716,1),(12,45717,1),(12,45718,1),(12,45719,1),(12,45720,1),(12,45816,1),(12,45817,1),(12,45818,1),(12,45819,1),(12,45820,1),(12,45870,1),(12,45871,1),(12,45872,1),(12,45873,1),(12,45874,1),(12,45875,1),(12,45979,1),(12,45981,1),(12,45982,1),(12,45983,1),(12,45984,1),(12,45985,1),(12,42946,2),(12,43045,2),(12,43060,2),(12,43101,2),(12,43495,2),(12,43613,2),(12,43659,2),(12,43669,2),(12,44009,2),(12,44036,2),(12,44064,2),(12,44076,2),(12,44225,2),(12,44250,2),(12,44431,2),(12,44457,2),(12,44854,2),(12,44870,2),(12,44879,2),(12,44883,2),(12,44886,2),(12,44923,2),(12,44936,2),(12,44942,2),(12,44945,2),(12,44947,2),(12,44973,2),(12,44989,2),(12,44998,2),(12,45002,2),(12,45005,2),(12,45042,2),(12,45054,2),(12,45063,2),(12,45067,2),(12,45070,2),(12,45384,2),(12,45474,2),(12,45517,2),(12,45536,2),(12,45554,2),(12,45612,2),(12,45629,2),(12,45706,2),(12,45714,2),(12,45722,2),(12,45723,2),(12,45724,2),(12,45815,2),(12,45821,2),(12,45869,2),(12,45876,2),(12,45877,2),(12,45878,2),(12,45980,2),(12,45986,2),(12,45987,2),(12,42951,3),(12,43007,3),(12,43052,3),(12,43072,3),(12,43165,3),(12,43448,3),(12,43519,3),(12,43575,3),(12,43620,3),(12,43640,3),(12,43734,3),(12,43963,3),(12,44031,3),(12,44251,3),(12,44477,3),(12,44576,3),(12,44882,3),(12,44891,3),(12,44892,3),(12,44944,3),(12,45001,3),(12,45010,3),(12,45011,3),(12,45066,3),(12,45381,3),(12,45533,3),(12,45626,3),(12,44949,4),(12,45073,4),(12,45531,4),(12,45625,4),(12,45721,4),(12,45703,6),(12,45813,6),(12,45866,6),(12,45976,6),(12,44311,7),(12,44338,7),(12,44345,7),(12,44371,7),(12,43162,8),(12,43730,8),(12,44312,8),(12,44346,8),(12,45704,10),(12,45705,10),(12,45814,10),(12,45867,10),(12,45868,10),(12,45977,10),(12,45978,10),(13,945,1),(13,1069,1),(13,1140,1),(13,1164,1),(13,42942,1),(13,42944,1),(13,42945,1),(13,42946,1),(13,42994,1),(13,43018,1),(13,43048,1),(13,43049,1),(13,43054,1),(13,43060,1),(13,43061,1),(13,43073,1),(13,43087,1),(13,43092,1),(13,43098,1),(13,43101,1),(13,43105,1),(13,43111,1),(13,43120,1),(13,43121,1),(13,43122,1),(13,43127,1),(13,43140,1),(13,43149,1),(13,43215,1),(13,43217,1),(13,43227,1),(13,43317,1),(13,43318,1),(13,43376,1),(13,43392,1),(13,43399,1),(13,43400,1),(13,43412,1),(13,43424,1),(13,43468,1),(13,43469,1),(13,43495,1),(13,43501,1),(13,43502,1),(13,43503,1),(13,43510,1),(13,43512,1),(13,43513,1),(13,43514,1),(13,43562,1),(13,43586,1),(13,43616,1),(13,43617,1),(13,43622,1),(13,43628,1),(13,43629,1),(13,43641,1),(13,43655,1),(13,43660,1),(13,43666,1),(13,43669,1),(13,43673,1),(13,43679,1),(13,43688,1),(13,43689,1),(13,43690,1),(13,43695,1),(13,43708,1),(13,43717,1),(13,43785,1),(13,43787,1),(13,43794,1),(13,43884,1),(13,43885,1),(13,43900,1),(13,43909,1),(13,43912,1),(13,43913,1),(13,43922,1),(13,43935,1),(13,43984,1),(13,43985,1),(13,44009,1),(13,44013,1),(13,44014,1),(13,44015,1),(13,44028,1),(13,44029,1),(13,44227,1),(13,44248,1),(13,44250,1),(13,44261,1),(13,44394,1),(13,44396,1),(13,44397,1),(13,44409,1),(13,44411,1),(13,44412,1),(13,44430,1),(13,44456,1),(13,44508,1),(13,44511,1),(13,44513,1),(13,44514,1),(13,44515,1),(13,44519,1),(13,44546,1),(13,44547,1),(13,44548,1),(13,44551,1),(13,44609,1),(13,44612,1),(13,44614,1),(13,44615,1),(13,44616,1),(13,44620,1),(13,44649,1),(13,44650,1),(13,44651,1),(13,44652,1),(13,44654,1),(13,44889,1),(13,44923,1),(13,44924,1),(13,44925,1),(13,44926,1),(13,44927,1),(13,44928,1),(13,44931,1),(13,44932,1),(13,44933,1),(13,44934,1),(13,44935,1),(13,44944,1),(13,44946,1),(13,45008,1),(13,45042,1),(13,45043,1),(13,45044,1),(13,45045,1),(13,45046,1),(13,45047,1),(13,45049,1),(13,45050,1),(13,45051,1),(13,45052,1),(13,45053,1),(13,45066,1),(13,45068,1),(13,45365,1),(13,45369,1),(13,45371,1),(13,45372,1),(13,45373,1),(13,45391,1),(13,45478,1),(13,45616,1),(13,46084,1),(13,46085,1),(13,46086,1),(13,46087,1),(13,46088,1),(13,46089,1),(13,46090,1),(13,46091,1),(13,46101,1),(13,46103,1),(13,46104,1),(13,46108,1),(13,46109,1),(13,46110,1),(13,46111,1),(13,46113,1),(13,46114,1),(13,46115,1),(13,46116,1),(13,46117,1),(13,46119,1),(13,46120,1),(13,46121,1),(13,46122,1),(13,46123,1),(13,46124,1),(13,46125,1),(13,46126,1),(13,46127,1),(13,46128,1),(13,46129,1),(13,46130,1),(13,46131,1),(13,46133,1),(13,46134,1),(13,46135,1),(13,46136,1),(13,46137,1),(13,46138,1),(13,46139,1),(13,46140,1),(13,46141,1),(13,46142,1),(13,46143,1),(13,46146,1),(13,46147,1),(13,46148,1),(13,46149,1),(13,46150,1),(13,46151,1),(13,46152,1),(13,46153,1),(13,46156,1),(13,46157,1),(13,46158,1),(13,46159,1),(13,46160,1),(13,46161,1),(13,46162,1),(13,46163,1),(13,46164,1),(13,46165,1),(13,46166,1),(13,46167,1),(13,46168,1),(13,46169,1),(13,46170,1),(13,46171,1),(13,46172,1),(13,46173,1),(13,46174,1),(13,46175,1),(13,46176,1),(13,46177,1),(13,46240,1),(13,46241,1),(13,46242,1),(13,46243,1),(13,46244,1),(13,46245,1),(13,46246,1),(13,46248,1),(13,46249,1),(13,46250,1),(13,46251,1),(13,46252,1),(13,46254,1),(13,46255,1),(13,46256,1),(13,46257,1),(13,46258,1),(13,46259,1),(13,46260,1),(13,46261,1),(13,46262,1),(13,46263,1),(13,46264,1),(13,46265,1),(13,46266,1),(13,46267,1),(13,46268,1),(13,46269,1),(13,46270,1),(13,46271,1),(13,46272,1),(13,46273,1),(13,46274,1),(13,46275,1),(13,46276,1),(13,46277,1),(13,46278,1),(13,46279,1),(13,46322,1),(13,46323,1),(13,46324,1),(13,46325,1),(13,46326,1),(13,46327,1),(13,46328,1),(13,46329,1),(13,46338,1),(13,46340,1),(13,46341,1),(13,46345,1),(13,46346,1),(13,46347,1),(13,46348,1),(13,46349,1),(13,46353,1),(13,46354,1),(13,46355,1),(13,46356,1),(13,46357,1),(13,46359,1),(13,46360,1),(13,46361,1),(13,46362,1),(13,46363,1),(13,46364,1),(13,46365,1),(13,46366,1),(13,46367,1),(13,46368,1),(13,46369,1),(13,46370,1),(13,46371,1),(13,46373,1),(13,46374,1),(13,46375,1),(13,46376,1),(13,46377,1),(13,46378,1),(13,46379,1),(13,46380,1),(13,46381,1),(13,46382,1),(13,46383,1),(13,46384,1),(13,46385,1),(13,46386,1),(13,46387,1),(13,46390,1),(13,46391,1),(13,46392,1),(13,46394,1),(13,46395,1),(13,46396,1),(13,46397,1),(13,46398,1),(13,46399,1),(13,46402,1),(13,46403,1),(13,46404,1),(13,46405,1),(13,46406,1),(13,46407,1),(13,46408,1),(13,46409,1),(13,46410,1),(13,46411,1),(13,46412,1),(13,46413,1),(13,46414,1),(13,46415,1),(13,46416,1),(13,46417,1),(13,46418,1),(13,46419,1),(13,46420,1),(13,46421,1),(13,46422,1),(13,46423,1),(13,46479,1),(13,46480,1),(13,46481,1),(13,46482,1),(13,46483,1),(13,46484,1),(13,46485,1),(13,46487,1),(13,46488,1),(13,46489,1),(13,46490,1),(13,46491,1),(13,46492,1),(13,46493,1),(13,46495,1),(13,46496,1),(13,46497,1),(13,46498,1),(13,46499,1),(13,46500,1),(13,46501,1),(13,46502,1),(13,46503,1),(13,46504,1),(13,46505,1),(13,46506,1),(13,46507,1),(13,46508,1),(13,46509,1),(13,46512,1),(13,46513,1),(13,46514,1),(13,46515,1),(13,46516,1),(13,46517,1),(13,46518,1),(13,46519,1),(13,46520,1),(13,46521,1),(13,46522,1),(13,46523,1),(13,46524,1),(13,46525,1),(13,46526,1),(13,1141,2),(13,2546,2),(13,2966,2),(13,43035,2),(13,43047,2),(13,43110,2),(13,43163,2),(13,43165,2),(13,43498,2),(13,43603,2),(13,43615,2),(13,43678,2),(13,43731,2),(13,43734,2),(13,44010,2),(13,44312,2),(13,44346,2),(13,44395,2),(13,44410,2),(13,44506,2),(13,44509,2),(13,44512,2),(13,44606,2),(13,44610,2),(13,44613,2),(13,45367,2),(13,45368,2),(13,45375,2),(13,45384,2),(13,45544,2),(13,46083,2),(13,46098,2),(13,46099,2),(13,46102,2),(13,46105,2),(13,46106,2),(13,46107,2),(13,46118,2),(13,46132,2),(13,46144,2),(13,46145,2),(13,46178,2),(13,46179,2),(13,46247,2),(13,46253,2),(13,46321,2),(13,46335,2),(13,46336,2),(13,46339,2),(13,46342,2),(13,46343,2),(13,46344,2),(13,46351,2),(13,46352,2),(13,46358,2),(13,46372,2),(13,46388,2),(13,46389,2),(13,46393,2),(13,46424,2),(13,46425,2),(13,46486,2),(13,46494,2),(13,43019,3),(13,43587,3),(13,44230,3),(13,44600,3),(13,45061,3),(13,45876,3),(13,46092,3),(13,46093,3),(13,46094,3),(13,46097,3),(13,46112,3),(13,46280,3),(13,46330,3),(13,46331,3),(13,46332,3),(13,46350,3),(13,46510,3),(13,46511,3),(13,46527,3),(13,42943,4),(13,43162,4),(13,43511,4),(13,43730,4),(13,44000,4),(13,44516,4),(13,44617,4),(13,46095,4),(13,46096,4),(13,46333,4),(13,46334,4),(13,44577,6),(13,46100,6),(13,46337,6),(13,43220,7),(13,44030,7),(13,44031,7),(13,44032,7),(13,44033,7),(13,46154,7),(13,46155,7),(13,46400,7),(13,46401,7),(13,43150,9),(13,43718,9),(13,44478,9),(13,44479,9),(13,44578,9),(13,44477,10),(13,44576,10),(14,43215,1),(14,43227,1),(14,43318,1),(14,43376,1),(14,43399,1),(14,43424,1),(14,43489,1),(14,43503,1),(14,43602,1),(14,43603,1),(14,43608,1),(14,43619,1),(14,43670,1),(14,43745,1),(14,43785,1),(14,43794,1),(14,43885,1),(14,43900,1),(14,43912,1),(14,43935,1),(14,44000,1),(14,44004,1),(14,44015,1),(14,44028,1),(14,44029,1),(14,44560,1),(14,44580,1),(14,44585,1),(14,44597,1),(14,44649,1),(14,44664,1),(14,44665,1),(14,44666,1),(14,44667,1),(14,44766,1),(14,44767,1),(14,44768,1),(14,44772,1),(14,44774,1),(14,44923,1),(14,44926,1),(14,44927,1),(14,44928,1),(14,44929,1),(14,44930,1),(14,44931,1),(14,44932,1),(14,44933,1),(14,44934,1),(14,44935,1),(14,44944,1),(14,44973,1),(14,44977,1),(14,44978,1),(14,44979,1),(14,44980,1),(14,44981,1),(14,44982,1),(14,44983,1),(14,44984,1),(14,44985,1),(14,44986,1),(14,44987,1),(14,45001,1),(14,45008,1),(14,45042,1),(14,45045,1),(14,45046,1),(14,45047,1),(14,45048,1),(14,45049,1),(14,45050,1),(14,45051,1),(14,45052,1),(14,45053,1),(14,45066,1),(14,45072,1),(14,45226,1),(14,45227,1),(14,46248,1),(14,46268,1),(14,46487,1),(14,46509,1),(14,46646,1),(14,46647,1),(14,46648,1),(14,46649,1),(14,46650,1),(14,46651,1),(14,46652,1),(14,46653,1),(14,46654,1),(14,46655,1),(14,46656,1),(14,46658,1),(14,46659,1),(14,46660,1),(14,46661,1),(14,46694,1),(14,46695,1),(14,46696,1),(14,46697,1),(14,46698,1),(14,46699,1),(14,46700,1),(14,46701,1),(14,46702,1),(14,46703,1),(14,46704,1),(14,46705,1),(14,46706,1),(14,46707,1),(14,46708,1),(14,46709,1),(14,46710,1),(14,46711,1),(14,46712,1),(14,46713,1),(14,46714,1),(14,46715,1),(14,46759,1),(14,46760,1),(14,46761,1),(14,46762,1),(14,46763,1),(14,46764,1),(14,46765,1),(14,46766,1),(14,46767,1),(14,46768,1),(14,46769,1),(14,46770,1),(14,46772,1),(14,46773,1),(14,46774,1),(14,46775,1),(14,46776,1),(14,46777,1),(14,46778,1),(14,46779,1),(14,43034,2),(14,43035,2),(14,43040,2),(14,43051,2),(14,43102,2),(14,43162,2),(14,43163,2),(14,43165,2),(14,43400,2),(14,43498,2),(14,43501,2),(14,43502,2),(14,43575,2),(14,43604,2),(14,43730,2),(14,43731,2),(14,43734,2),(14,43913,2),(14,44010,2),(14,44013,2),(14,44014,2),(14,44261,2),(14,44346,2),(14,44481,2),(14,44486,2),(14,44498,2),(14,44507,2),(14,44551,2),(14,44586,2),(14,44587,2),(14,44598,2),(14,44599,2),(14,44654,2),(14,44691,2),(14,44692,2),(14,44693,2),(14,44697,2),(14,44699,2),(14,44771,2),(14,44854,2),(14,44858,2),(14,44859,2),(14,44860,2),(14,44861,2),(14,44862,2),(14,44863,2),(14,44864,2),(14,44865,2),(14,44866,2),(14,44867,2),(14,44868,2),(14,44882,2),(14,44888,2),(14,44889,2),(14,44890,2),(14,44976,2),(14,44988,2),(14,46581,2),(14,46582,2),(14,46583,2),(14,46584,2),(14,46585,2),(14,46586,2),(14,46587,2),(14,46588,2),(14,46589,2),(14,46590,2),(14,46591,2),(14,46592,2),(14,46593,2),(14,46594,2),(14,46595,2),(14,46596,2),(14,46597,2),(14,46598,2),(14,46599,2),(14,46600,2),(14,46601,2),(14,44248,3),(14,46603,3),(14,46717,3),(14,43007,4),(14,43036,4),(14,44312,4),(14,44487,4),(14,44488,4),(14,44499,4),(14,44500,4),(14,44696,4),(14,44857,4),(14,44869,4),(14,46657,4),(14,46716,4),(14,46771,4),(14,46602,5),(14,44227,8),(14,44412,8),(14,44546,8),(14,44650,8),(14,44411,9),(14,44397,10),(14,44396,12),(15,43645,2),(15,44245,2),(15,44247,2),(15,44262,2),(15,44266,2),(15,44645,2),(15,46362,2),(15,46840,2),(15,46841,2),(15,46842,2),(15,46843,2),(15,46844,2),(15,46853,2),(15,46854,2),(15,46657,3),(15,46716,3),(15,46717,3),(15,46771,3),(15,44261,6),(15,44412,6),(15,46838,6),(15,46839,6),(15,46851,6),(15,46852,6),(15,44248,8),(15,44411,8),(17,2546,3),(17,2966,3),(17,3817,3),(17,4388,3),(17,26263,3),(17,26264,3),(17,26267,3),(17,26268,3),(17,26269,3),(17,26273,3),(19,2546,3),(19,2966,3),(19,3817,3),(19,4388,3),(19,26263,3),(19,26264,3),(19,26267,3),(19,26268,3),(19,26269,3),(19,26273,3),(21,2546,3),(21,2966,3),(21,3817,3),(21,4388,3),(21,26263,3),(21,26264,3),(21,26267,3),(21,26268,3),(21,26269,3),(21,26273,3),(21,26377,6);
/*!40000 ALTER TABLE `ps_search_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_search_index_cms`
--

DROP TABLE IF EXISTS `ps_search_index_cms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_search_index_cms` (
  `id_cms` int(11) unsigned NOT NULL,
  `id_word` int(11) unsigned NOT NULL,
  `weight` smallint(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_word`,`id_cms`),
  KEY `id_cms` (`id_cms`,`weight`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_search_index_cms`
--

LOCK TABLES `ps_search_index_cms` WRITE;
/*!40000 ALTER TABLE `ps_search_index_cms` DISABLE KEYS */;
INSERT INTO `ps_search_index_cms` VALUES (1,5,5),(1,6,5),(1,7,5),(1,8,5),(1,9,5),(1,10,5),(1,11,5),(1,14,5),(1,16,5),(1,17,5),(1,18,5),(1,19,5),(1,20,5),(1,21,5),(1,22,5),(1,25,5),(1,26,5),(1,27,5),(1,28,5),(1,29,5),(1,30,5),(1,39,5),(1,40,5),(1,44,5),(1,46,5),(1,47,5),(1,48,5),(1,51,5),(1,54,5),(1,55,5),(1,56,5),(1,57,5),(1,58,5),(1,59,5),(1,60,5),(1,61,5),(1,63,5),(1,64,5),(1,65,5),(1,66,5),(1,67,5),(1,68,5),(1,69,5),(1,70,5),(1,71,5),(1,72,5),(1,74,5),(1,75,5),(1,77,5),(1,78,5),(1,79,5),(1,80,5),(1,81,5),(1,82,5),(1,84,5),(1,85,5),(1,86,5),(1,88,5),(1,89,5),(1,90,5),(1,91,5),(1,92,5),(1,93,5),(1,95,5),(1,96,5),(1,97,5),(1,98,5),(1,99,5),(1,100,5),(1,101,5),(1,102,5),(1,103,5),(1,104,5),(1,105,5),(1,107,5),(1,110,5),(1,112,5),(1,114,5),(1,115,5),(1,116,5),(1,117,5),(1,118,5),(1,119,5),(1,120,5),(1,121,5),(1,122,5),(1,123,5),(1,126,5),(1,127,5),(1,128,5),(1,129,5),(1,130,5),(1,131,5),(1,132,5),(1,133,5),(1,134,5),(1,135,5),(1,136,5),(1,137,5),(1,138,5),(1,139,5),(1,140,5),(1,141,5),(1,142,5),(1,143,5),(1,144,5),(1,145,5),(1,146,5),(1,147,5),(1,148,5),(1,150,5),(1,151,5),(1,153,5),(1,154,5),(1,155,5),(1,156,5),(1,157,5),(1,158,5),(1,159,5),(1,161,5),(1,162,5),(1,163,5),(1,165,5),(1,166,5),(1,167,5),(1,168,5),(1,169,5),(1,170,5),(1,172,5),(1,173,5),(1,174,5),(1,175,5),(1,176,5),(1,177,5),(1,178,5),(1,179,5),(1,180,5),(1,181,5),(1,182,5),(1,183,5),(1,185,5),(1,188,5),(1,190,5),(1,192,5),(1,193,5),(1,194,5),(1,195,5),(1,196,5),(1,197,5),(1,198,5),(1,199,5),(1,200,5),(1,201,5),(1,204,5),(1,205,5),(1,206,5),(1,207,5),(1,208,5),(1,209,5),(1,210,5),(1,211,5),(1,212,5),(1,213,5),(1,214,5),(1,215,5),(1,216,5),(1,217,5),(1,218,5),(1,219,5),(1,220,5),(1,221,5),(1,222,5),(1,223,5),(1,224,5),(1,225,5),(1,226,5),(1,228,5),(1,229,5),(1,231,5),(1,232,5),(1,233,5),(1,234,5),(1,235,5),(1,236,5),(1,237,5),(1,239,5),(1,240,5),(1,241,5),(1,243,5),(1,244,5),(1,245,5),(1,246,5),(1,247,5),(1,248,5),(1,250,5),(1,251,5),(1,252,5),(1,253,5),(1,254,5),(1,255,5),(1,256,5),(1,257,5),(1,258,5),(1,259,5),(1,260,5),(1,261,5),(1,263,5),(1,266,5),(1,268,5),(1,270,5),(1,271,5),(1,272,5),(1,273,5),(1,274,5),(1,275,5),(1,276,5),(1,277,5),(1,278,5),(1,279,5),(1,282,5),(1,283,5),(1,284,5),(1,285,5),(1,286,5),(1,287,5),(1,288,5),(1,289,5),(1,290,5),(1,291,5),(1,292,5),(1,293,5),(1,294,5),(1,295,5),(1,296,5),(1,297,5),(1,298,5),(1,299,5),(1,300,5),(1,13,10),(1,15,10),(1,31,10),(1,32,10),(1,41,10),(1,42,10),(1,43,10),(1,49,10),(1,50,10),(1,53,10),(1,62,10),(1,76,10),(1,83,10),(1,87,10),(1,106,10),(1,108,10),(1,111,10),(1,113,10),(1,124,10),(1,125,10),(1,152,10),(1,160,10),(1,164,10),(1,184,10),(1,186,10),(1,189,10),(1,191,10),(1,202,10),(1,203,10),(1,230,10),(1,238,10),(1,242,10),(1,262,10),(1,264,10),(1,267,10),(1,269,10),(1,280,10),(1,281,10),(1,3,15),(1,4,15),(1,12,15),(1,23,15),(1,24,15),(1,33,15),(1,38,15),(1,73,15),(1,149,15),(1,227,15),(1,2,20),(1,34,20),(1,35,20),(1,45,20),(1,52,20),(1,94,20),(1,171,20),(1,249,20),(1,36,25),(1,37,25),(1,109,25),(1,187,25),(1,265,25),(1,1,30),(2,7,5),(2,8,5),(2,9,5),(2,14,5),(2,15,5),(2,29,5),(2,32,5),(2,63,5),(2,65,5),(2,305,5),(2,306,5),(2,307,5),(2,308,5),(2,309,5),(2,310,5),(2,311,5),(2,312,5),(2,313,5),(2,314,5),(2,315,5),(2,316,5),(2,326,5),(2,330,5),(2,331,5),(2,332,5),(2,334,5),(2,335,5),(2,338,5),(2,339,5),(2,343,5),(2,344,5),(2,348,5),(2,349,5),(2,351,5),(2,352,5),(2,353,5),(2,354,5),(2,355,5),(2,357,5),(2,358,5),(2,359,5),(2,360,5),(2,361,5),(2,362,5),(2,363,5),(2,364,5),(2,365,5),(2,366,5),(2,367,5),(2,368,5),(2,369,5),(2,370,5),(2,371,5),(2,372,5),(2,373,5),(2,374,5),(2,375,5),(2,376,5),(2,377,5),(2,378,5),(2,379,5),(2,380,5),(2,381,5),(2,382,5),(2,383,5),(2,384,5),(2,385,5),(2,386,5),(2,387,5),(2,388,5),(2,389,5),(2,408,5),(2,409,5),(2,410,5),(2,411,5),(2,412,5),(2,413,5),(2,414,5),(2,415,5),(2,416,5),(2,417,5),(2,418,5),(2,419,5),(2,420,5),(2,421,5),(2,422,5),(2,423,5),(2,424,5),(2,425,5),(2,13,10),(2,37,10),(2,42,10),(2,52,10),(2,301,10),(2,302,10),(2,303,10),(2,318,10),(2,322,10),(2,325,10),(2,327,10),(2,328,10),(2,337,10),(2,341,10),(2,342,10),(2,345,10),(2,346,10),(2,347,10),(2,350,10),(2,356,10),(2,45,15),(2,66,15),(2,319,15),(2,320,15),(2,321,15),(2,323,15),(2,324,15),(2,329,15),(2,333,15),(2,336,15),(2,340,15),(2,49,20),(2,24,25),(2,317,25),(2,25,35),(2,304,35),(3,2,5),(3,15,5),(3,23,5),(3,49,5),(3,60,5),(3,61,5),(3,63,5),(3,67,5),(3,154,5),(3,301,5),(3,308,5),(3,322,5),(3,325,5),(3,337,5),(3,358,5),(3,369,5),(3,376,5),(3,382,5),(3,430,5),(3,431,5),(3,432,5),(3,434,5),(3,435,5),(3,436,5),(3,437,5),(3,439,5),(3,440,5),(3,441,5),(3,442,5),(3,443,5),(3,444,5),(3,445,5),(3,446,5),(3,447,5),(3,448,5),(3,449,5),(3,456,5),(3,457,5),(3,458,5),(3,459,5),(3,462,5),(3,465,5),(3,466,5),(3,467,5),(3,469,5),(3,470,5),(3,471,5),(3,472,5),(3,473,5),(3,474,5),(3,475,5),(3,479,5),(3,482,5),(3,483,5),(3,484,5),(3,485,5),(3,487,5),(3,490,5),(3,491,5),(3,492,5),(3,494,5),(3,495,5),(3,496,5),(3,498,5),(3,499,5),(3,500,5),(3,501,5),(3,502,5),(3,503,5),(3,504,5),(3,505,5),(3,506,5),(3,507,5),(3,508,5),(3,509,5),(3,510,5),(3,511,5),(3,512,5),(3,513,5),(3,514,5),(3,560,5),(3,561,5),(3,562,5),(3,563,5),(3,621,5),(3,622,5),(3,623,5),(3,624,5),(3,626,5),(3,690,5),(3,692,5),(3,19,10),(3,21,10),(3,52,10),(3,309,10),(3,310,10),(3,312,10),(3,313,10),(3,314,10),(3,321,10),(3,346,10),(3,356,10),(3,367,10),(3,433,10),(3,438,10),(3,453,10),(3,454,10),(3,460,10),(3,461,10),(3,468,10),(3,476,10),(3,477,10),(3,481,10),(3,486,10),(3,497,10),(3,620,10),(3,689,10),(3,16,15),(3,24,15),(3,34,15),(3,50,15),(3,68,15),(3,303,15),(3,317,15),(3,463,15),(3,464,15),(3,478,15),(3,480,15),(3,488,15),(3,489,15),(3,493,15),(3,564,15),(3,565,15),(3,566,15),(3,568,15),(3,569,15),(3,570,15),(3,571,15),(3,572,15),(3,573,15),(3,584,15),(3,585,15),(3,586,15),(3,587,15),(3,588,15),(3,589,15),(3,590,15),(3,591,15),(3,592,15),(3,593,15),(3,594,15),(3,595,15),(3,596,15),(3,597,15),(3,598,15),(3,599,15),(3,600,15),(3,601,15),(3,602,15),(3,603,15),(3,604,15),(3,605,15),(3,606,15),(3,607,15),(3,608,15),(3,609,15),(3,610,15),(3,611,15),(3,612,15),(3,613,15),(3,614,15),(3,615,15),(3,616,15),(3,617,15),(3,618,15),(3,619,15),(3,625,15),(3,627,15),(3,628,15),(3,630,15),(3,631,15),(3,632,15),(3,633,15),(3,634,15),(3,635,15),(3,649,15),(3,650,15),(3,651,15),(3,652,15),(3,653,15),(3,654,15),(3,655,15),(3,656,15),(3,657,15),(3,658,15),(3,659,15),(3,660,15),(3,661,15),(3,662,15),(3,663,15),(3,664,15),(3,665,15),(3,666,15),(3,667,15),(3,668,15),(3,669,15),(3,670,15),(3,671,15),(3,672,15),(3,673,15),(3,674,15),(3,675,15),(3,676,15),(3,677,15),(3,678,15),(3,679,15),(3,680,15),(3,681,15),(3,682,15),(3,683,15),(3,684,15),(3,685,15),(3,686,15),(3,687,15),(3,688,15),(3,315,20),(3,316,20),(3,323,20),(3,327,20),(3,428,20),(3,429,20),(3,450,20),(3,451,20),(3,455,20),(3,574,20),(3,575,20),(3,636,20),(3,637,20),(3,7,25),(3,8,25),(3,30,25),(3,426,25),(3,427,25),(3,576,25),(3,577,25),(3,578,25),(3,580,25),(3,581,25),(3,582,25),(3,583,25),(3,638,25),(3,639,25),(3,641,25),(3,642,25),(3,644,25),(3,645,25),(3,646,25),(3,647,25),(3,648,25),(3,452,30),(3,567,30),(3,629,30),(3,340,35),(3,579,40),(3,643,40),(3,29,55),(3,640,65),(4,25,5),(4,693,5),(4,694,5),(4,695,5),(5,307,5),(5,337,5),(5,419,5),(5,697,5),(5,698,5),(5,699,5),(5,700,5),(5,703,5),(5,705,5),(5,706,5),(5,707,5),(5,708,5),(5,709,5),(5,710,5),(5,712,5),(5,714,5),(5,715,5),(5,716,5),(5,717,5),(5,718,5),(5,721,5),(5,723,5),(5,724,5),(5,725,5),(5,726,5),(5,727,5),(5,728,5),(5,80,10),(5,157,10),(5,235,10),(5,704,10),(5,713,10),(5,722,10),(6,734,5),(6,735,5),(6,439,10),(6,444,10),(6,445,10),(6,730,10);
/*!40000 ALTER TABLE `ps_search_index_cms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_search_word`
--

DROP TABLE IF EXISTS `ps_search_word`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_search_word` (
  `id_word` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_lang` int(10) unsigned NOT NULL,
  `word` varchar(15) NOT NULL,
  PRIMARY KEY (`id_word`),
  UNIQUE KEY `id_lang` (`id_lang`,`id_shop`,`word`)
) ENGINE=InnoDB AUTO_INCREMENT=49595 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_search_word`
--

LOCK TABLES `ps_search_word` WRITE;
/*!40000 ALTER TABLE `ps_search_word` DISABLE KEYS */;
INSERT INTO `ps_search_word` VALUES (43052,1,1,'1'),(44876,1,1,'10'),(44493,1,1,'105'),(44486,1,1,'11'),(46088,1,1,'12'),(46825,1,1,'123'),(42963,1,1,'130'),(42998,1,1,'143'),(44490,1,1,'148'),(42992,1,1,'15'),(46581,1,1,'153'),(45703,1,1,'15mm'),(42971,1,1,'1875'),(43051,1,1,'2'),(42952,1,1,'20'),(45714,1,1,'200'),(43044,1,1,'20042014'),(42949,1,1,'2010'),(43039,1,1,'2012'),(43032,1,1,'20134th'),(43040,1,1,'2014'),(43018,1,1,'2015'),(43045,1,1,'2016'),(43028,1,1,'2017'),(42996,1,1,'21'),(44489,1,1,'240g'),(44874,1,1,'25'),(43102,1,1,'3'),(44885,1,1,'30'),(46586,1,1,'3d'),(43101,1,1,'4'),(45360,1,1,'45'),(45355,1,1,'45mm'),(46598,1,1,'4band'),(44505,1,1,'5'),(42961,1,1,'5000'),(45413,1,1,'6'),(44880,1,1,'66'),(45721,1,1,'7'),(45408,1,1,'8'),(46084,1,1,'88'),(45414,1,1,'9'),(45361,1,1,'90'),(46601,1,1,'925'),(43001,1,1,'9789574173310'),(46855,1,1,'?'),(46163,1,1,'a'),(42967,1,1,'art'),(43034,1,1,'artist'),(43009,1,1,'arts'),(45705,1,1,'b'),(42990,1,1,'back'),(43010,1,1,'bank'),(44696,1,1,'beach'),(42984,1,1,'binding'),(42978,1,1,'black'),(43036,1,1,'book'),(43020,1,1,'books'),(43026,1,1,'ca'),(44492,1,1,'card'),(44488,1,1,'cean'),(43004,1,1,'central'),(44500,1,1,'chieh'),(44695,1,1,'chihsingtan'),(44482,1,1,'ching'),(44862,1,1,'chishingtan'),(42958,1,1,'chyaulun'),(44865,1,1,'clear'),(44484,1,1,'cliff'),(42993,1,1,'cm'),(46584,1,1,'coast'),(43021,1,1,'codex'),(43002,1,1,'collection'),(43015,1,1,'collectors'),(42954,1,1,'color'),(45358,1,1,'com'),(42983,1,1,'coptic'),(42977,1,1,'cover'),(43023,1,1,'craneway'),(46592,1,1,'daily'),(45718,1,1,'december'),(43042,1,1,'del'),(45719,1,1,'dergi'),(46585,1,1,'design'),(44856,1,1,'designed'),(46589,1,1,'differentintegr'),(42955,1,1,'digital'),(44858,1,1,'drawing'),(45380,1,1,'e'),(46583,1,1,'eastern'),(42951,1,1,'edition'),(42973,1,1,'egyptian'),(42962,1,1,'electrolnk'),(45712,1,1,'etsy'),(43031,1,1,'exhibition'),(42985,1,1,'exposed'),(46087,1,1,'fabric'),(44867,1,1,'face'),(43017,1,1,'fair'),(42966,1,1,'fine'),(42950,1,1,'first'),(42989,1,1,'flat'),(44496,1,1,'foil'),(46590,1,1,'formed'),(46091,1,1,'frankie'),(42953,1,1,'full'),(44508,1,1,'g'),(42974,1,1,'gold'),(43030,1,1,'group'),(42964,1,1,'gsm'),(42997,1,1,'h'),(46587,1,1,'handmade'),(42988,1,1,'hole'),(44495,1,1,'hot'),(42959,1,1,'hp'),(45711,1,1,'http'),(44697,1,1,'hualien'),(42940,1,1,'i'),(42960,1,1,'indigo'),(44861,1,1,'ink'),(42969,1,1,'inner'),(46582,1,1,'inspired'),(43033,1,1,'international'),(43000,1,1,'isbn'),(44491,1,1,'ivory'),(45108,1,1,'japanese'),(44886,1,1,'july'),(44881,1,1,'june'),(42972,1,1,'kg'),(44855,1,1,'kind'),(44509,1,1,'l'),(45706,1,1,'landscape'),(42987,1,1,'larger'),(43005,1,1,'library'),(46083,1,1,'light'),(45713,1,1,'listing'),(44699,1,1,'liu'),(43019,1,1,'liuyingchieh'),(44877,1,1,'long'),(44878,1,1,'loop'),(44506,1,1,'m'),(44884,1,1,'made'),(45707,1,1,'masking'),(42976,1,1,'metallic'),(44494,1,1,'mm'),(46600,1,1,'mmsterling'),(46085,1,1,'moisture'),(46164,1,1,'n'),(43003,1,1,'national'),(44852,1,1,'new'),(45715,1,1,'numbered'),(44487,1,1,'o'),(44869,1,1,'ocean'),(42956,1,1,'offset'),(44854,1,1,'one'),(44857,1,1,'original'),(45379,1,1,'p'),(44868,1,1,'pacific'),(42970,1,1,'page'),(42999,1,1,'pages'),(43043,1,1,'paine'),(45709,1,1,'painting'),(42968,1,1,'paper'),(44872,1,1,'paper1'),(45109,1,1,'paperset'),(43024,1,1,'pavilion'),(44864,1,1,'pebble'),(44860,1,1,'pen'),(45357,1,1,'pinkoi'),(44882,1,1,'printed'),(42957,1,1,'printing'),(43014,1,1,'private'),(43037,1,1,'prize'),(45359,1,1,'product'),(42948,1,1,'published'),(46594,1,1,'purpose'),(46857,1,1,'r'),(43016,1,1,'related'),(42947,1,1,'reproduction'),(43025,1,1,'richmond'),(46595,1,1,'ring'),(44863,1,1,'rocky'),(44873,1,1,'roll'),(45110,1,1,'rolls'),(45716,1,1,'round'),(43035,1,1,'s'),(44853,1,1,'sealed'),(44879,1,1,'second'),(42981,1,1,'section'),(45710,1,1,'series'),(42982,1,1,'sewn'),(43012,1,1,'sheffield'),(44483,1,1,'shui'),(44311,1,1,'significant'),(44498,1,1,'silver'),(46596,1,1,'size'),(44481,1,1,'sketch'),(44694,1,1,'sketches'),(46588,1,1,'slightly'),(45720,1,1,'sold'),(43038,1,1,'solo'),(42986,1,1,'spine'),(44859,1,1,'spread'),(44497,1,1,'stamping'),(45717,1,1,'stickermade'),(43011,1,1,'street'),(46090,1,1,'stretch'),(46591,1,1,'sturdy'),(46856,1,1,'t'),(37745,1,1,'t123'),(45704,1,1,'t123456789'),(44887,1,1,'tain'),(43006,1,1,'taipei'),(43007,1,1,'taiwan'),(44698,1,1,'taiwan240g'),(44870,1,1,'taiwanese'),(44485,1,1,'taiwanfrom'),(45708,1,1,'tape'),(43041,1,1,'torres'),(44312,1,1,'travel'),(44510,1,1,'u'),(43013,1,1,'uk'),(42979,1,1,'uncoated'),(43008,1,1,'university'),(46597,1,1,'us'),(43027,1,1,'usa'),(46593,1,1,'use'),(43022,1,1,'v'),(42941,1,1,'venezia'),(43029,1,1,'vi'),(42994,1,1,'w'),(44871,1,1,'washi'),(44866,1,1,'waves'),(46089,1,1,'way'),(42965,1,1,'white'),(46086,1,1,'wicking'),(44875,1,1,'wide'),(46599,1,1,'width'),(42980,1,1,'woodfree'),(45356,1,1,'www'),(42995,1,1,'x'),(46179,1,1,'xl'),(46178,1,1,'xs'),(26262,1,1,'xxx'),(34001,1,1,'xxxxvvvaaaabbbb'),(45378,1,1,'y'),(44499,1,1,'ying'),(42991,1,1,'yoshosu'),(44883,1,1,'yuanpao'),(42975,1,1,'zipang'),(44317,1,1,'あ'),(44319,1,1,'じ'),(44320,1,1,'ま'),(44318,1,1,'ら'),(44316,1,1,'イ'),(43081,1,1,'グ'),(43078,1,1,'ジ'),(44313,1,1,'ト'),(43079,1,1,'パ'),(44315,1,1,'メ'),(43080,1,1,'ン'),(44314,1,1,'ー'),(26389,1,1,'ㄌ'),(26390,1,1,'ㄏ'),(26391,1,1,'ㄑ'),(44395,1,1,'一'),(44691,1,1,'七'),(45397,1,1,'三'),(46132,1,1,'不'),(43107,1,1,'並'),(43161,1,1,'中'),(43116,1,1,'之'),(45392,1,1,'九'),(46137,1,1,'也'),(44430,1,1,'二'),(45363,1,1,'交'),(43095,1,1,'亭'),(46169,1,1,'以'),(45393,1,1,'份'),(46171,1,1,'伴'),(43061,1,1,'位'),(44516,1,1,'作'),(46131,1,1,'你'),(43114,1,1,'使'),(45371,1,1,'來'),(46160,1,1,'供'),(46129,1,1,'保'),(44518,1,1,'信'),(43148,1,1,'個'),(43067,1,1,'倫'),(45724,1,1,'值'),(46172,1,1,'做'),(43066,1,1,'僑'),(44322,1,1,'光'),(43073,1,1,'內'),(43058,1,1,'全'),(43098,1,1,'公'),(43123,1,1,'典'),(43049,1,1,'冊'),(46134,1,1,'冒'),(45395,1,1,'冷'),(43054,1,1,'出'),(43099,1,1,'分'),(45377,1,1,'列'),(43056,1,1,'初'),(43064,1,1,'刷'),(45406,1,1,'前'),(44324,1,1,'剛'),(45374,1,1,'創'),(46111,1,1,'力'),(43121,1,1,'加'),(42943,1,1,'動'),(43131,1,1,'北'),(46144,1,1,'匯'),(46148,1,1,'區'),(44394,1,1,'十'),(45402,1,1,'博'),(44503,1,1,'卡'),(43063,1,1,'印'),(45381,1,1,'卷'),(43068,1,1,'厚'),(45373,1,1,'原'),(43076,1,1,'及'),(43113,1,1,'口'),(44325,1,1,'古'),(46168,1,1,'可'),(42945,1,1,'台'),(46110,1,1,'向'),(46092,1,1,'吸'),(46139,1,1,'吹'),(43159,1,1,'和'),(45375,1,1,'品'),(43140,1,1,'四'),(43125,1,1,'國'),(43157,1,1,'園'),(43127,1,1,'圖'),(46150,1,1,'在'),(44512,1,1,'地'),(46126,1,1,'均'),(43152,1,1,'坊'),(45396,1,1,'坑'),(43075,1,1,'埃'),(46117,1,1,'墨'),(43120,1,1,'外'),(43110,1,1,'大'),(46165,1,1,'好'),(43133,1,1,'學'),(46128,1,1,'宜'),(46118,1,1,'室'),(45401,1,1,'宮'),(43126,1,1,'家'),(45370,1,1,'容'),(45367,1,1,'寫'),(43097,1,1,'寬'),(45415,1,1,'寶'),(43082,1,1,'封'),(45383,1,1,'尾'),(43141,1,1,'屆'),(43134,1,1,'展'),(44431,1,1,'山'),(44479,1,1,'崖'),(43151,1,1,'工'),(46141,1,1,'布'),(44892,1,1,'帶'),(46123,1,1,'常'),(43062,1,1,'平'),(43053,1,1,'年'),(46167,1,1,'幸'),(46174,1,1,'度'),(43160,1,1,'廊'),(44514,1,1,'式'),(46096,1,1,'彈'),(43059,1,1,'彩'),(45389,1,1,'循'),(46145,1,1,'德'),(45362,1,1,'心'),(46138,1,1,'怕'),(46097,1,1,'性'),(46133,1,1,'感'),(46147,1,1,'憩'),(26377,1,1,'我'),(46602,1,1,'戒'),(46119,1,1,'戶'),(44515,1,1,'手'),(45416,1,1,'承'),(46177,1,1,'拿'),(46603,1,1,'指'),(45409,1,1,'捲'),(46094,1,1,'排'),(45386,1,1,'接'),(46159,1,1,'提'),(45400,1,1,'故'),(46162,1,1,'教'),(43060,1,1,'數'),(46142,1,1,'料'),(43153,1,1,'新'),(44478,1,1,'斷'),(43122,1,1,'方'),(43050,1,1,'於'),(43149,1,1,'旅'),(46122,1,1,'日'),(46115,1,1,'昇'),(44517,1,1,'明'),(44692,1,1,'星'),(46149,1,1,'是'),(43090,1,1,'普'),(45366,1,1,'景'),(43105,1,1,'書'),(43135,1,1,'會'),(43108,1,1,'有'),(43057,1,1,'本'),(46101,1,1,'材'),(43088,1,1,'林'),(45723,1,1,'格'),(46143,1,1,'案'),(45405,1,1,'桌'),(46166,1,1,'榮'),(46098,1,1,'機'),(46153,1,1,'步'),(45410,1,1,'每'),(46113,1,1,'毒'),(44477,1,1,'水'),(46095,1,1,'汗'),(44323,1,1,'波'),(44397,1,1,'洋'),(43112,1,1,'洞'),(44396,1,1,'海'),(45399,1,1,'淺'),(45107,1,1,'淼'),(43150,1,1,'清'),(46829,1,1,'湖'),(46135,1,1,'滿'),(44693,1,1,'潭'),(46093,1,1,'濕'),(44888,1,1,'灘'),(42946,1,1,'灣'),(44321,1,1,'炫'),(45384,1,1,'無'),(46158,1,1,'照'),(46114,1,1,'熱'),(44480,1,1,'燙'),(43144,1,1,'爾'),(44519,1,1,'片'),(43055,1,1,'版'),(44502,1,1,'牙'),(45403,1,1,'物'),(43091,1,1,'特'),(46140,1,1,'獨'),(46161,1,1,'玩'),(46155,1,1,'珈'),(26388,1,1,'球'),(46823,1,1,'琺'),(46154,1,1,'瑜'),(46824,1,1,'瑯'),(43096,1,1,'璇'),(45390,1,1,'環'),(45368,1,1,'生'),(44511,1,1,'產'),(45407,1,1,'用'),(43048,1,1,'畫'),(43115,1,1,'疊'),(43111,1,1,'的'),(43094,1,1,'盧'),(43069,1,1,'磅'),(46156,1,1,'示'),(43089,1,1,'科'),(42942,1,1,'移'),(43158,1,1,'空'),(46124,1,1,'穿'),(43119,1,1,'窄'),(43129,1,1,'立'),(43154,1,1,'竹'),(43139,1,1,'第'),(46157,1,1,'範'),(44504,1,1,'精'),(45376,1,1,'系'),(46828,1,1,'紅'),(46109,1,1,'紗'),(43072,1,1,'紙'),(46830,1,1,'綠'),(46106,1,1,'維'),(45411,1,1,'編'),(45385,1,1,'縫'),(45369,1,1,'繪'),(46105,1,1,'纖'),(43070,1,1,'美'),(43065,1,1,'者'),(46103,1,1,'聚'),(43138,1,1,'聯'),(43106,1,1,'背'),(46099,1,1,'能'),(44432,1,1,'脈'),(44891,1,1,'膠'),(45372,1,1,'自'),(43130,1,1,'臺'),(43086,1,1,'色'),(45398,1,1,'芝'),(44889,1,1,'花'),(43147,1,1,'英'),(46116,1,1,'華'),(43143,1,1,'菲'),(46125,1,1,'著'),(44890,1,1,'蓮'),(46826,1,1,'藍'),(43124,1,1,'藏'),(43132,1,1,'藝'),(46151,1,1,'蘇'),(45412,1,1,'號'),(46827,1,1,'血'),(46121,1,1,'行'),(43071,1,1,'術'),(43092,1,1,'裝'),(43103,1,1,'裸'),(43047,1,1,'製'),(43046,1,1,'複'),(46100,1,1,'褲'),(43084,1,1,'襯'),(45722,1,1,'規'),(43093,1,1,'訂'),(46176,1,1,'計'),(42944,1,1,'記'),(46175,1,1,'設'),(43145,1,1,'評'),(46130,1,1,'護'),(44501,1,1,'象'),(46102,1,1,'質'),(46152,1,1,'路'),(46136,1,1,'身'),(45387,1,1,'軌'),(43109,1,1,'較'),(46107,1,1,'輕'),(44513,1,1,'造'),(46146,1,1,'遊'),(46120,1,1,'運'),(43087,1,1,'道'),(46127,1,1,'適'),(43146,1,1,'選'),(46104,1,1,'酯'),(43156,1,1,'醒'),(46108,1,1,'量'),(43077,1,1,'金'),(44507,1,1,'銀'),(45364,1,1,'錯'),(43100,1,1,'長'),(43117,1,1,'間'),(45388,1,1,'限'),(45404,1,1,'院'),(46170,1,1,'陪'),(45394,1,1,'陽'),(43118,1,1,'隔'),(43136,1,1,'際'),(43137,1,1,'雙'),(46173,1,1,'難'),(43142,1,1,'雪'),(43104,1,1,'露'),(43083,1,1,'面'),(43074,1,1,'頁'),(45365,1,1,'風'),(43128,1,1,'館'),(45382,1,1,'首'),(43155,1,1,'香'),(46112,1,1,'高'),(43085,1,1,'黑'),(45391,1,1,'點'),(44555,1,2,'105'),(45486,1,2,'108'),(37477,1,2,'123'),(43430,1,2,'130'),(43446,1,2,'143'),(1002,1,2,'143頁內頁裸露書背'),(43445,1,2,'148'),(46646,1,2,'153'),(45813,1,2,'15mm'),(43436,1,2,'1875'),(1300,1,2,'1y432pel山水手卷卷首卷'),(45815,1,2,'200'),(43174,1,2,'2002'),(43367,1,2,'2003'),(43493,1,2,'20042014'),(43168,1,2,'2010'),(43486,1,2,'2012'),(43480,1,2,'20134th'),(43489,1,2,'2014'),(43468,1,2,'2015'),(43495,1,2,'2016'),(1309,1,2,'20169原寶承印'),(43476,1,2,'2017'),(1215,1,2,'240g'),(43390,1,2,'411'),(45473,1,2,'45mm'),(46658,1,2,'4band'),(43416,1,2,'5000'),(45483,1,2,'57177974山水手卷卷首卷'),(46661,1,2,'925'),(43452,1,2,'9789574173310'),(43252,1,2,'accumulated'),(43218,1,2,'alishan'),(43270,1,2,'also'),(46258,1,2,'anti'),(43209,1,2,'around'),(43434,1,2,'art'),(43424,1,2,'artist'),(43507,1,2,'artists'),(43460,1,2,'arts'),(43317,1,2,'artwork'),(43421,1,2,'back'),(43462,1,2,'bank'),(43288,1,2,'base'),(43184,1,2,'bathroom'),(44551,1,2,'beach'),(43287,1,2,'become'),(43259,1,2,'becoming'),(43370,1,2,'bed'),(43192,1,2,'bedroom'),(43196,1,2,'beginning'),(43312,1,2,'binding'),(43442,1,2,'black'),(43199,1,2,'blonde'),(43191,1,2,'body'),(43163,1,2,'book'),(43228,1,2,'books'),(43423,1,2,'bound'),(43504,1,2,'buyer'),(46270,1,2,'canadadesigned'),(44554,1,2,'card'),(43229,1,2,'carrier'),(43255,1,2,'carry'),(44546,1,2,'cean'),(43455,1,2,'central'),(43231,1,2,'characteristic'),(43502,1,2,'chieh'),(44550,1,2,'chihsingtan'),(44547,1,2,'ching'),(43216,1,2,'chingshui'),(44929,1,2,'chishingtan'),(959,1,2,'chyaulun'),(44933,1,2,'clear'),(43217,1,2,'cliff'),(46274,1,2,'cliffs'),(46649,1,2,'coast'),(46273,1,2,'coastal'),(43470,1,2,'codex'),(43453,1,2,'collection'),(1021,1,2,'collectors'),(43466,1,2,'collectorsrelat'),(43411,1,2,'color'),(45482,1,2,'com'),(43206,1,2,'comics'),(43308,1,2,'complex'),(43273,1,2,'contact'),(43238,1,2,'continuous'),(43417,1,2,'coptic'),(43422,1,2,'cover'),(43472,1,2,'craneway'),(43257,1,2,'creates'),(43244,1,2,'creation'),(43230,1,2,'creations'),(43262,1,2,'creative'),(43272,1,2,'creator'),(43187,1,2,'curtain'),(46248,1,2,'daily'),(46242,1,2,'dancing'),(45818,1,2,'december'),(43491,1,2,'del'),(43299,1,2,'depth'),(45819,1,2,'dergi'),(46650,1,2,'design'),(44925,1,2,'designed'),(43204,1,2,'diaries'),(46653,1,2,'differentintegr'),(43412,1,2,'digital'),(43303,1,2,'dimensional'),(43280,1,2,'disappear'),(43304,1,2,'distance'),(43186,1,2,'door'),(43189,1,2,'dorm'),(43194,1,2,'draw'),(44926,1,2,'drawing'),(43171,1,2,'drawn'),(43311,1,2,'dreams'),(43225,1,2,'duration'),(43239,1,2,'dynamic'),(43407,1,2,'east'),(46648,1,2,'eastern'),(46260,1,2,'eco'),(43448,1,2,'edition'),(43437,1,2,'egyptian'),(962,1,2,'electrolnk'),(43315,1,2,'elements'),(43381,1,2,'eluanbi'),(43261,1,2,'entire'),(43505,1,2,'entitled'),(45481,1,2,'etsy'),(43479,1,2,'exhibition'),(43277,1,2,'experience'),(984,1,2,'exposed'),(43310,1,2,'express'),(43295,1,2,'extension'),(43266,1,2,'external'),(46253,1,2,'fabric'),(44935,1,2,'face'),(43467,1,2,'fair'),(43211,1,2,'fangliao'),(43173,1,2,'feb'),(43188,1,2,'female'),(43403,1,2,'fenqihu'),(43433,1,2,'fine'),(43177,1,2,'first'),(43420,1,2,'flat'),(43233,1,2,'flipping'),(43258,1,2,'flux'),(44558,1,2,'foil'),(46654,1,2,'formed'),(43313,1,2,'forms'),(43256,1,2,'fragments'),(43305,1,2,'frames'),(46278,1,2,'frankie'),(46261,1,2,'friendly'),(43242,1,2,'fulfill'),(43410,1,2,'full'),(43326,1,2,'future'),(43198,1,2,'giudecca'),(43438,1,2,'gold'),(43374,1,2,'gondola移動記'),(43478,1,2,'group'),(43292,1,2,'growth'),(43431,1,2,'gsm'),(43200,1,2,'hair'),(43170,1,2,'hand'),(46651,1,2,'handmade'),(43396,1,2,'heren'),(46264,1,2,'high'),(46246,1,2,'hiking'),(43419,1,2,'hole'),(43181,1,2,'hostel'),(44557,1,2,'hot'),(43268,1,2,'however'),(45479,1,2,'http'),(43215,1,2,'hualien'),(43243,1,2,'image'),(43240,1,2,'imagination'),(43250,1,2,'imitate'),(43278,1,2,'incidents'),(43415,1,2,'indigo'),(44928,1,2,'ink'),(45475,1,2,'inner'),(46647,1,2,'inspired'),(43300,1,2,'instead'),(43297,1,2,'intend'),(43265,1,2,'intended'),(43223,1,2,'interested'),(43267,1,2,'internal'),(43481,1,2,'international'),(43451,1,2,'isbn'),(44553,1,2,'ivory'),(43172,1,2,'jan'),(45175,1,2,'japanese'),(43263,1,2,'journey'),(44947,1,2,'july'),(44943,1,2,'june'),(44924,1,2,'kind'),(43378,1,2,'kissing'),(45474,1,2,'landscape'),(986,1,2,'larger'),(43285,1,2,'last'),(43321,1,2,'left'),(46280,1,2,'leggings'),(43271,1,2,'lets'),(43456,1,2,'library'),(43249,1,2,'life'),(46247,1,2,'light'),(43447,1,2,'limited'),(43385,1,2,'line'),(43322,1,2,'link'),(43450,1,2,'listing'),(43413,1,2,'lithography'),(43503,1,2,'liu'),(43469,1,2,'liuyingchieh'),(1229,1,2,'liu產地'),(43282,1,2,'long'),(44941,1,2,'loop'),(44946,1,2,'made'),(46269,1,2,'manufactured'),(45476,1,2,'masking'),(43197,1,2,'memories'),(43226,1,2,'memory'),(43441,1,2,'metallic'),(46259,1,2,'microbial'),(46660,1,2,'mmsterling'),(1218,1,2,'mm燙金'),(43245,1,2,'mode'),(46254,1,2,'moisture'),(43319,1,2,'moment'),(43307,1,2,'montage'),(43203,1,2,'monthes'),(43397,1,2,'moonrise'),(43241,1,2,'motivation'),(46272,1,2,'mountain'),(43235,1,2,'movement'),(43207,1,2,'movies'),(43246,1,2,'moving'),(43190,1,2,'naked'),(43454,1,2,'national'),(44921,1,2,'new'),(43289,1,2,'next'),(46262,1,2,'non'),(43320,1,2,'now'),(43449,1,2,'numbered'),(43400,1,2,'ocean'),(44556,1,2,'offset'),(44923,1,2,'one'),(43429,1,2,'open'),(43251,1,2,'opened'),(46267,1,2,'order'),(43389,1,2,'ordinary'),(43498,1,2,'original'),(46245,1,2,'outdoor'),(43281,1,2,'overnight'),(43399,1,2,'pacific'),(43232,1,2,'page'),(43254,1,2,'pages'),(43492,1,2,'paine'),(45477,1,2,'painting'),(43435,1,2,'paper'),(44938,1,2,'paper1'),(45176,1,2,'paperset'),(43323,1,2,'past'),(46271,1,2,'pattern'),(43473,1,2,'pavilion'),(44932,1,2,'pebble'),(44927,1,2,'pen'),(46251,1,2,'performance'),(43283,1,2,'period'),(46276,1,2,'photo'),(43212,1,2,'pinghsi'),(1297,1,2,'pinkoi'),(46249,1,2,'polyester'),(44561,1,2,'postcard'),(44544,1,2,'postcards'),(43284,1,2,'precipitation'),(43224,1,2,'preserving'),(44944,1,2,'printed'),(43414,1,2,'printing'),(43465,1,2,'private'),(43482,1,2,'prize'),(1299,1,2,'product'),(43291,1,2,'promote'),(43500,1,2,'property'),(43166,1,2,'published'),(46656,1,2,'purpose'),(46265,1,2,'quality'),(43405,1,2,'railway'),(43247,1,2,'real'),(43290,1,2,'reality'),(43264,1,2,'really'),(1022,1,2,'related'),(43499,1,2,'remains'),(43294,1,2,'repetition'),(43169,1,2,'reproduction'),(43237,1,2,'resulting'),(43474,1,2,'richmond'),(43408,1,2,'rift'),(43324,1,2,'right'),(43506,1,2,'rights'),(46657,1,2,'ring'),(43205,1,2,'road'),(44931,1,2,'rocky'),(44939,1,2,'roll'),(45177,1,2,'rolls'),(43372,1,2,'roomate'),(43201,1,2,'roommate'),(45816,1,2,'round'),(43392,1,2,'running'),(43213,1,2,'sandiaoling'),(43183,1,2,'saw'),(44922,1,2,'sealed'),(44942,1,2,'second'),(43427,1,2,'section'),(43167,1,2,'september'),(43306,1,2,'sequences'),(45478,1,2,'series'),(44543,1,2,'set'),(43202,1,2,'several'),(43428,1,2,'sewn'),(43464,1,2,'sheffield'),(43182,1,2,'shocked'),(43260,1,2,'show'),(43234,1,2,'showing'),(44548,1,2,'shui'),(43426,1,2,'signatures'),(44338,1,2,'significant'),(44560,1,2,'silver'),(43195,1,2,'since'),(46268,1,2,'size'),(43376,1,2,'sketch'),(44545,1,2,'sketches'),(46652,1,2,'slightly'),(45820,1,2,'sold'),(43485,1,2,'solo'),(43236,1,2,'space'),(46250,1,2,'spandex'),(43418,1,2,'special'),(985,1,2,'spine'),(46244,1,2,'spinning'),(43298,1,2,'spiritual'),(46241,1,2,'sport'),(43318,1,2,'spread'),(43293,1,2,'stacking'),(43309,1,2,'staggered'),(44559,1,2,'stamping'),(43193,1,2,'started'),(43269,1,2,'state'),(43222,1,2,'statement'),(43387,1,2,'station'),(43179,1,2,'stayed'),(45817,1,2,'stickermade'),(43276,1,2,'stimulation'),(43379,1,2,'stone'),(43391,1,2,'stop'),(43463,1,2,'street'),(46257,1,2,'stretch'),(43253,1,2,'strokes'),(43275,1,2,'strong'),(43296,1,2,'structure'),(46655,1,2,'sturdy'),(46266,1,2,'sublimation'),(46240,1,2,'summer'),(43401,1,2,'sunrise'),(37855,1,2,'t123'),(45814,1,2,'t123456789'),(44948,1,2,'tain'),(43458,1,2,'taipei'),(43165,1,2,'taiwan'),(44552,1,2,'taiwan240g'),(44936,1,2,'taiwanese'),(1213,1,2,'taiwanfrom'),(44949,1,2,'tape'),(43302,1,2,'three'),(43316,1,2,'throughout'),(43178,1,2,'time'),(46275,1,2,'top'),(43490,1,2,'torres'),(46263,1,2,'toxic'),(43210,1,2,'train'),(43162,1,2,'travel'),(43208,1,2,'traveled'),(43371,1,2,'traveler'),(43286,1,2,'trip'),(43248,1,2,'trying'),(43443,1,2,'uncoated'),(43325,1,2,'undertake'),(43274,1,2,'unexpected'),(43459,1,2,'university'),(43475,1,2,'usa'),(43477,1,2,'usarelated'),(43227,1,2,'use'),(43409,1,2,'valley'),(43369,1,2,'venezia'),(43164,1,2,'venice'),(43301,1,2,'virtual'),(43314,1,2,'visual'),(46279,1,2,'wang'),(1377,1,2,'wang好榮幸'),(44937,1,2,'washi'),(43373,1,2,'wave'),(44934,1,2,'waves'),(46256,1,2,'way'),(46252,1,2,'wear'),(43176,1,2,'weeks'),(43175,1,2,'went'),(43432,1,2,'white'),(46255,1,2,'wicking'),(44940,1,2,'wide'),(46659,1,2,'width'),(43279,1,2,'will'),(43185,1,2,'without'),(43444,1,2,'woodfree'),(45480,1,2,'www'),(43214,1,2,'yilan'),(43501,1,2,'ying'),(46243,1,2,'yoga'),(43425,1,2,'yoshosu'),(43180,1,2,'youth'),(44945,1,2,'yuanpao'),(43439,1,2,'zipang'),(43440,1,2,'ジパング'),(44339,1,2,'トーメイあらじま'),(26403,1,2,'ㄌㄏㄌㄑㄏㄌ'),(43393,1,2,'一步又一步'),(44549,1,2,'七星潭'),(44930,1,2,'七星潭海灘'),(1303,1,2,'三芝淺水灣'),(43219,1,2,'三貂嶺'),(43386,1,2,'三貂嶺車站'),(43347,1,2,'上一段旅行的回憶'),(1003,1,2,'並有較大的洞口使疊頁之間間隔較'),(45485,1,2,'九份陽明山冷水坑花蓮三芝淺水灣'),(43350,1,2,'交互推進'),(43357,1,2,'交錯的時空'),(43356,1,2,'以蒙太奇手法並置複雜'),(1307,1,2,'使用台灣和紙精印初版108捲'),(1369,1,2,'保護你不感冒'),(1293,1,2,'內心山水交錯台灣風景寫生和紙膠'),(999,1,2,'內襯黑色道林紙'),(44448,1,2,'其頁面接續頁面的結構易於表述時'),(994,1,2,'初版20本'),(44452,1,2,'則提供了較為私密'),(43341,1,2,'創作之旅'),(43360,1,2,'創作載體之裝訂形式'),(43366,1,2,'劉穎捷'),(1382,1,2,'加拿大製作'),(996,1,2,'印刷者僑倫'),(997,1,2,'厚磅數美術紙內頁'),(1295,1,2,'原創作品移動記系列之四'),(1379,1,2,'可以陪伴瑜珈高手做出高難度動作'),(43365,1,2,'右頁承接未來'),(43344,1,2,'同時也讓創作者不斷接觸意料之外'),(1355,1,2,'吸濕排汗彈性機能褲材質'),(1380,1,2,'吸濕排汗彈性機能褲產地'),(44449,1,2,'呈現宛如記憶綿延的連續動態想像'),(43355,1,2,'呈現對於純粹繪畫過於複雜的時空'),(43330,1,2,'呈現時空的轉移'),(1362,1,2,'四向彈力'),(43359,1,2,'回憶的特質'),(43457,1,2,'國家圖書館'),(43497,1,2,'國立台灣圖書館雙和藝廊'),(43461,1,2,'國立臺北藝術大學圖書館'),(43471,1,2,'國際雙年展'),(43335,1,2,'在旅途中'),(44444,1,2,'在路上邊走邊畫'),(26402,1,2,'地球山水冊頁'),(1368,1,2,'均適宜'),(998,1,2,'埃及金ジパング紙封面'),(1004,1,2,'外加方背封面'),(43398,1,2,'太平洋日出'),(43402,1,2,'奮起湖'),(45484,1,2,'寫生地點'),(1374,1,2,'寫生地點是在蘇花公路匯德步道瑜'),(43363,1,2,'對於左翻書而言'),(43364,1,2,'左頁聯繫著過去'),(43384,1,2,'平溪線'),(43395,1,2,'幸福水泥'),(1361,1,2,'彈性纖維'),(44451,1,2,'徒手翻頁的特質'),(43382,1,2,'從枋寮上車之後'),(43353,1,2,'意圖以心靈空間深度取代三度空間'),(43348,1,2,'成為下一段旅行的基底'),(43361,1,2,'成為貫穿整本書冊的視覺元素'),(44443,1,2,'手工縫製一本隨身畫冊'),(1231,1,2,'手工製作'),(43394,1,2,'接近清水斷崖之中'),(1305,1,2,'故宮博物院'),(995,1,2,'數位平版印刷'),(1304,1,2,'新竹內灣'),(43328,1,2,'於是選擇以書冊為創作載體'),(43375,1,2,'旅行寫生'),(43343,1,2,'旅行狀態本身'),(43332,1,2,'旅行的動機是為了成全以書冊為載'),(1367,1,2,'日常穿著'),(1232,1,2,'明信片'),(43340,1,2,'書冊呈現生命整體的綿延狀態'),(1372,1,2,'書冊繪畫作品移動記十一'),(43362,1,2,'書冊被攤開的雙頁即是當下'),(44450,1,2,'書冊需要近觀'),(44445,1,2,'書冊體積小'),(1306,1,2,'書桌前'),(43354,1,2,'書頁中的分格'),(43338,1,2,'書頁承載了片段時刻'),(43494,1,2,'書香醒園藝術空間'),(43383,1,2,'松山車站'),(1308,1,2,'每捲編號'),(44341,1,2,'波面剛古紙'),(43488,1,2,'清大藝術工坊'),(43220,1,2,'清水斷崖'),(1373,1,2,'清水斷崖匯德遊憩區寫生'),(1378,1,2,'清水斷崖瑜珈褲'),(1370,1,2,'滿身大汗也不怕吹風'),(44340,1,2,'炫光紙'),(43327,1,2,'熱衷於保存記憶的綿延狀態'),(1219,1,2,'燙銀240g'),(45821,1,2,'特性數值1'),(1371,1,2,'獨一無二的布料圖案來自'),(46277,1,2,'玩瑜珈教室'),(43349,1,2,'現實與想像互相模仿'),(1383,1,2,'瑜珈褲'),(43342,1,2,'確實意圖讓外部旅程模仿內部想像'),(1000,1,2,'科普特裝訂'),(43368,1,2,'移動記'),(43487,1,2,'移動記之旅'),(44406,1,2,'移動記十一'),(44442,1,2,'移動記十二'),(44337,1,2,'移動記四'),(43388,1,2,'第411號列車'),(43483,1,2,'第四屆雪菲爾國際藝術家的書評選'),(1266,1,2,'紙膠帶'),(43346,1,2,'經過長時間沈澱'),(1294,1,2,'繪畫內容來自'),(43336,1,2,'翻開書冊'),(43339,1,2,'翻頁造成變化之流'),(1356,1,2,'聚酯纖維'),(44447,1,2,'能收取到繪畫者面對大自然時最感'),(43406,1,2,'花東縱谷'),(43484,1,2,'英國related'),(43496,1,2,'藝術家的書'),(43358,1,2,'表現夢境'),(1001,1,2,'裝訂者盧亭璇'),(1230,1,2,'製造方式台灣'),(1381,1,2,'製造方式台灣設計'),(993,1,2,'複製畫冊於2010年出版'),(43377,1,2,'親吻岩'),(44453,1,2,'親近人身體的感官體驗'),(43334,1,2,'讓真實生活模仿記憶的綿延'),(1216,1,2,'象牙卡精印105'),(44446,1,2,'輕便易攜帶'),(1366,1,2,'輕裝旅行'),(1357,1,2,'輕量吸濕排汗原紗'),(43329,1,2,'透過書頁的翻頁特質'),(43351,1,2,'透過生長與堆疊'),(43333,1,2,'透過身體的真正移動'),(43337,1,2,'逐頁累積筆觸'),(43345,1,2,'這些意外的經歷並不會隔夜就消失'),(43331,1,2,'造成連續性的動態想像'),(43352,1,2,'重複與延伸的模式來結構畫面'),(43221,1,2,'阿里山'),(43404,1,2,'阿里山小火車'),(1302,1,2,'陽明山冷水坑'),(1365,1,2,'高品質無毒數位熱昇華墨水室內戶'),(43380,1,2,'鵝鑾鼻'),(43620,1,3,'1'),(44995,1,3,'10'),(44592,1,3,'105'),(45529,1,3,'108'),(44585,1,3,'11'),(46326,1,3,'12'),(37530,1,3,'123'),(43531,1,3,'130'),(43566,1,3,'143'),(44589,1,3,'148'),(43560,1,3,'15'),(46694,1,3,'153'),(45866,1,3,'15mm'),(43539,1,3,'1875'),(43619,1,3,'2'),(43520,1,3,'20'),(45869,1,3,'200'),(43612,1,3,'20042014'),(43517,1,3,'2010'),(43607,1,3,'2012'),(43600,1,3,'20134th'),(43608,1,3,'2014'),(43586,1,3,'2015'),(43613,1,3,'2016'),(43596,1,3,'2017'),(43564,1,3,'21'),(44588,1,3,'240g'),(44993,1,3,'25'),(43670,1,3,'3'),(45004,1,3,'30'),(46699,1,3,'3d'),(43669,1,3,'4'),(45527,1,3,'45'),(45516,1,3,'45mm'),(46711,1,3,'4band'),(44605,1,3,'5'),(43529,1,3,'5000'),(44999,1,3,'66'),(45531,1,3,'7'),(46322,1,3,'88'),(45532,1,3,'9'),(45528,1,3,'90'),(46714,1,3,'925'),(43569,1,3,'9789574173310'),(46409,1,3,'a'),(43535,1,3,'art'),(43602,1,3,'artist'),(43577,1,3,'arts'),(45868,1,3,'b'),(43558,1,3,'back'),(43578,1,3,'bank'),(44771,1,3,'beach'),(43552,1,3,'binding'),(43546,1,3,'black'),(43604,1,3,'book'),(43588,1,3,'books'),(43594,1,3,'ca'),(44591,1,3,'card'),(44587,1,3,'cean'),(43572,1,3,'central'),(44599,1,3,'chieh'),(44770,1,3,'chihsingtan'),(44581,1,3,'ching'),(44981,1,3,'chishingtan'),(43526,1,3,'chyaulun'),(44984,1,3,'clear'),(44583,1,3,'cliff'),(43561,1,3,'cm'),(46697,1,3,'coast'),(43589,1,3,'codex'),(43570,1,3,'collection'),(43583,1,3,'collectors'),(43522,1,3,'color'),(45525,1,3,'com'),(43551,1,3,'coptic'),(43545,1,3,'cover'),(43591,1,3,'craneway'),(46705,1,3,'daily'),(45873,1,3,'december'),(43610,1,3,'del'),(45874,1,3,'dergi'),(46698,1,3,'design'),(44975,1,3,'designed'),(46702,1,3,'differentintegr'),(43523,1,3,'digital'),(44977,1,3,'drawing'),(46696,1,3,'eastern'),(43519,1,3,'edition'),(43541,1,3,'egyptian'),(43530,1,3,'electrolnk'),(45524,1,3,'etsy'),(43599,1,3,'exhibition'),(43553,1,3,'exposed'),(46325,1,3,'fabric'),(44986,1,3,'face'),(43585,1,3,'fair'),(43534,1,3,'fine'),(43518,1,3,'first'),(43557,1,3,'flat'),(44595,1,3,'foil'),(46703,1,3,'formed'),(46329,1,3,'frankie'),(43521,1,3,'full'),(44609,1,3,'g'),(43542,1,3,'gold'),(43598,1,3,'group'),(43532,1,3,'gsm'),(43565,1,3,'h'),(46700,1,3,'handmade'),(43556,1,3,'hole'),(44594,1,3,'hot'),(43527,1,3,'hp'),(45522,1,3,'http'),(44772,1,3,'hualien'),(43508,1,3,'i'),(43528,1,3,'indigo'),(44980,1,3,'ink'),(43537,1,3,'inner'),(46695,1,3,'inspired'),(43601,1,3,'international'),(43568,1,3,'isbn'),(44590,1,3,'ivory'),(45223,1,3,'japanese'),(45005,1,3,'july'),(45000,1,3,'june'),(43540,1,3,'kg'),(44974,1,3,'kind'),(44610,1,3,'l'),(45517,1,3,'landscape'),(43555,1,3,'larger'),(43573,1,3,'library'),(46321,1,3,'light'),(45526,1,3,'listing'),(44774,1,3,'liu'),(43587,1,3,'liuyingchieh'),(44996,1,3,'long'),(44997,1,3,'loop'),(44606,1,3,'m'),(45003,1,3,'made'),(45518,1,3,'masking'),(43544,1,3,'metallic'),(44593,1,3,'mm'),(46713,1,3,'mmsterling'),(46323,1,3,'moisture'),(46410,1,3,'n'),(43571,1,3,'national'),(44971,1,3,'new'),(45870,1,3,'numbered'),(44586,1,3,'o'),(44988,1,3,'ocean'),(43524,1,3,'offset'),(44973,1,3,'one'),(44976,1,3,'original'),(44987,1,3,'pacific'),(43538,1,3,'page'),(43567,1,3,'pages'),(43611,1,3,'paine'),(45520,1,3,'painting'),(43536,1,3,'paper'),(44991,1,3,'paper1'),(45224,1,3,'paperset'),(43592,1,3,'pavilion'),(44983,1,3,'pebble'),(44979,1,3,'pen'),(45001,1,3,'printed'),(43525,1,3,'printing'),(43582,1,3,'private'),(43605,1,3,'prize'),(43516,1,3,'published'),(46707,1,3,'purpose'),(43584,1,3,'related'),(43515,1,3,'reproduction'),(43593,1,3,'richmond'),(46708,1,3,'ring'),(44982,1,3,'rocky'),(44992,1,3,'roll'),(45225,1,3,'rolls'),(45871,1,3,'round'),(43603,1,3,'s'),(44972,1,3,'sealed'),(44998,1,3,'second'),(43549,1,3,'section'),(45530,1,3,'september'),(45521,1,3,'series'),(43550,1,3,'sewn'),(43580,1,3,'sheffield'),(44582,1,3,'shui'),(44345,1,3,'significant'),(44597,1,3,'silver'),(46709,1,3,'size'),(44580,1,3,'sketch'),(44769,1,3,'sketches'),(46701,1,3,'slightly'),(45875,1,3,'sold'),(43606,1,3,'solo'),(43554,1,3,'spine'),(44978,1,3,'spread'),(44596,1,3,'stamping'),(45872,1,3,'stickermade'),(43579,1,3,'street'),(46328,1,3,'stretch'),(46704,1,3,'sturdy'),(37908,1,3,'t123'),(45867,1,3,'t123456789'),(45006,1,3,'tain'),(43574,1,3,'taipei'),(43575,1,3,'taiwan'),(44773,1,3,'taiwan240g'),(44989,1,3,'taiwanese'),(44584,1,3,'taiwanfrom'),(45519,1,3,'tape'),(43609,1,3,'torres'),(44346,1,3,'travel'),(44611,1,3,'u'),(43581,1,3,'uk'),(43547,1,3,'uncoated'),(43576,1,3,'university'),(46710,1,3,'us'),(43595,1,3,'usa'),(46706,1,3,'use'),(43590,1,3,'v'),(43509,1,3,'venezia'),(43597,1,3,'vi'),(43562,1,3,'w'),(44990,1,3,'washi'),(44985,1,3,'waves'),(46327,1,3,'way'),(43533,1,3,'white'),(46324,1,3,'wicking'),(44994,1,3,'wide'),(46712,1,3,'width'),(43548,1,3,'woodfree'),(45523,1,3,'www'),(43563,1,3,'x'),(46425,1,3,'xl'),(46424,1,3,'xs'),(44598,1,3,'ying'),(43559,1,3,'yoshosu'),(45002,1,3,'yuanpao'),(43543,1,3,'zipang'),(44353,1,3,'あ'),(44355,1,3,'じ'),(44356,1,3,'ま'),(44354,1,3,'ら'),(44352,1,3,'イ'),(43649,1,3,'グ'),(43646,1,3,'ジ'),(44349,1,3,'ト'),(43647,1,3,'パ'),(44351,1,3,'メ'),(43648,1,3,'ン'),(44350,1,3,'ー'),(44410,1,3,'一'),(44766,1,3,'七'),(45551,1,3,'三'),(46372,1,3,'不'),(43716,1,3,'个'),(43729,1,3,'中'),(43684,1,3,'之'),(45546,1,3,'九'),(46377,1,3,'也'),(43673,1,3,'书'),(44456,1,3,'二'),(43618,1,3,'于'),(44612,1,3,'产'),(43663,1,3,'亭'),(46415,1,3,'以'),(45547,1,3,'份'),(43703,1,3,'会'),(43635,1,3,'伦'),(46417,1,3,'伴'),(43629,1,3,'位'),(44617,1,3,'作'),(46371,1,3,'你'),(43682,1,3,'使'),(46406,1,3,'供'),(43634,1,3,'侨'),(46369,1,3,'保'),(44619,1,3,'信'),(45878,1,3,'值'),(46418,1,3,'做'),(44358,1,3,'光'),(45555,1,3,'內'),(43626,1,3,'全'),(43666,1,3,'公'),(43691,1,3,'典'),(43641,1,3,'内'),(43617,1,3,'册'),(46374,1,3,'冒'),(46393,1,3,'写'),(45549,1,3,'冷'),(43622,1,3,'出'),(43667,1,3,'分'),(43624,1,3,'初'),(43632,1,3,'刷'),(45563,1,3,'前'),(44361,1,3,'剛'),(46349,1,3,'力'),(43689,1,3,'加'),(43511,1,3,'动'),(44347,1,3,'動'),(43699,1,3,'北'),(46392,1,3,'区'),(44409,1,3,'十'),(46356,1,3,'华'),(45558,1,3,'博'),(44603,1,3,'卡'),(43662,1,3,'卢'),(43631,1,3,'印'),(45533,1,3,'卷'),(43636,1,3,'厚'),(46346,1,3,'原'),(43644,1,3,'及'),(43705,1,3,'双'),(43683,1,3,'叠'),(43681,1,3,'口'),(44362,1,3,'古'),(46414,1,3,'可'),(43513,1,3,'台'),(46348,1,3,'向'),(46330,1,3,'吸'),(46379,1,3,'吹'),(43727,1,3,'和'),(46351,1,3,'品'),(43708,1,3,'四'),(43725,1,3,'园'),(43693,1,3,'国'),(43695,1,3,'图'),(46396,1,3,'在'),(44613,1,3,'地'),(46366,1,3,'均'),(43720,1,3,'坊'),(45550,1,3,'坑'),(43643,1,3,'埃'),(46357,1,3,'墨'),(43688,1,3,'外'),(43678,1,3,'大'),(46411,1,3,'好'),(43701,1,3,'学'),(46368,1,3,'宜'),(46358,1,3,'室'),(45557,1,3,'宮'),(43694,1,3,'家'),(43665,1,3,'宽'),(45543,1,3,'寫'),(43650,1,3,'封'),(43712,1,3,'尔'),(45535,1,3,'尾'),(43709,1,3,'届'),(43702,1,3,'展'),(44457,1,3,'山'),(44578,1,3,'崖'),(43719,1,3,'工'),(46382,1,3,'布'),(45011,1,3,'带'),(1268,1,3,'帶'),(46363,1,3,'常'),(43630,1,3,'平'),(43621,1,3,'年'),(43675,1,3,'并'),(46413,1,3,'幸'),(46420,1,3,'度'),(43728,1,3,'廊'),(44615,1,3,'式'),(46334,1,3,'弹'),(43627,1,3,'彩'),(45541,1,3,'循'),(46389,1,3,'德'),(46378,1,3,'怕'),(45876,1,3,'性'),(46373,1,3,'感'),(46391,1,3,'憩'),(46716,1,3,'戒'),(46359,1,3,'户'),(44616,1,3,'手'),(46370,1,3,'护'),(46423,1,3,'拿'),(46717,1,3,'指'),(46332,1,3,'排'),(45538,1,3,'接'),(46405,1,3,'提'),(45556,1,3,'故'),(46408,1,3,'教'),(43628,1,3,'数'),(45877,1,3,'數'),(46383,1,3,'料'),(44600,1,3,'断'),(43721,1,3,'新'),(44577,1,3,'斷'),(43690,1,3,'方'),(43717,1,3,'旅'),(46352,1,3,'无'),(46362,1,3,'日'),(46355,1,3,'昇'),(44618,1,3,'明'),(44767,1,3,'星'),(46395,1,3,'是'),(43658,1,3,'普'),(45561,1,3,'書'),(43676,1,3,'有'),(43625,1,3,'本'),(43639,1,3,'术'),(46335,1,3,'机'),(46338,1,3,'材'),(46385,1,3,'来'),(43656,1,3,'林'),(46384,1,3,'案'),(45562,1,3,'桌'),(46399,1,3,'步'),(46353,1,3,'毒'),(44576,1,3,'水'),(46388,1,3,'汇'),(46333,1,3,'汗'),(44360,1,3,'波'),(44412,1,3,'洋'),(43680,1,3,'洞'),(44411,1,3,'海'),(45553,1,3,'淺'),(45222,1,3,'淼'),(43718,1,3,'清'),(46390,1,3,'游'),(46843,1,3,'湖'),(43514,1,3,'湾'),(46331,1,3,'湿'),(46375,1,3,'满'),(45007,1,3,'滩'),(44768,1,3,'潭'),(45226,1,3,'灘'),(45554,1,3,'灣'),(44357,1,3,'炫'),(46394,1,3,'点'),(44607,1,3,'烫'),(46354,1,3,'热'),(45536,1,3,'無'),(46404,1,3,'照'),(44579,1,3,'燙'),(44620,1,3,'片'),(43623,1,3,'版'),(44602,1,3,'牙'),(45559,1,3,'物'),(43659,1,3,'特'),(46381,1,3,'独'),(46407,1,3,'玩'),(46401,1,3,'珈'),(46838,1,3,'琺'),(46400,1,3,'瑜'),(46839,1,3,'瑯'),(43664,1,3,'璇'),(45542,1,3,'環'),(45544,1,3,'生'),(43616,1,3,'画'),(43679,1,3,'的'),(46365,1,3,'着'),(43637,1,3,'磅'),(46402,1,3,'示'),(43657,1,3,'科'),(43510,1,3,'移'),(43726,1,3,'空'),(46364,1,3,'穿'),(43687,1,3,'窄'),(43697,1,3,'立'),(43722,1,3,'竹'),(43707,1,3,'第'),(44604,1,3,'精'),(46842,1,3,'紅'),(44359,1,3,'紙'),(46844,1,3,'綠'),(45537,1,3,'縫'),(46342,1,3,'纤'),(46347,1,3,'纱'),(43640,1,3,'纸'),(46387,1,3,'绘'),(46343,1,3,'维'),(43638,1,3,'美'),(43633,1,3,'者'),(43706,1,3,'联'),(46340,1,3,'聚'),(43674,1,3,'背'),(45010,1,3,'胶'),(46336,1,3,'能'),(44458,1,3,'脈'),(1267,1,3,'膠'),(46386,1,3,'自'),(43698,1,3,'臺'),(43654,1,3,'色'),(43700,1,3,'艺'),(45552,1,3,'芝'),(45008,1,3,'花'),(46397,1,3,'苏'),(43715,1,3,'英'),(46403,1,3,'范'),(46412,1,3,'荣'),(45009,1,3,'莲'),(43711,1,3,'菲'),(45227,1,3,'蓮'),(46840,1,3,'藍'),(43692,1,3,'藏'),(1056,1,3,'藝'),(46841,1,3,'血'),(46361,1,3,'行'),(1057,1,3,'術'),(43652,1,3,'衬'),(43660,1,3,'装'),(46337,1,3,'裤'),(43671,1,3,'裸'),(43615,1,3,'製'),(43614,1,3,'複'),(1390,1,3,'褲'),(44348,1,3,'記'),(46422,1,3,'计'),(43661,1,3,'订'),(43512,1,3,'记'),(46421,1,3,'设'),(43713,1,3,'评'),(44601,1,3,'象'),(46339,1,3,'质'),(46398,1,3,'路'),(46376,1,3,'身'),(45539,1,3,'軌'),(46344,1,3,'轻'),(43677,1,3,'较'),(46360,1,3,'运'),(46367,1,3,'适'),(43714,1,3,'选'),(44614,1,3,'造'),(43655,1,3,'道'),(46341,1,3,'酯'),(43724,1,3,'醒'),(46345,1,3,'量'),(43645,1,3,'金'),(46715,1,3,'銀'),(44608,1,3,'银'),(43668,1,3,'长'),(43685,1,3,'间'),(43704,1,3,'际'),(45540,1,3,'限'),(45560,1,3,'院'),(46416,1,3,'陪'),(45548,1,3,'陽'),(43686,1,3,'隔'),(46419,1,3,'难'),(43710,1,3,'雪'),(43672,1,3,'露'),(43651,1,3,'面'),(43642,1,3,'页'),(46380,1,3,'风'),(43696,1,3,'馆'),(45534,1,3,'首'),(43723,1,3,'香'),(46350,1,3,'高'),(43653,1,3,'黑'),(45545,1,3,'點'),(44251,1,4,'1'),(45060,1,4,'10'),(44658,1,4,'105'),(45623,1,4,'108'),(44649,1,4,'11'),(46490,1,4,'12'),(37639,1,4,'123'),(43942,1,4,'130'),(43967,1,4,'14'),(43961,1,4,'143'),(43957,1,4,'148'),(45979,1,4,'15'),(46759,1,4,'153'),(45976,1,4,'15mm'),(43948,1,4,'1875'),(43745,1,4,'2'),(43964,1,4,'20'),(45980,1,4,'200'),(43743,1,4,'2002'),(43894,1,4,'2003'),(44008,1,4,'20042014'),(43737,1,4,'2010'),(44003,1,4,'2012'),(43998,1,4,'20134th'),(44004,1,4,'2014'),(43984,1,4,'2015'),(44009,1,4,'2016'),(43993,1,4,'2017'),(43960,1,4,'21'),(45058,1,4,'25'),(46775,1,4,'3'),(45069,1,4,'30'),(46764,1,4,'3d'),(44250,1,4,'4'),(43907,1,4,'411'),(45621,1,4,'45'),(45611,1,4,'45mm'),(46773,1,4,'4band'),(45624,1,4,'5'),(43927,1,4,'5000'),(45064,1,4,'66'),(45625,1,4,'7'),(46488,1,4,'88'),(43937,1,4,'9'),(45622,1,4,'90'),(46777,1,4,'925'),(43969,1,4,'9789574173310'),(44231,1,4,'a'),(43819,1,4,'accumulated'),(43788,1,4,'alishan'),(43837,1,4,'also'),(46499,1,4,'anti'),(43779,1,4,'around'),(43946,1,4,'art'),(43935,1,4,'artist'),(44310,1,4,'artists'),(43976,1,4,'arts'),(43884,1,4,'artwork'),(45978,1,4,'b'),(43932,1,4,'back'),(43977,1,4,'bank'),(43855,1,4,'base'),(43754,1,4,'bathroom'),(44654,1,4,'beach'),(43854,1,4,'become'),(43826,1,4,'becoming'),(43896,1,4,'bed'),(43762,1,4,'bedroom'),(43766,1,4,'beginning'),(43879,1,4,'binding'),(43954,1,4,'black'),(43769,1,4,'blonde'),(43761,1,4,'body'),(43731,1,4,'book'),(43795,1,4,'books'),(43934,1,4,'bound'),(44016,1,4,'buyer'),(43991,1,4,'ca'),(46513,1,4,'canadadesigned'),(44657,1,4,'card'),(43796,1,4,'carrier'),(43822,1,4,'carry'),(44650,1,4,'cean'),(43972,1,4,'central'),(43798,1,4,'characteristic'),(44014,1,4,'chieh'),(44653,1,4,'chihsingtan'),(44651,1,4,'ching'),(43786,1,4,'chingshui'),(45048,1,4,'chishingtan'),(45051,1,4,'clear'),(43787,1,4,'cliff'),(46517,1,4,'cliffs'),(43958,1,4,'cm'),(46762,1,4,'coast'),(46516,1,4,'coastal'),(43986,1,4,'codex'),(43970,1,4,'collection'),(43982,1,4,'collectorsrelat'),(43921,1,4,'color'),(45620,1,4,'com'),(43776,1,4,'comics'),(43875,1,4,'complex'),(43840,1,4,'contact'),(43805,1,4,'continuous'),(43928,1,4,'coptic'),(43933,1,4,'cover'),(43988,1,4,'craneway'),(43824,1,4,'creates'),(43811,1,4,'creation'),(43797,1,4,'creations'),(43829,1,4,'creative'),(43839,1,4,'creator'),(43757,1,4,'curtain'),(44229,1,4,'d'),(46487,1,4,'daily'),(46481,1,4,'dancing'),(45983,1,4,'december'),(44006,1,4,'del'),(43866,1,4,'depth'),(45984,1,4,'dergi'),(46763,1,4,'design'),(45044,1,4,'designed'),(43774,1,4,'diaries'),(46767,1,4,'differentintegr'),(43922,1,4,'digital'),(43870,1,4,'dimensional'),(43847,1,4,'disappear'),(43871,1,4,'distance'),(43756,1,4,'door'),(43759,1,4,'dorm'),(43764,1,4,'draw'),(45045,1,4,'drawing'),(43740,1,4,'drawn'),(43878,1,4,'dreams'),(43792,1,4,'duration'),(43806,1,4,'dynamic'),(44299,1,4,'e'),(43917,1,4,'east'),(46761,1,4,'eastern'),(46501,1,4,'eco'),(43963,1,4,'edition'),(43950,1,4,'egyptian'),(43882,1,4,'elements'),(43903,1,4,'eluanbi'),(43828,1,4,'entire'),(44017,1,4,'entitled'),(45619,1,4,'etsy'),(43997,1,4,'exhibition'),(43844,1,4,'experience'),(43877,1,4,'express'),(43862,1,4,'extension'),(43833,1,4,'external'),(46494,1,4,'fabric'),(45053,1,4,'face'),(43983,1,4,'fair'),(43781,1,4,'fangliao'),(43742,1,4,'feb'),(43758,1,4,'female'),(43915,1,4,'fenqihu'),(43945,1,4,'fine'),(43747,1,4,'first'),(43931,1,4,'flat'),(43800,1,4,'flipping'),(43825,1,4,'flux'),(44662,1,4,'foil'),(46768,1,4,'formed'),(43880,1,4,'forms'),(43823,1,4,'fragments'),(43872,1,4,'frames'),(46520,1,4,'frankie'),(46502,1,4,'friendly'),(43809,1,4,'fulfill'),(43920,1,4,'full'),(43893,1,4,'future'),(44226,1,4,'g'),(43768,1,4,'giudecca'),(43951,1,4,'gold'),(43996,1,4,'group'),(43859,1,4,'growth'),(43943,1,4,'gsm'),(43770,1,4,'hair'),(43739,1,4,'hand'),(46765,1,4,'handmade'),(43910,1,4,'heren'),(46505,1,4,'high'),(46485,1,4,'hiking'),(43930,1,4,'hole'),(43751,1,4,'hostel'),(44661,1,4,'hot'),(43835,1,4,'however'),(43925,1,4,'hp'),(45617,1,4,'http'),(43785,1,4,'hualien'),(43732,1,4,'i'),(43810,1,4,'image'),(43807,1,4,'imagination'),(43817,1,4,'imitate'),(43845,1,4,'incidents'),(43926,1,4,'indigo'),(45047,1,4,'ink'),(45613,1,4,'inner'),(46760,1,4,'inspired'),(43867,1,4,'instead'),(43864,1,4,'intend'),(43832,1,4,'intended'),(43790,1,4,'interested'),(43834,1,4,'internal'),(43999,1,4,'international'),(43968,1,4,'isbn'),(44656,1,4,'ivory'),(43741,1,4,'jan'),(45290,1,4,'japanese'),(43830,1,4,'journey'),(45070,1,4,'july'),(45065,1,4,'june'),(43949,1,4,'kg'),(45043,1,4,'kind'),(43901,1,4,'kissing'),(44230,1,4,'l'),(45612,1,4,'landscape'),(43852,1,4,'last'),(43888,1,4,'left'),(46527,1,4,'leggings'),(43838,1,4,'lets'),(43973,1,4,'library'),(43816,1,4,'life'),(46486,1,4,'light'),(43962,1,4,'limited'),(43904,1,4,'line'),(43889,1,4,'link'),(43966,1,4,'listing'),(43923,1,4,'lithography'),(44015,1,4,'liu'),(43985,1,4,'liuyingchieh'),(43849,1,4,'long'),(45062,1,4,'loop'),(45061,1,4,'m'),(45068,1,4,'made'),(46512,1,4,'manufactured'),(45614,1,4,'masking'),(43767,1,4,'memories'),(43793,1,4,'memory'),(43953,1,4,'metallic'),(46500,1,4,'microbial'),(44659,1,4,'mm'),(46776,1,4,'mmsterling'),(43812,1,4,'mode'),(46495,1,4,'moisture'),(43886,1,4,'moment'),(43874,1,4,'montage'),(43773,1,4,'monthes'),(43911,1,4,'moonrise'),(43808,1,4,'motivation'),(46515,1,4,'mountain'),(43802,1,4,'movement'),(43777,1,4,'movies'),(43813,1,4,'moving'),(44228,1,4,'n'),(43760,1,4,'naked'),(43971,1,4,'national'),(45040,1,4,'new'),(43856,1,4,'next'),(46503,1,4,'non'),(43887,1,4,'now'),(43965,1,4,'numbered'),(44227,1,4,'o'),(43913,1,4,'ocean'),(44660,1,4,'offset'),(45042,1,4,'one'),(43941,1,4,'open'),(43818,1,4,'opened'),(46508,1,4,'order'),(43906,1,4,'ordinary'),(44010,1,4,'original'),(46484,1,4,'outdoor'),(43848,1,4,'overnight'),(43912,1,4,'pacific'),(43799,1,4,'page'),(43821,1,4,'pages'),(44007,1,4,'paine'),(45615,1,4,'painting'),(43947,1,4,'paper'),(45056,1,4,'paper1'),(45291,1,4,'paperset'),(43890,1,4,'past'),(46514,1,4,'pattern'),(43989,1,4,'pavilion'),(45050,1,4,'pebble'),(45046,1,4,'pen'),(46492,1,4,'performance'),(43850,1,4,'period'),(46519,1,4,'photo'),(43782,1,4,'pinghsi'),(46489,1,4,'polyester'),(44668,1,4,'postcard'),(44647,1,4,'postcards'),(43851,1,4,'precipitation'),(43791,1,4,'preserving'),(45066,1,4,'printed'),(43924,1,4,'printing'),(43981,1,4,'private'),(44001,1,4,'prize'),(43858,1,4,'promote'),(44012,1,4,'property'),(43735,1,4,'published'),(46770,1,4,'purpose'),(46506,1,4,'quality'),(44298,1,4,'r'),(43916,1,4,'railway'),(43814,1,4,'real'),(43857,1,4,'reality'),(43831,1,4,'really'),(44011,1,4,'remains'),(43861,1,4,'repetition'),(43738,1,4,'reproduction'),(43804,1,4,'resulting'),(43990,1,4,'richmond'),(43918,1,4,'rift'),(43891,1,4,'right'),(44018,1,4,'rights'),(46771,1,4,'ring'),(43775,1,4,'road'),(45049,1,4,'rocky'),(45057,1,4,'roll'),(45292,1,4,'rolls'),(43898,1,4,'roomate'),(43771,1,4,'roommate'),(45981,1,4,'round'),(43909,1,4,'running'),(44000,1,4,'s'),(43783,1,4,'sandiaoling'),(43753,1,4,'saw'),(45041,1,4,'sealed'),(45063,1,4,'second'),(43939,1,4,'section'),(43736,1,4,'september'),(43873,1,4,'sequences'),(45616,1,4,'series'),(44646,1,4,'set'),(43772,1,4,'several'),(43940,1,4,'sewn'),(43979,1,4,'sheffield'),(43752,1,4,'shocked'),(43827,1,4,'show'),(43801,1,4,'showing'),(44652,1,4,'shui'),(43938,1,4,'signatures'),(44371,1,4,'significant'),(44664,1,4,'silver'),(43765,1,4,'since'),(46509,1,4,'size'),(43900,1,4,'sketch'),(44648,1,4,'sketches'),(46766,1,4,'slightly'),(45985,1,4,'sold'),(44002,1,4,'solo'),(43803,1,4,'space'),(46491,1,4,'spandex'),(43929,1,4,'special'),(46483,1,4,'spinning'),(43865,1,4,'spiritual'),(46480,1,4,'sport'),(43885,1,4,'spread'),(43860,1,4,'stacking'),(43876,1,4,'staggered'),(44663,1,4,'stamping'),(43763,1,4,'started'),(43836,1,4,'state'),(43789,1,4,'statement'),(43905,1,4,'station'),(43749,1,4,'stayed'),(45982,1,4,'stickermade'),(43843,1,4,'stimulation'),(43902,1,4,'stone'),(43908,1,4,'stop'),(43978,1,4,'street'),(46498,1,4,'stretch'),(43820,1,4,'strokes'),(43842,1,4,'strong'),(43863,1,4,'structure'),(46769,1,4,'sturdy'),(46507,1,4,'sublimation'),(46479,1,4,'summer'),(43914,1,4,'sunrise'),(44300,1,4,'t'),(38017,1,4,'t123'),(45977,1,4,'t123456789'),(45071,1,4,'tain'),(43974,1,4,'taipei'),(43734,1,4,'taiwan'),(44655,1,4,'taiwan240g'),(45054,1,4,'taiwanese'),(45073,1,4,'tape'),(43869,1,4,'three'),(43883,1,4,'throughout'),(43748,1,4,'time'),(46518,1,4,'top'),(44005,1,4,'torres'),(46504,1,4,'toxic'),(43780,1,4,'train'),(43730,1,4,'travel'),(43778,1,4,'traveled'),(43897,1,4,'traveler'),(43853,1,4,'trip'),(43815,1,4,'trying'),(43980,1,4,'uk'),(43955,1,4,'uncoated'),(43892,1,4,'undertake'),(43841,1,4,'unexpected'),(43975,1,4,'university'),(46772,1,4,'us'),(43992,1,4,'usa'),(43995,1,4,'usarelated'),(43794,1,4,'use'),(43987,1,4,'v'),(43919,1,4,'valley'),(43895,1,4,'venezia'),(43733,1,4,'venice'),(43994,1,4,'vi'),(43868,1,4,'virtual'),(43881,1,4,'visual'),(46521,1,4,'wang'),(45055,1,4,'washi'),(43899,1,4,'wave'),(45052,1,4,'waves'),(46497,1,4,'way'),(46493,1,4,'wear'),(43746,1,4,'weeks'),(43744,1,4,'went'),(43944,1,4,'white'),(46496,1,4,'wicking'),(45059,1,4,'wide'),(46774,1,4,'width'),(43846,1,4,'will'),(43755,1,4,'without'),(43956,1,4,'woodfree'),(45618,1,4,'www'),(43959,1,4,'x'),(46511,1,4,'xl'),(46510,1,4,'xs'),(43784,1,4,'yilan'),(44013,1,4,'ying'),(46482,1,4,'yoga'),(43936,1,4,'yoshosu'),(43750,1,4,'youth'),(45067,1,4,'yuanpao'),(43952,1,4,'zipang'),(44376,1,4,'あ'),(44378,1,4,'じ'),(44379,1,4,'ま'),(44377,1,4,'ら'),(44375,1,4,'イ'),(44275,1,4,'グ'),(44272,1,4,'ジ'),(44372,1,4,'ト'),(44273,1,4,'パ'),(44374,1,4,'メ'),(44274,1,4,'ン'),(44373,1,4,'ー'),(44150,1,4,'一'),(44665,1,4,'七'),(44023,1,4,'三'),(44149,1,4,'上'),(44152,1,4,'下'),(44127,1,4,'不'),(44138,1,4,'並'),(44098,1,4,'中'),(44111,1,4,'之'),(45637,1,4,'九'),(44125,1,4,'也'),(44083,1,4,'了'),(44468,1,4,'二'),(44156,1,4,'互'),(44135,1,4,'些'),(44158,1,4,'交'),(44260,1,4,'仁'),(44176,1,4,'代'),(44052,1,4,'以'),(45638,1,4,'份'),(44095,1,4,'仿'),(44165,1,4,'伸'),(44057,1,4,'作'),(44166,1,4,'來'),(44040,1,4,'保'),(1245,1,4,'信'),(44301,1,4,'個'),(45987,1,4,'值'),(44079,1,4,'像'),(44206,1,4,'元'),(44381,1,4,'光'),(44120,1,4,'內'),(44084,1,4,'全'),(44276,1,4,'典'),(44054,1,4,'冊'),(45641,1,4,'冷'),(44263,1,4,'出'),(44180,1,4,'分'),(44188,1,4,'列'),(44132,1,4,'刺'),(44108,1,4,'刻'),(45651,1,4,'前'),(44384,1,4,'剛'),(44056,1,4,'創'),(44221,1,4,'劉'),(44077,1,4,'動'),(44110,1,4,'化'),(44283,1,4,'北'),(44421,1,4,'十'),(45647,1,4,'博'),(44211,1,4,'即'),(45626,1,4,'卷'),(44218,1,4,'去'),(44254,1,4,'又'),(44175,1,4,'取'),(44385,1,4,'古'),(44224,1,4,'台'),(44219,1,4,'右'),(44124,1,4,'同'),(44234,1,4,'吻'),(44066,1,4,'呈'),(44113,1,4,'命'),(44259,1,4,'和'),(44291,1,4,'四'),(44151,1,4,'回'),(44278,1,4,'國'),(44308,1,4,'園'),(44085,1,4,'圖'),(44096,1,4,'在'),(45635,1,4,'地'),(44303,1,4,'坊'),(45642,1,4,'坑'),(44153,1,4,'基'),(44161,1,4,'堆'),(44198,1,4,'境'),(44117,1,4,'外'),(44141,1,4,'夜'),(44197,1,4,'夢'),(44286,1,4,'大'),(44190,1,4,'太'),(44144,1,4,'失'),(44191,1,4,'奇'),(44264,1,4,'奮'),(44041,1,4,'存'),(44287,1,4,'學'),(44026,1,4,'宜'),(46526,1,4,'室'),(45646,1,4,'宮'),(44279,1,4,'家'),(44092,1,4,'實'),(44232,1,4,'寫'),(44020,1,4,'寮'),(44182,1,4,'對'),(44267,1,4,'小'),(44142,1,4,'就'),(45628,1,4,'尾'),(44292,1,4,'屆'),(44288,1,4,'展'),(44036,1,4,'山'),(44235,1,4,'岩'),(44033,1,4,'崖'),(44025,1,4,'嶺'),(44302,1,4,'工'),(44213,1,4,'左'),(1271,1,4,'帶'),(44021,1,4,'平'),(44290,1,4,'年'),(44256,1,4,'幸'),(44187,1,4,'序'),(44154,1,4,'底'),(44174,1,4,'度'),(44309,1,4,'廊'),(44046,1,4,'延'),(44087,1,4,'式'),(44130,1,4,'強'),(44201,1,4,'形'),(44241,1,4,'後'),(44239,1,4,'從'),(45633,1,4,'循'),(44171,1,4,'心'),(44076,1,4,'性'),(44078,1,4,'想'),(44116,1,4,'意'),(44048,1,4,'態'),(44043,1,4,'憶'),(44073,1,4,'成'),(46779,1,4,'戒'),(44192,1,4,'手'),(44105,1,4,'承'),(1411,1,4,'指'),(44223,1,4,'捷'),(44128,1,4,'接'),(44159,1,4,'推'),(44051,1,4,'擇'),(44209,1,4,'攤'),(45645,1,4,'故'),(46525,1,4,'教'),(44114,1,4,'整'),(45986,1,4,'數'),(44129,1,4,'料'),(44304,1,4,'新'),(44032,1,4,'斷'),(44039,1,4,'於'),(44080,1,4,'旅'),(44262,1,4,'日'),(45640,1,4,'明'),(44666,1,4,'星'),(44049,1,4,'是'),(44068,1,4,'時'),(44053,1,4,'書'),(44139,1,4,'會'),(44220,1,4,'未'),(44123,1,4,'本'),(44269,1,4,'東'),(44242,1,4,'松'),(44019,1,4,'枋'),(44181,1,4,'格'),(45650,1,4,'桌'),(44168,1,4,'構'),(44086,1,4,'模'),(44246,1,4,'樹'),(44082,1,4,'機'),(44090,1,4,'正'),(44253,1,4,'步'),(44137,1,4,'歷'),(44107,1,4,'段'),(44031,1,4,'水'),(44147,1,4,'沈'),(44193,1,4,'法'),(44383,1,4,'波'),(44258,1,4,'泥'),(44261,1,4,'洋'),(44094,1,4,'活'),(44112,1,4,'流'),(44248,1,4,'海'),(44143,1,4,'消'),(44173,1,4,'深'),(45644,1,4,'淺'),(44030,1,4,'清'),(44266,1,4,'湖'),(44022,1,4,'溪'),(44667,1,4,'潭'),(44148,1,4,'澱'),(44133,1,4,'激'),(45072,1,4,'灘'),(44225,1,4,'灣'),(44268,1,4,'火'),(44380,1,4,'炫'),(44055,1,4,'為'),(44131,1,4,'烈'),(45629,1,4,'無'),(44121,1,4,'然'),(44037,1,4,'熱'),(44644,1,4,'燙'),(44295,1,4,'爾'),(44106,1,4,'片'),(45648,1,4,'物'),(44064,1,4,'特'),(44047,1,4,'狀'),(46522,1,4,'玩'),(46524,1,4,'珈'),(44067,1,4,'現'),(46851,1,4,'琺'),(46523,1,4,'瑜'),(46852,1,4,'瑯'),(45634,1,4,'環'),(44093,1,4,'生'),(44169,1,4,'畫'),(44212,1,4,'當'),(44162,1,4,'疊'),(44044,1,4,'的'),(44157,1,4,'相'),(44089,1,4,'真'),(44115,1,4,'確'),(44257,1,4,'福'),(44071,1,4,'移'),(44119,1,4,'程'),(44102,1,4,'積'),(44222,1,4,'穎'),(44069,1,4,'空'),(44203,1,4,'穿'),(44281,1,4,'立'),(44243,1,4,'站'),(44305,1,4,'竹'),(44249,1,4,'第'),(44103,1,4,'筆'),(44184,1,4,'粹'),(46854,1,4,'紅'),(44183,1,4,'純'),(44382,1,4,'紙'),(44207,1,4,'素'),(44101,1,4,'累'),(44167,1,4,'結'),(44136,1,4,'經'),(44245,1,4,'綠'),(44045,1,4,'綿'),(44244,1,4,'線'),(45630,1,4,'縫'),(44270,1,4,'縱'),(44185,1,4,'繪'),(44216,1,4,'繫'),(44075,1,4,'續'),(44194,1,4,'置'),(44063,1,4,'翻'),(44126,1,4,'者'),(44122,1,4,'而'),(44215,1,4,'聯'),(44469,1,4,'脈'),(1270,1,4,'膠'),(44282,1,4,'臺'),(44155,1,4,'與'),(45643,1,4,'芝'),(44028,1,4,'花'),(44297,1,4,'英'),(44294,1,4,'菲'),(44217,1,4,'著'),(44189,1,4,'蒙'),(44029,1,4,'蓮'),(44247,1,4,'藍'),(44277,1,4,'藏'),(44284,1,4,'藝'),(44027,1,4,'蘭'),(44179,1,4,'虛'),(44252,1,4,'號'),(46853,1,4,'血'),(44081,1,4,'行'),(44285,1,4,'術'),(44196,1,4,'表'),(44038,1,4,'衷'),(44208,1,4,'被'),(44199,1,4,'裝'),(44164,1,4,'複'),(1398,1,4,'褲'),(44204,1,4,'視'),(44233,1,4,'親'),(44205,1,4,'覺'),(44104,1,4,'觸'),(44214,1,4,'言'),(44200,1,4,'訂'),(44042,1,4,'記'),(44296,1,4,'評'),(44109,1,4,'變'),(44091,1,4,'讓'),(44271,1,4,'谷'),(44024,1,4,'貂'),(44202,1,4,'貫'),(44065,1,4,'質'),(44265,1,4,'起'),(44177,1,4,'距'),(44088,1,4,'身'),(44240,1,4,'車'),(45631,1,4,'軌'),(44058,1,4,'載'),(44070,1,4,'轉'),(44255,1,4,'近'),(44060,1,4,'透'),(44100,1,4,'逐'),(44097,1,4,'途'),(44134,1,4,'這'),(44072,1,4,'造'),(44074,1,4,'連'),(44160,1,4,'進'),(44061,1,4,'過'),(44050,1,4,'選'),(44118,1,4,'部'),(44307,1,4,'醒'),(44035,1,4,'里'),(44163,1,4,'重'),(44645,1,4,'金'),(46778,1,4,'銀'),(44195,1,4,'錯'),(44237,1,4,'鑾'),(44145,1,4,'長'),(44099,1,4,'開'),(44146,1,4,'間'),(44034,1,4,'阿'),(45632,1,4,'限'),(45649,1,4,'院'),(45639,1,4,'陽'),(44140,1,4,'隔'),(44289,1,4,'際'),(44210,1,4,'雙'),(44186,1,4,'雜'),(44178,1,4,'離'),(44293,1,4,'雪'),(44172,1,4,'靈'),(44170,1,4,'面'),(44062,1,4,'頁'),(44280,1,4,'館'),(45627,1,4,'首'),(44306,1,4,'香'),(44059,1,4,'體'),(44236,1,4,'鵝'),(45636,1,4,'點'),(44238,1,4,'鼻');
/*!40000 ALTER TABLE `ps_search_word` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_search_word_cms`
--

DROP TABLE IF EXISTS `ps_search_word_cms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_search_word_cms` (
  `id_word` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_lang` int(10) unsigned NOT NULL,
  `word` varchar(15) NOT NULL,
  PRIMARY KEY (`id_word`),
  UNIQUE KEY `id_lang` (`id_lang`,`id_shop`,`word`)
) ENGINE=InnoDB AUTO_INCREMENT=736 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_search_word_cms`
--

LOCK TABLES `ps_search_word_cms` WRITE;
/*!40000 ALTER TABLE `ps_search_word_cms` DISABLE KEYS */;
INSERT INTO `ps_search_word_cms` VALUES (700,1,1,'1314'),(432,1,1,'14'),(430,1,1,'2018'),(51,1,1,'3'),(57,1,1,'7'),(431,1,1,'9'),(6,1,1,'atm'),(433,1,1,'cookie'),(5,1,1,'facebook'),(434,1,1,'ip'),(699,1,1,'product'),(376,1,1,'一'),(318,1,1,'七'),(464,1,1,'三'),(367,1,1,'不'),(343,1,1,'並'),(322,1,1,'之'),(695,1,1,'事'),(479,1,1,'些'),(381,1,1,'享'),(451,1,1,'人'),(492,1,1,'他'),(36,1,1,'付'),(54,1,1,'代'),(364,1,1,'以'),(333,1,1,'件'),(442,1,1,'任'),(505,1,1,'位'),(443,1,1,'何'),(455,1,1,'使'),(490,1,1,'來'),(61,1,1,'例'),(316,1,1,'供'),(309,1,1,'保'),(450,1,1,'個'),(8,1,1,'們'),(62,1,1,'假'),(484,1,1,'傳'),(456,1,1,'僅'),(496,1,1,'充'),(350,1,1,'免'),(12,1,1,'入'),(53,1,1,'內'),(336,1,1,'全'),(491,1,1,'其'),(38,1,1,'出'),(497,1,1,'分'),(58,1,1,'到'),(380,1,1,'則'),(15,1,1,'加'),(495,1,1,'助'),(502,1,1,'動'),(461,1,1,'務'),(475,1,1,'勵'),(346,1,1,'包'),(494,1,1,'協'),(18,1,1,'即'),(355,1,1,'原'),(303,1,1,'及'),(44,1,1,'取'),(344,1,1,'另'),(19,1,1,'可'),(313,1,1,'名'),(447,1,1,'吝'),(448,1,1,'告'),(498,1,1,'和'),(25,1,1,'品'),(17,1,1,'員'),(302,1,1,'售'),(24,1,1,'商'),(444,1,1,'問'),(40,1,1,'單'),(509,1,1,'器'),(329,1,1,'回'),(64,1,1,'國'),(483,1,1,'在'),(506,1,1,'址'),(510,1,1,'型'),(27,1,1,'填'),(352,1,1,'壞'),(345,1,1,'外'),(500,1,1,'多'),(499,1,1,'大'),(59,1,1,'天'),(356,1,1,'如'),(46,1,1,'宅'),(338,1,1,'完'),(65,1,1,'定'),(487,1,1,'密'),(28,1,1,'寫'),(379,1,1,'封'),(357,1,1,'將'),(388,1,1,'對'),(435,1,1,'已'),(48,1,1,'帳'),(439,1,1,'常'),(436,1,1,'年'),(312,1,1,'店'),(466,1,1,'廠'),(35,1,1,'式'),(365,1,1,'影'),(334,1,1,'必'),(698,1,1,'息'),(317,1,1,'您'),(363,1,1,'意'),(386,1,1,'感'),(342,1,1,'態'),(7,1,1,'我'),(354,1,1,'或'),(328,1,1,'所'),(488,1,1,'技'),(378,1,1,'拆'),(468,1,1,'括'),(389,1,1,'持'),(315,1,1,'提'),(10,1,1,'援'),(31,1,1,'擇'),(306,1,1,'據'),(9,1,1,'支'),(453,1,1,'收'),(428,1,1,'政'),(694,1,1,'故'),(446,1,1,'敬'),(339,1,1,'整'),(60,1,1,'數'),(30,1,1,'料'),(337,1,1,'新'),(34,1,1,'方'),(50,1,1,'於'),(52,1,1,'日'),(4,1,1,'明'),(335,1,1,'是'),(513,1,1,'時'),(438,1,1,'更'),(697,1,1,'最'),(16,1,1,'會'),(437,1,1,'月'),(382,1,1,'有'),(460,1,1,'服'),(321,1,1,'期'),(305,1,1,'根'),(501,1,1,'樣'),(323,1,1,'權'),(37,1,1,'款'),(66,1,1,'法'),(21,1,1,'流'),(307,1,1,'消'),(507,1,1,'瀏'),(358,1,1,'為'),(13,1,1,'無'),(361,1,1,'煩'),(693,1,1,'牌'),(2,1,1,'物'),(341,1,1,'狀'),(360,1,1,'理'),(374,1,1,'生'),(327,1,1,'用'),(362,1,1,'留'),(11,1,1,'登'),(340,1,1,'的'),(324,1,1,'益'),(353,1,1,'盒'),(458,1,1,'相'),(476,1,1,'瞭'),(449,1,1,'知'),(351,1,1,'破'),(55,1,1,'碼'),(427,1,1,'私'),(22,1,1,'程'),(314,1,1,'稱'),(481,1,1,'站'),(463,1,1,'第'),(473,1,1,'等'),(429,1,1,'策'),(470,1,1,'細'),(472,1,1,'絡'),(462,1,1,'給'),(377,1,1,'經'),(480,1,1,'網'),(56,1,1,'繳'),(308,1,1,'者'),(471,1,1,'聯'),(467,1,1,'能'),(369,1,1,'自'),(63,1,1,'與'),(375,1,1,'若'),(68,1,1,'行'),(489,1,1,'術'),(373,1,1,'衛'),(348,1,1,'袋'),(371,1,1,'補'),(347,1,1,'裝'),(730,1,1,'見'),(311,1,1,'規'),(441,1,1,'視'),(508,1,1,'覽'),(477,1,1,'解'),(512,1,1,'言'),(39,1,1,'訂'),(452,1,1,'訊'),(326,1,1,'試'),(469,1,1,'詳'),(511,1,1,'語'),(3,1,1,'說'),(49,1,1,'請'),(385,1,1,'諒'),(387,1,1,'謝'),(310,1,1,'護'),(45,1,1,'貨'),(20,1,1,'買'),(42,1,1,'費'),(29,1,1,'資'),(320,1,1,'賞'),(1,1,1,'購'),(330,1,1,'贈'),(43,1,1,'超'),(504,1,1,'路'),(26,1,1,'車'),(485,1,1,'輸'),(47,1,1,'轉'),(359,1,1,'辦'),(304,1,1,'退'),(33,1,1,'送'),(478,1,1,'這'),(67,1,1,'進'),(41,1,1,'運'),(23,1,1,'選'),(349,1,1,'避'),(383,1,1,'還'),(503,1,1,'部'),(370,1,1,'郵'),(482,1,1,'都'),(32,1,1,'配'),(440,1,1,'重'),(465,1,1,'金'),(301,1,1,'銷'),(319,1,1,'鑑'),(486,1,1,'間'),(459,1,1,'關'),(332,1,1,'附'),(457,1,1,'限'),(331,1,1,'隨'),(426,1,1,'隱'),(454,1,1,'集'),(368,1,1,'需'),(325,1,1,'非'),(366,1,1,'響'),(14,1,1,'須'),(445,1,1,'題'),(493,1,1,'類'),(372,1,1,'食'),(384,1,1,'體'),(514,1,1,'點'),(474,1,1,'鼓'),(120,1,2,'according'),(92,1,2,'additional'),(572,1,2,'adipisicing'),(123,1,2,'advise'),(581,1,2,'aliqua'),(591,1,2,'aliquip'),(569,1,2,'amet'),(142,1,2,'amply'),(617,1,2,'anim'),(95,1,2,'applied'),(133,1,2,'apply'),(595,1,2,'aute'),(141,1,2,'boxes'),(136,1,2,'care'),(101,1,2,'choice'),(98,1,2,'choosing'),(601,1,2,'cillum'),(592,1,2,'commodo'),(562,1,2,'conditions'),(570,1,2,'conse'),(593,1,2,'consequat'),(97,1,2,'contact'),(93,1,2,'cost'),(115,1,2,'costs'),(571,1,2,'ctetur'),(612,1,2,'culpa'),(608,1,2,'cupidatat'),(78,1,2,'days'),(89,1,2,'delivery'),(615,1,2,'deserunt'),(76,1,2,'dispatched'),(129,1,2,'distinct'),(567,1,2,'dolor'),(579,1,2,'dolore'),(85,1,2,'drop'),(594,1,2,'duis'),(575,1,2,'eiusmod'),(573,1,2,'elit'),(582,1,2,'enim'),(600,1,2,'esse'),(618,1,2,'est'),(605,1,2,'excepteur'),(587,1,2,'exercitation'),(90,1,2,'extra'),(109,1,2,'fees'),(116,1,2,'fixed'),(139,1,2,'fragile'),(602,1,2,'fugiat'),(75,1,2,'generally'),(124,1,2,'group'),(69,1,2,'guide'),(111,1,2,'handling'),(577,1,2,'incididunt'),(110,1,2,'include'),(566,1,2,'ipsum'),(596,1,2,'irure'),(125,1,2,'items'),(578,1,2,'labore'),(589,1,2,'laboris'),(619,1,2,'laborum'),(104,1,2,'link'),(565,1,2,'lorem'),(580,1,2,'magna'),(102,1,2,'make'),(708,1,2,'mastercard'),(99,1,2,'method'),(583,1,2,'minim'),(616,1,2,'mollit'),(703,1,2,'news'),(590,1,2,'nisi'),(609,1,2,'non'),(586,1,2,'nostrud'),(603,1,2,'nulla'),(140,1,2,'objects'),(607,1,2,'occaecat'),(614,1,2,'officia'),(126,1,2,'one'),(107,1,2,'online'),(127,1,2,'order'),(130,1,2,'orders'),(72,1,2,'pack'),(106,1,2,'package'),(74,1,2,'packages'),(112,1,2,'packing'),(604,1,2,'pariatur'),(80,1,2,'payment'),(709,1,2,'paypal'),(131,1,2,'placed'),(96,1,2,'please'),(114,1,2,'postage'),(88,1,2,'prefer'),(560,1,2,'privacy'),(610,1,2,'proident'),(138,1,2,'protect'),(144,1,2,'protected'),(103,1,2,'provide'),(613,1,2,'qui'),(585,1,2,'quis'),(79,1,2,'receipt'),(409,1,2,'refund'),(597,1,2,'reprehenderit'),(91,1,2,'required'),(71,1,2,'returns'),(134,1,2,'risk'),(564,1,2,'rule'),(408,1,2,'sales'),(704,1,2,'secure'),(574,1,2,'sed'),(132,1,2,'separately'),(710,1,2,'services'),(73,1,2,'shipment'),(70,1,2,'shipments'),(81,1,2,'shipped'),(108,1,2,'shipping'),(87,1,2,'signature'),(606,1,2,'sint'),(568,1,2,'sit'),(143,1,2,'sized'),(135,1,2,'special'),(705,1,2,'ssl'),(611,1,2,'sunt'),(137,1,2,'taken'),(576,1,2,'tempor'),(561,1,2,'terms'),(121,1,2,'total'),(105,1,2,'track'),(84,1,2,'tracking'),(118,1,2,'transport'),(128,1,2,'two'),(588,1,2,'ullamco'),(83,1,2,'ups'),(563,1,2,'use'),(706,1,2,'using'),(119,1,2,'vary'),(599,1,2,'velit'),(584,1,2,'veniam'),(620,1,2,'veniamyu'),(82,1,2,'via'),(707,1,2,'visa'),(598,1,2,'voluptate'),(122,1,2,'weight'),(113,1,2,'well'),(117,1,2,'whereas'),(100,1,2,'whichever'),(94,1,2,'will'),(77,1,2,'within'),(86,1,2,'without'),(626,1,3,'1'),(154,1,3,'2'),(690,1,3,'3'),(198,1,3,'according'),(647,1,3,'ad'),(169,1,3,'additional'),(634,1,3,'adipisicing'),(201,1,3,'advise'),(645,1,3,'aliqua'),(656,1,3,'aliquip'),(631,1,3,'amet'),(220,1,3,'amply'),(685,1,3,'anim'),(172,1,3,'applied'),(211,1,3,'apply'),(662,1,3,'aute'),(219,1,3,'boxes'),(214,1,3,'care'),(179,1,3,'choice'),(176,1,3,'choosing'),(668,1,3,'cillum'),(659,1,3,'commodo'),(414,1,3,'concept'),(623,1,3,'conditions'),(632,1,3,'conse'),(660,1,3,'consequat'),(174,1,3,'contact'),(170,1,3,'cost'),(193,1,3,'costs'),(418,1,3,'created'),(413,1,3,'credits'),(633,1,3,'ctetur'),(680,1,3,'culpa'),(676,1,3,'cupidatat'),(155,1,3,'days'),(166,1,3,'delivery'),(683,1,3,'deserunt'),(152,1,3,'dispatched'),(207,1,3,'distinct'),(629,1,3,'dolor'),(643,1,3,'dolore'),(162,1,3,'drop'),(661,1,3,'duis'),(658,1,3,'ea'),(637,1,3,'eiusmod'),(635,1,3,'elit'),(646,1,3,'enim'),(667,1,3,'esse'),(687,1,3,'est'),(642,1,3,'et'),(669,1,3,'eu'),(657,1,3,'ex'),(673,1,3,'excepteur'),(652,1,3,'exercitation'),(167,1,3,'extra'),(187,1,3,'fees'),(194,1,3,'fixed'),(217,1,3,'fragile'),(670,1,3,'fugiat'),(151,1,3,'generally'),(202,1,3,'group'),(145,1,3,'guide'),(189,1,3,'handling'),(686,1,3,'id'),(639,1,3,'incididunt'),(188,1,3,'include'),(628,1,3,'ipsum'),(663,1,3,'irure'),(203,1,3,'items'),(641,1,3,'labore'),(654,1,3,'laboris'),(688,1,3,'laborum'),(412,1,3,'legal'),(182,1,3,'link'),(627,1,3,'lorem'),(644,1,3,'magna'),(180,1,3,'make'),(716,1,3,'mastercard'),(177,1,3,'method'),(648,1,3,'minim'),(684,1,3,'mollit'),(712,1,3,'news'),(655,1,3,'nisi'),(677,1,3,'non'),(651,1,3,'nostrud'),(671,1,3,'nulla'),(218,1,3,'objects'),(675,1,3,'occaecat'),(682,1,3,'officia'),(204,1,3,'one'),(185,1,3,'online'),(421,1,3,'open'),(205,1,3,'order'),(208,1,3,'orders'),(148,1,3,'pack'),(184,1,3,'package'),(150,1,3,'packages'),(190,1,3,'packing'),(672,1,3,'pariatur'),(157,1,3,'payment'),(717,1,3,'paypal'),(209,1,3,'placed'),(173,1,3,'please'),(192,1,3,'postage'),(165,1,3,'prefer'),(420,1,3,'prestashop'),(621,1,3,'privacy'),(415,1,3,'production'),(678,1,3,'proident'),(216,1,3,'protect'),(222,1,3,'protected'),(181,1,3,'provide'),(734,1,3,'q'),(681,1,3,'qui'),(650,1,3,'quis'),(156,1,3,'receipt'),(411,1,3,'refund'),(664,1,3,'reprehenderit'),(168,1,3,'required'),(147,1,3,'returns'),(212,1,3,'risk'),(625,1,3,'rule'),(410,1,3,'sales'),(713,1,3,'secure'),(636,1,3,'sed'),(210,1,3,'separately'),(718,1,3,'services'),(149,1,3,'shipment'),(146,1,3,'shipments'),(158,1,3,'shipped'),(186,1,3,'shipping'),(164,1,3,'signature'),(674,1,3,'sint'),(630,1,3,'sit'),(417,1,3,'site'),(221,1,3,'sized'),(423,1,3,'software'),(422,1,3,'source'),(213,1,3,'special'),(714,1,3,'ssl'),(679,1,3,'sunt'),(215,1,3,'taken'),(638,1,3,'tempor'),(622,1,3,'terms'),(199,1,3,'total'),(183,1,3,'track'),(161,1,3,'tracking'),(196,1,3,'transport'),(206,1,3,'two'),(653,1,3,'ullamco'),(160,1,3,'ups'),(175,1,3,'us'),(624,1,3,'use'),(419,1,3,'using'),(640,1,3,'ut'),(197,1,3,'vary'),(666,1,3,'velit'),(649,1,3,'veniam'),(689,1,3,'veniamyu'),(159,1,3,'via'),(715,1,3,'visa'),(665,1,3,'voluptate'),(416,1,3,'web'),(200,1,3,'weight'),(191,1,3,'well'),(195,1,3,'whereas'),(178,1,3,'whichever'),(171,1,3,'will'),(153,1,3,'within'),(163,1,3,'without'),(232,1,4,'2'),(276,1,4,'according'),(247,1,4,'additional'),(279,1,4,'advise'),(298,1,4,'amply'),(250,1,4,'applied'),(289,1,4,'apply'),(297,1,4,'boxes'),(292,1,4,'care'),(257,1,4,'choice'),(254,1,4,'choosing'),(252,1,4,'contact'),(248,1,4,'cost'),(271,1,4,'costs'),(233,1,4,'days'),(244,1,4,'delivery'),(230,1,4,'dispatched'),(285,1,4,'distinct'),(240,1,4,'drop'),(245,1,4,'extra'),(265,1,4,'fees'),(272,1,4,'fixed'),(295,1,4,'fragile'),(229,1,4,'generally'),(280,1,4,'group'),(223,1,4,'guide'),(267,1,4,'handling'),(266,1,4,'include'),(281,1,4,'items'),(260,1,4,'link'),(258,1,4,'make'),(726,1,4,'mastercard'),(255,1,4,'method'),(721,1,4,'news'),(296,1,4,'objects'),(282,1,4,'one'),(263,1,4,'online'),(283,1,4,'order'),(286,1,4,'orders'),(226,1,4,'pack'),(262,1,4,'package'),(228,1,4,'packages'),(268,1,4,'packing'),(235,1,4,'payment'),(727,1,4,'paypal'),(287,1,4,'placed'),(251,1,4,'please'),(270,1,4,'postage'),(243,1,4,'prefer'),(692,1,4,'privacy'),(294,1,4,'protect'),(300,1,4,'protected'),(259,1,4,'provide'),(735,1,4,'q'),(234,1,4,'receipt'),(425,1,4,'refund'),(246,1,4,'required'),(225,1,4,'returns'),(290,1,4,'risk'),(424,1,4,'sales'),(722,1,4,'secure'),(288,1,4,'separately'),(728,1,4,'services'),(227,1,4,'shipment'),(224,1,4,'shipments'),(236,1,4,'shipped'),(264,1,4,'shipping'),(242,1,4,'signature'),(299,1,4,'sized'),(291,1,4,'special'),(723,1,4,'ssl'),(293,1,4,'taken'),(277,1,4,'total'),(261,1,4,'track'),(239,1,4,'tracking'),(274,1,4,'transport'),(284,1,4,'two'),(238,1,4,'ups'),(253,1,4,'us'),(724,1,4,'using'),(275,1,4,'vary'),(237,1,4,'via'),(725,1,4,'visa'),(278,1,4,'weight'),(269,1,4,'well'),(273,1,4,'whereas'),(256,1,4,'whichever'),(249,1,4,'will'),(231,1,4,'within'),(241,1,4,'without');
/*!40000 ALTER TABLE `ps_search_word_cms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_shop`
--

DROP TABLE IF EXISTS `ps_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_shop` (
  `id_shop` int(11) NOT NULL AUTO_INCREMENT,
  `id_shop_group` int(11) NOT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `id_category` int(11) NOT NULL,
  `theme_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_shop`),
  KEY `IDX_CBDFBB9EF5C9E40` (`id_shop_group`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_shop`
--

LOCK TABLES `ps_shop` WRITE;
/*!40000 ALTER TABLE `ps_shop` DISABLE KEYS */;
INSERT INTO `ps_shop` VALUES (1,1,'My TekapoCart',2,'simplicity',1,0),(2,1,'shop 2',2,'simplicity',1,0),(3,1,'shop 3',2,'simplicity',1,0),(4,1,'shop 4',2,'simplicity',1,0);
/*!40000 ALTER TABLE `ps_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_shop_group`
--

DROP TABLE IF EXISTS `ps_shop_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_shop_group` (
  `id_shop_group` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `share_customer` tinyint(1) NOT NULL,
  `share_order` tinyint(1) NOT NULL,
  `share_stock` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_shop_group`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_shop_group`
--

LOCK TABLES `ps_shop_group` WRITE;
/*!40000 ALTER TABLE `ps_shop_group` DISABLE KEYS */;
INSERT INTO `ps_shop_group` VALUES (1,'Default',0,0,0,1,0);
/*!40000 ALTER TABLE `ps_shop_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_shop_url`
--

DROP TABLE IF EXISTS `ps_shop_url`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_shop_url` (
  `id_shop_url` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_shop` int(11) unsigned NOT NULL,
  `domain` varchar(150) NOT NULL,
  `domain_ssl` varchar(150) NOT NULL,
  `physical_uri` varchar(64) NOT NULL,
  `virtual_uri` varchar(64) NOT NULL,
  `main` tinyint(1) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_shop_url`),
  UNIQUE KEY `full_shop_url` (`domain`,`physical_uri`,`virtual_uri`),
  UNIQUE KEY `full_shop_url_ssl` (`domain_ssl`,`physical_uri`,`virtual_uri`),
  KEY `id_shop` (`id_shop`,`main`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_shop_url`
--

LOCK TABLES `ps_shop_url` WRITE;
/*!40000 ALTER TABLE `ps_shop_url` DISABLE KEYS */;
INSERT INTO `ps_shop_url` VALUES (1,1,'shop1.presta.shop','shop1.presta.shop','/','',1,1),(2,2,'shop2.presta.shop','shop2.presta.shop','/','',1,1),(3,3,'shop3.presta.shop','shop3.presta.shop','/','',1,1),(4,4,'shop4.presta.shop','shop4.presta.shop','/','',1,1);
/*!40000 ALTER TABLE `ps_shop_url` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_smarty_cache`
--

DROP TABLE IF EXISTS `ps_smarty_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_smarty_cache` (
  `id_smarty_cache` char(40) NOT NULL,
  `name` char(40) NOT NULL,
  `cache_id` varchar(254) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `content` longtext NOT NULL,
  PRIMARY KEY (`id_smarty_cache`),
  KEY `name` (`name`),
  KEY `cache_id` (`cache_id`),
  KEY `modified` (`modified`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_smarty_cache`
--

LOCK TABLES `ps_smarty_cache` WRITE;
/*!40000 ALTER TABLE `ps_smarty_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_smarty_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_smarty_last_flush`
--

DROP TABLE IF EXISTS `ps_smarty_last_flush`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_smarty_last_flush` (
  `type` enum('compile','template') NOT NULL,
  `last_flush` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_smarty_last_flush`
--

LOCK TABLES `ps_smarty_last_flush` WRITE;
/*!40000 ALTER TABLE `ps_smarty_last_flush` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_smarty_last_flush` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_smarty_lazy_cache`
--

DROP TABLE IF EXISTS `ps_smarty_lazy_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_smarty_lazy_cache` (
  `template_hash` varchar(32) NOT NULL DEFAULT '',
  `cache_id` varchar(255) NOT NULL DEFAULT '',
  `compile_id` varchar(32) NOT NULL DEFAULT '',
  `filepath` varchar(255) NOT NULL DEFAULT '',
  `last_update` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`template_hash`,`cache_id`,`compile_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_smarty_lazy_cache`
--

LOCK TABLES `ps_smarty_lazy_cache` WRITE;
/*!40000 ALTER TABLE `ps_smarty_lazy_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_smarty_lazy_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_smilepay_c2cup_table`
--

DROP TABLE IF EXISTS `ps_smilepay_c2cup_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_smilepay_c2cup_table` (
  `id_smilepay_c2cup` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_order` int(10) unsigned NOT NULL,
  `smse_id` text NOT NULL,
  `store_id` varchar(50) NOT NULL,
  `store_name` varchar(50) NOT NULL,
  `store_address` varchar(255) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `customer_phone` varchar(50) NOT NULL,
  `amount` decimal(20,6) NOT NULL,
  `c2cup_no` varchar(255) DEFAULT NULL,
  `btn_url` text NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_smilepay_c2cup`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_smilepay_c2cup_table`
--

LOCK TABLES `ps_smilepay_c2cup_table` WRITE;
/*!40000 ALTER TABLE `ps_smilepay_c2cup_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_smilepay_c2cup_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_smilepay_csv_table`
--

DROP TABLE IF EXISTS `ps_smilepay_csv_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_smilepay_csv_table` (
  `id_smilepay_csv` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_order` int(10) unsigned NOT NULL,
  `smse_id` text NOT NULL,
  `barcode1` varchar(50) NOT NULL,
  `barcode2` varchar(50) NOT NULL,
  `barcode3` varchar(50) NOT NULL,
  `dateline` varchar(255) NOT NULL,
  `amount` decimal(20,6) NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_smilepay_csv`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_smilepay_csv_table`
--

LOCK TABLES `ps_smilepay_csv_table` WRITE;
/*!40000 ALTER TABLE `ps_smilepay_csv_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_smilepay_csv_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_smilepay_ezcat_table`
--

DROP TABLE IF EXISTS `ps_smilepay_ezcat_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_smilepay_ezcat_table` (
  `id_order` int(10) unsigned NOT NULL,
  `smse_id` varchar(255) NOT NULL,
  `track_num` varchar(255) NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_smilepay_ezcat_table`
--

LOCK TABLES `ps_smilepay_ezcat_table` WRITE;
/*!40000 ALTER TABLE `ps_smilepay_ezcat_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_smilepay_ezcat_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_smilepay_ezcatup_table`
--

DROP TABLE IF EXISTS `ps_smilepay_ezcatup_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_smilepay_ezcatup_table` (
  `id_order` int(10) unsigned NOT NULL,
  `smse_id` varchar(255) NOT NULL,
  `track_num` varchar(255) NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_smilepay_ezcatup_table`
--

LOCK TABLES `ps_smilepay_ezcatup_table` WRITE;
/*!40000 ALTER TABLE `ps_smilepay_ezcatup_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_smilepay_ezcatup_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_smilepay_palmboxc2cup_table`
--

DROP TABLE IF EXISTS `ps_smilepay_palmboxc2cup_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_smilepay_palmboxc2cup_table` (
  `id_order` int(10) unsigned NOT NULL,
  `smse_id` varchar(255) DEFAULT NULL,
  `track_num` varchar(255) DEFAULT NULL,
  `checkcode` varchar(512) DEFAULT NULL,
  `storeid` varchar(255) NOT NULL,
  `storename` varchar(255) NOT NULL,
  `storeaddress` varchar(512) NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_smilepay_palmboxc2cup_table`
--

LOCK TABLES `ps_smilepay_palmboxc2cup_table` WRITE;
/*!40000 ALTER TABLE `ps_smilepay_palmboxc2cup_table` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_smilepay_palmboxc2cup_table` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_sociallogin`
--

DROP TABLE IF EXISTS `ps_sociallogin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_sociallogin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_social` varchar(20) NOT NULL,
  `type` varchar(10) NOT NULL,
  `id_customer` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_sociallogin`
--

LOCK TABLES `ps_sociallogin` WRITE;
/*!40000 ALTER TABLE `ps_sociallogin` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_sociallogin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_specific_price`
--

DROP TABLE IF EXISTS `ps_specific_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_specific_price` (
  `id_specific_price` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_specific_price_rule` int(11) unsigned NOT NULL,
  `id_cart` int(11) unsigned NOT NULL,
  `id_product` int(10) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_shop_group` int(11) unsigned NOT NULL,
  `id_currency` int(10) unsigned NOT NULL,
  `id_country` int(10) unsigned NOT NULL,
  `id_group` int(10) unsigned NOT NULL,
  `id_customer` int(10) unsigned NOT NULL,
  `id_product_attribute` int(10) unsigned NOT NULL,
  `price` decimal(20,6) NOT NULL,
  `from_quantity` mediumint(8) unsigned NOT NULL,
  `reduction` decimal(20,6) NOT NULL,
  `reduction_tax` tinyint(1) NOT NULL DEFAULT '1',
  `reduction_type` enum('amount','percentage') NOT NULL,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL,
  PRIMARY KEY (`id_specific_price`),
  UNIQUE KEY `id_product_2` (`id_product`,`id_product_attribute`,`id_customer`,`id_cart`,`from`,`to`,`id_shop`,`id_shop_group`,`id_currency`,`id_country`,`id_group`,`from_quantity`,`id_specific_price_rule`),
  KEY `id_product` (`id_product`,`id_shop`,`id_currency`,`id_country`,`id_group`,`id_customer`,`from_quantity`,`from`,`to`),
  KEY `from_quantity` (`from_quantity`),
  KEY `id_specific_price_rule` (`id_specific_price_rule`),
  KEY `id_cart` (`id_cart`),
  KEY `id_product_attribute` (`id_product_attribute`),
  KEY `id_shop` (`id_shop`),
  KEY `id_customer` (`id_customer`),
  KEY `from` (`from`),
  KEY `to` (`to`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_specific_price`
--

LOCK TABLES `ps_specific_price` WRITE;
/*!40000 ALTER TABLE `ps_specific_price` DISABLE KEYS */;
INSERT INTO `ps_specific_price` VALUES (18,0,0,1,1,0,0,0,0,0,0,-1.000000,1,0.100000,1,'percentage','0000-00-00 00:00:00','0000-00-00 00:00:00'),(19,0,0,14,1,0,0,0,0,0,0,-1.000000,1,0.100000,1,'percentage','0000-00-00 00:00:00','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `ps_specific_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_specific_price_priority`
--

DROP TABLE IF EXISTS `ps_specific_price_priority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_specific_price_priority` (
  `id_specific_price_priority` int(11) NOT NULL AUTO_INCREMENT,
  `id_product` int(11) NOT NULL,
  `priority` varchar(80) NOT NULL,
  PRIMARY KEY (`id_specific_price_priority`,`id_product`),
  UNIQUE KEY `id_product` (`id_product`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_specific_price_priority`
--

LOCK TABLES `ps_specific_price_priority` WRITE;
/*!40000 ALTER TABLE `ps_specific_price_priority` DISABLE KEYS */;
INSERT INTO `ps_specific_price_priority` VALUES (1,6,'id_shop;id_currency;id_country;id_group'),(7,8,'id_shop;id_currency;id_country;id_group'),(29,11,'id_shop;id_currency;id_country;id_group'),(35,13,'id_shop;id_currency;id_country;id_group'),(37,14,'id_shop;id_currency;id_country;id_group'),(38,15,'id_shop;id_currency;id_country;id_group'),(39,16,'id_shop;id_currency;id_country;id_group'),(40,17,'id_shop;id_currency;id_country;id_group'),(41,18,'id_shop;id_currency;id_country;id_group'),(43,19,'id_shop;id_currency;id_country;id_group'),(44,20,'id_shop;id_currency;id_country;id_group'),(45,21,'id_shop;id_currency;id_country;id_group'),(52,1,'id_shop;id_currency;id_country;id_group'),(60,2,'id_shop;id_currency;id_country;id_group'),(76,3,'id_shop;id_currency;id_country;id_group'),(82,4,'id_shop;id_currency;id_country;id_group'),(91,5,'id_shop;id_currency;id_country;id_group'),(94,7,'id_shop;id_currency;id_country;id_group'),(96,9,'id_shop;id_currency;id_country;id_group'),(97,10,'id_shop;id_currency;id_country;id_group'),(99,12,'id_shop;id_currency;id_country;id_group');
/*!40000 ALTER TABLE `ps_specific_price_priority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_specific_price_rule`
--

DROP TABLE IF EXISTS `ps_specific_price_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_specific_price_rule` (
  `id_specific_price_rule` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `id_shop` int(11) unsigned NOT NULL DEFAULT '1',
  `id_currency` int(10) unsigned NOT NULL,
  `id_country` int(10) unsigned NOT NULL,
  `id_group` int(10) unsigned NOT NULL,
  `from_quantity` mediumint(8) unsigned NOT NULL,
  `price` decimal(20,6) DEFAULT NULL,
  `reduction` decimal(20,6) NOT NULL,
  `reduction_tax` tinyint(1) NOT NULL DEFAULT '1',
  `reduction_type` enum('amount','percentage') NOT NULL,
  `from` datetime NOT NULL,
  `to` datetime NOT NULL,
  PRIMARY KEY (`id_specific_price_rule`),
  KEY `id_product` (`id_shop`,`id_currency`,`id_country`,`id_group`,`from_quantity`,`from`,`to`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_specific_price_rule`
--

LOCK TABLES `ps_specific_price_rule` WRITE;
/*!40000 ALTER TABLE `ps_specific_price_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_specific_price_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_specific_price_rule_condition`
--

DROP TABLE IF EXISTS `ps_specific_price_rule_condition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_specific_price_rule_condition` (
  `id_specific_price_rule_condition` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_specific_price_rule_condition_group` int(11) unsigned NOT NULL,
  `type` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id_specific_price_rule_condition`),
  KEY `id_specific_price_rule_condition_group` (`id_specific_price_rule_condition_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_specific_price_rule_condition`
--

LOCK TABLES `ps_specific_price_rule_condition` WRITE;
/*!40000 ALTER TABLE `ps_specific_price_rule_condition` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_specific_price_rule_condition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_specific_price_rule_condition_group`
--

DROP TABLE IF EXISTS `ps_specific_price_rule_condition_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_specific_price_rule_condition_group` (
  `id_specific_price_rule_condition_group` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_specific_price_rule` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_specific_price_rule_condition_group`,`id_specific_price_rule`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_specific_price_rule_condition_group`
--

LOCK TABLES `ps_specific_price_rule_condition_group` WRITE;
/*!40000 ALTER TABLE `ps_specific_price_rule_condition_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_specific_price_rule_condition_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_state`
--

DROP TABLE IF EXISTS `ps_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_state` (
  `id_state` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_country` int(11) unsigned NOT NULL,
  `id_zone` int(11) unsigned NOT NULL,
  `name` varchar(64) NOT NULL,
  `iso_code` varchar(7) NOT NULL,
  `tax_behavior` smallint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_state`),
  KEY `id_country` (`id_country`),
  KEY `name` (`name`),
  KEY `id_zone` (`id_zone`)
) ENGINE=InnoDB AUTO_INCREMENT=372 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_state`
--

LOCK TABLES `ps_state` WRITE;
/*!40000 ALTER TABLE `ps_state` DISABLE KEYS */;
INSERT INTO `ps_state` VALUES (1,21,2,'AA','AA',0,1),(2,21,2,'AE','AE',0,1),(3,21,2,'AP','AP',0,1),(4,21,2,'Alabama','AL',0,1),(5,21,2,'Alaska','AK',0,1),(6,21,2,'Arizona','AZ',0,1),(7,21,2,'Arkansas','AR',0,1),(8,21,2,'California','CA',0,1),(9,21,2,'Colorado','CO',0,1),(10,21,2,'Connecticut','CT',0,1),(11,21,2,'Delaware','DE',0,1),(12,21,2,'Florida','FL',0,1),(13,21,2,'Georgia','GA',0,1),(14,21,2,'Hawaii','HI',0,1),(15,21,2,'Idaho','ID',0,1),(16,21,2,'Illinois','IL',0,1),(17,21,2,'Indiana','IN',0,1),(18,21,2,'Iowa','IA',0,1),(19,21,2,'Kansas','KS',0,1),(20,21,2,'Kentucky','KY',0,1),(21,21,2,'Louisiana','LA',0,1),(22,21,2,'Maine','ME',0,1),(23,21,2,'Maryland','MD',0,1),(24,21,2,'Massachusetts','MA',0,1),(25,21,2,'Michigan','MI',0,1),(26,21,2,'Minnesota','MN',0,1),(27,21,2,'Mississippi','MS',0,1),(28,21,2,'Missouri','MO',0,1),(29,21,2,'Montana','MT',0,1),(30,21,2,'Nebraska','NE',0,1),(31,21,2,'Nevada','NV',0,1),(32,21,2,'New Hampshire','NH',0,1),(33,21,2,'New Jersey','NJ',0,1),(34,21,2,'New Mexico','NM',0,1),(35,21,2,'New York','NY',0,1),(36,21,2,'North Carolina','NC',0,1),(37,21,2,'North Dakota','ND',0,1),(38,21,2,'Ohio','OH',0,1),(39,21,2,'Oklahoma','OK',0,1),(40,21,2,'Oregon','OR',0,1),(41,21,2,'Pennsylvania','PA',0,1),(42,21,2,'Rhode Island','RI',0,1),(43,21,2,'South Carolina','SC',0,1),(44,21,2,'South Dakota','SD',0,1),(45,21,2,'Tennessee','TN',0,1),(46,21,2,'Texas','TX',0,1),(47,21,2,'Utah','UT',0,1),(48,21,2,'Vermont','VT',0,1),(49,21,2,'Virginia','VA',0,1),(50,21,2,'Washington','WA',0,1),(51,21,2,'West Virginia','WV',0,1),(52,21,2,'Wisconsin','WI',0,1),(53,21,2,'Wyoming','WY',0,1),(54,21,2,'Puerto Rico','PR',0,1),(55,21,2,'US Virgin Islands','VI',0,1),(56,21,2,'District of Columbia','DC',0,1),(57,145,2,'Aguascalientes','AGS',0,1),(58,145,2,'Baja California','BCN',0,1),(59,145,2,'Baja California Sur','BCS',0,1),(60,145,2,'Campeche','CAM',0,1),(61,145,2,'Chiapas','CHP',0,1),(62,145,2,'Chihuahua','CHH',0,1),(63,145,2,'Coahuila','COA',0,1),(64,145,2,'Colima','COL',0,1),(65,145,2,'Distrito Federal','DIF',0,1),(66,145,2,'Durango','DUR',0,1),(67,145,2,'Guanajuato','GUA',0,1),(68,145,2,'Guerrero','GRO',0,1),(69,145,2,'Hidalgo','HID',0,1),(70,145,2,'Jalisco','JAL',0,1),(71,145,2,'Estado de México','MEX',0,1),(72,145,2,'Michoacán','MIC',0,1),(73,145,2,'Morelos','MOR',0,1),(74,145,2,'Nayarit','NAY',0,1),(75,145,2,'Nuevo León','NLE',0,1),(76,145,2,'Oaxaca','OAX',0,1),(77,145,2,'Puebla','PUE',0,1),(78,145,2,'Querétaro','QUE',0,1),(79,145,2,'Quintana Roo','ROO',0,1),(80,145,2,'San Luis Potosí','SLP',0,1),(81,145,2,'Sinaloa','SIN',0,1),(82,145,2,'Sonora','SON',0,1),(83,145,2,'Tabasco','TAB',0,1),(84,145,2,'Tamaulipas','TAM',0,1),(85,145,2,'Tlaxcala','TLA',0,1),(86,145,2,'Veracruz','VER',0,1),(87,145,2,'Yucatán','YUC',0,1),(88,145,2,'Zacatecas','ZAC',0,1),(89,4,2,'Ontario','ON',0,1),(90,4,2,'Quebec','QC',0,1),(91,4,2,'British Columbia','BC',0,1),(92,4,2,'Alberta','AB',0,1),(93,4,2,'Manitoba','MB',0,1),(94,4,2,'Saskatchewan','SK',0,1),(95,4,2,'Nova Scotia','NS',0,1),(96,4,2,'New Brunswick','NB',0,1),(97,4,2,'Newfoundland and Labrador','NL',0,1),(98,4,2,'Prince Edward Island','PE',0,1),(99,4,2,'Northwest Territories','NT',0,1),(100,4,2,'Yukon','YT',0,1),(101,4,2,'Nunavut','NU',0,1),(102,44,6,'Buenos Aires','B',0,1),(103,44,6,'Catamarca','K',0,1),(104,44,6,'Chaco','H',0,1),(105,44,6,'Chubut','U',0,1),(106,44,6,'Ciudad de Buenos Aires','C',0,1),(107,44,6,'Córdoba','X',0,1),(108,44,6,'Corrientes','W',0,1),(109,44,6,'Entre Ríos','E',0,1),(110,44,6,'Formosa','P',0,1),(111,44,6,'Jujuy','Y',0,1),(112,44,6,'La Pampa','L',0,1),(113,44,6,'La Rioja','F',0,1),(114,44,6,'Mendoza','M',0,1),(115,44,6,'Misiones','N',0,1),(116,44,6,'Neuquén','Q',0,1),(117,44,6,'Río Negro','R',0,1),(118,44,6,'Salta','A',0,1),(119,44,6,'San Juan','J',0,1),(120,44,6,'San Luis','D',0,1),(121,44,6,'Santa Cruz','Z',0,1),(122,44,6,'Santa Fe','S',0,1),(123,44,6,'Santiago del Estero','G',0,1),(124,44,6,'Tierra del Fuego','V',0,1),(125,44,6,'Tucumán','T',0,1),(126,10,1,'Agrigento','AG',0,1),(127,10,1,'Alessandria','AL',0,1),(128,10,1,'Ancona','AN',0,1),(129,10,1,'Aosta','AO',0,1),(130,10,1,'Arezzo','AR',0,1),(131,10,1,'Ascoli Piceno','AP',0,1),(132,10,1,'Asti','AT',0,1),(133,10,1,'Avellino','AV',0,1),(134,10,1,'Bari','BA',0,1),(135,10,1,'Barletta-Andria-Trani','BT',0,1),(136,10,1,'Belluno','BL',0,1),(137,10,1,'Benevento','BN',0,1),(138,10,1,'Bergamo','BG',0,1),(139,10,1,'Biella','BI',0,1),(140,10,1,'Bologna','BO',0,1),(141,10,1,'Bolzano','BZ',0,1),(142,10,1,'Brescia','BS',0,1),(143,10,1,'Brindisi','BR',0,1),(144,10,1,'Cagliari','CA',0,1),(145,10,1,'Caltanissetta','CL',0,1),(146,10,1,'Campobasso','CB',0,1),(147,10,1,'Carbonia-Iglesias','CI',0,1),(148,10,1,'Caserta','CE',0,1),(149,10,1,'Catania','CT',0,1),(150,10,1,'Catanzaro','CZ',0,1),(151,10,1,'Chieti','CH',0,1),(152,10,1,'Como','CO',0,1),(153,10,1,'Cosenza','CS',0,1),(154,10,1,'Cremona','CR',0,1),(155,10,1,'Crotone','KR',0,1),(156,10,1,'Cuneo','CN',0,1),(157,10,1,'Enna','EN',0,1),(158,10,1,'Fermo','FM',0,1),(159,10,1,'Ferrara','FE',0,1),(160,10,1,'Firenze','FI',0,1),(161,10,1,'Foggia','FG',0,1),(162,10,1,'Forlì-Cesena','FC',0,1),(163,10,1,'Frosinone','FR',0,1),(164,10,1,'Genova','GE',0,1),(165,10,1,'Gorizia','GO',0,1),(166,10,1,'Grosseto','GR',0,1),(167,10,1,'Imperia','IM',0,1),(168,10,1,'Isernia','IS',0,1),(169,10,1,'L\'Aquila','AQ',0,1),(170,10,1,'La Spezia','SP',0,1),(171,10,1,'Latina','LT',0,1),(172,10,1,'Lecce','LE',0,1),(173,10,1,'Lecco','LC',0,1),(174,10,1,'Livorno','LI',0,1),(175,10,1,'Lodi','LO',0,1),(176,10,1,'Lucca','LU',0,1),(177,10,1,'Macerata','MC',0,1),(178,10,1,'Mantova','MN',0,1),(179,10,1,'Massa','MS',0,1),(180,10,1,'Matera','MT',0,1),(181,10,1,'Medio Campidano','VS',0,1),(182,10,1,'Messina','ME',0,1),(183,10,1,'Milano','MI',0,1),(184,10,1,'Modena','MO',0,1),(185,10,1,'Monza e della Brianza','MB',0,1),(186,10,1,'Napoli','NA',0,1),(187,10,1,'Novara','NO',0,1),(188,10,1,'Nuoro','NU',0,1),(189,10,1,'Ogliastra','OG',0,1),(190,10,1,'Olbia-Tempio','OT',0,1),(191,10,1,'Oristano','OR',0,1),(192,10,1,'Padova','PD',0,1),(193,10,1,'Palermo','PA',0,1),(194,10,1,'Parma','PR',0,1),(195,10,1,'Pavia','PV',0,1),(196,10,1,'Perugia','PG',0,1),(197,10,1,'Pesaro-Urbino','PU',0,1),(198,10,1,'Pescara','PE',0,1),(199,10,1,'Piacenza','PC',0,1),(200,10,1,'Pisa','PI',0,1),(201,10,1,'Pistoia','PT',0,1),(202,10,1,'Pordenone','PN',0,1),(203,10,1,'Potenza','PZ',0,1),(204,10,1,'Prato','PO',0,1),(205,10,1,'Ragusa','RG',0,1),(206,10,1,'Ravenna','RA',0,1),(207,10,1,'Reggio Calabria','RC',0,1),(208,10,1,'Reggio Emilia','RE',0,1),(209,10,1,'Rieti','RI',0,1),(210,10,1,'Rimini','RN',0,1),(211,10,1,'Roma','RM',0,1),(212,10,1,'Rovigo','RO',0,1),(213,10,1,'Salerno','SA',0,1),(214,10,1,'Sassari','SS',0,1),(215,10,1,'Savona','SV',0,1),(216,10,1,'Siena','SI',0,1),(217,10,1,'Siracusa','SR',0,1),(218,10,1,'Sondrio','SO',0,1),(219,10,1,'Taranto','TA',0,1),(220,10,1,'Teramo','TE',0,1),(221,10,1,'Terni','TR',0,1),(222,10,1,'Torino','TO',0,1),(223,10,1,'Trapani','TP',0,1),(224,10,1,'Trento','TN',0,1),(225,10,1,'Treviso','TV',0,1),(226,10,1,'Trieste','TS',0,1),(227,10,1,'Udine','UD',0,1),(228,10,1,'Varese','VA',0,1),(229,10,1,'Venezia','VE',0,1),(230,10,1,'Verbano-Cusio-Ossola','VB',0,1),(231,10,1,'Vercelli','VC',0,1),(232,10,1,'Verona','VR',0,1),(233,10,1,'Vibo Valentia','VV',0,1),(234,10,1,'Vicenza','VI',0,1),(235,10,1,'Viterbo','VT',0,1),(236,111,3,'Aceh','ID-AC',0,1),(237,111,3,'Bali','ID-BA',0,1),(238,111,3,'Banten','ID-BT',0,1),(239,111,3,'Bengkulu','ID-BE',0,1),(240,111,3,'Gorontalo','ID-GO',0,1),(241,111,3,'Jakarta','ID-JK',0,1),(242,111,3,'Jambi','ID-JA',0,1),(243,111,3,'Jawa Barat','ID-JB',0,1),(244,111,3,'Jawa Tengah','ID-JT',0,1),(245,111,3,'Jawa Timur','ID-JI',0,1),(246,111,3,'Kalimantan Barat','ID-KB',0,1),(247,111,3,'Kalimantan Selatan','ID-KS',0,1),(248,111,3,'Kalimantan Tengah','ID-KT',0,1),(249,111,3,'Kalimantan Timur','ID-KI',0,1),(250,111,3,'Kalimantan Utara','ID-KU',0,1),(251,111,3,'Kepulauan Bangka Belitug','ID-BB',0,1),(252,111,3,'Kepulauan Riau','ID-KR',0,1),(253,111,3,'Lampung','ID-LA',0,1),(254,111,3,'Maluku','ID-MA',0,1),(255,111,3,'Maluku Utara','ID-MU',0,1),(256,111,3,'Nusa Tengara Barat','ID-NB',0,1),(257,111,3,'Nusa Tenggara Timur','ID-NT',0,1),(258,111,3,'Papua','ID-PA',0,1),(259,111,3,'Papua Barat','ID-PB',0,1),(260,111,3,'Riau','ID-RI',0,1),(261,111,3,'Sulawesi Barat','ID-SR',0,1),(262,111,3,'Sulawesi Selatan','ID-SN',0,1),(263,111,3,'Sulawesi Tengah','ID-ST',0,1),(264,111,3,'Sulawesi Tenggara','ID-SG',0,1),(265,111,3,'Sulawesi Utara','ID-SA',0,1),(266,111,3,'Sumatera Barat','ID-SB',0,1),(267,111,3,'Sumatera Selatan','ID-SS',0,1),(268,111,3,'Sumatera Utara','ID-SU',0,1),(269,111,3,'Yogyakarta','ID-YO',0,1),(270,11,3,'Aichi','23',0,1),(271,11,3,'Akita','05',0,1),(272,11,3,'Aomori','02',0,1),(273,11,3,'Chiba','12',0,1),(274,11,3,'Ehime','38',0,1),(275,11,3,'Fukui','18',0,1),(276,11,3,'Fukuoka','40',0,1),(277,11,3,'Fukushima','07',0,1),(278,11,3,'Gifu','21',0,1),(279,11,3,'Gunma','10',0,1),(280,11,3,'Hiroshima','34',0,1),(281,11,3,'Hokkaido','01',0,1),(282,11,3,'Hyogo','28',0,1),(283,11,3,'Ibaraki','08',0,1),(284,11,3,'Ishikawa','17',0,1),(285,11,3,'Iwate','03',0,1),(286,11,3,'Kagawa','37',0,1),(287,11,3,'Kagoshima','46',0,1),(288,11,3,'Kanagawa','14',0,1),(289,11,3,'Kochi','39',0,1),(290,11,3,'Kumamoto','43',0,1),(291,11,3,'Kyoto','26',0,1),(292,11,3,'Mie','24',0,1),(293,11,3,'Miyagi','04',0,1),(294,11,3,'Miyazaki','45',0,1),(295,11,3,'Nagano','20',0,1),(296,11,3,'Nagasaki','42',0,1),(297,11,3,'Nara','29',0,1),(298,11,3,'Niigata','15',0,1),(299,11,3,'Oita','44',0,1),(300,11,3,'Okayama','33',0,1),(301,11,3,'Okinawa','47',0,1),(302,11,3,'Osaka','27',0,1),(303,11,3,'Saga','41',0,1),(304,11,3,'Saitama','11',0,1),(305,11,3,'Shiga','25',0,1),(306,11,3,'Shimane','32',0,1),(307,11,3,'Shizuoka','22',0,1),(308,11,3,'Tochigi','09',0,1),(309,11,3,'Tokushima','36',0,1),(310,11,3,'Tokyo','13',0,1),(311,11,3,'Tottori','31',0,1),(312,11,3,'Toyama','16',0,1),(313,11,3,'Wakayama','30',0,1),(314,11,3,'Yamagata','06',0,1),(315,11,3,'Yamaguchi','35',0,1),(316,11,3,'Yamanashi','19',0,1),(317,24,5,'Australian Capital Territory','ACT',0,1),(318,24,5,'New South Wales','NSW',0,1),(319,24,5,'Northern Territory','NT',0,1),(320,24,5,'Queensland','QLD',0,1),(321,24,5,'South Australia','SA',0,1),(322,24,5,'Tasmania','TAS',0,1),(323,24,5,'Victoria','VIC',0,1),(324,24,5,'Western Australia','WA',0,1),(325,11,3,'Aichi','JP-23',0,1),(326,11,3,'Akita','JP-05',0,1),(327,11,3,'Aomori','JP-02',0,1),(328,11,3,'Chiba','JP-12',0,1),(329,11,3,'Ehime','JP-38',0,1),(330,11,3,'Fukui','JP-18',0,1),(331,11,3,'Fukuoka','JP-40',0,1),(332,11,3,'Fukushima','JP-07',0,1),(333,11,3,'Gifu','JP-21',0,1),(334,11,3,'Gunma','JP-10',0,1),(335,11,3,'Hiroshima','JP-34',0,1),(336,11,3,'Hokkaido','JP-01',0,1),(337,11,3,'Hyōgo','JP-28',0,1),(338,11,3,'Ibaraki','JP-08',0,1),(339,11,3,'Ishikawa','JP-17',0,1),(340,11,3,'Iwate','JP-03',0,1),(341,11,3,'Kagawa','JP-37',0,1),(342,11,3,'Kagoshima','JP-46',0,1),(343,11,3,'Kanagawa','JP-14',0,1),(344,11,3,'Kōchi','JP-39',0,1),(345,11,3,'Kumamoto','JP-43',0,1),(346,11,3,'Kyoto','JP-26',0,1),(347,11,3,'Mie','JP-24',0,1),(348,11,3,'Miyagi','JP-04',0,1),(349,11,3,'Miyazaki','JP-45',0,1),(350,11,3,'Nagano','JP-20',0,1),(351,11,3,'Nagasaki','JP-42',0,1),(352,11,3,'Nara','JP-29',0,1),(353,11,3,'Niigata','JP-15',0,1),(354,11,3,'Ōita','JP-44',0,1),(355,11,3,'Okayama','JP-33',0,1),(356,11,3,'Okinawa','JP-47',0,1),(357,11,3,'Osaka','JP-27',0,1),(358,11,3,'Saga','JP-41',0,1),(359,11,3,'Saitama','JP-11',0,1),(360,11,3,'Shiga','JP-25',0,1),(361,11,3,'Shimane','JP-32',0,1),(362,11,3,'Shizuoka','JP-22',0,1),(363,11,3,'Tochigi','JP-09',0,1),(364,11,3,'Tokushima','JP-36',0,1),(365,11,3,'Tokyo','JP-13',0,1),(366,11,3,'Tottori','JP-31',0,1),(367,11,3,'Toyama','JP-16',0,1),(368,11,3,'Wakayama','JP-30',0,1),(369,11,3,'Yamagata','JP-06',0,1),(370,11,3,'Yamaguchi','JP-35',0,1),(371,11,3,'Yamanashi','JP-19',0,1);
/*!40000 ALTER TABLE `ps_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_stock`
--

DROP TABLE IF EXISTS `ps_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_stock` (
  `id_stock` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_warehouse` int(11) unsigned NOT NULL,
  `id_product` int(11) unsigned NOT NULL,
  `id_product_attribute` int(11) unsigned NOT NULL,
  `reference` varchar(32) NOT NULL,
  `ean13` varchar(13) DEFAULT NULL,
  `isbn` varchar(32) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `physical_quantity` int(11) unsigned NOT NULL,
  `usable_quantity` int(11) unsigned NOT NULL,
  `price_te` decimal(20,6) DEFAULT '0.000000',
  PRIMARY KEY (`id_stock`),
  KEY `id_warehouse` (`id_warehouse`),
  KEY `id_product` (`id_product`),
  KEY `id_product_attribute` (`id_product_attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_stock`
--

LOCK TABLES `ps_stock` WRITE;
/*!40000 ALTER TABLE `ps_stock` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_stock_available`
--

DROP TABLE IF EXISTS `ps_stock_available`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_stock_available` (
  `id_stock_available` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_product` int(11) unsigned NOT NULL,
  `id_product_attribute` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  `id_shop_group` int(11) unsigned NOT NULL,
  `quantity` int(10) NOT NULL DEFAULT '0',
  `physical_quantity` int(11) NOT NULL DEFAULT '0',
  `reserved_quantity` int(11) NOT NULL DEFAULT '0',
  `depends_on_stock` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `out_of_stock` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_stock_available`),
  UNIQUE KEY `product_sqlstock` (`id_product`,`id_product_attribute`,`id_shop`,`id_shop_group`),
  KEY `id_shop` (`id_shop`),
  KEY `id_shop_group` (`id_shop_group`),
  KEY `id_product` (`id_product`),
  KEY `id_product_attribute` (`id_product_attribute`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_stock_available`
--

LOCK TABLES `ps_stock_available` WRITE;
/*!40000 ALTER TABLE `ps_stock_available` DISABLE KEYS */;
INSERT INTO `ps_stock_available` VALUES (1,1,0,1,0,1000,1000,0,0,2),(2,2,0,1,0,59,59,0,0,2),(3,3,0,1,0,1000,1000,0,0,2),(4,4,0,1,0,998,998,0,0,2),(5,5,0,1,0,0,0,0,0,2),(6,6,0,1,0,997,997,0,0,2),(8,8,0,1,0,994,994,0,0,2),(9,9,0,1,0,974,988,14,0,2),(10,10,0,1,0,960,966,6,0,2),(11,11,0,1,0,976,977,1,0,2),(12,12,0,1,0,-8,-8,0,0,1),(13,13,0,1,0,499,499,0,0,2),(14,14,0,1,0,1,0,0,0,2),(15,15,0,1,0,360,600,0,0,2),(17,13,1,1,0,100,100,0,0,2),(18,13,2,1,0,100,100,0,0,2),(19,13,3,1,0,99,100,1,0,2),(20,13,4,1,0,100,100,0,0,2),(21,13,5,1,0,100,100,0,0,2),(22,2,6,1,0,9,9,0,0,2),(23,2,7,1,0,20,20,0,0,2),(24,2,8,1,0,30,30,0,0,2),(25,15,9,1,0,0,150,0,0,2),(26,15,10,1,0,110,150,0,0,2),(27,15,11,1,0,120,150,0,0,2),(28,15,12,1,0,130,150,0,0,2),(29,16,0,1,0,0,0,0,0,2);
/*!40000 ALTER TABLE `ps_stock_available` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_stock_mvt`
--

DROP TABLE IF EXISTS `ps_stock_mvt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_stock_mvt` (
  `id_stock_mvt` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_stock` int(11) NOT NULL,
  `id_order` int(11) DEFAULT NULL,
  `id_supply_order` int(11) DEFAULT NULL,
  `id_stock_mvt_reason` int(11) NOT NULL,
  `id_employee` int(11) NOT NULL,
  `employee_lastname` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `employee_firstname` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `physical_quantity` int(11) NOT NULL,
  `date_add` datetime NOT NULL,
  `sign` smallint(6) NOT NULL DEFAULT '1',
  `price_te` decimal(20,6) DEFAULT '0.000000',
  `last_wa` decimal(20,6) DEFAULT '0.000000',
  `current_wa` decimal(20,6) DEFAULT '0.000000',
  `referer` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_stock_mvt`),
  KEY `id_stock` (`id_stock`),
  KEY `id_stock_mvt_reason` (`id_stock_mvt_reason`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_stock_mvt`
--

LOCK TABLES `ps_stock_mvt` WRITE;
/*!40000 ALTER TABLE `ps_stock_mvt` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_stock_mvt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_stock_mvt_reason`
--

DROP TABLE IF EXISTS `ps_stock_mvt_reason`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_stock_mvt_reason` (
  `id_stock_mvt_reason` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sign` tinyint(1) NOT NULL DEFAULT '1',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_stock_mvt_reason`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_stock_mvt_reason`
--

LOCK TABLES `ps_stock_mvt_reason` WRITE;
/*!40000 ALTER TABLE `ps_stock_mvt_reason` DISABLE KEYS */;
INSERT INTO `ps_stock_mvt_reason` VALUES (1,1,'2018-07-11 11:57:36','2018-07-11 11:57:36',0),(2,-1,'2018-07-11 11:57:36','2018-07-11 11:57:36',0),(3,-1,'2018-07-11 11:57:36','2018-07-11 11:57:36',0),(4,-1,'2018-07-11 11:57:36','2018-07-11 11:57:36',0),(5,1,'2018-07-11 11:57:36','2018-07-11 11:57:36',0),(6,-1,'2018-07-11 11:57:36','2018-07-11 11:57:36',0),(7,1,'2018-07-11 11:57:36','2018-07-11 11:57:36',0),(8,1,'2018-07-11 11:57:36','2018-07-11 11:57:36',0),(9,1,'2018-07-11 11:57:36','2018-07-11 11:57:36',0),(10,1,'2018-07-11 11:57:36','2018-07-11 11:57:36',0),(11,1,'2018-07-11 11:57:36','2018-07-11 11:57:36',0),(12,-1,'2018-07-11 11:57:36','2018-07-11 11:57:36',0);
/*!40000 ALTER TABLE `ps_stock_mvt_reason` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_stock_mvt_reason_lang`
--

DROP TABLE IF EXISTS `ps_stock_mvt_reason_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_stock_mvt_reason_lang` (
  `id_stock_mvt_reason` int(11) unsigned NOT NULL,
  `id_lang` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id_stock_mvt_reason`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_stock_mvt_reason_lang`
--

LOCK TABLES `ps_stock_mvt_reason_lang` WRITE;
/*!40000 ALTER TABLE `ps_stock_mvt_reason_lang` DISABLE KEYS */;
INSERT INTO `ps_stock_mvt_reason_lang` VALUES (1,1,'漲價'),(1,2,'涨价'),(1,3,'涨价'),(1,4,'涨价'),(2,1,'減少'),(2,2,'下降'),(2,3,'下降'),(2,4,'下降'),(3,1,'客戶訂單'),(3,2,'Customer Order'),(3,3,'Customer Order'),(3,4,'顧客注文'),(4,1,'Regulation following an inventory of stock'),(4,2,'Regulation following an inventory of stock'),(4,3,'Regulation following an inventory of stock'),(4,4,'Regulation following an inventory of stock'),(5,1,'Regulation following an inventory of stock'),(5,2,'Regulation following an inventory of stock'),(5,3,'Regulation following an inventory of stock'),(5,4,'Regulation following an inventory of stock'),(6,1,'轉移到另一個倉庫'),(6,2,'Transfer to another warehouse'),(6,3,'Transfer to another warehouse'),(6,4,'別の倉庫に移動'),(7,1,'由另一個倉庫轉移到此'),(7,2,'Transfer from another warehouse'),(7,3,'Transfer from another warehouse'),(7,4,'別の倉庫から移動'),(8,1,'採購單'),(8,2,'Supply Order'),(8,3,'Supply Order'),(8,4,'発注'),(9,1,'客戶訂單'),(9,2,'Customer Order'),(9,3,'Customer Order'),(9,4,'顧客注文'),(10,1,'退回商品'),(10,2,'Product return'),(10,3,'Product Return'),(10,4,'商品の返品'),(11,1,'Employee Edition'),(11,2,'Employee Edition'),(11,3,'Employee Edition'),(11,4,'Employee Edition'),(12,1,'Employee Edition'),(12,2,'Employee Edition'),(12,3,'Employee Edition'),(12,4,'Employee Edition');
/*!40000 ALTER TABLE `ps_stock_mvt_reason_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_store`
--

DROP TABLE IF EXISTS `ps_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_store` (
  `id_store` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_country` int(10) unsigned NOT NULL,
  `id_state` int(10) unsigned DEFAULT NULL,
  `city` varchar(64) NOT NULL,
  `postcode` varchar(12) NOT NULL,
  `latitude` decimal(13,8) DEFAULT NULL,
  `longitude` decimal(13,8) DEFAULT NULL,
  `phone` varchar(16) DEFAULT NULL,
  `fax` varchar(16) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_store`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_store`
--

LOCK TABLES `ps_store` WRITE;
/*!40000 ALTER TABLE `ps_store` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_store_lang`
--

DROP TABLE IF EXISTS `ps_store_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_store_lang` (
  `id_store` int(11) unsigned NOT NULL,
  `id_lang` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `address1` varchar(255) NOT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `hours` text,
  `note` text,
  PRIMARY KEY (`id_store`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_store_lang`
--

LOCK TABLES `ps_store_lang` WRITE;
/*!40000 ALTER TABLE `ps_store_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_store_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_store_shop`
--

DROP TABLE IF EXISTS `ps_store_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_store_shop` (
  `id_store` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_store`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_store_shop`
--

LOCK TABLES `ps_store_shop` WRITE;
/*!40000 ALTER TABLE `ps_store_shop` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_store_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_supplier`
--

DROP TABLE IF EXISTS `ps_supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_supplier` (
  `id_supplier` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_supplier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_supplier`
--

LOCK TABLES `ps_supplier` WRITE;
/*!40000 ALTER TABLE `ps_supplier` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_supplier_lang`
--

DROP TABLE IF EXISTS `ps_supplier_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_supplier_lang` (
  `id_supplier` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `description` text,
  `meta_title` varchar(128) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_supplier`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_supplier_lang`
--

LOCK TABLES `ps_supplier_lang` WRITE;
/*!40000 ALTER TABLE `ps_supplier_lang` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_supplier_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_supplier_shop`
--

DROP TABLE IF EXISTS `ps_supplier_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_supplier_shop` (
  `id_supplier` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_supplier`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_supplier_shop`
--

LOCK TABLES `ps_supplier_shop` WRITE;
/*!40000 ALTER TABLE `ps_supplier_shop` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_supplier_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_supply_order`
--

DROP TABLE IF EXISTS `ps_supply_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_supply_order` (
  `id_supply_order` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_supplier` int(11) unsigned NOT NULL,
  `supplier_name` varchar(64) NOT NULL,
  `id_lang` int(11) unsigned NOT NULL,
  `id_warehouse` int(11) unsigned NOT NULL,
  `id_supply_order_state` int(11) unsigned NOT NULL,
  `id_currency` int(11) unsigned NOT NULL,
  `id_ref_currency` int(11) unsigned NOT NULL,
  `reference` varchar(64) NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  `date_delivery_expected` datetime DEFAULT NULL,
  `total_te` decimal(20,6) DEFAULT '0.000000',
  `total_with_discount_te` decimal(20,6) DEFAULT '0.000000',
  `total_tax` decimal(20,6) DEFAULT '0.000000',
  `total_ti` decimal(20,6) DEFAULT '0.000000',
  `discount_rate` decimal(20,6) DEFAULT '0.000000',
  `discount_value_te` decimal(20,6) DEFAULT '0.000000',
  `is_template` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id_supply_order`),
  KEY `id_supplier` (`id_supplier`),
  KEY `id_warehouse` (`id_warehouse`),
  KEY `reference` (`reference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_supply_order`
--

LOCK TABLES `ps_supply_order` WRITE;
/*!40000 ALTER TABLE `ps_supply_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_supply_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_supply_order_detail`
--

DROP TABLE IF EXISTS `ps_supply_order_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_supply_order_detail` (
  `id_supply_order_detail` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_supply_order` int(11) unsigned NOT NULL,
  `id_currency` int(11) unsigned NOT NULL,
  `id_product` int(11) unsigned NOT NULL,
  `id_product_attribute` int(11) unsigned NOT NULL,
  `reference` varchar(32) NOT NULL,
  `supplier_reference` varchar(32) NOT NULL,
  `name` varchar(128) NOT NULL,
  `ean13` varchar(13) DEFAULT NULL,
  `isbn` varchar(32) DEFAULT NULL,
  `upc` varchar(12) DEFAULT NULL,
  `exchange_rate` decimal(20,6) DEFAULT '0.000000',
  `unit_price_te` decimal(20,6) DEFAULT '0.000000',
  `quantity_expected` int(11) unsigned NOT NULL,
  `quantity_received` int(11) unsigned NOT NULL,
  `price_te` decimal(20,6) DEFAULT '0.000000',
  `discount_rate` decimal(20,6) DEFAULT '0.000000',
  `discount_value_te` decimal(20,6) DEFAULT '0.000000',
  `price_with_discount_te` decimal(20,6) DEFAULT '0.000000',
  `tax_rate` decimal(20,6) DEFAULT '0.000000',
  `tax_value` decimal(20,6) DEFAULT '0.000000',
  `price_ti` decimal(20,6) DEFAULT '0.000000',
  `tax_value_with_order_discount` decimal(20,6) DEFAULT '0.000000',
  `price_with_order_discount_te` decimal(20,6) DEFAULT '0.000000',
  PRIMARY KEY (`id_supply_order_detail`),
  KEY `id_supply_order` (`id_supply_order`,`id_product`),
  KEY `id_product_attribute` (`id_product_attribute`),
  KEY `id_product_product_attribute` (`id_product`,`id_product_attribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_supply_order_detail`
--

LOCK TABLES `ps_supply_order_detail` WRITE;
/*!40000 ALTER TABLE `ps_supply_order_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_supply_order_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_supply_order_history`
--

DROP TABLE IF EXISTS `ps_supply_order_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_supply_order_history` (
  `id_supply_order_history` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_supply_order` int(11) unsigned NOT NULL,
  `id_employee` int(11) unsigned NOT NULL,
  `employee_lastname` varchar(32) DEFAULT '',
  `employee_firstname` varchar(32) DEFAULT '',
  `id_state` int(11) unsigned NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_supply_order_history`),
  KEY `id_supply_order` (`id_supply_order`),
  KEY `id_employee` (`id_employee`),
  KEY `id_state` (`id_state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_supply_order_history`
--

LOCK TABLES `ps_supply_order_history` WRITE;
/*!40000 ALTER TABLE `ps_supply_order_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_supply_order_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_supply_order_receipt_history`
--

DROP TABLE IF EXISTS `ps_supply_order_receipt_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_supply_order_receipt_history` (
  `id_supply_order_receipt_history` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_supply_order_detail` int(11) unsigned NOT NULL,
  `id_employee` int(11) unsigned NOT NULL,
  `employee_lastname` varchar(32) DEFAULT '',
  `employee_firstname` varchar(32) DEFAULT '',
  `id_supply_order_state` int(11) unsigned NOT NULL,
  `quantity` int(11) unsigned NOT NULL,
  `date_add` datetime NOT NULL,
  PRIMARY KEY (`id_supply_order_receipt_history`),
  KEY `id_supply_order_detail` (`id_supply_order_detail`),
  KEY `id_supply_order_state` (`id_supply_order_state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_supply_order_receipt_history`
--

LOCK TABLES `ps_supply_order_receipt_history` WRITE;
/*!40000 ALTER TABLE `ps_supply_order_receipt_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_supply_order_receipt_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_supply_order_state`
--

DROP TABLE IF EXISTS `ps_supply_order_state`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_supply_order_state` (
  `id_supply_order_state` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `delivery_note` tinyint(1) NOT NULL DEFAULT '0',
  `editable` tinyint(1) NOT NULL DEFAULT '0',
  `receipt_state` tinyint(1) NOT NULL DEFAULT '0',
  `pending_receipt` tinyint(1) NOT NULL DEFAULT '0',
  `enclosed` tinyint(1) NOT NULL DEFAULT '0',
  `color` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id_supply_order_state`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_supply_order_state`
--

LOCK TABLES `ps_supply_order_state` WRITE;
/*!40000 ALTER TABLE `ps_supply_order_state` DISABLE KEYS */;
INSERT INTO `ps_supply_order_state` VALUES (1,0,1,0,0,0,'#faab00'),(2,1,0,0,0,0,'#273cff'),(3,0,0,0,1,0,'#ff37f5'),(4,0,0,1,1,0,'#ff3e33'),(5,0,0,1,0,1,'#00d60c'),(6,0,0,0,0,1,'#666666');
/*!40000 ALTER TABLE `ps_supply_order_state` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_supply_order_state_lang`
--

DROP TABLE IF EXISTS `ps_supply_order_state_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_supply_order_state_lang` (
  `id_supply_order_state` int(11) unsigned NOT NULL,
  `id_lang` int(11) unsigned NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_supply_order_state`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_supply_order_state_lang`
--

LOCK TABLES `ps_supply_order_state_lang` WRITE;
/*!40000 ALTER TABLE `ps_supply_order_state_lang` DISABLE KEYS */;
INSERT INTO `ps_supply_order_state_lang` VALUES (1,1,'1 - 正在創建'),(1,2,'1 - Creation in progress'),(1,3,'1 - Creation in progress'),(1,4,'1 - 処理手続き中'),(2,1,'2 - 訂單已驗證'),(2,2,'2 - Order validated'),(2,3,'2 - Order validated'),(2,4,'2 - 注文確定済み'),(3,1,'3 - 待收貨'),(3,2,'3 - Pending receipt'),(3,3,'3 - Pending receipt'),(3,4,'3 - 受取確認待ち'),(4,1,'4 - 收到部分訂單'),(4,2,'4 - Order received in part'),(4,3,'4 - Order received in part'),(4,4,'4 - 注文商品一部受取'),(5,1,'5 - 訂單已完全接收'),(5,2,'5 - Order received completely'),(5,3,'5 - Order received completely'),(5,4,'5 - 全ての注文商品を受取'),(6,1,'6 - 訂單取消'),(6,2,'6 - Order canceled'),(6,3,'6 - Order canceled'),(6,4,'6 - 注文キャンセル');
/*!40000 ALTER TABLE `ps_supply_order_state_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_tab`
--

DROP TABLE IF EXISTS `ps_tab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_tab` (
  `id_tab` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `module` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `hide_host_mode` tinyint(1) NOT NULL,
  `icon` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_tab`)
) ENGINE=InnoDB AUTO_INCREMENT=132 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_tab`
--

LOCK TABLES `ps_tab` WRITE;
/*!40000 ALTER TABLE `ps_tab` DISABLE KEYS */;
INSERT INTO `ps_tab` VALUES (1,0,1,NULL,'AdminDashboard',1,0,'trending_up'),(2,0,2,NULL,'SELL',1,0,''),(3,2,1,NULL,'AdminParentOrders',1,0,'shopping_basket'),(4,3,1,NULL,'AdminOrders',1,0,''),(5,3,2,NULL,'AdminInvoices',0,0,''),(6,3,3,NULL,'AdminSlip',0,0,''),(7,3,4,NULL,'AdminDeliverySlip',0,0,''),(8,3,5,NULL,'AdminCarts',1,0,''),(9,2,2,NULL,'AdminCatalog',1,0,'store'),(10,9,1,NULL,'AdminProducts',1,0,''),(11,9,2,NULL,'AdminCategories',1,0,''),(12,9,3,NULL,'AdminTracking',0,0,''),(13,9,4,NULL,'AdminParentAttributesGroups',1,0,''),(14,13,1,NULL,'AdminAttributesGroups',1,0,''),(15,13,2,NULL,'AdminFeatures',1,0,''),(16,9,5,NULL,'AdminParentManufacturers',0,0,''),(17,16,1,NULL,'AdminManufacturers',1,0,''),(18,16,2,NULL,'AdminSuppliers',1,0,''),(19,9,6,NULL,'AdminAttachments',0,0,''),(20,9,7,NULL,'AdminParentCartRules',0,0,''),(21,9,7,NULL,'AdminCartRules',1,0,''),(22,20,2,NULL,'AdminSpecificPriceRule',0,0,''),(23,9,8,NULL,'AdminStockManagement',1,0,''),(24,2,3,NULL,'AdminParentCustomer',1,0,'account_circle'),(25,24,1,NULL,'AdminCustomers',1,0,''),(26,24,2,NULL,'AdminAddresses',1,0,''),(27,24,3,NULL,'AdminOutstanding',0,0,''),(28,2,4,NULL,'AdminParentCustomerThreads',1,0,'chat'),(29,28,1,NULL,'AdminCustomerThreads',1,0,''),(30,28,2,NULL,'AdminOrderMessage',1,0,''),(31,28,3,NULL,'AdminReturn',0,0,''),(32,2,5,NULL,'AdminStats',1,0,'assessment'),(33,2,6,NULL,'AdminStock',1,0,'store'),(34,33,1,NULL,'AdminWarehouses',1,0,''),(35,33,2,NULL,'AdminParentStockManagement',1,0,''),(36,35,1,NULL,'AdminStockManagement',1,0,''),(37,36,1,NULL,'AdminStockMvt',1,0,''),(38,36,2,NULL,'AdminStockInstantState',1,0,''),(39,36,3,NULL,'AdminStockCover',1,0,''),(40,33,3,NULL,'AdminSupplyOrders',1,0,''),(41,33,4,NULL,'AdminStockConfiguration',1,0,''),(42,0,3,NULL,'IMPROVE',1,0,''),(43,42,1,NULL,'AdminParentModulesSf',0,0,'extension'),(44,43,1,NULL,'AdminModulesSf',0,0,''),(45,44,1,NULL,'AdminModulesManage',0,0,''),(46,44,2,NULL,'AdminModulesCatalog',0,0,''),(47,44,3,NULL,'AdminModulesNotifications',0,0,''),(48,43,2,NULL,'AdminModules',0,0,''),(49,43,3,NULL,'AdminAddonsCatalog',1,0,''),(50,42,2,NULL,'AdminParentThemes',1,0,'desktop_mac'),(51,121,1,'','AdminThemes',1,0,''),(52,50,2,NULL,'AdminThemesCatalog',0,0,''),(53,50,3,NULL,'AdminCmsContent',1,0,''),(54,50,5,NULL,'AdminModulesPositions',1,0,''),(55,77,5,NULL,'AdminImages',1,0,''),(56,42,3,NULL,'AdminParentShipping',0,0,'local_shipping'),(57,59,2,NULL,'AdminCarriers',1,0,''),(58,56,2,NULL,'AdminShipping',1,0,''),(59,42,4,NULL,'AdminParentPayment',1,0,'payment'),(60,59,1,NULL,'AdminPayment',1,0,''),(61,59,3,NULL,'AdminPaymentPreferences',1,0,''),(62,42,5,NULL,'AdminInternational',1,0,'language'),(63,62,1,NULL,'AdminParentLocalization',0,0,''),(64,63,1,NULL,'AdminLocalization',1,0,''),(65,62,1,NULL,'AdminLanguages',1,0,''),(66,62,2,NULL,'AdminCurrencies',1,0,''),(67,63,4,NULL,'AdminGeolocation',0,0,''),(68,62,2,NULL,'AdminParentCountries',1,0,''),(69,68,1,NULL,'AdminZones',1,0,''),(70,68,2,NULL,'AdminCountries',1,0,''),(71,68,3,NULL,'AdminStates',0,0,''),(72,62,3,NULL,'AdminParentTaxes',0,0,''),(73,72,1,NULL,'AdminTaxes',1,0,''),(74,72,2,NULL,'AdminTaxRulesGroup',1,0,''),(75,62,4,NULL,'AdminTranslations',0,0,''),(76,0,4,NULL,'CONFIGURE',1,0,''),(77,76,1,NULL,'ShopParameters',1,0,'settings'),(78,77,1,NULL,'AdminParentPreferences',0,0,''),(79,78,1,NULL,'AdminPreferences',1,0,''),(80,78,2,NULL,'AdminMaintenance',1,0,''),(81,77,2,NULL,'AdminParentOrderPreferences',0,0,''),(82,81,1,NULL,'AdminOrderPreferences',1,0,''),(83,81,2,NULL,'AdminStatuses',1,0,''),(84,77,3,NULL,'AdminPPreferences',1,0,''),(85,77,4,NULL,'AdminParentCustomerPreferences',0,0,''),(86,85,1,NULL,'AdminCustomerPreferences',0,0,''),(87,85,2,NULL,'AdminGroups',1,0,''),(88,85,3,NULL,'AdminGenders',1,0,''),(89,77,5,NULL,'AdminParentStores',1,0,''),(90,89,1,NULL,'AdminContacts',1,0,''),(91,89,2,NULL,'AdminStores',1,0,''),(92,77,6,NULL,'AdminParentMeta',1,0,''),(93,92,1,NULL,'AdminMeta',1,0,''),(94,92,2,NULL,'AdminSearchEngines',0,0,''),(95,92,3,NULL,'AdminReferrers',0,0,''),(96,77,7,NULL,'AdminParentSearchConf',1,0,''),(97,96,1,NULL,'AdminSearchConf',1,0,''),(98,96,2,NULL,'AdminTags',1,0,''),(99,76,2,NULL,'AdminAdvancedParameters',1,0,'settings_applications'),(100,99,1,NULL,'AdminInformation',0,0,''),(101,99,2,NULL,'AdminPerformance',1,0,''),(102,99,3,NULL,'AdminAdminPreferences',0,0,''),(103,77,4,NULL,'AdminEmails',1,0,''),(104,99,5,NULL,'AdminImport',1,0,''),(105,99,6,NULL,'AdminParentEmployees',0,0,''),(106,105,1,NULL,'AdminEmployees',1,0,''),(107,105,2,NULL,'AdminProfiles',0,0,''),(108,105,3,NULL,'AdminAccess',0,0,''),(109,99,7,NULL,'AdminParentRequestSql',1,0,''),(110,109,1,NULL,'AdminRequestSql',1,0,''),(111,109,2,NULL,'AdminBackup',1,0,''),(112,99,8,NULL,'AdminLogs',0,0,''),(113,99,9,NULL,'AdminWebservice',0,0,''),(114,99,10,NULL,'AdminShopGroup',1,0,''),(115,99,11,NULL,'AdminShopUrl',0,0,''),(116,-1,1,NULL,'AdminQuickAccesses',1,0,''),(117,0,5,NULL,'DEFAULT',0,0,''),(118,-1,2,NULL,'AdminPatterns',1,0,''),(119,-1,3,'dashgoals','AdminDashgoals',1,0,''),(120,50,6,'ps_linklist','AdminLinkWidget',0,0,''),(121,50,1,'','AdminThemesParent',1,0,''),(122,121,2,'ps_themecusto','AdminPsThemeCustoConfiguration',1,0,''),(123,121,3,'ps_themecusto','AdminPsThemeCustoAdvanced',1,0,''),(128,-1,5,'smilepay_palmboxc2cup','AdminSmilepayPalmboxc2cup',1,0,''),(129,-1,6,'smilepay_ezcatup','AdminSmilepayEzcatup',1,0,''),(130,-1,7,'smilepay_ezcat','AdminSmilepayEzcat',1,0,''),(131,-1,8,'simplicity_fbmessaging','AdminSimplicityFbMessaging',1,0,'');
/*!40000 ALTER TABLE `ps_tab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_tab_bak`
--

DROP TABLE IF EXISTS `ps_tab_bak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_tab_bak` (
  `id_tab` int(11) NOT NULL AUTO_INCREMENT,
  `id_parent` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `module` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `class_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `hide_host_mode` tinyint(1) NOT NULL,
  `icon` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_tab`)
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_tab_bak`
--

LOCK TABLES `ps_tab_bak` WRITE;
/*!40000 ALTER TABLE `ps_tab_bak` DISABLE KEYS */;
INSERT INTO `ps_tab_bak` VALUES (1,0,1,NULL,'AdminDashboard',1,0,'trending_up'),(2,0,2,NULL,'SELL',1,0,''),(3,2,1,NULL,'AdminParentOrders',1,0,'shopping_basket'),(4,3,1,NULL,'AdminOrders',1,0,''),(5,3,2,NULL,'AdminInvoices',1,0,''),(6,3,3,NULL,'AdminSlip',1,0,''),(7,3,4,NULL,'AdminDeliverySlip',1,0,''),(8,3,5,NULL,'AdminCarts',1,0,''),(9,2,2,NULL,'AdminCatalog',1,0,'store'),(10,9,1,NULL,'AdminProducts',1,0,''),(11,9,2,NULL,'AdminCategories',1,0,''),(12,9,3,NULL,'AdminTracking',1,0,''),(13,9,4,NULL,'AdminParentAttributesGroups',1,0,''),(14,13,1,NULL,'AdminAttributesGroups',1,0,''),(15,13,2,NULL,'AdminFeatures',1,0,''),(16,9,5,NULL,'AdminParentManufacturers',1,0,''),(17,16,1,NULL,'AdminManufacturers',1,0,''),(18,16,2,NULL,'AdminSuppliers',1,0,''),(19,9,6,NULL,'AdminAttachments',1,0,''),(20,9,7,NULL,'AdminParentCartRules',1,0,''),(21,20,1,NULL,'AdminCartRules',1,0,''),(22,20,2,NULL,'AdminSpecificPriceRule',1,0,''),(23,9,8,NULL,'AdminStockManagement',1,0,''),(24,2,3,NULL,'AdminParentCustomer',1,0,'account_circle'),(25,24,1,NULL,'AdminCustomers',1,0,''),(26,24,2,NULL,'AdminAddresses',1,0,''),(27,24,3,NULL,'AdminOutstanding',0,0,''),(28,2,4,NULL,'AdminParentCustomerThreads',1,0,'chat'),(29,28,1,NULL,'AdminCustomerThreads',1,0,''),(30,28,2,NULL,'AdminOrderMessage',1,0,''),(31,28,3,NULL,'AdminReturn',1,0,''),(32,2,5,NULL,'AdminStats',1,0,'assessment'),(33,2,6,NULL,'AdminStock',1,0,'store'),(34,33,1,NULL,'AdminWarehouses',1,0,''),(35,33,2,NULL,'AdminParentStockManagement',1,0,''),(36,35,1,NULL,'AdminStockManagement',1,0,''),(37,36,1,NULL,'AdminStockMvt',1,0,''),(38,36,2,NULL,'AdminStockInstantState',1,0,''),(39,36,3,NULL,'AdminStockCover',1,0,''),(40,33,3,NULL,'AdminSupplyOrders',1,0,''),(41,33,4,NULL,'AdminStockConfiguration',1,0,''),(42,0,3,NULL,'IMPROVE',1,0,''),(43,42,1,NULL,'AdminParentModulesSf',1,0,'extension'),(44,43,1,NULL,'AdminModulesSf',1,0,''),(45,44,1,NULL,'AdminModulesManage',1,0,''),(46,44,2,NULL,'AdminModulesCatalog',1,0,''),(47,44,3,NULL,'AdminModulesNotifications',1,0,''),(48,43,2,NULL,'AdminModules',0,0,''),(49,43,3,NULL,'AdminAddonsCatalog',1,0,''),(50,42,2,NULL,'AdminParentThemes',1,0,'desktop_mac'),(51,121,1,'','AdminThemes',1,0,''),(52,50,2,NULL,'AdminThemesCatalog',1,0,''),(53,50,3,NULL,'AdminCmsContent',1,0,''),(54,50,4,NULL,'AdminModulesPositions',1,0,''),(55,50,5,NULL,'AdminImages',1,0,''),(56,42,3,NULL,'AdminParentShipping',1,0,'local_shipping'),(57,56,1,NULL,'AdminCarriers',1,0,''),(58,56,2,NULL,'AdminShipping',1,0,''),(59,42,4,NULL,'AdminParentPayment',1,0,'payment'),(60,59,1,NULL,'AdminPayment',1,0,''),(61,59,2,NULL,'AdminPaymentPreferences',1,0,''),(62,42,5,NULL,'AdminInternational',1,0,'language'),(63,62,1,NULL,'AdminParentLocalization',1,0,''),(64,63,1,NULL,'AdminLocalization',1,0,''),(65,63,2,NULL,'AdminLanguages',1,0,''),(66,63,3,NULL,'AdminCurrencies',1,0,''),(67,63,4,NULL,'AdminGeolocation',1,0,''),(68,62,2,NULL,'AdminParentCountries',1,0,''),(69,68,1,NULL,'AdminZones',1,0,''),(70,68,2,NULL,'AdminCountries',1,0,''),(71,68,3,NULL,'AdminStates',1,0,''),(72,62,3,NULL,'AdminParentTaxes',1,0,''),(73,72,1,NULL,'AdminTaxes',1,0,''),(74,72,2,NULL,'AdminTaxRulesGroup',1,0,''),(75,62,4,NULL,'AdminTranslations',1,0,''),(76,0,4,NULL,'CONFIGURE',1,0,''),(77,76,1,NULL,'ShopParameters',1,0,'settings'),(78,77,1,NULL,'AdminParentPreferences',1,0,''),(79,78,1,NULL,'AdminPreferences',1,0,''),(80,78,2,NULL,'AdminMaintenance',1,0,''),(81,77,2,NULL,'AdminParentOrderPreferences',1,0,''),(82,81,1,NULL,'AdminOrderPreferences',1,0,''),(83,81,2,NULL,'AdminStatuses',1,0,''),(84,77,3,NULL,'AdminPPreferences',1,0,''),(85,77,4,NULL,'AdminParentCustomerPreferences',1,0,''),(86,85,1,NULL,'AdminCustomerPreferences',1,0,''),(87,85,2,NULL,'AdminGroups',1,0,''),(88,85,3,NULL,'AdminGenders',1,0,''),(89,77,5,NULL,'AdminParentStores',1,0,''),(90,89,1,NULL,'AdminContacts',1,0,''),(91,89,2,NULL,'AdminStores',1,0,''),(92,77,6,NULL,'AdminParentMeta',1,0,''),(93,92,1,NULL,'AdminMeta',1,0,''),(94,92,2,NULL,'AdminSearchEngines',1,0,''),(95,92,3,NULL,'AdminReferrers',1,0,''),(96,77,7,NULL,'AdminParentSearchConf',1,0,''),(97,96,1,NULL,'AdminSearchConf',1,0,''),(98,96,2,NULL,'AdminTags',1,0,''),(99,76,2,NULL,'AdminAdvancedParameters',1,0,'settings_applications'),(100,99,1,NULL,'AdminInformation',1,0,''),(101,99,2,NULL,'AdminPerformance',1,0,''),(102,99,3,NULL,'AdminAdminPreferences',1,0,''),(103,99,4,NULL,'AdminEmails',1,0,''),(104,99,5,NULL,'AdminImport',1,0,''),(105,99,6,NULL,'AdminParentEmployees',1,0,''),(106,105,1,NULL,'AdminEmployees',1,0,''),(107,105,2,NULL,'AdminProfiles',1,0,''),(108,105,3,NULL,'AdminAccess',1,0,''),(109,99,7,NULL,'AdminParentRequestSql',1,0,''),(110,109,1,NULL,'AdminRequestSql',1,0,''),(111,109,2,NULL,'AdminBackup',1,0,''),(112,99,8,NULL,'AdminLogs',1,0,''),(113,99,9,NULL,'AdminWebservice',1,0,''),(114,99,10,NULL,'AdminShopGroup',0,0,''),(115,99,11,NULL,'AdminShopUrl',0,0,''),(116,-1,1,NULL,'AdminQuickAccesses',1,0,''),(117,0,5,NULL,'DEFAULT',1,0,''),(118,-1,2,NULL,'AdminPatterns',1,0,''),(119,-1,3,'dashgoals','AdminDashgoals',1,0,''),(120,50,6,'ps_linklist','AdminLinkWidget',1,0,''),(121,50,1,'','AdminThemesParent',1,0,''),(122,121,2,'ps_themecusto','AdminPsThemeCustoConfiguration',1,0,''),(123,121,3,'ps_themecusto','AdminPsThemeCustoAdvanced',1,0,''),(128,-1,5,'smilepay_palmboxc2cup','AdminSmilepayPalmboxc2cup',1,0,''),(129,-1,6,'smilepay_ezcatup','AdminSmilepayEzcatup',1,0,''),(130,-1,7,'smilepay_ezcat','AdminSmilepayEzcat',1,0,'');
/*!40000 ALTER TABLE `ps_tab_bak` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_tab_lang`
--

DROP TABLE IF EXISTS `ps_tab_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_tab_lang` (
  `id_tab` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_tab`,`id_lang`),
  KEY `IDX_CFD9262DED47AB56` (`id_tab`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_tab_lang`
--

LOCK TABLES `ps_tab_lang` WRITE;
/*!40000 ALTER TABLE `ps_tab_lang` DISABLE KEYS */;
INSERT INTO `ps_tab_lang` VALUES (1,1,'主控台'),(1,2,'仪表盘'),(1,3,'仪表盘'),(1,4,'仪表盘'),(2,1,'銷售'),(2,2,'出售'),(2,3,'出售'),(2,4,'出售'),(3,1,'訂單'),(3,2,'订单'),(3,3,'订单'),(3,4,'订单'),(4,1,'訂單'),(4,2,'订单'),(4,3,'订单'),(4,4,'订单'),(5,1,'發票'),(5,2,'发票'),(5,3,'发票'),(5,4,'发票'),(6,1,'貸方票據'),(6,2,'Avoirs'),(6,3,'Avoirs'),(6,4,'Avoirs'),(7,1,'送貨單'),(7,2,'交货单'),(7,3,'交货单'),(7,4,'交货单'),(8,1,'購物車'),(8,2,'购物车'),(8,3,'购物车'),(8,4,'购物车'),(9,1,'商品'),(9,2,'分类'),(9,3,'分类'),(9,4,'分类'),(10,1,'商品'),(10,2,'单件商品'),(10,3,'单件商品'),(10,4,'单件商品'),(11,1,'分類'),(11,2,'分类'),(11,3,'分类'),(11,4,'分类'),(12,1,'監控'),(12,2,'监测'),(12,3,'监测'),(12,4,'监测'),(13,1,'屬性＆規格'),(13,2,'属性和功能'),(13,3,'属性和功能'),(13,4,'属性和功能'),(14,1,'屬性'),(14,2,'特征'),(14,3,'特征'),(14,4,'特征'),(15,1,'規格'),(15,2,'特性'),(15,3,'特性'),(15,4,'特性'),(16,1,'品牌&供應商'),(16,2,'品牌 & 供应商'),(16,3,'品牌 & 供应商'),(16,4,'品牌 & 供应商'),(17,1,'品牌'),(17,2,'品牌'),(17,3,'品牌'),(17,4,'品牌'),(18,1,'供應商'),(18,2,'供应商'),(18,3,'供应商'),(18,4,'供应商'),(19,1,'檔案'),(19,2,'文件'),(19,3,'文件'),(19,4,'文件'),(20,1,'折扣'),(20,2,'Discounts'),(20,3,'折扣'),(20,4,'割引'),(21,1,'購物車規則'),(21,2,'购物车规则'),(21,3,'购物车规则'),(21,4,'购物车规则'),(22,1,'滿件折抵規則'),(22,2,'Règles de prix catalogue'),(22,3,'Règles de prix catalogue'),(22,4,'Règles de prix catalogue'),(23,1,'庫存'),(23,2,'Stocks'),(23,3,'Stocks'),(23,4,'Stocks'),(24,1,'客戶'),(24,2,'客户'),(24,3,'客户'),(24,4,'客户'),(25,1,'客戶'),(25,2,'客户'),(25,3,'客户'),(25,4,'客户'),(26,1,'地址'),(26,2,'Addresses'),(26,3,'地址'),(26,4,'住所'),(27,1,'未付'),(27,2,'未支付'),(27,3,'未支付'),(27,4,'未支付'),(28,1,'客服'),(28,2,'客户服务'),(28,3,'客户服务'),(28,4,'客户服务'),(29,1,'客服問答'),(29,2,'客户服务'),(29,3,'客户服务'),(29,4,'客户服务'),(30,1,'預設訊息'),(30,2,'Messages prédéfinis'),(30,3,'Messages prédéfinis'),(30,4,'Messages prédéfinis'),(31,1,'退貨'),(31,2,'Retours produit'),(31,3,'Retours produit'),(31,4,'Retours produit'),(32,1,'追蹤'),(32,2,'统计信息'),(32,3,'统计信息'),(32,4,'统计信息'),(34,1,'倉庫'),(34,2,'仓库'),(34,3,'仓库'),(34,4,'仓库'),(35,1,'存貨管理'),(35,2,'Gestion de stock'),(35,3,'Gestion de stock'),(35,4,'Gestion de stock'),(36,1,'存貨管理'),(36,2,'Gestion de stock'),(36,3,'Gestion de stock'),(36,4,'Gestion de stock'),(37,1,'庫存變動'),(37,2,'库存变动'),(37,3,'库存变动'),(37,4,'库存变动'),(38,1,'即時庫存狀況'),(38,2,'Etat instantané du stock'),(38,3,'Etat instantané du stock'),(38,4,'Etat instantané du stock'),(39,1,'現貨供應情況'),(39,2,'库存范围'),(39,3,'库存范围'),(39,4,'库存范围'),(40,1,'採購訂單'),(40,2,'供应订单'),(40,3,'供应订单'),(40,4,'供应订单'),(41,1,'設定'),(41,2,'Configure'),(41,3,'配置'),(41,4,'設定'),(42,1,'佈置'),(42,2,'改进'),(42,3,'改进'),(42,4,'改进'),(43,1,'模組'),(43,2,'模块'),(43,3,'模块'),(43,4,'模块'),(44,1,'模組及服務'),(44,2,'模块和服务'),(44,3,'模块和服务'),(44,4,'模块和服务'),(45,1,'已安裝的模塊'),(45,2,'已安装的模块'),(45,3,'已安装的模块'),(45,4,'已安装的模块'),(46,1,'選項'),(46,2,'选择'),(46,3,'选择'),(46,4,'选择'),(47,1,'提醒'),(47,2,'通知'),(47,3,'通知'),(47,4,'通知'),(49,1,'模組目錄'),(49,2,'模块目录'),(49,3,'模块目录'),(49,4,'模块目录'),(50,1,'設計'),(50,2,'设计'),(50,3,'设计'),(50,4,'设计'),(51,1,'佈景主題 ＆ Logo'),(51,2,'Theme & Logo'),(51,3,'Theme & Logo'),(51,4,'テーマとロゴ'),(52,1,'佈景目錄'),(52,2,'主题分类'),(52,3,'主题分类'),(52,4,'主题分类'),(53,1,'自訂頁面'),(53,2,'网页'),(53,3,'网页'),(53,4,'网页'),(54,1,'模組位置'),(54,2,'Positions'),(54,3,'位置'),(54,4,'位置'),(55,1,'圖片'),(55,2,'图像设置'),(55,3,'图像设置'),(55,4,'图像设置'),(56,1,'運費'),(56,2,'配送'),(56,3,'配送'),(56,4,'配送'),(57,1,'承運商 (運費規則)'),(57,2,'货运商'),(57,3,'货运商'),(57,4,'货运商'),(58,1,'偏好設定'),(58,2,'选项'),(58,3,'选项'),(58,4,'选项'),(59,1,'金物流'),(59,2,'付款'),(59,3,'付款'),(59,4,'付款'),(60,1,'金物流模組'),(60,2,'Payment Methods'),(60,3,'Payment Methods'),(60,4,'支払い方法'),(61,1,'偏好設定'),(61,2,'选项'),(61,3,'选项'),(61,4,'选项'),(62,1,'多國'),(62,2,'国际'),(62,3,'国际'),(62,4,'国际'),(63,1,'本地化'),(63,2,'Localization'),(63,3,'本地化'),(63,4,'地域設定'),(64,1,'本地化'),(64,2,'Localization'),(64,3,'本地化'),(64,4,'地域設定'),(65,1,'語言'),(65,2,'语言'),(65,3,'语言'),(65,4,'语言'),(66,1,'貨幣'),(66,2,'货币'),(66,3,'货币'),(66,4,'货币'),(67,1,'地理位置'),(67,2,'Geolocation'),(67,3,'地理位置'),(67,4,'位置情報'),(68,1,'地點'),(68,2,'地点'),(68,3,'地点'),(68,4,'地点'),(69,1,'區域'),(69,2,'洲'),(69,3,'洲'),(69,4,'洲'),(70,1,'國家'),(70,2,'国家'),(70,3,'国家'),(70,4,'国家'),(71,1,'州'),(71,2,'省/市'),(71,3,'省/市'),(71,4,'省/市'),(72,1,'稅'),(72,2,'税'),(72,3,'税'),(72,4,'税'),(73,1,'稅'),(73,2,'税'),(73,3,'税'),(73,4,'税'),(74,1,'稅務規則'),(74,2,'税法'),(74,3,'税法'),(74,4,'税法'),(75,1,'翻譯'),(75,2,'翻译'),(75,3,'翻译'),(75,4,'翻译'),(76,1,'更多'),(76,2,'Configure'),(76,3,'配置'),(76,4,'設定'),(77,1,'設定'),(77,2,'Shop Parameters'),(77,3,'Shop Parameters'),(77,4,'ショップパラメータ'),(78,1,'一般'),(78,2,'General'),(78,3,'一般'),(78,4,'一般'),(79,1,'一般'),(79,2,'General'),(79,3,'一般'),(79,4,'一般'),(80,1,'網站維護'),(80,2,'维护'),(80,3,'维护'),(80,4,'维护'),(81,1,'訂購設定'),(81,2,'订单设置'),(81,3,'订单设置'),(81,4,'订单设置'),(82,1,'訂購設定'),(82,2,'订单设置'),(82,3,'订单设置'),(82,4,'订单设置'),(83,1,'États'),(83,2,'状态'),(83,3,'状态'),(83,4,'状态'),(84,1,'商品'),(84,2,'单件商品'),(84,3,'单件商品'),(84,4,'单件商品'),(85,1,'客戶設定'),(85,2,'客户设置'),(85,3,'客户设置'),(85,4,'客户设置'),(86,1,'客戶'),(86,2,'客户'),(86,3,'客户'),(86,4,'客户'),(87,1,'群組'),(87,2,'组'),(87,3,'组'),(87,4,'组'),(88,1,'稱號'),(88,2,'Titres de civilité'),(88,3,'Titres de civilité'),(88,4,'Titres de civilité'),(89,1,'商店'),(89,2,'联系我们'),(89,3,'联系我们'),(89,4,'联系我们'),(90,1,'客服信箱'),(90,2,'联系方式'),(90,3,'联系方式'),(90,4,'联系方式'),(91,1,'詳細資訊'),(91,2,'店铺'),(91,3,'店铺'),(91,4,'店铺'),(92,1,'SEO＆Meta'),(92,2,'Traffic & SEO'),(92,3,'Traffic & SEO'),(92,4,'トラフィックとSEOの設定'),(93,1,'SEO＆Meta'),(93,2,'SEO & 链接'),(93,3,'SEO & 链接'),(93,4,'SEO & 链接'),(94,1,'搜索引擎'),(94,2,'Search Engines'),(94,3,'搜索引擎'),(94,4,'検索エンジン'),(95,1,'來源'),(95,2,'推荐'),(95,3,'推荐'),(95,4,'推荐'),(96,1,'搜尋'),(96,2,'搜索'),(96,3,'搜索'),(96,4,'搜索'),(97,1,'搜尋'),(97,2,'搜索'),(97,3,'搜索'),(97,4,'搜索'),(98,1,'標籤'),(98,2,'标签'),(98,3,'标签'),(98,4,'标签'),(99,1,'進階設定'),(99,2,'高级参数'),(99,3,'高级参数'),(99,4,'高级参数'),(100,1,'資訊'),(100,2,'信息'),(100,3,'信息'),(100,4,'信息'),(101,1,'效能'),(101,2,'性能'),(101,3,'性能'),(101,4,'性能'),(102,1,'管理'),(102,2,'行政'),(102,3,'行政'),(102,4,'行政'),(103,1,'通知信'),(103,2,'邮箱'),(103,3,'邮箱'),(103,4,'邮箱'),(104,1,'匯入'),(104,2,'上传'),(104,3,'上传'),(104,4,'上传'),(105,1,'團隊'),(105,2,'雇员'),(105,3,'雇员'),(105,4,'雇员'),(106,1,'員工'),(106,2,'雇员'),(106,3,'雇员'),(106,4,'雇员'),(107,1,'職位'),(107,2,'职位'),(107,3,'职位'),(107,4,'职位'),(108,1,'權限'),(108,2,'权限'),(108,3,'权限'),(108,4,'权限'),(109,1,'資料庫'),(109,2,'数据库'),(109,3,'数据库'),(109,4,'数据库'),(110,1,'SQL管理器'),(110,2,'SQL Manager'),(110,3,'SQL管理器'),(110,4,'SQL マネージャ'),(111,1,'數據庫備份'),(111,2,'数据库备份'),(111,3,'数据库备份'),(111,4,'数据库备份'),(112,1,'記錄'),(112,2,'日志'),(112,3,'日志'),(112,4,'日志'),(113,1,'網路服務'),(113,2,'Web服务'),(113,3,'Web服务'),(113,4,'Web服务'),(114,1,'Multiboutique'),(114,2,'Multistore'),(114,3,'Multiboutique'),(114,4,'マルチストア'),(115,1,'Multiboutique'),(115,2,'Multistore'),(115,3,'Multiboutique'),(115,4,'マルチストア'),(116,1,'快速連結'),(116,2,'快速通道'),(116,3,'快速通道'),(116,4,'快速通道'),(117,1,'更多'),(117,2,'More'),(117,3,'更多'),(117,4,'もっと'),(119,1,'Dashgoals'),(119,2,'Dashgoals'),(119,3,'Dashgoals'),(119,4,'Dashgoals'),(120,1,'Link Widget'),(120,2,'Link Widget'),(120,3,'Link Widget'),(120,4,'Link Widget'),(121,1,'佈景'),(121,2,'Theme & Logo'),(121,3,'Theme & Logo'),(121,4,'テーマとロゴ'),(122,1,'佈景模組'),(122,2,'Homepage Configuration'),(122,3,'Homepage Configuration'),(122,4,'Homepage Configuration'),(123,1,'客製化佈景'),(123,2,'Advanced Customization'),(123,3,'Advanced Customization'),(123,4,'Advanced Customization'),(128,1,'Smilepaypalmboxc2cup'),(128,2,'Smilepaypalmboxc2cup'),(128,3,'Smilepaypalmboxc2cup'),(128,4,'Smilepaypalmboxc2cup'),(129,1,'SmilepayEzcatup'),(129,2,'SmilepayEzcatup'),(129,3,'SmilepayEzcatup'),(129,4,'SmilepayEzcatup'),(130,1,'SmilepayEzcat'),(130,2,'SmilepayEzcat'),(130,3,'SmilepayEzcat'),(130,4,'SmilepayEzcat'),(131,1,'AdminSimplicityFbMessaging'),(131,2,'AdminSimplicityFbMessaging'),(131,3,'AdminSimplicityFbMessaging'),(131,4,'AdminSimplicityFbMessaging');
/*!40000 ALTER TABLE `ps_tab_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_tab_lang_bak`
--

DROP TABLE IF EXISTS `ps_tab_lang_bak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_tab_lang_bak` (
  `id_tab` int(11) NOT NULL,
  `id_lang` int(11) NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_tab`,`id_lang`),
  KEY `IDX_CFD9262DED47AB56` (`id_tab`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_tab_lang_bak`
--

LOCK TABLES `ps_tab_lang_bak` WRITE;
/*!40000 ALTER TABLE `ps_tab_lang_bak` DISABLE KEYS */;
INSERT INTO `ps_tab_lang_bak` VALUES (1,1,'控制面板'),(1,2,'仪表盘'),(1,3,'仪表盘'),(1,4,'仪表盘'),(2,1,'銷售'),(2,2,'出售'),(2,3,'出售'),(2,4,'出售'),(3,1,'訂單'),(3,2,'订单'),(3,3,'订单'),(3,4,'订单'),(4,1,'訂單'),(4,2,'订单'),(4,3,'订单'),(4,4,'订单'),(5,1,'發票'),(5,2,'发票'),(5,3,'发票'),(5,4,'发票'),(6,1,'貸方票據'),(6,2,'Avoirs'),(6,3,'Avoirs'),(6,4,'Avoirs'),(7,1,'送貨單'),(7,2,'交货单'),(7,3,'交货单'),(7,4,'交货单'),(8,1,'購物車'),(8,2,'购物车'),(8,3,'购物车'),(8,4,'购物车'),(9,1,'商品目錄'),(9,2,'分类'),(9,3,'分类'),(9,4,'分类'),(10,1,'商品'),(10,2,'单件商品'),(10,3,'单件商品'),(10,4,'单件商品'),(11,1,'分類'),(11,2,'分类'),(11,3,'分类'),(11,4,'分类'),(12,1,'監控'),(12,2,'监测'),(12,3,'监测'),(12,4,'监测'),(13,1,'規格&特點'),(13,2,'属性和功能'),(13,3,'属性和功能'),(13,4,'属性和功能'),(14,1,'屬性'),(14,2,'特征'),(14,3,'特征'),(14,4,'特征'),(15,1,'特性'),(15,2,'特性'),(15,3,'特性'),(15,4,'特性'),(16,1,'品牌&供應商'),(16,2,'品牌 & 供应商'),(16,3,'品牌 & 供应商'),(16,4,'品牌 & 供应商'),(17,1,'品牌'),(17,2,'品牌'),(17,3,'品牌'),(17,4,'品牌'),(18,1,'供應商'),(18,2,'供应商'),(18,3,'供应商'),(18,4,'供应商'),(19,1,'檔案'),(19,2,'文件'),(19,3,'文件'),(19,4,'文件'),(20,1,'折扣'),(20,2,'Discounts'),(20,3,'折扣'),(20,4,'割引'),(21,1,'購物車規則'),(21,2,'购物车规则'),(21,3,'购物车规则'),(21,4,'购物车规则'),(22,1,'滿件折抵規則'),(22,2,'Règles de prix catalogue'),(22,3,'Règles de prix catalogue'),(22,4,'Règles de prix catalogue'),(23,1,'Stocks'),(23,2,'Stocks'),(23,3,'Stocks'),(23,4,'Stocks'),(24,1,'客戶'),(24,2,'客户'),(24,3,'客户'),(24,4,'客户'),(25,1,'客戶'),(25,2,'客户'),(25,3,'客户'),(25,4,'客户'),(26,1,'地址'),(26,2,'Addresses'),(26,3,'地址'),(26,4,'住所'),(27,1,'未付'),(27,2,'未支付'),(27,3,'未支付'),(27,4,'未支付'),(28,1,'客戶服務'),(28,2,'客户服务'),(28,3,'客户服务'),(28,4,'客户服务'),(29,1,'客戶服務'),(29,2,'客户服务'),(29,3,'客户服务'),(29,4,'客户服务'),(30,1,'訂購消息'),(30,2,'Messages prédéfinis'),(30,3,'Messages prédéfinis'),(30,4,'Messages prédéfinis'),(31,1,'退貨'),(31,2,'Retours produit'),(31,3,'Retours produit'),(31,4,'Retours produit'),(32,1,'統計'),(32,2,'统计信息'),(32,3,'统计信息'),(32,4,'统计信息'),(34,1,'倉庫'),(34,2,'仓库'),(34,3,'仓库'),(34,4,'仓库'),(35,1,'存貨管理'),(35,2,'Gestion de stock'),(35,3,'Gestion de stock'),(35,4,'Gestion de stock'),(36,1,'存貨管理'),(36,2,'Gestion de stock'),(36,3,'Gestion de stock'),(36,4,'Gestion de stock'),(37,1,'庫存變動'),(37,2,'库存变动'),(37,3,'库存变动'),(37,4,'库存变动'),(38,1,'即時庫存狀況'),(38,2,'Etat instantané du stock'),(38,3,'Etat instantané du stock'),(38,4,'Etat instantané du stock'),(39,1,'現貨供應情況'),(39,2,'库存范围'),(39,3,'库存范围'),(39,4,'库存范围'),(40,1,'採購訂單'),(40,2,'供应订单'),(40,3,'供应订单'),(40,4,'供应订单'),(41,1,'設定'),(41,2,'Configure'),(41,3,'配置'),(41,4,'設定'),(42,1,'改進'),(42,2,'改进'),(42,3,'改进'),(42,4,'改进'),(43,1,'模組'),(43,2,'模块'),(43,3,'模块'),(43,4,'模块'),(44,1,'模組及服務'),(44,2,'模块和服务'),(44,3,'模块和服务'),(44,4,'模块和服务'),(45,1,'已安装的模塊'),(45,2,'已安装的模块'),(45,3,'已安装的模块'),(45,4,'已安装的模块'),(46,1,'選項'),(46,2,'选择'),(46,3,'选择'),(46,4,'选择'),(47,1,'提醒'),(47,2,'通知'),(47,3,'通知'),(47,4,'通知'),(49,1,'模組目錄'),(49,2,'模块目录'),(49,3,'模块目录'),(49,4,'模块目录'),(50,1,'設計'),(50,2,'设计'),(50,3,'设计'),(50,4,'设计'),(51,1,'佈景 ＆ Logo'),(51,2,'Theme & Logo'),(51,3,'Theme & Logo'),(51,4,'テーマとロゴ'),(52,1,'佈景目錄'),(52,2,'主题分类'),(52,3,'主题分类'),(52,4,'主题分类'),(53,1,'網頁'),(53,2,'网页'),(53,3,'网页'),(53,4,'网页'),(54,1,'位置'),(54,2,'Positions'),(54,3,'位置'),(54,4,'位置'),(55,1,'圖片配置'),(55,2,'图像设置'),(55,3,'图像设置'),(55,4,'图像设置'),(56,1,'運費'),(56,2,'配送'),(56,3,'配送'),(56,4,'配送'),(57,1,'承運商'),(57,2,'货运商'),(57,3,'货运商'),(57,4,'货运商'),(58,1,'偏好設定'),(58,2,'选项'),(58,3,'选项'),(58,4,'选项'),(59,1,'付款方式'),(59,2,'付款'),(59,3,'付款'),(59,4,'付款'),(60,1,'付款方式'),(60,2,'Payment Methods'),(60,3,'Payment Methods'),(60,4,'支払い方法'),(61,1,'偏好設定'),(61,2,'选项'),(61,3,'选项'),(61,4,'选项'),(62,1,'多國語言'),(62,2,'国际'),(62,3,'国际'),(62,4,'国际'),(63,1,'本地化'),(63,2,'Localization'),(63,3,'本地化'),(63,4,'地域設定'),(64,1,'本地化'),(64,2,'Localization'),(64,3,'本地化'),(64,4,'地域設定'),(65,1,'語言'),(65,2,'语言'),(65,3,'语言'),(65,4,'语言'),(66,1,'貨幣'),(66,2,'货币'),(66,3,'货币'),(66,4,'货币'),(67,1,'地理位置'),(67,2,'Geolocation'),(67,3,'地理位置'),(67,4,'位置情報'),(68,1,'地點'),(68,2,'地点'),(68,3,'地点'),(68,4,'地点'),(69,1,'區塊'),(69,2,'洲'),(69,3,'洲'),(69,4,'洲'),(70,1,'國家'),(70,2,'国家'),(70,3,'国家'),(70,4,'国家'),(71,1,'州'),(71,2,'省/市'),(71,3,'省/市'),(71,4,'省/市'),(72,1,'稅'),(72,2,'税'),(72,3,'税'),(72,4,'税'),(73,1,'稅'),(73,2,'税'),(73,3,'税'),(73,4,'税'),(74,1,'稅務規則'),(74,2,'税法'),(74,3,'税法'),(74,4,'税法'),(75,1,'翻譯'),(75,2,'翻译'),(75,3,'翻译'),(75,4,'翻译'),(76,1,'配置'),(76,2,'Configure'),(76,3,'配置'),(76,4,'設定'),(77,1,'商店參數'),(77,2,'Shop Parameters'),(77,3,'Shop Parameters'),(77,4,'ショップパラメータ'),(78,1,'一般'),(78,2,'General'),(78,3,'一般'),(78,4,'一般'),(79,1,'一般'),(79,2,'General'),(79,3,'一般'),(79,4,'一般'),(80,1,'網站維護'),(80,2,'维护'),(80,3,'维护'),(80,4,'维护'),(81,1,'訂購設定'),(81,2,'订单设置'),(81,3,'订单设置'),(81,4,'订单设置'),(82,1,'訂購設定'),(82,2,'订单设置'),(82,3,'订单设置'),(82,4,'订单设置'),(83,1,'États'),(83,2,'状态'),(83,3,'状态'),(83,4,'状态'),(84,1,'商品設定'),(84,2,'单件商品'),(84,3,'单件商品'),(84,4,'单件商品'),(85,1,'客戶設定'),(85,2,'客户设置'),(85,3,'客户设置'),(85,4,'客户设置'),(86,1,'客戶'),(86,2,'客户'),(86,3,'客户'),(86,4,'客户'),(87,1,'群組'),(87,2,'组'),(87,3,'组'),(87,4,'组'),(88,1,'標題'),(88,2,'Titres de civilité'),(88,3,'Titres de civilité'),(88,4,'Titres de civilité'),(89,1,'聯絡我們'),(89,2,'联系我们'),(89,3,'联系我们'),(89,4,'联系我们'),(90,1,'聯絡人'),(90,2,'联系方式'),(90,3,'联系方式'),(90,4,'联系方式'),(91,1,'商店'),(91,2,'店铺'),(91,3,'店铺'),(91,4,'店铺'),(92,1,'流量來源 ＆ SEO網址'),(92,2,'Traffic & SEO'),(92,3,'Traffic & SEO'),(92,4,'トラフィックとSEOの設定'),(93,1,'SEO & 鏈接'),(93,2,'SEO & 链接'),(93,3,'SEO & 链接'),(93,4,'SEO & 链接'),(94,1,'搜索引擎'),(94,2,'Search Engines'),(94,3,'搜索引擎'),(94,4,'検索エンジン'),(95,1,'來源'),(95,2,'推荐'),(95,3,'推荐'),(95,4,'推荐'),(96,1,'搜尋'),(96,2,'搜索'),(96,3,'搜索'),(96,4,'搜索'),(97,1,'搜尋'),(97,2,'搜索'),(97,3,'搜索'),(97,4,'搜索'),(98,1,'標籤'),(98,2,'标签'),(98,3,'标签'),(98,4,'标签'),(99,1,'進階設定'),(99,2,'高级参数'),(99,3,'高级参数'),(99,4,'高级参数'),(100,1,'資訊'),(100,2,'信息'),(100,3,'信息'),(100,4,'信息'),(101,1,'效能'),(101,2,'性能'),(101,3,'性能'),(101,4,'性能'),(102,1,'管理'),(102,2,'行政'),(102,3,'行政'),(102,4,'行政'),(103,1,'電子信箱'),(103,2,'邮箱'),(103,3,'邮箱'),(103,4,'邮箱'),(104,1,'匯入'),(104,2,'上传'),(104,3,'上传'),(104,4,'上传'),(105,1,'團隊'),(105,2,'雇员'),(105,3,'雇员'),(105,4,'雇员'),(106,1,'員工'),(106,2,'雇员'),(106,3,'雇员'),(106,4,'雇员'),(107,1,'設定檔'),(107,2,'职位'),(107,3,'职位'),(107,4,'职位'),(108,1,'權限'),(108,2,'权限'),(108,3,'权限'),(108,4,'权限'),(109,1,'資料庫'),(109,2,'数据库'),(109,3,'数据库'),(109,4,'数据库'),(110,1,'SQL管理器'),(110,2,'SQL Manager'),(110,3,'SQL管理器'),(110,4,'SQL マネージャ'),(111,1,'數據庫備份'),(111,2,'数据库备份'),(111,3,'数据库备份'),(111,4,'数据库备份'),(112,1,'記錄'),(112,2,'日志'),(112,3,'日志'),(112,4,'日志'),(113,1,'網路服務'),(113,2,'Web服务'),(113,3,'Web服务'),(113,4,'Web服务'),(114,1,'Multiboutique'),(114,2,'Multistore'),(114,3,'Multiboutique'),(114,4,'マルチストア'),(115,1,'Multiboutique'),(115,2,'Multistore'),(115,3,'Multiboutique'),(115,4,'マルチストア'),(116,1,'快速連結'),(116,2,'快速通道'),(116,3,'快速通道'),(116,4,'快速通道'),(117,1,'更多'),(117,2,'More'),(117,3,'更多'),(117,4,'もっと'),(119,1,'Dashgoals'),(119,2,'Dashgoals'),(119,3,'Dashgoals'),(119,4,'Dashgoals'),(120,1,'Link Widget'),(120,2,'Link Widget'),(120,3,'Link Widget'),(120,4,'Link Widget'),(121,1,'佈景 ＆ Logo'),(121,2,'Theme & Logo'),(121,3,'Theme & Logo'),(121,4,'テーマとロゴ'),(122,1,'Homepage Configuration'),(122,2,'Homepage Configuration'),(122,3,'Homepage Configuration'),(122,4,'Homepage Configuration'),(123,1,'Advanced Customization'),(123,2,'Advanced Customization'),(123,3,'Advanced Customization'),(123,4,'Advanced Customization'),(128,1,'Smilepaypalmboxc2cup'),(128,2,'Smilepaypalmboxc2cup'),(128,3,'Smilepaypalmboxc2cup'),(128,4,'Smilepaypalmboxc2cup'),(129,1,'SmilepayEzcatup'),(129,2,'SmilepayEzcatup'),(129,3,'SmilepayEzcatup'),(129,4,'SmilepayEzcatup'),(130,1,'SmilepayEzcat'),(130,2,'SmilepayEzcat'),(130,3,'SmilepayEzcat'),(130,4,'SmilepayEzcat');
/*!40000 ALTER TABLE `ps_tab_lang_bak` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_tab_module_preference`
--

DROP TABLE IF EXISTS `ps_tab_module_preference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_tab_module_preference` (
  `id_tab_module_preference` int(11) NOT NULL AUTO_INCREMENT,
  `id_employee` int(11) NOT NULL,
  `id_tab` int(11) NOT NULL,
  `module` varchar(255) NOT NULL,
  PRIMARY KEY (`id_tab_module_preference`),
  UNIQUE KEY `employee_module` (`id_employee`,`id_tab`,`module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_tab_module_preference`
--

LOCK TABLES `ps_tab_module_preference` WRITE;
/*!40000 ALTER TABLE `ps_tab_module_preference` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_tab_module_preference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_tag`
--

DROP TABLE IF EXISTS `ps_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_tag` (
  `id_tag` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id_tag`),
  KEY `tag_name` (`name`),
  KEY `id_lang` (`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_tag`
--

LOCK TABLES `ps_tag` WRITE;
/*!40000 ALTER TABLE `ps_tag` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_tag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_tag_count`
--

DROP TABLE IF EXISTS `ps_tag_count`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_tag_count` (
  `id_group` int(10) unsigned NOT NULL DEFAULT '0',
  `id_tag` int(10) unsigned NOT NULL DEFAULT '0',
  `id_lang` int(10) unsigned NOT NULL DEFAULT '0',
  `id_shop` int(11) unsigned NOT NULL DEFAULT '0',
  `counter` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_group`,`id_tag`),
  KEY `id_group` (`id_group`,`id_lang`,`id_shop`,`counter`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_tag_count`
--

LOCK TABLES `ps_tag_count` WRITE;
/*!40000 ALTER TABLE `ps_tag_count` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_tag_count` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_tax`
--

DROP TABLE IF EXISTS `ps_tax`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_tax` (
  `id_tax` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rate` decimal(10,3) NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_tax`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_tax`
--

LOCK TABLES `ps_tax` WRITE;
/*!40000 ALTER TABLE `ps_tax` DISABLE KEYS */;
INSERT INTO `ps_tax` VALUES (1,5.000,1,0),(2,5.000,1,0),(3,4.000,1,0),(4,0.000,1,0),(5,6.600,1,0),(6,6.000,1,0),(7,8.250,1,0),(8,2.900,1,0),(9,0.000,1,0),(10,0.000,1,0),(11,6.000,1,0),(12,4.000,1,0),(13,4.000,1,0),(14,6.000,1,0),(15,6.250,1,0),(16,7.000,1,0),(17,6.000,1,0),(18,5.300,1,0),(19,6.000,1,0),(20,4.000,1,0),(21,5.000,1,0),(22,6.000,1,0),(23,6.250,1,0),(24,6.000,1,0),(25,6.875,1,0),(26,7.000,1,0),(27,4.225,1,0),(28,0.000,1,0),(29,5.500,1,0),(30,6.850,1,0),(31,0.000,1,0),(32,7.000,1,0),(33,5.125,1,0),(34,4.000,1,0),(35,5.500,1,0),(36,5.000,1,0),(37,5.500,1,0),(38,4.500,1,0),(39,0.000,1,0),(40,6.000,1,0),(41,7.000,1,0),(42,6.000,1,0),(43,4.000,1,0),(44,7.000,1,0),(45,6.250,1,0),(46,4.750,1,0),(47,6.000,1,0),(48,4.000,1,0),(49,6.500,1,0),(50,6.000,1,0),(51,5.000,1,0),(52,4.000,1,0),(53,5.500,1,0),(54,6.000,1,0);
/*!40000 ALTER TABLE `ps_tax` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_tax_lang`
--

DROP TABLE IF EXISTS `ps_tax_lang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_tax_lang` (
  `id_tax` int(10) unsigned NOT NULL,
  `id_lang` int(10) unsigned NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id_tax`,`id_lang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_tax_lang`
--

LOCK TABLES `ps_tax_lang` WRITE;
/*!40000 ALTER TABLE `ps_tax_lang` DISABLE KEYS */;
INSERT INTO `ps_tax_lang` VALUES (1,1,'VAT TW 5%'),(1,2,'VAT TW 5%'),(1,3,'VAT TW 5%'),(1,4,'VAT TW 5%'),(2,1,'消費税 JP 5%'),(2,2,'消費税 JP 5%'),(2,3,'消費税 JP 5%'),(2,4,'消費税 JP 5%'),(3,1,'Sales-taxes US-AL 4%'),(3,2,'Sales-taxes US-AL 4%'),(3,3,'Sales-taxes US-AL 4%'),(3,4,'Sales-taxes US-AL 4%'),(4,1,'Sales-taxes US-AK 0%'),(4,2,'Sales-taxes US-AK 0%'),(4,3,'Sales-taxes US-AK 0%'),(4,4,'Sales-taxes US-AK 0%'),(5,1,'Sales-taxes US-AZ 6.6%'),(5,2,'Sales-taxes US-AZ 6.6%'),(5,3,'Sales-taxes US-AZ 6.6%'),(5,4,'Sales-taxes US-AZ 6.6%'),(6,1,'Sales-taxes US-AR 6%'),(6,2,'Sales-taxes US-AR 6%'),(6,3,'Sales-taxes US-AR 6%'),(6,4,'Sales-taxes US-AR 6%'),(7,1,'Sales-taxes US-CA 8.25%'),(7,2,'Sales-taxes US-CA 8.25%'),(7,3,'Sales-taxes US-CA 8.25%'),(7,4,'Sales-taxes US-CA 8.25%'),(8,1,'Sales-taxes US-CO 2.9%'),(8,2,'Sales-taxes US-CO 2.9%'),(8,3,'Sales-taxes US-CO 2.9%'),(8,4,'Sales-taxes US-CO 2.9%'),(9,1,'Sales-taxes US-CT 0%'),(9,2,'Sales-taxes US-CT 0%'),(9,3,'Sales-taxes US-CT 0%'),(9,4,'Sales-taxes US-CT 0%'),(10,1,'Sales-taxes US-DE 0%'),(10,2,'Sales-taxes US-DE 0%'),(10,3,'Sales-taxes US-DE 0%'),(10,4,'Sales-taxes US-DE 0%'),(11,1,'Sales-taxes US-FL 6%'),(11,2,'Sales-taxes US-FL 6%'),(11,3,'Sales-taxes US-FL 6%'),(11,4,'Sales-taxes US-FL 6%'),(12,1,'Sales-taxes US-GA 4%'),(12,2,'Sales-taxes US-GA 4%'),(12,3,'Sales-taxes US-GA 4%'),(12,4,'Sales-taxes US-GA 4%'),(13,1,'Sales-taxes US-HI 4%'),(13,2,'Sales-taxes US-HI 4%'),(13,3,'Sales-taxes US-HI 4%'),(13,4,'Sales-taxes US-HI 4%'),(14,1,'Sales-taxes US-ID 6%'),(14,2,'Sales-taxes US-ID 6%'),(14,3,'Sales-taxes US-ID 6%'),(14,4,'Sales-taxes US-ID 6%'),(15,1,'Sales-taxes US-IL 6.25%'),(15,2,'Sales-taxes US-IL 6.25%'),(15,3,'Sales-taxes US-IL 6.25%'),(15,4,'Sales-taxes US-IL 6.25%'),(16,1,'Sales-taxes US-IN 7%'),(16,2,'Sales-taxes US-IN 7%'),(16,3,'Sales-taxes US-IN 7%'),(16,4,'Sales-taxes US-IN 7%'),(17,1,'Sales-taxes US-IA 6%'),(17,2,'Sales-taxes US-IA 6%'),(17,3,'Sales-taxes US-IA 6%'),(17,4,'Sales-taxes US-IA 6%'),(18,1,'Sales-taxes US-KS 5.3%'),(18,2,'Sales-taxes US-KS 5.3%'),(18,3,'Sales-taxes US-KS 5.3%'),(18,4,'Sales-taxes US-KS 5.3%'),(19,1,'Sales-taxes US-KY 6%'),(19,2,'Sales-taxes US-KY 6%'),(19,3,'Sales-taxes US-KY 6%'),(19,4,'Sales-taxes US-KY 6%'),(20,1,'Sales-taxes US-LA 4%'),(20,2,'Sales-taxes US-LA 4%'),(20,3,'Sales-taxes US-LA 4%'),(20,4,'Sales-taxes US-LA 4%'),(21,1,'Sales-taxes US-ME 5%'),(21,2,'Sales-taxes US-ME 5%'),(21,3,'Sales-taxes US-ME 5%'),(21,4,'Sales-taxes US-ME 5%'),(22,1,'Sales-taxes US-MD 6%'),(22,2,'Sales-taxes US-MD 6%'),(22,3,'Sales-taxes US-MD 6%'),(22,4,'Sales-taxes US-MD 6%'),(23,1,'Sales-taxes US-MA 6.25%'),(23,2,'Sales-taxes US-MA 6.25%'),(23,3,'Sales-taxes US-MA 6.25%'),(23,4,'Sales-taxes US-MA 6.25%'),(24,1,'Sales-taxes US-MI 6%'),(24,2,'Sales-taxes US-MI 6%'),(24,3,'Sales-taxes US-MI 6%'),(24,4,'Sales-taxes US-MI 6%'),(25,1,'Sales-taxes US-MN 6.875%'),(25,2,'Sales-taxes US-MN 6.875%'),(25,3,'Sales-taxes US-MN 6.875%'),(25,4,'Sales-taxes US-MN 6.875%'),(26,1,'Sales-taxes US-MS 7%'),(26,2,'Sales-taxes US-MS 7%'),(26,3,'Sales-taxes US-MS 7%'),(26,4,'Sales-taxes US-MS 7%'),(27,1,'Sales-taxes US-MO 4.225%'),(27,2,'Sales-taxes US-MO 4.225%'),(27,3,'Sales-taxes US-MO 4.225%'),(27,4,'Sales-taxes US-MO 4.225%'),(28,1,'Sales-taxes US-MT 0.0%'),(28,2,'Sales-taxes US-MT 0.0%'),(28,3,'Sales-taxes US-MT 0.0%'),(28,4,'Sales-taxes US-MT 0.0%'),(29,1,'Sales-taxes US-NE 5.5%'),(29,2,'Sales-taxes US-NE 5.5%'),(29,3,'Sales-taxes US-NE 5.5%'),(29,4,'Sales-taxes US-NE 5.5%'),(30,1,'Sales-taxes US-NV 6.85%'),(30,2,'Sales-taxes US-NV 6.85%'),(30,3,'Sales-taxes US-NV 6.85%'),(30,4,'Sales-taxes US-NV 6.85%'),(31,1,'Sales-taxes US-NH 0%'),(31,2,'Sales-taxes US-NH 0%'),(31,3,'Sales-taxes US-NH 0%'),(31,4,'Sales-taxes US-NH 0%'),(32,1,'Sales-taxes US-NJ 7%'),(32,2,'Sales-taxes US-NJ 7%'),(32,3,'Sales-taxes US-NJ 7%'),(32,4,'Sales-taxes US-NJ 7%'),(33,1,'Sales-taxes US-NM 5.125%'),(33,2,'Sales-taxes US-NM 5.125%'),(33,3,'Sales-taxes US-NM 5.125%'),(33,4,'Sales-taxes US-NM 5.125%'),(34,1,'Sales-taxes US-NY 4%'),(34,2,'Sales-taxes US-NY 4%'),(34,3,'Sales-taxes US-NY 4%'),(34,4,'Sales-taxes US-NY 4%'),(35,1,'Sales-taxes US-NC 5.5%'),(35,2,'Sales-taxes US-NC 5.5%'),(35,3,'Sales-taxes US-NC 5.5%'),(35,4,'Sales-taxes US-NC 5.5%'),(36,1,'Sales-taxes US-ND 5%'),(36,2,'Sales-taxes US-ND 5%'),(36,3,'Sales-taxes US-ND 5%'),(36,4,'Sales-taxes US-ND 5%'),(37,1,'Sales-taxes US-OH 5.5%'),(37,2,'Sales-taxes US-OH 5.5%'),(37,3,'Sales-taxes US-OH 5.5%'),(37,4,'Sales-taxes US-OH 5.5%'),(38,1,'Sales-taxes US-OK 4.5%'),(38,2,'Sales-taxes US-OK 4.5%'),(38,3,'Sales-taxes US-OK 4.5%'),(38,4,'Sales-taxes US-OK 4.5%'),(39,1,'Sales-taxes US-OR 0%'),(39,2,'Sales-taxes US-OR 0%'),(39,3,'Sales-taxes US-OR 0%'),(39,4,'Sales-taxes US-OR 0%'),(40,1,'Sales-taxes US-PA 6%'),(40,2,'Sales-taxes US-PA 6%'),(40,3,'Sales-taxes US-PA 6%'),(40,4,'Sales-taxes US-PA 6%'),(41,1,'Sales-taxes US-RI 7%'),(41,2,'Sales-taxes US-RI 7%'),(41,3,'Sales-taxes US-RI 7%'),(41,4,'Sales-taxes US-RI 7%'),(42,1,'Sales-taxes US-SC 6%'),(42,2,'Sales-taxes US-SC 6%'),(42,3,'Sales-taxes US-SC 6%'),(42,4,'Sales-taxes US-SC 6%'),(43,1,'Sales-taxes US-SD 4%'),(43,2,'Sales-taxes US-SD 4%'),(43,3,'Sales-taxes US-SD 4%'),(43,4,'Sales-taxes US-SD 4%'),(44,1,'Sales-taxes US-TN 7%'),(44,2,'Sales-taxes US-TN 7%'),(44,3,'Sales-taxes US-TN 7%'),(44,4,'Sales-taxes US-TN 7%'),(45,1,'Sales-taxes US-TX 6.25%'),(45,2,'Sales-taxes US-TX 6.25%'),(45,3,'Sales-taxes US-TX 6.25%'),(45,4,'Sales-taxes US-TX 6.25%'),(46,1,'Sales-taxes US-UT 4.75%'),(46,2,'Sales-taxes US-UT 4.75%'),(46,3,'Sales-taxes US-UT 4.75%'),(46,4,'Sales-taxes US-UT 4.75%'),(47,1,'Sales-taxes US-VT 6%'),(47,2,'Sales-taxes US-VT 6%'),(47,3,'Sales-taxes US-VT 6%'),(47,4,'Sales-taxes US-VT 6%'),(48,1,'Sales-taxes US-VA 4%'),(48,2,'Sales-taxes US-VA 4%'),(48,3,'Sales-taxes US-VA 4%'),(48,4,'Sales-taxes US-VA 4%'),(49,1,'Sales-taxes US-WA 6.5%'),(49,2,'Sales-taxes US-WA 6.5%'),(49,3,'Sales-taxes US-WA 6.5%'),(49,4,'Sales-taxes US-WA 6.5%'),(50,1,'Sales-taxes US-WV 6%'),(50,2,'Sales-taxes US-WV 6%'),(50,3,'Sales-taxes US-WV 6%'),(50,4,'Sales-taxes US-WV 6%'),(51,1,'Sales-taxes US-WI 5%'),(51,2,'Sales-taxes US-WI 5%'),(51,3,'Sales-taxes US-WI 5%'),(51,4,'Sales-taxes US-WI 5%'),(52,1,'Sales-taxes US-WY 4%'),(52,2,'Sales-taxes US-WY 4%'),(52,3,'Sales-taxes US-WY 4%'),(52,4,'Sales-taxes US-WY 4%'),(53,1,'Sales-taxes US-PR 5.5%'),(53,2,'Sales-taxes US-PR 5.5%'),(53,3,'Sales-taxes US-PR 5.5%'),(53,4,'Sales-taxes US-PR 5.5%'),(54,1,'Sales-taxes US-DC 6%'),(54,2,'Sales-taxes US-DC 6%'),(54,3,'Sales-taxes US-DC 6%'),(54,4,'Sales-taxes US-DC 6%');
/*!40000 ALTER TABLE `ps_tax_lang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_tax_rule`
--

DROP TABLE IF EXISTS `ps_tax_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_tax_rule` (
  `id_tax_rule` int(11) NOT NULL AUTO_INCREMENT,
  `id_tax_rules_group` int(11) NOT NULL,
  `id_country` int(11) NOT NULL,
  `id_state` int(11) NOT NULL,
  `zipcode_from` varchar(12) NOT NULL,
  `zipcode_to` varchar(12) NOT NULL,
  `id_tax` int(11) NOT NULL,
  `behavior` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id_tax_rule`),
  KEY `id_tax_rules_group` (`id_tax_rules_group`),
  KEY `id_tax` (`id_tax`),
  KEY `category_getproducts` (`id_tax_rules_group`,`id_country`,`id_state`,`zipcode_from`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_tax_rule`
--

LOCK TABLES `ps_tax_rule` WRITE;
/*!40000 ALTER TABLE `ps_tax_rule` DISABLE KEYS */;
INSERT INTO `ps_tax_rule` VALUES (1,1,203,0,'0','0',1,0,''),(2,2,11,0,'0','0',2,0,''),(3,3,21,4,'0','0',3,1,''),(4,4,21,5,'0','0',4,1,''),(5,5,21,6,'0','0',5,1,''),(6,6,21,7,'0','0',6,1,''),(7,7,21,8,'0','0',7,1,''),(8,8,21,9,'0','0',8,1,''),(9,9,21,10,'0','0',9,1,''),(10,10,21,11,'0','0',10,1,''),(11,11,21,12,'0','0',11,1,''),(12,12,21,13,'0','0',12,1,''),(13,13,21,14,'0','0',13,1,''),(14,14,21,15,'0','0',14,1,''),(15,15,21,16,'0','0',15,1,''),(16,16,21,17,'0','0',16,1,''),(17,17,21,18,'0','0',17,1,''),(18,18,21,19,'0','0',18,1,''),(19,19,21,20,'0','0',19,1,''),(20,20,21,21,'0','0',20,1,''),(21,21,21,22,'0','0',21,1,''),(22,22,21,23,'0','0',22,1,''),(23,23,21,24,'0','0',23,1,''),(24,24,21,25,'0','0',24,1,''),(25,25,21,26,'0','0',25,1,''),(26,26,21,27,'0','0',26,1,''),(27,27,21,28,'0','0',27,1,''),(28,28,21,29,'0','0',28,1,''),(29,29,21,30,'0','0',29,1,''),(30,30,21,31,'0','0',30,1,''),(31,31,21,32,'0','0',31,1,''),(32,32,21,33,'0','0',32,1,''),(33,33,21,34,'0','0',33,1,''),(34,34,21,35,'0','0',34,1,''),(35,35,21,36,'0','0',35,1,''),(36,36,21,37,'0','0',36,1,''),(37,37,21,38,'0','0',37,1,''),(38,38,21,39,'0','0',38,1,''),(39,39,21,40,'0','0',39,1,''),(40,40,21,41,'0','0',40,1,''),(41,41,21,42,'0','0',41,1,''),(42,42,21,43,'0','0',42,1,''),(43,43,21,44,'0','0',43,1,''),(44,44,21,45,'0','0',44,1,''),(45,45,21,46,'0','0',45,1,''),(46,46,21,47,'0','0',46,1,''),(47,47,21,48,'0','0',47,1,''),(48,48,21,49,'0','0',48,1,''),(49,49,21,50,'0','0',49,1,''),(50,50,21,51,'0','0',50,1,''),(51,51,21,52,'0','0',51,1,''),(52,52,21,53,'0','0',52,1,''),(53,53,21,54,'0','0',53,1,''),(54,54,21,56,'0','0',54,1,'');
/*!40000 ALTER TABLE `ps_tax_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_tax_rules_group`
--

DROP TABLE IF EXISTS `ps_tax_rules_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_tax_rules_group` (
  `id_tax_rules_group` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `active` int(11) NOT NULL,
  `deleted` tinyint(1) unsigned NOT NULL,
  `date_add` datetime NOT NULL,
  `date_upd` datetime NOT NULL,
  PRIMARY KEY (`id_tax_rules_group`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_tax_rules_group`
--

LOCK TABLES `ps_tax_rules_group` WRITE;
/*!40000 ALTER TABLE `ps_tax_rules_group` DISABLE KEYS */;
INSERT INTO `ps_tax_rules_group` VALUES (1,'TW Standard Rate (5%)',1,0,'2018-07-11 11:57:38','2018-07-11 11:57:38'),(2,'JP Standard Rate (5%)',1,0,'2018-07-11 22:15:57','2018-07-11 22:15:57'),(3,'US-AL Rate (4%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(4,'US-AK Rate (0%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(5,'US-AZ Rate (6.6%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(6,'US-AR Rate (6%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(7,'US-CA Rate (8.25%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(8,'US-CO Rate (2.9%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(9,'US-CT Rate (0%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(10,'US-DE Rate (0%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(11,'US-FL Rate (6%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(12,'US-GA Rate (4%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(13,'US-HI Rate (4%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(14,'US-ID Rate (6%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(15,'US-IL Rate (6.25%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(16,'US-IN Rate (7%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(17,'US-IA Rate (6%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(18,'US-KS Rate (5.3%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(19,'US-KY Rate (6%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(20,'US-LA Rate (4%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(21,'US-ME Rate (5%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(22,'US-MD Rate (6%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(23,'US-MA Rate (6.25%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(24,'US-MI Rate (6%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(25,'US-MN Rate (6.875%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(26,'US-MS Rate (7%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(27,'US-MO Rate (4.225%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(28,'US-MT Rate (0%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(29,'US-NE Rate (5.5%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(30,'US-NV Rate (6.85%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(31,'US-NH Rate (0%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(32,'US-NJ Rate (7%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(33,'US-NM Rate (5.125%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(34,'US-NY Rate (4%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(35,'US-NC Rate (5.5%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(36,'US-ND Rate (5%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(37,'US-OH Rate (5.5%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(38,'US-OK Rate (4.5%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(39,'US-OR Rate (0%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(40,'US-PA Rate (6%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(41,'US-RI Rate (7%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(42,'US-SC Rate (6%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(43,'US-SD Rate (4%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(44,'US-TN Rate (7%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(45,'US-TX Rate (6.25%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(46,'US-UT Rate (4.75%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(47,'US-VT Rate (6%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(48,'US-VA Rate (4%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(49,'US-WA Rate (6.5%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(50,'US-WV Rate (6%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(51,'US-WI Rate (5%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(52,'US-WY Rate (4%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(53,'US-PR Rate (5.5%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12'),(54,'US-DC Rate (6%)',1,0,'2018-07-13 14:32:12','2018-07-13 14:32:12');
/*!40000 ALTER TABLE `ps_tax_rules_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_tax_rules_group_shop`
--

DROP TABLE IF EXISTS `ps_tax_rules_group_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_tax_rules_group_shop` (
  `id_tax_rules_group` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_tax_rules_group`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_tax_rules_group_shop`
--

LOCK TABLES `ps_tax_rules_group_shop` WRITE;
/*!40000 ALTER TABLE `ps_tax_rules_group_shop` DISABLE KEYS */;
INSERT INTO `ps_tax_rules_group_shop` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1),(37,1),(38,1),(39,1),(40,1),(41,1),(42,1),(43,1),(44,1),(45,1),(46,1),(47,1),(48,1),(49,1),(50,1),(51,1),(52,1),(53,1),(54,1);
/*!40000 ALTER TABLE `ps_tax_rules_group_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_timezone`
--

DROP TABLE IF EXISTS `ps_timezone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_timezone` (
  `id_timezone` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id_timezone`)
) ENGINE=InnoDB AUTO_INCREMENT=561 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_timezone`
--

LOCK TABLES `ps_timezone` WRITE;
/*!40000 ALTER TABLE `ps_timezone` DISABLE KEYS */;
INSERT INTO `ps_timezone` VALUES (1,'Africa/Abidjan'),(2,'Africa/Accra'),(3,'Africa/Addis_Ababa'),(4,'Africa/Algiers'),(5,'Africa/Asmara'),(6,'Africa/Asmera'),(7,'Africa/Bamako'),(8,'Africa/Bangui'),(9,'Africa/Banjul'),(10,'Africa/Bissau'),(11,'Africa/Blantyre'),(12,'Africa/Brazzaville'),(13,'Africa/Bujumbura'),(14,'Africa/Cairo'),(15,'Africa/Casablanca'),(16,'Africa/Ceuta'),(17,'Africa/Conakry'),(18,'Africa/Dakar'),(19,'Africa/Dar_es_Salaam'),(20,'Africa/Djibouti'),(21,'Africa/Douala'),(22,'Africa/El_Aaiun'),(23,'Africa/Freetown'),(24,'Africa/Gaborone'),(25,'Africa/Harare'),(26,'Africa/Johannesburg'),(27,'Africa/Kampala'),(28,'Africa/Khartoum'),(29,'Africa/Kigali'),(30,'Africa/Kinshasa'),(31,'Africa/Lagos'),(32,'Africa/Libreville'),(33,'Africa/Lome'),(34,'Africa/Luanda'),(35,'Africa/Lubumbashi'),(36,'Africa/Lusaka'),(37,'Africa/Malabo'),(38,'Africa/Maputo'),(39,'Africa/Maseru'),(40,'Africa/Mbabane'),(41,'Africa/Mogadishu'),(42,'Africa/Monrovia'),(43,'Africa/Nairobi'),(44,'Africa/Ndjamena'),(45,'Africa/Niamey'),(46,'Africa/Nouakchott'),(47,'Africa/Ouagadougou'),(48,'Africa/Porto-Novo'),(49,'Africa/Sao_Tome'),(50,'Africa/Timbuktu'),(51,'Africa/Tripoli'),(52,'Africa/Tunis'),(53,'Africa/Windhoek'),(54,'America/Adak'),(55,'America/Anchorage '),(56,'America/Anguilla'),(57,'America/Antigua'),(58,'America/Araguaina'),(59,'America/Argentina/Buenos_Aires'),(60,'America/Argentina/Catamarca'),(61,'America/Argentina/ComodRivadavia'),(62,'America/Argentina/Cordoba'),(63,'America/Argentina/Jujuy'),(64,'America/Argentina/La_Rioja'),(65,'America/Argentina/Mendoza'),(66,'America/Argentina/Rio_Gallegos'),(67,'America/Argentina/Salta'),(68,'America/Argentina/San_Juan'),(69,'America/Argentina/San_Luis'),(70,'America/Argentina/Tucuman'),(71,'America/Argentina/Ushuaia'),(72,'America/Aruba'),(73,'America/Asuncion'),(74,'America/Atikokan'),(75,'America/Atka'),(76,'America/Bahia'),(77,'America/Barbados'),(78,'America/Belem'),(79,'America/Belize'),(80,'America/Blanc-Sablon'),(81,'America/Boa_Vista'),(82,'America/Bogota'),(83,'America/Boise'),(84,'America/Buenos_Aires'),(85,'America/Cambridge_Bay'),(86,'America/Campo_Grande'),(87,'America/Cancun'),(88,'America/Caracas'),(89,'America/Catamarca'),(90,'America/Cayenne'),(91,'America/Cayman'),(92,'America/Chicago'),(93,'America/Chihuahua'),(94,'America/Coral_Harbour'),(95,'America/Cordoba'),(96,'America/Costa_Rica'),(97,'America/Cuiaba'),(98,'America/Curacao'),(99,'America/Danmarkshavn'),(100,'America/Dawson'),(101,'America/Dawson_Creek'),(102,'America/Denver'),(103,'America/Detroit'),(104,'America/Dominica'),(105,'America/Edmonton'),(106,'America/Eirunepe'),(107,'America/El_Salvador'),(108,'America/Ensenada'),(109,'America/Fort_Wayne'),(110,'America/Fortaleza'),(111,'America/Glace_Bay'),(112,'America/Godthab'),(113,'America/Goose_Bay'),(114,'America/Grand_Turk'),(115,'America/Grenada'),(116,'America/Guadeloupe'),(117,'America/Guatemala'),(118,'America/Guayaquil'),(119,'America/Guyana'),(120,'America/Halifax'),(121,'America/Havana'),(122,'America/Hermosillo'),(123,'America/Indiana/Indianapolis'),(124,'America/Indiana/Knox'),(125,'America/Indiana/Marengo'),(126,'America/Indiana/Petersburg'),(127,'America/Indiana/Tell_City'),(128,'America/Indiana/Vevay'),(129,'America/Indiana/Vincennes'),(130,'America/Indiana/Winamac'),(131,'America/Indianapolis'),(132,'America/Inuvik'),(133,'America/Iqaluit'),(134,'America/Jamaica'),(135,'America/Jujuy'),(136,'America/Juneau'),(137,'America/Kentucky/Louisville'),(138,'America/Kentucky/Monticello'),(139,'America/Knox_IN'),(140,'America/La_Paz'),(141,'America/Lima'),(142,'America/Los_Angeles'),(143,'America/Louisville'),(144,'America/Maceio'),(145,'America/Managua'),(146,'America/Manaus'),(147,'America/Marigot'),(148,'America/Martinique'),(149,'America/Mazatlan'),(150,'America/Mendoza'),(151,'America/Menominee'),(152,'America/Merida'),(153,'America/Mexico_City'),(154,'America/Miquelon'),(155,'America/Moncton'),(156,'America/Monterrey'),(157,'America/Montevideo'),(158,'America/Montreal'),(159,'America/Montserrat'),(160,'America/Nassau'),(161,'America/New_York'),(162,'America/Nipigon'),(163,'America/Nome'),(164,'America/Noronha'),(165,'America/North_Dakota/Center'),(166,'America/North_Dakota/New_Salem'),(167,'America/Panama'),(168,'America/Pangnirtung'),(169,'America/Paramaribo'),(170,'America/Phoenix'),(171,'America/Port-au-Prince'),(172,'America/Port_of_Spain'),(173,'America/Porto_Acre'),(174,'America/Porto_Velho'),(175,'America/Puerto_Rico'),(176,'America/Rainy_River'),(177,'America/Rankin_Inlet'),(178,'America/Recife'),(179,'America/Regina'),(180,'America/Resolute'),(181,'America/Rio_Branco'),(182,'America/Rosario'),(183,'America/Santarem'),(184,'America/Santiago'),(185,'America/Santo_Domingo'),(186,'America/Sao_Paulo'),(187,'America/Scoresbysund'),(188,'America/Shiprock'),(189,'America/St_Barthelemy'),(190,'America/St_Johns'),(191,'America/St_Kitts'),(192,'America/St_Lucia'),(193,'America/St_Thomas'),(194,'America/St_Vincent'),(195,'America/Swift_Current'),(196,'America/Tegucigalpa'),(197,'America/Thule'),(198,'America/Thunder_Bay'),(199,'America/Tijuana'),(200,'America/Toronto'),(201,'America/Tortola'),(202,'America/Vancouver'),(203,'America/Virgin'),(204,'America/Whitehorse'),(205,'America/Winnipeg'),(206,'America/Yakutat'),(207,'America/Yellowknife'),(208,'Antarctica/Casey'),(209,'Antarctica/Davis'),(210,'Antarctica/DumontDUrville'),(211,'Antarctica/Mawson'),(212,'Antarctica/McMurdo'),(213,'Antarctica/Palmer'),(214,'Antarctica/Rothera'),(215,'Antarctica/South_Pole'),(216,'Antarctica/Syowa'),(217,'Antarctica/Vostok'),(218,'Arctic/Longyearbyen'),(219,'Asia/Aden'),(220,'Asia/Almaty'),(221,'Asia/Amman'),(222,'Asia/Anadyr'),(223,'Asia/Aqtau'),(224,'Asia/Aqtobe'),(225,'Asia/Ashgabat'),(226,'Asia/Ashkhabad'),(227,'Asia/Baghdad'),(228,'Asia/Bahrain'),(229,'Asia/Baku'),(230,'Asia/Bangkok'),(231,'Asia/Beirut'),(232,'Asia/Bishkek'),(233,'Asia/Brunei'),(234,'Asia/Calcutta'),(235,'Asia/Choibalsan'),(236,'Asia/Chongqing'),(237,'Asia/Chungking'),(238,'Asia/Colombo'),(239,'Asia/Dacca'),(240,'Asia/Damascus'),(241,'Asia/Dhaka'),(242,'Asia/Dili'),(243,'Asia/Dubai'),(244,'Asia/Dushanbe'),(245,'Asia/Gaza'),(246,'Asia/Harbin'),(247,'Asia/Ho_Chi_Minh'),(248,'Asia/Hong_Kong'),(249,'Asia/Hovd'),(250,'Asia/Irkutsk'),(251,'Asia/Istanbul'),(252,'Asia/Jakarta'),(253,'Asia/Jayapura'),(254,'Asia/Jerusalem'),(255,'Asia/Kabul'),(256,'Asia/Kamchatka'),(257,'Asia/Karachi'),(258,'Asia/Kashgar'),(259,'Asia/Kathmandu'),(260,'Asia/Katmandu'),(261,'Asia/Kolkata'),(262,'Asia/Krasnoyarsk'),(263,'Asia/Kuala_Lumpur'),(264,'Asia/Kuching'),(265,'Asia/Kuwait'),(266,'Asia/Macao'),(267,'Asia/Macau'),(268,'Asia/Magadan'),(269,'Asia/Makassar'),(270,'Asia/Manila'),(271,'Asia/Muscat'),(272,'Asia/Nicosia'),(273,'Asia/Novosibirsk'),(274,'Asia/Omsk'),(275,'Asia/Oral'),(276,'Asia/Phnom_Penh'),(277,'Asia/Pontianak'),(278,'Asia/Pyongyang'),(279,'Asia/Qatar'),(280,'Asia/Qyzylorda'),(281,'Asia/Rangoon'),(282,'Asia/Riyadh'),(283,'Asia/Saigon'),(284,'Asia/Sakhalin'),(285,'Asia/Samarkand'),(286,'Asia/Seoul'),(287,'Asia/Shanghai'),(288,'Asia/Singapore'),(289,'Asia/Taipei'),(290,'Asia/Tashkent'),(291,'Asia/Tbilisi'),(292,'Asia/Tehran'),(293,'Asia/Tel_Aviv'),(294,'Asia/Thimbu'),(295,'Asia/Thimphu'),(296,'Asia/Tokyo'),(297,'Asia/Ujung_Pandang'),(298,'Asia/Ulaanbaatar'),(299,'Asia/Ulan_Bator'),(300,'Asia/Urumqi'),(301,'Asia/Vientiane'),(302,'Asia/Vladivostok'),(303,'Asia/Yakutsk'),(304,'Asia/Yekaterinburg'),(305,'Asia/Yerevan'),(306,'Atlantic/Azores'),(307,'Atlantic/Bermuda'),(308,'Atlantic/Canary'),(309,'Atlantic/Cape_Verde'),(310,'Atlantic/Faeroe'),(311,'Atlantic/Faroe'),(312,'Atlantic/Jan_Mayen'),(313,'Atlantic/Madeira'),(314,'Atlantic/Reykjavik'),(315,'Atlantic/South_Georgia'),(316,'Atlantic/St_Helena'),(317,'Atlantic/Stanley'),(318,'Australia/ACT'),(319,'Australia/Adelaide'),(320,'Australia/Brisbane'),(321,'Australia/Broken_Hill'),(322,'Australia/Canberra'),(323,'Australia/Currie'),(324,'Australia/Darwin'),(325,'Australia/Eucla'),(326,'Australia/Hobart'),(327,'Australia/LHI'),(328,'Australia/Lindeman'),(329,'Australia/Lord_Howe'),(330,'Australia/Melbourne'),(331,'Australia/North'),(332,'Australia/NSW'),(333,'Australia/Perth'),(334,'Australia/Queensland'),(335,'Australia/South'),(336,'Australia/Sydney'),(337,'Australia/Tasmania'),(338,'Australia/Victoria'),(339,'Australia/West'),(340,'Australia/Yancowinna'),(341,'Europe/Amsterdam'),(342,'Europe/Andorra'),(343,'Europe/Athens'),(344,'Europe/Belfast'),(345,'Europe/Belgrade'),(346,'Europe/Berlin'),(347,'Europe/Bratislava'),(348,'Europe/Brussels'),(349,'Europe/Bucharest'),(350,'Europe/Budapest'),(351,'Europe/Chisinau'),(352,'Europe/Copenhagen'),(353,'Europe/Dublin'),(354,'Europe/Gibraltar'),(355,'Europe/Guernsey'),(356,'Europe/Helsinki'),(357,'Europe/Isle_of_Man'),(358,'Europe/Istanbul'),(359,'Europe/Jersey'),(360,'Europe/Kaliningrad'),(361,'Europe/Kiev'),(362,'Europe/Lisbon'),(363,'Europe/Ljubljana'),(364,'Europe/London'),(365,'Europe/Luxembourg'),(366,'Europe/Madrid'),(367,'Europe/Malta'),(368,'Europe/Mariehamn'),(369,'Europe/Minsk'),(370,'Europe/Monaco'),(371,'Europe/Moscow'),(372,'Europe/Nicosia'),(373,'Europe/Oslo'),(374,'Europe/Paris'),(375,'Europe/Podgorica'),(376,'Europe/Prague'),(377,'Europe/Riga'),(378,'Europe/Rome'),(379,'Europe/Samara'),(380,'Europe/San_Marino'),(381,'Europe/Sarajevo'),(382,'Europe/Simferopol'),(383,'Europe/Skopje'),(384,'Europe/Sofia'),(385,'Europe/Stockholm'),(386,'Europe/Tallinn'),(387,'Europe/Tirane'),(388,'Europe/Tiraspol'),(389,'Europe/Uzhgorod'),(390,'Europe/Vaduz'),(391,'Europe/Vatican'),(392,'Europe/Vienna'),(393,'Europe/Vilnius'),(394,'Europe/Volgograd'),(395,'Europe/Warsaw'),(396,'Europe/Zagreb'),(397,'Europe/Zaporozhye'),(398,'Europe/Zurich'),(399,'Indian/Antananarivo'),(400,'Indian/Chagos'),(401,'Indian/Christmas'),(402,'Indian/Cocos'),(403,'Indian/Comoro'),(404,'Indian/Kerguelen'),(405,'Indian/Mahe'),(406,'Indian/Maldives'),(407,'Indian/Mauritius'),(408,'Indian/Mayotte'),(409,'Indian/Reunion'),(410,'Pacific/Apia'),(411,'Pacific/Auckland'),(412,'Pacific/Chatham'),(413,'Pacific/Easter'),(414,'Pacific/Efate'),(415,'Pacific/Enderbury'),(416,'Pacific/Fakaofo'),(417,'Pacific/Fiji'),(418,'Pacific/Funafuti'),(419,'Pacific/Galapagos'),(420,'Pacific/Gambier'),(421,'Pacific/Guadalcanal'),(422,'Pacific/Guam'),(423,'Pacific/Honolulu'),(424,'Pacific/Johnston'),(425,'Pacific/Kiritimati'),(426,'Pacific/Kosrae'),(427,'Pacific/Kwajalein'),(428,'Pacific/Majuro'),(429,'Pacific/Marquesas'),(430,'Pacific/Midway'),(431,'Pacific/Nauru'),(432,'Pacific/Niue'),(433,'Pacific/Norfolk'),(434,'Pacific/Noumea'),(435,'Pacific/Pago_Pago'),(436,'Pacific/Palau'),(437,'Pacific/Pitcairn'),(438,'Pacific/Ponape'),(439,'Pacific/Port_Moresby'),(440,'Pacific/Rarotonga'),(441,'Pacific/Saipan'),(442,'Pacific/Samoa'),(443,'Pacific/Tahiti'),(444,'Pacific/Tarawa'),(445,'Pacific/Tongatapu'),(446,'Pacific/Truk'),(447,'Pacific/Wake'),(448,'Pacific/Wallis'),(449,'Pacific/Yap'),(450,'Brazil/Acre'),(451,'Brazil/DeNoronha'),(452,'Brazil/East'),(453,'Brazil/West'),(454,'Canada/Atlantic'),(455,'Canada/Central'),(456,'Canada/East-Saskatchewan'),(457,'Canada/Eastern'),(458,'Canada/Mountain'),(459,'Canada/Newfoundland'),(460,'Canada/Pacific'),(461,'Canada/Saskatchewan'),(462,'Canada/Yukon'),(463,'CET'),(464,'Chile/Continental'),(465,'Chile/EasterIsland'),(466,'CST6CDT'),(467,'Cuba'),(468,'EET'),(469,'Egypt'),(470,'Eire'),(471,'EST'),(472,'EST5EDT'),(473,'Etc/GMT'),(474,'Etc/GMT+0'),(475,'Etc/GMT+1'),(476,'Etc/GMT+10'),(477,'Etc/GMT+11'),(478,'Etc/GMT+12'),(479,'Etc/GMT+2'),(480,'Etc/GMT+3'),(481,'Etc/GMT+4'),(482,'Etc/GMT+5'),(483,'Etc/GMT+6'),(484,'Etc/GMT+7'),(485,'Etc/GMT+8'),(486,'Etc/GMT+9'),(487,'Etc/GMT-0'),(488,'Etc/GMT-1'),(489,'Etc/GMT-10'),(490,'Etc/GMT-11'),(491,'Etc/GMT-12'),(492,'Etc/GMT-13'),(493,'Etc/GMT-14'),(494,'Etc/GMT-2'),(495,'Etc/GMT-3'),(496,'Etc/GMT-4'),(497,'Etc/GMT-5'),(498,'Etc/GMT-6'),(499,'Etc/GMT-7'),(500,'Etc/GMT-8'),(501,'Etc/GMT-9'),(502,'Etc/GMT0'),(503,'Etc/Greenwich'),(504,'Etc/UCT'),(505,'Etc/Universal'),(506,'Etc/UTC'),(507,'Etc/Zulu'),(508,'Factory'),(509,'GB'),(510,'GB-Eire'),(511,'GMT'),(512,'GMT+0'),(513,'GMT-0'),(514,'GMT0'),(515,'Greenwich'),(516,'Hongkong'),(517,'HST'),(518,'Iceland'),(519,'Iran'),(520,'Israel'),(521,'Jamaica'),(522,'Japan'),(523,'Kwajalein'),(524,'Libya'),(525,'MET'),(526,'Mexico/BajaNorte'),(527,'Mexico/BajaSur'),(528,'Mexico/General'),(529,'MST'),(530,'MST7MDT'),(531,'Navajo'),(532,'NZ'),(533,'NZ-CHAT'),(534,'Poland'),(535,'Portugal'),(536,'PRC'),(537,'PST8PDT'),(538,'ROC'),(539,'ROK'),(540,'Singapore'),(541,'Turkey'),(542,'UCT'),(543,'Universal'),(544,'US/Alaska'),(545,'US/Aleutian'),(546,'US/Arizona'),(547,'US/Central'),(548,'US/East-Indiana'),(549,'US/Eastern'),(550,'US/Hawaii'),(551,'US/Indiana-Starke'),(552,'US/Michigan'),(553,'US/Mountain'),(554,'US/Pacific'),(555,'US/Pacific-New'),(556,'US/Samoa'),(557,'UTC'),(558,'W-SU'),(559,'WET'),(560,'Zulu');
/*!40000 ALTER TABLE `ps_timezone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_translation`
--

DROP TABLE IF EXISTS `ps_translation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_translation` (
  `id_translation` int(11) NOT NULL AUTO_INCREMENT,
  `id_lang` int(11) NOT NULL,
  `key` text COLLATE utf8_unicode_ci NOT NULL,
  `translation` text COLLATE utf8_unicode_ci NOT NULL,
  `domain` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `theme` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id_translation`),
  KEY `IDX_ADEBEB36BA299860` (`id_lang`),
  KEY `key` (`domain`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_translation`
--

LOCK TABLES `ps_translation` WRITE;
/*!40000 ALTER TABLE `ps_translation` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_translation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_warehouse`
--

DROP TABLE IF EXISTS `ps_warehouse`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_warehouse` (
  `id_warehouse` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_currency` int(11) unsigned NOT NULL,
  `id_address` int(11) unsigned NOT NULL,
  `id_employee` int(11) unsigned NOT NULL,
  `reference` varchar(32) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `management_type` enum('WA','FIFO','LIFO') NOT NULL DEFAULT 'WA',
  `deleted` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_warehouse`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_warehouse`
--

LOCK TABLES `ps_warehouse` WRITE;
/*!40000 ALTER TABLE `ps_warehouse` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_warehouse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_warehouse_carrier`
--

DROP TABLE IF EXISTS `ps_warehouse_carrier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_warehouse_carrier` (
  `id_carrier` int(11) unsigned NOT NULL,
  `id_warehouse` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_warehouse`,`id_carrier`),
  KEY `id_warehouse` (`id_warehouse`),
  KEY `id_carrier` (`id_carrier`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_warehouse_carrier`
--

LOCK TABLES `ps_warehouse_carrier` WRITE;
/*!40000 ALTER TABLE `ps_warehouse_carrier` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_warehouse_carrier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_warehouse_product_location`
--

DROP TABLE IF EXISTS `ps_warehouse_product_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_warehouse_product_location` (
  `id_warehouse_product_location` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_product` int(11) unsigned NOT NULL,
  `id_product_attribute` int(11) unsigned NOT NULL,
  `id_warehouse` int(11) unsigned NOT NULL,
  `location` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id_warehouse_product_location`),
  UNIQUE KEY `id_product` (`id_product`,`id_product_attribute`,`id_warehouse`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_warehouse_product_location`
--

LOCK TABLES `ps_warehouse_product_location` WRITE;
/*!40000 ALTER TABLE `ps_warehouse_product_location` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_warehouse_product_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_warehouse_shop`
--

DROP TABLE IF EXISTS `ps_warehouse_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_warehouse_shop` (
  `id_shop` int(11) unsigned NOT NULL,
  `id_warehouse` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_warehouse`,`id_shop`),
  KEY `id_warehouse` (`id_warehouse`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_warehouse_shop`
--

LOCK TABLES `ps_warehouse_shop` WRITE;
/*!40000 ALTER TABLE `ps_warehouse_shop` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_warehouse_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_web_browser`
--

DROP TABLE IF EXISTS `ps_web_browser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_web_browser` (
  `id_web_browser` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id_web_browser`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_web_browser`
--

LOCK TABLES `ps_web_browser` WRITE;
/*!40000 ALTER TABLE `ps_web_browser` DISABLE KEYS */;
INSERT INTO `ps_web_browser` VALUES (1,'Safari'),(2,'Safari iPad'),(3,'Firefox'),(4,'Opera'),(5,'IE 6'),(6,'IE 7'),(7,'IE 8'),(8,'IE 9'),(9,'IE 10'),(10,'IE 11'),(11,'Chrome');
/*!40000 ALTER TABLE `ps_web_browser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_webservice_account`
--

DROP TABLE IF EXISTS `ps_webservice_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_webservice_account` (
  `id_webservice_account` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(32) NOT NULL,
  `description` text,
  `class_name` varchar(50) NOT NULL DEFAULT 'WebserviceRequest',
  `is_module` tinyint(2) NOT NULL DEFAULT '0',
  `module_name` varchar(50) DEFAULT NULL,
  `active` tinyint(2) NOT NULL,
  PRIMARY KEY (`id_webservice_account`),
  KEY `key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_webservice_account`
--

LOCK TABLES `ps_webservice_account` WRITE;
/*!40000 ALTER TABLE `ps_webservice_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_webservice_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_webservice_account_shop`
--

DROP TABLE IF EXISTS `ps_webservice_account_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_webservice_account_shop` (
  `id_webservice_account` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_webservice_account`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_webservice_account_shop`
--

LOCK TABLES `ps_webservice_account_shop` WRITE;
/*!40000 ALTER TABLE `ps_webservice_account_shop` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_webservice_account_shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_webservice_permission`
--

DROP TABLE IF EXISTS `ps_webservice_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_webservice_permission` (
  `id_webservice_permission` int(11) NOT NULL AUTO_INCREMENT,
  `resource` varchar(50) NOT NULL,
  `method` enum('GET','POST','PUT','DELETE','HEAD') NOT NULL,
  `id_webservice_account` int(11) NOT NULL,
  PRIMARY KEY (`id_webservice_permission`),
  UNIQUE KEY `resource_2` (`resource`,`method`,`id_webservice_account`),
  KEY `resource` (`resource`),
  KEY `method` (`method`),
  KEY `id_webservice_account` (`id_webservice_account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_webservice_permission`
--

LOCK TABLES `ps_webservice_permission` WRITE;
/*!40000 ALTER TABLE `ps_webservice_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `ps_webservice_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_zone`
--

DROP TABLE IF EXISTS `ps_zone`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_zone` (
  `id_zone` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_zone`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_zone`
--

LOCK TABLES `ps_zone` WRITE;
/*!40000 ALTER TABLE `ps_zone` DISABLE KEYS */;
INSERT INTO `ps_zone` VALUES (1,'Europe',1),(2,'North America',1),(3,'Asia',1),(4,'Africa',1),(5,'Oceania',1),(6,'South America',1),(7,'Europe (non-EU)',1),(8,'Central America/Antilla',1),(9,'台灣',1),(10,'日本',1),(11,'中國',1);
/*!40000 ALTER TABLE `ps_zone` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ps_zone_shop`
--

DROP TABLE IF EXISTS `ps_zone_shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ps_zone_shop` (
  `id_zone` int(11) unsigned NOT NULL,
  `id_shop` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id_zone`,`id_shop`),
  KEY `id_shop` (`id_shop`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ps_zone_shop`
--

LOCK TABLES `ps_zone_shop` WRITE;
/*!40000 ALTER TABLE `ps_zone_shop` DISABLE KEYS */;
INSERT INTO `ps_zone_shop` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(1,2),(2,2),(3,2),(4,2),(5,2),(6,2),(7,2),(8,2),(9,2),(10,2),(11,2),(1,3),(2,3),(3,3),(4,3),(5,3),(6,3),(7,3),(8,3),(9,3),(10,3),(11,3),(1,4),(2,4),(3,4),(4,4),(5,4),(6,4),(7,4),(8,4),(9,4),(10,4),(11,4);
/*!40000 ALTER TABLE `ps_zone_shop` ENABLE KEYS */;
UNLOCK TABLES;
/*!50112 SET @disable_bulk_load = IF (@is_rocksdb_supported, 'SET SESSION rocksdb_bulk_load = @old_rocksdb_bulk_load', 'SET @dummy_rocksdb_bulk_load = 0') */;
/*!50112 PREPARE s FROM @disable_bulk_load */;
/*!50112 EXECUTE s */;
/*!50112 DEALLOCATE PREPARE s */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-24 16:12:06
