UPDATE `ps_order_state` SET `send_email` = '0' WHERE `ps_order_state`.`id_order_state` = 1;
UPDATE `ps_order_state` SET `send_email` = '0' WHERE `ps_order_state`.`id_order_state` = 9;
UPDATE `ps_order_state` SET `send_email` = '0' WHERE `ps_order_state`.`id_order_state` = 10;

ALTER TABLE `ps_carrier_lang` ADD `email_extra_info` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '已出貨通知信補充資訊' AFTER `delay`;

UPDATE `ps_carrier_lang` SET `email_extra_info` = '預計2日送達指定門市，取件人需出示身份證件，讓門市人員核對無誤，方可取貨。' WHERE `ps_carrier_lang`.`id_carrier` = 103 AND `ps_carrier_lang`.`id_shop` = 1 AND `ps_carrier_lang`.`id_lang` = 1;