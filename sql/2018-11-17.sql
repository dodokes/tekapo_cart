INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(NULL, NULL, NULL, 'TC_GCP_KEY', '/var/bak/cdn_key.json', '2018-11-11 11:11:11', '2018-11-11 11:11:11');

INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(NULL, NULL, NULL, 'TC_GCP_BUCKET', 'tekapocart-cdn', '2018-11-11 11:11:11', '2018-11-11 11:11:11');

--------------------------------

INSERT INTO `ps_meta` (`id_meta`, `page`, `configurable`) VALUES (51, 'viewed-products', '1');
INSERT INTO `ps_meta_lang` (`id_meta`, `id_shop`, `id_lang`, `title`, `description`, `keywords`, `url_rewrite`) VALUES
('51', '1', '1', '瀏覽紀錄', '', '', 'viewed-products'),
('51', '1', '2', 'Viewed Products', '', '', 'viewed-products'),
('51', '1', '3', 'Viewed Products', '', '', 'viewed-products'),
('51', '1', '4', 'Viewed Products', '', '', 'viewed-products');

--

UPDATE `ps_configuration` SET `value` = '0' WHERE `ps_configuration`.`name` = 'PS_SHIPPING_HANDLING';
UPDATE `ps_configuration` SET `value` = '0' WHERE `ps_configuration`.`name` = 'PS_SHIPPING_FREE_PRICE';

UPDATE `ps_configuration` SET `value` = '110' WHERE `ps_configuration`.`name` = 'SMILEPAY_EZCATP_NORMAL_CARRIER_ID_REF';
UPDATE `ps_configuration` SET `value` = '111' WHERE `ps_configuration`.`name` = 'SMILEPAY_EZCATP_FRIDGE_CARRIER_ID_REF';
UPDATE `ps_configuration` SET `value` = '112' WHERE `ps_configuration`.`name` = 'SMILEPAY_EZCATP_FREEZE_CARRIER_ID_REF';

UPDATE `ps_delivery` SET `id_range_price` = '39', `id_range_weight` = NULL WHERE `ps_delivery`.`id_delivery` = 233;
UPDATE `ps_delivery` SET `id_range_price` = '40', `id_range_weight` = NULL WHERE `ps_delivery`.`id_delivery` = 234;
UPDATE `ps_delivery` SET `id_range_price` = '41', `id_range_weight` = NULL WHERE `ps_delivery`.`id_delivery` = 235;

UPDATE `ps_delivery` SET `id_range_price` = '42', `id_range_weight` = NULL WHERE `ps_delivery`.`id_delivery` = 236;
UPDATE `ps_delivery` SET `id_range_price` = '43', `id_range_weight` = NULL WHERE `ps_delivery`.`id_delivery` = 237;
UPDATE `ps_delivery` SET `id_range_price` = '44', `id_range_weight` = NULL WHERE `ps_delivery`.`id_delivery` = 238;

INSERT INTO `ps_hook_module` (`id_module`, `id_shop`, `id_hook`, `position`) VALUES
('57', '1', '13', '1'),
('57', '2', '13', '1'),
('57', '3', '13', '1'),
('57', '4', '13', '1');

INSERT INTO `ps_hook_module` (`id_module`, `id_shop`, `id_hook`, `position`) VALUES
('62', '1', '13', '1'),
('62', '2', '13', '1'),
('62', '3', '13', '1'),
('62', '4', '13', '1');

INSERT INTO `ps_hook_module` (`id_module`, `id_shop`, `id_hook`, `position`) VALUES
('58', '1', '13', '1'),
('58', '2', '13', '1'),
('58', '3', '13', '1'),
('58', '4', '13', '1');

-- 主打商品
INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(755, NULL, NULL, 'HOME_FEATURED_LABEL', NULL, NOW(), NOW());
INSERT INTO `ps_configuration_lang` (`id_configuration`, `id_lang`, `value`, `date_upd`) VALUES
('755', '1', '', NOW()),
('755', '2', '', NOW()),
('755', '3', '', NOW()),
('755', '4', '', NOW());
-- 熱銷商品
INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(756, NULL, NULL, 'PS_BLOCK_BESTSELLERS_LABEL', NULL, NOW(), NOW());
INSERT INTO `ps_configuration_lang` (`id_configuration`, `id_lang`, `value`, `date_upd`) VALUES
('756', '1', '熱銷商品', NOW()),
('756', '2', 'Best Sellers', NOW()),
('756', '3', 'Best Sellers', NOW()),
('756', '4', 'Best Sellers', NOW());
-- 新品上架
INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(757, NULL, NULL, 'PS_NB_LABEL', NULL, NOW(), NOW());
INSERT INTO `ps_configuration_lang` (`id_configuration`, `id_lang`, `value`, `date_upd`) VALUES
('757', '1', '新品上架', NOW()),
('757', '2', 'New Products', NOW()),
('757', '3', 'New Products', NOW()),
('757', '4', 'New Products', NOW());
-- 特價商品
INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(758, NULL, NULL, 'BLOCKSPECIALS_LABEL', NULL, NOW(), NOW());
INSERT INTO `ps_configuration_lang` (`id_configuration`, `id_lang`, `value`, `date_upd`) VALUES
('758', '1', '特價商品', NOW()),
('758', '2', 'Specials', NOW()),
('758', '3', 'Specials', NOW()),
('758', '4', 'Specials', NOW());


UPDATE `ps_configuration` SET `value` = '5' WHERE `ps_configuration`.`name` = 'PS_BLOCK_BESTSELLERS_TO_DISPLAY';

--------------------------------

DROP TABLE `ps_dateofdelivery_carrier_rule`;

DELETE FROM `ps_configuration` WHERE `ps_configuration`.`name` = 'DOD_DATE_FORMAT';
DELETE FROM `ps_configuration` WHERE `ps_configuration`.`name` = 'DOD_EXTRA_TIME_PREPARATION';
DELETE FROM `ps_configuration` WHERE `ps_configuration`.`name` = 'DOD_EXTRA_TIME_PRODUCT_OOS';
DELETE FROM `ps_configuration` WHERE `ps_configuration`.`name` = 'DOD_PREPARATION_SATURDAY';
DELETE FROM `ps_configuration` WHERE `ps_configuration`.`name` = 'DOD_PREPARATION_SUNDAY';

DELETE FROM `ps_module` WHERE `ps_module`.`id_module` = 56;

DELETE FROM `ps_hook_module` WHERE `ps_hook_module`.`id_module` = 56 AND `ps_hook_module`.`id_shop` = 1 AND `ps_hook_module`.`id_hook` = 36;
DELETE FROM `ps_hook_module` WHERE `ps_hook_module`.`id_module` = 56 AND `ps_hook_module`.`id_shop` = 2 AND `ps_hook_module`.`id_hook` = 36;
DELETE FROM `ps_hook_module` WHERE `ps_hook_module`.`id_module` = 56 AND `ps_hook_module`.`id_shop` = 3 AND `ps_hook_module`.`id_hook` = 36;
DELETE FROM `ps_hook_module` WHERE `ps_hook_module`.`id_module` = 56 AND `ps_hook_module`.`id_shop` = 4 AND `ps_hook_module`.`id_hook` = 36;
DELETE FROM `ps_hook_module` WHERE `ps_hook_module`.`id_module` = 56 AND `ps_hook_module`.`id_shop` = 1 AND `ps_hook_module`.`id_hook` = 62;
DELETE FROM `ps_hook_module` WHERE `ps_hook_module`.`id_module` = 56 AND `ps_hook_module`.`id_shop` = 2 AND `ps_hook_module`.`id_hook` = 62;
DELETE FROM `ps_hook_module` WHERE `ps_hook_module`.`id_module` = 56 AND `ps_hook_module`.`id_shop` = 3 AND `ps_hook_module`.`id_hook` = 62;
DELETE FROM `ps_hook_module` WHERE `ps_hook_module`.`id_module` = 56 AND `ps_hook_module`.`id_shop` = 4 AND `ps_hook_module`.`id_hook` = 62;
DELETE FROM `ps_hook_module` WHERE `ps_hook_module`.`id_module` = 56 AND `ps_hook_module`.`id_shop` = 1 AND `ps_hook_module`.`id_hook` = 71;
DELETE FROM `ps_hook_module` WHERE `ps_hook_module`.`id_module` = 56 AND `ps_hook_module`.`id_shop` = 2 AND `ps_hook_module`.`id_hook` = 71;
DELETE FROM `ps_hook_module` WHERE `ps_hook_module`.`id_module` = 56 AND `ps_hook_module`.`id_shop` = 3 AND `ps_hook_module`.`id_hook` = 71;
DELETE FROM `ps_hook_module` WHERE `ps_hook_module`.`id_module` = 56 AND `ps_hook_module`.`id_shop` = 4 AND `ps_hook_module`.`id_hook` = 71;
DELETE FROM `ps_hook_module` WHERE `ps_hook_module`.`id_module` = 56 AND `ps_hook_module`.`id_shop` = 1 AND `ps_hook_module`.`id_hook` = 73;
DELETE FROM `ps_hook_module` WHERE `ps_hook_module`.`id_module` = 56 AND `ps_hook_module`.`id_shop` = 2 AND `ps_hook_module`.`id_hook` = 73;
DELETE FROM `ps_hook_module` WHERE `ps_hook_module`.`id_module` = 56 AND `ps_hook_module`.`id_shop` = 3 AND `ps_hook_module`.`id_hook` = 73;
DELETE FROM `ps_hook_module` WHERE `ps_hook_module`.`id_module` = 56 AND `ps_hook_module`.`id_shop` = 4 AND `ps_hook_module`.`id_hook` = 73;

DELETE FROM `ps_module_group` WHERE `ps_module_group`.`id_module` = 56 AND `ps_module_group`.`id_shop` = 1 AND `ps_module_group`.`id_group` = 1;
DELETE FROM `ps_module_group` WHERE `ps_module_group`.`id_module` = 56 AND `ps_module_group`.`id_shop` = 1 AND `ps_module_group`.`id_group` = 2;
DELETE FROM `ps_module_group` WHERE `ps_module_group`.`id_module` = 56 AND `ps_module_group`.`id_shop` = 1 AND `ps_module_group`.`id_group` = 3;
DELETE FROM `ps_module_group` WHERE `ps_module_group`.`id_module` = 56 AND `ps_module_group`.`id_shop` = 2 AND `ps_module_group`.`id_group` = 1;
DELETE FROM `ps_module_group` WHERE `ps_module_group`.`id_module` = 56 AND `ps_module_group`.`id_shop` = 2 AND `ps_module_group`.`id_group` = 2;
DELETE FROM `ps_module_group` WHERE `ps_module_group`.`id_module` = 56 AND `ps_module_group`.`id_shop` = 2 AND `ps_module_group`.`id_group` = 3;
DELETE FROM `ps_module_group` WHERE `ps_module_group`.`id_module` = 56 AND `ps_module_group`.`id_shop` = 3 AND `ps_module_group`.`id_group` = 1;
DELETE FROM `ps_module_group` WHERE `ps_module_group`.`id_module` = 56 AND `ps_module_group`.`id_shop` = 3 AND `ps_module_group`.`id_group` = 2;
DELETE FROM `ps_module_group` WHERE `ps_module_group`.`id_module` = 56 AND `ps_module_group`.`id_shop` = 3 AND `ps_module_group`.`id_group` = 3;
DELETE FROM `ps_module_group` WHERE `ps_module_group`.`id_module` = 56 AND `ps_module_group`.`id_shop` = 4 AND `ps_module_group`.`id_group` = 1;
DELETE FROM `ps_module_group` WHERE `ps_module_group`.`id_module` = 56 AND `ps_module_group`.`id_shop` = 4 AND `ps_module_group`.`id_group` = 2;
DELETE FROM `ps_module_group` WHERE `ps_module_group`.`id_module` = 56 AND `ps_module_group`.`id_shop` = 4 AND `ps_module_group`.`id_group` = 3;

DELETE FROM `ps_module_shop` WHERE `ps_module_shop`.`id_module` = 56 AND `ps_module_shop`.`id_shop` = 1;
DELETE FROM `ps_module_shop` WHERE `ps_module_shop`.`id_module` = 56 AND `ps_module_shop`.`id_shop` = 2;
DELETE FROM `ps_module_shop` WHERE `ps_module_shop`.`id_module` = 56 AND `ps_module_shop`.`id_shop` = 3;
DELETE FROM `ps_module_shop` WHERE `ps_module_shop`.`id_module` = 56 AND `ps_module_shop`.`id_shop` = 4;

DELETE FROM `ps_authorization_role` WHERE `ps_authorization_role`.`id_authorization_role` = 705;
DELETE FROM `ps_authorization_role` WHERE `ps_authorization_role`.`id_authorization_role` = 708;
DELETE FROM `ps_authorization_role` WHERE `ps_authorization_role`.`id_authorization_role` = 706;
DELETE FROM `ps_authorization_role` WHERE `ps_authorization_role`.`id_authorization_role` = 707;

DELETE FROM `ps_module_access` WHERE `ps_module_access`.`id_profile` = 1 AND `ps_module_access`.`id_authorization_role` = 705;
DELETE FROM `ps_module_access` WHERE `ps_module_access`.`id_profile` = 1 AND `ps_module_access`.`id_authorization_role` = 706;
DELETE FROM `ps_module_access` WHERE `ps_module_access`.`id_profile` = 1 AND `ps_module_access`.`id_authorization_role` = 707;
DELETE FROM `ps_module_access` WHERE `ps_module_access`.`id_profile` = 1 AND `ps_module_access`.`id_authorization_role` = 708;

