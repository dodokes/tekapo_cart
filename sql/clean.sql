TRUNCATE TABLE `ps_connections`;
TRUNCATE TABLE `ps_connections_source`;
TRUNCATE TABLE `ps_emailsubscription`;
TRUNCATE TABLE `ps_log`;
TRUNCATE TABLE `ps_module_history`;
TRUNCATE TABLE `ps_address`;
TRUNCATE TABLE `ps_cart`;
TRUNCATE TABLE `ps_cart_product`;
TRUNCATE TABLE `ps_guest`;
TRUNCATE TABLE `ps_customer`;
TRUNCATE TABLE `ps_customer_group`;
TRUNCATE TABLE `ps_customer_message`;
TRUNCATE TABLE `ps_customer_thread`;
TRUNCATE TABLE `ps_mail`;
TRUNCATE TABLE `ps_order_carrier`;
TRUNCATE TABLE `ps_order_detail`;
TRUNCATE TABLE `ps_order_history`;
TRUNCATE TABLE `ps_orders`;
TRUNCATE TABLE `ps_order_cart_rule`;
TRUNCATE TABLE `ps_order_invoice`;
TRUNCATE TABLE `ps_order_invoice_payment`;
TRUNCATE TABLE `ps_order_payment`;
TRUNCATE TABLE `ps_sociallogin`;
TRUNCATE TABLE `ps_message`;
TRUNCATE TABLE `ps_smilepay_c2cup_table`;
TRUNCATE TABLE `ps_smilepay_csv_table`;
TRUNCATE TABLE `ps_smilepay_ezcat_table`;
TRUNCATE TABLE `ps_smilepay_ezcatup_table`;
TRUNCATE TABLE `ps_smilepay_palmboxc2cup_table`;
TRUNCATE TABLE `ps_stock_mvt`;
TRUNCATE TABLE `ps_fbmessenger_subscription`;
TRUNCATE TABLE `ps_fbmessenger_message`;
TRUNCATE TABLE `ps_gtm_client`;
TRUNCATE TABLE `ps_gtm_orders`;

--

UPDATE `ps_configuration` SET `value`=NULL WHERE `name`='SIMPLICITY_FB_APP_ID';

UPDATE `ps_configuration` SET `value`=NULL WHERE `name`='SIMPLICITY_TINYPNG_API_KEY_1';

UPDATE `ps_configuration` SET `value`=NULL WHERE `name`='SIMPLICITY_GTM_ID';
UPDATE `ps_configuration` SET `value`=NULL WHERE `name`='SIMPLICITY_GTM_GUA_ID';
UPDATE `ps_configuration` SET `value`=NULL WHERE `name`='SIMPLICITY_GTM_ADWORDS_ID';
UPDATE `ps_configuration` SET `value`=NULL WHERE `name`='SIMPLICITY_GTM_ADWORDS_LABEL';
UPDATE `ps_configuration` SET `value`=NULL WHERE `name`='SIMPLICITY_GTM_FACEBOOK_ID';

UPDATE `ps_configuration` SET `value`='e.tekapo@gmail.com' WHERE `name`='PS_MAIL_USER';
UPDATE `ps_configuration` SET `value`=NULL WHERE `name`='PS_MAIL_PASSWD';

UPDATE `ps_configuration` SET `value`='admin@example.com' WHERE `name`='PS_SHOP_EMAIL';
UPDATE `ps_configuration` SET `value`='admin@example.com' WHERE `name`='MA_MERCHANT_MAILS';

UPDATE `ps_configuration` SET `value`='0000' WHERE `name`='SMILEPAY_c2c_MID';
UPDATE `ps_configuration` SET `value`='0000' WHERE `name`='SMILEPAY_famiport_MID';
UPDATE `ps_configuration` SET `value`='0000' WHERE `name`='SMILEPAY_credit_MID';
UPDATE `ps_configuration` SET `value`='0000' WHERE `name`='SMILEPAY_ATM_MID';
UPDATE `ps_configuration` SET `value`='0000' WHERE `name`='SMILEPAY_ibon_MID';
UPDATE `ps_configuration` SET `value`='0000' WHERE `name`='SMILEPAY_ezcat_MID';
UPDATE `ps_configuration` SET `value`='0000' WHERE `name`='SMILEPAY_csv_MID';

