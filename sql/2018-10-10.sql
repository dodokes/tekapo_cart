INSERT INTO `ps_authorization_role` (`id_authorization_role`, `slug`) VALUES
(901, 'ROLE_MOD_MODULE_SIMPLICITY_GTM_CREATE'),
(902, 'ROLE_MOD_MODULE_SIMPLICITY_GTM_READ'),
(903, 'ROLE_MOD_MODULE_SIMPLICITY_GTM_UPDATE'),
(904, 'ROLE_MOD_MODULE_SIMPLICITY_GTM_DELETE');

INSERT INTO `ps_configuration` (`id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(NULL, NULL, 'SIMPLICITY_GTM_GUA_SITE_SPEED_SAMPLE_RATE', '1', NOW(), NOW()),
(NULL, NULL, 'SIMPLICITY_GTM_GUA_ECOMM_PRODID', '1',  NOW(), NOW()),
(NULL, NULL, 'SIMPLICITY_GTM_GUA_ECOMM_PAGETYPE', '2',  NOW(), NOW()),
(NULL, NULL, 'SIMPLICITY_GTM_GUA_ECOMM_TOTALVALUE', '3',  NOW(), NOW()),
(NULL, NULL, 'SIMPLICITY_GTM_GUA_ECOMM_CATEGORY', '4',  NOW(), NOW()),
(NULL, NULL, 'SIMPLICITY_GTM_EXCLUDED_ORDER_STATES', '6,8,7',  NOW(), NOW()),
(NULL, NULL, 'SIMPLICITY_GTM_REFUND_ORDER_STATES', '6,7',  NOW(), NOW()),
(NULL, NULL, 'SIMPLICITY_GTM_NO_BO_TRACK', NULL,  NOW(), NOW()),
(NULL, NULL, 'SIMPLICITY_GTM_DO_NOT_TRACK', '1',  NOW(), NOW()),
(NULL, NULL, 'SIMPLICITY_GTM_ID', NULL,  NOW(), NOW()),
(NULL, NULL, 'SIMPLICITY_GTM_GUA_ID', NULL,  NOW(), NOW()),
(NULL, NULL, 'SIMPLICITY_GTM_GUA_ANONYMIZE_IP', NULL,  NOW(), NOW()),
(NULL, NULL, 'SIMPLICITY_GTM_GUA_UNIFY_USER_ID', NULL,  NOW(), NOW()),
(NULL, NULL, 'SIMPLICITY_GTM_GUA_DYNAMIC_REMARKETING', NULL,  NOW(), NOW()),
(NULL, NULL, 'SIMPLICITY_GTM_GUA_MERCHANT_VARIANT', NULL,  NOW(), NOW()),
(NULL, NULL, 'SIMPLICITY_GTM_ADWORDS_ID', NULL,  NOW(), NOW()),
(NULL, NULL, 'SIMPLICITY_GTM_ADWORDS_LABEL', NULL,  NOW(), NOW()),
(NULL, NULL, 'SIMPLICITY_GTM_FACEBOOK_ID', NULL,  NOW(), NOW()),
(NULL, NULL, 'SIMPLICITY_GTM_FACEBOOK_CATALOG_VARIANT', NULL,  NOW(), NOW()),
(NULL, NULL, 'SIMPLICITY_GTM_NO_BO_TRACKING', NULL,  NOW(), NOW());

CREATE TABLE `ps_gtm_client` (
  `id_customer` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL,
  `id_client` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ps_gtm_orders` (
  `id_order` int(10) UNSIGNED NOT NULL,
  `id_shop` int(10) UNSIGNED NOT NULL,
  `via` enum('admin','shop') DEFAULT NULL,
  `date_add` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `ps_hook_module` (`id_module`, `id_shop`, `id_hook`, `position`) VALUES
(100, 1, 14, 10),
(100, 1, 20, 2),
(100, 1, 29, 2),
(100, 1, 31, 7),
(100, 1, 33, 7),
(100, 1, 40, 7),
(100, 1, 65, 3);

INSERT INTO `ps_module` (`id_module`, `name`, `active`, `version`) VALUES
(100, 'simplicity_gtm', 1, '1.0.0');

INSERT INTO `ps_module_access` (`id_profile`, `id_authorization_role`) VALUES
(1, 901),
(1, 902),
(1, 903),
(1, 904);

INSERT INTO `ps_module_group` (`id_module`, `id_shop`, `id_group`) VALUES
(100, 1, 1),
(100, 1, 2),
(100, 1, 3);

INSERT INTO `ps_module_shop` (`id_module`, `id_shop`, `enable_device`) VALUES
(100, 1, 7);

-- 手動部份：
-- ps_tab
-- ps_tab_lang
-- 刪除統計模組：pagesnotfound、statsstock、statsbestvouchers、statscarrier 29,50,35,36 （先不處理）
-- (crontab -l ; echo "0 3 * * * curl -sS https://$TC_DOMAIN/modules/gsitemap/gsitemap-cron.php?token=XXXXXXXX") | crontab -