INSERT INTO `ps_meta_lang` (`id_meta`, `id_shop`, `id_lang`, `title`, `description`, `keywords`, `url_rewrite`) VALUES
('26', '1', '1', '商品', '', '', 'product'),
('26', '1', '2', 'Product', '', '', 'product'),
('26', '1', '3', 'Product', '', '', 'product'),
('26', '1', '4', 'Product', '', '', 'product');

INSERT INTO `ps_meta_lang` (`id_meta`, `id_shop`, `id_lang`, `title`, `description`, `keywords`, `url_rewrite`) VALUES
('27', '1', '1', '商品分類', '', '', 'category'),
('27', '1', '2', 'Category', '', '', 'category'),
('27', '1', '3', 'Category', '', '', 'category'),
('27', '1', '4', 'Category', '', '', 'category');

INSERT INTO `ps_meta_lang` (`id_meta`, `id_shop`, `id_lang`, `title`, `description`, `keywords`, `url_rewrite`) VALUES
('28', '1', '1', '自訂頁面', '', '', 'cms'),
('28', '1', '2', 'Cms', '', '', 'cms'),
('28', '1', '3', 'Cms', '', '', 'cms'),
('28', '1', '4', 'Cms', '', '', 'cms');

INSERT INTO `ps_hook` (`id_hook`, `name`, `title`, `description`, `position`, `sort`) VALUES (246, 'displayLeftColumnContact', '聯絡我們 左側欄', '', '1', '20');

INSERT INTO `ps_hook_module` (`id_module`, `id_shop`, `id_hook`, `position`) VALUES ('11', '1', '246', '1');

INSERT INTO `ps_hook` (`id_hook`, `name`, `title`, `description`, `position`, `sort`) VALUES (247, 'actionPsAdminCustoConfigurationControllerUpdateModuleAfter', 'actionPsAdminCustoConfigurationControllerUpdateModuleAfter', '', '1', '0');

INSERT INTO `ps_hook_module` (`id_module`, `id_shop`, `id_hook`, `position`) VALUES ('26', '1', '247', '1');

INSERT INTO `ps_hook` (`id_hook`, `name`, `title`, `description`, `position`, `sort`) VALUES (248, 'actionAdminModulesPositionsControllerEditGraftAfter', 'actionAdminModulesPositionsControllerEditGraftAfter', '', '1', '0');

INSERT INTO `ps_hook_module` (`id_module`, `id_shop`, `id_hook`, `position`) VALUES ('26', '1', '248', '1');