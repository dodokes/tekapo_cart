
UPDATE `ps_tab_lang` SET `name` = '追蹤' WHERE `ps_tab_lang`.`id_tab` = 32 AND `ps_tab_lang`.`id_lang` = 1;

UPDATE `ps_order_state_lang` SET `name` = '已付款（無庫存）' WHERE `ps_order_state_lang`.`id_order_state` = 9 AND `ps_order_state_lang`.`id_lang` = 1;
UPDATE `ps_order_state_lang` SET `name` = '等待付款（無庫存）' WHERE `ps_order_state_lang`.`id_order_state` = 12 AND `ps_order_state_lang`.`id_lang` = 1;
UPDATE `ps_order_state_lang` SET `name` = '超商取貨付款 處理中' WHERE `ps_order_state_lang`.`id_order_state` = 14 AND `ps_order_state_lang`.`id_lang` = 2;

UPDATE `ps_configuration` SET `value` = '103' WHERE `ps_configuration`.`name` = 'SMILEPAY_C2CUP_711_CARRIER_ID_REF';
UPDATE `ps_configuration` SET `value` = '105' WHERE `ps_configuration`.`name` = 'SMILEPAY_C2CUP_FAMI_CARRIER_ID_REF';

UPDATE `ps_configuration` SET `value` = '1' WHERE `ps_configuration`.`name` = 'PS_CONDITIONS_CMS_ID';

UPDATE `ps_order_state` SET `color` = '#FDA729' WHERE `ps_order_state`.`id_order_state` = 12;

INSERT INTO `ps_order_state` (`id_order_state`, `invoice`, `send_email`, `module_name`, `color`, `unremovable`, `hidden`, `logable`, `delivery`, `shipped`, `paid`, `pdf_invoice`, `pdf_delivery`, `deleted`, `sort`) VALUES
(26, '0', '0', '', '#3b5aa1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0');

INSERT INTO `ps_order_state_lang` (`id_order_state`, `id_lang`, `name`, `template`) VALUES
('26', '1', '售完', 'outofstock'),
('26', '2', 'Sold out', 'outofstock'),
('26', '3', 'Sold out', 'outofstock'),
('26', '4', 'Sold out', 'outofstock');

UPDATE `ps_order_state_lang` SET `name` = 'PayPal 等待付款' WHERE `ps_order_state_lang`.`id_order_state` = 23 AND `ps_order_state_lang`.`id_lang` = 1;
UPDATE `ps_order_state_lang` SET `name` = 'Braintree 等待付款' WHERE `ps_order_state_lang`.`id_order_state` = 24 AND `ps_order_state_lang`.`id_lang` = 1;
UPDATE `ps_order_state_lang` SET `name` = 'Braintree 等待驗證' WHERE `ps_order_state_lang`.`id_order_state` = 25 AND `ps_order_state_lang`.`id_lang` = 1;
UPDATE `ps_order_state_lang` SET `name` = '貨到付款 等待驗證' WHERE `ps_order_state_lang`.`id_order_state` = 13 AND `ps_order_state_lang`.`id_lang` = 1;

TRUNCATE TABLE `ps_order_cart_rule`;
