ALTER TABLE `ps_image_type` DROP `deleted`;
ALTER TABLE `ps_image_type` DROP `description`;
ALTER TABLE `ps_image_type` DROP `crop`;

UPDATE `ps_hook` SET `sort` = '6' WHERE `ps_hook`.`id_hook` = 9;
UPDATE `ps_hook` SET `title` = '全站 頁尾滿版' WHERE `ps_hook`.`id_hook` = 9;
UPDATE `ps_hook` SET `description` = '預設模組：無' WHERE `ps_hook`.`id_hook` = 9;
UPDATE `ps_hook` SET `description` = '預設模組：無' WHERE `ps_hook`.`id_hook` = 21;
UPDATE `ps_hook` SET `description` = '預設模組：無' WHERE `ps_hook`.`id_hook` = 22;

UPDATE `ps_hook` SET `sort` = '7' WHERE `ps_hook`.`id_hook` = 179;
UPDATE `ps_hook` SET `sort` = '8' WHERE `ps_hook`.`id_hook` = 35;

UPDATE `ps_hook` SET `sort` = '20' WHERE `ps_hook`.`id_hook` = 13;
UPDATE `ps_hook` SET `sort` = '30' WHERE `ps_hook`.`id_hook` = 12;

UPDATE `ps_hook` SET `sort` = '40' WHERE `ps_hook`.`id_hook` = 57;
UPDATE `ps_hook` SET `sort` = '41' WHERE `ps_hook`.`id_hook` = 52;
UPDATE `ps_hook` SET `sort` = '42' WHERE `ps_hook`.`id_hook` = 27;


-- 篩選條件

UPDATE `ps_configuration` SET `value` = '0' WHERE `ps_configuration`.`name` = 'PS_LAYERED_FILTER_PRICE_USETAX';


-- 新 module: simplicity_igfeed

INSERT INTO `ps_module` (`id_module`, `name`, `active`, `version`) VALUES
(101, 'simplicity_igfeed', 1, '1.0.0');

INSERT INTO `ps_authorization_role` (`id_authorization_role`, `slug`) VALUES
(905, 'ROLE_MOD_MODULE_SIMPLICITY_IGFEED_CREATE'),
(906, 'ROLE_MOD_MODULE_SIMPLICITY_IGFEED_READ'),
(907, 'ROLE_MOD_MODULE_SIMPLICITY_IGFEED_UPDATE'),
(908, 'ROLE_MOD_MODULE_SIMPLICITY_IGFEED_DELETE');

DELETE FROM `ps_meta` WHERE `ps_meta`.`id_meta` = 45;
DELETE FROM `ps_meta_lang` WHERE `ps_meta_lang`.`id_meta` = 45;
DELETE FROM `ps_meta` WHERE `ps_meta`.`id_meta` = 46;
DELETE FROM `ps_meta_lang` WHERE `ps_meta_lang`.`id_meta` = 46;
DELETE FROM `ps_meta` WHERE `ps_meta`.`id_meta` = 47;
DELETE FROM `ps_meta_lang` WHERE `ps_meta_lang`.`id_meta` = 47;
DELETE FROM `ps_meta` WHERE `ps_meta`.`id_meta` = 48;
DELETE FROM `ps_meta_lang` WHERE `ps_meta_lang`.`id_meta` = 48;

INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(NULL, NULL, NULL, 'SIMPLICITY_IGFEED_CLIENT_ID', '', '2019-01-26 00:00:00', '2019-01-26 00:00:00');
INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(NULL, NULL, NULL, 'SIMPLICITY_IGFEED_CLIENT_SECRET', '', '2019-01-26 00:00:00', '2019-01-26 00:00:00');
INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(NULL, NULL, NULL, 'SIMPLICITY_IGFEED_ACCESS_TOKEN', '', '2019-01-26 00:00:00', '2019-01-26 00:00:00');
INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(NULL, NULL, NULL, 'SIMPLICITY_IGFEED_IMAGE_NUM', '18', '2019-01-26 00:00:00', '2019-01-26 00:00:00');
INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(NULL, NULL, NULL, 'SIMPLICITY_IGFEED_REFRESH', 'day', '2019-01-26 00:00:00', '2019-01-26 00:00:00');