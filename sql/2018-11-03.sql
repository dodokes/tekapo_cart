
-- 加入自訂頁面搜尋（隱藏功能）

ALTER TABLE `ps_cms_shop` ADD `indexed` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '已索引 flag' AFTER `id_shop`;

INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES (NULL, NULL, NULL, 'PS_SEARCH_WEIGHT_CMS_META_TITLE', '5', NOW(), NOW());
INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES (NULL, NULL, NULL, 'PS_SEARCH_WEIGHT_CMS_CONTENT', '5', NOW(), NOW());

CREATE TABLE `ps_search_index_cms` (
  `id_cms` int(11) UNSIGNED NOT NULL,
  `id_word` int(11) UNSIGNED NOT NULL,
  `weight` smallint(4) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `ps_search_index_cms`
  ADD PRIMARY KEY (`id_word`,`id_cms`),
  ADD KEY `id_cms` (`id_cms`,`weight`) USING BTREE;
COMMIT;

CREATE TABLE `ps_search_word_cms` (
  `id_word` int(10) UNSIGNED NOT NULL,
  `id_shop` int(11) UNSIGNED NOT NULL DEFAULT '1',
  `id_lang` int(10) UNSIGNED NOT NULL,
  `word` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `ps_search_word_cms`
  ADD PRIMARY KEY (`id_word`),
  ADD UNIQUE KEY `id_lang` (`id_lang`,`id_shop`,`word`);

ALTER TABLE `ps_search_word_cms`
  MODIFY `id_word` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
COMMIT;

