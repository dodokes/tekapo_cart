-- FB 登入用的 Hook

INSERT INTO `ps_hook` (`id_hook`, `name`, `title`, `description`, `position`, `sort`) VALUES (245, 'displayCustomerLoginLink', 'displayCustomerLoginLink', '', '1', '0');
INSERT INTO `ps_hook_module` (`id_module`, `id_shop`, `id_hook`, `position`) VALUES ('82', '1', '245', '1');
DELETE FROM `ps_hook_module` WHERE `ps_hook_module`.`id_module` = 82 AND `ps_hook_module`.`id_shop` = 1 AND `ps_hook_module`.`id_hook` = 116;
UPDATE `ps_hook` SET `title` = '頁首登入按鈕' WHERE `ps_hook`.`id_hook` = 245;

-- 轉帳匯款的 Hook

INSERT INTO `ps_hook_module` (`id_module`, `id_shop`, `id_hook`, `position`) VALUES ('28', '1', '172', '2');

-- Google Sitemap

UPDATE `ps_cms` SET `indexation` = '1' WHERE `ps_cms`.`id_cms` = 1;
UPDATE `ps_cms` SET `indexation` = '1' WHERE `ps_cms`.`id_cms` = 2;
UPDATE `ps_cms` SET `indexation` = '1' WHERE `ps_cms`.`id_cms` = 3;
UPDATE `ps_cms` SET `indexation` = '1' WHERE `ps_cms`.`id_cms` = 4;
UPDATE `ps_cms` SET `indexation` = '1' WHERE `ps_cms`.`id_cms` = 5;
UPDATE `ps_cms` SET `indexation` = '1' WHERE `ps_cms`.`id_cms` = 6;

-- 後台選單調整

UPDATE `ps_tab` SET `active` = '0' WHERE `ps_tab`.`id_tab` = 86;
UPDATE `ps_tab_lang` SET `name` = '稱號' WHERE `ps_tab_lang`.`id_tab` = 88 AND `ps_tab_lang`.`id_lang` = 1;
UPDATE `ps_tab` SET `active` = '0' WHERE `ps_tab`.`id_tab` = 85;

UPDATE `ps_tab_lang` SET `name` = '通知信' WHERE `ps_tab_lang`.`id_tab` = 103 AND `ps_tab_lang`.`id_lang` = 1;
UPDATE `ps_tab_lang` SET `name` = '商品' WHERE `ps_tab_lang`.`id_tab` = 84 AND `ps_tab_lang`.`id_lang` = 1;
UPDATE `ps_tab_lang` SET `name` = '圖片' WHERE `ps_tab_lang`.`id_tab` = 55 AND `ps_tab_lang`.`id_lang` = 1;
UPDATE `ps_tab_lang` SET `name` = '多國' WHERE `ps_tab_lang`.`id_tab` = 62 AND `ps_tab_lang`.`id_lang` = 1;
UPDATE `ps_tab_lang` SET `name` = '商店' WHERE `ps_tab_lang`.`id_tab` = 89 AND `ps_tab_lang`.`id_lang` = 1;

UPDATE `ps_tab` SET `active` = '0' WHERE `ps_tab`.`id_tab` = 44;
UPDATE `ps_tab` SET `active` = '0' WHERE `ps_tab`.`id_tab` = 45;
UPDATE `ps_tab` SET `active` = '0' WHERE `ps_tab`.`id_tab` = 46;
UPDATE `ps_tab` SET `active` = '0' WHERE `ps_tab`.`id_tab` = 47;
UPDATE `ps_tab` SET `id_parent` = '77' WHERE `ps_tab`.`id_tab` = 103;

-- 刪除 module newletter

DELETE FROM `ps_module` WHERE `ps_module`.`id_module` = 63;
DELETE FROM `ps_module_group` WHERE `ps_module_group`.`id_module` = 63 AND `ps_module_group`.`id_shop` = 1 AND `ps_module_group`.`id_group` = 1;
DELETE FROM `ps_module_group` WHERE `ps_module_group`.`id_module` = 63 AND `ps_module_group`.`id_shop` = 1 AND `ps_module_group`.`id_group` = 2;
DELETE FROM `ps_module_group` WHERE `ps_module_group`.`id_module` = 63 AND `ps_module_group`.`id_shop` = 1 AND `ps_module_group`.`id_group` = 3;
DELETE FROM `ps_module_shop` WHERE `ps_module_shop`.`id_module` = 63 AND `ps_module_shop`.`id_shop` = 1;

DELETE FROM `ps_authorization_role` WHERE `ps_authorization_role`.`id_authorization_role` = 733;
DELETE FROM `ps_authorization_role` WHERE `ps_authorization_role`.`id_authorization_role` = 734;
DELETE FROM `ps_authorization_role` WHERE `ps_authorization_role`.`id_authorization_role` = 735;
DELETE FROM `ps_authorization_role` WHERE `ps_authorization_role`.`id_authorization_role` = 736;

DELETE FROM `ps_module_access` WHERE `ps_module_access`.`id_profile` = 1 AND `ps_module_access`.`id_authorization_role` = 733;
DELETE FROM `ps_module_access` WHERE `ps_module_access`.`id_profile` = 1 AND `ps_module_access`.`id_authorization_role` = 734;
DELETE FROM `ps_module_access` WHERE `ps_module_access`.`id_profile` = 1 AND `ps_module_access`.`id_authorization_role` = 735;
DELETE FROM `ps_module_access` WHERE `ps_module_access`.`id_profile` = 1 AND `ps_module_access`.`id_authorization_role` = 736;

-- 商品 rewrite

UPDATE `ps_configuration` SET `value` = '{category}/{id}{/:id_product_attribute}/{rewrite}{/:ean13}', `date_upd` = NOW() WHERE `ps_configuration`.`name` = 'PS_ROUTE_product_rule';

---

UPDATE `ps_configuration` SET `value` = NULL, `date_upd` = NOW() WHERE `ps_configuration`.`name` = 'PS_COOKIE_CHECKIP';


