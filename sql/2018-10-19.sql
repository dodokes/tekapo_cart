-- 庫存
UPDATE `ps_tab` SET `active` = '1' WHERE `ps_tab`.`id_tab` = 23;
-- 匯入
UPDATE `ps_tab` SET `active` = '1' WHERE `ps_tab`.`id_tab` = 104;

-- 調整 hook 順序
UPDATE `ps_hook` SET `title` = '全站 頁首中區' WHERE `ps_hook`.`id_hook` = 21;
UPDATE `ps_hook` SET `sort` = '5' WHERE `ps_hook`.`id_hook` = 22;
UPDATE `ps_hook` SET `sort` = '4' WHERE `ps_hook`.`id_hook` = 21;

-- SEO＆Meta
UPDATE `ps_tab_lang` SET `name` = 'SEO＆Meta' WHERE `ps_tab_lang`.`id_tab` = 92 AND `ps_tab_lang`.`id_lang` = 1;
UPDATE `ps_tab` SET `active` = '1' WHERE `ps_tab`.`id_tab` = 92;
UPDATE `ps_tab_lang` SET `name` = 'SEO＆Meta' WHERE `ps_tab_lang`.`id_tab` = 93 AND `ps_tab_lang`.`id_lang` = 1;
UPDATE `ps_tab` SET `active` = '0' WHERE `ps_tab`.`id_tab` = 94;
UPDATE `ps_tab` SET `active` = '0' WHERE `ps_tab`.`id_tab` = 95;

-- 清空再新增
INSERT INTO `ps_meta_lang` (`id_meta`, `id_shop`, `id_lang`, `title`, `description`, `keywords`, `url_rewrite`) VALUES
(1, 1, 1, '404 錯誤', '找不到此頁', '', 'page-not-found'),
(1, 1, 2, '404 error', 'This page cannot be found', '', 'page-not-found'),
(1, 1, 3, '404 error', 'This page cannot be found', '', 'page-not-found'),
(1, 1, 4, '404エラー', 'ページが見つかりませんでした。', '', 'page-not-found'),
(2, 1, 1, '最近熱門', '今日熱門商品', '', 'best-sales'),
(2, 1, 2, 'Best sales', 'Our best sales', '', 'best-sales'),
(2, 1, 3, 'Best sales', 'Our best sales', '', 'best-sales'),
(2, 1, 4, 'ベストセラー', '当店のベストセラー', '', 'best-sales'),
(3, 1, 1, '聯絡我們', '歡迎透過「聯絡我們」詢問，我們會盡快答覆您。', '', 'contact'),
(3, 1, 2, 'Contact', 'Use our form to contact us', '', 'contact'),
(3, 1, 3, 'Contact', 'Use our form to contact us', '', 'contact'),
(3, 1, 4, 'Contact', 'ご質問は問合せフォームをご利用ください。', '', 'contact'),
(3, 2, 1, '聯絡我們', '使用以下表格聯絡我們', '', 'contact'),
(3, 2, 2, 'Contact', 'Use our form to contact us', '', 'contact'),
(3, 2, 3, 'Contact', 'Use our form to contact us', '', 'contact'),
(3, 2, 4, 'Contact', 'ご質問は問合せフォームをご利用ください。', '', 'contact'),
(3, 3, 1, '聯絡我們', '使用以下表格聯絡我們', '', 'contact'),
(3, 3, 2, 'Contact', 'Use our form to contact us', '', 'contact'),
(3, 3, 3, 'Contact', 'Use our form to contact us', '', 'contact'),
(3, 3, 4, 'Contact', 'ご質問は問合せフォームをご利用ください。', '', 'contact'),
(4, 1, 1, '我的 TekapoCart 網路商店', '本站於 _TIME_ 建立，全站 A+ SSL 加密，顧客可以安心的購物網站。', '', ''),
(4, 1, 2, 'My TekapoCart Online Store', 'Started on _TIME_. A secure and reliable online store.', '', ''),
(4, 1, 3, '', 'Software maintained by TekapoCart', '', ''),
(4, 1, 4, '', 'Software maintained by TekapoCart', '', ''),
(5, 1, 1, 'Brands', 'Brands list', '', 'brands'),
(5, 1, 2, 'Brands', 'Brands list', '', 'brands'),
(5, 1, 3, 'Brands', 'Brands list', '', 'brands'),
(5, 1, 4, 'Brands', 'Brands list', '', 'brands'),
(6, 1, 1, '新品上架', '質感新鮮品', '', 'new-products'),
(6, 1, 2, 'New Products', 'Our new products', '', 'new-products'),
(6, 1, 3, 'New Products', 'Our new products', '', 'new-products'),
(6, 1, 4, 'New Products', '新着商品', '', 'new-products'),
(7, 1, 1, '忘記密碼', '輸入您用於登錄的電郵地址以接收具有新密碼的電子郵件', '', 'password-recovery'),
(7, 1, 2, 'Forgot Password', 'Enter your e-mail address used to register in goal to get e-mail with your new password', '', 'password-recovery'),
(7, 1, 3, 'Forgot Password', 'Enter your e-mail address used to register in goal to get e-mail with your new password', '', 'password-recovery'),
(7, 1, 4, 'Forgot Password', 'Enter your e-mail address used to register in goal to get e-mail with your new password', '', 'password-recovery'),
(8, 1, 1, '更多優惠', '限時限量折扣專區', '', 'prices-drop'),
(8, 1, 2, 'Prices drop', 'Our special products', '', 'prices-drop'),
(8, 1, 3, 'Prices drop', 'Our special products', '', 'prices-drop'),
(8, 1, 4, '値下げ商品', 'Our special products', '', 'prices-drop'),
(9, 1, 1, '網站導覽', '再次搜索', '', '網站導覽'),
(9, 1, 2, 'Sitemap', 'Lost ? Find what your are looking for', '', '网站地图'),
(9, 1, 3, '站点地图', 'Lost ? Find what your are looking for', '', '网站地图'),
(9, 1, 4, 'Sitemap', 'お探しのものが見つかりませんか？', '', '网站地图'),
(10, 1, 1, '供應商', '供應商列表', '', 'supplier'),
(10, 1, 2, 'Supplier', 'Suppliers list', '', 'supplier'),
(10, 1, 3, 'Supplier', 'Suppliers list', '', 'supplier'),
(10, 1, 4, 'Supplier', 'サプライヤー一覧', '', 'supplier'),
(11, 1, 1, '配送資訊', '', '', 'address'),
(11, 1, 2, 'Address', '', '', 'address'),
(11, 1, 3, '地址', '', '', 'address'),
(11, 1, 4, 'Address', '', '', 'address'),
(12, 1, 1, '配送資訊', '', '', 'addresses'),
(12, 1, 2, 'Addresses', '', '', 'addresses'),
(12, 1, 3, '地址', '', '', 'addresses'),
(12, 1, 4, 'Addresses', '', '', 'addresses'),
(13, 1, 1, '登入', '', '', 'login'),
(13, 1, 2, 'Login', '', '', 'login'),
(13, 1, 3, '登录', '', '', 'login'),
(13, 1, 4, 'Login', '', '', 'login'),
(14, 1, 1, '購物車', '', '', 'cart'),
(14, 1, 2, 'Cart', '', '', 'cart'),
(14, 1, 3, '购物车', '', '', 'cart'),
(14, 1, 4, 'Cart', '', '', 'cart'),
(15, 1, 1, '優惠券', '', '', 'discount'),
(15, 1, 2, 'Discount', '', '', 'discount'),
(15, 1, 3, '折扣', '', '', 'discount'),
(15, 1, 4, '割引', '', '', 'discount'),
(16, 1, 1, '訂單記錄', '', '', 'order-history'),
(16, 1, 2, 'Order History', '', '', 'order-history'),
(16, 1, 3, 'Order History', '', '', 'order-history'),
(16, 1, 4, 'Order History', '', '', 'order-history'),
(17, 1, 1, '個人資料', '', '', 'identity'),
(17, 1, 2, 'Personal Info', '', '', 'identity'),
(17, 1, 3, 'Personal Info', '', '', 'identity'),
(17, 1, 4, 'Personal Info', '', '', 'identity'),
(18, 1, 1, '會員中心', '', '', 'my-account'),
(18, 1, 2, 'Member Area', '', '', 'my-account'),
(18, 1, 3, 'Member Area', '', '', 'my-account'),
(18, 1, 4, 'Member Area', '', '', 'my-account'),
(19, 1, 1, '訂單追蹤', '', '', 'order-follow'),
(19, 1, 2, 'Order Follow', '', '', 'order-follow'),
(19, 1, 3, 'Order Follow', '', '', 'order-follow'),
(19, 1, 4, '注文対応', '', '', 'order-follow'),
(20, 1, 1, '折讓單', '', '', 'order-slip'),
(20, 1, 2, 'Order Slip', '', '', 'order-slip'),
(20, 1, 3, 'Order Slip', '', '', 'order-slip'),
(20, 1, 4, 'Order Slip', '', '', 'order-slip'),
(21, 1, 1, '訂單', '', '', 'order'),
(21, 1, 2, 'Order', '', '', 'order'),
(21, 1, 3, '购买', '', '', '订单'),
(21, 1, 4, 'Order', '', '', 'order'),
(22, 1, 1, '搜尋結果', '用關鍵字搜尋', '', 'search'),
(22, 1, 2, 'Search', '', '', 'search'),
(22, 1, 3, '搜索', '', '', 'search'),
(22, 1, 4, 'Search', '', '', 'search'),
(23, 1, 1, '商店', '', '', '商店'),
(23, 1, 2, 'Store', '', '', 'stores'),
(23, 1, 3, 'Store', '', '', 'stores'),
(23, 1, 4, 'Store', '', '', '店舗'),
(24, 1, 1, '非會員訂單追蹤', '', '', 'guest-tracking'),
(24, 1, 2, 'Guest tracking', '', '', 'guest-tracking'),
(24, 1, 3, 'Guest tracking', '', '', 'guest-tracking'),
(24, 1, 4, 'ゲストトラッキング', '', '', 'guest-tracking'),
(25, 1, 1, '訂單確認', '', '', 'order-confirmation'),
(25, 1, 2, 'Order Confirmation', '', '', 'order-confirmation'),
(25, 1, 3, 'Order Confirmation', '', '', 'order-confirmation'),
(25, 1, 4, 'Order Confirmation', '', '', 'order-confirmation'),
(26, 1, 1, '商品頁', '', '', 'product'),
(26, 1, 2, 'Product', '', '', 'product'),
(26, 1, 3, 'Product', '', '', 'product'),
(26, 1, 4, 'Product', '', '', 'product'),
(27, 1, 1, '商品分類', '', '', 'category'),
(27, 1, 2, 'Category', '', '', 'category'),
(27, 1, 3, 'Category', '', '', 'category'),
(27, 1, 4, 'Category', '', '', 'category'),
(28, 1, 1, '自訂頁面', '', '', 'cms'),
(28, 1, 2, 'Cms', '', '', 'cms'),
(28, 1, 3, 'Cms', '', '', 'cms'),
(28, 1, 4, 'Cms', '', '', 'cms'),
(36, 1, 1, '', '', '', ''),
(36, 1, 2, '', '', '', ''),
(36, 1, 3, '', '', '', ''),
(36, 1, 4, '', '', '', ''),
(37, 1, 1, '', '', '', ''),
(37, 1, 2, '', '', '', ''),
(37, 1, 3, '', '', '', ''),
(37, 1, 4, '', '', '', ''),
(38, 1, 1, '', '', '', ''),
(38, 1, 2, '', '', '', ''),
(38, 1, 3, '', '', '', ''),
(38, 1, 4, '', '', '', ''),
(39, 1, 1, '', '', '', ''),
(39, 1, 2, '', '', '', ''),
(39, 1, 3, '', '', '', ''),
(39, 1, 4, '', '', '', ''),
(40, 1, 1, '', '', '', ''),
(40, 1, 2, '', '', '', ''),
(40, 1, 3, '', '', '', ''),
(40, 1, 4, '', '', '', ''),
(42, 1, 1, '', '', '', ''),
(42, 1, 2, '', '', '', ''),
(42, 1, 3, '', '', '', ''),
(42, 1, 4, '', '', '', ''),
(43, 1, 1, '', '', '', ''),
(43, 1, 2, '', '', '', ''),
(43, 1, 3, '', '', '', ''),
(43, 1, 4, '', '', '', ''),
(45, 1, 1, '', '', '', ''),
(45, 1, 2, '', '', '', ''),
(45, 1, 3, '', '', '', ''),
(45, 1, 4, '', '', '', ''),
(46, 1, 1, '', '', '', ''),
(46, 1, 2, '', '', '', ''),
(46, 1, 3, '', '', '', ''),
(46, 1, 4, '', '', '', ''),
(47, 1, 1, '', '', '', ''),
(47, 1, 2, '', '', '', ''),
(47, 1, 3, '', '', '', ''),
(47, 1, 4, '', '', '', ''),
(48, 1, 1, '', '', '', ''),
(48, 1, 2, '', '', '', ''),
(48, 1, 3, '', '', '', ''),
(48, 1, 4, '', '', '', ''),
(49, 1, 1, '', '', '', ''),
(49, 1, 2, '', '', '', ''),
(49, 1, 3, '', '', '', ''),
(49, 1, 4, '', '', '', ''),
(50, 1, 1, '', '', '', ''),
(50, 1, 2, '', '', '', ''),
(50, 1, 3, '', '', '', ''),
(50, 1, 4, '', '', '', '');

-- multi shops
INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(NULL, NULL, NULL, 'PS_MULTISHOP_FEATURE_ACTIVE', NULL, NOW(), NOW());

INSERT INTO `ps_cms_category_lang` (`id_cms_category`, `id_lang`, `id_shop`, `name`, `description`, `link_rewrite`, `meta_title`, `meta_keywords`, `meta_description`) VALUES ('1', '1', '2', '首頁', '', '首頁', '', '', ''), ('1', '2', '2', '首页', '', '首页', '', '', ''), ('1', '3', '2', '首页', '', '首页', '', '', ''), ('1', '4', '2', '首页', '', '首页', '', '', '');
INSERT INTO `ps_cms_category_shop` (`id_cms_category`, `id_shop`) VALUES ('1', '2');
INSERT INTO `ps_cms_category_lang` (`id_cms_category`, `id_lang`, `id_shop`, `name`, `description`, `link_rewrite`, `meta_title`, `meta_keywords`, `meta_description`) VALUES ('1', '1', '3', '首頁', '', '首頁', '', '', ''), ('1', '2', '3', '首页', '', '首页', '', '', ''), ('1', '3', '3', '首页', '', '首页', '', '', ''), ('1', '4', '3', '首页', '', '首页', '', '', '');
INSERT INTO `ps_cms_category_shop` (`id_cms_category`, `id_shop`) VALUES ('1', '3');
INSERT INTO `ps_cms_category_lang` (`id_cms_category`, `id_lang`, `id_shop`, `name`, `description`, `link_rewrite`, `meta_title`, `meta_keywords`, `meta_description`) VALUES ('1', '1', '4', '首頁', '', '首頁', '', '', ''), ('1', '2', '4', '首页', '', '首页', '', '', ''), ('1', '3', '4', '首页', '', '首页', '', '', ''), ('1', '4', '4', '首页', '', '首页', '', '', '');
INSERT INTO `ps_cms_category_shop` (`id_cms_category`, `id_shop`) VALUES ('1', '4');

UPDATE `ps_image_type` SET `deleted` = '1' WHERE `ps_image_type`.`id_image_type` = 1;
UPDATE `ps_image_type` SET `description` = '手機版列表' WHERE `ps_image_type`.`id_image_type` = 2;
UPDATE `ps_image_type` SET `deleted` = '1' WHERE `ps_image_type`.`id_image_type` = 3;
UPDATE `ps_image_type` SET `description` = '桌機版列表' WHERE `ps_image_type`.`id_image_type` = 4;
UPDATE `ps_image_type` SET `description` = '商品主圖' WHERE `ps_image_type`.`id_image_type` = 5;
UPDATE `ps_image_type` SET `deleted` = '1' WHERE `ps_image_type`.`id_image_type` = 6;
UPDATE `ps_image_type` SET `deleted` = '1' WHERE `ps_image_type`.`id_image_type` = 7;
UPDATE `ps_image_type` SET `crop` = '1' WHERE `ps_image_type`.`id_image_type` = 2;
UPDATE `ps_image_type` SET `crop` = '1' WHERE `ps_image_type`.`id_image_type` = 4;

INSERT INTO `ps_meta_lang` (`id_meta`, `id_shop`, `id_lang`, `title`, `description`, `keywords`, `url_rewrite`) VALUES
('3', '2', '1', '聯絡我們', '歡迎透過「聯絡我們」詢問，我們會盡快答覆您。', '', 'contact'),
('3', '2', '2', 'Contact', 'Use our form to contact us', '', 'contact'),
('3', '2', '3', 'Contact', 'Use our form to contact us', '', 'contact'),
('3', '2', '4', 'Contact', 'ご質問は問合せフォームをご利用ください。', '', 'contact');

INSERT INTO `ps_meta_lang` (`id_meta`, `id_shop`, `id_lang`, `title`, `description`, `keywords`, `url_rewrite`) VALUES
('3', '3', '1', '聯絡我們', '歡迎透過「聯絡我們」詢問，我們會盡快答覆您。', '', 'contact'),
('3', '3', '2', 'Contact', 'Use our form to contact us', '', 'contact'),
('3', '3', '3', 'Contact', 'Use our form to contact us', '', 'contact'),
('3', '3', '4', 'Contact', 'ご質問は問合せフォームをご利用ください。', '', 'contact');

INSERT INTO `ps_meta_lang` (`id_meta`, `id_shop`, `id_lang`, `title`, `description`, `keywords`, `url_rewrite`) VALUES
('3', '4', '1', '聯絡我們', '歡迎透過「聯絡我們」詢問，我們會盡快答覆您。', '', 'contact'),
('3', '4', '2', 'Contact', 'Use our form to contact us', '', 'contact'),
('3', '4', '3', 'Contact', 'Use our form to contact us', '', 'contact'),
('3', '4', '4', 'Contact', 'ご質問は問合せフォームをご利用ください。', '', 'contact');

-- FavIcon
UPDATE `ps_configuration` SET `value` = 'favicon.png', `date_upd` = '2018-10-19 18:00:00' WHERE `ps_configuration`.`name` = 'PS_FAVICON';

INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(NULL, NULL, NULL, 'PS_FAVICON_IPHONE', 'touch-icon-iphone.png', '2018-10-19 18:00:00', '2018-10-19 18:00:00');

INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(NULL, NULL, NULL, 'PS_FAVICON_IPAD', 'touch-icon-ipad.png', '2018-10-19 18:00:00', '2018-10-19 18:00:00');

INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(NULL, NULL, NULL, 'PS_FAVICON_IPAD_RETINA', 'touch-icon-ipad-retina.png', '2018-10-19 18:00:00', '2018-10-19 18:00:00');

INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(NULL, NULL, NULL, 'PS_FAVICON_IPHONE_RETINA', 'touch-icon-iphone-retina.png', '2018-10-19 18:00:00', '2018-10-19 18:00:00');

INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(NULL, NULL, NULL, 'PS_FAVICON_ANDROID', 'android-icon.png', '2018-10-19 18:00:00', '2018-10-19 18:00:00');

INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(NULL, NULL, NULL, 'PS_FAVICON_MICROSOFT', 'microsoft-icon.png', '2018-10-19 18:00:00', '2018-10-19 18:00:00');

INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(NULL, NULL, NULL, 'TC_THEME_COLOR_SIMPLICITY', '#67C8C2', '2018-10-19 18:00:00', '2018-10-19 18:00:00');

INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(NULL, NULL, NULL, 'TC_THEME_COLOR_SIMPLICITY_BLACK', '#000', '2018-10-19 18:00:00', '2018-10-19 18:00:00');

INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(NULL, NULL, NULL, 'TC_THEME_COLOR_SIMPLICITY_BLUE', '#2fb5d2', '2018-10-19 18:00:00', '2018-10-19 18:00:00');

INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(NULL, NULL, NULL, 'TC_THEME_COLOR_SIMPLICITY_PINK', '#F0648F', '2018-10-19 18:00:00', '2018-10-19 18:00:00');