INSERT INTO `ps_authorization_role` (`id_authorization_role`, `slug`) VALUES
(893, 'ROLE_MOD_MODULE_PAYPAL_CREATE'),
(894, 'ROLE_MOD_MODULE_PAYPAL_READ'),
(895, 'ROLE_MOD_MODULE_PAYPAL_UPDATE'),
(896, 'ROLE_MOD_MODULE_PAYPAL_DELETE');

INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES
(606, NULL, NULL, 'CONF_PAYPAL_FIXED', '0.2', '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(607, NULL, NULL, 'CONF_PAYPAL_VAR', '2', '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(608, NULL, NULL, 'CONF_PAYPAL_FIXED_FOREIGN', '0.2', '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(609, NULL, NULL, 'CONF_PAYPAL_VAR_FOREIGN', '2', '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(610, NULL, NULL, 'PAYPAL_OS_WAITING', '23', '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(611, NULL, NULL, 'PAYPAL_BRAINTREE_OS_AWAITING', '24', '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(612, NULL, NULL, 'PAYPAL_BRAINTREE_OS_AWAITING_VALIDATION', '25', '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(613, NULL, NULL, 'PAYPAL_MERCHANT_ID_SANDBOX', NULL, '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(614, NULL, NULL, 'PAYPAL_MERCHANT_ID_LIVE', NULL, '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(615, NULL, NULL, 'PAYPAL_USERNAME_SANDBOX', NULL, '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(616, NULL, NULL, 'PAYPAL_PSWD_SANDBOX', NULL, '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(617, NULL, NULL, 'PAYPAL_SIGNATURE_SANDBOX', NULL, '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(618, NULL, NULL, 'PAYPAL_SANDBOX_ACCESS', '0', '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(619, NULL, NULL, 'PAYPAL_USERNAME_LIVE', NULL, '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(620, NULL, NULL, 'PAYPAL_PSWD_LIVE', NULL, '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(621, NULL, NULL, 'PAYPAL_SIGNATURE_LIVE', NULL, '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(622, NULL, NULL, 'PAYPAL_LIVE_ACCESS', '0', '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(623, NULL, NULL, 'PAYPAL_SANDBOX', '0', '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(624, NULL, NULL, 'PAYPAL_API_INTENT', 'sale', '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(625, NULL, NULL, 'PAYPAL_API_ADVANTAGES', '1', '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(626, NULL, NULL, 'PAYPAL_API_CARD', '0', '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(627, NULL, NULL, 'PAYPAL_METHOD', NULL, '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(628, NULL, NULL, 'PAYPAL_EXPRESS_CHECKOUT_SHORTCUT', '0', '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(629, NULL, NULL, 'PAYPAL_EXPRESS_CHECKOUT_SHORTCUT_CART', '1', '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(630, NULL, NULL, 'PAYPAL_CRON_TIME', '2018-09-18 15:09:37', '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(631, NULL, NULL, 'PAYPAL_BY_BRAINTREE', '0', '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(632, NULL, NULL, 'PAYPAL_EXPRESS_CHECKOUT_IN_CONTEXT', '0', '2018-09-18 15:05:37', '2018-09-18 15:05:37'),
(633, NULL, NULL, 'PAYPAL_VAULTING', '0', '2018-09-18 15:05:37', '2018-09-18 15:05:37');

ALTER TABLE `ps_customer_message` ADD `system` TINYINT NOT NULL DEFAULT '0' AFTER `read`;

INSERT INTO `ps_hook` (`id_hook`, `name`, `title`, `description`, `position`, `sort`) VALUES
(243, 'actionObjectCurrencyAddAfter', 'actionObjectCurrencyAddAfter', '', 1, 0),
(244, 'actionCartUpdateQuantityBefore', 'actionCartUpdateQuantityBefore', '', 1, 0);

INSERT INTO `ps_hook_module` (`id_module`, `id_shop`, `id_hook`, `position`) VALUES
(98, 1, 243, 1),
(98, 1, 244, 1);

INSERT INTO `ps_meta` (`id_meta`, `page`, `configurable`) VALUES
(49, 'module-paypal-payment', 1),
(50, 'module-paypal-validation', 1);

INSERT INTO `ps_meta_lang` (`id_meta`, `id_shop`, `id_lang`, `title`, `description`, `keywords`, `url_rewrite`) VALUES
(49, 1, 1, '', '', '', ''),
(49, 1, 2, '', '', '', ''),
(49, 1, 3, '', '', '', ''),
(49, 1, 4, '', '', '', ''),
(50, 1, 1, '', '', '', ''),
(50, 1, 2, '', '', '', ''),
(50, 1, 3, '', '', '', ''),
(50, 1, 4, '', '', '', '');

INSERT INTO `ps_module` (`id_module`, `name`, `active`, `version`) VALUES
(98, 'paypal', 1, '4.4.2');

INSERT INTO `ps_module_access` (`id_profile`, `id_authorization_role`) VALUES
(1, 893),
(1, 894),
(1, 895),
(1, 896);

INSERT INTO `ps_module_carrier` (`id_module`, `id_shop`, `id_reference`) VALUES
(98, 1, 1),
(98, 1, 2);

INSERT INTO `ps_module_country` (`id_module`, `id_shop`, `id_country`) VALUES
(98, 1, 5),
(98, 1, 11),
(98, 1, 21),
(98, 1, 22),
(98, 1, 25),
(98, 1, 136),
(98, 1, 203);

INSERT INTO `ps_module_currency` (`id_module`, `id_shop`, `id_currency`) VALUES
(98, 1, -1);

INSERT INTO `ps_module_group` (`id_module`, `id_shop`, `id_group`) VALUES
(98, 1, 1),
(98, 1, 2),
(98, 1, 3);

INSERT INTO `ps_module_shop` (`id_module`, `id_shop`, `enable_device`) VALUES
(98, 1, 7);

INSERT INTO `ps_order_state` (`id_order_state`, `invoice`, `send_email`, `module_name`, `color`, `unremovable`, `hidden`, `logable`, `delivery`, `shipped`, `paid`, `pdf_invoice`, `pdf_delivery`, `deleted`, `sort`) VALUES
(23, 0, 0, '', '#4169E1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(24, 0, 0, '', '#4169E1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(25, 0, 0, '', '#4169E1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

INSERT INTO `ps_order_state_lang` (`id_order_state`, `id_lang`, `name`, `template`) VALUES
(23, 1, 'Awaiting for PayPal payment', ''),
(23, 2, 'Awaiting for PayPal payment', ''),
(23, 3, 'Awaiting for PayPal payment', ''),
(23, 4, 'Awaiting for PayPal payment', ''),
(24, 1, 'Awaiting for Braintree payment', ''),
(24, 2, 'Awaiting for Braintree payment', ''),
(24, 3, 'Awaiting for Braintree payment', ''),
(24, 4, 'Awaiting for Braintree payment', ''),
(25, 1, 'Awaiting for Braintree validation', ''),
(25, 2, 'Awaiting for Braintree validation', ''),
(25, 3, 'Awaiting for Braintree validation', ''),
(25, 4, 'Awaiting for Braintree validation', '');

CREATE TABLE `ps_paypal_capture` (
  `id_paypal_capture` int(11) NOT NULL AUTO_INCREMENT,
  `id_capture` varchar(55) DEFAULT NULL,
  `id_paypal_order` int(11) DEFAULT NULL,
  `capture_amount` float DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `date_add` datetime DEFAULT NULL,
  `date_upd` datetime DEFAULT NULL,
  PRIMARY KEY (`id_paypal_capture`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ps_paypal_customer` (
  `id_paypal_customer` int(11) NOT NULL AUTO_INCREMENT,
  `id_customer` int(11) DEFAULT NULL,
  `reference` varchar(55) DEFAULT NULL,
  `method` varchar(55) DEFAULT NULL,
  `date_add` datetime DEFAULT NULL,
  `date_upd` datetime DEFAULT NULL,
  PRIMARY KEY (`id_paypal_customer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ps_paypal_order` (
  `id_paypal_order` int(11) NOT NULL AUTO_INCREMENT,
  `id_order` int(11) DEFAULT NULL,
  `id_cart` int(11) DEFAULT NULL,
  `id_transaction` varchar(55) DEFAULT NULL,
  `id_payment` varchar(55) DEFAULT NULL,
  `client_token` varchar(255) DEFAULT NULL,
  `payment_method` varchar(255) DEFAULT NULL,
  `currency` varchar(21) DEFAULT NULL,
  `total_paid` float DEFAULT NULL,
  `payment_status` varchar(255) DEFAULT NULL,
  `total_prestashop` float DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `payment_tool` varchar(255) DEFAULT NULL,
  `date_add` datetime DEFAULT NULL,
  `date_upd` datetime DEFAULT NULL,
  PRIMARY KEY (`id_paypal_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ps_paypal_vaulting` (
  `id_paypal_vaulting` int(11) NOT NULL AUTO_INCREMENT,
  `id_paypal_customer` int(11) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `info` varchar(255) DEFAULT NULL,
  `payment_tool` varchar(255) DEFAULT NULL,
  `date_add` datetime DEFAULT NULL,
  `date_upd` datetime DEFAULT NULL,
  PRIMARY KEY (`id_paypal_vaulting`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;