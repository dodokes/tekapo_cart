-- ps_image_type
ALTER TABLE `ps_image_type` ADD `crop` TINYINT NOT NULL DEFAULT '0' AFTER `stores`;
ALTER TABLE `ps_image_type` ADD `description` VARCHAR(50) NULL DEFAULT NULL AFTER `crop`;
ALTER TABLE `ps_image_type` ADD `deleted` TINYINT NOT NULL DEFAULT '0' AFTER `description`;

UPDATE `ps_image_type` SET `crop` = '1' WHERE `ps_image_type`.`id_image_type` = 2;
UPDATE `ps_image_type` SET `crop` = '1' WHERE `ps_image_type`.`id_image_type` = 4;

UPDATE `ps_image_type` SET `description` = '手機版列表' WHERE `ps_image_type`.`id_image_type` = 2;
UPDATE `ps_image_type` SET `description` = '桌機版列表' WHERE `ps_image_type`.`id_image_type` = 4;
UPDATE `ps_image_type` SET `description` = '商品主圖' WHERE `ps_image_type`.`id_image_type` = 5;

UPDATE `ps_image_type` SET `deleted` = '1' WHERE `ps_image_type`.`id_image_type` = 1;
UPDATE `ps_image_type` SET `deleted` = '1' WHERE `ps_image_type`.`id_image_type` = 3;
UPDATE `ps_image_type` SET `deleted` = '1' WHERE `ps_image_type`.`id_image_type` = 6;
UPDATE `ps_image_type` SET `deleted` = '1' WHERE `ps_image_type`.`id_image_type` = 7;
UPDATE `ps_image_type` SET `deleted` = '1' WHERE `ps_image_type`.`id_image_type` = 8;

ALTER TABLE `ps_image_type` CHANGE `crop` `crop` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '自動裁切';
ALTER TABLE `ps_image_type` CHANGE `description` `description` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '說明描述';
ALTER TABLE `ps_image_type` CHANGE `deleted` `deleted` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '假刪除真隱藏';

UPDATE `ps_image_type` SET `width` = '300', `height` = '300' WHERE `ps_image_type`.`id_image_type` = 2;
UPDATE `ps_image_type` SET `width` = '500', `height` = '500' WHERE `ps_image_type`.`id_image_type` = 4;

---
INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES (NULL, NULL, NULL, 'SIMPLICITY_TINYPNG_API_KEY_1', NULL, '2018-09-20 15:00:00', '2018-09-20 15:00:00');
INSERT INTO `ps_configuration` (`id_configuration`, `id_shop_group`, `id_shop`, `name`, `value`, `date_add`, `date_upd`) VALUES (NULL, NULL, NULL, 'SIMPLICITY_IMAGE_USE_CROP', '0', '2018-09-22 15:00:00', '2018-09-22 15:00:00');

---
UPDATE `ps_tab` SET `id_parent` = '77', `active` = '1' WHERE `ps_tab`.`id_tab` = 55;
UPDATE `ps_tab_lang` SET `name` = '圖片設定' WHERE `ps_tab_lang`.`id_tab` = 55 AND `ps_tab_lang`.`id_lang` = 1;

---
ALTER TABLE `ps_customer_message` CHANGE `system` `system` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '是否為系統訊息';

---

UPDATE `ps_carrier` SET `need_address` = '1' WHERE `ps_carrier`.`id_carrier` = 114;

-- 對齊 ps_carrier.id_reference

---